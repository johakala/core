// $Id: Signature.h,v 1.1 2007/08/08 16:07:31 lorsini Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _ws_security_Signature_h_
#define _ws_security_Signature_h_

#include <string>

#include "ws/security/Element.h"
#include "ws/security/Transform.h"

namespace ws
{

namespace security
{

	class Signature : public ws::security::Element
	{
	
		// Signature algorithm is taken from given security token.
		//
		//Parameters:
		// securityToken 	security token this signature will be based on (the caller can use the token 
		//			after the call, but must not delete it).
		// canonicalizationMethod 	(optional) canonicalization method to be used by this signature; 
		//				if null, default canonicalization (exclusive canonicalization without comments) will be used
		// signatureMethod 	(optional) signature method to be used by this signature; if null, default 
		//			signature (depending on the security token being used) will be used
		// tokenReferenceMethod 	(optional) token reference method; if null, default value 
		//				(determined from the securityToken) will be used
		// tokenReferenceId 	(optional) security token reference Id; if null, no Id will be set
		// id 	(optional) signature id
		Signature (ws::security::Token *securityToken, 
			const std::string &signatureMethod="", 
			const std::string &canonicalizationMethod="",
			const WASP_VString &tokenReferenceMethod="" , 
			const std::string &tokenReferenceId="",
			const std::string &id="");
		
		// Add a reference in this signature.
		// You can specify any URI (see http://www.w3.org/TR/xmldsig-core/#sec-ReferenceProcessingModel),
		//  for example "#some-id" to refer to the element (appearing anywhere in the SOAP:Envelope) bearing 
		// given ID, or an empty URI "" to refer to the whole message containing the signature, i.e. SOAP:Envelope. 
		// You can then use XPath filtering transform to select only some specific parts of the result of the 
		// URIReference resolution to be signed, e.g. using XPath transform 'ancestor-or-self::some-ns:some-local-name'. 
		// Note, however, that such filtering, when applied at a large node set (e.g. the whole message), presents 
		// performance issue. It is much better to point directly to an element bearing a specific ID.
		//
		//Parameters:
		// reference 	URI reference to data to be signed (the caller can use the reference after the call, but must not delete it)
		// digestMethod 	(optional) digest method to be applied to the reference; if null, default digest will b 	
		void 	addReference (const std::string & reference, const std::string  &digestMethod="")
 	
		// Instruct this signature to include message body. 
		void 	setSignBody (const std::string &digestMethod= "");
 	
		// Add transformation to the reference to message body. 
		// The body signature must have been previously enabled via call to setSignBody() method.
		// Note: when adding an XPath transformation, the XPath expression may ONLY be a filter expression
		// , e.g. 'ancestor-or-self::some-ns:some-local-name'. Note location paths may not be used.
		void 	addBodyTransform (ws::security::Transform *transform);
 	
		// Get signature method. 
		std::string getSignatureMethod ();
 		
		// Get canonicalization method. 
		std::string getCanonicalizationMethod ();
 		
		// Get token reference method of the token set to this signature
		std::string getTokenReferenceMethod ();
 	
		// Get token reference Id. 
		std::string getTokenReferenceId ();
 	
		// Get references assigned to this signature. 
		std::list<std::string> 	getReferences (const std::string &digestMethods);
 	
		// Determine whether message body should be signed. 
		bool getSignBody (const std::string &digestMethod, std::list<ws::security::Transform> &transforms, int &len);
 	
		protected:
		
		bool 	readOnly_;
		std::string 	signatureMethod_
		std::string 	canonicalizationMethod_;
		std::string 	tokenReferenceMethod_;
		std::string 	tokenReferenceId_;
		std::string 	uriReferences_;
		std::string 	digestMethods_;
		std::list<std::string>  digestMethodsArray_;
		bool 		doSignBody_;
		std::string	bodyDigestMethod_;
		std::list<std::string> 	bodyTransforms_;



};

}}

#endif
