// $Id: Token.h,v 1.1 2007/08/08 16:07:31 lorsini Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _ws_security_Token_h_
#define _ws_security_Token_h_

#include <string>



namespace ws
{

namespace security
{

	class Token
	{
		public:
		
		// Get default signature method. 
		virtual  std::string getDefaultSignatureMethod () = 0;
		
		//Get default encryption method. 
		virtual std::string getDefaultEncryptionMethod () = 0;
		
		// Get default key encryption method. 
		virtual std::string getDefaultKeyEncryptionMethod () = 0;
		
		// Return default token reference method. 
		virtual std::string getDefaultTokenReferenceMethod (const WASP_WSS_SecurityElement *securityElement) = 0;
		
		//Determines whether this token is used by a Signature. 
		virtual bool isUsedBySignature ();
		
		// Determines whether this token is used by an EncryptedData. 
		virtual bool isUsedByEncryptedData ();
		
		protected:
		
		//Lets this security token know it is about to be used by some security element (e.g. 
		virtual void 	usedBy (ws::security::Element *element)
 	
		//Lets this security token know it is about to be used by some security element (e.g. 
		virtual void 	initializeUsedByFlags ()

		protected:

		bool 	areUsedByFlagsInitialized_;
		bool 	isUsedBySignatureFlag_;
		bool 	isUsedByEncryptedDataFlag_;


};

}}

#endif
