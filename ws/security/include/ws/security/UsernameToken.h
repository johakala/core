// $Id: UsernameToken.h,v 1.1 2007/08/08 16:07:31 lorsini Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _ws_security_Token_h_
#define _ws_security_Token_h_

#include <string>

#include "ws/security/Token.h"

namespace ws
{

namespace security
{

	class UsernameToken : public ws::security::Token
	{
	
		enum  	PasswordType { TextPassword, DigestedPassword, NoPassword };
		
		public:
		 
		// Creates WASP_WSS_UsernameToken from current user credentials. 
		UsernameToken (const PasswordType &passwordType, bool disableNonceAndCreated=false, const std::string &id="", int iterationCount );
		
		// Get default signature method. 
		std::string getDefaultSignatureMethod ();
		
		//Get default encryption method. 
		std::string getDefaultEncryptionMethod () = 0;
		
		// Get default key encryption method. 
		std::string getDefaultKeyEncryptionMethod () = 0;
		
		// Return default token reference method. 
		std::string getDefaultTokenReferenceMethod (const WASP_WSS_SecurityElement *securityElement) = 0;
		
		
		
		protected:
		
		//Determine whether password is in plain text or is digested. 
		const ws::security::UsernameToken::PasswordType getPasswordType ();
 		
		//Determines whether "nonce" and "created" are disabled for this Username token. 
		bool 	isNonceAndCreatedDisabled ();
 		
		//Gets the Iteration count. 
		int 	getIterationCount ();
 	


		protected:


		PasswordType 	passwordType;
		bool 	disableNonceAndCreated
		int 	iterationCount


};

}}

#endif
