// $Id: Header.h,v 1.1 2007/08/08 16:07:31 lorsini Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _ws_security_Header_h_
#define _ws_security_Header_h_

#include <string>

#include "ws/security/Element.h"
#include "ws/security/Token.h"
#include "ws/security/Validator.h"

namespace ws
{

namespace security
{

	class Header
	{
		public:
		//Add given security element in the this message configuration. 
		void addSecurityElement	( ws::security::Element * securityElement);	

 		//Add given security token in the this message configuration. 
		void 	addSecurityToken (ws::security::Token *securityToken)
		
		//Enable timestamp addition in a message. 
		bool 	addTimestamp (const toolbox::TimeInterval &expirationPeriod, const std::string &id )
 			
		 //Disable Nonce and Created username token subelements checking. 
		void 	setAcceptUsernameTokenWithoutNonceAndCreated ()
		 	
		//Set requirement of reception of (optionally signed and / or encrypted) username token. 
		void 	setRequireUsernameToken (bool signatureRequired=false, bool encryptionRequired=false)
		
		//Set requirement of SOAP Body signature. 
		void 	setRequireBodySignature ()
 			
		//Set requirement of SOAP Body encryption. 
		void 	setRequireBodyEncryption ()
 		
		//Set Timestamp expiration is ignored. 	
		void 	setIgnoreTimestampExpiration ()
 			
		//Set requirement of (optionally signed and / or encrypted) timestamp. 
		void 	setRequireTimestamp (bool signatureRequired=false, bool encryptionRequired=false)
 		
		//Set extra validator to be called during incoming message processing. 
		void 	setValidator (ws::security::Validator *validator)
 		
		//Set wsse namespace URI. 	
		void 	setWsseNamespace (const std::string &wsseNamespace)
 			
		//Set wsu namespace URI. 
		void 	setWsuNamespace (const std::string &wsuNamespace)
 			
		//Control whether the configuration will be applied only to regular messages or to all types of messages (including SOAP:Faults).
		void 	setApplyToAllMessageTypes (bool applyToAll=false)
 			 
		//Sets the compatibility mode. 
		void 	setCompatibilityMode (const std::string & mode)
 			

		protected:
		
 			
		//Determine whether Nonce and Created username token subelements checking was disabled. 
		bool 	getAcceptUsernameTokenWithoutNonceAndCreated ()
 		
		//Get requirement of reception of (optionally signed and / or encrypted) username token. 	
		bool 	getRequireUsernameToken (bool &signatureRequired, bool &encryptionRequired)
 		
		//Get requirement of SOAP Body signature. 	
		bool 	getRequireBodySignature ()
 		
		//Get requirement of SOAP Body encryption. 	
		bool 	getRequireBodyEncryption ()
 		
		//Determine whether Timestamp expiration is ignored. 	
		bool 	getIgnoreTimestampExpiration ()
 		
		//Get requirement of (optionally signed and / or encrypted) timestamp. 	
		bool 	getRequireTimestamp (bool &signatureRequired, bool &encryptionRequired)
 		
		//Get extra validator to be called during incoming message processing. 	
		ws::security::Validator * getValidator ()
 		
		//Get wsse namespace URI. 	
		std::string getWsseNamespace ()
 		
		//Get wsu namespace URI. 	
		std::string getWsuNamespace ()
 		
		//Determine whether the configuration should be applied only to regular messages or to all types of messages (including SOAP:Faults). 
		bool 	getApplyToAllMessageTypes ()
		
		//Gets the compatibility mode. 
		std::string getCompatibilityMode ()
 			

		protected:

		bool 	timestampAdded_;
		bool 	acceptUsernameTokenWithoutNonceAndCreated_;
		bool 	ignoreTimestampExpiration_;
		bool 	requireUsernameToken_;
		bool 	requireUsernameTokenSignature_;
		bool 	requireUsernameTokenEncryption_;
		bool 	requireBodySignature_;
		bool 	requireBodyEncryption_;
		bool 	requireTimestamp_;
		bool 	requireTimestampSignature_;
		bool 	requireTimestampEncryption_;
		ws::security::Validator * 	validator_;
		std::string 	wsseNamespace_;
		std::string 	wsuNamespace_;
		bool 	applyToAllMessageTypes_;
		std::string 	compatibilityMode_;


};

}}

#endif
