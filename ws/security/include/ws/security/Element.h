// $Id: Element.h,v 1.1 2007/08/08 16:07:31 lorsini Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _ws_security_Element_h_
#define _ws_security_Element_h_

#include <string>



namespace ws
{

namespace security
{

	class Element
	{
		public:
		

 		Element (ws::security::Token *securityToken, const std::string &id);
 	
		// Get security token. 
		ws::security::Token * 	getSecurityToken ();
 	
		protected: 
		
		ws::security::Token * securityToken_;
};

}}

#endif
