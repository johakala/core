

#include "ws/addressing/EndpointReference.h"

ws::adressing::EndpointReference::EndpointReference( const std::string& address, const std::string& addressingURI )
{
	m_address                 = address;
	m_addressingVersionURI    = addressingURI;
}

ws::adressing::EndpointReference::~EndpointReference()
{

}
   

const std::string& ws::adressing::EndpointReference::getAddress(  )
{
	return m_address;
}

const std::string& ws::adressing::EndpointReference::getPortName(  )
{
	return m_servicePortName;
}

QName ws::adressing::EndpointReference::getPortType(  )
{
	return m_portTypeQName;
}

void ws::adressing::EndpointReference::setPortTypeQName( QName portTypeQName )
{
	m_portTypeQName = portTypeQName;
}
   
std::list<std::string>& ws::adressing::EndpointReference::getReferenceParameters(  )
{
	return m_refParams;
}

std::list<std::string>& ws::adressing::EndpointReference::getReferenceProperties(  )
{
	return m_refProps;
}

QName ws::adressing::EndpointReference::getServiceName(  )
{
	return m_serviceQName;
}

void ws::adressing::EndpointReference::setServicePortName( const std::string& servicePortName )
{
	m_servicePortName = servicePortName;
}

void ws::adressing::EndpointReference::setServiceQName( QName serviceQName )
{
	m_serviceQName = serviceQName;
}

// void ws::adressing::EndpointReference::addReferenceParameter( Object refProp )

// void ws::adressing::EndpointReference::addReferenceProperty( Object refProp )
   

#endif
