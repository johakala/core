// $Id: Subscriber.cc,v 1.1 2006/04/07 13:07:38 xdaq Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/
 

#include "ws/notification/Subscriber.h"


ws::notification::Subscriber::Subscriber( const std::string& url, const std::string& configfile )
{
	#warning "TBD"
	 // create EPR TODO clean
	 /*
	 epr = new EndpointReference( ...

	 org.xmlsoap.schemas.ws.x2004.x08.addressing.EndpointReferenceType e =
	    org.xmlsoap.schemas.ws.x2004.x08.addressing.EndpointReferenceType.Factory.newInstance(  );
	 org.xmlsoap.schemas.ws.x2004.x08.addressing.AttributedURI         auri =
	    org.xmlsoap.schemas.ws.x2004.x08.addressing.AttributedURI.Factory.newInstance(  );
	 auri.setStringValue( url );
	 e.setAddress( auri );

	 this.epr = new XmlBeansEndpointReference( e );
	 */
}


ws::addressing::EndpointReference* ws::notification::Subscriber::getEPR()
{
	return epr;
	
}

#ifdef TBD


xoap::MessageReference ws::notification::Subscriber::getCurrentMessage( std::list<Filter*>& filters )
{
	
	
	xoap::MessageReference msg = xoap::createMessage();
	return msg;
	
}


ws::notification::Subscription* ws::notification::Subscriber::subscribe( ws::notification::NotificationConsumer * notificationConsumer,
									 ws::notification::SubscriptionEndConsumer * subscriptionEndConsumer,
									 ws::notification::topics::TopicFilter * tf,
									 XPathFilter * xf,
									 toolbox::Timeval & initialTerminationTime,
									 bool UseNotify )
{
	
	SubscribeDocument           sdom = SubscribeDocument.Factory.newInstance(  );
	SubscribeDocument.Subscribe s = sdom.addNewSubscribe(  );

	//subscription ends are send to:
	//TODO
	//notifications are send to:
	s.setConsumerReference( (org.xmlsoap.schemas.ws.x2003.x03.addressing.EndpointReferenceType) ( (XmlObjectWrapper) notificationConsumer
                                                                                        	    .getEPR(  ) )
                	      .getXmlObject(  ) );

	//TODO check Calendar serializing
	s.setInitialTerminationTime( initialTerminationTime );

	//TODO multiple filters
	if ( tf != null )
	{
	 TopicExpressionType te = s.addNewTopicExpression(  );
	 XmlCursor           xc = te.newCursor(  );
	 xc.toNextToken(  );
	 xc.insertNamespace( "tns",
                	     tf.getNameSpace(  ) );
	 te.newCursor(  ).setTextValue( (String) tf.getExpression(  ) );
	 te.setDialect( tf.getURI(  ).toString(  ) );
	 
	}

	//create Subscription
	//add to local SubscriptionHome        
	org.apache.ws.notification.pubsub.Subscription ls      = this.sH.create(  );
	SubscribeResponseDocument.SubscribeResponse    sresres = null;
	try
	{
	 //now call 
	 wsaSOAPConnection sconn = wsaSOAPConnection.newInstance(  );
	 MessageFactory    mf    = MessageFactory.newInstance(  );
	 SOAPMessage       soapm = mf.createMessage(  );

	 //put XMLbean into SOAPBody
	 soapm.getSOAPBody(  ).addDocument( (org.w3c.dom.Document) sdom.newDomNode(  ) );
	 SOAPMessage                   subscribeResponse = sconn.call( soapm,
                                                        	       epr.getAddress(  ).toString(  ) );
	 java.io.ByteArrayOutputStream os = new java.io.ByteArrayOutputStream(  );
	 subscribeResponse.writeTo( os );
	 EnvelopeDocument sres =
	    EnvelopeDocument.Factory.parse( new java.io.ByteArrayInputStream( os.toByteArray(  ) ) );

	 //extract SubscribeResponse from SOAPBody

	 //SubscribeResponseDocument sresdom = SubscribeResponseDocument.Factory.newInstance();
	 //   sresres= sresdom.addNewSubscribeResponse();
	 //   sresres.set(sres.getEnvelope().getBody());
	 //

	 //TODO handle faults
	 SubscribeResponseDocument sresdom =
	    SubscribeResponseDocument.Factory.parse( sres.getEnvelope(  ).getBody(  ).xmlText(  ) );
	 sresres = sresdom.getSubscribeResponse(  );
	}
	catch ( Exception e )
	{
	 e.printStackTrace(  );
	}

	//set SubscritpionManager
	ls.setSubscriptionManager( sresres );
	ls.setNotificationConsumer( notificationConsumer );
	return ls;
	
}
#endif
