/*=============================================================================*
 *  Copyright 2004 The Apache Software Foundation
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *=============================================================================*/


#include "ws/notification/base/Subscription.h"
#include "ws/addressing.EndpointReference.h"
#include "ws/notification/base/NotificationProducerResource.h"
#include "ws/notification/base/exception/PauseFailedException.h"
#include "ws/notification/base/exception/ResumeFailedException.h"
#include "ws/notification/base/Subscription.h"
#include "ws/notification/base/SubscriptionManager.h"
#include "ws/notification/topics/Topic.h"
#include "ws/notification/topics/TopicExpression.h"
#include "ws/notification/topics/TopicExpressionException.h"
			
#include "ws/notification/Filter.h"
#include "ws/notification/NotificationConsumer.h"
#include "ws/notification/NotificationProducer.h"
#include "ws/resource/ResourceHome.h"
#include "ws/resource/WsrfRuntime.h"
#include "ws/resource/lifetime/ResourceTerminationEvent.h"
#include "ws/resource/lifetime/ResourceTerminationListener.h"
#include "ws/resource/lifetime/ResourceTerminationEventImpl.h"
#include "ws/resource/properties/ResourcePropertySet.h"
#include "ws/resource/properties/query/QueryExpression.h"
		   
#include "toolbox/TimeVal.h"
#include <string>
#include <set>
#include <slist>

/**
 * An abstract base class for WSN Subscription implementations.
 */


/**
* Creates a new {@link AbstractSubscription} object.
*
* @param producerRef DOCUMENT_ME
* @param consumerRef DOCUMENT_ME
* @param topicExpr DOCUMENT_ME
*/
ws::notification::base::Subscription::Subscription( 
   			EndpointReference* producerRef,
                        EndpointReference* consumerRef,
                        TopicExpression*   topicExpr )
{
	m_producerRef    = producerRef;
	m_consumerRef    = consumerRef;
	m_topicExpr      = topicExpr;
}

ws::notification::base::Subscription::~Subscription()
{

}

/**
* DOCUMENT_ME
*
* @param consumerRef DOCUMENT_ME
*/
void ws::notification::base::Subscription::setConsumerReference( EndpointReference* consumerRef )
{
	m_consumerRef = consumerRef;
}

/**
* DOCUMENT_ME
*
* @return DOCUMENT_ME
*/
EndpointReference*  ws::notification::base::Subscription::getConsumerReference(  )
{
	return m_consumerRef;
}

/**
* DOCUMENT_ME
*
* @return DOCUMENT_ME
*/
toolbox::TimeVal& ws::notification::base::Subscription::getCreationTime(  )
{
	return m_creationTime;
}

/**
* DOCUMENT_ME
*
* @return DOCUMENT_ME
*/
toolbox::TimeVal ws::notification::base::Subscription::getCurrentTime(  )
{
	return toolbox::TimeVal::gettimeofday();
}

   /**
    * DOCUMENT_ME
    *
    * @return DOCUMENT_ME
    */
    /*
   URI getDeliveryMode(  )
   {
      // TODO. *SJC* This is provided for pubsub abstraction layer
      return null;
   }
   */

/**
* DOCUMENT_ME
*
* @param epr DOCUMENT_ME
*/
void ws::notification::base::Subscription::setEndpointReference( EndpointReference* epr )
{
	m_epr = epr;
}

/**
* DOCUMENT_ME
*
* @return DOCUMENT_ME
*/
EndpointReference* ws::notification::base::Subscription::getEndpointReference(  )
{
	return m_epr;
}

/**
* DOCUMENT_ME
*
* @return DOCUMENT_ME
*/
Filter* ws::notification::base::Subscription::getFilters(  )
{
	// TODO. *SJC* This is provided for pubsub abstraction layer
	return 0;
}

/**
* DOCUMENT_ME
*
* @param id DOCUMENT_ME
*/
void ws::notification::base::Subscription::setID( const std::string& id )
{
	m_id = id;
}

/**
* DOCUMENT_ME
*
* @return DOCUMENT_ME
*/
const std::string& ws::notification::base::Subscription::getID(  )
{
	return m_id;
}

/**
* DOCUMENT_ME
*
* @param notificationConsumer DOCUMENT_ME
*/
void ws::notification::base::Subscription::setNotificationConsumer( NotificationConsumer* notificationConsumer )
{
	m_notificationConsumer = notificationConsumer;
}

/**
* DOCUMENT_ME
*
* @return DOCUMENT_ME
*/
NotificationConsumer* ws::notification::base::Subscription::getNotificationConsumer(  )
{
	return m_notificationConsumer;
}

/**
* DOCUMENT_ME
*
* @param notificationProducer DOCUMENT_ME
*/
void ws::notification::base::Subscription::setNotificationProducer( NotificationProducer* notificationProducer )
{
	m_notificationProducer = notificationProducer;
}

/**
* DOCUMENT_ME
*
* @return DOCUMENT_ME
*/
NotificationProducer* ws::notification::base::Subscription::getNotificationProducer(  )
{
	return m_notificationProducer;
}

/**
* DOCUMENT_ME
*
* @return DOCUMENT_ME
*/
bool ws::notification::base::Subscription::isPaused(  )
{
	return m_isPaused;
}

   /**
    * DOCUMENT_ME
    *
    * @param policy DOCUMENT_ME
    */
    /*
   void ws::notification::base::Subscription::setPolicy( Object policy )
   {
      m_policy = policy;
   }
   */

   /**
    * DOCUMENT_ME
    *
    * @return DOCUMENT_ME
    */
    /*
   Object ws::notification::base::Subscription::getPolicy(  )
   {
      return m_policy;
   }
   */

/**
* DOCUMENT_ME
*
* @param precondition DOCUMENT_ME
*/
void ws::notification::base::Subscription::setPrecondition( QueryExpression* precondition )
{
	m_precondition = precondition;
}

/**
* DOCUMENT_ME
*
* @return DOCUMENT_ME
*/
QueryExpression* ws::notification::base::Subscription::getPrecondition(  )
{
	return m_precondition;
}

/**
* DOCUMENT_ME
*
* @return DOCUMENT_ME
*/
EndpointReference* ws::notification::base::Subscription::getProducerReference(  )
{
	return m_producerRef;
}

/**
* DOCUMENT_ME
*
* @return DOCUMENT_ME
*/
NotificationProducerResource* ws::notification::base::Subscription::getProducerResource(  )
{
	std::string producerAddress     = m_producerRef->getAddress(  );

	size_type pos = producerAddress.find_last_of( '/' );

	std::string producerServiceName = "";
	if (pos != std::string::npos)
	{
		producerServiceName = producerAddress.substr(pos+1);
	}

	try
	{
		ResourceHome* producerHome = WsrfRuntime::getRuntime(  )->getResourceHome( producerServiceName );
		std::string producerId = producerHome->extractResourceIdentifier( m_producerRef );
		NotificationProducerResource* npResource = producerHome->find( producerId );
		return npResource;
	}
	catch ( ws::notification::exception::Exception& e )
	{
		#warning "TBD: Catch propert exception in getProducerResource()"
		XCEPT_RETHROW (ws::notification::exception::RuntimeException("Failed to lookup NotificationProducer resource", e );
	}
}

/**
* DOCUMENT_ME
*
* @param resourcePropertySet DOCUMENT_ME
*/
void ws::notification::base::Subscription::setResourcePropertySet( ResourcePropertySet* resourcePropertySet )
{
	m_propSet = resourcePropertySet;
}

/**
* DOCUMENT_ME
*
* @return DOCUMENT_ME
*/
ResourcePropertySet* ws::notification::base::Subscription::getResourcePropertySet(  )
{
	return m_propSet;
}

/**
* DOCUMENT_ME
*
* @param selector DOCUMENT_ME
*/
void ws::notification::base::Subscription::setSelector( QueryExpression* selector )
{
	m_selector = selector;
}

/**
* DOCUMENT_ME
*
* @return DOCUMENT_ME
*/
QueryExpression* ws::notification::base::Subscription::getSelector(  )
{
	return m_selector;
}

/**
* DOCUMENT_ME
*
* @param time DOCUMENT_ME
*/
void ws::notification::base::Subscription::setTerminationTime( const toolbox::TimeVal& time )
{
	m_terminationTime = time;
}

/**
* DOCUMENT_ME
*
* @return DOCUMENT_ME
*/
const toolbox::TimeVal& ws::notification::base::Subscription::getTerminationTime(  )
{
	return m_terminationTime;
}

/**
* DOCUMENT_ME
*
* @param topicExpr DOCUMENT_ME
*/
void ws::notification::base::Subscription::setTopicExpression( TopicExpression* topicExpr )
{
	m_topicExpr = topicExpr;
}

/**
* DOCUMENT_ME
*
* @return DOCUMENT_ME
*/
TopicExpression* ws::notification::base::Subscription::getTopicExpression(  )
{
	return m_topicExpr;
}

/**
* DOCUMENT_ME
*
* @param useNotify DOCUMENT_ME
*/
void ws::notification::base::Subscription::setUseNotify( bool useNotify )
{
	m_useNotify = useNotify;
}

/**
* DOCUMENT_ME
*
* @return DOCUMENT_ME
*/
bool ws::notification::base::Subscription::getUseNotify(  )
{
	return m_useNotify;
}

/**
* DOCUMENT_ME
*
* @param resourceTerminationListener DOCUMENT_ME
*/
void ws::notification::base::Subscription::addTerminationListener( 
	ResourceTerminationListener* resourceTerminationListener )
{
	m_terminationListeners.push_back( resourceTerminationListener );
}

/**
* DOCUMENT_ME
*/
void ws::notification::base::Subscription::destroy(  )
{
	SubscriptionManager::getInstance(  )->removeSubscription( 
      		this,
        	this->evaluateTopicExpression(  ) );
	this->notifyResourceTerminationListeners(  );
}

/**
* @see org.apache.ws.notification.base.Subscription#pause()
*/
void ws::notification::base::Subscription::pause(  )
	throw (ws::notification::base::exception::PauseFailedException)
{
	m_isPaused = true;
}

/**
* @see org.apache.ws.notification.base.Subscription#resume()
*/
void ws::notification::base::Subscription::resume(  )
	throw (ws::notification::base::exception::ResumeFailedException)
{
	m_isPaused = false;
}

/**
* @see org.apache.ws.notification.base.Subscription#unsubscribe()
*/
void ws::notification::base::Subscription::unsubscribe(  )
{
	this->destroy(  );
}

   /*
   std::string ws::notification::base::Subscription::createUuid(  )
   {
      return IdentifierUtils.UUID_VERSION_FOUR_GENERATOR.nextIdentifier(  ).toString(  );
   }
   */

std::set<Topic*> ws::notification::base::Subscription::evaluateTopicExpression(  )
	throw (ws::notification::topics::Expression)
{
	try
	{
		return getProducerResource(  )->getTopicSet(  )->evaluateTopicExpression( m_topicExpr );
	}
	catch ( ws::notification::topics::Expression& tee )
	{
		XCEPT_RETHROW (ws::notification::topics::Expression, "An exception occurred evaluating the topic expression");
	}
}

void ws::notification::base::Subscription::notifyResourceTerminationListeners(  )
{
	ResourceTerminationEvent rte(this->getID(), "Destroyed subscription with id " + this->getID() );

	std::list<ResourceTerminationListener*>::iterator i;

	for (i = m_terminationListeners.begin(); i != m_terminationListeners.end(); ++i)
	{
		ResourceTerminationListener* resourceTerminationListener = (*i);
		resourceTerminationListener->terminationOccurred( rte );
	}
}

