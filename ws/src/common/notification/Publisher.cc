// $Id: Publisher.cc,v 1.1 2006/04/07 13:07:38 xdaq Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/


#include "ws/notification/Publisher.h"

ws::notification::Publisher::Publisher( const std::string& url )
{
	m_url = url;
}

void ws::notification::Publisher::publish( xoap::MessageReference soapMsg )
{
#warning "TBD: ws::notification::Publisher::publish"
}
