// $Id: ConcreteTopicExpression.cc,v 1.1 2006/04/07 13:07:38 xdaq Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include <string> // for string
#include <new> // for bad_alloc
#include <typeinfo> // for bad_cast, bad_typeid
#include <exception> // for exception, bad_exception
#include <stdexcept> // for std exception hierarchy 

#include "ws/notification/topics/ConcreteTopicExpression.h"


ws::notification::topics::ConcreteTopicExpression::ConcreteTopicExpression( const std::string &  qname ): 
	ws::notification::topics::TopicExpression( ws::notification::topics::TOPIC_EXPR_DIALECT_CONCRETE )
{
	m_topicPath = qname;
}


std::string ws::notification::topics::ConcreteTopicExpression::getTopicPath()
{
	return m_topicPath;
}


bool ws::notification::topics::ConcreteTopicExpression::equals( ws::notification::topics::ConcreteTopicExpression::TopicExpression * te )
{
	if ( te != 0 )
	{
		try 
		{
      			return dynamic_cast<ConcreteTopicExpression*>(te)->getTopicPath() == m_topicPath;
		}
		catch(const std::bad_cast & ex)
		{
			return false;
		}
	} 

	return false;
}
