// $Id: TopicsTypeReader.cc,v 1.1 2006/04/07 13:07:38 xdaq Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "ws/notification/topics/TopicsTypeReader.h"

ws::notification::topics::TopicsTypeReader newInstance( const std::string & schemaNamespace ) 
	throw ( ws::notification::topics::exception::UnsupportedNamespace )
{
	#warning "TBD"
	/*
	if ( schemaNamespace.equals( TopicsConstants.NSURI_WSTOP_SCHEMA ) )
	{
	 return new TopicsTypeReaderImpl(  );
	}
	else
	{
	 throw new UnsupportedNamespaceException( schemaNamespace
                                        	  + " is not the namespace of a supported version of the WS-Topics schema." );
	}
	*/
}


ws::notification::topics::TopicExpression * ws::notification::topics::TopicsTypeReader::toTopicExpression( Document * xBean ) 
	throw ( ws::notification::topics::exception::InvalidTopicExpression)
{
	#warning "TBD"
	/*
	TopicExpressionType topicExprType = toTopicExpressionType( xBean );
	if ( !topicExprType.isSetDialect(  ) )
	{
	 throw new IllegalStateException( "The Dialect attribute is required by the WS-BaseNotification TopicExpressionType." );
	}

	String          dialect   = topicExprType.getDialect(  );
	TopicExpression topicExpr;
	if ( dialect.equals( TopicsConstants.TOPIC_EXPR_DIALECT_SIMPLE ) )
	{
	 topicExpr = new SimpleTopicExpression( XmlBeanUtils.getValueAsQName( topicExprType ) );
	}
	else if ( dialect.equals( TopicsConstants.TOPIC_EXPR_DIALECT_CONCRETE ) )
	{
	 topicExpr = new ConcreteTopicExpression( XmlBeanUtils.getValueAsQName( topicExprType ) );
	}
	else if ( dialect.equals( TopicsConstants.TOPIC_EXPR_DIALECT_FULL ) )
	{
	 String          expr       = XmlBeanUtils.getValue( topicExprType );
	 StringTokenizer tokenizer  = new StringTokenizer( expr, "|" );
	 QName[]         topicPaths = new QName[tokenizer.countTokens(  )];
	 int             i          = 0;
	 while ( tokenizer.hasMoreTokens(  ) )
	 {
	    topicPaths[i++] = toQName( tokenizer.nextToken(  ),
                        	       new XmlBeansNamespaceContext( topicExprType ) );
	 }

	 topicExpr = new FullTopicExpression( topicPaths );
	}
	else
	{
	 throw new IllegalStateException( "Unknown TopicExpression dialect: " + dialect );
	}

	return topicExpr;
	*/
}


ws::notification::topics::TopicNamespace * ws::notification::topics::TopicsTypeReader::toTopicNamespace( Document * xBean )
{
	#warning "TBD"
	/*
	TopicSpaceType topicSpaceType = toTopicSpaceType( xBean );

	// TODO: validate the TopicSpaceType
	TopicNamespace topicNs =
	 TopicsTypeFactory.newInstance(  ).createTopicNamespace( topicSpaceType.getTargetNamespace(  ) );
	topicNs.setName( topicSpaceType.getName(  ) );
	importTopics( topicSpaceType.getTopicArray(  ),
        	    topicNs );
	return topicNs;
	*/
}



void ws::notification::topics::TopicsTypeWriter::importTopics( std::list<ws::notification::topics::TopicType> &  topicTypes,
    							ws::notification::topics::TopicSiblingSet & topicSiblingSet )
{
	#warning "TBD"
	/*
	for ( int i = 0; i < topicTypes.length; i++ )
	{
	 TopicType topicType = topicTypes[i];
	 Topic     topic = topicSiblingSet.addTopic( topicType.getName(  ) );
	 importTopics( topicType.getTopicArray(  ),
        	       topic ); // recurse
	}
	*/
}

QName ws::notification::topics::TopicsTypeWriter::toQName( const std::string & topicPath,  NamespaceContext nsContext ) 
	throw ( ws::notification::topics::exception::InvalidTopicExpression)
{
	#warning "TBD"
	/*
	String prefix;
	String localPart;
	int    i = topicPath.indexOf( ':' );
	if ( i == -1 ) // no prefix
	{
	 prefix       = "";
	 localPart    = topicPath;
	}
	else if ( i == 0 )
	{
	 throw new InvalidTopicExpressionException( "Topic path '" + topicPath + "' starts with a colon." );
	}
	else if ( i == ( topicPath.length(  ) - 1 ) )
	{
	 throw new InvalidTopicExpressionException( "Topic path '" + topicPath + "' ends with a colon." );
	}
	else
	{
	 prefix       = topicPath.substring( 0, i );
	 localPart    = topicPath.substring( i + 1 );
	 if ( localPart.indexOf( ':' ) != -1 )
	 {
	    throw new InvalidTopicExpressionException( "Local part of topic path '" + topicPath
                                        	       + "' contains a colon." );
	 }
	}

	String nsURI = nsContext.getNamespaceURI( prefix );
	return new QName( nsURI, localPart, prefix );
	*/
}

ws::notification::topics::TopicExpressionType  ws::notification::topics::TopicsTypeWriter::toTopicExpressionType( Document * xBean )
{
	#warning "TBD"
	/*
	TopicExpressionType topicExpressionType;
	try
	{
	 topicExpressionType = (TopicExpressionType) xBean;
	}
	catch ( ClassCastException cce )
	{
	 if ( xBean instanceof TopicExpressionDocument )
	 {
	    TopicExpressionDocument topicExpressionDoc = (TopicExpressionDocument) xBean;
	    topicExpressionType = topicExpressionDoc.getTopicExpression(  );
	 }
	 else
	 {
	    throw new IllegalArgumentException(  );
	 }
	}

	return topicExpressionType;
	*/
}

ws::notification::topics::TopicSpaceType  ws::notification::topics::TopicsTypeWriter::toTopicSpaceType( Document * xBean )
{
	#warning "TBD"
	/*
	TopicSpaceType topicSpaceType;
	try
	{
	 topicSpaceType = (TopicSpaceType) xBean;
	}
	catch ( ClassCastException cce )
	{
	 if ( xBean instanceof TopicSpaceDocument )
	 {
	    TopicSpaceDocument topicSpaceDoc = (TopicSpaceDocument) xBean;
	    topicSpaceType = topicSpaceDoc.getTopicSpace(  );
	 }
	 else
	 {
	    throw new IllegalArgumentException(  );
	 }
	}

	return topicSpaceType;
	*/
}


