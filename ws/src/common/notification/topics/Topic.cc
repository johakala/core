// $Id: Topic.cc,v 1.1 2006/04/07 13:07:38 xdaq Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "ws/notification/topics/exception/InvalidTopic.h"
#include "ws/notification/topics/exception/TopicNotFound.h"

ws::notification::topics::Topic::Topic( const std::string& name )
{
	m_name = name;
	m_topicNs = 0;
	m_reference = 0;
}

const std::string& ws::notification::topics::Topic::getName(  )
{
	return m_name;
}

bool ws::notification::topics::Topic::isReference(  )
{
	return (m_reference != 0);
}

Topic* ws::notification::topics::Topic::getTopic( const std::string& topicName )
	throw (ws::notification::topics::exception::TopicNotFound)
{
	std::map<std::string, Topic*>::iterator i = m_subTopics.find(topicName);
	if (i != m_subTopics.end())
	{
		return (*i).second;
	}
	else
	{
		std::string msg = "Topic ";
		msg += topicName;
		msg += " not found";
		XCEPT_RAISE(ws::notification::topics::exception::TopicNotFound, msg);
	}
}

void ws::notification::topics::Topic::setTopicNamespace( TopicNamespace* topicNs )
{
	this->m_topicNs = topicNs;
}

TopicNamespace* ws::notification::topics::Topic::getTopicNamespace(  )
{
	return m_topicNs;
}

void ws::notification::topics::Topic::setTopicPath( std::list<std::string>& topicPath )
{
	this->m_topicPath = topicPath;
}

std::list<std::string>& ws::notification::topics::Topic::getTopicPath(  )
{
	return m_topicPath;
}

void ws::notification::topics::Topic::setTopicReference( TopicExpression* topicPath )
{
	this->m_reference = topicPath;
}

TopicExpression* ws::notification::topics::Topic::getTopicReference(  )
{
	return m_reference;
}

Topic* ws::notification::topics::Topic::addTopic( const std::string& name )
	throw (ws::notification::topics::exception::InvalidTopic)
{
	Topic* t = new Topic( name );
	
	try
	{
		this->addTopic( t );
	}
	catch (ws::notification::topics::exception::InvalidTopic& e)
	{
		delete t;
		XCEPT_RETHROW (ws::notification::topics::exception::InvalidTopic, "Failed to add topic", e);
	}
}

bool ws::notification::topics::Topic::containsTopic( const std::string& name )
{
	return m_subTopics.count( name );
}

bool ws::notification::topics::Topic::equals( Topic* topic )
{
	if (topic != 0)
	{
   		return (this->getName() == topic->getName())
	}
	else
	{
		return false;
	}
}

void ws::notification::topics::Topic::removeTopic( const std::string& name )
  throw (ws::notification::topics::exception::TopicNotFound)
{
	std::map<std::string, Topic*>::iterator i = m_subTopics.find(name);
	if (i != m_subTopics.end())
	{
		Topic* t = (*i).second;
		m_subTopics.erase(i);
		delete t;
	}
	else
	{
		std::string msg = "Topic ";
		msg += topicName;
		msg += " not found";
		XCEPT_RAISE(ws::notification::topics::exception::TopicNotFound, msg);	
	}
}

void ws::notification::topics::Topic::removeTopic( Topic* topic )
	throw (ws::notification::topics::exception::TopicNotFound)
{
	this->removeTopic( topic.getName() );
}

/**
* Returns a String representation of this Topic in following format: "{" + target_namespace + "}" + topic_path. If
* the target namespace is "" (i.e. the ad-hoc namespace), only the topic path is returned. For example:
* "{http://example.org/topicSpace/example1}t1/t3" or "t7/t8".
*
* @return a String representation of this Topic
*/
std::string ws::notification::topics::Topic::toString(  )
{
	std::string strBuf = "";	
	std::string targetNs = getTopicNamespace(  )->getTargetNamespace(  );

	if ( targetNs != "" )
	{
		strBuf  = '{';
		strBuf += targetNs;
		strBuf += '}';
	}	

	std::string path = "";	
	std::list<std::string>::iterator i;
	for (i = m_topicPath.begin(); i != m_topicPath.end(); ++i)
	{
	    if ( path != "" ) path += '/';
	    path += (*i);
	}

	strBuf += path;

	return strBuf;
}


std::set<Topic*> ws::notification::topics::Topic::getTopics(  )
{
	std::set<Topic*> s;
	std::map<std::string, Topic*>::iterator i;

	for (i = m_subTopics.begin(); i != m_subTopics.end(); ++i)
	{
		s.insert( (*i).second );
	}
	return s;	
}


Topic* ws::notification::topics::Topic::addTopic( Topic* topic )
throw (ws::notification::topics::exception::InvalidTopic)
{
	if (topic == 0)
	{
		XCEPT_RAISE (ws::notification::topics::exception::InvalidTopic, "Cannot add null pointer topic object");
	}

	if ( m_reference != 0 )
	{
		XCEPT_RAISE (ws::notification::topics::exception::InvalidTopic, "Cannot modify the TopicExpression reference by adding an additional Topic." );
	}

	std::string name = topic->getName();
	std::map<std::string, Topic*>::iterator i = m_subTopics.find( name );
	if (i != m_subTopics.end())
	{
		// replace topic
		m_subTopics.erase(i);
		Topic* t = (*i).second;
		delete t;		
	}
	
	topic->setTopicNamespace (m_topicNs );

	// Add topic name to topic path
	std::list<std::string> topicPath;
	topicPath = m_topicPath;
	topicPath.push_back( topic->getName(  ) );
	topic->setTopicPath( topicPath );
	m_subTopics[name] = topic;
	
	return topic;
}
