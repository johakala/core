// $Id: TopicNamespace.cc,v 1.1 2006/04/07 13:07:38 xdaq Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "ws/notification/topics/Topic.h"
#include "ws/notification/topics/TopicNamespace.h"

/**
* An implementation of a <code>TopicNamespace</code>.
*/

/**
* Creates a topic namespace with the specified target namespace.
*
* @param targetNs DOCUMENT_ME
*/
ws::notification::topics::TopicNamespace::TopicNamespace( const std::string& targetNs )
throw (ws::notification::topics::exception::InvalidNamespace)
{
	// \"\" is allowed to represent an empty namespace

	m_targetNs = targetNs;
}
   
ws::notification::topics::TopicNamespace::~TopicNamespace()
{

}

/**
* DOCUMENT_ME
*
* @param finality DOCUMENT_ME
*/
void ws::notification::topics::TopicNamespace::setFinal( bool finality )
{
	m_final = finality;
}

/**
* DOCUMENT_ME
*
* @return DOCUMENT_ME
*/
bool ws::notification::topics::TopicNamespace::isFinal(  )
{
	return m_final;
}

/**
* DOCUMENT_ME
*
* @param name DOCUMENT_ME
*/
void ws::notification::topics::TopicNamespace::setName( const std::string& name )
{
	m_name = name;
}

/**
* DOCUMENT_ME
*
* @return DOCUMENT_ME
*/
const std::string& ws::notification::topics::TopicNamespace::getName(  )
{
	return m_name;
}

/**
* DOCUMENT_ME
*
* @return DOCUMENT_ME
*/
const std::string& ws::notification::topics::TopicNamespace::getTargetNamespace(  )
{
	return m_targetNs;
}

/**
* DOCUMENT_ME
*
* @param name DOCUMENT_ME
*
* @return DOCUMENT_ME
*/
Topic* ws::notification::topics::TopicNamespace::getTopic( const std::string& name ) 
throw (ws::notification::topics::exception::TopicNotFound)
{
	std::map<std::string, Topic*>::iterator i = m_rootTopicMap.find(name);
	if (i != m_rootTopicMap.end())
	{
		return (*i).second;
	}
	else
	{
		std::string msg = "Topic ";
		msg += name;
		msg += " not found";
		XCEPT_RAISE (ws::notification::topics::exception::TopicNotFound, msg);
	}
}

/**
* DOCUMENT_ME
*
* @param name DOCUMENT_ME
*
* @return DOCUMENT_ME
*/
Topic* ws::notification::topics::TopicNamespace::addTopic( const std::string& name ) 
	throw (ws::notification::topics::exception::InvalidTopic)
{
	Topic* t = new Topic( name );
	try
	{
		return this->addTopic( t );
	}
	catch ()
	{
		delete t;
		XCEPT_RETHROW (ws::notification::topics::exception::InvalidTopic, "Failed to add topic", e);
	}
}

/**
* DOCUMENT_ME
*
* @param name DOCUMENT_ME
*
* @return DOCUMENT_ME
*/
bool ws::notification::topics::TopicNamespace::containsTopic( const std::string& name )
{
	return m_rootTopicMap.count( name );
}

/**
* DOCUMENT_ME
*
* @param obj DOCUMENT_ME
*
* @return DOCUMENT_ME
*/
bool ws::notification::topics::TopicNamespace::equals( TopicNamespace* obj )
{
	if (obj != 0)
	{
		return ( this->m_targetNs == obj->getTargetNamespace() );
	}
	else
	{
		return false;
	}
}

/**
* DOCUMENT_ME
*
* @param name DOCUMENT_ME
*/
void ws::notification::topics::TopicNamespace::removeTopic( const std::string& name )
   throw (ws::notification::topics::exception::TopicNotFound)
{
	std::map<std::string, Topic*>::iterator i = m_rootTopicMap.find(name);
	if (i != m_rootTopicMap.end())
	{
		Topic* t = (*i).second;
		m_rootTopicMap.erase(i);
		delete t;
	}
	else
	{
		std::string msg = "Topic ";
		msg += name;
		msg += " not found";
		XCEPT_RAISE(ws::notification::topics::exception::TopicNotFound, msg);	
	}
}

/**
* Returns a const std::string& representation of this TopicNamespace in following format:
* "{" + target_namespace + "}" + name. If the target namespace is
* "" (i.e. the ad-hoc namespace), only the name is returned. If the name is
* null, the name is ommitted.
* For example: "{http://example.org/topicSpace/example1}TopicSpaceExample1" or
* "AdHocTopicSpace".
*
* @return a const std::string& representation of this TopicNamespace
*/
std::string ws::notification::topics::TopicNamespace::toString(  )
{
	std::string strBuf = "";	

	if ( m_targetNs != "" )
	{
		strBuf  = '{';
		strBuf += m_targetNs;
		strBuf += '}';
	}

	if ( m_name != "" )
	{
		strBuf += m_name;
	}	

	return strBuf;
}

/**
* DOCUMENT_ME
*
* @return DOCUMENT_ME
*/
std::set<Topic*> ws::notification::topics::TopicNamespace::getTopics(  )
{
	std::set<Topic*> s;
	std::map<std::string, Topic*>::iterator i;

	for (i = m_rootTopicMap.begin(); i != m_rootTopicMap.end(); ++i)
	{
		s.insert( (*i).second );
	}
	return s;
}

   /**
    * DOCUMENT_ME
    *
    * @param topic DOCUMENT_ME
    *
    * @return DOCUMENT_ME
    */
Topic* ws::notification::topics::TopicNamespace::addTopic( Topic* topic )
   throw (ws::notification::topics::exception::InvalidTopic)
{
	if (topic == 0)
	{
		XCEPT_RAISE (ws::notification::topics::exception::InvalidTopic, "Cannot add null pointer topic object");
	}

	std::string name = topic->getName();
	std::map<std::string, Topic*>::iterator i = m_rootTopicMap.find( name );
	if (i != m_subTopics.end())
	{
		// replace topic
		m_rootTopicMap.erase(i);
		Topic* t = (*i).second;
		delete t;	
	}	

	topic->setTopicNamespace( this );
	std::list<std::string> topicPath;
	topicPath.push_back( name );
	topic->setTopicPath( topicPath );
	m_rootTopicMap[topic->getName(  )] = topic;

	return topic;
}
