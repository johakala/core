// $Id: FullTopicExpressionEvaluator.cc,v 1.1 2006/04/07 13:07:38 xdaq Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "ws/notification/topics/FullTopicExpressionEvaluator.h"
#include "ws/notification/topics/TopicNamespaceRegistry.h"
#include "ws/notification/topics/TopicsConstants.h"


ws::notification::topics::FullTopicExpressionEvaluator::FullTopicExpressionEvaluator()
{
	SUPPORTED_DIALECTS.push_back(ws::notification::topics::TOPIC_EXPR_DIALECT_FULL);
}
	
std::list<std::string> & ws::notification::topics::FullTopicExpressionEvaluator::getDialects(  )
{
	return SUPPORTED_DIALECTS;
}

std::list<ws::notification::topics::Topic*> ws::notification::topics::FullTopicExpressionEvaluator::evaluate( ws::notification::topics::TopicNamespaceRegistry * topicNsRegistry,
                     					 ws::notification::topics::TopicExpression        &   topicExpr )
		throw( ws::notification::topics::exception::TopicPathDialectUnknown, 
		       ws::notification::topics::exception::TopicExpressionNoResolution, 
		       ws::notification::topics::exception::InvalidTopicExpression, 
		       ws::notification::topics::exception::Expression)
{
	std::list<ws::notification::topics::Topic*> results;
	#warning "TBD"
	/*
	if ( RESULTS_CACHE.containsKey( topicExpr ) )
	{
	 results = (Topic[]) RESULTS_CACHE.get( topicExpr );
	}
	else
	{
	 FullTopicExpression fullTopicExpr    = (FullTopicExpression) topicExpr;
	 Set                 allMatchedTopics = new HashSet(  );
	 QName[]             topicPaths       = fullTopicExpr.getTopicPaths(  );
	 for ( int i = 0; i < topicPaths.length; i++ )
	 {
	    List matchedTopics = evaluateTopicPath( topicNsRegistry, topicPaths[i] );
	    allMatchedTopics.addAll( matchedTopics );
	 }

	 results = (Topic[]) allMatchedTopics.toArray( new Topic[0] );
	}
	*/
	return results;
}

std::list<ws::notification::topics::Topic*> ws::notification::topics::FullTopicExpressionEvaluator::evaluateTopicPath( ws::notification::topics::TopicNamespaceRegistry * topicNsRegistry, const std::string & topicPath )
		throw( ws::notification::topics::exception::TopicExpressionNoResolution, ws::notification::topics::exception::InvalidTopicExpression){

	validateTopicPath( topicPath );
	std::list<ws::notification::topics::Topic*> matchedTopics;
	#warning "TBD"
	/*
	TopicNamespace topicNs = getTopicNamespace( topicNsRegistry, topicPath );
	if ( topicPath.getLocalPart(  ).indexOf( "///" ) != -1 )
	{
	 throw new InvalidTopicExpressionException( "Topic path '" + topicPath
                                        	    + "' contains an empty path component." );
	}

	PathTokenizer pathTokenizer     = new PathTokenizer( topicPath.getLocalPart(  ) );
	List          topicSetsToSearch = new ArrayList(  );
	topicSetsToSearch.add( topicNs );
	boolean atFirstToken = true;
	while ( pathTokenizer.hasMoreTokens(  ) )
	{
	 String pathToken = pathTokenizer.nextToken(  );
	 matchedTopics.clear(  );
	 for ( int i = 0; i < topicSetsToSearch.size(  ); i++ )
	 {
	    TopicSiblingSet topicSiblingSetToSearch = (TopicSiblingSet) topicSetsToSearch.get( i );
	    boolean         recurse = pathToken.startsWith( "/" );
	    String          name    = recurse ? pathToken.substring( 1 ) : pathToken;
	    matchedTopics.addAll( findTopics( topicSiblingSetToSearch, name, recurse ) );
	 }

	 if ( atFirstToken && matchedTopics.isEmpty(  ) )
	 {
	    throw new InvalidTopicExpressionException( "Topic path '" + topicPath
                                        	       + "' refers to a root topic that is not defined in the referenced topic space." );
	 }

	 topicSetsToSearch.clear(  );
	 topicSetsToSearch.addAll( matchedTopics );
	 atFirstToken = false;
	}
	*/
	return matchedTopics;
}

std::list<ws::notification::topics::Topic*>  ws::notification::topics::FullTopicExpressionEvaluator::findTopics( ws::notification::topics::TopicSiblingSet * topicSiblingSet, const std::string & name )
{
	#warning "TBD"
	std::list<ws::notification::topics::Topic*> matchedTopics;
	/*
	if ( name.equals( "*" ) )
	{
	 Iterator topicIter = topicSiblingSet.topicIterator(  );
	 while ( topicIter.hasNext(  ) )
	 {
	    matchedTopics.add( (Topic) topicIter.next(  ) );
	 }
	}
	else
	{
	 if ( topicSiblingSet.containsTopic( name ) )
	 {
	    matchedTopics.add( topicSiblingSet.getTopic( name ) );
	 }
	}
	*/
	return matchedTopics;

}

std::list<ws::notification::topics::Topic*>  ws::notification::topics::FullTopicExpressionEvaluator::findTopics( ws::notification::topics::TopicSiblingSet * topicSiblingSet, const std::string &  name, bool recurse ) 
		throw (ws::notification::topics::exception::InvalidTopicExpression)
{
	std::list<ws::notification::topics::Topic*> allMatchedTopics;
	#warning "TBD"
	/*
	if ( name.equals( "." ) )
	{
	 allMatchedTopics.add( topicSiblingSet );
	 name = "*"; // we only want to evaluate "." the first time through during recursion
	}

	List matchedTopics = findTopics( topicSiblingSet, name );
	allMatchedTopics.addAll( matchedTopics );
	if ( recurse )
	{
	 Iterator topicIter = topicSiblingSet.topicIterator(  );
	 while ( topicIter.hasNext(  ) )
	 {
	    allMatchedTopics.addAll( findTopics( (Topic) topicIter.next(  ), name, recurse ) );
	 }
	}
	*/
	return allMatchedTopics;
}
