// $Id: ConcreteTopicExpressionEvaluator.cc,v 1.1 2006/04/07 13:07:38 xdaq Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "ws/notification/topics/ConcreteTopicExpressionEvaluator.h"
#include "ws/notification/topics/TopicsConstants.h"
#include "ws/notification/topics/TopicNamespaceRegistry.h"

	
ws::notification::topics::ConcreteTopicExpressionEvaluator::ConcreteTopicExpressionEvaluator()
{
	SUPPORTED_DIALECTS.push_back(ws::notification::topics::TOPIC_EXPR_DIALECT_CONCRETE);
}
	
std::list<std::string> & ws::notification::topics::ConcreteTopicExpressionEvaluator::getDialects(  )
{
	return SUPPORTED_DIALECTS;
}

   
std::list<ws::notification::topics::Topic*> ws::notification::topics::ConcreteTopicExpressionEvaluator::evaluate( ws::notification::topics::TopicNamespaceRegistry * topicNsRegistry,
                     					 ws::notification::topics::TopicExpression        &   topicExpr )
		throw( ws::notification::topics::exception::TopicPathDialectUnknown, 
		       ws::notification::topics::exception::TopicExpressionNoResolution, 
		       ws::notification::topics::exception::InvalidTopicExpression, 
		       ws::notification::topics::exception::Expression)
{
	std::list<ws::notification::topics::Topic*>  results;
	#warning "TBD"

	/*
	if ( RESULTS_CACHE.containsKey( topicExpr ) )
	{
	 results = (Topic[]) RESULTS_CACHE.get( topicExpr );
	}
	else
	{
	 ConcreteTopicExpression concreteTopicExpr = (ConcreteTopicExpression) topicExpr;
	 results = evaluateTopicPath( topicNsRegistry,
                        	      concreteTopicExpr.getTopicPath(  ) );
	 RESULTS_CACHE.put( topicExpr, results );
	}
	*/
	return results;
}


std::list<ws::notification::topics::Topic*> ws::notification::topics::ConcreteTopicExpressionEvaluator::evaluateTopicPath( ws::notification::topics::TopicNamespaceRegistry * topicNsRegistry, const std::string & topicPath )
		throw ( ws::notification::topics::exception::TopicExpressionNoResolution, ws::notification::topics::exception::InvalidTopicExpression)

{
	ws::notification::topics::TopicExpressionEvaluator::validateTopicPath( topicPath );
	std::list<ws::notification::topics::Topic*>           matchedTopics;
	#warning "TBD"
	/*
	TopicNamespace topicNs = getTopicNamespace( topicNsRegistry, topicPath );
	if ( ( topicPath.getLocalPart(  ).indexOf( "//" ) != -1 )
	   || ( topicPath.getLocalPart(  ).indexOf( "*" ) != -1 )
	   || ( topicPath.getLocalPart(  ).indexOf( "|" ) != -1 )
	   || ( topicPath.getLocalPart(  ).indexOf( "." ) != -1 ) )
	{
	 throw new InvalidTopicExpressionException( "Topic path '" + topicPath
                                        	    + "' contains one or more illegal characters ('//', '*', '|' or '.')." );
	}

	StringTokenizer pathTokenizer = new StringTokenizer( topicPath.getLocalPart(  ),
                                                	   "/" );
	TopicSiblingSet topicSiblingSet = topicNs;
	boolean         resolvedPath    = true;
	while ( pathTokenizer.hasMoreTokens(  ) )
	{
	 String name = pathTokenizer.nextToken(  );
	 if ( !topicSiblingSet.containsTopic( name ) )
	 {
	    resolvedPath = false;
	    break;
	 }

	 topicSiblingSet = topicSiblingSet.getTopic( name );
	}

	if ( resolvedPath )
	{
	 matchedTopics.add( topicSiblingSet );
	}

	return (Topic[]) matchedTopics.toArray( new Topic[0] );
	*/
}

		
