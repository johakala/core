// $Id: FullTopicExpression.cc,v 1.1 2006/04/07 13:07:38 xdaq Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/
#include <string> // for string
#include <new> // for bad_alloc
#include <typeinfo> // for bad_cast, bad_typeid
#include <exception> // for exception, bad_exception
#include <stdexcept> // for std exception hierarchy 


#include "ws/notification/topics/FullTopicExpression.h"


ws::notification::topics::FullTopicExpression::FullTopicExpression( std::list<std::string> & qnames ):  
	ws::notification::topics::TopicExpression( ws::notification::topics::TOPIC_EXPR_DIALECT_FULL )
{
	m_topicPaths = qnames;
}



std::list<std::string> & ws::notification::topics::FullTopicExpression::getTopicPaths(  )
{
	return m_topicPaths;
}


bool ws::notification::topics::FullTopicExpression::equals( ws::notification::topics::FullTopicExpression::TopicExpression * te )
{
	if ( te != 0 )
	{
		try 
		{
      			return dynamic_cast<ws::notification::topics::FullTopicExpression*>(te)->getTopicPaths() == m_topicPaths;
		}
		catch(const std::bad_cast & ex)
		{
			return false;
		}
	} 

	return false;
}
