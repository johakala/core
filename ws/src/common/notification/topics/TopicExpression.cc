// $Id: TopicExpression.cc,v 1.1 2006/04/07 13:07:38 xdaq Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "ws/notification/topics/TopicExpression.h"


ws::notification::topics::TopicExpression::TopicExpression( const std::string &  dialect)
{
	m_dialect    = dialect;
}


std::string ws::notification::topics::TopicExpression::getDialect()
{
	return m_dialect;
}


void  std::string ws::notification::topics::TopicExpression::setDialect( const std::string & dialect )
{
	m_dialect = dialect;
}
