// $Id: TopicsTypeFactory.cc,v 1.1 2006/04/07 13:07:38 xdaq Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "ws/notification/topics/TopicsTypeFactory.h"

static ws::notification::topics::TopicsTypeFactory::instance_;

ws::notification::topics::TopicsTypeFactory ws::notification::topics::TopicsTypeFactory::newInstance()
{
/*
	return new ws::notification::topics::TopicsTypeFactoryImpl();

*/
}


ws::notification::topics::TopicNamespace ws::notification::topics::createTopicNamespace( const std::string & targetNamespace )
{
}
