// $Id: SimpleTopicExpressionEvaluator.cc,v 1.1 2006/04/07 13:07:38 xdaq Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "ws/notification/topics/SimpleTopicExpressionEvaluator.h"

publicstd::list<std::string> ws::notification::topics::SimpleTopicExpressionEvaluator::getDialects()
{
	#warning TBD
	//return SUPPORTED_DIALECTS;
}

std::list<ws::notification::topics::Topic*> ws::notification::topics::SimpleTopicExpressionEvaluator::evaluate( ws::notification::topics::TopicNamespaceRegistry & topicNsRegistry,
                     				 ws::notification::topics::TopicExpression        &   topicExpr )
throw( ws::notification::topics::exception::TopicPathDialectUnknown, 
	ws::notification::topics::exception::TopicExpressionNoResolution, 
	ws::notification::topics::exception::InvalidTopicExpression, 
	ws::notification::topics::exception::Expression) 
{
	std::list<ws::notification::topics::Topic*> results;
	#warning "TBD"
	/*
	if ( RESULTS_CACHE.containsKey( topicExpr ) )
	{
	 results = (Topic[]) RESULTS_CACHE.get( topicExpr );
	}
	else
	{
	 SimpleTopicExpression simpleTopicExpr = (SimpleTopicExpression) topicExpr;
	 results = evaluateTopicPath( topicNsRegistry,
                        	      simpleTopicExpr.getTopicPath(  ) );
	 RESULTS_CACHE.put( topicExpr, results );
	}
	*/
	return results;
}

std::list<ws::notification::topics::Topic*> evaluateTopicPath( TopicNamespaceRegistry topicNsRegistry, const std::string & topicPath )
throw ( ws::notification::topics::exception::TopicExpressionNoResolution, ws::notification::topics::exception::InvalidTopicExpression)
{
	std::list<ws::notification::topics::Topic*> results;
	#warning "TBD"
	/*validateTopicPath( topicPath );
	TopicNamespace topicNs = getTopicNamespace( topicNsRegistry, topicPath );
	if ( ( topicPath.getLocalPart(  ).indexOf( "/" ) != -1 )
	   || ( topicPath.getLocalPart(  ).indexOf( "*" ) != -1 )
	   || ( topicPath.getLocalPart(  ).indexOf( "|" ) != -1 )
	   || ( topicPath.getLocalPart(  ).indexOf( "." ) != -1 ) )
	{
	 throw new InvalidTopicExpressionException( "Topic path '" + topicPath
                                        	    + "' contains one or more illegal characters ('/', '*', '|' or '.')." );
	}

	String name  = topicPath.getLocalPart(  );
	Topic  topic = topicNs.getTopic( name );
	if ( topic != null )
	{
	 results = new Topic[]
        	   {
        	      topic
        	   };
	}
	else
	{
	 results = new Topic[0];
	}
	*/
	return results;
}
