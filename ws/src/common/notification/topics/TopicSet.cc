// $Id: TopicSet.cc,v 1.1 2006/04/07 13:07:38 xdaq Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "ws/notification/topics/TopicSet.h"

/**
 * An implementation of a <code>TopicSet</code>.
 */

/**
* Creates a new {@link TopicSetImpl} object.
*/
ws::notification::topics::TopicSet::TopicSet(  )
{
	m_fixed = false;
}

/**
* Creates a new {@link TopicSetImpl} object.
*
* @param topicExprs DOCUMENT_ME
*
* @throws TopicExpressionException DOCUMENT_ME
*/
ws::notification::topics::TopicSet::TopicSet( std::set<TopicExpression*>& topicExprs )
	throw (ws::notification::topics::exception::Expression)
{
	std::set<TopicExpression*>::iterator i;	

	try
	{
		for (i = topicExprs.begin(); i != topicExprs.end(); ++i)
		{
			this->addTopicExpression( *i );	
		}
	}
	catch (ws::notification::topics::exception::Expression& e)
	{
		std::msg = "Failed to create topic set from other topic set";
		XCEPT_RETHROW (ws::notification::topics::exception::Expression, msg, e);
	}
}

/**
* DOCUMENT_ME
*
* @param fixed DOCUMENT_ME
*/
void ws::notification::topics::TopicSet::setFixed( bool fixed )
{
	m_fixed = fixed;
}

/**
* DOCUMENT_ME
*
* @return DOCUMENT_ME
*/
bool ws::notification::topics::TopicSet::isFixed(  )
{
	return m_fixed;
}

/**
* DOCUMENT_ME
*
* @return DOCUMENT_ME
*/
std::set<TopicExpression*>& ws::notification::topics::TopicSet::getTopicExpressions(  )
{
	return m_topicExprs;
}

/**
* DOCUMENT_ME
*
* @return DOCUMENT_ME
*/
std::set<Topic*> ws::notification::topics::TopicSet::getTopics(  )
{
	std::set<Topic*> topics;
	
	std::map<std::string, Topic*>::iterator i;
	for (i = m_topics.begin(); i != m_topics.end(); ++i)
	{
		topics.insert (*i).second );
	}
	
	return topics;
}

/**
* DOCUMENT_ME
*
* @param topicExpr DOCUMENT_ME
*
* @throws TopicExpressionException DOCUMENT_ME
*/
void ws::notification::topics::TopicSet::addTopicExpression( TopicExpression* topicExpr )
	throw (ws::notification::topics::exception::Expression)
{
	try
	{
		std::list<Topic*> topics = WsnRuntime::getRuntime(  )->getTopicExpressionEngine(  )->evaluateTopicExpression( topicExpr );
		m_topicExprs.insert( topicExpr );

		std::list<Topic*>::iterator i;

		for ( i = topics.begin(); i != topics.end(); ++i)
		{
			m_topics[(*i)->toString()] = (*i);
		}
	}
	catch (ws::notification::topics::exception::Exception& e)
	{
		XCEPT_RETHROW (ws::notification::topics::exception::Expression, "Failed to add topic expression", e);
	}
}


/**
* DOCUMENT_ME
*
* @param topicPath DOCUMENT_ME
*
* @return DOCUMENT_ME
*/
bool ws::notification::topics::TopicSet::containsTopic( const std::string& topicPath )
{	
	return m_topics.count( topicPath );
}

/**
* DOCUMENT_ME
*
* @param topicExpr DOCUMENT_ME
*
* @return DOCUMENT_ME
*
* @throws TopicExpressionException DOCUMENT_ME
*/
std::set<Topic*> ws::notification::topics::TopicSet::evaluateTopicExpression( TopicExpression* topicExpr )
	throw (ws::notification::topic::exception::Expression)
{
	try
	{
		std::list<Topic*> topics = WsnRuntime::getRuntime(  )->getTopicExpressionEngine(  )->evaluateTopicExpression( topicExpr );
		return this->getSupportedTopics( topics );
	}
	catch (ws::notification::topics::exception::Exception& e)
	{
		XCEPT_RETHROW (ws::notification::topics::exception::Expression, "Failed to evaluate topic expression", e);
	}
}

/**
* DOCUMENT_ME
*
* @return DOCUMENT_ME
*/
std::string ws::notification::topics::TopicSet::toString(  )
{
	std::map<std::string, Topic*>::iterator i;
	std::string val = "";
	for (i = m_topics.begin(); i != m_topics.end(); ++i)
	{
		if (val != "")
		{
			value += ",";
		}
		val += (*i).first;
	}
	return val;
}

std::set<Topic*> ws::notification::topics::TopicSet::getSupportedTopics( std::list<Topic*>& topics )
{
	std::set<Topic*> supportedTopics;
	std::list<Topic*>::iterator i;
	
	for ( i = topics.begin(); i != topics.end(); ++i)
	{
		Topic* topic = *i;
		if ( this->containsTopic( topic->toString() )
		{
			supportedTopics.insert( topic );
		}
	}

	return supportedTopics;
}

