// $Id: TopicNamespaceRegistry.cc,v 1.1 2006/04/07 13:07:38 xdaq Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/
 

#include "ws/notification/topics/TopicNamespaceRegistry.h"

ws::notification::topics::TopicNamespaceRegitry::TopicNamespaceRegistry()
{
}


ws::notification::topics::TopicNamespaceRegitry::TopicNamespaceRegistry( std::set<std::string>& topicNsSet )
{
	this->initTopicNamespaceMap( topicNsSet );
}

ws::notification::topics::TopicNamespace* ws::notification::topics::TopicNamespaceRegitry::getTopicNamespace( const std::string& targetNamespace ) throw (ws::notification::topics::exception::NamespaceNotFound)
{
	std::map<std::string, TopicNamespace*>::iterator i = m_topicNsMap.find(targetNamespace);
	if (i != m_topicNsMap.end())
	{
		return (*i).second;
	}
	else
	{
		std::string msg = "Topic namespace ";
		msg += targetNamespace;
		msg += " not found";
		XCEPT_RAISE (ws::notification::topics::exception::NamespaceNotFound, msg);
	}
}


void ws::notification::topics::TopicNamespaceRegitry::addTopicNamespace( TopicNamespace* topicNs ) 
	throw (ws::notification::topics::exception::NamespaceExisting)
{
	std::map<std::string, TopicNamespace*>::iterator i = m_topicNsMap.find(targetNamespace);
	if (i != m_topicNsMap.end())
	{
		std::string msg = "Topic namespace ";
		msg += targetNamespace;
		msg += " already existing";
		XCEPT_RAISE (ws::notification::topics::exception::NamespaceExisting, msg);
	}

	if (topicNS->getTargetNamespace(  ) != "")
	{
		m_topicNsMap[topicNS->getTargetNamespace()] = topicNs;
	}
	else
	{
		m_topicNsMap[""] = topicNs;
	}	
}


void ws::notification::topics::TopicNamespaceRegitry::initTopicNamespaceMap( std::set<std::string>& topicNsSet )
{
	#warning "TBD"
	/*
	for ( Iterator iterator = topicNsSet.iterator(  ); iterator.hasNext(  ); )
	{
	 String   location = (String) iterator.next(  );
	 Resource resource;
	 try
	 {
	    resource = new DefaultResourceLoader(  ).getResource( location );
	 }
	 catch ( RuntimeException re )
	 {
	    System.err.println( "ERROR: " + location
                        	+ " is not a valid URL - an associated topic namespace will not be added to the registry." );
	    continue;
	 }

	 TopicNamespace topicNs = null;
	 try
	 {
	    topicNs =
	       TopicsTypeReader.newInstance( TopicsConstants.NSURI_WSTOP_SCHEMA ).toTopicNamespace( XmlObject.Factory
                                                                                        	    .parse( resource
                                                                                                	    .getInputStream(  ) ) );
	 }
	 catch ( Exception e )
	 {
	    System.err.println( "ERROR: Unable to load topic namespace document from URL " + location
                        	+ " - an associated topic namespace will not be added to the registry." );
	    e.printStackTrace(  );
	    continue;
	 }

	 m_topicNsMap.put( topicNs.getTargetNamespace(  ),
                	   topicNs );
	}
	*/
	
}
