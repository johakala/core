// $Id: SimpleTopicExpression.cc,v 1.2 2006/04/07 13:11:35 xdaq Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/


#include "ws/notification/topics/SimpleTopicExpression.h"


ws::notification::topics::SimpleTopicExpression::SimpleTopicExpression( const std::string & qname ):TopicExpression( ws::notification::topics::TOPIC_EXPR_DIALECT_SIMPLE )
{

	m_topicPath = qname;
}



std::string ws::notification::topics::SimpleTopicExpression::getTopicPath(  )
{
	return m_topicPath;
}


bool ws::notification::topics::SimpleTopicExpression::equals( TopicExpression * te )
{
	if ( te != 0 )
	{
		try 
		{
      			return dynamic_cast<SimpleTopicExpression*>(te)->getTopicPath() == m_topicPath;
		}
		catch(const std::bad_cast & ex)
		{
			return false;
		}
	} 

	return false;
}


  
