// $Id: TopicExpressionEvaluator.cc,v 1.1 2006/04/07 13:07:38 xdaq Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "ws/notification/topics/TopicExpressionEvaluator.h"


TopicNamespace * ws::notification::topics::TopicExpressionEvaluator::getTopicNamespace( TopicNamespaceRegistry & topicNsRegistry, std::string topicPath )
   	throw ( ws::notification::topics::exception::TopicExpressionNoResolution )
{
	TopicNamespace * topicNs = topicNsRegistry.getTopicNamespace( topicPath.getNamespaceURI(  ) );
	
	if ( topicNs == null )
	{
		std::string msg = "Topic path '" ;
		msg += topicPath;
		msg += "' references an unknown TopicNamespace with target namespace '";
		msg += topicPath.getNamespaceURI( ) ;
		msg += "'.";

		XCEPT_RAISE ( ws::notification::topics::exception::TopicExpressionNoResolution, msg);

	}

	return topicNs;
}


void ws::notification::topics::TopicExpressionEvaluator::validateTopicPath( std::string topicPath )
	throw ( ws::notification::topics::exception::InvalidTopicExpression )
{
	std::string localPart = topicPath.getLocalPart( );
	
	if ( StringUtils.isEmpty( localPart ) || !StringUtils.containsNone( localPart, " \t\n\r\f" ) )
	{
		 std::string msg = "Topic path '" 
		 msg += topicPath;
		 msg += "' contains whitespace.";

		 XCEPT_RAISE ( ws::notification::topics::exception::TopicExpressionNoResolution, msg);
	}

	if ( localPart.indexOf( ':' ) != -1 )
	{
		 std::string msg =  "Topic path '";
		 msg += topicPath;
		 msg += "' contains a colon.";

		 XCEPT_RAISE ( ws::notification::topics::exception::TopicExpressionNoResolution, msg);
	}
}				       

