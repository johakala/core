// $Id: TopicExpressionEngine.cc,v 1.2 2006/04/07 13:11:35 xdaq Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "ws/notification/topics/TopicExpressionEngine.h"


ws::notification::topics::TopicExpressionEngine::TopicExpressionEngine( TopicNamespaceRegistry * topicNsRegistry )
{
       m_topicNsRegistry    = topicNsRegistry;
       this->registerEvaluator( new SimpleTopicExpressionEvaluator(  ) );
       this->registerEvaluator( new ConcreteTopicExpressionEvaluator(  ) );
       this->registerEvaluator( new FullTopicExpressionEvaluator(  ) );
}

ws::notification::topics::TopicExpressionEngine::TopicExpressionEngine( ws::notification::topics::TopicExpressionEngine::TopicNamespaceRegistry topicNsRegistry,
                                 std::map<std::string, ws::notifiction::topics::TopicExpressionEvaluator * > &   topicExprEvaluatorMap )
{
	m_topicNsRegistry    = topicNsRegistry;
	m_evaluators         = topicExprEvaluatorMap;
	if ( m_evaluators.empty(  ) )
	{
		 // defaults...
		 this->registerEvaluator( new SimpleTopicExpressionEvaluator(  ) );
		 this->registerEvaluator( new ConcreteTopicExpressionEvaluator(  ) );
		 this->registerEvaluator( new FullTopicExpressionEvaluator(  ) );
	}
}

ws::notification::topics::TopicExpressionEvaluator * ws::notification::topics::TopicExpressionEngine::getEvaluator( const std::string & dialect ) 
	throw (ws::notification::topics::exception::NoExpressionEvaluatorForDialect)
{
	std::map<std::string, ws::notifiction::topics::TopicExpressionEvaluator * >::iterator i = m_evaluators.find(dialect);
	if ( i != m_evaluators.end() )
	{
		return m_evaluators[dialect];
	}
	std::msg =  "cannot find topic expression evaluator for dialect ";
	msg += dialect;

	XCEPT_RAISE(ws::notification::topics::exception::NoExpressionEvaluatorForDialect, msg);
}

std::list<std::string>  ws::notification::topics::TopicExpressionEngine::getSupportedDialects(  )
{
	std::list <std::string> dialects;
	std::map<std::string, ws::notifiction::topics::TopicExpressionEvaluator * >::iterator i;
	for ( i = m_evaluators.begin(); i != m_evaluators.end(); i++ )
	{
		dialects.push_back((*i).first)
	}
	return dialects;
}

std::list<Topic *> ws::notification::topics::TopicExpressionEngine::evaluateTopicExpression( TopicExpression & topicExpr )
   throw (ws::notification::topics::exception::TopicPathDialectUnknown, 
          ws::notification::topics::exception::TopicExpressionNoResolution, 
          ws::notification::topics::exception::InvalidTopicExpression, 
          ws::notification::topics::exception::Expression)
{
   #warning "evaluateTopicExpression TBD"
   /*
      if ( topicExpr == null )
      {
         throw new IllegalArgumentException( "Specified TopicExpression was null." );
      }

      if ( topicExpr.getDialect(  ) == null )
      {
         throw new org.apache.ws.notification.topics.expression.TopicPathDialectUnknownException( "nullArgument"
                                                                                                  + "topicExpression.dialect" );
      }

      String                   dialect   = topicExpr.getDialect(  );
      TopicExpressionEvaluator evaluator = getEvaluator( dialect );

      if ( evaluator == null )
      {
         if ( LOG.isDebugEnabled(  ) )
         {
            LOG.debug( "Dialect not supported:" + dialect );
            LOG.debug( "Registered dialects are:" );
            Iterator keyIterator = m_evaluators.keySet(  ).iterator(  );
            Object   key = null;
            while ( keyIterator.hasNext(  ) )
            {
               key = keyIterator.next(  );
               LOG.debug( key );
            }

            LOG.debug( "key.equals(dialect): " + ( key.equals( dialect.toString(  ) ) ) );
            LOG.debug( "m_evaluators.containsKey(dialect): " + m_evaluators.containsKey( dialect ) );
            LOG.debug( "HashCode of key: " + String.valueOf( key.hashCode(  ) ) );
            LOG.debug( "HashCode of dialect: " + String.valueOf( dialect.toString(  ).hashCode(  ) ) );
            LOG.debug( "Object stored for key: " + m_evaluators.get( key ) );
         }

         throw new TopicPathDialectUnknownException( "The dialect '" + dialect + "' was not recognized." );
      }

      return evaluator.evaluate( m_topicNsRegistry, topicExpr );
     */
      
}


void ws::notification::topics::TopicExpressionEngine::registerEvaluator( TopicExpressionEvaluator evaluator )
{
	std::list<std::string> dialects = evaluator.getDialects(  );
	std::list<std::string>::iterator i;
	for (  i = dialects.begin() ; i != dialects.end(); i++ )
	{
		#warning "If the evaluator alread y existing for this dialect"
		 m_evaluators[(*i)] = evaluator;
	}
}
   
