// $Id: EndpointReference.cc,v 1.12 2008/07/18 15:27:44 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "ws/addressing/EndpointReference.h"
#include "ws/addressing/WSAddressing.h"
#include "xoap/SOAPName.h"
#include "xoap/SOAPElement.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/domutils.h"
#include "toolbox/net/URL.h"
#include "toolbox/net/UUID.h"
#include <vector>
#include "xoap/Lock.h"


ws::addressing::EndpointReference::EndpointReference ()
{
	serviceName_ = "";
	portType_ = "";
	portName_ = "";
	address_ = "";

	parameters_ = 0;
	properties_ = 0;
}

ws::addressing::EndpointReference::EndpointReference (xoap::SOAPElement& element)
	throw (ws::addressing::exception::Exception)
{
	
	//TDB: currently only the old namespace (NamespaceUri[0]) is parsed
	parameters_ = 0;
        properties_ = 0;
	// Address
	//
	xoap::SOAPName addressName("Address", ws::addressing::NamespacePrefix,  ws::addressing::NamespaceUri[0]);
	std::vector<xoap::SOAPElement> address = element.getChildElements(addressName);
	XCEPT_ASSERT(address.size() == 1, ws::addressing::exception::Exception, "Invalid message (<Address> tag not exactly one)");

	address_ = address[0].getValue();	

	// Service Name and port name
	//
	xoap::SOAPName serviceName("ServiceName", ws::addressing::NamespacePrefix,  ws::addressing::NamespaceUri[0]);
	std::vector<xoap::SOAPElement> service = element.getChildElements(serviceName);
	XCEPT_ASSERT(service.size() <= 1, ws::addressing::exception::Exception, "Invalid message (<ServiceName> tag not exactly one)");

	if (service.size() == 1)
	{
		serviceName_ = service[0].getValue();

		xoap::SOAPName portNameAttributeName("PortName", "", "");
		portName_ = service[0].getAttributeValue(portNameAttributeName);
	}

	// PortType
	//
	xoap::SOAPName portTypeName("PortType", ws::addressing::NamespacePrefix,  ws::addressing::NamespaceUri[0]);
	std::vector<xoap::SOAPElement> portType = element.getChildElements(portTypeName);
	XCEPT_ASSERT(portType.size() <= 1, ws::addressing::exception::Exception,"Invalid message (<PortType> tag not exactly one)");

	if (portType.size() == 1)
	{
		portType_ = portType[0].getValue();
	}

	xoap::lock();

	// Parameters and Properties
	//
	DOMImplementation* impl = DOMImplementationRegistry::getDOMImplementation (xoap::XStr(xoap::DOMImplementationFeatures));

	xoap::SOAPName referenceParamatersName("ReferenceParameters", ws::addressing::NamespacePrefix,  ws::addressing::NamespaceUri[0]);
	std::vector<xoap::SOAPElement> referenceParamatersElements = element.getChildElements(referenceParamatersName);			
	if ( referenceParamatersElements.size() > 1 )
	{
		xoap::unlock();
		XCEPT_RAISE( ws::addressing::exception::Exception,"Invalid message (<ReferenceParameters> > 1)");
	}
	if ( referenceParamatersElements.size() == 1)
	{	
		parameters_ =  impl->createDocument (
			xoap::XStr ( ws::addressing::NamespaceUri[0] ),
			xoap::XStr(ws::addressing::NamespacePrefix+":ReferenceParameters"),
			0);
			
		// Add namespace def in root node
		parameters_->getDocumentElement()->setAttribute 
			(
				xoap::XStr("xmlns:"+ws::addressing::NamespacePrefix),
				xoap::XStr ( ws::addressing::NamespaceUri[0] )
			);
		
		std::vector<xoap::SOAPElement> children  = referenceParamatersElements[0].getChildElements();
		for (std::vector<xoap::SOAPElement>::iterator i = children.begin(); i != children.end(); i++)	
		{
			DOMNode * child = parameters_->importNode( (*i).getDOM(), true );
			parameters_->getDocumentElement()->appendChild( child );
		}	
	}
	

	xoap::SOAPName referencePropertiesName("ReferenceProperties",  ws::addressing::NamespacePrefix,  ws::addressing::NamespaceUri[0]);
	std::vector<xoap::SOAPElement> referencePropertiesElements = element.getChildElements(referencePropertiesName);
	
	if ( referencePropertiesElements.size() > 1 )
	{
		xoap::unlock();
		XCEPT_RAISE( ws::addressing::exception::Exception,"Invalid message (<referenceProperties> > 1)");
	}
	
	if (referencePropertiesElements.size() == 1)
	{
			
		properties_ = impl->createDocument (
			xoap::XStr ( ws::addressing::NamespaceUri[0] ),
			xoap::XStr(ws::addressing::NamespacePrefix+":ReferenceProperties"),
			0);
			
		// Add namespace def in root node
		properties_->getDocumentElement()->setAttribute 
			(
				xoap::XStr("xmlns:"+ws::addressing::NamespacePrefix),
				xoap::XStr ( ws::addressing::NamespaceUri[0] )
			);
			
		std::vector<xoap::SOAPElement> children  = referencePropertiesElements[0].getChildElements();
		for (std::vector<xoap::SOAPElement>::iterator i = children.begin(); i != children.end(); i++)	
		{
			DOMNode * child = properties_->importNode( (*i).getDOM(), true );
			properties_->getDocumentElement()->appendChild( child );
		}			
	}	
	
		xoap::unlock();

}

ws::addressing::EndpointReference::EndpointReference (const ws::addressing::EndpointReference & epr)
{	
	xoap::lock();

	parameters_ = 0;
	properties_ = 0;
	serviceName_ =   epr.serviceName_;
	portType_    =   epr.portType_;
	portName_    =   epr.portName_;
	address_     =   epr.address_;
	uuid_        =   epr.uuid_;
	
	if (epr.parameters_ != 0)
	{
		parameters_  =   (DOMDocument*) epr.parameters_->cloneNode(true);
	}
	
	if (epr.properties_ != 0)
	{
		properties_  =   (DOMDocument*) epr.properties_->cloneNode(true);
	}
	xoap::unlock();
}

ws::addressing::EndpointReference&
ws::addressing::EndpointReference::operator= (const ws::addressing::EndpointReference & epr)
{	
	xoap::lock();

	serviceName_ =   epr.serviceName_;
	portType_    =   epr.portType_;
	portName_    =   epr.portName_;
	address_     =   epr.address_;
	uuid_        =   epr.uuid_;
	
	if (parameters_ != 0)
	{
		parameters_->release();
	}
	
	if (epr.parameters_ != 0)
	{
		parameters_  =   (DOMDocument*) epr.parameters_->cloneNode(true);
	}
	else
	{
		parameters_ = 0;
	}
	
	if (properties_ != 0)
	{
		properties_->release();
	}
	
	if (epr.properties_ != 0)
	{
		properties_  =   (DOMDocument*) epr.properties_->cloneNode(true);
	}
	else
	{
		properties_ = 0;
	}
	xoap::unlock();
	return *this;
}

ws::addressing::EndpointReference::EndpointReference (const toolbox::net::URL& url)
	throw (ws::addressing::exception::Exception)
{
	address_ = url.toString();
	parameters_ = 0;
	properties_ = 0;
	serviceName_ = "";
	portType_ = "";
	portName_ = "";
}

/*
ws::addressing::EndpointReference::EndpointReference (toolbox::net::URL& url, toolbox::net::UUID& id)
	throw (ws::addressing::exception::Exception)
{
	address_ = url.toString();
	parameters_ = 0;
	properties_ = 0;
	
	try 
	{
		// Parameters
		//
		DOMImplementation* impl = DOMImplementationRegistry::getDOMImplementation (xoap::XStr("Core"));		
		parameters_ = impl->createDocument (
			xoap::XStr ( ws::addressing::NamespaceUri[0] ),
			xoap::XStr(ws::addressing::NamespacePrefix+":ReferenceParameters"),
			0);
			
		// Add namespace def in root node
		parameters_->getDocumentElement()->setAttribute 
			(
				xoap::XStr("xmlns:"+ws::addressing::NamespacePrefix),
				xoap::XStr ( ws::addressing::NamespaceUri[0] )
			);

		DOMElement* identifier = parameters_->createElementNS(xoap::XStr(ws::addressing::NamespaceUri[0]), xoap::XStr(ws::addressing::NamespacePrefix+":Identifier"));

		std::string idString = "uuid:";
		idString += id.toString();
		identifier->setTextContent ( xoap::XStr ( idString ) );

		parameters_->getDocumentElement()->appendChild ( identifier );
	}
	catch (DOMException & e)
	{
		XCEPT_RAISE(ws::addressing::exception::Exception, xoap::XMLCh2String(e.msg) );
	}
	
	serviceName_ = "";
	portType_ = "";
	portName_ = "";
}
*/		
ws::addressing::EndpointReference::~EndpointReference()
{
	xoap::lock();
	// Destruct DOM document
	//
	if (parameters_ != 0) parameters_->release();
	
	if (properties_ != 0) properties_->release();
	xoap::unlock();	
}
	
std::string ws::addressing::EndpointReference::getAddress(  )
{
	return address_;
}

std::string ws::addressing::EndpointReference::getPortName(  )
{
	//  <wsa:ServiceName PortName="xs:NCName"?>xs:QName</wsa:ServiceName>
	//
	return portName_;
}

std::string ws::addressing::EndpointReference::getPortType(  )
{
	// <wsa:PortType>fabrikam:InventoryPortType</wsa:PortType>
	return portType_;
}

DOMElement* ws::addressing::EndpointReference::getReferenceParameters(  )
	throw (ws::addressing::exception::Exception)
{
	xoap::lock();
	if (parameters_ == 0)
	{
		try 
		{
			// Parameters
			//
			DOMImplementation* impl = DOMImplementationRegistry::getDOMImplementation (xoap::XStr(xoap::DOMImplementationFeatures));		
			parameters_ = impl->createDocument (
				xoap::XStr ( ws::addressing::NamespaceUri[0] ),
				xoap::XStr(ws::addressing::NamespacePrefix+":ReferenceParameters"),
				0);

			// Add namespace def in root node
			parameters_->getDocumentElement()->setAttribute 
				(
					xoap::XStr("xmlns:"+ws::addressing::NamespacePrefix),
					xoap::XStr ( ws::addressing::NamespaceUri[0] )
				);

		}
		catch (DOMException & e)
		{
			xoap::unlock();
			XCEPT_RAISE(ws::addressing::exception::Exception, xoap::XMLCh2String(e.msg) );
		}
	}
	xoap::unlock();
	return parameters_->getDocumentElement();
}

bool ws::addressing::EndpointReference::hasReferenceParameters(  )
{
	return (parameters_ != 0);
}

DOMElement* ws::addressing::EndpointReference::getReferenceProperties(  )
	throw (ws::addressing::exception::Exception)
{
	xoap::lock();
	if (properties_ == 0)
	{
		try 
		{
			// Parameters
			//
			DOMImplementation* impl = DOMImplementationRegistry::getDOMImplementation (xoap::XStr(xoap::DOMImplementationFeatures));		
			parameters_ = impl->createDocument (
				xoap::XStr ( ws::addressing::NamespaceUri[0] ),
				xoap::XStr(ws::addressing::NamespacePrefix+":ReferenceProperties"),
				0);

			// Add namespace def in root node
			parameters_->getDocumentElement()->setAttribute 
				(
					xoap::XStr("xmlns:"+ws::addressing::NamespacePrefix),
					xoap::XStr ( ws::addressing::NamespaceUri[0] )
				);

		}
		catch (DOMException & e)
		{
			xoap::unlock();
			XCEPT_RAISE(ws::addressing::exception::Exception, xoap::XMLCh2String(e.msg) );
		}
	}
	xoap::unlock();
	return properties_->getDocumentElement();
}

bool ws::addressing::EndpointReference::hasReferenceProperties(  )
{
	return (properties_ != 0);
}

std::string ws::addressing::EndpointReference::getServiceName(  )
{
	//  <wsa:ServiceName PortName="xs:NCName"?>xs:QName</wsa:ServiceName> ?
	return serviceName_;
}

void ws::addressing::EndpointReference::toSOAP(xoap::SOAPElement& element, xoap::SOAPEnvelope& envelope)
{
	// <a:Address>
	xoap::SOAPName addressTag = envelope.createName
		("Address",ws::addressing::NamespacePrefix, ws::addressing::NamespaceUri[0]);				
	xoap::SOAPElement addressElement = element.addChildElement(addressTag);
	addressElement.addTextNode (address_);
	// </a:Address>
	
	// <a:ReferenceProperties>
	if (properties_ != 0)
		element.addChildElement(properties_->getDocumentElement());
	// </a:ReferenceProperties>
	
	// <a:ReferenceParameters>
	if (parameters_ != 0) 
		element.addChildElement(parameters_->getDocumentElement());
	
	// </a:ReferenceParameters>
	
	
	// <a:PortType>
	if (portType_ != "")
	{
		xoap::SOAPName portTypeTag = envelope.createName
			("PortType",ws::addressing::NamespacePrefix, ws::addressing::NamespaceUri[0]);				
		xoap::SOAPElement portTypeElement = element.addChildElement(portTypeTag);
		portTypeElement.addTextNode (portType_);	
	}
	// </a:PortType
	
	// <a:ServiceName PortName="">
	if (serviceName_ != "")
	{
		xoap::SOAPName serviceNameTag = envelope.createName
			("ServiceName",ws::addressing::NamespacePrefix, ws::addressing::NamespaceUri[0]);				
		xoap::SOAPElement serviceNameElement = element.addChildElement(serviceNameTag);
		serviceNameElement.addTextNode (serviceName_);
		
		if (portName_ != "")
		{
			xoap::SOAPName portNameAttribute = envelope.createName("PortName","", "");				
			serviceNameElement.addAttribute (portNameAttribute, portName_);
		}	
	}
	// </a:ServiceName PortName="">
}

toolbox::net::UUID ws::addressing::EndpointReference::getUUID()
{
	return uuid_;
}



