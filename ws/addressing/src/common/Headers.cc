// $Id: Headers.cc,v 1.3 2008/07/18 15:27:44 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include <vector>
#include "toolbox/string.h"
#include "ws/addressing/Headers.h"
#include "ws/addressing/WSAddressing.h"
#include "xoap/SOAPPart.h"
#include "xoap/SOAPHeader.h"
#include "xoap/SOAPEnvelope.h"

ws::addressing::Headers::Headers()
{
	action_ = "";
	faultTo_ = "";
	from_ = "";
	messageId_ = "";
	relatesTo_ = "";
	replyAfter_ = "";
	replyTo_ = "";
	to_ = "";
}

bool ws::addressing::Headers::hasAction()
{
	return (!action_.empty());
}

std::string ws::addressing::Headers::getAction()
{
	return action_;
}

void ws::addressing::Headers::setAction ( const std::string & value )
{
	action_ = value;
}

bool ws::addressing::Headers::hasFaultTo()
{
	return (!faultTo_.empty());
}

std::string ws::addressing::Headers::getFaultTo()
{
	return faultTo_;
}

void ws::addressing::Headers::setFaultTo ( const std::string & value )
{
	faultTo_ = value;
}

bool ws::addressing::Headers::hasFrom()
{
	return (!from_.empty());
}

std::string ws::addressing::Headers::getFrom()
{
	return from_;
}

void ws::addressing::Headers::setFrom ( const std::string & value )
{
	from_ = value;
}

bool ws::addressing::Headers::hasMessageId()
{
	return (!messageId_.empty());
}

std::string ws::addressing::Headers::getMessageId()
{
	return messageId_;
}

void ws::addressing::Headers::setMessageId(const std::string & id)
{
	messageId_ = id;
}

bool ws::addressing::Headers::hasRelatesTo()
{
	return (!relatesTo_.empty());
}

std::string ws::addressing::Headers::getRelatesTo()
{
	return relatesTo_;
}

void ws::addressing::Headers::setRelatesTo ( const std::string & value )
{
	relatesTo_ = value;
}

bool ws::addressing::Headers::hasReplyAfter()
{
	return (!replyAfter_.empty());
}

std::string ws::addressing::Headers::getReplyAfter()
{
	return replyAfter_;
}

void ws::addressing::Headers::setReplyAfter ( const std::string & value )
{
	replyAfter_ = value;
}

bool ws::addressing::Headers::hasReplyTo()
{
	return (!replyTo_.empty());
}

std::string ws::addressing::Headers::getReplyTo()
{
	return replyTo_;
}

void ws::addressing::Headers::setReplyTo ( const std::string & value )
{
	replyTo_ = value;
}

bool ws::addressing::Headers::hasTo()
{
	return (!to_.empty());
}

std::string ws::addressing::Headers::getTo()
{
	return to_;
}

void ws::addressing::Headers::setTo ( const std::string & value )
{
	to_ = value;
}

void ws::addressing::Headers::fromSOAP(xoap::MessageReference& msg) throw (ws::addressing::exception::Exception)
{
	xoap::SOAPPart soap = msg->getSOAPPart();
	xoap::SOAPEnvelope envelope = soap.getEnvelope();
	xoap::SOAPHeader header = envelope.getHeader();
	std::vector<xoap::SOAPHeaderElement> elements = header.examineHeaderElements ();

	// Extract header fields to determine the subscription
	std::vector<xoap::SOAPHeaderElement>::iterator i;
	for (i = elements.begin(); i != elements.end(); ++i)
	{
		xoap::SOAPName soapName = (*i).getElementName();
		std::string name = soapName.getLocalName();
		std::string uri = soapName.getURI();
		std::string value = toolbox::trim((*i).getValue());

		for ( size_t j=0 ; j < ws::addressing::NamespaceVersions; j++)
		{
			if( uri == ws::addressing::NamespaceUri[j] )
			{
				if (name == "Action")
				{
					this->setAction(value);
				}
				else if (name == "FaultTo")
				{
					this->setFaultTo(value);
				}
				else if (name == "From")
				{
					this->setFrom(value);
				}
				else if (name == "MessageID")
				{
					this->setMessageId(value);
				}
				else if (name == "RelatesTo")
				{
					this->setRelatesTo(value);
				}
				else if (name == "ReplyTo")
				{
					this->setReplyTo(value);
				}
				else if (name == "To")
				{
					this->setTo(value);
				}
			}
		}
	}
}

void ws::addressing::Headers::toSOAP(xoap::MessageReference& msg) throw (ws::addressing::exception::Exception)
{
	xoap::SOAPPart soap = msg->getSOAPPart();
	xoap::SOAPEnvelope envelope = soap.getEnvelope();
	// xoap::SOAPBody body = envelope.getBody();
	
	if (envelope.hasHeader())
	{
		xoap::SOAPHeader header = envelope.getHeader();
		std::vector<xoap::SOAPHeaderElement> elements = header.examineHeaderElements ();

		std::vector<xoap::SOAPHeaderElement>::iterator i;
		for (i = elements.begin(); i != elements.end(); ++i)
		{
			xoap::SOAPName soapName = (*i).getElementName();

			std::string name = soapName.getLocalName();
			std::string uri = soapName.getURI();

			for ( size_t j=0 ; j < ws::addressing::NamespaceVersions; j++)
			{
				if( uri == ws::addressing::NamespaceUri[j] )
				{
					if (name == "Action")
					{
						(*i).setTextContent(this->getAction());
					}
					else if (name == "FaultTo")
					{
						(*i).setTextContent(this->getFaultTo());
					}
					else if (name == "From")
					{
						(*i).setTextContent(this->getFrom());
					}
					else if (name == "MessageID")
					{
						(*i).setTextContent(this->getMessageId());
					}
					else if (name == "RelatesTo")
					{
						(*i).setTextContent(this->getRelatesTo());
					}
					else if (name == "ReplyTo")
					{
						(*i).setTextContent(this->getReplyTo());
					}
					else if (name == "To")
					{
						(*i).setTextContent(this->getTo());
					}
				}
			}
		}		
	}
	else	
	{
		xoap::SOAPHeader header = envelope.addHeader();
	
		if (action_ != "")
		{
			xoap::SOAPName name = envelope.createName
			("Action", 
			ws::addressing::NamespacePrefix,
			ws::addressing::NamespaceUri[0]);
			xoap::SOAPHeaderElement element = header.addHeaderElement (name);
			element.addTextNode(action_);
		}

		if (faultTo_ != "")
		{
			xoap::SOAPName name = envelope.createName
			("FaultTo", 
			ws::addressing::NamespacePrefix,
			ws::addressing::NamespaceUri[0]);
			xoap::SOAPHeaderElement element = header.addHeaderElement (name);
			element.addTextNode(faultTo_);
		}

		if (from_ != "")
		{
			xoap::SOAPName name = envelope.createName
			("From", 
			ws::addressing::NamespacePrefix,
			ws::addressing::NamespaceUri[0]);
			xoap::SOAPHeaderElement element = header.addHeaderElement (name);
			element.addTextNode(from_);
		}

		if (messageId_ != "")
		{
			xoap::SOAPName name = envelope.createName
			("MessageID", 
			ws::addressing::NamespacePrefix,
			ws::addressing::NamespaceUri[0]);
			xoap::SOAPHeaderElement element = header.addHeaderElement (name);
			element.addTextNode(messageId_);
		}

		if (relatesTo_ != "")
		{
			xoap::SOAPName name = envelope.createName
			("RelatesTo", 
			ws::addressing::NamespacePrefix,
			ws::addressing::NamespaceUri[0]);
			xoap::SOAPHeaderElement element = header.addHeaderElement (name);
			element.addTextNode(relatesTo_);
		}

		if (replyAfter_ != "")
		{
			xoap::SOAPName name = envelope.createName
			("ReplyAfter", 
			ws::addressing::NamespacePrefix,
			ws::addressing::NamespaceUri[0]);
			xoap::SOAPHeaderElement element = header.addHeaderElement (name);
			element.addTextNode(replyAfter_);
		}

		if (replyTo_ != "")
		{
			xoap::SOAPName name = envelope.createName
			("ReplyTo", 
			ws::addressing::NamespacePrefix,
			ws::addressing::NamespaceUri[0]);
			xoap::SOAPHeaderElement element = header.addHeaderElement (name);
			element.addTextNode(replyTo_);
		}

		if (to_ != "")
		{
			xoap::SOAPName name = envelope.createName
			("To", 
			ws::addressing::NamespacePrefix,
			ws::addressing::NamespaceUri[0]);
			xoap::SOAPHeaderElement element = header.addHeaderElement (name);
			element.addTextNode(to_);
		}
	}
}
      
