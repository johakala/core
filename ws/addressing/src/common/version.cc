// $Id: version.cc,v 1.3 2008/07/18 15:27:44 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "ws/addressing/version.h"
#include "config/version.h"
#include "xcept/version.h"
#include "xoap/version.h"
#include "toolbox/version.h"

GETPACKAGEINFO(wsaddressing)

void wsaddressing::checkPackageDependencies() throw (config::PackageInfo::VersionException)
{
	CHECKDEPENDENCY(config);  
	CHECKDEPENDENCY(xcept);  
	CHECKDEPENDENCY(xoap);  
	CHECKDEPENDENCY(toolbox); 
}

std::set<std::string, std::less<std::string> > wsaddressing::getPackageDependencies()
{
	std::set<std::string, std::less<std::string> > dependencies;

	ADDDEPENDENCY(dependencies,config); 
	ADDDEPENDENCY(dependencies,xcept);
	ADDDEPENDENCY(dependencies,xoap);
	ADDDEPENDENCY(dependencies,toolbox);
  
	return dependencies;
}	
	
