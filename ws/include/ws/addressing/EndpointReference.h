// $Id: EndpointReference.h,v 1.1 2006/04/07 13:07:35 xdaq Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _ws_adressing_EndpointReference_h_
#define _ws_adressing_EndpointReference_h_

#include <string>
#include <xercesc/util/QName.hpp>


XERCES_CPP_NAMESPACE_USE

namespace ws {
namespace addressing {

/*
 * Interface for EndpointReferences
 *
 */
class EndpointReference
{
	public:
	
	/*
	* Constructs an EPR (and the underlying XmlObject representation) given the params.
	*
	* @param address       - Endpoint Address.  Must Not Be Null
	* @param addressingURI - WS-Addressing URI - Must Not Be Null
	*/
	EndpointReference( const std::string& address, const std::string& addressingURI );

	~EndpointReference();

	/*
	* Returns the Address from the EPR as a const std::string&.
	*
	* @return Address
	*/
	const std::string& getAddress(  );

	/*
	* Returns the PortName associated with the Service in the EPR as a const std::string&
	*
	* @return Service's Port Name
	*/
	const std::string& getPortName(  );

	/*
	* Returns the PortType QName
	*
	* @return PortType QName
	*/
	QName getPortType(  );

	/*
	* 
	*
	* @param portTypeQName DOCUMENT_ME
	*/
	void setPortTypeQName( QName portTypeQName );

	/*
	* Returns the ReferenceParameters Object.
	*
	* @return Object[] The ReferenceParameters
	*/
	std::list<std::string>& getReferenceParameters(  );

	/*
	* Returns the ReferenceProperties Object.
	*
	* @return Object[] The ReferenceProperties
	*/
	std::list<std::string>& getReferenceProperties(  );

	/*
	* Service QName
	*
	* @return Service QName
	*/
	QName getServiceName(  );

	/*
	* 
	*
	* @param servicePortName 
	*/
	void setServicePortName( const std::string& servicePortName );

	/*
	* 
	*
	* @param serviceQName 
	*/
	void setServiceQName( QName serviceQName );

	/*
	*
	* @param refProp
	*/
	// void addReferenceParameter( Object refProp );

	/*
	*
	* @param refProp
	*/
	// void addReferenceProperty( Object refProp );

	protected:

	// XmlObject m_xmlObjectEPR;
	std::string    m_address;
	QName     m_portTypeQName;
	std::string    m_servicePortName;
	QName     m_serviceQName;
	std::string    m_addressingVersionURI;
	std::list<std::string>      m_refParams;
	std::list<std::string>      m_refProps;

};

}}
#endif
