// $Id: Constants.h,v 1.1 2006/04/07 13:07:35 xdaq Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _ws_adressing_Constants_h_
#define _ws_adressing_Constants_h_

#include<string>

namespace ws {
namespace adressing {


   const std::string NSURI_ADDRESSING_SCHEMA = "http://schemas.xmlsoap.org/ws/2004/08/addressing";

  
   const std::string NSPREFIX_ADDRESSING_SCHEMA = "wsa";

  
   const std::string TO = "To";

  
   const std::string ACTION = "Action";

}}

#endif
