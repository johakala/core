// $Id: Filter.h,v 1.1 2006/04/07 14:19:54 xdaq Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/
 
#ifndef _ws_notification_Filter_h_
#define _ws_notification_Filter_h_

namespace ws {
namespace notification {

/*
 * 
 * 
 */
public interface Filter
{
	public:

	/*
	* Creates a new {@link AbstractFilter} object.
	*/
	AbstractFilter()
	{
		#warning "TBD"
		/*
		m_content    = null;
		m_dialect    = null;
		*/
	}

	/**
	* Creates a new {@link AbstractFilter} object.
	*
	* @param content 
	*/
	AbstractFilter( Node content )
	{
		#warning "TBD"
		/*
		m_content    = content;
		m_dialect    = null;
		*/
	}

	/*
	 * Creates a new {@link AbstractFilter} object.
	 *
	 * @param content 
	 * @param dialect 
	 */
	AbstractFilter( Node content, toolbox::net::URI  dialect )
	{
		#warning "TBD"
		/*
		m_content    = content;
		m_dialect    = dialect;
		*/
	}

	/*
	 *
	 * @param n 
	 */
	void setContent( Node n )
	{
		#warning "TBD"
		/*
		this.m_content = n;
		*/
	}

	/*
	 *
	 * @return 
	 */
	Object getExpression(  )
	{
		return m_content.getNodeValue(  );
	}

	/*
	 *
	 * @param uri 
	 */
	public void setURI( String uri )
	{
		#warning "TBD"
		/*
		try
		{
		 this.m_dialect = new URI( uri );
		}
		catch ( java.net.URISyntaxException e )
		{
		 //TODO
		 e.printStackTrace(  );
		}
		*/
	}

	/*
	 *
	 * @param uri 
	 */
	void setURI( toolbox::net::URI uri )
	{
		#warning "TBD"
		/*
		this.m_dialect = uri;
		*/
	}

	/*
	 *
	 * @return 
	 */
	toolbox::net::URI getURI(  )
	{
		#warning "TBD"
		/*
		return m_dialect;
		*/
	}
	
	protected: 

	Node m_content;
	toolbox::net::URI  m_dialect;
};

}}

#endif
