// $Id: Publisher.h,v 1.1 2006/04/07 13:07:35 xdaq Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/
 
#ifndef _ws_notification_Publisher_h_
#define _ws_notification_Publisher_h_

#include "xoap/MessageReference.h"

namespace ws {
namespace notification {


class Publisher
{
	public:
	
	/*
	 * Creates a new {@link Publisher} object.
	 *
	 * @param url 
	 */
	Publisher( const std::string& url );

	/*
	 * 
	 *
	 * @param msg 
	 * @param t 
	 */
	/*
	public void publish( Object                                  msg,
                	org.apache.ws.notification.topics.Topic t )
	{
	}
	*/

	/*
	 * Send the @param soapMsg to the url to which this publisher points.
	 * The message represents the <Notify> message to be sent
	 */
	void publish(  xoap::MessageReference msg );

	protected:

	std::string  m_url;
};

}}
#endif
