// $Id: TopicSet.h,v 1.1 2006/04/07 13:07:36 xdaq Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _ws_notification_topics_TopicSet_h_
#define _ws_notification_topics_TopicSet_h_

#include <string>
#include <set>
#include <list>
#include <map>

#include "ws/notification/topics/TopicExpression.h"
#include "ws/notification/topics/exception/Expression.h"

namespace ws {
namespace notification {
namespace topics {

/**
 * The set of topics that a particular NotificationProducer supports.
 */
class TopicSet
{
	TopicSet();
	
	TopicSet( std::set<TopicExpression*>& topicExprs )
		throw (ws::notification::topics::exception::Expression);
	
	~TopicSet();

   /**
    * Sets the boolean that indicates if this topic set is fixed as defined by the WS-BaseNotification specification.
    *
    * @param fixed
    */
   void setFixed( bool fixed );

   /**
    * Returns a boolean that indicates if this topic set is fixed as defined by the WS-BaseNotification specification.
    */
   bool isFixed(  );

   /**
    * Returns the topic expressions contained in this topic set.
    */
   std::set<TopicExpression*>& getTopicExpressions(  );

   /**
    * Returns the topics contained in this topic set.
    */
   std::set<Topic*> getTopics(  );

   /**
    * @param topicExpr
    */
   void addTopicExpression( TopicExpression* topicExpr ) 
   	throw (ws::notification::topic::exception::Expression);

   /**
    *
    * @param topicPath the path to exactly one Topic (e.g. new QName( "http://example.org/topicSpace/example1", "t4/t5" ))
    *
    * @return true if this topic set contains the specified topic, or false if it does not
    */
   bool containsTopic( const std::string& topicPath ); // QNAME

   /**
    *
    * @param topicExpr
    * @return
    * @throws TopicExpressionException
    */
   std::set<Topic*> evaluateTopicExpression( TopicExpression* topicExpr ) 
   	throw (ws::notification::topic::exception::Expression);
	
	std::string toString(  );
	
	protected:
	
	std::set<Topic*> getSupportedTopics( std::list<Topic*>& topics );
	
	 std::set<TopicExpression*>     m_topicExprs;
   	 std::map<std::string, Topic*>     m_topics; // maps topicPath QName to Topic
   	 bool m_fixed;
   
};

}}}

#endif
