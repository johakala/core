// $Id: NamespaceNotFound.h,v 1.1 2006/04/07 13:07:37 xdaq Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _ws_notification_topics_exception_NamespaceNotFound_h_
#define _ws_notification_topics_exception_NamespaceNotFound_h_

#include "ws/notification/topics/exception/Exception.h"

namespace ws {
	namespace notification {
		namespace topics { 
			namespace exception { 
				class NamespaceNotFound: public ws::notification::topics::exception::Exception 
				{
					public: 
					NamespaceNotFound( std::string name, std::string message, std::string module, int line, std::string function ): 
							ws::notification::topics::exception::Exception(name, message, module, line, function) 
					{} 

					NamespaceNotFound( std::string name, std::string message, std::string module, int line, std::string function,
						xcept::Exception& e ): 
							ws::notification::topics::exception::Exception(name, message, module, line, function, e) 
					{} 
				}; 
			}
		}
	} 
}

#endif
