// $Id: FullTopicExpression.h,v 1.1 2006/04/07 13:07:36 xdaq Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _ws_notification_topics_FullTopicExpression_h_
#define _ws_notification_topics_FullTopicExpression_h_

#include <string>
#include <list>

#include "ws/notification/topics/TopicExpression.h"
#include "ws/notification/topics/TopicsConstants.h"

namespace ws {
namespace notification {
namespace topics {

/*
 * A Full TopicExpression as defined by the WS-Topics specification.
 */
class FullTopicExpression: public TopicExpression
{
  
	public:
	
	/*
	 * Creates a new {@link FullTopicExpression} object.
	 *
	 * @param topicPaths 
	 */
	FullTopicExpression( std::list<std::string> & qnames );


	/*
	 * 
	 *
	 * @return 
	 */
	std::list<std::string> & getTopicPaths();

	/*
	 * 
	 *
	 * @param obj 
	 *
	 * @return 
	*/
	bool equals( TopicExpression * te );


	protected:

	std::list<std::string> m_topicPaths;
};

}}}

#endif
