// $Id: TopicsTypeFactory.h,v 1.1 2006/04/07 13:07:36 xdaq Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _ws_notification_topics_TopicsTypeFactory_h_
#define _ws_notification_topics_TopicsTypeFactory_h_

namesapce ws {
namesapce notification {
namesapce topics {
/**
 * A factory for types defined by the WS-Topics specification.
 */
class TopicsTypeFactory
{
	public:

	/*
	* 
	*
	* @return 
	*/
	static TopicsTypeFactory newInstance()
	{
		return new TopicsTypeFactoryImpl();
	}

	/**
	* 
	* @param targetNamespace 
	*
	* @return 
	*/
	TopicNamespace createTopicNamespace( String targetNamespace );

	protected:

	static TopicsTypeFactory instance_;
};

}}}

#endif
