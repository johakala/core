// $Id: TopicNamespaceRegistry.h,v 1.1 2006/04/07 13:07:36 xdaq Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/
 
#include <string>
#include <set>
#include <map>

#include "ws/notification/topics/TopicNamespace.h"
#include "ws/notification/topics/exception/NamespaceNotFound.h"
#include "ws/notification/topics/exception/NamespaceExisting.h"

namespace ws {
namespace notification {
namespace topics {


/*
 * A registry for topic namespace definitions.
 *
 * 
 */
class TopicNamespaceRegistry
{

	public:

   /*
    * Creates a new {@link TopicNamespaceRegistry} object.
    */
   TopicNamespaceRegistry();

   /*
    * Creates a new {@link TopicNamespaceRegistry} object.
    *
    * @param topicNsSet 
    */
   TopicNamespaceRegistry( std::set<std::string>& topicNsSet );

   /*
    * 
    *
    * @param targetNamespace 
    *
    * @return 
    */
   TopicNamespace* getTopicNamespace( const std::string& targetNamespace )
    	throw (ws::notification::topics::exception::NamespaceNotFound);

   /*
    * 
    *
    * @param topicNs 
    */
   void addTopicNamespace( TopicNamespace* topicNs ) 
   	throw (ws::notification::topics::exception::NamespaceExisting);

   /*
    * Loads each of the TopicNamespace documents from the URLs in the specified set and
    * adds them to this registry.
    *
    * @param topicNsSet a set of Strings representing URLs of TopicNamespace documents
    */
   void initTopicNamespaceMap( std::set<std::string>& topicNsSet );
   
   	protected:
   /*
    * maps targetNamespace <String> to <TopicNamespace>
    */
   std::map<std::string, TopicNamespace*> m_topicNsMap;

};

}}}
