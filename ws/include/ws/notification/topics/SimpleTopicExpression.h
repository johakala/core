// $Id: SimpleTopicExpression.h,v 1.2 2006/04/07 13:11:35 xdaq Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _ws_notification_topics_SimpleTopicExpression_h_
#define _ws_notification_topics_SimpleTopicExpression_h_

#include <string>
#include "ws/notification/topics/TopicExpression.h"
#include "ws/notification/topics/TopicsConstants.h"

namespace ws {
namesapce notification {
namesapce topics {
/*
 * A Simple TopicExpression as defined by the WS-Topics specification.
 */
public class SimpleTopicExpression: public TopicExpression
{

	public:


	/*
	 * Creates a new {@link SimpleTopicExpression} object.
	 *
	 * @param topicPath 
	 */
	SimpleTopicExpression( const std::string & qname );



	/*
	 * 
	 *
	 * @return 
	 */
	std::string getTopicPath(  );

	/*
	 * @param obj 
	 *
	 * @return 
	 */
	bool equals( TopicExpression * te );



	protected:

	std::string m_topicPath;
};

}}}

#endif
