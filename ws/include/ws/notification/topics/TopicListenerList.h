/*=============================================================================*
 *  Copyright 2004 The Apache Software Foundation
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *=============================================================================*/
package org.apache.ws.notification.topics;

import java.util.Iterator;

/**
 * Interface used for managing a list of topic listeners. Currently inherited by the <code>TopicList</code> and
 * <code>Topic</code> interfaces.
 *
 * @see TopicListener
 * @see TopicSpace
 * @see Topic
 */
public interface TopicListenerList
{
   /**
    * Add a topic listener
    *
    * @param listener The topic listener to add
    *
    * @see TopicListener
    */
   void addTopicListener( TopicListener listener );

   /**
    * Remove a topic listener
    *
    * @param listener The topic listener to remove.
    *
    * @see TopicListener
    */
   void removeTopicListener( TopicListener listener );

   /**
    * Get a iterator for the list of TopicListeners
    *
    * @return The iterator
    *
    * @see TopicListener
    */
   Iterator topicListenerIterator(  );
}