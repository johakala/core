// $Id: ConcreteTopicExpression.h,v 1.1 2006/04/07 13:07:36 xdaq Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _ws_notification_topics_ConcreteTopicExpression_h_
#define _ws_notification_topics_ConcreteTopicExpression_h_

#include <string>
#include "ws/notification/topics/TopicExpression.h"
#include "ws/notification/topics/TopicsConstants.h"

namespace ws {
namespace notification {
namespace topics {
/**
 * A Concrete TopicExpression as defined by the WS-Topics specification.
 */
class ConcreteTopicExpression: public TopicExpression
{

   	public:
   
	ConcreteTopicExpression( const std::string &  qname );

	std::string getTopicPath();

	bool equals( TopicExpression * te );

	protected:

	std::string m_topicPath;

};

}}}

#endif
