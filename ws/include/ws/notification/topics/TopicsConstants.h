// $Id: TopicsConstants.h,v 1.1 2006/04/07 13:07:36 xdaq Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _ws_notification_topics_TopicsConstant_h_
#define _ws_notification_topics_TopicsConstant_h_

namespace ws {
namespace notification {
namespace topics {
/**
 * Constants defined by the WS-Topics 1.2 Draft 01 specification.
 */

   /**
    * Namespace URI of the WS-Topics 1.2 Draft 01 schema.
    */
   const std::string NSURI_WSTOP_SCHEMA = "http://docs.oasis-open.org/wsn/2004/06/wsn-WS-Topics-1.2-draft-01.xsd";

   /**
    * Namespace prefix for the WS-Topics 1.2 Draft 01 schema.
    */
   const std::string NSPREFIX_WSTOP_SCHEMA = "wstop";

   /**
    * URI of the "Simple" topic expression dialect.
    */
   const std::string TOPIC_EXPR_DIALECT_SIMPLE = "http://docs.oasis-open.org/wsn/2004/06/TopicExpression/Simple";

   /**
    * URI of the "Concrete" topic expression dialect.
    */
   const std::string TOPIC_EXPR_DIALECT_CONCRETE = "http://docs.oasis-open.org/wsn/2004/06/TopicExpression/Concrete";

   /**
    * URI of the "Full" topic expression dialect.
    */
   const std::string TOPIC_EXPR_DIALECT_FULL = "http://docs.oasis-open.org/wsn/2004/06/TopicExpression/Full";
   
}}}

#endif
