// $Id: PathTokenizer.h,v 1.1 2006/04/07 13:07:36 xdaq Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _ws_notification_topics_PathTokenizer_h_
#define _ws_notification_topics_PathTokenizer_h_


namesapce ws {
namespace notification {
namesapce topics {

/**
* DOCUMENT_ME
*
*/
class PathTokenizer
{
	public:
	
	/**
	* Creates a new {@link PathTokenizer} object.
	*
	* @param path DOCUMENT_ME
	*/
	PathTokenizer( const std:;string & path )
	{
	 	m_path      = path;
		m_maxPos    = m_path.length(  );
	}

	/**
	* DOCUMENT_ME
	*
	* @return DOCUMENT_ME
	*/
	bool hasMoreTokens(  )
	{
	 	return m_currentPos < m_maxPos;
	}

	/**
	* DOCUMENT_ME
	*
	* @return DOCUMENT_ME
	*/
	std::string nextToken(  ) throws (ws::notification::topics::exception::InvalidTopicExpression)
	{
		 if ( m_currentPos >= m_maxPos )
		 {
		    throw new NoSuchElementException(  );
		 }

		 int startPos = m_currentPos;
		 m_currentPos = scanToken( startPos );
		 String token = m_path.substring( startPos, m_currentPos );
		 if ( token.startsWith( "//" ) )
		 {
		    token = token.substring( 1 );
		 }

		 ++m_currentPos; // skip the slash
		 return token;
	}

	protected:
	
	int scanToken( int startPos ) throw (ws::notification::topics::exception::InvalidTopicExpression )
	{
		 int newPos = startPos;
		 if ( newPos == 0 ) // special handling for head of path, as per spec
		 {
		    if ( m_path.startsWith( "//" ) )
		    {
		       newPos += 2;
		    }

		    if ( m_path.charAt( newPos ) == '.' )
		    {
		       throw new InvalidTopicExpressionException( "'.' may not be used as the first component of a topic path." );
		    }
		 }
		 else
		 {
		    if ( m_path.charAt( newPos ) == '/' )
		    {
		       newPos += 1; // do not count leading slash as a delim
		    }
		 }
		 while ( newPos < m_maxPos )
		 {
		    if ( m_path.charAt( newPos ) == '/' )
		    {
		       break;
		    }

		    newPos++;
		 }

		 return newPos;
	}
	
	protected:
	
	std::string m_path;
	int    m_currentPos;
	int    m_maxPos;

};
}}}

#endif
