// $Id: Topic.h,v 1.1 2006/04/07 13:07:36 xdaq Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _ws_notification_topics_Topic_h_
#define _ws_notification_topics_Topic_h_
 
#include<string>
#include<list>

#include "ws/notification/topics/TopicSiblingSet.h"
#include "ws/notification/topics/TopicExpression.h"
#include "ws/notification/topics/TopicNamespace.h"

namespace ws {
namespace notification {
namespace topics {

/**
 * Interface for representing a topic. A topic is either a child of a parent topic or in the case of root topics a
 * member of a topic list. Non-root topics have names without a namespace component.
 *
 * @see TopicNamespace
 */
class Topic: public TopicSiblingSet
{
	public:

	 /**
	* Create a topic with the given name
	*
	* @param name The name of the created topic
	*/
	Topic( const std::string& name );

	~Topic();

	/**
	* Get the name of this topic.
	*
	* @return the name of this topic
	*/
	const std::string& getName(  );

	/**
	* Is this a topic reference?
	*
	* @return true if this topic is a reference to another topic, false if not
	*/
	bool isReference(  );

	/**
	* Set the TopicSpace this Topic is part of
	*
	* @param topicNs The TopicSpace this Topic is part of
	*/
	void setTopicNamespace( TopicNamespace* topicNs );

	/**
	* Gets the TopicSpace this Topic is part of
	*
	* @return the TopicSpace this Topic is part of
	*/
	TopicNamespace* getTopicNamespace(  );

	/**
	* Set the topic path. The topic path is represented as a ordered list of topic names
	*
	* @param topicPath The topic path to associate with this topic
	*/
	void setTopicPath( std::list<std::string>& topicPath );

	/**
	* Get the topic path. The topic path is represented as an ordered list of topic names
	*
	* @return The topic path of this topic
	*/
	std::list<std::string>& getTopicPath(  );

	/**
	* Set the topic expression that resolves to a set of topics that this topic references.
	* Only used for reference topics.
	*
	* @param topicPath The topic expression to set.
	*/
	void setTopicReference( TopicExpression* topicPath );

	/**
	* Get the topic expression for the topic(s) that this topic references.
	*
	* @return The topic expression that this topic reference or null if this topic is not a reference.
	*/
	TopicExpression* getTopicReference(  );


	// Interface inherited from TopicSiblingSet
	// ----------------------------------------
	/**
	* Gets the topic with the specified name.
	*
	* @param name the name of the topic to get
	*
	* @return the topic, or null if no topic with the given name exists in this set
	*/
	Topic* getTopic( const std::string& name )
		throw (ws::notification::topics::exception::TopicNotFound);

	/**
	* Creates a new Topic object with the specified name and adds it to this set.
	* If this set already contains a Topic with the specified name, it will be replaced.
	*
	* @param name the name of the topic to be added
	*/
	Topic* addTopic( const std::string& name )
		throw (ws::notification::topics::exception::InvalidTopic);

	/**
	* Returns <tt>true</tt> if this set contains a topic with the specified name.
	*
	* @param name the name
	*
	* @return <tt>true</tt> if this set contains a topic with the specified name
	*/
	bool containsTopic( const std::string& name );

	/**
	* Removes the topic with the specified name from this set.
	*
	* @param name the name of the topic to remove
	*/
	void removeTopic( const std::string& name )
		throw (ws::notification::topics::exception::TopicNotFound);
		
	void removeTopic( Topic* topic )
		throw (ws::notification::topics::exception::TopicNotFound);
		
	/**
	 * @return true if the name of the @param topic equals the name of this topic object
	 * @return false otherwise
	 */
	bool equals( Topic* topic );

	/**
	* Gets an iterator of the topics in this set.
	*
	* @return the iterator
	*/
	std::set<Topic*> getTopics(  );
	
	/**
	* Returns a String representation of this Topic in following format: "{" + target_namespace + "}" + topic_path. If
	* the target namespace is "" (i.e. the ad-hoc namespace), only the topic path is returned. For example:
	* "{http://example.org/topicSpace/example1}t1/t3" or "t7/t8".
	*
	* @return a String representation of this Topic
	*/
	std::string toString(  );

	protected:
	
	Topic* ws::notification::topics::Topic::addTopic( Topic* topic )
		throw (ws::notification::topics::exception::InvalidTopic);

	/**
	* DOCUMENT_ME
	*/
	std::string m_name;

	/**
	* DOCUMENT_ME
	*/
	std::map<std::string, Topic*> m_subTopics;

	/**
	* DOCUMENT_ME
	*/
	TopicExpression* m_reference;

	/*
	* List of the components of the topic path
	*/

	/** DOCUMENT_ME */
	std::list<std::string> m_topicPath; //

	/**
	* DOCUMENT_ME
	*/
	TopicNamespace* m_topicNs;
};

}}}

#endif
