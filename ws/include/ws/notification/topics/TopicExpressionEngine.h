// $Id: TopicExpressionEngine.h,v 1.1 2006/04/07 13:07:36 xdaq Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _ws_notification_topics_TopicExpressionEngine_h_
#define _ws_notification_topics_TopicExpressionEngine_h_


#include <string>
#include "ws/notification/topics/Topic.h"
#include "ws/notification/topics/TopicExpressionEvaluator.h"
#include "ws/notification/topics/ConcreteTopicExpressionEvaluator.h"
#include "ws/notification/topics/FullTopicExpressionEvaluator.h"
#include "ws/notification/topics/SimpleTopicExpressionEvaluator.h"
#include "ws/notification/topics/exception/NoExpressionEvaluatorForDialect.h"

namespace ws {
namespace notification {
namespace topics {

class TopicNamespaceRegistry;

/**
 * Resolves topic expressions over the set of topic namespaces stored in its topic
 * namespace registry.
 *
 * @see TopicNamespaceRegistry
 */
class TopicExpressionEngine
{
	public:
	
	/*
	 * Creates a new {@link TopicExpressionEngine}.
	 */
	TopicExpressionEngine( TopicNamespaceRegistry * topicNsRegistry );
	
	/*
	 * Creates a new {@link TopicExpressionEngine}.
	 *
	 * @param topicNsRegistry
	 * @param topicExprEvaluatorMap
	 */
	TopicExpressionEngine( TopicNamespaceRegistry * topicNsRegistry,
                        	 std::map<std::string, ws::notification::topics::TopicExpressionEvaluator * > &   topicExprEvaluatorMap );

	/*
	 * @param dialect 
	 *
	 * @return
	 */
	ws::notification::topics::TopicExpressionEvaluator * getEvaluator( const std::string & dialect ) 
		throw (ws::notification::topics::exception::NoExpressionEvaluatorForDialect);

	/*
	 * 
	 *
	 * @return DOCUMENT_ME
	 */
	std::list<std::string>  getSupportedDialects();
	

	/*
	 * @param topicExpr 
	 *
	 * @return DOCUMENT_ME
	 *
	 */
	std::list<Topic *> evaluateTopicExpression( ws::notification::topics::TopicExpression & topicExpr )
		throw (ws::notification::topics::exception::TopicPathDialectUnknown, 
	  	       ws::notification::topics::exception::TopicExpressionNoResolution, 
	 	       ws::notification::topics::exception::InvalidTopicExpression, 
	  	       ws::notification::topics::exception::Expression);

	/*
	 * @param evaluator
	 */
	void registerEvaluator( ws::notification::topics::TopicExpressionEvaluator * evaluator );
	
	protected:

	ws::notification::topics::TopicNamespaceRegistry * m_topicNsRegistry;
	std::map<std::string, ws::notification::topics::TopicExpressionEvaluator * >   m_evaluators;
};

}}}

#endif
