// $Id: SimpleTopicExpressionEvaluator.h,v 1.1 2006/04/07 13:07:36 xdaq Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _ws_notification_topics_SimpleTopicExpressionEvaluator_h_
#define _ws_notification_topics_SimpleTopicExpressionEvaluator_h_


#include <string>
#include "ws/notification/topics/Topic.h"
#include "ws/notification/topics/exception/TopicPathDialectUnknown.h" 
#include "ws/notification/topics/exception/TopicExpressionNoResolution.h"
#include "ws/notification/topics/exception/InvalidTopicExpression.h" 
#include "ws/notification/topics/exception/Expression.h"

namespace ws {
namespace notification {
namespace topics {

class TopicNamespaceRegistry;

/**
 * Topic expression evalutor for the WS-Topics "Simple" topic expression dialect.
 *
 * @see org.apache.ws.notification.topics.expression.TopicExpressionEvaluator
 *
 * @author Ian Springer
 */
class SimpleTopicExpressionEvaluator: public TopicExpressionEvaluator
{

	public:
	

	//private static final String[] SUPPORTED_DIALECTS = 
	//                                                   {
	//                                                      TopicsConstants.TOPIC_EXPR_DIALECT_SIMPLE
	//                                                   };

	
	std::list<std::string> & getDialects();

	virtual std::list<ws::notification::topics::Topic*> evaluate( ws::notification::topics::TopicNamespaceRegistry * topicNsRegistry,
                     					              ws::notification::topics::TopicExpression & topicExpr )
		throw( ws::notification::topics::exception::TopicPathDialectUnknown, 
		       ws::notification::topics::exception::TopicExpressionNoResolution, 
		       ws::notification::topics::exception::InvalidTopicExpression, 
		       ws::notification::topics::exception::Expression);

	std::list<ws::notification::topics::Topic*> evaluateTopicPath( ws::notification::topics::TopicNamespaceRegistry * topicNsRegistry,
	                                                               const std::string & topicPath )
		throw ( ws::notification::topics::exception::TopicExpressionNoResolution, 
          	ws::notification::topics::exception::InvalidTopicExpression);
};

}}}
#endif
