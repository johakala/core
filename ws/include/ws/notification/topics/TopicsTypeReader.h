// $Id: TopicsTypeReader.h,v 1.1 2006/04/07 13:07:36 xdaq Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _ws_notification_topics_TopicsTypeReader_h_
#define _ws_notification_topics_TopicsTypeReader_h_

#include <string>
#include <list>

#include "ws/notification/topics/TopicExpression.h"
#include "ws/notification/topics/TopicsConstants.h"

namesapce ws {
namesapce notification {
namesapce topics {

/*
 * 
 */
public abstract class TopicsTypeReader
{
	public:
	
   /*
    * @param schemaNamespace 
    *
    * @return 
    *
    */
   static TopicsTypeReader newInstance( const std::string & schemaNamespace ) 
   	throw ( ws::notification::topics::exception::UnsupportedNamespace );
 
   /*
    * @param xBean 
    *
    * @return 
    *
    */
    toTopicExpression( Document * xBean ) throw ( ws::notification::topics::exception::InvalidTopicExpression);

   /**
    * 
    *
    * @param xBean 
    *
    * @return 
    */
   TopicNamespace toTopicNamespace( Document * xBean );
   
   protected:
   
   
};

}}}

#endif
