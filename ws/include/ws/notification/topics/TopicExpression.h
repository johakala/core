// $Id: TopicExpression.h,v 1.1 2006/04/07 13:07:36 xdaq Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _ws_notification_topics_TopicExpression_h_
#define _ws_notification_topics_TopicExpression_h_

#include <string>


namespace ws {
namespace notification {
namespace topics {
/**
 * C++ representation of the wsnt:TopicExpressionType XML schema type.
 */
class TopicExpression
{
	public:
	
	virtual ~TopicExpression() {}
	/*
	 * Creates a new {@link AbstractTopicExpression} object.
	 */

	TopicExpression( const std::string &  dialect);

	/*
	 * Returns the dialect URI of this topic expression.
	 */
	std::string getDialect();

	/*
	 * @return 
	*/
	virtual std::string toString(  ) = 0;

	/*
	 * @return 
	 */
	virtual  bool equals( TopicExpression * te ) = 0;

	protected:


	/*
	 * @param dialect 
	 */
	void setDialect( const std::string & dialect );

	std::string m_dialect;

};

}}}

#endif
