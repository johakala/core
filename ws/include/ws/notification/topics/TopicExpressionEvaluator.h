// $Id: TopicExpressionEvaluator.h,v 1.1 2006/04/07 13:07:36 xdaq Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _ws_notification_topics_TopicExpressionEvaluator_h_
#define _ws_notification_topics_TopicExpressionEvaluator_h_


#include <string>
#include <list>

#include "ws/notification/topics/Topic.h"
#include "ws/notification/topics/exception/TopicPathDialectUnknown.h" 
#include "ws/notification/topics/exception/TopicExpressionNoResolution.h"
#include "ws/notification/topics/exception/InvalidTopicExpression.h" 
#include "ws/notification/topics/exception/Expression.h"

namespace ws {
namespace notification {
namespace topics {

class TopicNamespaceRegistry;

/**
* A <code>TopicExpressionEvaluator</code> is used to evaluate a topic expression of a known dialect
* against a set of topic namespaces. An evaluator can be registered with a <code>TopicExpressionEngine</code>,
* which in turn delegates to the evaluator evaluation of expressions with the corresponding dialect.
*/
class TopicExpressionEvaluator
{
	public:

	/**
	* Gets the URIs for the dialects that this evaluator can handle
	*
	* @return list of URIs supported by this evaluator
	*/
	virtual std::list<std::string> & getDialects() = 0;

	/**
	* Evaluates the expression over a TopicNamespace and returns the set of matching topics.
	*
	* @param topicNsRegistry
	* @param topicExpr object passed by client representing the topic expression
	* @return the set of topics that matched the specified expression
	*
	* @throws ws::notification::topics::exception::TopicPathDialectUnknown
	*                                  if the topic expression dialect is not supported
	* @throws ws::notification::topics::exception::TopicExpressionNoResolution
	*                                  if the expression could not be evaluated
	* @throws ws::notification::topics::exception::InvalidTopicExpression
	*                                  if the topic expression is invalid
	* @throws ws::notification::topics::exception::Expression
	*                                  if any other error occurs
	*/
	virtual std::list<ws::notification::topics::Topic*> evaluate( ws::notification::topics::TopicNamespaceRegistry * topicNsRegistry,
                     						 ws::notification::topics::TopicExpression & topicExpr )
   					throw( ws::notification::topics::exception::TopicPathDialectUnknown, 
          				       ws::notification::topics::exception::TopicExpressionNoResolution, 
          				       ws::notification::topics::exception::InvalidTopicExpression, 
          				       ws::notification::topics::exception::Expression) = 0;

	protected:


	 /** DOCUMENT_ME */
	// static final Map RESULTS_CACHE = Collections.synchronizedMap( new HashMap(  ) );

	/**
	* DOCUMENT_ME
	*
	* @param topicPath DOCUMENT_ME
	*
	* @return DOCUMENT_ME
	*
	* @throws TopicExpressionResolutionException
	*          DOCUMENT_ME
	*/
	TopicNamespace getTopicNamespace( TopicNamespaceRegistry * topicNsRegistry, std::string topicPath )
   		throw ( ws::notification::topics::exception::TopicExpressionNoResolution );
	/**
	* Performs basic validation of the specified topic path.
	*
	* @param topicPath a topicPath
	*
	* @throws InvalidTopicExpressionException
	*/
	void validateTopicPath( const std::string & topicPath ) 
		throw ( ws::notification::topics::exception::InvalidTopicExpression );
};	

}}}

#endif
