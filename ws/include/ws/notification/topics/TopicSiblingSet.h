// $Id: TopicSiblingSet.h,v 1.1 2006/04/07 13:07:36 xdaq Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _ws_notification_topics_TopicSiblingSet_h_
#define _ws_notification_topics_TopicSiblingSet_h_

#include<string>
#include<set>

#include "ws/notification/topics/exception/InvalidTopic.h"
#include "ws/notification/topics/exception/TopicNotFound.h"

namespace ws {
namespace notification {
namespace topics {

class Topic;

/**
 * A set of sibling {@link Topic}s.
 */
class TopicSiblingSet
{
	public:

	virtual ~TopicSiblingSet() {}

	/*
	* Gets the topic with the specified name.
	*
	* @param name the name of the topic to get
	*
	* @return the topic, or null if no topic with the given name exists in this set
	*/
	virtual ws::notification::topics::Topic* getTopic( const std::string& name ) 
		throw (ws::notification::topics::exception::TopicNotFound) = 0;

	/*
	* Creates a new Topic object with the specified name and adds it to this set.
	* If this set already contains a Topic with the specified name, it will be replaced.
	*
	* @param name the name of the topic to be added
	*/
	virtual ws::notification::topics::Topic* addTopic( const std::string& name ) 
		throw (ws::notification::topics::exception::InvalidTopic) = 0;

	/**
	* Returns <tt>true</tt> if this set contains a topic with the specified name.
	*
	* @param name the name
	*
	* @return <tt>true</tt> if this set contains a topic with the specified name
	*/
	virtual bool containsTopic( const std::string& name ) = 0;

	/**
	* Removes the topic with the specified name from this set.
	*
	* @param name the name of the topic to remove
	*/
	virtual void removeTopic( const std::string& name ) 
		throw (ws::notification::topics::exception::TopicNotFound) = 0;

	/**
	* Gets an iterator of the topics in this set.
	*
	* @return the iterator
	*/
	virtual std::set<ws::notification::topics::Topic*> getTopics() = 0;
};

}}}

#endif
