// $Id: TopicNamespace.h,v 1.1 2006/04/07 13:07:36 xdaq Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/
 
#ifndef _ws_notification_topics_TopicNamespace_h_
#define _ws_notification_topics_TopicNamespace_h_
 
#include<string>
#include<map>
#include<list>
#include<set>
#include "ws/notification/topics/TopicSiblingSet.h"
 
namespace ws {
namespace notification {
namespace topics {

/**
 * A topic namespace (a hierarchy of topics within a particular namespace), as defined by the WS-Topics 1.3
 * specification.
 */
class TopicNamespace: public TopicSiblingSet
{
	public:

	TopicNamespace( const std::string& targetNs );

	~TopicNamespace();

	/**
	* Sets this topic namespace's finality.
	*
	* @param finality the finality
	*/
	void setFinal( bool finality );

	/**
	* If the value is <code>true</code> it indicates that NotificationProducers cannot use topics whose root topics do
	* not appear in this topic namespace. The default value is <code>false</code>.
	*
	* @return true if this topic namespace is final, or false if it is not
	*/
	bool isFinal(  );

	/**
	* Sets this topic namespace's (optional) name.
	*
	* @param name the name
	*/
	void setName( const std::string& name );

	/**
	* Returns this topic namespace's name, or null if no name is defined.
	*
	* @return this topic namespace's name, or null if no name is defined
	*/
	const std::string& getName(  );

	/**
	* Returns this topic namespace's target namespace.
	*
	* @return this topic namespace's target namespace
	*/
	const std::string& getTargetNamespace(  );

	// Interface inherited from TopicSiblingSet
	// ----------------------------------------
	/**
	* Gets the topic with the specified name.
	*
	* @param name the name of the topic to get
	*
	* @return the topic, or null if no topic with the given name exists in this set
	*/
	Topic* getTopic( const std::string& name )
		throw (ws::notification::topics::exception::TopicNotFound);

	/**
	* Creates a new Topic object with the specified name and adds it to this set.
	* If this set already contains a Topic with the specified name, it will be replaced.
	*
	* @param name the name of the topic to be added
	*/
	Topic* addTopic( const std::string& name )
		throw (ws::notification::topics::exception::InvalidTopic);

	/**
	* Returns <tt>true</tt> if this set contains a topic with the specified name.
	*
	* @param name the name
	*
	* @return <tt>true</tt> if this set contains a topic with the specified name
	*/
	bool containsTopic( const std::string& name );

	/**
	* Removes the topic with the specified name from this set.
	*
	* @param name the name of the topic to remove
	*/
	void removeTopic( const std::string& name )
	   throw (ws::notification::topics::exception::TopicNotFound);

	/**
	* Gets an iterator of the topics in this set.
	*
	* @return the iterator
	*/
	std::set<Topic*> getTopics(  );

	/**
	 * @return true if the target namespace of the @param obj equals the target namespace of this object
	 * @return false otherwise
	 */
	bool equals( TopicNamespace* obj );

	/**
	* Returns a const std::string& representation of this TopicNamespace in following format:
	* "{" + target_namespace + "}" + name. If the target namespace is
	* "" (i.e. the ad-hoc namespace), only the name is returned. If the name is
	* null, the name is ommitted.
	* For example: "{http://example.org/topicSpace/example1}TopicSpaceExample1" or
	* "AdHocTopicSpace".
	*
	* @return a const std::string& representation of this TopicNamespace
	*/
	std::string toString(  );
   
   protected:   
   
   Topic* ws::notification::topics::TopicNamespace::addTopic( Topic* topic )
	throw (ws::notification::topics::exception::InvalidTopic);
   
   std::string  m_targetNs;
   std::string m_name;
   bool m_final;
   std::map<std::string, Topic*> m_rootTopicMap;
};

}}}

#endif
