// $Id: TopicsTypeWriter.h,v 1.1 2006/04/07 13:07:36 xdaq Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _ws_notification_topics_TopicsTypeWriter_h_
#define _ws_notification_topics_TopicsTypeWriter_h_

#include <string>
#include <list>

#include "ws/notification/topics/TopicExpression.h"
#include "ws/notification/topics/TopicsConstants.h"

namesapce ws {
namesapce notification {
namesapce topics {

/**
 * A writer that can serialize various WS-Topics types to strongly-typed {@link XmlObject}s.
 * It is capable of supporting multiple versions of the WS-Topics specification.
 */
class TopicsTypeWriter
{
   /*
    * @param schemaNamespace 
    *
    * @return 
    *
    * @throws UnsupportedNamespaceException 
    */
   static TopicsTypeWriter newInstance( const std::string & schemaNamespace ) throw ( ws::notification::topics::exception::UnsupportedNamespace )
   {
    #warning "TBD"
   /*
      if ( schemaNamespace.equals( TopicsConstants.NSURI_WSTOP_SCHEMA ) )
      {
         return new TopicsTypeWriterImpl(  );
      }
      else
      {
         throw new UnsupportedNamespaceException( schemaNamespace
                                                  + " is not the namespace of a supported version of the WS-Topics schema." );
      }
      */
   }

   /*
    * @param topicExpr 
    * @param elemName 
    *
    * @return 
    */
    Document * toXmlObject( ws::notification::topics::TopicExpression topicExpr, QName elemName );

   /*
    * 
    *
    * @param topicNs 
    *
    * @return 
    */
    Document * toXmlObject( ws::notification::topics::TopicNamespace topicNs );

   protected:
   
   /*
    * 
    * @param topicExpr 
    * @param topicExprXBean 
    */
   void copyContent( ws::notification::topics::TopicExpression topicExpr, Document * topicExprXBean )
   {
   #warning "TBD"
   /*
      Object content = topicExpr.getContent(  );
      if ( content instanceof QName )
      {
         QName topicPath = (QName) content;
         XmlBeanUtils.setValue( topicExprXBean,
                                toString( topicPath, topicExprXBean ) );
      }
      else if ( content instanceof QName[] )
      {
         QName[] topicPaths = (QName[]) content;
         XmlBeanUtils.setValue( topicExprXBean,
                                toString( topicPaths, topicExprXBean ) );
      }
     */ 
   }

    std::string toString( QName topicPath, Document * targetXBean )
   { 
   #warning "TBD"
   /*
      StringBuffer strBuf = new StringBuffer(  );
      if ( StringUtils.isNotEmpty( topicPath.getNamespaceURI(  ) ) )
      {
         XmlCursor xCursor = targetXBean.newCursor(  );
         String    prefix = xCursor.prefixForNamespace( topicPath.getNamespaceURI(  ) );
         xCursor.dispose(  );
         strBuf.append( prefix );
         strBuf.append( ':' );
      }

      strBuf.append( topicPath.getLocalPart(  ) );
      return strBuf.toString(  );
      */
   }

  std::string toString( std::list<QName> & topicPaths, Document * targetXBean )
   {
   
   #warning "TBD"
   /*
      StringBuffer strBuf = new StringBuffer(  );
      for ( int i = 0; i < ( topicPaths.length - 1 ); i++ )
      {
         strBuf.append( toString( topicPaths[i], targetXBean ) );
         strBuf.append( '|' );
      }

      strBuf.append( toString( topicPaths[topicPaths.length - 1], targetXBean ) );
      return strBuf.toString(  );
   }
   */
};

}}}

#endif
