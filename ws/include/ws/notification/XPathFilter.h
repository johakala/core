// $Id: XPathFilter.h,v 1.1 2006/04/07 14:19:54 xdaq Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/
 
#ifndef _ws_notification_TopicFilter_h_
#define _ws_notification_TopicFilter_h_

#include <string>

#include "ws/notification/Filter.h"

namespace ws {
namespace notification {

/*
 *
 */
class XPathFilter: public Filter
{
	public:
	/*
	 * Creates a new {@link XPathFilter} object.
	 *
	 * @param filter 
	 */
	XPathFilter( const std::string & filter )
	{
		#warning "TBD"
		/*
		this( filter, FilterFactory.xpathuri );
		*/
	}

	/*
	 * Creates a new {@link XPathFilter} object.
	 *
	 * @param filter 
	 * @param dialect 
	 */
	XPathFilter( const std::string & filter, const std::string & dialect )
	{
		#warning "TBD"
		/*
		//TODO test pathexepr type
		//TODO create node
		javax.xml.parsers.DocumentBuilderFactory dbf = javax.xml.parsers.DocumentBuilderFactory.newInstance(  );
		try
		{
		 javax.xml.parsers.DocumentBuilder db = dbf.newDocumentBuilder(  );

		 org.w3c.dom.Document              dom = db.newDocument(  );
		 super.setContent( dom.createTextNode( filter ) );
		 super.setURI( FilterFactory.xpathuri );
		}
		catch ( javax.xml.parsers.ParserConfigurationException e )
		{
		 //TODO
		 e.printStackTrace(  );
		}

		super.setURI( dialect );
		*/
	}
};

}}

#endif
