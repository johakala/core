// $Id: Subscriber.h,v 1.1 2006/04/07 13:07:35 xdaq Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/
 
#ifndef _ws_notification_Subscriber_h_
#define _ws_notification_Subscriber_h_


#include "xoap/MessageFactory.h"
#include "ws/addressing/EndpointReference.h"

namespace ws {
namespace notification {
/*
 *
 */
class Subscriber // implements NotificationProducer
{
	public:
	
	/*
	 * Creates a new instance of Subscriber 
	 */
	Subscriber( const std::string& url, const std::string& configfile );

	/*
	 * @param filters 
	 *
	 * @return a SOAP message with the last notification
	 */
	 #warning "TBD"
	//xoap::MessageReference getCurrentMessage( std::list<Filter*>& filters );

	/*
	 * 
	 *
	 * @return 
	 */
	ws::addressing::EndpointReference* getEPR();

	/*
	 * @param notificationConsumer 
	 * @param subscriptionEndConsumer 
	 * @param tf 
	 * @param xf 
	 * @param initialTerminationTime 
	 * @param UseNotify 
	 *
	 * @return 
	 */
	 #warning "TBD"
	/*Subscription* subscribe( NotificationConsumer*    notificationConsumer,
				SubscriptionEndConsumer* subscriptionEndConsumer,
				TopicFilter*             tf,
				XPathFilter*             xf,
				toolbox::Timeval&                initialTerminationTime,
				bool                 UseNotify );
	*/			
	protected:
				
	ws::addressing::EndpointReference * epr;
	// SubscriptionHome  sH;
			
};
}}
#endif
