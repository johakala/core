// $Id: NotificationProducer.h,v 1.1 2006/04/07 14:19:54 xdaq Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/
 
#ifndef _ws_notification_NotificationProducer_h_
#define _ws_notification_NotificationProducer_h_


#include "ws/addressing/EndpointReference.h"

/*
 * 
 *
 */
class NotificationProducer
{
   /**
    * Returns this producer's endpoint reference.
    *
    * @return this producer's endpoint reference
    */
   virtual ws::addressing::EndpointReference getEPR() = 0;

   /**
    * Subscribe to notifications from this producer.
    *
    * @param notificationConsumer
    * @param subscriptionEndConsumer the callback Interface for SubscriptionEnd Notifications, or null if no SubscriptionEnd should be send
    * @param topicFilter
    * @param xpathFilter
    * @param initialTerminationTime
    * @param useNotify the notification delivery mode, or null to use default mode
    *
    * @return the subscription
    */
   virtual ws::notification::Subscription subscribe( ws::notification::NotificationConsumer  *  notificationConsumer,
                           ws::notification::SubscriptionEndConsumer * subscriptionEndConsumer,
                           ws::notification::topics::TopicFilter     *        topicFilter,
                           XPathFilter             xpathFilter,
                           Calendar                initialTerminationTime,
                           boolean                 useNotify );
};

}}
#endif

