// $Id: WsnRuntime.h,v 1.1 2006/04/07 13:07:35 xdaq Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _ws_notification_topics_WsnRuntime_h_
#define _ws_notification_topics_WsnRuntime_h_

#include <string>
#include "ws/notification/topics/TopicExpressionEngine.h"


namespace ws {
namespace notification {

/**
 * A singleton respresenting the WSN runtime.
 */
class WsnRuntime
{
	public:

	/**
	* Static function of the WSN runtime singleton
	*
	* @return a pointer to a WSN runtime object
	*/
	static WsnRuntime* getRuntime(  );

	/**
	* Destroy the WSN Runtime
	*/
	static void destroyRuntime(  );

	/**
	* Set the topic expression engine
	*
	* @param topicExprEngine DOCUMENT_ME
	*/
	void setTopicExpressionEngine( ws::notification::topics::TopicExpressionEngine* topicExprEngine );

	/**
	* Retrieve the topic expression engine
	*
	* @return a pointer to the topic expression engine or 0 if the engine is not set
	*/
	ws::notification::topics::TopicExpressionEngine* getTopicExpressionEngine(  );
	
	/**
	* Version of the WSN runtime
	*
	* @return the current version of the WSN runtime, should be 1.1
	*/
	std::string getVersion(  );
	
	private:

	WsnRuntime(  );
	~WsnRuntime(  );

	static WsnRuntime* instance_;
	ws::notification::topics::TopicExpressionEngine*   m_topicExprEngine;
};

}}
#endif
