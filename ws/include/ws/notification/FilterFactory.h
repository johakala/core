// $Id: FilterFactory.h,v 1.1 2006/04/07 14:19:54 xdaq Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/
 
#ifndef _ws_notification_FilterFactory_h_
#define _ws_notification_FilterFactory_h_

namespace ws {
namespace notification {

/*
 *
 */
class FilterFactory
{
  
	public:

	static std::string xpathuri = "http://www.w3.org/TR/1999/REC-xpath-19991116";


	static std::string topicsuri = "http://docs.oasis-open.org/wsn/2004/06/TopicExpression/Concrete";

	/**
	* 
	*
	* @param ft 
	*
	* @return 
	*/
	static Filter * createFilter( FilterType ft )
	{
		#warning "TBD"
		/*
		String dialect = ft.getDialect(  );
		Filter f = null;

		if ( ( dialect == null ) || ( dialect.equals( xpathuri ) ) )
		{
		 //its an XPathFilter
		 f = new XPathFilter( ft.newCursor(  ).getTextValue(  ),
                		      dialect );
		}
		else if ( dialect.equals( topicsuri ) )
		{
		 //TODO testing if topicexpression is ok
		 try
		 {
		    f = new TopicFilter( ft.newDomNode(  ),
                        		 new java.net.URI( dialect ) );
		 }
		 catch ( java.net.URISyntaxException e )
		 {
		    //TODO
		    e.printStackTrace(  );
		 }
		}

		LOG.info( "FilterFactory : " + dialect + " FilterExpr: " + f.getExpression(  ) );
		return f;
		*/
	}

    
};

}}

#endif
