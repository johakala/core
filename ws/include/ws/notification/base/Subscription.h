/*=============================================================================*
 *  Copyright 2004 The Apache Software Foundation
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *=============================================================================*/

#ifndef _ws_notification_base_Subscription_h_
#define _ws_notification_base_Subscription_h_

#include <string>
#include "ws/addressing/EndpointReference.h"
#include "ws/notification/topics/TopicExpression.h"
#include "ws/pubsub/NotificationConsumer.h"
#include "ws/pubsub/NotificationProducer.h"
#include "ws/resource/PropertiesResource.h"
#include "ws/resource/properties/query/QueryExpression.h"

namespace ws {
namespace notification {
namespace base {

/**
 * A WSN subscription resources. Exposes a subscription's properties, as well as
 * resource lifetime-related state.
 */
class Subscription: public PropertiesResource /*, ScheduledResourceTerminationResource */
{
	Subscription( 
   		EndpointReference* producerRef,
		EndpointReference* consumerRef,
		TopicExpression*   topicExpr );

	~Subscription();

	/**
	* Sets the consumer EPR associated with this subscription.
	*
	* @param consumerRef the consumer EPR to be associated with this subscription
	*/
	void setConsumerReference( EndpointReference* consumerRef );

	/**
	* Gets the consumer EPR associated with this subscription.
	*
	* @return the consumer EPR associated with this subscription
	*/
	EndpointReference* getConsumerReference(  );

	/**
	* This method exists for the Pubsub abstraction layer.
	*
	* @param notificationConsumer
	*/
	void setNotificationConsumer( NotificationConsumer* notificationConsumer );

	/**
	* This method exists for the Pubsub abstraction layer.
	*
	* @param notificationProducer
	*/
	void setNotificationProducer( NotificationProducer* notificationProducer );

	/**
	* Returns true if this subscription is currently paused, or false otherwise.
	*
	* @return true if this subscription is currently paused, or false otherwise
	*/
	bool isPaused(  );

	/**
	* Sets the policy for the subscription
	*
	* @param policy
	*/
	// void setPolicy( Object policy );

	/**
	* Gets the policy for this subscription.
	*
	* @return the policy for the subscription, or null if no policy is defined
	*/
	// Object getPolicy(  );

	/**
	* Sets the precondition for the subscription.
	*
	* @param precondition
	*/
	void setPrecondition( QueryExpression* precondition );

	/**
	* Gets the precondition associated with this subscription.
	*
	* @return the precondition, or null if no precondition was specified in the subscribe request
	*/
	QueryExpression* getPrecondition(  );

	/**
	* Gets the producer EPR associated with this subscription. Note, the producer EPR property is read-only.
	*
	* @return the producer EPR associated with this subscription
	*/
	EndpointReference* getProducerReference(  );

	/**
	* Gets the producer resource associated with this subscription.
	*
	* @return the producer resource
	*/
	NotificationProducerResource* getProducerResource(  );

	/**
	* Sets the selector for the subscription
	*
	* @param selector
	*/
	void setSelector( QueryExpression* selector );

	/**
	* Gets the selector associated with this subscription.
	*
	* @return the selector, or null if no selector was specified in the subscribe request
	*/
	QueryExpression* getSelector(  );

	/**
	* Sets the topic expression associated with this subscription.
	*
	* @param topicExpr the topic expression to be associated with this subscription
	*/
	void setTopicExpression( TopicExpression* topicExpr );

	/**
	* Gets the topic expression associated with this subscription.
	*
	* @return the topic expression associated with this subscription
	*/
	TopicExpression* getTopicExpression(  );

	/**
	* Sets the boolean useNotify flag on the subscription to determine if the notification is wrapped in a Notify
	* element.
	*
	* @param useNotify
	*/
	void setUseNotify( bool useNotify );

	/**
	* Wrap notification messages in the notify element?
	*
	* @return true (default) if notify should be used, or false if not
	*/
	bool getUseNotify(  );

	/**
	* Temporarily suspends the delivery of notification messages for this subscription.
	*
	* @throws PauseFailedException if unable to suspend message delivery
	*/
	void pause(  ) throw (ws::notification::base::exception::PauseFailedException);

	/**
	* Resumes the delivery of notification messages for this subscription.
	*
	* @throws ResumeFailedException if unable to resume message delivery
	*/
	void resume(  ) throw (ws::notification::base::exception::ResumeFailedException);

	void setResourcePropertySet( ResourcePropertySet* resourcePropertySet );

	ResourcePropertySet* getResourcePropertySet(  );

	void addTerminationListener(ResourceTerminationListener* resourceTerminationListener );

	void destroy(  );

	//
	// --------------------------------------------------
	//

	/**
	* Specification-generic interface respresenting a subscription.
	*
	*/

	/**
	* Returns the time at which this subscription was created.
	*
	* @return the time at which this subscription was created
	*/
	toolbox::TimeVal& getCreationTime(  );

	/**
	* Returns a list of any notification filters that are associated with this subscription. The filters are ordered in
	* the order in which they will be applied to notifications.
	*
	* @return a list of any notification filters that are associated with this subscription
	*/
	Filter* getFilters(  );

	/**
	* Get the notification consumer associated with this subscription.
	*
	* @return the notification consumer
	*/
	NotificationConsumer* getNotificationConsumer(  );

	/**
	* Get the notification producer associated with this subscription.
	*
	* @return the notification producer
	*/
	NotificationProducer* getNotificationProducer(  );

	/**
	* Get the subscriptionEnd consumer associated with this subscription.
	*
	* @return the subscriptionEnd consumer
	*/
	SubscriptionEndConsumer* getSubscriptionEndConsumer(  );

	/**
	* Sets the termination time for this subscription (i.e. the time at which it expires).
	*
	* @param terminationTime the termination time
	*/
	void setTerminationTime( const toolbox::TimeVal& terminationTime );

	/**
	* Gets the termination time for this subscription (i.e. the time at which it expires).
	*
	* @return the termination time
	*/
	toolbox::TimeVal& getTerminationTime(  );

	/**
	* Cancel this subscription.
	*/
	void unsubscribe(  );  

	/**
	* Sets the subscription identifier
	*
	* @param id the subscription identifier
	*/
	void setID( const std::string& id );

	/**
	* Returns the subscription identifier
	*
	* @return the subscription identifier
	*/
	const std::string& getID(  );

	protected:
	std::string	m_id;   
	toolbox::TimeVal	m_creationTime;
	toolbox::TimeVal	m_terminationTime;
	EndpointReference*	m_producerRef;
	EndpointReference*	m_consumerRef;
	EndpointReference*	m_epr;	
	TopicExpression*	m_topicExpr;

	bool			m_useNotify; // default true
	bool			m_isPaused; // default false

	QueryExpression*	m_precondition;
	QueryExpression*	m_selector;

	ResourcePropertySet*	m_propSet;

	NotificationConsumer*	m_notificationConsumer;
	NotificationProducer*	m_notificationProducer;
	std::list<ResourceTerminationListener*>	m_terminationListeners; 

	//Object			m_policy;

	void notifyResourceTerminationListeners(  );

	std::set<Topic*> evaluateTopicExpression(  )
		throw (ws::notification::topics::Expression);
};

}}}

#endif
