// $Id: TopicFilter.h,v 1.1 2006/04/07 14:19:54 xdaq Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/
 
#ifndef _ws_notification_TopicFilter_h_
#define _ws_notification_TopicFilter_h_

#include <string>

#include "ws/notification/Filter.h"
#include "ws/notification/topics/Topic.h"

namespace ws {
namespace notification {

/**
 *
 */
class TopicFilter: public Filter
{

	public:
	
	/*
	* Creates a new {@link TopicFilter} object.
	*
	* @param t 
	*/
	TopicFilter( Topic t )
	{
		#warning "TBD"
		/*
		String             topicexpr = "";
		java.util.Iterator it = t.getTopicPath(  ).iterator(  );
		while ( it.hasNext(  ) )
		{
			 String tt = (String) it.next(  );
			 topicexpr += ( "tns:" + tt );
			 if ( it.hasNext(  ) )
			 {
			    topicexpr += "/";
			 }
		}

		m_targetNamespace = t.getTopicNamespace(  ).getTargetNamespace(  ).toString(  );
		super.setURI( FilterFactory.topicsuri );
		super.setContent( parse( topicexpr ) );
		*/	
	}

	/*
	* Creates a new {@link TopicFilter} object.
	*
	* @param filter 
	*/
	TopicFilter( String filter )
	{
		#warning "TBD"
		/*
		super.setContent( parse( filter ) );
		super.setURI( FilterFactory.topicsuri );
		*/
	}

	/**
	* Creates a new {@link TopicFilter} object.
	*
	* @param content 
	* @param dialect 
	*/
	TopicFilter( Node content,
        	       URI  dialect )
	{
		#warning "TBD"
		/*
		super( content, dialect );
		*/
	}

	/**
	* DOCUMENT_ME
	*
	* @return DOCUMENT_ME
	*/
	String getNameSpace(  )
	{
		#warning "TBD"
		/*
		return m_targetNamespace;
		*/
	}

	Node parse( String s )
	{
		#warning "TBD"
		/*
		javax.xml.parsers.DocumentBuilderFactory dbf = javax.xml.parsers.DocumentBuilderFactory.newInstance(  );
		try
		{
		 javax.xml.parsers.DocumentBuilder db  = dbf.newDocumentBuilder(  );
		 org.w3c.dom.Document              dom = db.newDocument(  );
		 return ( dom.createTextNode( s ) );
		}
		catch ( javax.xml.parsers.ParserConfigurationException e )
		{
		 //TODO
		 e.printStackTrace(  );
		}

		return null;
		*/
	}
	
	protected:

	std::string m_targetNamespace;

};

}}

#endif
