//
//  File Name:
//
//      FNInterface.h
//
//  File Type:
//
//      C++ Header file
//
//  Author: EM
//
//  Version Number
//
//
//
 
#ifndef __FNInterface_h__
#define __FNInterface_h__

//  ------------------------------------------------------------------------ 
//  ------------------------  Include Header Files  ------------------------
//  ------------------------------------------------------------------------
 
// Include the following system header files:
 

  
// Include the following vxRU  header files:
 
#include "DAQBinding.h" 
 
//  ------------------------------------------------------------------------ 
//  --------------------  Public Constant Declarations  -------------------- 
//  ------------------------------------------------------------------------ 
 

//  ------------------------------------------------------------------------ 
//  ----------------------  Public Class Declarations  --------------------- 
//  ------------------------------------------------------------------------ 
//


// 
//
//  Class:
//
//     FN
//
//  Superclasses:
//
//     NONE 
//
//
//  Class Arguments
//
//
//  Description:
//
//	The interface for the Filter Node. From this file the
//	stubs  and skeletons have to be derived using an agreed protocol.
//	The FN requests the allocation of an event with a given profile and then 
//      tries to collect sets of fragments belonging to that event. 
//      Data messages containing a certain fragment set are accepted from the FU
	


class FNInterface
{

//
//  Member Functions:
//

//
//      This class defines the following public member functions:
//
//
public:

  // 
  // Upon an allocate request or a collect request from the filter nodes 
  // the FU delivers to the appropriate FN a data message containing the default or 
  // requested FragmentDataSet
  //
  virtual void take(EventIdentifier* eid, FragmentDataSet* fds) = 0;

};
  
#endif /* ifndef  __FN_h__ */

