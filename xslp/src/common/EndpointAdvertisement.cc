// $Id: EndpointAdvertisement.cc,v 1.2 2008/07/18 15:28:51 gutleber Exp $
/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber, G. Lo Presti and L. Orsini                      *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include "xslp/EndpointAdvertisement.h"


xslp::EndpointAdvertisement::EndpointAdvertisement (const std::string& source) throw (xslp::exception::Exception)
	:xslp::AdvertisementImpl("endpoint", source)
{

}

xslp::EndpointAdvertisement::~EndpointAdvertisement ()
{}

std::string xslp::EndpointAdvertisement::toString()
{
	std::string result = "service:";
	result += this->getType();
	result += ":";
	result += url_;
	return result;
}


