// $Id: AdvertisementImpl.cc,v 1.2 2008/07/18 15:28:51 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber, G. Lo Presti and L. Orsini                      *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include <sstream>
#include "xslp/AdvertisementImpl.h"
#include "toolbox/net/URL.h"

xslp::AdvertisementImpl::AdvertisementImpl (const std::string& type, const std::string & source) 
	throw (xslp::exception::Exception): type_(type)
{
	// Parse the following string: service:peer:<svc>/<name>
	// Example: service:peer:http://myhost.com:40000/urn:xdaq-application:lid=4

	std::string pattern = "service:";
	pattern += type_;
	pattern += ":";
	std::string::size_type protocolDelim = source.find(pattern);
	if (protocolDelim == std::string::npos)
	{
		std::string msg = "Incoming peer advertisement is lacking '";
		msg += pattern;
		msg += "' specifier for ";
		msg += source;
		XCEPT_RAISE (xslp::exception::Exception, msg);
	}
	
	try
	{
		url_ = source.substr (protocolDelim + pattern.size());
	}
	catch(toolbox::net::exception::MalformedURL & e)
	{
		XCEPT_RETHROW (xslp::exception::Exception, "invalid advertisement", e);
	}
}

xslp::AdvertisementImpl::~AdvertisementImpl()
{

}


std::string xslp::AdvertisementImpl::getType()
{
	return type_;
}


std::vector<std::string> xslp::AdvertisementImpl::getPropertyNames()
{
	std::map<std::string, std::string, std::less<std::string> >::iterator i;
	std::vector<std::string> result;
	
	for (i = properties_.begin(); i != properties_.end(); i++)
	{
		result.push_back((*i).first);
	}
	return result;
}

void xslp::AdvertisementImpl::setProperty (const std::string& name, const std::string& value)
{
	properties_[name] = value;
}

std::string xslp::AdvertisementImpl::getProperty(const std::string& name)
{
	return properties_[name];
}

std::string xslp::AdvertisementImpl::getURL()
{
	return url_;
}


