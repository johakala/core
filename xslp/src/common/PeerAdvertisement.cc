// $Id: PeerAdvertisement.cc,v 1.2 2008/07/18 15:28:51 gutleber Exp $
/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber, G. Lo Presti and L. Orsini                      *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/
#include <sstream>

#include "toolbox/net/URL.h"

#include "xslp/PeerAdvertisement.h"

xslp::PeerAdvertisement::PeerAdvertisement (const std::string& source) 
	throw (xslp::exception::Exception)
	:xslp::AdvertisementImpl("peer", source)
{}

xslp::PeerAdvertisement::~PeerAdvertisement ()
{}

std::string xslp::PeerAdvertisement::toString()
{
	std::string result = "service:";
	result += this->getType();
	result += ":";
	result += url_;
	return result;
}

