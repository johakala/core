// $Id: DiscoveryService.cc,v 1.3 2008/11/10 09:53:31 lorsini Exp $
/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber, G. Lo Presti and L. Orsini                      *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/
#include <sstream>
#include <stdexcept>

#include "toolbox/string.h"
#include "xcept/tools.h"
#include "xslp/DiscoveryService.h"
#include "xslp/PeerAdvertisement.h"
#include "xslp/PeerGroupAdvertisement.h"
#include "xslp/EndpointAdvertisement.h"

#include "slp.h"
//#include "libslp.h"

// The callback function that is activated when the Find finds something
/*
	ONLY THIS CALLBACK service:peer: urls
	other service  types need other callbacks
*/
SLPBoolean xslp::SLPFindCallback ( SLPHandle hslp,
				const char* srvurl,
				unsigned short lifetime,
				SLPError errcode,
				void* cookie)
{
	std::vector<xplore::Advertisement::Reference>* resultSet = static_cast<std::vector<xplore::Advertisement::Reference>*>(cookie);
	
	if (errcode == SLP_OK)
	{
		/*	
		SLP collates the incoming URLS in synchronous mode. It knows that the last
		has arrived if the timeout strikes. The list is growing by one entry at each
		callback - would be nice if the list could be retrieved at the end. Right
		now this is not possible, cause the header files are not put into the include
		directory. The SLPHandle is a void*
			
		 SLPHandleInfo * handle = (SLPHandleInfo *)hslp;
		 SLPSrvUrlCollatedItem * collateditem = (SLPSrvUrlCollatedItem *)handle->collatedsrvurls.head;
		 while (collateditem)
		 {
		 	std::cout << "Item: " << collateditem->srvurl << std::endl;
		        collateditem = (SLPSrvUrlCollatedItem *)collateditem->listitem.next;
		 }
		 */

		try
		{
			std::string source = srvurl;
			xplore::Advertisement::Reference a = xslp::DiscoveryService::createAdvertisementFromSource(source);
			resultSet->push_back(a);
		} 
		catch (xslp::exception::Exception& e)
		{
			// For the time being, print out the exception
			std::cout << xcept::stdformat_exception_history (e) << std::endl;
		}
	}
	else if (errcode == SLP_LAST_CALL)
	{
		// nothing
		return SLP_FALSE;
	}
	
	return SLP_TRUE; // return true cause we want to be called again if more services found
}

SLPBoolean xslp::SLPAttributesCallback ( SLPHandle hslp,
				const char* attrlist,
				SLPError errcode,
				void* cookie)
{
	toolbox::Properties* properties = static_cast<toolbox::Properties*>(cookie);
	
	if (errcode == SLP_OK)
	{
		try
		{
			/* attribute list is in format:
				(attr-id=attr-value-list),(attr-id=attr-value-list),...
			*/
			toolbox::StringTokenizer tokenizer(attrlist, "(");
			while (tokenizer.hasMoreTokens())
			{
				std::string name = tokenizer.nextToken("=");
				std::string value = tokenizer.nextToken(")");
				(void) tokenizer.filterNextToken(",");
				
				// std::cout << "Name: " << name << ", value: " << value << std::endl;
				
				properties->setProperty(name,value);
			}			
		} 
		catch (xslp::exception::Exception& e)
		{
			// For the time being, print out the exception
			std::cout << xcept::stdformat_exception_history (e) << std::endl;
		}
		
		// In synchronous mode, the attributes are collated and one they arrive we can return
		// and ask not to be called back anymore
		//
		return SLP_FALSE;
	}
	else if (errcode == SLP_LAST_CALL)
	{
		// nothing
		return SLP_FALSE;
	}
	
	return SLP_TRUE; // return true cause we want to be called again if more services found
}



// The callback function that is activated when the Register operation returns
void SLPRegisterCallback (SLPHandle hslp, SLPError errcode, void* cookie)
{
    // do nothing here
    if (errcode != SLP_OK)
    {
    	std::cout << "********************************* Error in registration: " << (int) errcode << std::endl;
    }
}

// The callback function that is activated when the Register operation returns
void SLPDeRegisterCallback (SLPHandle hslp, SLPError errcode, void* cookie)
{
    // do nothing here
    if (errcode != SLP_OK)
    {
    	std::cout << "Error in deregistration: " << (int) errcode << std::endl;
	
    }
    // return the error code
    *(SLPError*)cookie = errcode;
}


xslp::DiscoveryService::DiscoveryService ()
	 throw (xslp::exception::Exception) :mutex_(toolbox::BSem::FULL)
{
	// SLPError err = SLPOpen ("en", SLP_TRUE, &hslp_); // try to open in async mode
	SLPError err = SLPOpen ("en", SLP_FALSE, &hslp_); // try to open in sync mode
	if (err == SLP_NOT_IMPLEMENTED)
	{
		XCEPT_RAISE (xslp::exception::Exception, "Cannot created SLP handle. No support for asynchronous operation.");
	}
	if (err != SLP_OK)
	{
		XCEPT_RAISE (xslp::exception::Exception, "Cannot created SLP handle.");
	}
	
	// Set multicast timeout to 1 second: NOT IMPLEMENTEd IN OPENSLP
	// SLPSetProperty ("net.slp.multicastMaximumWait", "1000");
	// SLPSetProperty ("net.slp.unicastMaximumWait", "1000");
}

xslp::DiscoveryService::~DiscoveryService ()
{
	SLPClose(hslp_);
}

void 
xslp::DiscoveryService::search
(
	const std::string& advType, 
	const std::string& filter,
	std::vector<xplore::Advertisement::Reference>& resultSet) throw (xslp::exception::Exception)
{
	mutex_.take();
	
	// Launch an SLP search
	std::string service = "service:";
	service += advType;
	
	
	// RELY ON SYNCHRONOUS FUNCTION
	// Pass pointer to result set to SLP find, this find assumed to be blocking.
	// If it becomes asynchronous, some synchronization needs to be introduced
	//
		
	// std::string filter = "(group=*cluster*)";		
	SLPError err = SLPFindSrvs ( 	hslp_, 
					service.c_str(), 
					0, // use configured scopes
					filter.c_str(), // no attr filter
					SLPFindCallback,
					&resultSet
				   );
					
	// Catch synchronous errors
	if (err != SLP_OK)
	{
		std::stringstream msg ;
		msg << "search of advertisements for service '" << service << "' with filter '" << filter << "', error status " << err;
		mutex_.give();

		XCEPT_RAISE (xslp::exception::Exception, msg.str());
	}
	mutex_.give();
}

void 
xslp::DiscoveryService::retrieveProperties
(
	const std::string& service, toolbox::Properties& properties
) 
	throw (xslp::exception::Exception)
{
	mutex_.take();
	
	// RELY ON SYNCHRONOUS FUNCTION
	// Pass pointer to result set to SLP find, this find assumed to be blocking.
	// If it becomes asynchronous, some synchronization needs to be introduced
	//
		
	SLPError err = SLPFindAttrs ( 	hslp_, 
					service.c_str(), 
					0, // use configured scopes
					"",
					SLPAttributesCallback,
					&properties
				   );
					
	// Catch synchronous errors
	if (err != SLP_OK)
	{
		std::stringstream msg;
		msg << "failed to retrive attributes for service '" << service << "', error status " << err;
		mutex_.give();
		XCEPT_RAISE (xslp::exception::Exception, msg.str());
	}
	mutex_.give();
}


void 
xslp::DiscoveryService::publish(xplore::Advertisement::Reference adv) throw (xslp::exception::Exception)
{
	// "(attr1=val1),(attr2=val2)"

	mutex_.take();

	std::string service = adv->toString();
	std::stringstream attributes;
	
	std::vector<std::string> properties = adv->getPropertyNames();
        for (std::vector<std::string>::iterator j = properties.begin(); j != properties.end(); j++)
        {
		attributes << "(" << *j << "=" << adv->getProperty(*j) << "),";
        }
	attributes << "(domain=xdaq)";
		
	SLPError err = SLPReg (hslp_,
				service.c_str(),
				SLP_LIFETIME_MAXIMUM, // 5 seconds now, SLP_LIFETIME_MAXIMUM
				0, // always 0
				attributes.str().c_str(),
				SLP_TRUE, // always fresh, re-registration unsupported
				SLPRegisterCallback,
				this);

	// Catch synchronous errors
	if (err != SLP_OK)
	{
		std::stringstream msg;
		msg << "Publish of advertisement " << service << " failed with error " << err;
		mutex_.give();
		XCEPT_RAISE (xslp::exception::Exception, msg.str());
	}

	mutex_.give();
}

xplore::Advertisement::Reference xslp::DiscoveryService::createAdvertisementFromSource(const std::string& source)
throw (xslp::exception::Exception)
{
	xplore::Advertisement* a;
	
	// source is of form service:type:sap
	//

	std::string type;
	std::string sap;
	try
	{
		std::string::size_type firstColon = source.find(':');
		std::string::size_type secondColon = source.find(':', firstColon+1);
		type = source.substr(firstColon+1, secondColon - (firstColon+1));
		sap = source.substr(secondColon+1);
	}
	catch(std::out_of_range & e)
	{
		std::stringstream msg;
		msg << "Cannot interpret incoming advertisement. Malformed string: " << source;
		XCEPT_RAISE (xslp::exception::Exception, msg.str());
	}

	if (type == "peer")
	{
		try
		{
			a = new xslp::PeerAdvertisement(source);
		} 
		catch (xslp::exception::Exception& e)
		{
			XCEPT_RETHROW (xslp::exception::Exception, "Cannot interpret incoming peer advertisement",e);
		}
	}
	else if (type == "group")
	{
		try
		{
	 		a = new xslp::PeerGroupAdvertisement(source);
		} 
		catch (xslp::exception::Exception& e)
		{
			XCEPT_RETHROW (xslp::exception::Exception, "Cannot interpret incoming peer group advertisement",e);
		}
	} 
	else if (type == "endpoint")
	{
		try
		{
			a = new xslp::EndpointAdvertisement(source);
		} 
		catch (xslp::exception::Exception& e)
		{
			XCEPT_RETHROW (xslp::exception::Exception, "Cannot interpret incoming endpoint advertisement",e);
		}
	}
	 else
	{
		std::string msg = "Cannot interpret incoming advertisement. Type unknown: ";
		msg += type;
		XCEPT_RAISE (xslp::exception::Exception, msg);
	}
	return xplore::Advertisement::Reference(a);
}


// The callback function that is activated when the FindSrvTypes finds something
/*
SLPBoolean xslp::SLPFindTypesCallback ( SLPHandle hslp,
				const char* srvtypes,
				SLPError errcode,
				void* cookie)
{
	// srvtypes is a comma separated list: peer,peer-xdaq,peer-sentinel, ...

	xplore::DiscoveryEvent* e = (xplore::DiscoveryEvent*) cookie;
	xslp::DiscoveryService* s = (xslp::DiscoveryService*) e->getDiscoveryService();
	
	if (errcode == SLP_OK)
	{
		try
		{
			toolbox::StringTokenizer tokens(srvtypes,",");
			
			while (tokens.hasMoreTokens())
			{
				std::string token = tokens.nextToken();
				// Extract type-subtype between the two ":"
				std::string::size_type firstColon = token.find(':');
				std::string::size_type secondColon = token.find(':', firstColon+1);
				std::string type = token.substr(firstColon+1, secondColon - (firstColon+1));
				s->addAdvertisementType (type);
			}		
		} catch (xplore::exception::Exception& e)
		{
			// For the time being, print out the exception
			std::cout << xcept::stdformat_exception_history (e) << std::endl;
		}
	}
	else if (errcode == SLP_LAST_CALL)
	{
		xplore::DiscoveryListener* listener = e->getListener();
		
		// The discovery event will be auto deleted ad the end of the callback
		// because of the created reference.
		//
		if (listener != 0)
			listener->discoveryEvent(xplore::DiscoveryEvent::Reference(e));		
	}
	
	return SLP_TRUE; // return true cause we want to be called again if more services found
}
*/

// The callback function that is activated when the FindSrvTypes finds something
/*
SLPBoolean xslp::SLPFindTypesCallback ( SLPHandle hslp,
				const char* srvtypes,
				SLPError errcode,
				void* cookie)
{
	// srvtypes is a comma separated list: peer,peer-xdaq,peer-sentinel, ...

	xplore::DiscoveryEvent* e = (xplore::DiscoveryEvent*) cookie;
	xslp::DiscoveryService* s = (xslp::DiscoveryService*) e->getDiscoveryService();
	
	if (errcode == SLP_OK)
	{
		try
		{
			toolbox::StringTokenizer tokens(srvtypes,",");
			
			while (tokens.hasMoreTokens())
			{
				std::string token = tokens.nextToken();
				// Extract type-subtype between the two ":"
				std::string::size_type firstColon = token.find(':');
				std::string::size_type secondColon = token.find(':', firstColon+1);
				std::string type = token.substr(firstColon+1, secondColon - (firstColon+1));
				s->addAdvertisementType (type);
			}		
		} catch (xplore::exception::Exception& e)
		{
			// For the time being, print out the exception
			std::cout << xcept::stdformat_exception_history (e) << std::endl;
		}
	}
	else if (errcode == SLP_LAST_CALL)
	{
		xplore::DiscoveryListener* listener = e->getListener();
		
		// The discovery event will be auto deleted ad the end of the callback
		// because of the created reference.
		//
		if (listener != 0)
			listener->discoveryEvent(xplore::DiscoveryEvent::Reference(e));		
	}
	
	return SLP_TRUE; // return true cause we want to be called again if more services found
}
*/

/*
void 
xslp::DiscoveryService::removeAdvertisement(xplore::Advertisement::Reference adv)
throw (xplore::exception::Exception)
{
	std::map<std::string, xplore::Advertisement::Reference, std::less<std::string> >::iterator i;
	i = advs_.find(adv->getID());
	if (i != advs_.end())
	{
		// Check if the advertisement is local
		if (xplore::idIsLocal(adv->getID()))
		{
			// if local, unpublish first
			//std::cout << "Removing local advertismenet " << adv->getID() << std::endl;
			SLPError errcode;
			SLPError err = SLPDereg (hslp_,
					adv->toString().c_str(),
					SLPDeRegisterCallback,
					&errcode);

			// Catch synchronous errors
			if ((err != SLP_OK) || (errcode != SLP_OK))
			{
				std::string msg  = toolbox::toString("Deregistration of advertisement %s  failed with error %d",adv->toString().c_str(),err);
				XCEPT_RAISE (xplore::exception::Exception, msg);
			}
		} else
		{
			// std::cout << "Removing remote advertisement from cache " << adv->getID() << std::endl;
		}

		// remove from cache
		advs_.erase(i);
	} 
	else
	{
		// not found
		std::string msg = "Cannot flush advertisement with id ";
		msg += adv->getID();
		msg += ", advertisement not found";
		XCEPT_RAISE (xplore::exception::Exception, msg);
	}
}
*/

/*
void xslp::DiscoveryService::searchRemoteAdvertisementTypes() throw (xplore::exception::Exception)
{
	mutex_.take();


	// Launch an SLP search for service types
	xplore::DiscoveryEvent* e = new xplore::DiscoveryEvent(this, listener_);
	
	// clear all existing types in local vector
	advTypes_.clear();
	
	SLPError err = SLPFindSrvTypes (hslp_, 
					"", // IANA nameing authority, use "*" for all authorities
					"", // use configured scopes
					SLPFindTypesCallback,
					e );
					
	// Catch synchronous errors
	if (err != SLP_OK)
	{
		mutex_.give();

		XCEPT_RAISE (xplore::exception::Exception, "search of advertisement types failed");
		
	}
	
	mutex_.give();
}
*/
