// $Id: PeerGroupAdvertisement.cc,v 1.2 2008/07/18 15:28:51 gutleber Exp $
/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber, G. Lo Presti and L. Orsini                      *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include "xslp/PeerGroupAdvertisement.h"
	

xslp::PeerGroupAdvertisement::PeerGroupAdvertisement (const std::string& source) throw (xslp::exception::Exception)
	:xslp::AdvertisementImpl("group", source)
{
}

xslp::PeerGroupAdvertisement::~PeerGroupAdvertisement ()
{}

std::string xslp::PeerGroupAdvertisement::toString()
{
	std::string result = "service:group:";
	result += properties_["svc"];
	result += "/";
	result += properties_["name"];
	
	return result;
}
