// $Id: Publish2.cpp,v 1.2 2008/07/18 15:28:51 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber, G. Lo Presti and L. Orsini                      *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include <string>

#include "xslp/Platform.h"

/*! This is a simple test program to publishes various services.
    It then looks if they are properly stored.
    Finally it will remove them.
    This program uses the xslp classes directly.
 */

class SearchResult: public xplore::DiscoveryListener
{
	public:
	    void discoveryEvent(xplore::DiscoveryEvent::Reference e)
	    {
	    	std::vector<xplore::Advertisement::Reference>& advList = e->getResult();
		for (unsigned int i = 0; i < advList.size(); i++)
		{
			std::cout << "--- BEGIN OF ADVERTISEMENT ---" << std::endl;
			std::vector<std::string> properties = advList[i]->getPropertyNames();
			for (unsigned int j = 0; j < properties.size(); j++)
			{
				std::cout << properties[j] << " - " << advList[i]->getProperty(properties[j]);
				std::cout << std::endl;
			}
			std::cout << "--- END OF ADVERTISEMENT ---" << std::endl;
			
			
		}
	    }	
};

int main (int argc, char**argv)
{
	try
	{
		xplore::xslp::Platform* p = xplore::xslp::getPlatform();
		xplore::PeerGroup* g = p->getNetPeerGroup();
		xplore::DiscoveryService* ds = g->getDiscoveryService();

		xplore::Advertisement::Reference adv1 = p->createAdvertisement("Peer");
		adv1->setProperty("svc", "http://testmachine.org");
		adv1->setProperty("name", "TestApplication1");
		
		xplore::Advertisement::Reference adv2 = p->createAdvertisement("Peer");
		adv2->setProperty("svc", "http://testmachine.org");
		adv2->setProperty("name", "TestApplication2");
		
		xplore::Advertisement::Reference adv3 = p->createAdvertisement("Peer");
		adv3->setProperty("svc", "http://testmachine.org");
		adv3->setProperty("name", "TestApplication3");

		// Publish advertisement
		ds->publishAdvertisement(adv1);
		ds->publishAdvertisement(adv2);
		ds->publishAdvertisement(adv3);
		
		// See if they have been cached properly
		std::vector<xplore::Advertisement::Reference> a1;
		a1 = ds->getKnownAdvertisements("Peer");
		for (unsigned int i = 0; i < a1.size(); i++)
		{
			std::cout << "Cached before search: " << a1[i]->toString() << std::endl;
		}
		
		std::cout << std::endl;

		SearchResult listener;
		ds->addServiceListener(&listener);
		
		// Search for advertisement
		ds->searchRemoteAdvertisements("Peer");
		
		// See if they have been cached properly
		std::vector<xplore::Advertisement::Reference> a2;
		a2 = ds->getKnownAdvertisements("Peer");
		for (unsigned int i = 0; i < a2.size(); i++)
		{
			std::cout << "Cached after search: " << a2[i]->toString() << std::endl;
		}
		
		std::cout << std::endl;
		
		// Remove 2 advertisements
		ds->flushAdvertisement(a2[0]);
		ds->flushAdvertisement(a2[1]);
		
		// See if they have been cached properly
		std::vector<xplore::Advertisement::Reference> a3;
		a3 = ds->getKnownAdvertisements("Peer");
		for (unsigned int i = 0; i < a3.size(); i++)
		{
			std::cout << "Cached after flush: " << a3[i]->toString() << std::endl;
		}
		
		std::cout << std::endl;
		
		// Search again after removal
		ds->searchRemoteAdvertisements("Peer");
		
		xplore::xslp::destroyPlatform();
	} 
	catch (xplore::exception::Exception& e)
	{
		std::cout << "Caught: " << e.message() << std::endl;
	}
	return 0;
}
