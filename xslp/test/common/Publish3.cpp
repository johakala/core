// $Id: Publish3.cpp,v 1.2 2008/07/18 15:28:51 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber, G. Lo Presti and L. Orsini                      *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include <string>

#include "xcept/tools.h"
#include "xslp/Platform.h"
#include "xplore/Id.h"

/*! 2 Instances of this program shall be run on the same
    or on two different computers.
    Each program will publish 2 services.
    It will then aim to remove one that it created itself and
    one that has been created by the other program.
    Although the service URLs are always the same,
    the platform will be able to distinguish the locally and
    remotely created advertisements solely by their Ids.
    Only locally created ones will really be removed. The
    others are only removed from the cache and will be
    visible again if the program is restarted.
    This program uses the xslp classes directly.
 */

class SearchResult: public xplore::DiscoveryListener
{
	public:
	    void discoveryEvent(xplore::DiscoveryEvent::Reference e)
	    {
	    	std::vector<xplore::Advertisement::Reference>& advList = e->getResult();
		for (unsigned int i = 0; i < advList.size(); i++)
		{
			std::cout << "--- BEGIN OF ADVERTISEMENT ---" << std::endl;
			std::vector<std::string> properties = advList[i]->getPropertyNames();
			for (unsigned int j = 0; j < properties.size(); j++)
			{
				std::cout << properties[j] << " - " << advList[i]->getProperty(properties[j]);
				std::cout << std::endl;
			}
			std::cout << "--- END OF ADVERTISEMENT ---" << std::endl << std::endl;
			
			
		}
	    }	
};

int main (int argc, char**argv)
{
	try
	{
		xplore::xslp::Platform* p = xplore::xslp::getPlatform();
		xplore::PeerGroup* g = p->getNetPeerGroup();
		xplore::DiscoveryService* ds = g->getDiscoveryService();

		xplore::Advertisement::Reference adv1 = p->createAdvertisement("Peer");
		adv1->setProperty("svc", "http://testmachine.org");
		adv1->setProperty("name", "TestApplication1");
		
		xplore::Advertisement::Reference adv2 = p->createAdvertisement("Peer");
		adv2->setProperty("svc", "http://testmachine.org");
		adv2->setProperty("name", "TestApplication2");

		// Publish advertisement
		ds->publishAdvertisement(adv1);
		ds->publishAdvertisement(adv2);

		SearchResult listener;
		ds->addServiceListener(&listener);
		
		// Search for advertisement
		ds->searchRemoteAdvertisements("Peer");

		int choice;
		
		// Get all cached advertisements.
		// Select one local and one remote one and remove them
		std::vector<xplore::Advertisement::Reference> ads;
		
		do
		{
			ads = ds->getKnownAdvertisements("Peer");
			for (unsigned int i = 0; i < ads.size(); i++)
			{
				std::cout << "Advertisement: " << (i+1) << " --> " << ads[i]->toString() << " is local: ";
				std::cout << xplore::idIsLocal(ads[i]->getID()) << std::endl;
			}
		
			std::cout << std::endl << "Choose a number for flushing or 0 to end." << std::endl;
			std::cout << "-> ";

			std::cin >> choice;
			std::cout << std::endl;
			
			if ((choice > 0) && (choice <= (int) ads.size()))
			{
				try
				{
					ds->flushAdvertisement(ads[choice-1]);
				} catch (xplore::exception::Exception& e)
				{
					std::cout << std::endl;
					std::cout << xcept::stdformat_exception_history(e) << std::endl;
				}
			}			
		} while (choice != 0);
		
		xplore::xslp::destroyPlatform();
		return 0;
	} 
	catch (xplore::exception::Exception& e)
	{
		std::cout << "Caught: " << e.message() << std::endl;
	}
	return 0;
}
