// $Id: Publish1.cpp,v 1.2 2008/07/18 15:28:51 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber, G. Lo Presti and L. Orsini                      *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include <sstream>
#include <string>
#include <sys/types.h>
#include <unistd.h>
#include "xslp/DiscoveryService.h"

/*! This is a simple test program to publish a service and to look if
    it has been published correctly.
    this program uses the xslp classes directly.
 */

int main (int argc, char**argv)
{
	if (argc != 2)
	{
		std::cout << "Usage: " << argv[0] << " [number of advertisements] " << std::endl;
		return 1;
	}

	size_t count = std::atoi(argv[1]);

	try
	{
		xslp::DiscoveryService ds;
		
		for (size_t i = 0; i < count; ++i)
		{
			std::stringstream source;
			source << "service:peer:http://localhost:1234/service-" << getpid() << "-" << i;
			xplore::Advertisement::Reference adv = xslp::DiscoveryService::createAdvertisementFromSource(source.str());
			ds.publish (adv);
		}
		
		std::cout << "Press any key to flush the advertisement";

		getchar();
		
		std::cout << std::endl;
		std::cout << "Flush advertisement not yet implemented" << std::endl;		
	} 
	catch (xplore::exception::Exception& e)
	{
		std::cout << "Caught: " << e.message() << std::endl;
	}
	return 0;
}
