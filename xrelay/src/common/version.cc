// $Id: version.cc,v 1.2 2008/07/18 15:28:50 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xrelay/version.h"
#include "xoap/version.h"
#include "config/version.h"
#include "xcept/version.h"
#include "toolbox/version.h"
#include "xdata/version.h"
#include "xdaq/version.h"
#include "pt/version.h"

GETPACKAGEINFO(xrelay)

void xrelay::checkPackageDependencies() throw (config::PackageInfo::VersionException)
{
	CHECKDEPENDENCY(config);
	CHECKDEPENDENCY(xcept);
	CHECKDEPENDENCY(toolbox);
	CHECKDEPENDENCY(toolbox);   
	CHECKDEPENDENCY(xdaq);   
	CHECKDEPENDENCY(xoap);    
	CHECKDEPENDENCY(pt);     
}

std::set<std::string, std::less<std::string> > xrelay::getPackageDependencies()
{
	std::set<std::string, std::less<std::string> > dependencies;

	ADDDEPENDENCY(dependencies,config); 
	ADDDEPENDENCY(dependencies,xcept); 
	ADDDEPENDENCY(dependencies,toolbox); 
	ADDDEPENDENCY(dependencies,xdaq); 
 	ADDDEPENDENCY(dependencies,xoap);
	ADDDEPENDENCY(dependencies,pt);
	  
	return dependencies;
}	
	
