// $Id$

/*************************************************************************
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include <stdint.h>
#include "xrelay/Application.h"
#include "xcept/tools.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPHeader.h"
#include "xoap/domutils.h"
#include "pt/PeerTransportAgent.h"
#include "pt/SOAPMessenger.h"
#include "xercesc/dom/DOMNode.hpp"

#include "toolbox/task/WorkLoopFactory.h"
#include "toolbox/task/WaitingWorkLoop.h"
#include "toolbox/task/Action.h"
#include "toolbox/utils.h"

#include "xdaq/NamespaceURI.h"
#include "xoap/Method.h"

XDAQ_INSTANTIATOR_IMPL(xrelay::Application)


xrelay::Application::Application(xdaq::ApplicationStub* s)  throw (xdaq::exception::Exception)
	: xdaq::Application(s), xgi::framework::UIManager(this)
{
	maxRetries_ = 10; // fail after max number of enqueuing retries

        relayQueue_ = toolbox::rlist<xoap::MessageReference*>::create("xrelay-queue");


	s->getDescriptor()->setAttribute("icon", "/xrelay/images/xrelay-icon.png");
	s->getDescriptor()->setAttribute("service","xrelay");

	// Bind SOAP callback for any incoming message
	xoap::bind (this, &xrelay::Application::onMessage, "*", "*");
	xoap::bind (this, &xrelay::Application::onNotify, "Notify", "http://xdaq.web.cern.ch/xdaq/xsd/2004/XRelay-10");
	xoap::bind (this, &xrelay::Application::onTest, "Test", XDAQ_NS_URI);	
	processRelayJob_ = toolbox::task::bind (this, &xrelay::Application::processRelay, "processRelay");
	
	// Get a work loop
	workLoop_ = toolbox::task::getWorkLoopFactory()->getWorkLoop("xrelay", "waiting");
	workLoop_->activate();
}


xoap::MessageReference xrelay::Application::onTest 
(
	xoap::MessageReference msg
) 
	throw (xoap::exception::Exception)
{
	LOG4CPLUS_INFO (getApplicationLogger(), "received test message");
	
	//msg->writeTo(cout);
	//cout << endl;
	
	xoap::MessageReference reply = xoap::createMessage();
	xoap::SOAPEnvelope envelope = reply->getSOAPPart().getEnvelope();
	xoap::SOAPBody b = envelope.getBody();
	xoap::SOAPName responseName = envelope.createName("TestResponse", "xdaq", XDAQ_NS_URI);
	b.addBodyElement ( responseName );
	return reply;
}

xoap::MessageReference xrelay::Application::onNotify 
(
	xoap::MessageReference msg
) 
	throw (xoap::exception::Exception)
{
	//msg->writeTo(cout);
	//cout << endl;
	
	xoap::SOAPPart part = msg->getSOAPPart();
	xoap::SOAPEnvelope env = part.getEnvelope();
	xoap::SOAPBody body = env.getBody();
	DOMNode* node = body.getDOMNode();
	DOMNodeList* bodyList = node->getChildNodes();
	
	for (XMLSize_t i = 0; i < bodyList->getLength(); i++)
	{
		DOMNode* notifyNode = bodyList->item(i);
		if (
			(notifyNode->getNodeType() == DOMNode::ELEMENT_NODE) && 
			(xoap::XMLCh2String(notifyNode->getLocalName()) == "Notify")
		)
		{
			std::string url = xoap::getNodeAttribute(notifyNode, "url");
			DOMNodeList* notificationElementList = notifyNode->getChildNodes();
			for (XMLSize_t j = 0; j < notificationElementList->getLength(); j++)
			{
				DOMNode* notificationElementNode = notificationElementList->item(j);
				if (notificationElementNode->getNodeType() == DOMNode::ELEMENT_NODE)
				{
					std::string itemName = xoap::XMLCh2String(notificationElementNode->getLocalName());
					std::string msg = itemName;
					msg += " to url '";
					msg += xoap::getNodeAttribute(notificationElementNode, "url");
					msg += "', urn '";
					msg += xoap::getNodeAttribute(notificationElementNode, "urn");
					msg += "' via XRelay url '";
					msg += url;
					msg += "'";
					
					if (itemName == "delivered")
					{
						LOG4CPLUS_INFO (this->getApplicationLogger(), msg);
					} 
					else
					{
						LOG4CPLUS_ERROR (this->getApplicationLogger(), msg);
					}
				}
			}
		}
	}	
	
	xoap::MessageReference reply = xoap::createMessage();
	xoap::SOAPEnvelope envelope = reply->getSOAPPart().getEnvelope();
	xoap::SOAPBody b = envelope.getBody();
	xoap::SOAPName responseName = envelope.createName("NotifyResponse", "xr", "http://xdaq.web.cern.ch/xdaq/xsd/2004/XRelay-10");
	b.addBodyElement ( responseName );
	return reply;
}

xoap::MessageReference xrelay::Application::onMessage 
(
	xoap::MessageReference msg
) 
	throw (xoap::exception::Exception)
{
	// extract command name
	xoap::SOAPPart part = msg->getSOAPPart();
	xoap::SOAPEnvelope env = part.getEnvelope();
	xoap::SOAPBody body = env.getBody();
	DOMNode* node = body.getDOMNode();
	DOMNodeList* bodyList = node->getChildNodes();
	
	std::string namespaceURI = "";
	std::string namespacePrefix = "";
	std::string commandName = "";
	
	try
	{
		xoap::SOAPHeader header = env.getHeader();
		// as actor use the namespace
		std::vector<xoap::SOAPHeaderElement> elements = header.examineHeaderElements ("http://xdaq.web.cern.ch/xdaq/xsd/2004/XRelay-10");
		if ( elements.size() == 0 )
		{	
			// No Header elemnts for XRelay threfore ignore this message, return a NULL reference
			return xoap::MessageReference(0);
		}
	}
	catch(xoap::exception::Exception & e)
	{
		// No header therfore nothin to do return a NULL reference
		return xoap::MessageReference(0);
	}
		
	for (XMLSize_t i = 0; i < bodyList->getLength(); i++) 
	{
		DOMNode* command = bodyList->item(i);

		if (command->getNodeType() == DOMNode::ELEMENT_NODE)
		{
			namespaceURI = xoap::XMLCh2String (command->getNamespaceURI());

			if (namespaceURI == "")
			{
				LOG4CPLUS_ERROR (this->getApplicationLogger(), "Missing namespace in incoming SOAP message");
				XCEPT_RAISE (xoap::exception::Exception, "Missing namespace in incoming SOAP message");
			}
			
			namespacePrefix = xoap::XMLCh2String (command->getPrefix());
			commandName = xoap::XMLCh2String (command->getLocalName());	
		}
	}

	if ( commandName != "" ) 
	{
		// push the message in queue
		xoap::MessageReference *  relayMessage = new xoap::MessageReference(msg);

		size_t retry = 0;		
		while (retry < maxRetries_)
		{
			try
			{
				relayQueue_->push_back(relayMessage);
				break; // break out of loop, go on with processing
			}
			catch (toolbox::exception::RingListFull& rlf)
			{
				++retry;
				LOG4CPLUS_WARN (this->getApplicationLogger(), "Failed to enqueue request, retry " << retry);
				toolbox::u_sleep(100);
			}
		}

		if (retry == maxRetries_)
		{
			std::stringstream msg;
			msg << "Failed to enqueue request, giving up after retry " << retry;
			LOG4CPLUS_ERROR (this->getApplicationLogger(), msg.str() );					
			// Should return a SOAP fault
			XCEPT_RAISE (xoap::exception::Exception, msg.str() );
		}

		// submit a job
		xoap::MessageReference reply = xoap::createMessage();
		
		try
		{
			workLoop_->submit(processRelayJob_);			
			// send the SOAP message to all URIs and collect the responses
			xoap::SOAPEnvelope envelope = reply->getSOAPPart().getEnvelope();
			xoap::SOAPBody b = envelope.getBody();
			xoap::SOAPName responseName = envelope.createName(commandName + "Response", namespacePrefix, namespaceURI);
			b.addBodyElement ( responseName );
		}
		catch (toolbox::task::exception::Exception& e)
		{			
			LOG4CPLUS_ERROR (getApplicationLogger(), xcept::stdformat_exception_history(e));		
			XCEPT_RETHROW(xoap::exception::Exception, "Failed to submit processing job", e);
		}
		catch (std::exception& se)
		{
			LOG4CPLUS_FATAL (getApplicationLogger(), "Caught standard exception, " << se.what());
			XCEPT_RAISE(xoap::exception::Exception, "Failed to submit processing job, standard exception raised");
		}	
		catch (...)
		{
			LOG4CPLUS_FATAL (getApplicationLogger(), "Caught unknown exception");
			XCEPT_RAISE(xoap::exception::Exception, "Failed to submit processing job, unknown raised");
		}

		return reply;
	}
	else
	{
		LOG4CPLUS_ERROR (getApplicationLogger(), "Missing command in incoming SOAP message");
		XCEPT_RAISE (xoap::exception::Exception, "Missing command in incoming SOAP message");
	}	
}

bool  xrelay::Application::processRelay(toolbox::task::WorkLoop* wl)
{
	// pop message from queue
	xoap::MessageReference*  relayMessage = relayQueue_->front();
	xoap::MessageReference msg = *relayMessage;
	
	// Delete pointer, then remove from queue
	delete relayMessage;
	relayQueue_->pop_front();

	LOG4CPLUS_INFO (this->getApplicationLogger(), "processing SOAP '<relay>' header");	
		
	//cout << " ----> Received Relay" << endl;	
	//msg->writeTo(cout); 
	//cout << endl << "---- " << endl;
	
	// Find the <xrelay> tag in the header of the SOAP message
	// within the defined namespace http://xdaq.web.cern.ch/xdaq/xsd/2004/XRelay-10
	xoap::SOAPEnvelope env = msg->getSOAPPart().getEnvelope();
	xoap::SOAPHeader header = env.getHeader();

	// as actor use the namespace
	std::vector<xoap::SOAPHeaderElement> xrelayHeader = header.extractHeaderElements ("http://xdaq.web.cern.ch/xdaq/xsd/2004/XRelay-10");
	
	// there shall be only a single xrelay header tag.
	if (xrelayHeader.size() != 1)
	{
		std::stringstream msg;
		msg << "Exactly one '<relay>' header tag is allowed. Number of '<relay>' headers in message is ";
		msg << xrelayHeader.size();
		LOG4CPLUS_ERROR (this->getApplicationLogger(), msg.str());
		return false;
	}
	
	DOMNode * relayNode = xrelayHeader[0].getDOM();
	std::string prefix = xoap::XMLCh2String(relayNode->getPrefix());
	DOMNodeList* toNodes = relayNode->getChildNodes();
	DOMNode * fromNode = this->extractFromNode(relayNode);
	std::vector<std::string> uris; // Vector to store all the destination URIs
	
	// Check if reply is requested for this relay message
	xoap::MessageReference replyMessage = xoap::createMessage();
	
	// prepare command
	xoap::SOAPEnvelope replyEnvelope =  replyMessage->getSOAPPart().getEnvelope();
	xoap::SOAPBody replyBody = replyEnvelope.getBody();
	xoap::SOAPName commandName = replyEnvelope.createName("Notify",prefix,"http://xdaq.web.cern.ch/xdaq/xsd/2004/XRelay-10");
	
	xoap::SOAPName notifyUrlName = replyEnvelope.createName("url");
	xoap::SOAPElement commandElement = replyBody.addBodyElement(commandName);
	commandElement.addAttribute( notifyUrlName, getApplicationContext()->getContextDescriptor()->getURL() );
	
	for (XMLSize_t i = 0; i < toNodes->getLength(); i++)
	{
		DOMNode* n = toNodes->item(i);
		// check node of type <xr:to url="..." urn="..."/>
		if ((n->getNodeType() == DOMNode::ELEMENT_NODE) && (xoap::XMLCh2String(n->getLocalName()) == "to"))
		{
			// Extract URI and put it into a vector
			std::string url = xoap::getNodeAttribute(n,"url");
			std::string urn = xoap::getNodeAttribute(n,"urn");
			
			if ((url == "") || (urn == ""))
			{
				LOG4CPLUS_ERROR (getApplicationLogger(), "Missing URL or URN attribute in '<to>' tag of '<relay>' SOAP header element");
				continue;
			}
		
			try
			{
				if ( this->isRoutingNode(n) )
				{
					LOG4CPLUS_INFO (getApplicationLogger(), "send '<relay>' command to " << url << " " << urn);	
					this->processRoute(msg, n, fromNode, url, urn, prefix);
				}
				else
				{
					LOG4CPLUS_INFO (getApplicationLogger(), "send direct message to " << url << " " << urn);
					this->sendMessage(msg, url, urn);
				}
				// succeded append to command <xr::delivered url="" urn="" />
				//....
				// prepare the to route back Node
				xoap::SOAPName deliveredName = replyEnvelope.createName("delivered", prefix, "http://xdaq.web.cern.ch/xdaq/xsd/2004/XRelay-10");
				xoap::SOAPElement deliveredElement = commandElement.addChildElement( deliveredName );	
				xoap::SOAPName urlName = replyEnvelope.createName("url");
				xoap::SOAPName urnName = replyEnvelope.createName("urn");
				deliveredElement.addAttribute( urlName, url );
				deliveredElement.addAttribute( urnName, urn ); 				
			}
			catch (xdaq::exception::Exception & e)
			{
				// failed append to command <xr::failed url="" urn="" />
				xoap::SOAPName failedName = replyEnvelope.createName("failed", prefix, "http://xdaq.web.cern.ch/xdaq/xsd/2004/XRelay-10");
				xoap::SOAPElement failedElement = commandElement.addChildElement( failedName );	
				xoap::SOAPName urlName = replyEnvelope.createName("url");
				xoap::SOAPName urnName = replyEnvelope.createName("urn");
				failedElement.addAttribute( urlName, url );
				failedElement.addAttribute( urnName, urn ); 
			}
		}
	}
	
	// check if requested reply	
	std::string replyAttribute = xoap::getNodeAttribute(relayNode,"notify");
	
	if ( replyAttribute == "true" )
	{
		if ( fromNode == 0 )
		{
			LOG4CPLUS_ERROR (getApplicationLogger(), "cannot notify, missing '<from>' tag in '<relay>' command");
			return false;
		}
	
		try
		{
			this->setReplyRoute(replyMessage, fromNode);
			// extract first 'to' node
			DOMNode * toNode = this->extractToNode(fromNode);
			if ( toNode != 0 ) 
			{
				// Extract URI and put it into a vector
				std::string url = xoap::getNodeAttribute(toNode,"url");
				std::string urn = xoap::getNodeAttribute(toNode,"urn");
				
				LOG4CPLUS_INFO (this->getApplicationLogger(), "send '<Notify>' message to " << url << " " << urn);	
				
				//replyMessage->writeTo(cout);
				//cout << endl;
				this->sendMessage(replyMessage, url, urn);
			}
			else
			{
				LOG4CPLUS_ERROR (getApplicationLogger(), "cannot send '<Notify>', missing '<to>' node in '<from>' tag");
			}
		}
		catch (xdaq::exception::Exception & e)
		{
			LOG4CPLUS_ERROR (getApplicationLogger(), xcept::stdformat_exception_history(e));	
		}	
	}
	
	return false;
}


void xrelay::Application::setReplyRoute
(
	xoap::MessageReference msg, 
	DOMNode * fromNode 
) 
	throw (xdaq::exception::Exception)
{
	// extract first 'to' node
	DOMNode * toNode =  this->extractToNode(fromNode);
	if ( toNode == 0 )
	{
		XCEPT_RAISE(xdaq::exception::Exception, "cannot create '<Notify>' message, missing '<to>' node in '<from>' tag" );
	}
	
	xoap::SOAPEnvelope envelope =  msg->getSOAPPart().getEnvelope();
	xoap::SOAPName envelopeName = envelope.getElementName();
	// unfold route stack, it becomes <relay> in this message
	DOMNode * routeNode =  this->extractToNode(toNode);
	if ( routeNode != 0 ) 
	{	
		std::string prefix = xoap::XMLCh2String(fromNode->getPrefix());
		// add SOAP header
		xoap::SOAPHeader header = envelope.addHeader();
		xoap::SOAPName relayName = envelope.createName("relay", prefix, "http://xdaq.web.cern.ch/xdaq/xsd/2004/XRelay-10");
		xoap::SOAPHeaderElement relayElement = header.addHeaderElement( relayName );
	
		// Add the actor attribute
		xoap::SOAPName actorName = envelope.createName("actor", envelopeName.getPrefix(), envelopeName.getURI());
		relayElement.addAttribute( actorName, "http://xdaq.web.cern.ch/xdaq/xsd/2004/XRelay-10");

		// append all nodes to a <relay> element
		DOMNode * relayNode = relayElement.getDOM();
		DOMNode * importedRouteNode = relayNode->getOwnerDocument()->importNode(routeNode,true);
		relayNode->appendChild(importedRouteNode);
	}
}


DOMNode * xrelay::Application::extractFromNode(DOMNode * relayNode)
{
	DOMNodeList* toNodes = relayNode->getChildNodes();
	for (XMLSize_t i = 0; i < toNodes->getLength(); i++)
	{
		DOMNode* n = toNodes->item(i);
		// check node of type <xr:to url="..." urn="..."/>
		if ((n->getNodeType() == DOMNode::ELEMENT_NODE) && (xoap::XMLCh2String(n->getLocalName()) == "from"))
		{
			return n;
		}
	}
	return 0;
}

DOMNode * xrelay::Application::extractToNode(DOMNode * node)
{
	DOMNodeList* toNodes = node->getChildNodes();
	for (XMLSize_t i = 0; i < toNodes->getLength(); i++)
	{
		DOMNode* n = toNodes->item(i);
		// check node of type <xr:to url="..." urn="..."/>
		if ((n->getNodeType() == DOMNode::ELEMENT_NODE) && (xoap::XMLCh2String(n->getLocalName()) == "to"))
		{
			return n;
		}
	}
	return 0;
}


bool xrelay::Application::isRoutingNode(DOMNode * toNode)
{
	// check that this node has children named to
	DOMNodeList* toNodes = toNode->getChildNodes();
	for (XMLSize_t i = 0; i < toNodes->getLength(); i++)
	{
		DOMNode* n = toNodes->item(i);
		// check node of type <xr:to url="..." urn="..."/>
		if ((n->getNodeType() == DOMNode::ELEMENT_NODE) && (xoap::XMLCh2String(n->getLocalName()) == "to"))
		{
			return true;
		}
	}
	return false;
}


void xrelay::Application::processRoute
(
	xoap::MessageReference message, 
	DOMNode* toNode, 
	DOMNode * fromNode, 
	const std::string & url, 
	const std::string & urn, 
	const std::string & prefix
) 
	throw (xdaq::exception::Exception)
{
	DOMNodeList* toNodes = toNode->getChildNodes();
	
	// Check if reply is requested for this relay message
	std::string replyAttribute = xoap::getNodeAttribute(toNode,"notify");
	
	xoap::MessageReference msg = xoap::createMessage(message);

	xoap::SOAPEnvelope envelope = msg->getSOAPPart().getEnvelope();
	xoap::SOAPName envelopeName = envelope.getElementName();
	
	// add <relay> SOAP header
	xoap::SOAPHeader header = envelope.addHeader();
	xoap::SOAPName relayName = envelope.createName("relay", prefix, "http://xdaq.web.cern.ch/xdaq/xsd/2004/XRelay-10");
	xoap::SOAPHeaderElement relayElement = header.addHeaderElement( relayName );
	
	if (replyAttribute == "true")
	{
		// Add the actor attribute
		xoap::SOAPName replyAttributeName = envelope.createName("notify");
		relayElement.addAttribute( replyAttributeName, "true");
	}
	
	// Add the actor attribute
	xoap::SOAPName actorName = envelope.createName("actor", envelopeName.getPrefix(), envelopeName.getURI());
	relayElement.addAttribute( actorName, "http://xdaq.web.cern.ch/xdaq/xsd/2004/XRelay-10");

	// append all nodes to a <relay> element
	DOMNode * relayNode = relayElement.getDOM();
	
	for (XMLSize_t i = 0; i < toNodes->getLength(); i++)
	{
		DOMNode* n = toNodes->item(i);
		// check node of type <xr:to url="..." urn="..."/>
		if ((n->getNodeType() == DOMNode::ELEMENT_NODE) && (xoap::XMLCh2String(n->getLocalName()) == "to"))
		{
			DOMNode * importedNode = relayNode->getOwnerDocument()->importNode(n, true);
			relayNode->appendChild(importedNode);
		}
	}
	
	// prepare the from Node
	xoap::SOAPName fromName = envelope.createName("from", prefix, "http://xdaq.web.cern.ch/xdaq/xsd/2004/XRelay-10");
	xoap::SOAPElement fromElement = relayElement.addChildElement( fromName );	
	
	// prepare the to route back Node
	xoap::SOAPName toName = envelope.createName("to", prefix, "http://xdaq.web.cern.ch/xdaq/xsd/2004/XRelay-10");
	xoap::SOAPElement toElement = fromElement.addChildElement( toName );	
	xoap::SOAPName urlName = envelope.createName("url");
	xoap::SOAPName urnName = envelope.createName("urn");
	toElement.addAttribute( urlName, getApplicationContext()->getContextDescriptor()->getURL() );
	toElement.addAttribute( urnName, "urn:xdaq-application:service=xrelay" ); // always the Xrelay URN
	
	if ( fromNode != 0 ) 
	{
		// stack up  'to' elements of  previous 'from' node
		DOMNode * toNewNode = toElement.getDOM();
		if ( fromNode->hasChildNodes() ) 
		{
			DOMNode * importedToNode = toNewNode->getOwnerDocument()->importNode(this->extractToNode(fromNode), true);
			toNewNode->appendChild(importedToNode);
		}	
	}
	
	this->sendMessage(msg, url, urn);
}

void xrelay::Application::sendMessage
(
	xoap::MessageReference msg, 
	const std::string & url, 
	const std::string & urn
) 
	throw (xdaq::exception::Exception)
{
	pt::PeerTransportAgent* pta = pt::getPeerTransportAgent();
	
	try
	{
		// Currently the addresses are created, but not deleted.
		// Either the messenger has to take care of that or the addresses need to be reference counted
		pt::Address::Reference destAddress = pta->createAddress(url, "soap");
		pt::Address::Reference localAddress =
			pt::getPeerTransportAgent()->createAddress(getApplicationContext()->getContextDescriptor()->getURL(), "soap");

		// These two lines cannot be merges, since a reference that is a temporary object
		// would delete the contained object pointer immediately after use.
		//
		pt::Messenger::Reference mr = pta->getMessenger(destAddress,localAddress);
		pt::SOAPMessenger& m = dynamic_cast<pt::SOAPMessenger&>(*mr);

		// fill the SOAPAction field
		msg->getMimeHeaders()->setHeader("SOAPAction", urn);
		
		// If the original message has this defined, then we need to set it to the new intended destination
		if (msg->getMimeHeaders()->getHeader("request_uri").size() > 0)
		{
			msg->getMimeHeaders()->setHeader("request_uri", urn);
		}

		/*
		std::cout << "sending soap message to " << url << "/" << urn << ":" << std::endl;
		std::string themsg;
		msg->writeTo (themsg);
		std::cout << themsg << std::endl;
		*/

		xoap::MessageReference r = m.send (msg);
		
		xoap::SOAPBody rb = r->getSOAPPart().getEnvelope().getBody();
		if (rb.hasFault())
		{
			LOG4CPLUS_ERROR (getApplicationLogger(), rb.getFault().getFaultString());
		}
	}
	catch (pt::exception::Exception& pte)
	{	
		XCEPT_RETHROW(xdaq::exception::Exception, "failed to relay message", pte);
	}	
}
