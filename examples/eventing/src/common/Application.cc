// $Id: Application.cc,v 1.3 2008/07/18 15:26:40 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "benchmark/mstreamioeventing/Client.h"
#include "benchmark/mstreamioeventing/Server.h"
#include "benchmark/mstreamioeventing/DS.h"
#include "benchmark/mstreamioeventing/DR.h"
#include "benchmark/mstreamioeventing/DP.h"
#include "benchmark/mstreamioeventing/DPBus.h"
#include "benchmark/mstreamioeventing/DSBus.h"
#include "benchmark/mstreamioeventing/DRBus.h"

XDAQ_INSTANTIATOR_IMPL(Server)
XDAQ_INSTANTIATOR_IMPL(Client)
XDAQ_INSTANTIATOR_IMPL(DS)
XDAQ_INSTANTIATOR_IMPL(DR)
XDAQ_INSTANTIATOR_IMPL(DP)
XDAQ_INSTANTIATOR_IMPL(DPBus)
XDAQ_INSTANTIATOR_IMPL(DSBus)
XDAQ_INSTANTIATOR_IMPL(DRBus)
