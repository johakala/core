// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.			                			 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, A. Forrest										 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                        	 *
 * For the list of contributors see CREDITS.   			        		 *
 *************************************************************************/

#include "examples/eventing/Subscriber.h"

#include "xdaq/Application.h"
#include "xdaq/ApplicationContext.h"
#include "xdaq/ApplicationGroup.h"
#include "toolbox/PerformanceMeter.h"
#include "xdaq/NamespaceURI.h"

#include "toolbox/BSem.h"

#include "toolbox/rlist.h"
#include "toolbox/string.h"
#include "toolbox/mem/Reference.h"
#include "toolbox/mem/Pool.h"
#include "toolbox/task/TimerTask.h"
#include "toolbox/task/Timer.h"
#include "toolbox/task/TimerFactory.h"

#include "xdata/UnsignedLong.h"
#include "xdata/Double.h"
#include "xdata/Boolean.h"
#include "xdata/ActionListener.h"

#include "xoap/MessageFactory.h"
#include "xoap/MessageReference.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPName.h"
#include "xoap/SOAPBody.h"
#include "xoap/Method.h"

#include "xcept/Exception.h"
#include "xcept/tools.h"

#include "xgi/framework/Method.h"

#include "b2in/nub/Method.h"

XDAQ_INSTANTIATOR_IMPL (Subscriber);

Subscriber::Subscriber (xdaq::ApplicationStub* c) throw (xdaq::exception::Exception)
	: xdaq::Application(c), xgi::framework::UIManager(this), eventing::api::Member(this), subMutex_(toolbox::BSem::FULL)
{
	counter_ = 0;
	eventingBusName_ = "eventing1";

	getApplicationInfoSpace()->fireItemAvailable("counter", &counter_);
	getApplicationInfoSpace()->fireItemAvailable("eventingBusName", &eventingBusName_);

	b2in::nub::bind(this, &Subscriber::onMessage);

	xgi::framework::deferredbind(this, this, &Subscriber::Default, "Default");

	this->getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");
}

void Subscriber::actionPerformed (xdata::Event& event)
{
	if (event.type() == "urn:xdaq-event:setDefaultValues")
	{
		try
		{
			this->getEventingBus(eventingBusName_).subscribe("test");
		}
		catch (eventing::api::exception::Exception & e)
		{
			std::cout << "Failed to subscribe - " << xcept::stdformat_exception(e) << std::endl;
		}
	}
}

// Callback function invoked for the b2in token message
// that is received from the Clients
//
void Subscriber::onMessage (toolbox::mem::Reference * ref, xdata::Properties & plist) throw (b2in::nub::exception::Exception)
{
	if (ref != 0)
	{
		ref->release();
	}

	std::string action = plist.getProperty("urn:b2in-eventing:action");
	if (action != "notify")
	{
		//std::cout << "received control message << std::endl;
		return;
	}

	subMutex_.take();
	counter_ = counter_ + 1;
	subMutex_.give();
	
	LOG4CPLUS_DEBUG(getApplicationLogger(), toolbox::toString("received message number %d%s%s", (xdata::UnsignedLongT) counter_, " from ", plist.getProperty("sender").c_str()));
}

//
void Subscriber::Default (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	*out << "<div class=\"xdaq-tab-wrapper\">";
        *out << "<div class=\"xdaq-tab\" title=\"Statistics\">";
	*out << "Messages received : " << counter_ << std::endl;
        *out << "</div>";
        *out << "<div class=\"xdaq-tab\" title=\"Eventing\">";
        
        *out << "<h3>Buses</h3>";
        
        *out << this->busesToHTML() << std::endl;
        
        *out << "</div>";
        *out << "</div>";
}
