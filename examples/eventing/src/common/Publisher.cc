// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.			                			 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, A. Forrest										 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                        	 *
 * For the list of contributors see CREDITS.   			        		 *
 *************************************************************************/

#include "examples/eventing/Publisher.h"

#include "xgi/framework/Method.h"
#include "xgi/Method.h"

XDAQ_INSTANTIATOR_IMPL (Publisher);

Publisher::Publisher (xdaq::ApplicationStub* c) throw (xdaq::exception::Exception)
	: xdaq::Application(c), xgi::framework::UIManager(this), eventing::api::Member(this)
{
	eventingBusName_ = "eventing1";
	topicName_ = "test";
	this->getApplicationInfoSpace()->fireItemAvailable("eventingBusName", &eventingBusName_);
	this->getApplicationInfoSpace()->fireItemAvailable("topicName", &topicName_);

	xgi::framework::deferredbind(this, this, &Publisher::Default, "Default");
	xgi::bind(this, &Publisher::sendNext, "sendNext");
	
	this->getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");
}

void Publisher::actionPerformed (toolbox::Event& event)
{
	if (event.type() == "eventing::api::BusReadyToPublish")
	{
		std::string busname = (static_cast<eventing::api::Bus*>(event.originator()))->getBusName();
		std::cout << "event Bus '" << busname << "' is ready to publish"  << std::endl;
	}
}

void Publisher::actionPerformed (xdata::Event& event)
{
	if (event.type() == "urn:xdaq-event:setDefaultValues")
	{
		// User should listen for publsh readiness, but should also check immediately
		this->getEventingBus(eventingBusName_).addActionListener(this);
		if (this->getEventingBus(eventingBusName_).canPublish())
		{
			// ready now
			std::cout << "Eventing bus is ready at setDefaultValues" << std::endl;
		}
	}
}

void Publisher::sendNext (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	// create fake message with fake property
	xdata::Properties plist;
	plist.setProperty("counter", "0");

	this->getEventingBus(eventingBusName_).publish(topicName_, 0, plist);
	
	std::cout << "message sent" << std::endl;

	out->getHTTPResponseHeader().addHeader("Content-Type", "text/plain");
}

void Publisher::Default (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	*out << "<div class=\"xdaq-tab-wrapper\">";
	*out << "<div class=\"xdaq-tab\" title=\"Control\">";
	std::string triggerURL;
	triggerURL = "/";
	triggerURL += getApplicationDescriptor()->getURN();
	triggerURL += "/sendNext";
	*out << "<a onclick=\"xdaqAJAX({url: '" << triggerURL << "'});\" href=\"#\">" << "Send message" << "</a>" << std::endl;

	*out << "<br/><br/><hr/><br/>" << std::endl;

	// test eventing table

	*out << "</div>";
	*out << "<div class=\"xdaq-tab\" title=\"Eventing\">";

	*out << "<h3>Buses</h3>";

	*out << this->busesToHTML() << std::endl;

	*out << "</div>";
	*out << "</div>";
}


