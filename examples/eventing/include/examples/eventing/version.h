// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

//
// Version definition for MStreamIO
//
#ifndef _eventingexample_Version_h_
#define _eventingexample_Version_h_

#include "config/PackageInfo.h"
// !!! Edit this line to reflect the latest package version !!!

#define EVENTINGEXAMPLE_VERSION_MAJOR 1
#define EVENTINGEXAMPLE_VERSION_MINOR 0
#define EVENTINGEXAMPLE_VERSION_PATCH 0
// If any previous versions available E.g. #define EVENTINGEXAMPLE_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#undef EVENTINGEXAMPLE_PREVIOUS_VERSIONS


//
// Template macros
//
#define EVENTINGEXAMPLE_VERSION_CODE PACKAGE_VERSION_CODE(EVENTINGEXAMPLE_VERSION_MAJOR,EVENTINGEXAMPLE_VERSION_MINOR,EVENTINGEXAMPLE_VERSION_PATCH)
#ifndef EVENTINGEXAMPLE_PREVIOUS_VERSIONS
#define EVENTINGEXAMPLE_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(EVENTINGEXAMPLE_VERSION_MAJOR,EVENTINGEXAMPLE_VERSION_MINOR,EVENTINGEXAMPLE_VERSION_PATCH)
#else 
#define EVENTINGEXAMPLE_FULL_VERSION_LIST  EVENTINGEXAMPLE_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(EVENTINGEXAMPLE_VERSION_MAJOR,EVENTINGEXAMPLE_VERSION_MINOR,EVENTINGEXAMPLE_VERSION_PATCH)
#endif 

namespace eventingexample
{
	const std::string package  =  "eventingexample";
	const std::string versions =  EVENTINGEXAMPLE_FULL_VERSION_LIST;
	const std::string summary = "Example for using the eventingexample package";
	const std::string description = "";
	const std::string authors = "Luciano Orsini, Andrea Petrucci, Andy Forrest";
	const std::string link = "http://xdaqwiki.cern.ch/index.php/Power_Pack";
	config::PackageInfo getPackageInfo();
	void checkPackageDependencies() throw (config::PackageInfo::VersionException);
	std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
