// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.			                			 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, A. Forrest										 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                        	 *
 * For the list of contributors see CREDITS.   			        		 *
 *************************************************************************/

#ifndef _examples_eventing_subscriber_h_
#define _examples_eventing_subscriber_h_

#include "xdaq/Application.h"
#include "xdata/ActionListener.h"

#include "xdata/UnsignedLong.h"
#include "xdata/String.h"

#include "xgi/Input.h"
#include "xgi/Output.h"

#include "xgi/framework/UIManager.h"

#include "eventing/api/Member.h"
#include "b2in/nub/exception/Exception.h"

class Subscriber : public xdaq::Application, public xgi::framework::UIManager, public eventing::api::Member, public xdata::ActionListener
{
	public:
		XDAQ_INSTANTIATOR();

	 	Subscriber (xdaq::ApplicationStub* c) throw (xdaq::exception::Exception);

		void onMessage (toolbox::mem::Reference * ref, xdata::Properties & plist) throw (b2in::nub::exception::Exception);

		void Default (xgi::Input *in, xgi::Output *out) throw (xgi::exception::Exception);

	protected:

		void actionPerformed (xdata::Event& event);
 
		toolbox::BSem subMutex_;
		
		xdata::UnsignedLong counter_;
		xdata::String eventingBusName_;
};

#endif
