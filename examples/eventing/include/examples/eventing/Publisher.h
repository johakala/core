// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.			                			 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, A. Forrest										 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                        	 *
 * For the list of contributors see CREDITS.   			        		 *
 *************************************************************************/

#ifndef _examples_eventing_publisher_h_
#define _examples_eventing_publisher_h_

#include "toolbox/ActionListener.h"
#include "xdaq/Application.h"
#include "xgi/framework/UIManager.h"
#include "xdata/String.h"
#include "xdata/ActionListener.h"

#include "xgi/Input.h"
#include "xgi/Output.h"

#include "eventing/api/Member.h"

class Publisher : public xdaq::Application, public xgi::framework::UIManager, public eventing::api::Member, public toolbox::ActionListener, public xdata::ActionListener
{
	public:
		XDAQ_INSTANTIATOR();

		Publisher (xdaq::ApplicationStub* c) throw (xdaq::exception::Exception);

		void Default (xgi::Input *in, xgi::Output *out) throw (xgi::exception::Exception);
		void sendNext(xgi::Input *in, xgi::Output *out) throw (xgi::exception::Exception);

	protected:

		void actionPerformed (toolbox::Event& e);
		void actionPerformed (xdata::Event& e);

		xdata::String eventingBusName_;
		xdata::String topicName_;

};

#endif
