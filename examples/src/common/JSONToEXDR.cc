// $Id: JSONToEXDR.cc,v 1.2 2008/07/18 15:26:45 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/
#include <iostream>
#include <sstream>
 
#include "toolbox/TimeVal.h"
#include "xcept/tools.h"
#include "xdata/Table.h"
#include "xdata/UnsignedLong.h"
#include "xdata/UnsignedInteger.h"
#include "xdata/UnsignedInteger64.h"
#include "xdata/String.h"
#include "xdata/Double.h"
#include "xdata/exdr/FixedSizeInputStreamBuffer.h"
#include "xdata/exdr/FixedSizeOutputStreamBuffer.h"
#include "xdata/exdr/Serializer.h"



int main (int argc, char** argv)
{
	try
	{
		//Declare a table object
		xdata::Table t;
		t.addColumn("Number1", "unsigned long");
		t.addColumn("Text1", "string");
		t.addColumn("Float1", "float");
		t.addColumn("Text2", "string");
		t.addColumn("Text3", "string");
		t.addColumn("Float2", "float");
		t.addColumn("Float3", "float");
		t.addColumn("Number2", "unsigned long");
		t.addColumn("Number3", "unsigned long");
		t.addColumn("Text4", "string");
		
		std::string buffer;
	
		char c;
		while (std::cin >> std::noskipws >> c)
		{
			buffer.append(1,c);
		}
		
		std::cout << "Size of JSON buffer: " << buffer.length() << std::endl;

		toolbox::TimeVal start = toolbox::TimeVal::gettimeofday();

		std::istringstream instream(buffer);
		t.readFrom(instream);

		toolbox::TimeVal stop1 = toolbox::TimeVal::gettimeofday();

		std::cout << "Deserialize time: " << ((double)stop1 - (double)start) << std::endl;

		xdata::exdr::Serializer serializer;

        	char buf[0x100000];
        	int size = 0x100000;

        	xdata::exdr::FixedSizeOutputStreamBuffer outBuffer(buf, size);

        	serializer.exportAll (&t, &outBuffer);
		
		toolbox::TimeVal stop2 = toolbox::TimeVal::gettimeofday();

		std::cout << "Size of encoded table: " << outBuffer.tellp() << ", time: " << ((double)stop2 - (double)start) << std::endl;
		
		xdata::exdr::FixedSizeInputStreamBuffer  inBuffer(buf, size);
		start = toolbox::TimeVal::gettimeofday();
		xdata::Table incoming;
        	serializer.import(&incoming, &inBuffer); 
		stop2 = toolbox::TimeVal::gettimeofday();
		std::cout << "EXDR table deserialize time: " << ((double)stop2 - (double)start) << std::endl;
		
		
	} 
	catch (xdata::exception::Exception& e)
	{
		std::cout << "Error in create table";
		std::cout << xcept::stdformat_exception_history(e) << std::endl;
	}

	return 0;
}

