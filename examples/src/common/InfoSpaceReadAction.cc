// $Id: InfoSpaceReadAction.cc,v 1.2 2008/07/18 15:26:45 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/
 
#include "InfoSpaceListener.h"

int main (int argc, char** argv)
{
	// Create an InfoSpace
	xdata::InfoSpace * is = xdata::InfoSpace::get("NewInfoSpace");
	
	// Create an integer to be published into the infospace
	xdata::Integer * i = new xdata::Integer();
	
	// Assign 1 to the Integer
	*i = 1;
	
	// Publish the integer into the InfoSpace
	// first parameter is name of serializable
	// second parameter is serializable
	// third parameter is source of event, 0 here
	is->fireItemAvailable("testInteger", i, 0);

	// Attach a listener to the retrieval of the integer value
	ItemActionListener * l  = new ItemActionListener();
	is->addItemRetrieveListener("testInteger", l);
	
	// Find the integer in the InfoSpace
	try
	{
		is->fireItemValueRetrieve("testInteger", 0);
		xdata::Serializable *s = is->find("testInteger");
		std::cout << "found integer testInteger, value: " << dynamic_cast<xdata::Integer*>(s)->toString() << std::endl;
	} 
	catch (xdata::exception::Exception& e)
	{
		std::cout << "Could not find testInteger in InfoSpace" << std::endl;
	}
	
	// Remove the listener to the InfoSpace	event, before the item is revoked
	is->removeItemRetrieveListener("testInteger", l);

	// Remove the integer from the InfoSpace
	// First parameter is name of serializable
	// Second parameter is source of event, 0 here
	is->fireItemRevoked("testInteger", 0);

	delete l;	

	// delete the integer
	delete i;
	
	// Remove the InfoSpace
	xdata::InfoSpace::remove("NewInfoSpace");	

	return 0;
}

