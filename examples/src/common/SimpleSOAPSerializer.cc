// $Id: SimpleSOAPSerializer.cc,v 1.2 2008/07/18 15:26:45 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/
 
#include "xdata/xdata.h"
#include "xdata/XMLDOM.h"

#include "xdata/Serializable.h"
#include "xdata/soap/Serializer.h"
#include "xdata/exception/Exception.h"
#include "xdata/XStr.h"
#include "xdata/Float.h"
#include "xdata/Integer.h"
#include "xdata/Boolean.h"
#include "xdata/String.h"

#include "xoap/MessageFactory.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPName.h"
#include "xoap/SOAPBody.h"
#include "xoap/SOAPBodyElement.h"

/*! the output of this program should be
<soap-env:Envelope 
	soap-env:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" 
	xmlns:soap-env="http://schemas.xmlsoap.org/soap/envelope/">
	<soap-env:Header/>
	<soap-env:Body>
		<xdaq:data xmlns:xdaq="urn:xdaq-soap:3.0">
			<xdaq:myInteger 
				xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding" 
				xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
				xsi:type="xsd:integer">
				1
			</xdaq:myInteger>
			<xdaq:myFloat 
				xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding" 
				xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
				xsi:type="xsd:float">
				3.141000
			</xdaq:myFloat>
			<xdaq:myBoolean xsi:type="xsd:boolean">true</xdaq:myBoolean>
			<xdaq:myString xsi:type="xsd:string">Example text</xdaq:myString>
		</xdaq:data>
	</soap-env:Body>
</soap-env:Envelope>
*/

int main(int argc, char** argv)
{
	// Initialize XML platform in standalone main program only
	XMLPlatformUtils::Initialize();
	
	// Create a SOAP message
	xoap::MessageReference msg = xoap::createMessage();
	xoap::SOAPEnvelope envelope = msg->getSOAPPart().getEnvelope();
        xoap::SOAPBody body = envelope.getBody();
        xoap::SOAPName bodyElementName = envelope.createName("data", "xdaq", "urn:xdaq-soap:3.0");
        xoap::SOAPBodyElement bodyElement = body.addBodyElement ( bodyElementName );

	// Declare a SOAP serializer
	xdata::soap::Serializer serializer;
	
	// Declare a few basic data types and serialize them
	// export xdata variable into the SOAP message body element
	// Create also all necessary SOAP namespaces that contain the
	// definition of the SOAP data types
	xdata::Integer i;
	i = 1;
	xoap::SOAPName integerElementName = envelope.createName ("myInteger", "xdaq", "urn:xdaq-soap:3.0");
	xoap::SOAPElement integerElement = bodyElement.addChildElement(integerElementName);
	serializer.exportAll(&i, dynamic_cast<DOMElement*>(integerElement.getDOMNode()), true);
	
	xdata::Float f;
	f = 3.141;
	xoap::SOAPName floatElementName = envelope.createName ("myFloat", "xdaq", "urn:xdaq-soap:3.0");
	xoap::SOAPElement floatElement = bodyElement.addChildElement(floatElementName);
	serializer.exportAll(&f, dynamic_cast<DOMElement*>(floatElement.getDOMNode()), true);
	
	xdata::Boolean b;
	b = true;
	xoap::SOAPName booleanElementName = envelope.createName ("myBoolean", "xdaq", "urn:xdaq-soap:3.0");
	xoap::SOAPElement booleanElement = bodyElement.addChildElement(booleanElementName);
	serializer.exportAll(&b, dynamic_cast<DOMElement*>(booleanElement.getDOMNode()), false);
	
	xdata::String s;
	s = "Example text";
	xoap::SOAPName stringElementName = envelope.createName ("myString", "xdaq", "urn:xdaq-soap:3.0");
	xoap::SOAPElement stringElement = bodyElement.addChildElement(stringElementName);
	serializer.exportAll(&s, dynamic_cast<DOMElement*>(stringElement.getDOMNode()), false);
	
	// print the newly created message to the console
	msg->writeTo(std::cout);
	std::cout << std::endl;
	
	return 0;
}
