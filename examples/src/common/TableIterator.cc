// $Id: TableIterator.cc,v 1.3 2008/07/18 15:26:45 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/
#include <iostream>
 
#include "xdata/TableIterator.h"
#include "xdata/Table.h"
#include "xdata/UnsignedLong.h"
#include "xdata/String.h"
#include "xdata/Double.h"
#include "xdata/exdr/FixedSizeInputStreamBuffer.h"
#include "xdata/exdr/FixedSizeOutputStreamBuffer.h"
#include "xdata/exdr/Serializer.h"

int main (int argc, char** argv)
{

	try
	{
		//Declare a table object
		xdata::Table t;
		t.addColumn("Number", "unsigned long");
		t.addColumn("Time", "string");
		t.addColumn("Bandwidth", "double");
		t.addColumn("Rate","double");
		t.addColumn("Latency", "double");
		
		for (size_t i = 0; i< 10; i++)
		{
			xdata::UnsignedLong  number;
			number = i;
			t.setValueAt(i,"Number", number);
			
			xdata::String  time;
			time = "10:00:00";
			t.setValueAt(i,"Time", time);
			
			xdata::Double  b;
			b = 100.4;
			t.setValueAt(i,"Bandwidth", b);
		}
		
		xdata::Table::iterator ti;
		
		ti.operator=(t.begin());
	 
		for (ti = t.begin(); ti != t.end(); ti++)
		{
			xdata::UnsignedLong * number = dynamic_cast<xdata::UnsignedLong *>((*ti).getField("Number"));
			xdata::String * time = dynamic_cast<xdata::String *>((*ti).getField("Time"));
			xdata::Double * b = dynamic_cast<xdata::Double *>((*ti).getField("Bandwidth"));
			
			std::cout << number->toString() << "\t" << time->toString() << "\t" << b->toString() << std::endl;
		}
	} 
	catch (xdata::exception::Exception& e)
	{
		std::cout << "Error in create table" << e.what() << std::endl;
	}

	return 0;
}

