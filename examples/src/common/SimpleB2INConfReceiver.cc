// $Id: SimpleB2INConfReceiver.cc,v 1.1 2008/12/08 10:33:08 lorsini Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "SimpleB2INConfReceiver.h"

//
//
XDAQ_INSTANTIATOR_IMPL(SimpleB2INConfReceiver)
