// $Id: SimpleWebPlainText.cc,v 1.2 2008/07/18 15:26:45 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "SimpleWebPlainText.h"

//
// provides factory method for instantion of SimpleWebPlainText application
//
XDAQ_INSTANTIATOR_IMPL(SimpleWebPlainText)

SimpleWebPlainText::SimpleWebPlainText(xdaq::ApplicationStub * s)
	throw (xdaq::exception::Exception): xdaq::Application(s)
{	
	xgi::bind(this,&SimpleWebPlainText::Default, "Default");
}
	
void SimpleWebPlainText::Default(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception)
{
	out->getHTTPResponseHeader().addHeader("Content-Type", "text/plain");

	*out << "My simple text" << std::endl;
}
