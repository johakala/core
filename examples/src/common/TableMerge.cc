// $Id: TableMerge.cc,v 1.3 2008/07/18 15:26:45 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/
#include <iostream>
 
#include "xdata/TableIterator.h"
#include "xdata/TableAlgorithms.h"
#include "xdata/Table.h"
#include "xdata/UnsignedLong.h"
#include "xdata/String.h"
#include "xdata/Double.h"
#include "xdata/exdr/FixedSizeInputStreamBuffer.h"
#include "xdata/exdr/FixedSizeOutputStreamBuffer.h"
#include "xdata/exdr/Serializer.h"

void merge (xdata::Table* dest, xdata::Table* src, std::set<std::string>& keys)
{
	std::map<std::string, xdata::AbstractVector *, xdata::Table::ci_less >& dstColumnData = dest->columnData_;
	std::map<std::string, xdata::AbstractVector *, xdata::Table::ci_less >& srcColumnData = src->columnData_;
	
	std::set<std::string>::iterator ki;
		
	for (size_t srci = 0; srci < src->numberOfRows_; ++srci)
	{
		bool merged = false;
		for (size_t dsti = 0; dsti < dest->numberOfRows_; ++dsti)
		{
			size_t matches = 0;
			for (ki = keys.begin(); ki != keys.end(); ++ki)
			{
				xdata::Serializable* dstSerializable = (dstColumnData[*ki])->elementAt(dsti);
				xdata::Serializable* srcSerializable = (srcColumnData[*ki])->elementAt(srci);
				
				if (dstSerializable->equals(*srcSerializable))
				{
					++matches;				
				}
			}
			
			if (matches == keys.size())
			{
				merged = true;
				// copy src into dst
				std::map<std::string, std::string, xdata::Table::ci_less >::iterator ci;
				for (ci = dest->columnDefinitions_.begin(); ci != dest->columnDefinitions_.end(); ++ci)
				{
					dstColumnData[(*ci).first]->elementAt(dsti)->setValue(*(srcColumnData[(*ci).first]->elementAt(srci)));
				}	
			}
		}
		
		if (!merged)
		{
			// append the row of the src table
			std::map<std::string, std::string, xdata::Table::ci_less >::iterator ci;
			size_t row = dest->numberOfRows_;
			for (ci = dest->columnDefinitions_.begin(); ci != dest->columnDefinitions_.end(); ++ci)
			{
				//std::cout << "Insert, Number of rows in dst table: " << row << std::endl;
				dest->setValueAt(row, (*ci).first, *(srcColumnData[(*ci).first]->elementAt(srci)) );
			}
		}		
	}
}

int main (int argc, char** argv)
{

	try
	{
		std::cout << "Old merge W O2!" << std::endl;
		for (size_t tests = 1; tests <= 10; ++tests)
		{
			//Declare a table object
			xdata::Table firstTable;
			firstTable.addColumn("Host", "string");
			firstTable.addColumn("Number", "unsigned long");
			firstTable.addColumn("Time", "string");
			firstTable.addColumn("Bandwidth", "double");
			firstTable.addColumn("Rate","double");
			firstTable.addColumn("Latency", "double");

			size_t t1 = tests * 1000;
			for (size_t i = 0; i < t1; i++)
			{
				std::stringstream s;
				s << "http://HOST:" << i;
				xdata::String  host(s.str());
				firstTable.setValueAt(i,"Host", host);

				xdata::UnsignedLong  number;
				number = i;
				firstTable.setValueAt(i,"Number", number);

				xdata::String  time;
				time = "10:00:00";
				firstTable.setValueAt(i,"Time", time);

				xdata::Double  b;
				b = 100.0 + i;
				firstTable.setValueAt(i,"Bandwidth", b);
			}
			
			//std::cout << "Row count in first table: " << firstTable.getRowCount() << std::endl;


			//Declare a table object
			xdata::Table secondTable;
			secondTable.addColumn("Host", "string");
			secondTable.addColumn("Number", "unsigned long");
			secondTable.addColumn("Time", "string");
			secondTable.addColumn("Bandwidth", "double");
			secondTable.addColumn("Rate","double");
			secondTable.addColumn("Latency", "double");

			for (size_t i = 0; i < 100; i++)
			{
				std::stringstream s;
				s << "http://HOST:" << (t1-1);
				xdata::String  host(s.str());
				secondTable.setValueAt(i,"Host", host);

				xdata::UnsignedLong  number;
				number = i;
				secondTable.setValueAt(i,"Number", number);

				xdata::String  time;
				time = "20:00:00";
				secondTable.setValueAt(i,"Time", time);

				xdata::Double  b;
				b = 200.0 + i;
				secondTable.setValueAt(i,"Bandwidth", b);
			}


			//std::cout << "Before merge row count in first table: " << firstTable.getRowCount() << std::endl;

			std::set<std::string> keys;
			//keys.insert("Number");
			//keys.insert("Bandwidth");
			keys.insert("Host");

			toolbox::TimeVal start = toolbox::TimeVal::gettimeofday();
			//xdata::merge (&firstTable, &secondTable, keys);
			::merge (&firstTable, &secondTable, keys);
			toolbox::TimeVal stop = toolbox::TimeVal::gettimeofday();
			
			//std::cout << "After merge row count in first table: " << firstTable.getRowCount() << std::endl;


			std::cout << "Merging time for " << t1 << " rows " << (double)(stop-start) << " seconds" << std::endl;
		}
		
		/*
		xdata::TableIterator ti;
		for (ti = firstTable.begin(); ti != firstTable.end(); ti++)
		{
			xdata::UnsignedLong * number = dynamic_cast<xdata::UnsignedLong *>((*ti).getField("Number"));
			xdata::String * time = dynamic_cast<xdata::String *>((*ti).getField("Time"));
			xdata::Double * b = dynamic_cast<xdata::Double *>((*ti).getField("Bandwidth"));
			std::cout << number->toString() << "\t" << time->toString() << "\t" << b->toString() << std::endl;
		}
		*/
		


		
	} 
	catch (xdata::exception::Exception& e)
	{
		std::cout << "Error in create table" << e.what() << std::endl;
	}

	return 0;
}

