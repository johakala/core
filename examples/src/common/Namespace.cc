// $Id: Namespace.cc,v 1.2 2008/07/18 15:26:45 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "examples/Namespace.h"
#include "toolbox/lang/RTTI.h"

/*xdaq::Application * examples::Namespace::instantiate(xdaq::ApplicationStub * s) throw (xdaq::exception::Exception)
{
	return new examples::Namespace(s);	
}*/

XDAQ_INSTANTIATOR_IMPL(examples::Namespace);


examples::Namespace::Namespace(xdaq::ApplicationStub * s): xdaq::Application(s)
{
	std::cout << "_ZN" << toolbox::lang::scopemangle("examples::Namespace") << "11instantiateEPN4xdaq15ApplicationStubE" << std::endl;
}

