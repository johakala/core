// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2016, CERN.			                             *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, D. Simelev 					                     *
 *                                                                       *
 * For the licensing terms see LICENSE.		                             *
 * For the list of contributors see CREDITS.   			                 *
 *************************************************************************/

#include "SimpleWorkLoopWithArg.h"
#include "xcept/tools.h"


XDAQ_INSTANTIATOR_IMPL(SimpleWorkLoopWithArg)

// nice for polimorphism
std::ostream& operator << (std::ostream& os, const std::list<std::string>& sequence)
{
	for (std::list<std::string>::const_iterator i = sequence.begin(); i != sequence.end(); i++)
	{
		os << *i << " ";
	}
	return os;
}


SimpleWorkLoopWithArg::SimpleWorkLoopWithArg(xdaq::ApplicationStub * s) throw (xdaq::exception::Exception): xdaq::Application(s)
{

	std::list<std::string> alist;
	alist.push_back("may");
	alist.push_back("the");
	alist.push_back("force");
	alist.push_back("be");
	alist.push_back("with");
	alist.push_back("you");

	// Bind workloop function
	toolbox::task::ActionSignature* job1 = toolbox::task::bindwitharg<SimpleWorkLoopWithArg,std::string>(this, &SimpleWorkLoopWithArg::working, "working", "hello world");
	toolbox::task::ActionSignature* job2 = toolbox::task::bindwitharg<SimpleWorkLoopWithArg,int>(this, &SimpleWorkLoopWithArg::working, "working", 34);
	toolbox::task::ActionSignature* job3 = toolbox::task::bindwitharg<SimpleWorkLoopWithArg,float>(this, &SimpleWorkLoopWithArg::working, "working", 3.14);
	toolbox::task::ActionSignature* job4 = toolbox::task::bindwitharg<SimpleWorkLoopWithArg,std::list<std::string> >(this, &SimpleWorkLoopWithArg::working, "working", alist);


	toolbox::task::WorkLoop* workLoop = toolbox::task::getWorkLoopFactory()->getWorkLoop("SimpleWorkLoopWithArg", "waiting");


	if (!workLoop->isActive())
	{
		workLoop->activate();
	}

	try
	{
		workLoop->submit(job1);
		workLoop->submit(job2);
		workLoop->submit(job3);
		workLoop->submit(job4);
	}
	catch (xdaq::exception::Exception& e)
	{
		std::cout <<  xcept::stdformat_exception_history(e) << std::endl;
	}
}


template <class T>
bool SimpleWorkLoopWithArg::working(toolbox::task::WorkLoop* wl, T arg)
{
	std::cout <<  "Start working for 5 seconds..." << arg << std::endl;;
	::sleep(5);
	std::cout <<  "Finished working!" << std::endl;

	// Return false to return to terminate job
	return true;
}



