// $Id: WebObject.cc,v 1.2 2008/07/18 15:26:46 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "WebObject.h"

//
// provides factory method for instantion of SimpleWeb application
//
XDAQ_INSTANTIATOR_IMPL(WebObject)

WebObject::WebObject(xdaq::ApplicationStub * s)
	throw (xdaq::exception::Exception): xdaq::Application(s)
{			
	object_ = new WebGUI();
	
	std::vector<toolbox::lang::Method*> v =  object_->getMethods();
	std::vector<toolbox::lang::Method*>::iterator i;
	for (i = v.begin(); i != v.end(); ++i)
	{
		if ((*i)->type() == "cgi")
		{
			std::string name = static_cast<xgi::MethodSignature*>( *i )->name();
			xgi::bind(this, &WebObject::Default, name);
		}
	}
}

void WebObject::Default(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception)
{
	std::string name = in->getenv("PATH_INFO");
	static_cast<xgi::MethodSignature*>(object_->getMethod(name))->invoke(in,out);
}


WebGUI::WebGUI()
{
	xgi::bind(this, &WebGUI::page1, "page1");
	xgi::bind(this, &WebGUI::page2, "page2");
}
	
void WebGUI::page1 (xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception)
{
	out->getHTTPResponseHeader().addHeader("Content-Type", "text/html");
	*out << cgicc::HTMLDoctype(cgicc::HTMLDoctype::eStrict) << std::endl;
	*out << cgicc::html().set("lang", "en").set("dir","ltr") << std::endl;
	*out << cgicc::title("Page 1") << std::endl;
	*out << cgicc::p("Page 1") << endl;	
}

void WebGUI::page2 (xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception)
{
	out->getHTTPResponseHeader().addHeader("Content-Type", "text/html");
	*out << cgicc::HTMLDoctype(cgicc::HTMLDoctype::eStrict) << std::endl;
	*out << cgicc::html().set("lang", "en").set("dir","ltr") << std::endl;
	*out << cgicc::title("Page 2") << std::endl;
	*out << cgicc::p("Page 2") << endl;		
}
