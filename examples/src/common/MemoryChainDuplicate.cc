// $Id: MemoryChainDuplicate.cc,v 1.2 2008/07/18 15:26:45 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/
#include <iostream>
 
#include "toolbox/mem/HeapAllocator.h"
#include "toolbox/mem/Reference.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "toolbox/mem/exception/Exception.h"

int main (int argc, char** argv)
{

	try 
	{
		toolbox::net::URN urn("toolbox-mem-pool", "myPool");

		toolbox::mem::HeapAllocator* a = new toolbox::mem::HeapAllocator();
		toolbox::mem::Pool * p = toolbox::mem::getMemoryPoolFactory()->createPool(urn, a);

		// Chain two blocks of 8196 
		toolbox::mem::Reference* myChain  = toolbox::mem::getMemoryPoolFactory()->getFrame(p, 8196);
		myChain->setNextReference(toolbox::mem::getMemoryPoolFactory()->getFrame(p, 8196));

		// Zero copy duplicate chain
		toolbox::mem::Reference* myChainDuplicate  = myChain->duplicate();

        	// Free both chains	
		myChain->release();
		myChainDuplicate->release();


	}
	catch (toolbox::mem::exception::Exception & e)
	{
		std::cout << "An error has occurred while processing duplicate: " << e.what() << std::endl ;
	}


	return 0;
}

