// $Id: WebEnvironment.cc,v 1.5 2008/07/18 15:26:45 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Discgicc::tributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of concgicc::tributors see CREDITS.   			         *
 *************************************************************************/

#include "WebEnvironment.h"

//
// provides factory method for instantion of WebEnvironment application
//
XDAQ_INSTANTIATOR_IMPL(WebEnvironment)

WebEnvironment::WebEnvironment(xdaq::ApplicationStub * s)
	throw (xdaq::exception::Exception): xdaq::Application(s)
{	
	xgi::bind(this,&WebEnvironment::Default, "Default");
}
	
void WebEnvironment::Default(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception)
{
	out->getHTTPResponseHeader().addHeader("Content-Type", "text/html");
	*out << cgicc::HTMLDoctype(cgicc::HTMLDoctype::eStrict) << std::endl;
	*out << cgicc::html().set("lang", "en").set("dir","ltr") << std::endl;
	*out << cgicc::title("Web Environment") << std::endl;

	*out << cgicc::a("Visit the XDAQ documentation site").set("href","http://xdaqwiki.cern.ch") << std::endl;
  // This is just a brain-dead dump of information.
  // Almost all of this code is for HTML formatting
 cgicc::Cgicc cgi(in);
	  const cgicc::CgiEnvironment& env = cgi.getEnvironment();
  *out << cgicc::h2("Environment information from CgiEnvironment") << std::endl;
  
  *out << cgicc::div().set("align","center") << std::endl;
  
  *out << cgicc::table() << std::endl;
  
  *out << cgicc::tr() << cgicc::td("Request Method").set("class","title") 
       << cgicc::td(env.getRequestMethod()).set("class","data") << cgicc::tr() << std::endl;
  *out << cgicc::tr() << cgicc::td("Path Info").set("class","title") 
       << cgicc::td(env.getPathInfo()).set("class","data") << cgicc::tr() << std::endl;
  *out << cgicc::tr() << cgicc::td("Path Translated").set("class","title") 
       << cgicc::td(env.getPathTranslated()).set("class","data") << cgicc::tr() << std::endl;
  *out << cgicc::tr() << cgicc::td("Script Name").set("class","title") 
       << cgicc::td(env.getScriptName()).set("class","data") << cgicc::tr() << std::endl;
  *out << cgicc::tr() << cgicc::td("HTTP Referrer").set("class","title") 
       << cgicc::td(env.getReferrer()).set("class","data") << cgicc::tr() << std::endl;
  *out << cgicc::tr() << cgicc::td("HTTP Cookie").set("class","title") 
       << cgicc::td(env.getCookies()).set("class","data") << cgicc::tr() << std::endl;
  *out << cgicc::tr() << cgicc::td("Query Scgicc::tring").set("class","title") 
       << cgicc::td(env.getQueryString()).set("class","data") << cgicc::tr() << std::endl;
  *out << cgicc::tr() << cgicc::td("Content Length").set("class","title") 
       << cgicc::td().set("class","data") << env.getContentLength() 
       << cgicc::td() << cgicc::tr() << std::endl;
  *out << cgicc::tr() << cgicc::td("Post Data").set("class","title")
       << cgicc::td().set("class","data")
       << cgicc::pre(env.getPostData()).set("class","data") << cgicc::td() 
       << cgicc::tr() << std::endl;
  *out << cgicc::tr() << cgicc::td("Remote Host").set("class","title") 
       << cgicc::td(env.getRemoteHost()).set("class","data") << cgicc::tr() << std::endl;
  *out << cgicc::tr() << cgicc::td("Remote Address").set("class","title") 
       << cgicc::td(env.getRemoteAddr()).set("class","data") << cgicc::tr() << std::endl;
  *out << cgicc::tr() << cgicc::td("Authorization Type").set("class","title") 
       << cgicc::td(env.getAuthType()).set("class","data") << cgicc::tr() << std::endl;
  *out << cgicc::tr() << cgicc::td("Remote User").set("class","title") 
       << cgicc::td(env.getRemoteUser()).set("class","data") << cgicc::tr() << std::endl;
  *out << cgicc::tr() << cgicc::td("Remote Identification").set("class","title") 
       << cgicc::td(env.getRemoteIdent()).set("class","data") << cgicc::tr() << std::endl;
  *out << cgicc::tr() << cgicc::td("Content Type").set("class","title") 
       << cgicc::td(env.getContentType()).set("class","data") << cgicc::tr() << std::endl;
  *out << cgicc::tr() << cgicc::td("HTTP Accept").set("class","title") 
       << cgicc::td(env.getAccept()).set("class","data") << cgicc::tr() << std::endl;
  *out << cgicc::tr() << cgicc::td("User Agent").set("class","title") 
       << cgicc::td(env.getUserAgent()).set("class","data") << cgicc::tr() << std::endl;
  *out << cgicc::tr() << cgicc::td("Server Software").set("class","title") 
       << cgicc::td(env.getServerSoftware()).set("class","data") << cgicc::tr() << std::endl;
  *out << cgicc::tr() << cgicc::td("Server Name").set("class","title") 
       << cgicc::td(env.getServerName()).set("class","data") << cgicc::tr() << std::endl;
  *out << cgicc::tr() << cgicc::td("Gateway Interface").set("class","title") 
       << cgicc::td(env.getGatewayInterface()).set("class","data") << cgicc::tr() << std::endl;
  *out << cgicc::tr() << cgicc::td("Server Protocol").set("class","title") 
       << cgicc::td(env.getServerProtocol()).set("class","data") << cgicc::tr() << std::endl;
  *out << cgicc::tr() << cgicc::td("Server Port").set("class","title") 
       << cgicc::td().set("class","data") << env.getServerPort() 
       << cgicc::td() << cgicc::tr() << std::endl;
  *out << cgicc::tr() << cgicc::td("HTTPS").set("class","title")
       << cgicc::td().set("class","data") << (env.usingHTTPS() ? "cgicc::true" : "false")
       << cgicc::td() << cgicc::tr() << std::endl;
  *out << cgicc::tr() << cgicc::td("Redirect Request").set("class","title") 
       << cgicc::td(env.getRedirectRequest()).set("class","data") << cgicc::tr() << std::endl;
  *out << cgicc::tr() << cgicc::td("Redirect URL").set("class","title") 
       << cgicc::td(env.getRedirectURL()).set("class","data") << cgicc::tr() << std::endl;
  *out << cgicc::tr() << cgicc::td("Redirect Status").set("class","title") 
       << cgicc::td(env.getRedirectStatus()).set("class","data") << cgicc::tr() << std::endl;
  
  *out << cgicc::table() << cgicc::div() << std::endl;
}
