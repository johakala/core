// $Id: TableFromString.cc,v 1.3 2008/07/21 08:06:57 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/
#include <iostream>
#include <sstream>
 
#include "xcept/tools.h"
#include "xdata/Table.h"
#include "xdata/UnsignedLong.h"
#include "xdata/UnsignedInteger.h"
#include "xdata/UnsignedInteger64.h"
#include "xdata/String.h"
#include "xdata/Double.h"
#include "xdata/exdr/FixedSizeInputStreamBuffer.h"
#include "xdata/exdr/FixedSizeOutputStreamBuffer.h"
#include "xdata/exdr/Serializer.h"



int main (int argc, char** argv)
{
	try
	{
		//Declare a table object
		xdata::Table t;
		
		std::string buffer;
	
		char c;
		while (std::cin >> std::noskipws >> c)
		{
			buffer.append(1,c);
		}

		std::istringstream instream(buffer);
		t.readFrom(instream);

		std::cout << "Parsed table: " << std::endl;

		std::cout << t.toString() << std::endl;
	} 
	catch (xdata::exception::Exception& e)
	{
		std::cout << "Error in create table";
		std::cout << xcept::stdformat_exception_history(e) << std::endl;
	}

	return 0;
}

