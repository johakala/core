// $Id: FlashTimeChart.cc,v 1.2 2008/07/18 15:26:45 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "FlashTimeChart.h"
#include "toolbox/TimeVal.h"
//
// provides factory method for instantion of SimpleWeb application
//
XDAQ_INSTANTIATOR_IMPL(FlashTimeChart)

FlashTimeChart::FlashTimeChart(xdaq::ApplicationStub * s)
	throw (xdaq::exception::Exception): xdaq::WebApplication(s)
{	
	xgi::bind(this,&FlashTimeChart::Default, "Default");
	xgi::bind(this,&FlashTimeChart::getInitialData, "getInitialData");
	xgi::bind(this,&FlashTimeChart::getData, "getData");
	data_ = 10;
}
	
void FlashTimeChart::Default(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception)
{
	*out << "<html><head><title>Real-time chart</title>" << std::endl;
	*out << "<link REL=\"stylesheet\" HREF=\"/daq/extern/FusionCharts/SampleCode/ClientSide/Style.css\">" << std::endl;
	*out << "</head>" << std::endl;
	*out << "<body topmargin=\"0\" leftmargin=\"0\">" << std::endl;
	*out << "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"4\" class=\"topbar\">" << std::endl;
	*out << "<tr><td><br></td></tr>" << std::endl;
	*out << "<tr><td align=\"center\"><span class=\"header\">Real-time chart</span></td></tr>" << std::endl;
	*out << "</table><br>" << std::endl;
	*out << "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">" << std::endl;
	*out << "<tr><td valign=\"top\" align=\"center\">" << std::endl;
	*out << "<table width=\"550\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\" align=\"center\" class=\"formtable\">" << std::endl;
	*out << "<tr><td colspan=\"3\" width=\"100%\"><center>" << std::endl;
	
	// For Microsoft browsers
	//
	*out << "<object classid=\"clsid:D27CDB6E-AE6D-11cf-96B8-444553540000\" ";
	*out << "codebase=\"http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0\" ";
	*out << "WIDTH=\"550\" HEIGHT=\"450\" id=\"FusionCharts\" VIEWASTEXT>" << std::endl;
	*out << "<param NAME=\"movie\" ";
	*out << "VALUE=\"/daq/extern/FusionCharts/Charts/FI2_RT_Line.swf?currTime=";
	// add time
	*out << "1%2F18%2F2006+3%3A30%3A28+AM&amp;";
	
	*out << "chartWidth=550&amp;chartHeight=450&amp;";
	
	// add data url
	*out << "dataUrl=";
	
	std::string url = getApplicationDescriptor()->getContextDescriptor()->getURL();
	url += "/";
	url += getApplicationDescriptor()->getURN();
	url += "/getInitialData";
	
	*out << cgicc::form_urlencode(url) << "\"" << std::endl;
	
	*out << "<param NAME=\"FlashVars\" VALUE=\"\">" << std::endl;
	*out << "<param NAME=\"quality\" VALUE=\"high\">" << std::endl;
	*out << "<param NAME=\"bgcolor\" VALUE=\"#FFFFFF\">" << std::endl;
	
	// For netscape like browsers
	//
	*out << "<embed src=\"/daq/extern/FusionCharts/Charts/FI2_RT_Line.swf?currTime=";
	
	// time
	*out << "1%2F18%2F2006+3%3A30%3A28+AM&amp;";
	*out << "chartWidth=550&amp;chartHeight=450&amp;";
	*out << "dataUrl=";
	
	*out << cgicc::form_urlencode(url);
	
	*out << "\" FlashVars=\"\" quality=\"high\" bgcolor=\"#FFFFFF\" ";
	*out << "WIDTH=\"550\" HEIGHT=\"450\" NAME=\"FusionCharts\" ALIGN TYPE=\"application/x-shockwave-flash\" ";
	*out << "PLUGINSPAGE=\"http://www.macromedia.com/go/getflashplayer\">" << std::endl;
	*out << "</object>" << std::endl;
	*out << "</center>" << std::endl;
	*out << "</td></tr>" << std::endl;
	*out << "<tr><td height=\"10\" colspan=\"3\" width=\"100%\"></td></tr>" << std::endl;
	*out << "<tr height=\"35\"><td align=\"center\" valign=\"bottom\">" << std::endl;
	
	// Again link to data...	
	*out << "<a href=\"" << url << "\" target=\"_blank\">View XML</a>" << std::endl;
	
	*out << "</td></tr></table>" << std::endl;
	*out << "</td></tr>" << std::endl;
	*out << "</table>" << std::endl;
	*out << "</body></html>" << std::endl;
}

void FlashTimeChart::getData(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception)
{
	std::string time = toolbox::TimeVal::gettimeofday().toString(toolbox::TimeVal::loc);
	unsigned long value = (unsigned long) (100.0*rand()/(RAND_MAX+1.0));
	*out << "&name='" << time << "'&value=" << value;
}

void FlashTimeChart::getInitialData(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception)
{
	try
	{
		cgicc::Cgicc cgi(in);	
		
		// Stream out data
		
		*out << "<chart bgColor='EEF2FB' canvasBorderThickness='1' canvasBorderColor='DAE1E8' ";
		*out << "canvasBgColor='FFFFFF' yAxisMaxValue='100' hovercapbg='FFFFFF' hovercapborder='DAE1E8' ";
		*out << "formatNumberScale='1' decimalPrecision='2' divLineDecimalPrecision='0' ";
		*out << "limitsDecimalPrecision='0' numdivlines='9' numVDivLines='30' numDisplaySets='30' ";
		*out << "divLineColor='DAE1E8' vDivLineColor='DAE1E8' chartLeftMargin='10' baseFontColor='899FB6' ";
		*out << "showRealTimeValue='1' ";
		
		std::string url = getApplicationDescriptor()->getContextDescriptor()->getURL();
		url += "/";
		url += getApplicationDescriptor()->getURN();
		url += "/getData";
		
		*out << "dataStreamURL='" << url << "?lowerLimit=0%26upperLimit=100' ";
		*out << "refreshInterval='1' ";
		*out << "numberSuffix='%25' rotateNames='1' hoverCapBgColor='000000' hoverCapBorderColor='008040' ";
		*out << "baseFontSize='11'>" << std::endl;
		*out << "<categories>" << std::endl;
				
		*out << "<category name=\"" << "Initial" << "\"/>" << std::endl;
		*out << "</categories>" << std::endl;
				
		*out << "<dataset color='00dd00' seriesName='Values' showValues='0' alpha='100' anchorAlpha='0' lineThickness='2'>" << std::endl;
		
		*out << "<set value='" << 10 << "' />" << std::endl;
		
		*out << "</dataset>" << std::endl;
		*out << "</chart>" << std::endl;
			
	}
	catch (xgi::exception::Exception& xe)
	{
		XCEPT_RETHROW (xgi::exception::Exception, "CGI processing error", xe);
	}
	catch (xcept::Exception & e)
	{
		XCEPT_RETHROW (xgi::exception::Exception, "invalid settings", e);
	}
	catch (std::exception e)
	{
		XCEPT_RAISE (xgi::exception::Exception, "Cannot interpret incoming CGI content");
	}
}
