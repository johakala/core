// $Id: TableRemoveColumn.cc,v 1.2 2008/07/18 15:26:46 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/
#include <iostream>
 
#include "xcept/tools.h"
#include "xdata/Table.h"
#include "xdata/UnsignedLong.h"
#include "xdata/UnsignedInteger.h"
#include "xdata/UnsignedInteger64.h"
#include "xdata/String.h"
#include "xdata/Double.h"
#include "xdata/exdr/FixedSizeInputStreamBuffer.h"
#include "xdata/exdr/FixedSizeOutputStreamBuffer.h"
#include "xdata/exdr/Serializer.h"

int main (int argc, char** argv)
{
	try
	{
		//Declare a table object
		xdata::Table t;
		t.addColumn("Number", "unsigned long");
		t.addColumn("NumberInt", "unsigned int");
		t.addColumn("NumberInt64", "unsigned int 64");
		t.addColumn("Time", "string");
		t.addColumn("Bandwidth", "double");
		t.addColumn("Rate","double");
		t.addColumn("Latency", "double");
		
		for (size_t i = 0; i< 10; i++)
		{
			xdata::UnsignedLong  number;
			number = i;
			t.setValueAt(i,"Number", number);
			
			xdata::UnsignedInteger  numberInt;
			numberInt = (i + 0xFFFF0000);
			t.setValueAt(i,"NumberInt", numberInt);
			
			xdata::UnsignedInteger64  numberInt64;
			numberInt64 = (i + 0xFFFFFFFF00000000);
			t.setValueAt(i,"NumberInt64", numberInt64);
			
			xdata::String  time;
			time = "10:00:00";
			t.setValueAt(i,"Time", time);
			
			xdata::Double  b;
			b = 100.4;
			t.setValueAt(i,"Bandwidth", b);
		}
		
		
		std::vector<std::string> columns = t.getColumns();
		for (std::vector<std::string>::iterator ci = columns.begin(); ci != columns.end(); ++ci)
		{
			std::cout << "Column '" << (*ci) << "', type '" << t.getColumnType ( *ci ) << "'" << std::endl;
		}

		std::cout << std::endl;
		
		t.removeColumn("Number");
		t.removeColumn("Time");
		
		columns = t.getColumns();
		for (std::vector<std::string>::iterator ci = columns.begin(); ci != columns.end(); ++ci)
		{
			std::cout << "Column '" << (*ci) << "', type '" << t.getColumnType ( *ci ) << "'" << std::endl;
		}
		
	} 
	catch (xdata::exception::Exception& e)
	{
		std::cout << "Error in create table";
		std::cout << xcept::stdformat_exception_history(e) << std::endl;
	}

	return 0;
}

