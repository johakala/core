// $Id: SOAPStateMachine.cc,v 1.4 2008/07/18 15:26:45 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "SOAPStateMachine.h"

//
// provides factory method for instantion of SOAPStateMachine application
//
XDAQ_INSTANTIATOR_IMPL(SOAPStateMachine)

SOAPStateMachine::SOAPStateMachine(xdaq::ApplicationStub * s)
	throw (xdaq::exception::Exception)
	: xdaq::Application(s) 	
{	
	//
	// Bind SOAP callbacks
	//
	xoap::bind(this, &SOAPStateMachine::fireEvent, "Configure", XDAQ_NS_URI );
	xoap::bind(this, &SOAPStateMachine::fireEvent, "Enable", XDAQ_NS_URI );
	xoap::bind(this, &SOAPStateMachine::fireEvent, "Suspend", XDAQ_NS_URI );
	xoap::bind(this, &SOAPStateMachine::fireEvent, "Resume", XDAQ_NS_URI );
	xoap::bind(this, &SOAPStateMachine::fireEvent, "Halt", XDAQ_NS_URI );
	xoap::bind(this, &SOAPStateMachine::reset, "Reset", XDAQ_NS_URI );

	//
	// Define FSM
	//		
	fsm_.addState('H', "Halted", this, &SOAPStateMachine::stateChanged);
	fsm_.addState('R', "Ready", this, &SOAPStateMachine::stateChanged);
	fsm_.addState('E', "Enabled", this, &SOAPStateMachine::stateChanged);
	fsm_.addState('S', "Suspended", this, &SOAPStateMachine::stateChanged);

	fsm_.addStateTransition('H', 'R', "Configure", this, &SOAPStateMachine::ConfigureAction);
	fsm_.addStateTransition('R', 'E', "Enable", this,&SOAPStateMachine::EnableAction);
	fsm_.addStateTransition('E', 'S', "Suspend",this, &SOAPStateMachine::SuspendAction);
	fsm_.addStateTransition('S', 'E', "Resume",this, &SOAPStateMachine::ResumeAction);

	fsm_.addStateTransition('H', 'H', "Halt", this, &SOAPStateMachine::HaltAction);
	fsm_.addStateTransition('R', 'H', "Halt", this, &SOAPStateMachine::HaltAction);
	fsm_.addStateTransition('E', 'H', "Halt", this, &SOAPStateMachine::HaltAction);
	fsm_.addStateTransition('S', 'H', "Halt", this, &SOAPStateMachine::HaltAction);

	// Failure state setting
	fsm_.setFailedStateTransitionAction( this, &SOAPStateMachine::failedTransition );
	fsm_.setFailedStateTransitionChanged(this, &SOAPStateMachine::stateChanged );

	fsm_.setInitialState('H');
	fsm_.setStateName('F', "Failed"); // give a name to the 'F' state

	fsm_.reset();

	// Export a "State" variable that reflects the state of the state machine
	state_ = fsm_.getStateName (fsm_.getCurrentState());
	getApplicationInfoSpace()->fireItemAvailable("State",&state_);
}
	
		
xoap::MessageReference SOAPStateMachine::fireEvent (xoap::MessageReference msg) 
	throw (xoap::exception::Exception)
{
	xoap::SOAPPart part = msg->getSOAPPart();
	xoap::SOAPEnvelope env = part.getEnvelope();
	xoap::SOAPBody body = env.getBody();
	DOMNode* node = body.getDOMNode();
	DOMNodeList* bodyList = node->getChildNodes();
	for (unsigned int i = 0; i < bodyList->getLength(); i++) 
	{
		DOMNode* command = bodyList->item(i);

		if (command->getNodeType() == DOMNode::ELEMENT_NODE)
		{

			std::string commandName = xoap::XMLCh2String (command->getLocalName());


			try 
			{
				toolbox::Event::Reference e(new toolbox::Event(commandName,this));
				fsm_.fireEvent(e);
			}
			catch (toolbox::fsm::exception::Exception & e)
			{
				XCEPT_RETHROW(xoap::exception::Exception, "invalid command", e);
			}

			xoap::MessageReference reply = xoap::createMessage();
			xoap::SOAPEnvelope envelope = reply->getSOAPPart().getEnvelope();
			xoap::SOAPName responseName = envelope.createName( commandName +"Response", "xdaq", XDAQ_NS_URI);
			// xoap::SOAPBodyElement e = envelope.getBody().addBodyElement ( responseName );
			(void) envelope.getBody().addBodyElement ( responseName );
			return reply;
		}
	}

	XCEPT_RAISE(xcept::Exception,"command not found");		
}

	
xoap::MessageReference SOAPStateMachine::reset (xoap::MessageReference msg) throw (xoap::exception::Exception)
{
	LOG4CPLUS_INFO (getApplicationLogger(), "New state before reset is: " << fsm_.getStateName (fsm_.getCurrentState()) );

	fsm_.reset();
	state_ = fsm_.getStateName (fsm_.getCurrentState());

	xoap::MessageReference reply = xoap::createMessage();
	xoap::SOAPEnvelope envelope = reply->getSOAPPart().getEnvelope();
	xoap::SOAPName responseName = envelope.createName("ResetResponse", "xdaq", XDAQ_NS_URI);
	// xoap::SOAPBodyElement e = envelope.getBody().addBodyElement ( responseName );
	(void) envelope.getBody().addBodyElement ( responseName );

	LOG4CPLUS_INFO (getApplicationLogger(), "New state after reset is: " << fsm_.getStateName (fsm_.getCurrentState()) );

	return reply;
}

	
void SOAPStateMachine::ConfigureAction (toolbox::Event::Reference e) throw (toolbox::fsm::exception::Exception)
{
	LOG4CPLUS_INFO (getApplicationLogger(), e->type());
}

void SOAPStateMachine::EnableAction (toolbox::Event::Reference e) throw (toolbox::fsm::exception::Exception) 
{
	LOG4CPLUS_INFO (getApplicationLogger(), e->type());
}

void SOAPStateMachine::SuspendAction (toolbox::Event::Reference e) throw (toolbox::fsm::exception::Exception) 
{
	LOG4CPLUS_INFO (getApplicationLogger(), e->type());

	// a failure is forced here
	XCEPT_RAISE(toolbox::fsm::exception::Exception, "error in suspend");
}

void SOAPStateMachine::ResumeAction (toolbox::Event::Reference e) throw (toolbox::fsm::exception::Exception) 
{
	LOG4CPLUS_INFO (getApplicationLogger(), e->type());

}

void SOAPStateMachine::HaltAction (toolbox::Event::Reference e) throw (toolbox::fsm::exception::Exception) 
{
	LOG4CPLUS_INFO (getApplicationLogger(), e->type());

}



void SOAPStateMachine::stateChanged (toolbox::fsm::FiniteStateMachine & fsm) throw (toolbox::fsm::exception::Exception)
{
	// Reflect the new state
	state_ = fsm.getStateName (fsm.getCurrentState());
	LOG4CPLUS_INFO (getApplicationLogger(), "New state is:" << fsm.getStateName (fsm.getCurrentState()) );
}

void SOAPStateMachine::failedTransition (toolbox::Event::Reference e) throw (toolbox::fsm::exception::Exception)
{
	toolbox::fsm::FailedEvent & fe = dynamic_cast<toolbox::fsm::FailedEvent&>(*e);
	LOG4CPLUS_INFO (getApplicationLogger(), "Failure occurred when performing transition from: "  <<
			fe.getFromState() <<  " to: " << fe.getToState() << " exception: " << fe.getException().what() );
}
	
