// $Id: InfoSpaceSOAPSerializer.cc,v 1.2 2008/07/18 15:26:45 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/
 
#include "xdata/xdata.h"
#include "xdata/XMLDOM.h"

#include "xdata/Serializable.h"
#include "xdata/soap/Serializer.h"
#include "xdata/exception/Exception.h"
#include "xdata/XStr.h"
#include "xdata/InfoSpace.h"
#include "xdata/Float.h"
#include "xdata/Integer.h"
#include "xdata/Boolean.h"
#include "xdata/String.h"

#include "xoap/MessageFactory.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPName.h"
#include "xoap/SOAPBody.h"
#include "xoap/SOAPBodyElement.h"

/*! the output of this program should be
<soap-env:Envelope 
	soap-env:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" 
	xmlns:soap-env="http://schemas.xmlsoap.org/soap/envelope/">
	<soap-env:Header/>
	<soap-env:Body>
		<xdaq:data 
			xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding" 
			xmlns:xdaq="urn:xdaq-soap:3.0" 
			xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
			xsi:type="soapenc:Struct">
				<xdaq:myBoolean xsi:type="xsd:boolean">true</xdaq:myBoolean>
				<xdaq:myFloat xsi:type="xsd:float">3.141000</xdaq:myFloat>
				<xdaq:myInteger xsi:type="xsd:integer">1</xdaq:myInteger>
				<xdaq:myString xsi:type="xsd:string">Example text</xdaq:myString>
		</xdaq:data>
	</soap-env:Body>
</soap-env:Envelope>
*/

int main(int argc, char** argv)
{
	// Initialize XML platform in standalone main program only
	XMLPlatformUtils::Initialize();
	
	// Create a SOAP message
	xoap::MessageReference msg = xoap::createMessage();
	xoap::SOAPEnvelope envelope = msg->getSOAPPart().getEnvelope();
        xoap::SOAPBody body = envelope.getBody();
        xoap::SOAPName bodyElementName = envelope.createName("data", "xdaq", "urn:xdaq-soap:3.0");
        xoap::SOAPBodyElement bodyElement = body.addBodyElement ( bodyElementName );

	// Declare a SOAP serializer
	xdata::soap::Serializer serializer;
	
	//Declare an InfoSpace
	xdata::InfoSpace * infospace = xdata::InfoSpace::get("myInfoSpace");
	
	// Add some data to it
	xdata::Integer i;
	i = 1;
	infospace->fireItemAvailable("myInteger", &i, 0);
	
	xdata::Float f;
	f = 3.141;
	infospace->fireItemAvailable("myFloat", &f, 0);
	
	xdata::Boolean b;
	b = true;
	infospace->fireItemAvailable("myBoolean", &b, 0);
	
	xdata::String s;
	s = "Example text";
	infospace->fireItemAvailable("myString", &s, 0);
	
	// Serialize the InfoSpace into the SOAP Body element
	serializer.exportAll(infospace, dynamic_cast<DOMElement*>(bodyElement.getDOMNode()), true);
	
	// print the newly created message to the console
	msg->writeTo(std::cout);
	std::cout << std::endl;
	
	xdata::InfoSpace::remove("myInfoSpace");
	
	return 0;
}
