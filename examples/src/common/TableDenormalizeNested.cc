// $Id: TableDenormalizeNested.cc,v 1.2 2008/07/18 15:26:45 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/
#include <iostream>
 
#include "xcept/tools.h"
#include "xdata/Table.h"
#include "xdata/UnsignedLong.h"
#include "xdata/UnsignedInteger.h"
#include "xdata/UnsignedInteger64.h"
#include "xdata/String.h"
#include "xdata/Double.h"
#include "xdata/exdr/FixedSizeInputStreamBuffer.h"
#include "xdata/exdr/FixedSizeOutputStreamBuffer.h"
#include "xdata/exdr/Serializer.h"



int main (int argc, char** argv)
{
	try
	{
		//Declare a table object
		xdata::Table t;
		t.addColumn("Number", "unsigned long");
		t.addColumn("T", "table");
		
		for (size_t i = 0; i< 5; i++)
		{
			xdata::UnsignedLong  number;
			number = i;
			t.setValueAt(i,"Number", number);
			
			xdata::Table  r;
			r.addColumn("A", "unsigned long");
			r.setValueAt(0,"A", number);
			r.addColumn("B", "unsigned long");
			r.setValueAt(0,"B", number);
			
			t.setValueAt(i,"T",r);
		}
		
		std::cout << std::endl << "--- Perform normalization ---" << std::endl;	
		xdata::Table * normalized = xdata::Table::normalize(&t);
		 
		std::cout << std::endl << "--- After normaliztion ---" << std::endl;
		normalized->writeTo (std::cout);
		
		delete normalized;

	} 
	catch (xdata::exception::Exception& e)
	{
		std::cout << "Error in create table";
		std::cout << xcept::stdformat_exception_history(e) << std::endl;
	}

	return 0;
}

