// $Id: MonitoringProducer.cc,v 1.3 2008/07/18 15:26:45 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "MonitoringProducer.h"
#include "toolbox/task/TimerFactory.h"

//
// provides factory method for instantion of SimpleWeb application
//
XDAQ_INSTANTIATOR_IMPL(MonitoringProducer)

MonitoringProducer::MonitoringProducer(xdaq::ApplicationStub * s)
	throw (xdaq::exception::Exception): xdaq::Application(s)
{	
	xgi::bind(this,&MonitoringProducer::Default, "Default");
	xgi::bind(this,&MonitoringProducer::start, "start");
	xgi::bind(this,&MonitoringProducer::stop, "stop");
	
	// initialize parameter to a default value
	counter_ = 0;
	lastExecutionTime_ = "";
	randomValue_ = 0.0;
	infoSpace_ = 0;
		
	try
	{
		timer_ = toolbox::task::getTimerFactory()->createTimer("MonitoringProducerTimer");
		timer_->stop();
	}
	catch (toolbox::task::exception::Exception& te)
	{
		XCEPT_RETHROW (xdaq::exception::Exception, "Cannot run MonitoringProducer, timer already created", te);
	}
}
	
void MonitoringProducer::Default(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception)
{
	*out << cgicc::HTMLDoctype(cgicc::HTMLDoctype::eStrict) << std::endl;
	*out << cgicc::html().set("lang", "en").set("dir","ltr") << std::endl;
	*out << cgicc::title("Simple Web Form") << std::endl;
	
	
	// This method can be invoked using Linux 'wget' command
	// e.g http://lxcmd101:1972/urn:xdaq-application:lid=23/setParameter?value=24
	std::string startMethod = toolbox::toString("/%s/start",getApplicationDescriptor()->getURN().c_str());
	std::string stopMethod = toolbox::toString("/%s/stop",getApplicationDescriptor()->getURN().c_str());
	
	if (timer_->isActive())
	{
		*out << cgicc::a("Stop").set("href",stopMethod) << " producing data " << std::endl;
	} else
	{
		*out << cgicc::a("Start").set("href",startMethod) << " producing data " << std::endl;
	}
	
	*out << cgicc::br() << std::endl;
	*out << "Last created monitoring values updated at: " << lastExecutionTime_.toString() << cgicc::br() << std::endl;
	*out << "Counter: " << counter_.toString() << cgicc::br() << std::endl;
	*out << "Random: " << randomValue_.toString() << cgicc::br() << std::endl;
	
	std::string updateMethod = toolbox::toString("/%s",getApplicationDescriptor()->getURN().c_str());
	*out << cgicc::a("Update").set("href",updateMethod) << " display" << std::endl;
}

void MonitoringProducer::start(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception)
{
	try
	{
		// Create an infospace and launch a workloop task that populates data
		// every 5 seconds
		std::string infoSpaceName = toolbox::toString("urn:xdaq-monitorable:MonitoringProducer-%d", getApplicationDescriptor()->getInstance());
		infoSpace_ = xdata::InfoSpace::get(infoSpaceName);

		infoSpace_->fireItemAvailable("counter", &counter_, 0);
		infoSpace_->fireItemAvailable("random", &randomValue_, 0);
		infoSpace_->fireItemAvailable("lastUpdated", &lastExecutionTime_, 0);

		toolbox::TimeInterval interval;
		interval.fromString("00:00:00:00:02");
		toolbox::TimeVal startTime;
		startTime = toolbox::TimeVal::gettimeofday();

		// *********************

		// Pass a pointer to the infoSpace as the context
		// The name of this timer task is "MonitoringProducer"
		timer_->start(); // must activate timer before submission, abort otherwise!!!
		timer_->scheduleAtFixedRate(startTime,this, interval, infoSpace_, "MonitoringProducer" );
	}
	catch (xdaq::exception::Exception& e)
	{
		XCEPT_RETHROW (xgi::exception::Exception, "Failed to start monitoring producer", e);
	}	
	
	this->Default(in,out);
}

void MonitoringProducer::stop(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception)
{
	timer_->remove("MonitoringProducer");
	timer_->stop();
	
	// remove InfoSpace
	std::string infoSpaceName = toolbox::toString("urn:xdaq-monitorable:MonitoringProducer", getApplicationDescriptor()->getInstance());
	xdata::InfoSpace::remove(infoSpaceName);
	this->Default(in,out);
}

void MonitoringProducer::timeExpired (toolbox::task::TimerEvent& e) 
{ 
	std::string name = e.getTimerTask()->name;

	// To assure atomic update of all values, lock the InfoSpace
	// Note: Do not lock the InfoSpace in a callback of an InfoSpace event,
	// e.g. ItemRetrieved
	infoSpace_->lock();
	
	// increment the counter
	counter_ = counter_ + 1;
	lastExecutionTime_ = e.getTimerTask()->lastExecutionTime.toString(toolbox::TimeVal::loc);
	randomValue_ = rand() * 10;
	
	infoSpace_->unlock();
	
}	
