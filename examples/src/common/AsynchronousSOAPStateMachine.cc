// $Id: AsynchronousSOAPStateMachine.cc,v 1.4 2008/07/18 15:26:45 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "AsynchronousSOAPStateMachine.h"

//
// provides factory method for instantion of the application
//
XDAQ_INSTANTIATOR_IMPL(AsynchronousSOAPStateMachine)

AsynchronousSOAPStateMachine::AsynchronousSOAPStateMachine(xdaq::ApplicationStub * s)
	throw (xdaq::exception::Exception)
	: xdaq::Application(s), 
	  fsm_("urn:toolbox-task-workloop:AsynchronousSOAPStateMachineExample") 
	
{	
	//
	// Bind SOAP callback
	//
	xoap::bind(this, &AsynchronousSOAPStateMachine::fireEvent, "Configure", XDAQ_NS_URI );
	xoap::bind(this, &AsynchronousSOAPStateMachine::fireEvent, "Enable", XDAQ_NS_URI );
	xoap::bind(this, &AsynchronousSOAPStateMachine::fireEvent, "Suspend", XDAQ_NS_URI );
	xoap::bind(this, &AsynchronousSOAPStateMachine::fireEvent, "Resume", XDAQ_NS_URI );
	xoap::bind(this, &AsynchronousSOAPStateMachine::fireEvent, "Halt", XDAQ_NS_URI );
	xoap::bind(this, &AsynchronousSOAPStateMachine::reset, "Reset", XDAQ_NS_URI );

	//
	// Define FSM
	//		
	fsm_.addState('H', "Halted", this, &AsynchronousSOAPStateMachine::stateChanged);
	fsm_.addState('R', "Ready", this, &AsynchronousSOAPStateMachine::stateChanged);
	fsm_.addState('E', "Enabled", this, &AsynchronousSOAPStateMachine::stateChanged);
	fsm_.addState('S', "Suspended", this, &AsynchronousSOAPStateMachine::stateChanged);

	fsm_.addStateTransition('H', 'R', "Configure", this, &AsynchronousSOAPStateMachine::ConfigureAction);
	fsm_.addStateTransition('R', 'E', "Enable", this,&AsynchronousSOAPStateMachine::EnableAction);
	fsm_.addStateTransition('E', 'S', "Suspend",this, &AsynchronousSOAPStateMachine::SuspendAction);
	fsm_.addStateTransition('S', 'E', "Resume",this, &AsynchronousSOAPStateMachine::ResumeAction);

	fsm_.addStateTransition('H', 'H', "Halt", this, &AsynchronousSOAPStateMachine::HaltAction);
	fsm_.addStateTransition('R', 'H', "Halt", this, &AsynchronousSOAPStateMachine::HaltAction);
	fsm_.addStateTransition('E', 'H', "Halt", this, &AsynchronousSOAPStateMachine::HaltAction);
	fsm_.addStateTransition('S', 'H', "Halt", this, &AsynchronousSOAPStateMachine::HaltAction);

	// Failure state setting
	fsm_.setFailedStateTransitionAction( this, &AsynchronousSOAPStateMachine::failedTransition );
	fsm_.setFailedStateTransitionChanged(this, &AsynchronousSOAPStateMachine::stateChanged );

	fsm_.setInitialState('H');
	fsm_.setStateName('F', "Failed"); // give a name to the 'F' state

	fsm_.reset();

	// Export a "State" variable that reflects the state of the state machine
	state_ = fsm_.getStateName (fsm_.getCurrentState());
	getApplicationInfoSpace()->fireItemAvailable("State",&state_);
}
	
	
//
// SOAP Callback trigger state change 
//
xoap::MessageReference AsynchronousSOAPStateMachine::fireEvent (xoap::MessageReference msg) 
	throw (xoap::exception::Exception)
{
	xoap::SOAPPart part = msg->getSOAPPart();
	xoap::SOAPEnvelope env = part.getEnvelope();
	xoap::SOAPBody body = env.getBody();
	DOMNode* node = body.getDOMNode();
	DOMNodeList* bodyList = node->getChildNodes();
	for (unsigned int i = 0; i < bodyList->getLength(); i++) 
	{
		DOMNode* command = bodyList->item(i);

		if (command->getNodeType() == DOMNode::ELEMENT_NODE)
		{

			std::string commandName = xoap::XMLCh2String (command->getLocalName());


			try 
			{
				toolbox::Event::Reference e(new toolbox::Event(commandName,this));
				fsm_.fireEvent(e);
			}
			catch (toolbox::fsm::exception::Exception & e)
			{
				XCEPT_RETHROW(xoap::exception::Exception, "invalid command", e);
			}

			xoap::MessageReference reply = xoap::createMessage();
			xoap::SOAPEnvelope envelope = reply->getSOAPPart().getEnvelope();
			xoap::SOAPName responseName = envelope.createName( commandName +"Response", "xdaq", XDAQ_NS_URI);
			// xoap::SOAPBodyElement e = envelope.getBody().addBodyElement ( responseName );
			(void) envelope.getBody().addBodyElement ( responseName );
			return reply;
		}
	}

	XCEPT_RAISE(xoap::exception::Exception,"command not found");		
}

//
// SOAP Callback to reset the state machine
//
xoap::MessageReference AsynchronousSOAPStateMachine::reset (xoap::MessageReference msg) 
	throw (xoap::exception::Exception)
{
	LOG4CPLUS_INFO (getApplicationLogger(), "New state before reset is: " << fsm_.getStateName (fsm_.getCurrentState()) );

	fsm_.reset();
	state_ = fsm_.getStateName (fsm_.getCurrentState());

	xoap::MessageReference reply = xoap::createMessage();
	xoap::SOAPEnvelope envelope = reply->getSOAPPart().getEnvelope();
	xoap::SOAPName responseName = envelope.createName("ResetResponse", "xdaq", XDAQ_NS_URI);
	// xoap::SOAPBodyElement e = envelope.getBody().addBodyElement ( responseName );
	(void) envelope.getBody().addBodyElement ( responseName );

	LOG4CPLUS_INFO (getApplicationLogger(), "New state after reset is: " << fsm_.getStateName (fsm_.getCurrentState()) );

	return reply;
}

//
// Finite State Machine Actions callback
//
void AsynchronousSOAPStateMachine::ConfigureAction (toolbox::Event::Reference e) throw (toolbox::fsm::exception::Exception)
{
	LOG4CPLUS_INFO (getApplicationLogger(), "Entering configure action, sleep 15 seconds...");
	::sleep(15);
	LOG4CPLUS_INFO (getApplicationLogger(), "Exiting configure action");
}
	
void AsynchronousSOAPStateMachine::EnableAction (toolbox::Event::Reference e) throw (toolbox::fsm::exception::Exception) 
{
	LOG4CPLUS_INFO (getApplicationLogger(), "Entering enable action, sleep 15 seconds...");
	::sleep(15);
	LOG4CPLUS_INFO (getApplicationLogger(), "Exiting enable action");
}
	
void AsynchronousSOAPStateMachine::SuspendAction (toolbox::Event::Reference e) throw (toolbox::fsm::exception::Exception) 
{
	LOG4CPLUS_INFO (getApplicationLogger(), e->type());

	// a failure is forced here
	XCEPT_RAISE(toolbox::fsm::exception::Exception, "error in suspend");
}
	
void AsynchronousSOAPStateMachine::ResumeAction (toolbox::Event::Reference e) throw (toolbox::fsm::exception::Exception) 
{
	LOG4CPLUS_INFO (getApplicationLogger(), e->type());

}

void AsynchronousSOAPStateMachine::HaltAction (toolbox::Event::Reference e) throw (toolbox::fsm::exception::Exception) 
{
	LOG4CPLUS_INFO (getApplicationLogger(), e->type());
}
		
void  AsynchronousSOAPStateMachine::stateChanged (toolbox::fsm::FiniteStateMachine & fsm) throw (toolbox::fsm::exception::Exception)
{
	// Reflect the new state
	state_ = fsm.getStateName (fsm.getCurrentState());
	LOG4CPLUS_INFO (getApplicationLogger(), "New state is:" << fsm.getStateName (fsm.getCurrentState()) );
}
	
void AsynchronousSOAPStateMachine::failedTransition (toolbox::Event::Reference e) throw (toolbox::fsm::exception::Exception)
{
	toolbox::fsm::FailedEvent & fe = dynamic_cast<toolbox::fsm::FailedEvent&>(*e);
	LOG4CPLUS_INFO (getApplicationLogger(), "Failure occurred when performing transition from: "  <<
			fe.getFromState() <<  " to: " << fe.getToState() << " exception: " << fe.getException().what() );
}
