// $Id: TableToString.cc,v 1.3 2008/07/21 08:06:57 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/
#include <iostream>
#include <sstream>
 
#include "xcept/tools.h"
#include "xdata/Table.h"
#include "xdata/UnsignedLong.h"
#include "xdata/UnsignedInteger.h"
#include "xdata/UnsignedInteger64.h"
#include "xdata/String.h"
#include "xdata/Double.h"
#include "xdata/exdr/FixedSizeInputStreamBuffer.h"
#include "xdata/exdr/FixedSizeOutputStreamBuffer.h"
#include "xdata/exdr/Serializer.h"



int main (int argc, char** argv)
{
	try
	{
		//Declare a table object
		xdata::Table t;
		t.addColumn("Number", "unsigned long");
		t.addColumn("Text", "string");
		//t.addColumn("Inner", "table");
		
		for (size_t i = 0; i< 5; i++)
		{
			xdata::UnsignedLong  number;
			number = i;
			t.setValueAt(i,"Number", number);
			xdata::String text("This is some text 'with' \"quotes\" inside");
			t.setValueAt(i,"Text", text);
			
			xdata::Table  r;
			r.addColumn("A", "unsigned long");
			r.setValueAt(0,"A", number);
			r.addColumn("B", "unsigned long");
			r.setValueAt(0,"B", number);
			
		//	t.setValueAt(i,"Inner",r);
		}

		std::string outString = t.toString();
		
		std::cout << outString << std::endl;

		xdata::Table t2;

		std::istringstream instream(outString);
		t2.readFrom(instream);

	} 
	catch (xdata::exception::Exception& e)
	{
		std::cout << "Error in create table";
		std::cout << xcept::stdformat_exception_history(e) << std::endl;
	}

	return 0;
}

