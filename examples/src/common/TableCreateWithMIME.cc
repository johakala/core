// $Id: TableCreateWithMIME.cc,v 1.3 2008/07/18 15:26:45 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/
#include <iostream>
 
#include "xdata/Table.h"
#include "xdata/UnsignedLong.h"
#include "xdata/String.h"
#include "xdata/Double.h"
#include "xdata/exdr/FixedSizeInputStreamBuffer.h"
#include "xdata/exdr/FixedSizeOutputStreamBuffer.h"
#include "xdata/exdr/AutoSizeOutputStreamBuffer.h"
#include "xdata/Mime.h"

#include "xdata/exdr/Serializer.h"

#include "mimetic/mimetic.h"
#include "mimetic/codec/code.h"


int main (int argc, char** argv)
{

	try
	{
		//Declare a table object
		xdata::Table t;
		t.addColumn("Name", "string");
		t.addColumn("MIME", "mime");
		
		
		for (size_t i = 0; i< 20; i++)
		{
		
			xdata::String  name;
			name = "10:00:00";
			t.setValueAt(i,"Name", name);
			
			
			xdata::Mime  m;
			mimetic::ImageJpeg* oct = new mimetic::ImageJpeg("/users/xdaq/europe.jpg");

			//const mimetic::Header& header = oct->header();
			//const mimetic::ContentType& ct = header.contentType();
			m.setEntity(oct);
	
			t.setValueAt(i,"MIME", m);
		}
		
		for (size_t i = 0; i< t.getRowCount(); i++)
		{
			xdata::String * n = dynamic_cast<xdata::String *>(t.getValueAt(i,"Name"));
			xdata::Mime * m = dynamic_cast<xdata::Mime *>(t.getValueAt(i,"MIME"));
			
		}
		
		xdata::exdr::Serializer serializer;


        	xdata::exdr::AutoSizeOutputStreamBuffer outBuffer;

        	serializer.exportAll (&t, &outBuffer);

		xdata::exdr::FixedSizeInputStreamBuffer  inBuffer(outBuffer.getBuffer(), outBuffer.tellp());

        	serializer.import(&t, &inBuffer);

		
		for (size_t i = 0; i< t.getRowCount(); i++)
		{
			xdata::String * n = dynamic_cast<xdata::String *>(t.getValueAt(i,"Name"));
			xdata::Mime * m = dynamic_cast<xdata::Mime *>(t.getValueAt(i,"MIME"));
		
		}
		
		
	} 
	catch (xdata::exception::Exception& e)
	{
		std::cout << "Error in create table" << e.what() << std::endl;
	}

	return 0;
}

