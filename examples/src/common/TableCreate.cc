// $Id: TableCreate.cc,v 1.6 2008/07/18 15:26:45 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/
#include <iostream>
 
#include "xcept/tools.h"
#include "xdata/Table.h"
#include "xdata/Boolean.h"
#include "xdata/TimeVal.h"
#include "xdata/Float.h"
#include "xdata/UnsignedLong.h"
#include "xdata/UnsignedShort.h"
#include "xdata/UnsignedInteger.h"
#include "xdata/UnsignedInteger64.h"
#include "xdata/UnsignedInteger32.h"
#include "xdata/String.h"
#include "xdata/Double.h"
#include "xdata/Vector.h"
#include "xdata/exdr/FixedSizeInputStreamBuffer.h"
#include "xdata/exdr/FixedSizeOutputStreamBuffer.h"
#include "xdata/exdr/Serializer.h"

int main (int argc, char** argv)
{
	try
	{
		//Declare a table object
		xdata::Table t;
		t.addColumn("Number", "unsigned long");
		t.addColumn("NumberInt", "unsigned int");
		t.addColumn("NumberInt64", "unsigned int 64");
		t.addColumn("Time", "string");
		t.addColumn("Bandwidth", "double");
		t.addColumn("Rate","double");
		t.addColumn("Latency", "double");
		t.addColumn("Plot32", "vector unsigned int 32");
		t.addColumn("Plot64", "vector unsigned int 64");
		t.addColumn("PlotShort", "vector unsigned short");
		t.addColumn("PlotBoolean", "vector bool");
		t.addColumn("PlotString", "vector string");
		t.addColumn("PlotTimeVal", "vector time");
		t.addColumn("PlotFloat", "vector float");
		t.addColumn("PlotDouble", "vector double");
		
		for (size_t i = 0; i< 10; i++)
		{
			xdata::UnsignedLong  number;
			number = i;
			t.setValueAt(i,"Number", number);
			
			xdata::UnsignedInteger  numberInt;
			numberInt = (i + 0xFFFF0000);
			t.setValueAt(i,"NumberInt", numberInt);
			
			xdata::UnsignedInteger64  numberInt64;
			numberInt64 = (((xdata::UnsignedInteger64T)i) + 0xFFFFFFFF00000000LL);
			t.setValueAt(i,"NumberInt64", numberInt64);
			
			xdata::String  time;
			time = "10:00:00";
			t.setValueAt(i,"Time", time);
			
			xdata::Double  b;
			b = 100.4;
			t.setValueAt(i,"Bandwidth", b);
			
			xdata::Vector<xdata::UnsignedInteger32>  plot32;
			xdata::Vector<xdata::UnsignedInteger64>  plot64;
			xdata::Vector<xdata::UnsignedShort>  plotShort;
			xdata::Vector<xdata::Boolean>  plotBoolean;
			xdata::Vector<xdata::Float>  plotFloat;
			xdata::Vector<xdata::Double>  plotDouble;
			xdata::Vector<xdata::String>  plotString;
			xdata::Vector<xdata::TimeVal>  plotTimeVal;
			xdata::UnsignedInteger32  numberInt32;
			xdata::UnsignedShort  numberShort;
			xdata::Double  numberDouble;
			xdata::Float  numberFloat;
			xdata::TimeVal timeVal;
			for (size_t j = 0; j< i; j++)
			{
				numberInt32 = j;
				numberInt64 = j;
				numberShort = j;
				numberDouble = j;
				numberFloat = j;
				plot32.push_back(numberInt32);
				plot64.push_back(numberInt64);
				plotShort.push_back(numberShort);
				plotBoolean.push_back(true);
				plotFloat.push_back(numberFloat);
				plotDouble.push_back(numberDouble);
				plotString.push_back(numberInt32.toString());
				timeVal = toolbox::TimeVal::gettimeofday();
				plotTimeVal.push_back( timeVal );
			}
			t.setValueAt(i,"Plot32", plot32);
			t.setValueAt(i,"Plot64", plot64);
			t.setValueAt(i,"PlotShort", plotShort);
			t.setValueAt(i,"PlotBoolean", plotBoolean);
			t.setValueAt(i,"PlotTimeVal", plotTimeVal);
			t.setValueAt(i,"PlotString", plotString);
			t.setValueAt(i,"PlotFloat", plotFloat);
			t.setValueAt(i,"PlotDouble", plotDouble);
		}
		
		std::cout << "--- Before serializing ---" << std::endl;
		std::vector<std::string> columns = t.getColumns();
		for (std::vector<std::string>::iterator ci = columns.begin(); ci != columns.end(); ++ci)
		{
			std::cout << "Column '" << (*ci) << "', type '" << t.getColumnType ( *ci ) << "'" << std::endl;
		}

		std::cout << std::endl;
		std::cout << "Number\tNumberInt\tNumberInt64\t\tTime\t\tBandwidth\tRate\tLatency\tPlot32" ;
		std::cout << "\tPlot64\tPlotShort\tPlotBoolean\tPlotFloat\tPlotDouble\tPlotString\tPlotTimeVal" << std::endl;
		
		for (size_t i = 0; i< t.getRowCount(); i++)
		{
			xdata::UnsignedLong * number = dynamic_cast<xdata::UnsignedLong *>(t.getValueAt(i,"Number"));
			xdata::UnsignedInteger * numberInt = dynamic_cast<xdata::UnsignedInteger *>(t.getValueAt(i,"NumberInt"));
			xdata::UnsignedInteger64 * numberInt64 = dynamic_cast<xdata::UnsignedInteger64 *>(t.getValueAt(i,"NumberInt64"));
			xdata::String * time = dynamic_cast<xdata::String *>(t.getValueAt(i,"Time"));
			xdata::Double * b = dynamic_cast<xdata::Double *>(t.getValueAt(i,"Bandwidth"));
			xdata::Double * r = dynamic_cast<xdata::Double *>(t.getValueAt(i,"Rate"));
			xdata::Double * l = dynamic_cast<xdata::Double *>(t.getValueAt(i,"Latency"));
			xdata::Vector<xdata::UnsignedInteger32> * plot32 = dynamic_cast<xdata::Vector<xdata::UnsignedInteger32>*>(t.getValueAt(i,"Plot32"));
			xdata::Vector<xdata::UnsignedInteger64> * plot64 = dynamic_cast<xdata::Vector<xdata::UnsignedInteger64>*>(t.getValueAt(i,"Plot64"));
			xdata::Vector<xdata::UnsignedShort> * plotShort = dynamic_cast<xdata::Vector<xdata::UnsignedShort>*>(t.getValueAt(i,"PlotShort"));
			xdata::Vector<xdata::Boolean> * plotBoolean = dynamic_cast<xdata::Vector<xdata::Boolean>*>(t.getValueAt(i,"PlotBoolean"));
			xdata::Vector<xdata::Float> * plotFloat = dynamic_cast<xdata::Vector<xdata::Float>*>(t.getValueAt(i,"PlotFloat"));
			xdata::Vector<xdata::Double> * plotDouble = dynamic_cast<xdata::Vector<xdata::Double>*>(t.getValueAt(i,"PlotDouble"));
			xdata::Vector<xdata::String> * plotString = dynamic_cast<xdata::Vector<xdata::String>*>(t.getValueAt(i,"PlotString"));
			xdata::Vector<xdata::TimeVal> * plotTimeVal = dynamic_cast<xdata::Vector<xdata::TimeVal>*>(t.getValueAt(i,"PlotTimeVal"));
			
			std::cout << number->toString() << "\t";
			std::cout << numberInt->toString() << "\t";
			std::cout << numberInt64->toString() << "\t";
			std::cout << time->toString() << "\t";
			std::cout << b->toString() << "\t";
			std::cout << r->toString() << "\t";
			std::cout << l->toString() << "\t";
			std::cout << plot32->toString() << "\t";
			std::cout << plot64->toString() << "\t";
			std::cout << plotShort->toString() << "\t";
			std::cout << plotBoolean->toString() << "\t";
			std::cout << plotFloat->toString() << "\t";
			std::cout << plotDouble->toString() << "\t";
			std::cout << plotString->toString() << "\t";
			std::cout << plotTimeVal->toString() << std::endl;
		}
		
		xdata::exdr::Serializer serializer;

        	char buf[0x100000];
        	int size = 0x100000;

        	xdata::exdr::FixedSizeOutputStreamBuffer outBuffer(buf, size);

        	serializer.exportAll (&t, &outBuffer);

        	xdata::exdr::FixedSizeInputStreamBuffer  inBuffer(buf, size);

        	serializer.import(&t, &inBuffer);

		std::cout << std::endl << "--- After serializing ---" << std::endl;
		
		for (size_t i = 0; i< t.getRowCount(); i++)
		{
			xdata::UnsignedLong * number = dynamic_cast<xdata::UnsignedLong *>(t.getValueAt(i,"Number"));
			xdata::UnsignedInteger * numberInt = dynamic_cast<xdata::UnsignedInteger *>(t.getValueAt(i,"NumberInt"));
			xdata::UnsignedInteger64 * numberInt64 = dynamic_cast<xdata::UnsignedInteger64 *>(t.getValueAt(i,"NumberInt64"));
			xdata::String * time = dynamic_cast<xdata::String *>(t.getValueAt(i,"Time"));
			xdata::Double * b = dynamic_cast<xdata::Double *>(t.getValueAt(i,"Bandwidth"));
			xdata::Double * r = dynamic_cast<xdata::Double *>(t.getValueAt(i,"Rate"));
			xdata::Double * l = dynamic_cast<xdata::Double *>(t.getValueAt(i,"Latency"));			
			xdata::Vector<xdata::UnsignedInteger32> * plot32 = dynamic_cast<xdata::Vector<xdata::UnsignedInteger32>*>(t.getValueAt(i,"Plot32"));
			xdata::Vector<xdata::UnsignedInteger64> * plot64 = dynamic_cast<xdata::Vector<xdata::UnsignedInteger64>*>(t.getValueAt(i,"Plot64"));
			xdata::Vector<xdata::UnsignedShort> * plotShort = dynamic_cast<xdata::Vector<xdata::UnsignedShort>*>(t.getValueAt(i,"PlotShort"));
			xdata::Vector<xdata::Boolean> * plotBoolean = dynamic_cast<xdata::Vector<xdata::Boolean>*>(t.getValueAt(i,"PlotBoolean"));
			xdata::Vector<xdata::Float> * plotFloat = dynamic_cast<xdata::Vector<xdata::Float>*>(t.getValueAt(i,"PlotFloat"));
			xdata::Vector<xdata::Double> * plotDouble = dynamic_cast<xdata::Vector<xdata::Double>*>(t.getValueAt(i,"PlotDouble"));
			xdata::Vector<xdata::String> * plotString = dynamic_cast<xdata::Vector<xdata::String>*>(t.getValueAt(i,"PlotString"));
			xdata::Vector<xdata::TimeVal> * plotTimeVal = dynamic_cast<xdata::Vector<xdata::TimeVal>*>(t.getValueAt(i,"PlotTimeVal"));

			
			std::cout << number->toString() << "\t";
			std::cout << numberInt->toString() << "\t";
			std::cout << numberInt64->toString() << "\t";
			std::cout << time->toString() << "\t";
			std::cout << b->toString() << "\t";
			std::cout << r->toString() << "\t";
			std::cout << l->toString() << "\t";
			std::cout << plot32->toString() << "\t";
			std::cout << plot64->toString() << "\t";
			std::cout << plotShort->toString() << "\t";
			std::cout << plotBoolean->toString() << "\t";
			std::cout << plotFloat->toString() << "\t";
			std::cout << plotDouble->toString() << "\t";
			std::cout << plotString->toString() << "\t";
			std::cout << plotTimeVal->toString() << std::endl;
		}
	} 
	catch (xdata::exception::Exception& e)
	{
		std::cout << "Error in create table";
		std::cout << xcept::stdformat_exception_history(e) << std::endl;
	}

	return 0;
}

