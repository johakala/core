// $Id: TableCreatePerformance.cc,v 1.3 2008/07/18 15:26:45 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/
#include <iostream>
 
#include "xdata/Table.h"
#include "xdata/UnsignedLong.h"
#include "xdata/String.h"
#include "xdata/Double.h"
#include "xdata/exdr/AutoSizeOutputStreamBuffer.h"
#include "xdata/exdr/Serializer.h"
#include "toolbox/Chrono.h"

unsigned long counter = 0;
unsigned long depth = 0;
void fillTable (xdata::Table  * t , int level)
{

	//std::cout << "fill table level:" << level << std::endl;
	try
	{
	
		t->addColumn("A", "unsigned long");
		t->addColumn("B", "unsigned long");
		t->addColumn("C", "unsigned long");
		t->addColumn("D", "unsigned long");
		t->addColumn("E", "unsigned long");
		t->addColumn("F", "unsigned long");
		t->addColumn("G", "unsigned long");
		t->addColumn("H", "unsigned long");
		t->addColumn("I", "unsigned long");
		
		if ( level <= depth )
		{
			t->addColumn("T", "table");
		}
		
			
		t->reserve(10);

		for (size_t i = 0; i< 10; i++)
		{
			
			xdata::UnsignedLong  b;
			b = 100;
			t->setValueAt(i,"A", b);
			t->setValueAt(i,"B", b);
			t->setValueAt(i,"C", b);
			t->setValueAt(i,"D", b);
			t->setValueAt(i,"E", b);
			t->setValueAt(i,"F", b);
			t->setValueAt(i,"G", b);
			t->setValueAt(i,"H", b);
			t->setValueAt(i,"I", b);
			
			counter += 9;
			
			if (  level <= depth ) {
				xdata::Table  emptyt;
			
				t->setValueAt(i,"T", emptyt);
				xdata::Table  * cursor = dynamic_cast<xdata::Table*>(t->getValueAt(i,"T"));
			
				//std::cout << "recursive call at level: " << level + 1 << std::endl; 
				fillTable(cursor, level +1 );
			}
			
		}
		
		//std::cout << "Filled table at level:" << level << std::endl;
		

		
		
	} 
	catch (xdata::exception::Exception& e)
	{
		std::cout << "Error in create table" << e.what() << std::endl;
	}

	return ;
}


int main (int argc, char** argv)
{
	toolbox::Chrono c;
	char car;
	std::cin >> car;
	
	c.start(0);
	xdata::Table t;
	depth = atoi(argv[1]);
	
	fillTable( &t, 0 );
	
	c.stop(0);
	
	std::cout << "Total building time in secs:" << c.dusecs() / 1000000 << " usecs:" << c.dusecs()<< std::endl;
	std::cout << "Time per cell (usecs):" << c.dusecs() / counter << std::endl;
	std::cout << "Total number of cells:" << counter <<  std::endl;
	
	c.start(0);
	xdata::exdr::Serializer serializer;
        xdata::exdr::AutoSizeOutputStreamBuffer outBuffer;
        serializer.exportAll (&t, &outBuffer);
	c.stop(0);
	std::cout << "Total serialized size (MBytes):" << outBuffer.tellp() / 0x100000 << " size per cell (bytes)" << outBuffer.tellp() / counter <<  std::endl;
	std::cout << "Total serialization time in secs:" << c.dusecs() / 1000000 << " usecs:" << c.dusecs()<< std::endl;

	std::cin >> car;
	return 0;
}

