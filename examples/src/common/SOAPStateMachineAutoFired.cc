// $Id: SOAPStateMachineAutoFired.cc,v 1.2 2008/07/18 15:26:45 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "SOAPStateMachineAutoFired.h"
#include "xcept/tools.h"

//
// provides factory method for instantion of SOAPStateMachine application
//
XDAQ_INSTANTIATOR_IMPL(SOAPStateMachineAutoFired)

SOAPStateMachineAutoFired::SOAPStateMachineAutoFired(xdaq::ApplicationStub * s)
	throw (xdaq::exception::Exception)
	: xdaq::Application(s) 	
{
	// Bind workloop function
        job_ = toolbox::task::bind(this, &SOAPStateMachineAutoFired::working, "working");
        workLoop_ = toolbox::task::getWorkLoopFactory()->getWorkLoop("SOAPStateMachineAutoFired", "waiting");

	
	//
	// Bind SOAP callbacks
	//
	xoap::bind(this, &SOAPStateMachineAutoFired::Configure, "Configure", XDAQ_NS_URI );	
	xoap::bind(this, &SOAPStateMachineAutoFired::Reset, "Reset", XDAQ_NS_URI );	

	//
	// Define FSM
	//		
	fsm_.addState('i', "initialized");
	fsm_.addState('w', "working", this, &SOAPStateMachineAutoFired::stateChanged);
	fsm_.addState('c', "configured", this, &SOAPStateMachineAutoFired::stateChanged);

	fsm_.addStateTransition('i', 'w', "configure");
	fsm_.addStateTransition('w', 'c', "done");

	fsm_.setInitialState('i');

	fsm_.reset();

	if (!workLoop_->isActive())
        {
                workLoop_->activate();
        }    
}
	
		
xoap::MessageReference SOAPStateMachineAutoFired::Configure (xoap::MessageReference msg) 
	throw (xoap::exception::Exception)
{	
	try 
	{
		toolbox::Event::Reference e(new toolbox::Event("configure",this));
		fsm_.fireEvent(e);
		
		xoap::MessageReference reply = xoap::createMessage();
		xoap::SOAPEnvelope envelope = reply->getSOAPPart().getEnvelope();
		xoap::SOAPName responseName = envelope.createName( "ConfigureResponse", "xdaq", XDAQ_NS_URI);
		(void) envelope.getBody().addBodyElement ( responseName );
		return reply;
	}
	catch (toolbox::fsm::exception::Exception & e)
	{
		XCEPT_RETHROW(xoap::exception::Exception, "invalid command", e);
	}	
}

xoap::MessageReference SOAPStateMachineAutoFired::Reset (xoap::MessageReference msg) 
	throw (xoap::exception::Exception)
{
	fsm_.reset();

	xoap::MessageReference reply = xoap::createMessage();
	xoap::SOAPEnvelope envelope = reply->getSOAPPart().getEnvelope();
	xoap::SOAPName responseName = envelope.createName("ResetResponse", "xdaq", XDAQ_NS_URI);
	(void) envelope.getBody().addBodyElement ( responseName );

	return reply;
}

	
void SOAPStateMachineAutoFired::stateChanged (toolbox::fsm::FiniteStateMachine & fsm) throw (toolbox::fsm::exception::Exception)
{
	// Reflect the new state
	std::string state = fsm.getStateName (fsm.getCurrentState());
	LOG4CPLUS_INFO (getApplicationLogger(), "New state is:" << state );
	
	if (state == "working")
	{
		// submit the job in working state
		try
        	{
                	workLoop_->submit(job_);
        	}
        	catch (xdaq::exception::Exception& e)
        	{
                	LOG4CPLUS_ERROR (getApplicationLogger(), xcept::stdformat_exception_history(e));
        	}
	}
	else if (state == "done")
	{
		LOG4CPLUS_INFO (getApplicationLogger(), "Finished working");	
	}
}

bool SOAPStateMachineAutoFired::working(toolbox::task::WorkLoop* wl)
{
	LOG4CPLUS_INFO(getApplicationLogger(), "Start working for 5 seconds...");
	::sleep(5);
	LOG4CPLUS_INFO(getApplicationLogger(), "Finished working!");
	
	toolbox::Event::Reference e(new toolbox::Event("done",this));
	fsm_.fireEvent(e);
	
	// Return false to return to terminate job
	return false;
}

