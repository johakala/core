// $Id: SynchronousStateMachine.cc,v 1.2 2008/07/18 15:26:45 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "SynchronousStateMachine.h"

MyStateMachine::MyStateMachine()
{		
		fsm_.addState('S', "Initial state", this, &MyStateMachine::stateChanged);
		fsm_.addState('A', "State A",       this, &MyStateMachine::stateChanged);
		fsm_.addState('B', "State B",       this, &MyStateMachine::stateChanged);
		fsm_.addState('C', "State C",       this, &MyStateMachine::stateChanged);
	
		fsm_.addStateTransition('S', 'A', "Go", this,    &MyStateMachine::enter);
		fsm_.addStateTransition('A', 'B', "Run", this,   &MyStateMachine::enter);
		fsm_.addStateTransition('B', 'A', "Halt",this,   &MyStateMachine::enter);
		fsm_.addStateTransition('A', 'C', "Finish",this, &MyStateMachine::enter);
	
		fsm_.setFailedStateTransitionAction( this, &MyStateMachine::failed );
		fsm_.setFailedStateTransitionChanged(this, &MyStateMachine::inFailed );
	
		fsm_.setInitialState('S');
		fsm_.setStateName('F', "Failed state"); // give a name to the 'F' state
		fsm_.reset();
}
	
void MyStateMachine::input(const std::string& in)
{
	try 
	{
		toolbox::Event::Reference e(new toolbox::Event(in,this));
		fsm_.fireEvent(e);
	}
	catch (toolbox::fsm::exception::Exception & e)
	{
		std::cout << "Invalid input " << in << ", error: " << e.what() << std::endl;
	}		
}	

void MyStateMachine::enter (toolbox::Event::Reference e) throw (toolbox::fsm::exception::Exception)
{
	std::cout << "Event: [" << e->type() << "]" << std::endl;
}

void MyStateMachine::failed (toolbox::Event::Reference e) throw (toolbox::fsm::exception::Exception)
{
	toolbox::fsm::FailedEvent & fe = dynamic_cast<toolbox::fsm::FailedEvent&>(*e);
	std::cout << "Failure occurred when performing transition from: ";
	std::cout << fe.getFromState() <<  " to: " << fe.getToState();
	std::cout << ", exception: " << fe.getException().what();
	std::cout << std::endl;
}

void MyStateMachine::injectFailure (toolbox::Event::Reference e) throw (toolbox::fsm::exception::Exception)
{
	std::cout << "Event: [" << e->type() << "], raising exception" << std::endl;
	XCEPT_RAISE (toolbox::fsm::exception::Exception, "injected failure");
}
	
void MyStateMachine::stateChanged (toolbox::fsm::FiniteStateMachine & fsm) throw (toolbox::fsm::exception::Exception)
{
	// Reflect the new state
	std::cout << "Current state is: [" << fsm.getStateName (fsm.getCurrentState()) << "]" << std::endl;
}
	
void MyStateMachine::inFailed (toolbox::fsm::FiniteStateMachine & fsm) throw (toolbox::fsm::exception::Exception)
{
	std::cout << "Entered state: [" << fsm.getStateName (fsm.getCurrentState()) << "]" << std::endl;
}
	
void MyStateMachine::test()
{
	fsm_.reset();
	this->input("Go");
	this->input("Run");
	this->input("Halt");
	this->input("Finish");
}

void MyStateMachine::testWithError()
{
	fsm_.reset();
	this->input("Go");
	this->input("Halt"); // transition not allowed
	this->input("Run");  // we may continue to cycle
	this->input("Halt");
	this->input("Finish");
}

void MyStateMachine::testWithFailure()
{
	// Override the existing transition from A->B with an function that raises
	fsm_.addStateTransition('A', 'B', "Run", this,  &MyStateMachine::injectFailure);

	fsm_.reset();
	this->input("Go");
	this->input("Run"); // will fail
	this->input("Halt");
	this->input("Finish");
}

int main(int argc, char** argv)
{
	MyStateMachine m;

	std::string test = "simple";
	if (argc>1)
		test = argv[1];

	if (test == "simple")
		m.test();
	else if (test == "error")
		m.testWithError();
	else if (test == "failure")
		m.testWithFailure();

	return 0;
}

