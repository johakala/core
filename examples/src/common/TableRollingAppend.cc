// $Id: TableRollingAppend.cc,v 1.3 2008/07/18 15:26:46 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/
#include <iostream>
 
#include "xdata/TableIterator.h"
#include "xdata/Table.h"
#include "xdata/UnsignedLong.h"
#include "xdata/String.h"
#include "xdata/Double.h"
#include "toolbox/TimeVal.h"
#include "xcept/tools.h"

void append1(xdata::Table* table, xdata::UnsignedLongT i)
{
		size_t line = table->getRowCount();
		
		std::cout << "table size: " << line << std::endl;

		// Insert at the end
		toolbox::TimeVal current;
		current = toolbox::TimeVal::gettimeofday();

		xdata::String ts = current.toString("%Y:%m:%d:%H:%M:%S",toolbox::TimeVal::loc);
		xdata::UnsignedLong value = i;
		
		table->setValueAt (line, "timestamp", ts);
		table->setValueAt (line, "value", value); 
				
		if (line >= 10)
		{
			// erase first line
			std::cout << "Line count is " << line << ", erase first line" << std::endl;
			table->erase (table->begin());
			std::cout << "Line count after erase is " << table->getRowCount() << std::endl;	
		}
}

void append2(xdata::Table* table, size_t i)
{
	toolbox::TimeVal current;
	current = toolbox::TimeVal::gettimeofday();

	xdata::String ts = current.toString("%Y:%m:%d:%H:%M:%S",toolbox::TimeVal::loc);
	xdata::UnsignedLong value = i;

	if (table->getRowCount() >= 10)
	{
		std::cout << "Row count is " << table->getRowCount() << ", erase table begin" << std::endl;
		table->erase(table->begin());
	}
	else
	{
		std::cout << "Row count is " << table->getRowCount() << ", do not erase" << std::endl;
	}
	
	xdata::Table::Row& row = *(table->append());
	row.setField("timestamp", ts);
	row.setField("value", value);
}


int main (int argc, char** argv)
{

	try
	{
		//Declare a table object
		xdata::Table table;
		table.reserve(10);
		table.addColumn("timestamp", "string");
		table.addColumn("value", "unsigned long");

		for (size_t i = 0; i < 1000; i++)
		{
			append2(&table, i);
		}
		
		for (size_ti = 0; i< table.getRowCount(); i++)
		{
			xdata::UnsignedLong * number = dynamic_cast<xdata::UnsignedLong *>(table.getValueAt(i,"value"));
			xdata::String * time = dynamic_cast<xdata::String *>(table.getValueAt(i,"timestamp"));
			
			std::cout << number->toString() << "\t" << time->toString() << std::endl;
		}
	} 
	catch (xdata::exception::Exception& e)
	{
		std::cout << xcept::stdformat_exception_history(e) << std::endl;
	}
	catch (...)
	{
		std::cout << "Unknown exception caught" << std::endl;
	}

	return 0;
}

