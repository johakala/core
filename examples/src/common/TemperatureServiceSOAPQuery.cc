// $Id: TemperatureServiceSOAPQuery.cc,v 1.2 2008/07/18 15:26:46 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "TemperatureServiceSOAPQuery.h"

//
// provides factory method for instantion of the application
//
XDAQ_INSTANTIATOR_IMPL(TemperatureServiceSOAPQuery)
