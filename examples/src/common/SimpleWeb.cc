// $Id: SimpleWeb.cc,v 1.5 2008/07/18 15:26:45 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "SimpleWeb.h"

//
// provides factory method for instantion of SimpleWeb application
//
XDAQ_INSTANTIATOR_IMPL(SimpleWeb)

SimpleWeb::SimpleWeb(xdaq::ApplicationStub * s)
	throw (xdaq::exception::Exception): xdaq::Application(s)
{	
	xgi::bind(this,&SimpleWeb::Default, "Default");
}
	
void SimpleWeb::Default(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception)
{
	out->getHTTPResponseHeader().addHeader("Content-Type", "text/html");
	*out << cgicc::HTMLDoctype(cgicc::HTMLDoctype::eStrict) << std::endl;
	*out << cgicc::html().set("lang", "en").set("dir","ltr") << std::endl;
	*out << cgicc::title("Simple Web") << std::endl;
	*out << cgicc::a("Visit the XDAQ documentation site").set("href","http://xdaqwiki.cern.ch") << endl;
}
