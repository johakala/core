// $Id: SimpleSOAPDeserializer.cc,v 1.2 2008/07/18 15:26:45 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/
 
#include "xdata/xdata.h"
#include "xdata/XMLDOM.h"

#include "xdata/Serializable.h"
#include "xdata/soap/Serializer.h"
#include "xdata/exception/Exception.h"
#include "xdata/XStr.h"
#include "xdata/Float.h"
#include "xdata/Integer.h"
#include "xdata/Boolean.h"
#include "xdata/String.h"

#include "xoap/MessageFactory.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPName.h"
#include "xoap/SOAPBody.h"
#include "xoap/SOAPBodyElement.h"
#include "xoap/DOMParser.h"
#include "xoap/domutils.h"

#include "xcept/tools.h"

int main(int argc, char** argv)
{
	// Initialize XML platform in standalone main program only
	XMLPlatformUtils::Initialize();
	
	xoap::DOMParser* parser = xoap::DOMParser::get("ParseFromSOAP");
	std::string data = "<soap-env:Envelope soap-env:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\" xmlns:soap-env=\"http://schemas.xmlsoap.org/soap/envelope/\"><soap-env:Header/><soap-env:Body><xdaq:data xmlns:xdaq=\"urn:xdaq-soap:3.0\"><xdaq:myInteger xmlns:soapenc=\"http://schemas.xmlsoap.org/soap/encoding\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:type=\"xsd:integer\">1</xdaq:myInteger><xdaq:myFloat xmlns:soapenc=\"http://schemas.xmlsoap.org/soap/encoding\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:type=\"xsd:float\">3.141000</xdaq:myFloat><xdaq:myBoolean xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:type=\"xsd:boolean\">true</xdaq:myBoolean><xdaq:myString xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:type=\"xsd:string\">Example text</xdaq:myString></xdaq:data></soap-env:Body></soap-env:Envelope>";

	DOMDocument* doc = 0;
	try
	{
		doc = parser->parse (data);
	} 
	catch (xoap::exception::Exception& e)
	{
		std::cout << xcept::stdformat_exception_history(e) << std::endl;
		return 1;
	}
	
	// Declare a SOAP serializer
	xdata::soap::Serializer serializer;
	
	xdata::Integer i;
	xdata::Float f;
	xdata::Boolean b;
	xdata::String s;
	
	// Find the <xdaq:xdata> tag
	DOMNodeList* dataNode = doc->getElementsByTagNameNS(xoap::XStr("urn:xdaq-soap:3.0"), xoap::XStr("data"));
	
	// We expect only one data node
	DOMNodeList* dataElements = dataNode->item(0)->getChildNodes();
	
	for (unsigned int j = 0; j < dataElements->getLength(); j++)
	{
		DOMNode* n = dataElements->item(j);
		if (n->getNodeType() == DOMNode::ELEMENT_NODE)
		{	
			std::string nodeName = xoap::XMLCh2String(n->getNodeName());	
			if (nodeName == "xdaq:myInteger")
			{
				serializer.import (&i, n);
			} 
			else if (nodeName == "xdaq:myFloat")
			{
				serializer.import (&f, n);
			} 
			else if (nodeName == "xdaq:myBoolean")
			{
				serializer.import (&b, n);
			} 
			else if (nodeName == "xdaq:myString")
			{
				serializer.import (&s, n);
			}
			
		}
	}
	
	std::cout << "Read integer: " << i.toString() << std::endl;
	std::cout << "Read float: " << f.toString() << std::endl;
	std::cout << "Read boolean: " << b.toString() << std::endl;
	std::cout << "Read string: " << s.toString() << std::endl;
	
	// Remove the parser and therefore release the DOM document
	xoap::DOMParser::remove("ParseFromSOAP");
	
	return 0;
}
