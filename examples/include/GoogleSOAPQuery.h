// $Id: GoogleSOAPQuery.h,v 1.3 2008/07/18 15:26:44 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _GoogleSOAPQuery_h_
#define _GoogleSOAPQuery_h_

#include "xdaq/Application.h"
#include "xdaq/ApplicationGroup.h"
#include "xdaq/ApplicationContext.h"
#include "xdaq/ApplicationStub.h"
#include "xdaq/exception/Exception.h"

#include "xdaq/NamespaceURI.h"

#include "xoap/MessageReference.h"
#include "xoap/MessageFactory.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPBody.h"
#include "xoap/Method.h"

#include "xgi/Utils.h"
#include "xgi/Method.h"
#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"

#include "xdata/String.h"
#include "xdata/UnsignedLong.h"

// This is a simple example for sending a SOAP message. It can be used in combination
// with the SimpleSOAPReceiver application.

/*
<?xml version='1.0' encoding='UTF-8'?>

<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/1999/XMLSchema-instance" xmlns:xsd="http://www.w3.org/1999/XMLSchema">
  <SOAP-ENV:Body>
    <ns1:doGoogleSearch xmlns:ns1="urn:GoogleSearch" 
         SOAP-ENV:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
      <key xsi:type="xsd:string">00000000000000000000000000000000</key>
      <q xsi:type="xsd:string">shrdlu winograd maclisp teletype</q>
      <start xsi:type="xsd:int">0</start>
      <maxResults xsi:type="xsd:int">10</maxResults>
      <filter xsi:type="xsd:boolean">true</filter>
      <restrict xsi:type="xsd:string"></restrict>
      <safeSearch xsi:type="xsd:boolean">false</safeSearch>
      <lr xsi:type="xsd:string"></lr>
      <ie xsi:type="xsd:string">latin1</ie>
      <oe xsi:type="xsd:string">latin1</oe>
    </ns1:doGoogleSearch>
  </SOAP-ENV:Body>
</SOAP-ENV:Envelope>
*/

class GoogleSOAPQuery: public xdaq::Application  
{
	
	public:
	
	XDAQ_INSTANTIATOR();
	
	GoogleSOAPQuery(xdaq::ApplicationStub * s) throw (xdaq::exception::Exception)
		: xdaq::Application(s)
	{	
		// A simple web control interface
		xgi::bind(this,&GoogleSOAPQuery::Default, "Default");
		xgi::bind(this,&GoogleSOAPQuery::submitQueryForm, "submitQueryForm");
	}
	
	void Default(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception)
	{
		*out << cgicc::HTMLDoctype(cgicc::HTMLDoctype::eStrict) << std::endl;
		*out << cgicc::html().set("lang", "en").set("dir","ltr") << std::endl;
		*out << cgicc::title("Google Query") << std::endl;

		xgi::Utils::getPageHeader
			(out, 
			"Google Query", 
			getApplicationDescriptor()->getContextDescriptor()->getURL(),
			getApplicationDescriptor()->getURN(),
			"/daq/xgi/images/Application.jpg"
			);

		std::string url = "/";
		url += getApplicationDescriptor()->getURN();
		url += "/submitQueryForm";	
		*out << cgicc::form().set("method","post").set("action", url).set("enctype","multipart/form-data") << std::endl;
		
		*out << cgicc::label("Search ") << std::endl;
		*out << cgicc::input().set("type", "text").set("name", "searchString").set("size","35") << std::endl;
		
		*out << "<br>";
		
		*out << cgicc::label("Google key ") << std::endl;
		*out << cgicc::input().set("type", "text").set("name", "searchKey").set("size","35") << std::endl;
		
		*out << "<br>";
		*out << cgicc::input().set("type", "submit").set("name", "send").set("value", "Query");
		*out << cgicc::form() << std::endl;

		xgi::Utils::getPageFooter(*out);	
	}
	
	void submitQueryForm(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception)
	{
		// Retrieve the search string and the Google Web API key
		std::string searchString = "";
		std::string searchKey = "";
		try 
		{		
			cgicc::Cgicc cgi(in);
			searchString = cgi["searchString"]->getValue();
			searchKey = cgi["searchKey"]->getValue();
			
			std::cout << "Q: " << searchString << std::endl;
			std::cout << "K: " << searchKey << std::endl;
			
		} 
		catch (std::exception& e)
		{
			std::string msg = "An error occurred while interpreting the form content: ";
			msg += e.what();
			XCEPT_RAISE (xgi::exception::Exception, msg);
		}
	
	  	xoap::MessageReference msg = xoap::createMessage();
		
		try
		{
                	xoap::SOAPPart soap = msg->getSOAPPart();
                	xoap::SOAPEnvelope envelope = soap.getEnvelope();
			
			envelope.addNamespaceDeclaration ("xsi", "http://www.w3.org/1999/XMLSchema-instance");
			envelope.addNamespaceDeclaration ("xsd", "http://www.w3.org/1999/XMLSchema");
			
                	xoap::SOAPBody body = envelope.getBody();
                	xoap::SOAPName command = envelope.createName("doGoogleSearch","ns1", "urn:GoogleSearch");
                	xoap::SOAPElement bodyElement = body.addBodyElement(command);

			xoap::SOAPName xsiType = envelope.createName("type","xsi", "http://www.w3.org/1999/XMLSchema-instance");
			xoap::SOAPName encodingStyle = envelope.createName("encodingStyle","soap-env", "http://schemas.xmlsoap.org/soap/envelope/");
			
			bodyElement.addAttribute(encodingStyle, "http://schemas.xmlsoap.org/soap/encoding/");


			/* <key xsi:type="xsd:string">00000000000000000000000000000000</key>
			*/
			xoap::SOAPName key = envelope.createName("key");
                	xoap::SOAPElement keyElement = bodyElement.addChildElement(key);
			keyElement.addAttribute(xsiType, "xsd:string");
			keyElement.addTextNode(searchKey);

			/*  <q xsi:type="xsd:string">shrdlu winograd maclisp teletype</q>
			*/
			xoap::SOAPName query = envelope.createName("q");
                	xoap::SOAPElement queryElement = bodyElement.addChildElement(query);
			queryElement.addAttribute(xsiType, "xsd:string");
			queryElement.addTextNode(searchString);
			
			     
			/* <start xsi:type="xsd:int">0</start>
			*/
			xoap::SOAPName start = envelope.createName("start");
                	xoap::SOAPElement startElement = bodyElement.addChildElement(start);
			startElement.addAttribute(xsiType, "xsd:int");
			startElement.addTextNode("0");
			
			/* <maxResults xsi:type="xsd:int">10</maxResults> 
			*/
			xoap::SOAPName maxResults = envelope.createName("maxResults");
                	xoap::SOAPElement maxResultsElement = bodyElement.addChildElement(maxResults);
			maxResultsElement.addAttribute(xsiType, "xsd:int");
			maxResultsElement.addTextNode("10");
			
			/* <filter xsi:type="xsd:boolean">true</filter>
			*/
			xoap::SOAPName filter = envelope.createName("filter");
                	xoap::SOAPElement filterElement = bodyElement.addChildElement(filter);
			filterElement.addAttribute(xsiType, "xsd:boolean");
			filterElement.addTextNode("true");
			
			/* <restrict xsi:type="xsd:string"></restrict> */
			xoap::SOAPName restrict = envelope.createName("restrict");
                	xoap::SOAPElement restrictElement = bodyElement.addChildElement(restrict);
			restrictElement.addAttribute(xsiType, "xsd:string");
			/* restrictElement.addTextNode("true"); */
			
			/* <safeSearch xsi:type="xsd:boolean">false</safeSearch> */
			xoap::SOAPName safeSearch = envelope.createName("safeSearch");
                	xoap::SOAPElement safeSearchElement = bodyElement.addChildElement(safeSearch);
			safeSearchElement.addAttribute(xsiType, "xsd:boolean");
			safeSearchElement.addTextNode("false");
			
			/* <lr xsi:type="xsd:string"></lr> */
			xoap::SOAPName lr = envelope.createName("lr");
                	xoap::SOAPElement lrElement = bodyElement.addChildElement(lr);
			lrElement.addAttribute(xsiType, "xsd:string");
			/* lrElement.addTextNode("false"); */
			
			/* <ie xsi:type="xsd:string">latin1</ie> */
			xoap::SOAPName ie = envelope.createName("ie");
                	xoap::SOAPElement ieElement = bodyElement.addChildElement(ie);
			ieElement.addAttribute(xsiType, "xsd:string");
			ieElement.addTextNode("latin1");
			
			/* <oe xsi:type="xsd:string">latin1</oe> */
			xoap::SOAPName oe = envelope.createName("oe");
                	xoap::SOAPElement oeElement = bodyElement.addChildElement(oe);
			oeElement.addAttribute(xsiType, "xsd:string");
			oeElement.addTextNode("latin1");			
		}
		catch(xoap::exception::Exception& xe)
		{
			XCEPT_RETHROW (xgi::exception::Exception, "Cannot create message", xe);	
		}

		try
		{	
			xdaq::ApplicationDescriptor * d = 
				getApplicationContext()->getApplicationGroup()->getApplicationDescriptor("GoogleSearchAction", 0);
				
			std::cout << "Sending SOAPAction: " << d->getURN() << std::endl;
			
			std::cout << "*** MSG ***" << std::endl;
			msg->writeTo(std::cout);
			std::cout << "*** MSG ***" << std::endl << std::endl;
				
			xoap::MessageReference reply = getApplicationContext()->postSOAP(msg, d, "urn:GooleSearchAction");
			
			std::cout << "------------------ RECEIVED ----------------------" << std::endl;
			reply->writeTo(std::cout);
			std::cout << std::endl;
		} 
		catch (xdaq::exception::Exception& e)
		{
			XCEPT_RETHROW (xgi::exception::Exception, "Cannot send message", e);		
		}
				
		this->Default(in,out);

	}

	
};

#endif
