// $Id: Namespace.h,v 1.2 2008/07/18 15:26:45 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _Namespace_h_
#define _Namespace_h_

#include "xdaq/Application.h"



namespace examples {

class Namespace: public xdaq::Application  
{
	
	public:

	XDAQ_INSTANTIATOR();
		
	Namespace(xdaq::ApplicationStub * s);
};
}

#endif
