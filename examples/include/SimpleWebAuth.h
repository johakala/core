// $Id: SimpleWebAuth.h,v 1.4 2008/07/18 15:26:44 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _SimpleWebAuth_h_
#define _SimpleWebAuth_h_

#include "xdaq/Application.h"
#include "xgi/Utils.h"
#include "xgi/Method.h"

#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"
#include "cgicc/HTTPResponseHeader.h"
#include "cgicc/HTMLDoctype.h"

class SimpleWebAuth: public xdaq::Application 
{	
	public:
	
	SimpleWebAuth(xdaq::ApplicationStub * s): xdaq::Application(s) 
	{	
		xgi::bind(this,&SimpleWebAuth::Default, "Default");
	}
	
	
	void Default(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception)
	{	
		try 
		{		
			cgicc::Cgicc cgi(in);

			const cgicc::CgiEnvironment& env = cgi.getEnvironment();
			string remoteuser = env.getRemoteUser();
			string serversw = env.getServerSoftware();
			string clientsw = env.getUserAgent();
			string authtype = env.getAuthType();

			if(remoteuser.empty()) 
			{

				out->getHTTPResponseHeader().getStatusCode(401);
				out->getHTTPResponseHeader().getReasonPhrase("Unauthorized");
				out->getHTTPResponseHeader().addHeader("WWW-Authenticate", "Basic realm=\"cgicc\"");

				// do not add html data: browsers should not display this anyway
				//  they should request user/password from the user and re-emit
				//  the same request, only with the authentification info added
				//  to the request 
				*out << cgicc::HTMLDoctype( cgicc::HTMLDoctype::eStrict) << endl;
				*out << cgicc::html().set("lang", "EN").set("dir", "LTR") << endl;
				*out << cgicc::head() << endl;

				*out << cgicc::title("401 Authorization Required")  << endl;
				*out << cgicc::head() << endl;
				*out << cgicc::body() << endl;

				*out << cgicc::h1("401 Authorization Required") << endl;
				*out << cgicc::p() << "This server could not verify that you are "
				<< "authorized to access the document requested. Either you "
				<< "supplied the wrong credentials (e.g., bad password), or "
				<< "your browser doesn't understand how to supply the "
				<< "credentials required." << cgicc::p();
				*out << cgicc::hr() << endl;
				*out << cgicc::address() << "GNU cgicc \"server\" version " << cgi.getVersion()
				<<  cgicc::address() << endl;
				return;

			}

			// Output the HTTP headers 200 OK header for an HTML document, and
			// the HTML 4.0 DTD info

			*out << cgicc::HTMLDoctype(cgicc::HTMLDoctype::eStrict) << endl;
			*out << cgicc::html().set("lang", "EN").set("dir", "LTR") << endl;

			// Set up the page's header and title.
			*out << cgicc::head() << endl;
			*out << cgicc::title() << "GNU cgicc v" << cgi.getVersion() << cgicc::title() << endl;
			*out << cgicc::head() << endl;

			// Start the HTML body
			*out << cgicc::body() << endl;

			// Print out a message

			*out << "Hello " << env.getRemoteUser() 
			<< " your login was accepted" << cgicc::br() << endl;
			*out << "You were authenticated using authentication scheme : " 
			<< env.getAuthType() << cgicc::br() << endl;
			*out << "Your server software is :" << serversw << cgicc::br() << endl;
			*out << "Your browser software is :" << clientsw << cgicc::br() << endl;
			// Close the document
			*out << cgicc::body() <<  cgicc::html();
		}
		catch(const exception& e) 
		{
			// handle error condition
		}
	}
};

#endif
