// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini                                    *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _SimpleB2INConfReceiver_h_
#define _SimpleB2INConfReceiver_h_

#include "xcept/tools.h"
#include "toolbox/TimeVal.h"
#include "xdata/String.h"
#include "xdata/UnsignedInteger32.h"
#include "xdata/Table.h"
#include "xdata/Boolean.h"
#include "xdata/TimeVal.h"
#include "xdata/Float.h"
#include "xdata/UnsignedLong.h"
#include "xdata/UnsignedShort.h"
#include "xdata/UnsignedInteger.h"
#include "xdata/UnsignedInteger64.h"
#include "xdata/UnsignedInteger32.h"
#include "xdata/String.h"
#include "xdata/Double.h"
#include "xdata/Vector.h"
#include "xdata/exdr/FixedSizeInputStreamBuffer.h"
#include "xdata/exdr/FixedSizeOutputStreamBuffer.h"
#include "xdata/exdr/Serializer.h"
#include "xdaq/Application.h"
#include "xdaq/ApplicationContext.h"
#include "xdaq/ApplicationStub.h"
#include "xdaq/exception/Exception.h"
#include "xdaq/NamespaceURI.h"

#include "b2in/nub/Method.h"

class SimpleB2INConfReceiver: public xdaq::Application
{

	public:

	XDAQ_INSTANTIATOR();

	SimpleB2INConfReceiver(xdaq::ApplicationStub* s): xdaq::Application(s)
	{
		//
		// Bind callback
		//
		b2in::nub::bind(this, &SimpleB2INConfReceiver::onMessage);
		counter = 0;
		start_ = toolbox::TimeVal::gettimeofday();
	}

	//
	//  Callback  
	//
	void onMessage (toolbox::mem::Reference* msg, xdata::Properties& plist) throw (b2in::nub::exception::Exception)
	{
		//std::cout << "received message" << std::endl;

		if (plist.getProperty("flashlist") != "example")
		{
			std::cout << "Corrupted plist: " << plist.getProperty("flashlist") << std::endl;
		}
		else if (msg != 0)
		{
			xdata::Table table;
			xdata::exdr::FixedSizeInputStreamBuffer inBuffer((char*) msg->getDataLocation(), msg->getDataSize());
			try
			{
				serializer_.import(&table, &inBuffer);
			}
			catch (xdata::exception::Exception& e)
			{
				std::cout << "Failed to deserialize incoming table: " << xcept::stdformat_exception_history(e) << std::endl;
			}
		}

		if ((counter % 10000) == 9999)
		{
			toolbox::TimeVal stop = toolbox::TimeVal::gettimeofday();
			std::cout << "Time: " << (double)(stop - start_) << ", rate: " << 10000 / ((double)(stop - start_)) << std::endl;
			start_ = toolbox::TimeVal::gettimeofday();
		}
		counter++;

		if (msg != 0)
		{
			msg->release();
		}
	}

	xdata::exdr::Serializer serializer_;
	size_t counter;
	toolbox::TimeVal start_;
};

#endif
