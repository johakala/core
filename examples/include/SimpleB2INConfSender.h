// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini and G. Lo Presti                   *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _SimpleB2INConfSender_h_
#define _SimpleB2INConfSender_h_

#include <string>
#include <stack>
#include <set>
#include <map>
#include <list>

#include "xcept/tools.h"
#include "xdaq/Application.h"
#include "xdaq/ApplicationContext.h"
#include "xoap/exception/Exception.h"
#include "xoap/MessageReference.h"

#include "xdata/ActionListener.h"
#include "xdata/String.h"
#include "xdata/UnsignedInteger32.h"
#include "xdata/Table.h"
#include "xdata/Boolean.h"
#include "xdata/TimeVal.h"
#include "xdata/Float.h"
#include "xdata/UnsignedLong.h"
#include "xdata/UnsignedShort.h"
#include "xdata/UnsignedInteger.h"
#include "xdata/UnsignedInteger64.h"
#include "xdata/UnsignedInteger32.h"
#include "xdata/String.h"
#include "xdata/Double.h"
#include "xdata/Vector.h"
#include "xdata/exdr/FixedSizeInputStreamBuffer.h"
#include "xdata/exdr/FixedSizeOutputStreamBuffer.h"
#include "xdata/exdr/Serializer.h"

#include "toolbox/Properties.h"
#include "toolbox/ActionListener.h"
#include "toolbox/task/WorkLoopFactory.h"
#include "toolbox/task/WaitingWorkLoop.h"
#include "toolbox/task/Action.h"
#include "toolbox/task/TimerEvent.h"
#include "toolbox/task/TimerListener.h"
#include "toolbox/task/TimerFactory.h"
#include "toolbox/BSem.h"
#include "xplore/utils/DescriptorsCache.h"
#include "xplore/Interface.h"
#include "xplore/exception/Exception.h"
#include "toolbox/mem/Reference.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "toolbox/mem/Pool.h"
#include "toolbox/mem/CommittedHeapAllocator.h"
#include "toolbox/exception/Handler.h"
#include "toolbox/net/URN.h"

#include "pt/PeerTransportAgent.h"
#include "pt/tcp/Address.h"
#include "b2in/nub/Messenger.h"
#include "xgi/Method.h"

class SimpleB2INConfSender: public xdaq::Application, public toolbox::task::TimerListener
{
public:

	XDAQ_INSTANTIATOR();

	SimpleB2INConfSender(xdaq::ApplicationStub* s) throw (xdaq::exception::Exception): xdaq::Application(s)
	{
		xgi::bind(this, &SimpleB2INConfSender::start, "start");

		toolbox::mem::CommittedHeapAllocator* a = new toolbox::mem::CommittedHeapAllocator(1024 * 1024 * 100);
		toolbox::net::URN urn("b2in", "tx");
		pool_ = toolbox::mem::getMemoryPoolFactory()->createPool(urn, a);
	}

	void start(xgi::Input* in, xgi::Output* out) throw (xgi::exception::Exception)
	{
		this->buildMessengerTable();

		if (!toolbox::task::getTimerFactory()->hasTimer("b2in"))
		{
			toolbox::task::Timer* timer = toolbox::task::getTimerFactory()->createTimer("b2in");
			toolbox::TimeInterval interval;
			interval.fromString("PT1S"); // in seconds
			toolbox::TimeVal start;
			start = toolbox::TimeVal::gettimeofday();
			timer->scheduleAtFixedRate(start, this, interval, 0, "tx");
		}
	}

	void buildMessengerTable()
	{

		xdaq::Network* network = this->getApplicationContext()->getNetGroup()->getNetwork("test");
		xdaq::ApplicationDescriptor* d = getApplicationContext()->getDefaultZone()->getApplicationDescriptor("SimpleB2INConfReceiver", 0);

		pt::Address::Reference remote = network->getAddress(d->getContextDescriptor());

		pt::Address::Reference local = network->getAddress(this->getApplicationContext()->getContextDescriptor());

		// 5) create messenger for this address
		// It would be good to retrieve the local address always from the pt directly
		//
		pt::Messenger::Reference mr = pt::getPeerTransportAgent()->getMessenger(remote, local);

		// 6) cache the messenger for performance
		//
		messenger_ = mr;
	}

	void prepareMessage(toolbox::mem::Reference* ref, size_t maxSize, xdata::Table* table)
	{
		xdata::exdr::Serializer serializer;
		xdata::exdr::FixedSizeOutputStreamBuffer outBuffer((char*)ref->getDataLocation(), maxSize);
		serializer.exportAll(table, &outBuffer);

		ref->setDataSize(outBuffer.tellp());
	}

	void prepareTable (xdata::Table* table)
	{
		table->addColumn("Number", "unsigned long");
		table->addColumn("NumberInt", "unsigned int");
		table->addColumn("NumberInt64", "unsigned int 64");
		table->addColumn("Time", "string");
		table->addColumn("Bandwidth", "double");
		table->addColumn("Rate","double");
		table->addColumn("Latency", "double");
		table->addColumn("PlotDouble", "vector double");

		xdata::UnsignedLong number;
		number = 123;
		table->setValueAt(0, "Number", number);

		xdata::UnsignedInteger numberInt;
		numberInt = (123 + 0xFFFF0000);
		table->setValueAt(0, "NumberInt", numberInt);

		xdata::UnsignedInteger64 numberInt64;
		numberInt64 = (((xdata::UnsignedInteger64T)123) + 0xFFFFFFFF00000000LL);
		table->setValueAt(0, "NumberInt64", numberInt64);

		xdata::String time;
		time = "10:00:00";
		table->setValueAt(0, "Time", time);

		xdata::Double b;
		b = 100.4;
		table->setValueAt(0,"Bandwidth", b);

		xdata::Vector<xdata::Double> plotDouble;
		xdata::Double numberDouble;
		for (size_t j = 0; j < 10; j++)
		{
			numberDouble = j;
			plotDouble.push_back(numberDouble);
		}
		table->setValueAt(0, "PlotDouble", plotDouble);
	}

	void timeExpired(toolbox::task::TimerEvent& e)
	{
		xdata::Table table;
		this->prepareTable (&table);

		for (size_t i = 0; i < 5000; ++i)
		{
			try
			{
				toolbox::mem::Reference* ref = toolbox::mem::getMemoryPoolFactory()->getFrame(pool_, 2048);
				ref->setDataSize(2048);
				this->prepareMessage (ref, 2048, &table);

				xdata::Properties plist;
				plist.setProperty ("urn:b2in-protocol:service", "rx");
				plist.setProperty("flashlist", "example");
				plist.setProperty("tag", "tag");

				dynamic_cast<b2in::nub::Messenger&>(*messenger_).send(ref, plist, 0, 0);
				//dynamic_cast<b2in::nub::Messenger&>(*messenger_).send(0, plist, 0, 0);
			}
			catch (toolbox::mem::exception::Exception& me)
			{
				std::cout << "Memory exception: " << xcept::stdformat_exception_history(me) << std::endl;
			}
			catch (pt::exception::Exception & e)
			{
				std::cout << "Failed to send" << std::endl;
				::usleep(100);
			}
			catch (std::exception& se)
			{
				std::cout << "Caught standard exception: " << se.what() << std::endl;
			}
			catch (...)
			{
				std::cout << "Unhandled exception..." << std::endl;
			}
		}
		
		LOG4CPLUS_INFO (this->getApplicationLogger(), "Sent B2IN message");
	}

protected:

	toolbox::mem::Pool* pool_;
	pt::Messenger::Reference messenger_;
};
#endif
