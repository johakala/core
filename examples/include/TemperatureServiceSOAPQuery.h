// $Id: TemperatureServiceSOAPQuery.h,v 1.3 2008/07/18 15:26:44 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _TemperatureServiceSOAPQuery_h_
#define _TemperatureServiceSOAPQuery_h_

#include "xdaq/Application.h"
#include "xdaq/ApplicationGroup.h"
#include "xdaq/ApplicationContext.h"
#include "xdaq/ApplicationStub.h"
#include "xdaq/exception/Exception.h"

#include "xdaq/NamespaceURI.h"

#include "xoap/MessageReference.h"
#include "xoap/MessageFactory.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPBody.h"
#include "xoap/Method.h"

#include "xgi/Utils.h"
#include "xgi/Method.h"
#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"

#include "xdata/String.h"
#include "xdata/UnsignedLong.h"

// This is a simple example for sending a SOAP message. It can be used in combination
// with the SimpleSOAPReceiver application.

class TemperatureServiceSOAPQuery: public xdaq::Application  
{
	
	public:
	
	XDAQ_INSTANTIATOR();
	
	TemperatureServiceSOAPQuery(xdaq::ApplicationStub * s) throw (xdaq::exception::Exception)
		: xdaq::Application(s)
	{	
		// A simple web control interface
		xgi::bind(this,&TemperatureServiceSOAPQuery::Default, "Default");
		xgi::bind(this,&TemperatureServiceSOAPQuery::submitQueryForm, "submitQueryForm");
	}
	
	void Default(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception)
	{
		out->getHTTPResponseHeader().addHeader("Content-Type", "text/html");
		*out << cgicc::HTMLDoctype(cgicc::HTMLDoctype::eStrict) << std::endl;
		*out << cgicc::html().set("lang", "en").set("dir","ltr") << std::endl;
		*out << cgicc::title("TemperatureService Query") << std::endl;

		xgi::Utils::getPageHeader
			(out, 
			"TemperatureService Query", 
			getApplicationDescriptor()->getContextDescriptor()->getURL(),
			getApplicationDescriptor()->getURN(),
			"/daq/xgi/images/Application.gif"
			);

		std::string url = "/";
		url += getApplicationDescriptor()->getURN();
		url += "/submitQueryForm";	
		*out << cgicc::form().set("method","post").set("action", url).set("enctype","multipart/form-data") << std::endl;
		
		*out << cgicc::label("Method ") << std::endl;
		*out << cgicc::select().set("name","method") << std::endl;
			*out << cgicc::option("getTemp")  << std::endl;
		*out << cgicc::select()  << std::endl;		
				
		*out << "<br>";
		
		
		*out << "<br>";
		*out << cgicc::input().set("type", "submit").set("name", "send").set("value", "Query");
		*out << cgicc::form() << std::endl;

		xgi::Utils::getPageFooter(*out);	
	}
	
	void submitQueryForm(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception)
	{
		// Retrieve the search string and the Google Web API key
		
		std::string method = "";
		try 
		{		
			cgicc::Cgicc cgi(in);
			method = cgi["method"]->getValue();
		} 
		catch (std::exception& e)
		{
			std::string msg = "An error occurred while interpreting the form content: ";
			msg += e.what();
			XCEPT_RAISE (xgi::exception::Exception, msg);
		}
	
	  	xoap::MessageReference msg = xoap::createMessage();
		std::string action = "";
		try
		{
                	xoap::SOAPPart soap = msg->getSOAPPart();
                	xoap::SOAPEnvelope envelope = soap.getEnvelope();
			
			envelope.addNamespaceDeclaration ("xsd", "http://www.w3.org/2001/XMLSchema");
			envelope.addNamespaceDeclaration ("xsi", "http://www.w3.org/2001/XMLSchema-instance");
		
			
                	xoap::SOAPBody body = envelope.getBody();
                	xoap::SOAPName encodingStyle = envelope.createName("encodingStyle","soap-env", "http://schemas.xmlsoap.org/soap/envelope/");
			
			xoap::SOAPName command = envelope.createName(method,"mns", "urn:xmethods-Temperature");
                	xoap::SOAPElement bodyElement = body.addBodyElement(command);
			bodyElement.addAttribute(encodingStyle, "http://schemas.xmlsoap.org/soap/encoding/");

			xoap::SOAPName key = envelope.createName("zipcode");
                	xoap::SOAPElement keyElement = bodyElement.addChildElement(key);
			keyElement.addTextNode("london");
			xoap::SOAPName xsiType = envelope.createName("type","xsi", "http://www.w3.org/1999/XMLSchema-instance");
			keyElement.addAttribute(xsiType, "xsd:string");
			
			// build urn and put SOAPAction MimeHeader into message
			msg->getMimeHeaders()->setHeader("SOAPAction", method);
		}
		catch(xoap::exception::Exception& xe)
		{
			XCEPT_RETHROW (xgi::exception::Exception, "Cannot create message", xe);	
		}

		try
		{	
			xdaq::ApplicationDescriptor * d = 
				getApplicationContext()->getApplicationGroup()->getApplicationDescriptor("TemperatureService", 0);
				
			//msg->writeTo(std::cout);
				
			xoap::MessageReference reply = getApplicationContext()->postSOAP(msg, d);
			
			reply->writeTo(std::cout);
			std::cout << std::endl;
		} 
		catch (xdaq::exception::Exception& e)
		{
			XCEPT_RETHROW (xgi::exception::Exception, "Cannot send message", e);		
		}
				
		this->Default(in,out);
	}
};

#endif
/*
<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tns="http://www.xmethods.net/sd/TemperatureService.wsdl" 
xmlns:xsd="http://www.w3.org/1999/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/wsdl/soap/" 
xmlns:xsi="http://www.w3.org/1999/XMLSchema-instance" xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/">
<SOAP-ENV:Body><mns:getTemp xmlns:mns="urn:xmethods-Temperature" SOAP-ENV:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
<zipcode xsi:type="xsd:string">london</zipcode></mns:getTemp></SOAP-ENV:Body></SOAP-ENV:Envelope>
*/
