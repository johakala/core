// $Id: HelloWorld.h,v 1.4 2008/07/18 15:26:44 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _HelloWorld_h_
#define _HelloWorld_h_

#include "xdaq/Application.h"


// Hello World is very close to the simplest program imaginable. When you successfully compile and run it, 
// it prints the words 'Hello World!' on your log output. Although it doesn't teach very much about XDAQ programming, 
// it gives you a chance to learn the mechanics of typing and compiling code and loading the application in a running 
// executive context.

class HelloWorld: public xdaq::Application  
{
	
	public:
	
	//! define factory method for instantion of HelloWorld application
	XDAQ_INSTANTIATOR();
	
	HelloWorld(xdaq::ApplicationStub * s)
		throw (xdaq::exception::Exception): xdaq::Application(s)
	{	
		LOG4CPLUS_INFO(this->getApplicationLogger(),"Hello World!");		
	}
};

#endif
