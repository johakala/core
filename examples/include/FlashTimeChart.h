// $Id: FlashTimeChart.h,v 1.2 2008/07/18 15:26:44 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _FlashTimeChart_h_
#define _FlashTimeChart_h_

#include "xdaq/WebApplication.h"
//#include "xgi/Utils.h"
#include "xgi/Method.h"

//#include "cgicc/CgiDefs.h"
//#include "cgicc/Cgicc.h"
//#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"

class FlashTimeChart: public xdaq::WebApplication 
{
	public:
	
	XDAQ_INSTANTIATOR();
	
	FlashTimeChart (xdaq::ApplicationStub * s) throw (xdaq::exception::Exception);
	
	//! Display a flash chart that retrieves data through the \function getData callback
	//
	void Default (xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);
	
	//! Get initial data for flash chart in XML format
	//
	void getInitialData (xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);
		
	//! Get data for flash chart in ASCII format
	//
	void getData (xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);
	
	protected:
	
	unsigned long data_;
};

#endif
