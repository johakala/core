// $Id: AsynchronousSOAPStateMachine.h,v 1.5 2008/07/18 15:26:44 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _AsynchronousSOAPStateMachine_h_
#define _AsynchronousSOAPStateMachine_h_

#include "xdaq/Application.h"
#include "xdaq/ApplicationContext.h"
#include "xdaq/ApplicationStubImpl.h"
#include "xdaq/exception/Exception.h"
#include "xdaq/NamespaceURI.h"

#include "xoap/MessageReference.h"
#include "xoap/MessageFactory.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPBody.h"
#include "xoap/domutils.h"
#include "xoap/Method.h"


#include "toolbox/fsm/AsynchronousFiniteStateMachine.h"
#include "toolbox/fsm/FailedEvent.h"


class AsynchronousSOAPStateMachine: public xdaq::Application  
{
	
	public:
	
	XDAQ_INSTANTIATOR();
	
	AsynchronousSOAPStateMachine(xdaq::ApplicationStub * s) throw (xdaq::exception::Exception);
	
	//
	// SOAP Callback trigger state change 
	//
	xoap::MessageReference fireEvent (xoap::MessageReference msg) throw (xoap::exception::Exception);

	//
	// SOAP Callback to reset the state machine
	//
	xoap::MessageReference reset (xoap::MessageReference msg) throw (xoap::exception::Exception);

	//
	// Finite State Machine Actions callback
	//
	
	// This action takes 15 seconds
	void ConfigureAction (toolbox::Event::Reference e) throw (toolbox::fsm::exception::Exception);
	
	// This action takes 15 seconds
	void EnableAction (toolbox::Event::Reference e) throw (toolbox::fsm::exception::Exception);
	
	// This action will raise an exception
	void SuspendAction (toolbox::Event::Reference e) throw (toolbox::fsm::exception::Exception);
	
	// This action does nothing
	void ResumeAction (toolbox::Event::Reference e) throw (toolbox::fsm::exception::Exception);

	// This action does nothing
	void HaltAction (toolbox::Event::Reference e) throw (toolbox::fsm::exception::Exception);
	
	void stateChanged (toolbox::fsm::FiniteStateMachine & fsm) throw (toolbox::fsm::exception::Exception);
	
	void failedTransition (toolbox::Event::Reference e) throw (toolbox::fsm::exception::Exception);
	
	protected:
	
	toolbox::fsm::AsynchronousFiniteStateMachine fsm_;
	xdata::String state_; // reflects the current state of fsm_ to the outside world
};

#endif
