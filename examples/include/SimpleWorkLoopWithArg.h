// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2016, CERN.			                             *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, D. Simelev 					                     *
 *                                                                       *
 * For the licensing terms see LICENSE.		                             *
 * For the list of contributors see CREDITS.   			                 *
 *************************************************************************/

#ifndef _SimpleWorkLoopWithArg_h_
#define _SimpleWorkLoopWithArg_h_

#include "xdaq/Application.h"


#include "toolbox/task/WorkLoopFactory.h"
#include "toolbox/task/WaitingWorkLoop.h"

#include "toolbox/task/ActionWithArg.h"


class SimpleWorkLoopWithArg: public xdaq::Application
{	
public:

	XDAQ_INSTANTIATOR();

	SimpleWorkLoopWithArg(xdaq::ApplicationStub * s)
	throw (xdaq::exception::Exception);

	//
	// WorkLoop function performing work for 5 seconds
	//
	template <typename T> bool working(toolbox::task::WorkLoop* wl, T arg);


};

#endif
