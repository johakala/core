// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _XDAQEvents_h_
#define _XDAQEvents_h_

#include "xdaq/Application.h"
#include "xdata/ActionListener.h"


// Hello World is very close to the simplest program imaginable. When you successfully compile and run it, 
// it prints the words 'Hello World!' on your log output. Although it doesn't teach very much about XDAQ programming, 
// it gives you a chance to learn the mechanics of typing and compiling code and loading the application in a running 
// executive context.

class XDAQEvents: public xdaq::Application, public xdata::ActionListener
{
	
	public:
	
	//! define factory method for instantion of HelloWorld application
	XDAQ_INSTANTIATOR();
	
	XDAQEvents(xdaq::ApplicationStub * s)
		throw (xdaq::exception::Exception): xdaq::Application(s)
	{	
		LOG4CPLUS_INFO(this->getApplicationLogger(),"XDAQEvents!");
		this->getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");
		this->getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:ParameterQuery");
		this->getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:ParameterSet");
		this->getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:ParameterGet");


	}

	void actionPerformed( xdata::Event& event)
	{
		LOG4CPLUS_INFO(this->getApplicationLogger(),"got " << event.type() );

	}

};

#endif
