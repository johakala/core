// $Id: AsynchronousStateMachine.h,v 1.2 2008/07/18 15:26:44 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _SynchronousStateMachine_h_
#define _SynchronousStateMachine_h_

#include "xdaq/Application.h"
#include "xdaq/ApplicationContext.h"
#include "xdaq/ApplicationStubImpl.h"
#include "xdaq/exception/Exception.h"
#include "xdaq/NamespaceURI.h"

#include "xoap/MessageReference.h"
#include "xoap/MessageFactory.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPBody.h"
#include "xoap/domutils.h"
#include "xoap/Method.h"


#include "toolbox/fsm/AsynchronousFiniteStateMachine.h"
#include "toolbox/fsm/FailedEvent.h"

class MyStateMachine: public toolbox::lang::Class
{
	public:
	
	MyStateMachine(const std::string& workloop);
	
	// Cycle through the state machine
	void test();
	
	// Cycle through the state machine with an invalid transition
	void testWithError();
	
	// Cycle through the state machine with an transition that leads to the 'F' fail state
	void testWithFailure();
	
	// User callback invoked when new state is entered
	void enter (toolbox::Event::Reference e) throw (toolbox::fsm::exception::Exception);
	
	// User callback invoked when failed state is entered
	void failed (toolbox::Event::Reference e) throw (toolbox::fsm::exception::Exception);
	
	// User callback invoked when new state is entered, throws an exception to provoke a failure
	void injectFailure (toolbox::Event::Reference e) throw (toolbox::fsm::exception::Exception);
	
	// callback invoked when state transition occurs
	void stateChanged (toolbox::fsm::FiniteStateMachine & fsm) throw (toolbox::fsm::exception::Exception);
	
	// callback invoked when failed state is entered
	void inFailed (toolbox::fsm::FiniteStateMachine & fsm) throw (toolbox::fsm::exception::Exception);
	
	// Give an input to the state machine and fire a transition
	void input(const std::string& in);
	
	toolbox::fsm::AsynchronousFiniteStateMachine fsm_;		
};

#endif
