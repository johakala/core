// $Id: WebStateMachine.h,v 1.5 2008/07/18 15:26:44 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _WebStateMachine_h_
#define _WebStateMachine_h_

#include "xdaq/Application.h"

#include "xgi/WSM.h"
#include "xgi/Utils.h"
#include "xgi/Method.h"

#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"

class WebStateMachine: public xdaq::Application
{
	
	public:
	
	WebStateMachine(xdaq::ApplicationStub * s): xdaq::Application(s) 
	{	
	
		xgi::bind(this, &WebStateMachine::Default, "Default");
		xgi::bind(this, &WebStateMachine::dispatch, "dispatch");
	
		wsm_.addState('H', "Halted",    this, &WebStateMachine::stateMachinePage);
		wsm_.addState('R', "Ready",     this, &WebStateMachine::stateMachinePage);
		wsm_.addState('E', "Enabled",   this, &WebStateMachine::stateMachinePage);
		wsm_.addState('S', "Suspended", this, &WebStateMachine::stateMachinePage);

		wsm_.addStateTransition('H','R', "Configure", this, &WebStateMachine::Configure, &WebStateMachine::failurePage);
		wsm_.addStateTransition('R','E', "Enable",    this, &WebStateMachine::Enable,    &WebStateMachine::failurePage);
		wsm_.addStateTransition('E','S', "Suspend",   this, &WebStateMachine::Suspend,   &WebStateMachine::failurePage);
		wsm_.addStateTransition('S','E', "Resume",    this, &WebStateMachine::Resume,    &WebStateMachine::failurePage);

		wsm_.addStateTransition('H','H', "Halt", this, &WebStateMachine::Halt, &WebStateMachine::failurePage);
		wsm_.addStateTransition('R','H', "Halt",    this, &WebStateMachine::Halt,    &WebStateMachine::failurePage);
		wsm_.addStateTransition('E','H', "Halt",   this, &WebStateMachine::Halt,   &WebStateMachine::failurePage);
		wsm_.addStateTransition('S','H', "Halt",    this, &WebStateMachine::Halt,    &WebStateMachine::failurePage);
		wsm_.setInitialState('H');
		
		
	}
	
	
	void Default(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception)
	{
		wsm_.displayPage(out);
	}
	
	
	// WSM Dispatcher function
	void dispatch (xgi::Input * in, xgi::Output * out)  throw (xgi::exception::Exception)
	{
		cgicc::Cgicc cgi(in);
    		const cgicc::CgiEnvironment& env = cgi.getEnvironment();
		cgicc::const_form_iterator stateInputElement = cgi.getElement("StateInput");
		std::string stateInput = (*stateInputElement).getValue();
		wsm_.fireEvent(stateInput,in,out);
	}

	
	//
	// Web Events that trigger state changes
	//
	void Configure(xgi::Input * in ) throw (xgi::exception::Exception)
	{
		
	}
	
	void Enable(xgi::Input * in ) throw (xgi::exception::Exception)
	{
		
		
	}
	void Suspend(xgi::Input * in ) throw (xgi::exception::Exception)
	{
		
		
	}
	void Resume(xgi::Input * in ) throw (xgi::exception::Exception)
	{
		
		
	}
	void Halt(xgi::Input * in ) throw (xgi::exception::Exception)
	{
		
		
	}
	
	//
	// Web Navigation Pages
	//
	void stateMachinePage( xgi::Output * out ) throw (xgi::exception::Exception)
	{
		out->getHTTPResponseHeader().addHeader("Content-Type", "text/html");
		*out << cgicc::HTMLDoctype(cgicc::HTMLDoctype::eStrict) << std::endl;
		*out << cgicc::html().set("lang", "en").set("dir","ltr") << std::endl;
		
		xgi::Utils::getPageHeader(*out, "WebStateMachine Home", wsm_.getStateName(wsm_.getCurrentState()));
		
		std::string url = "/";
		url += getApplicationDescriptor()->getURN();
		url += "/dispatch";	
		
		// display FSM
  		std::set<std::string> possibleInputs = wsm_.getInputs(wsm_.getCurrentState());
        	std::set<std::string> allInputs = wsm_.getInputs();


        	*out << cgicc::h3("Finite State Machine").set("style", "font-family: arial") << std::endl;
        	*out << "<table border cellpadding=10 cellspacing=0>" << std::endl;
        	*out << "<tr>" << std::endl;
        	*out << "<th>" << wsm_.getStateName(wsm_.getCurrentState()) << "</th>" << std::endl;
        	*out << "</tr>" << std::endl;
        	*out << "<tr>" << std::endl;
       		std::set<std::string>::iterator i;
        	for ( i = allInputs.begin(); i != allInputs.end(); i++)
        	{
                	*out << "<td>"; 
			*out << cgicc::form().set("method","get").set("action", url).set("enctype","multipart/form-data") << std::endl;

                	if ( possibleInputs.find(*i) != possibleInputs.end() )
                	{
                        	*out << cgicc::input().set("type", "submit").set("name", "StateInput").set("value", (*i) );
                	}
                	else
                	{
                       	 	*out << cgicc::input() .set("type", "submit").set("name", "StateInput").set("value", (*i) ).set("disabled", "true");
                	}

                	*out << cgicc::form();
                	*out << "</td>" << std::endl;
        	}

        	*out << "</tr>" << std::endl;
        	*out << "</table>" << std::endl;
		//	
		xgi::Utils::getPageFooter(*out);
	}
	
	//
	// Failure Pages
	//
	void failurePage(xgi::Output * out, xgi::exception::Exception & e)  throw (xgi::exception::Exception)
	{
		out->getHTTPResponseHeader().addHeader("Content-Type", "text/html");

		*out << cgicc::HTMLDoctype(cgicc::HTMLDoctype::eStrict) << std::endl;
		*out << cgicc::html().set("lang", "en").set("dir","ltr") << std::endl;
		
		xgi::Utils::getPageHeader(*out, "WebStateMachine Home", "Failure");
		
		
		*out << cgicc::br() << e.what() << cgicc::br() << endl;
		std::string url = "/";
		url += getApplicationDescriptor()->getURN();
		;
		*out << cgicc::br() << "<a href=\"" << url << "\">" << "retry" << "</a>" << cgicc::br() << endl;
		
		xgi::Utils::getPageFooter(*out);
		
	}
	
	protected:
	
	xgi::WSM wsm_;
	
};

#endif
