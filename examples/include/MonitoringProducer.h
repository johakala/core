// $Id: MonitoringProducer.h,v 1.3 2008/07/18 15:26:44 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _MonitoringProducer_h_
#define _MonitoringProducer_h_

#include "xdaq/WebApplication.h"
#include "xgi/Method.h"
#include "xdata/UnsignedLong.h"
#include "xdata/String.h"
#include "xdata/Float.h"
#include "xdata/Table.h"
#include "xdata/InfoSpace.h"
#include "cgicc/HTMLClasses.h"
#include "toolbox/task/Timer.h"

class MonitoringProducer: public xdaq::Application, public toolbox::task::TimerListener
{
	public:
	
	XDAQ_INSTANTIATOR();
	
	MonitoringProducer(xdaq::ApplicationStub * s) throw (xdaq::exception::Exception);
	
	//! Displays a HTML menu
	void Default(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);
	
	//! Callback for starting the production of data
	void start(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);
	
	//! Callback for stopping the production of data
	void stop(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);
	
	//! Timer callback that produces monitoring data at regular intervals
	void timeExpired (toolbox::task::TimerEvent& e);
	
	protected:
	
	xdata::UnsignedLong counter_;  // an integer that is exported for monitoring
	xdata::String lastExecutionTime_;  // monitor the last execution time of a value update
	xdata::Float randomValue_; // a random number to be monitored
	xdata::Table* table_; // a table that is exported for monitoring
	xdata::InfoSpace * infoSpace_; // an InfoSpace into which monitoring data are written
	
	toolbox::task::Timer * timer_; // timer for periodic production of monitoring data
};

#endif
