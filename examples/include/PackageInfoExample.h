// $Id: PackageInfoExample.h,v 1.3 2008/07/18 15:26:44 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _PackageInfoExample_h_
#define _PackageInfoExample_h_

#include "xdaq/Application.h"
#include "PackageInfo.h"

/*! PackageInfoExample is a HelloWorld example that also provides
 *  information about the binary package loaded into the XDAQ
 *  process.
 */

class PackageInfoExample: public xdaq::Application  
{
	public:
	
	XDAQ_INSTANTIATOR();
	
	PackageInfoExample (xdaq::ApplicationStub * s): xdaq::Application(s) 
	{	
		LOG4CPLUS_INFO(this->getApplicationLogger(),"Hello World!");		
	}
};

#endif
