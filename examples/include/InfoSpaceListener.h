// $Id: InfoSpaceListener.h,v 1.3 2008/07/18 15:26:44 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _InfoSpace1_h_
#define _InfoSpace1_h_

#include "xdata/InfoSpace.h"
#include "xdata/Integer.h"
#include "xdata/ItemEvent.h"

//! An example listener to infospace events
//
class ItemActionListener: public xdata::ActionListener
{
	public:

	//! Called back when an InfoSpace event occurs
	//
	void actionPerformed(xdata::Event & received )
	{
		xdata::ItemEvent& e = dynamic_cast<xdata::ItemEvent&>(received);

		std::cout << "Received an InfoSpace event" << std::endl;
		std::cout << "Source InfoSpace: " << e.infoSpace()->name() << std::endl;
		std::cout << "Event type: " << e.type() << std::endl;
		std::cout << "Event name: " << e.itemName() << std::endl;
		std::cout << "Serializable: " << std::hex << e.item() << std::dec << std::endl;
		std::cout << "Type of serializable: " << e.item()->type() << std::endl;
	}
};

#endif
