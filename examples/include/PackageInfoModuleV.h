// $Id: PackageInfoModuleV.h,v 1.2 2008/07/18 15:26:44 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _PackageInfoModuleV_h_
#define _PackageInfoModuleV_h_

#include "PackageInfo.h"

namespace PackageInfoModule
{
    const string package  =  "PackageInfoModule";
    const string versions =  "1.0";
    const string description = "Module with package information and a simple application called PackageInfoExample";
    const string link = "http://cern.ch/xdaq/doc/3/examples/PackageInfoExample/index.html";
    toolbox::PackageInfo getPackageInfo();
    void checkPackageDependencies() throw (toolbox::PackageInfo::VersionException);
    set<string, less<string> > getPackageDependencies();
}

#endif
