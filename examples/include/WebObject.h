// $Id: WebObject.h,v 1.2 2008/07/18 15:26:44 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _WebObject_h_
#define _WebObject_h_

#include "xdaq/WebApplication.h"
#include "xgi/Method.h"
#include "cgicc/HTMLClasses.h"

class WebObject: public xdaq::Application 
{
	public:
	
	XDAQ_INSTANTIATOR();
	
	WebObject(xdaq::ApplicationStub * s) throw (xdaq::exception::Exception);
	
	void Default(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);
	
	toolbox::lang::Class* object_;
};

class WebGUI : public toolbox::lang::Class
{
	public:
		
	WebGUI();
	
	void page1 (xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);
	
	void page2 (xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);
};

#endif
