// $Id: SOAPStateMachine.h,v 1.13 2008/07/18 15:26:44 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _SOAPStateMachine_h_
#define _SOAPStateMachine_h_

#include "xdaq/Application.h"
#include "xdaq/ApplicationContext.h"
#include "xdaq/ApplicationStubImpl.h"
#include "xdaq/exception/Exception.h"
#include "xdaq/NamespaceURI.h"

#include "xoap/MessageReference.h"
#include "xoap/MessageFactory.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPBody.h"
#include "xoap/domutils.h"
#include "xoap/Method.h"

#include "toolbox/fsm/FiniteStateMachine.h"
#include "toolbox/fsm/FailedEvent.h"

class SOAPStateMachine: public xdaq::Application  
{	
	public:
	
	XDAQ_INSTANTIATOR();
	
	SOAPStateMachine(xdaq::ApplicationStub * s)
		throw (xdaq::exception::Exception);
		
	//
	// SOAP Callback trigger state change 
	//
	xoap::MessageReference fireEvent (xoap::MessageReference msg) 
		throw (xoap::exception::Exception);

	//
	// SOAP Callback to reset the state machine
	//
	xoap::MessageReference reset (xoap::MessageReference msg) 
		throw (xoap::exception::Exception);

	//
	// Finite State Machine action callbacks
	//
	void ConfigureAction (toolbox::Event::Reference e) 
		throw (toolbox::fsm::exception::Exception);
	
	void EnableAction (toolbox::Event::Reference e) 
		throw (toolbox::fsm::exception::Exception);
	
	void SuspendAction (toolbox::Event::Reference e) 
		throw (toolbox::fsm::exception::Exception);
	
	void ResumeAction (toolbox::Event::Reference e) 
		throw (toolbox::fsm::exception::Exception);

	void HaltAction (toolbox::Event::Reference e) 
		throw (toolbox::fsm::exception::Exception);
	
	void failedTransition (toolbox::Event::Reference e) 
		throw (toolbox::fsm::exception::Exception);
	
	//
	// Finite State Machine callback for entring state
	//
	void stateChanged (toolbox::fsm::FiniteStateMachine & fsm) 
		throw (toolbox::fsm::exception::Exception);		
	
	protected:
	
	toolbox::fsm::FiniteStateMachine fsm_; // the actual state machine
	xdata::String state_; // used to reflect the current state to the outside world	
};

#endif
