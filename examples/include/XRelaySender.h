// $Id: XRelaySender.h,v 1.2 2008/07/18 15:26:44 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _XRelaySender_h_
#define _XRelaySender_h_

#include "xcept/tools.h"

#include "toolbox/task/WorkLoopFactory.h"
#include "toolbox/task/WaitingWorkLoop.h"
#include "toolbox/task/Action.h"
#include "toolbox/lang/Class.h"

#include "xdaq/WebApplication.h"
#include "xdaq/ApplicationGroup.h"
#include "xdaq/ApplicationContext.h"
#include "xdaq/ApplicationStub.h"
#include "xdaq/exception/Exception.h"

#include "xdaq/NamespaceURI.h"

#include "xoap/MessageReference.h"
#include "xoap/MessageFactory.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPBody.h"
#include "xoap/Method.h"

#include "xgi/Utils.h"
#include "xgi/Method.h"
#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"
#include "xdata/String.h"

// This is a simple example for sending a SOAP message. It can be used in combination
// with the XRelaySender application.
//
class XRelaySender: public xdaq::WebApplication  
{	
	public:
	
	toolbox::task::ActionSignature* job_;
	toolbox::task::WorkLoop* wl_;
	
	XDAQ_INSTANTIATOR();
	
	bool sendJob(toolbox::task::WorkLoop* wl)
	{
		xoap::MessageReference msg = xoap::createMessage();
		std::vector<xdaq::ApplicationDescriptor *> apps = this->getApplicationContext()->
									getApplicationGroup()->
									getApplicationDescriptors ("XRelay");
		if (apps.empty())
		{
			LOG4CPLUS_ERROR (this->getApplicationLogger(), "Failed to find XRelay application");
			return false;
		}
		
		try
		{
			this->buildTheXRelayHeader(msg, true);
			this->buildTheMessage(msg,"Test", "xdaq", XDAQ_NS_URI);

			std::string smesg = "[";
			msg->writeTo(smesg);
			smesg+="]";

			LOG4CPLUS_TRACE(getApplicationLogger(), smesg);

			xoap::MessageReference reply = this->getApplicationContext()->postSOAP(msg, apps[0]);
		}
		catch (xoap::exception::Exception& ee)
		{
			LOG4CPLUS_ERROR (this->getApplicationLogger(), xcept::stdformat_exception_history(ee));
		}
		catch (xdaq::exception::Exception& e) 
		{
			LOG4CPLUS_ERROR (this->getApplicationLogger(), xcept::stdformat_exception_history(e));
		}
		
		return true; // re-submit
	}
	
	XRelaySender(xdaq::ApplicationStub * s): xdaq::WebApplication(s) 
	{	
		// A simple web control interface
		xgi::bind(this,&XRelaySender::Default, "Default");
		xgi::bind(this,&XRelaySender::sendMessage, "sendMessage");
		
		xoap::bind(this, &XRelaySender::Test, "Test", XDAQ_NS_URI );
		
		wl_ = toolbox::task::getWorkLoopFactory()->getWorkLoop("WaitingWorkLoop", "waiting");
		job_ = toolbox::task::bind (this, &XRelaySender::sendJob, "sendJob");
	}
	
	void Default(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception)
	{
		*out << cgicc::HTMLDoctype(cgicc::HTMLDoctype::eStrict) << std::endl;
		*out << cgicc::html().set("lang", "en").set("dir","ltr") << std::endl;
		*out << cgicc::title("Send SOAP Message") << std::endl;

		xgi::Utils::getPageHeader
			(out, 
			"XRelay Sender", 
			getApplicationDescriptor()->getContextDescriptor()->getURL(),
			getApplicationDescriptor()->getURN(),
			"/daq/xgi/images/Application.jpg"
			);

		std::string url = "/";
		url += getApplicationDescriptor()->getURN();
		url += "/sendMessage";	
		*out << cgicc::form().set("method","get").set("action", url).set("enctype","multipart/form-data") << std::endl;
		*out << cgicc::input().set("type", "submit").set("name", "send").set("value", "Send");
		*out << cgicc::p() << std::endl;
		*out << cgicc::form() << std::endl;

		xgi::Utils::getPageFooter(*out);	
	}
	
	void buildTheXRelayHeader
	(
		xoap::MessageReference msg,
		bool notification
	)
	{
		std::vector<xdaq::ApplicationDescriptor *> apps = this->getApplicationContext()->
									getApplicationGroup()->
									getApplicationDescriptors ("XRelaySender");
		
		std::string topNode = "relay";
		std::string prefix = "xr";
		std::string httpAdd = "http://xdaq.web.cern.ch/xdaq/xsd/2004/XRelay-10";
		xoap::SOAPEnvelope envelope = msg->getSOAPPart().getEnvelope();
		xoap::SOAPName envelopeName = envelope.getElementName();
		xoap::SOAPHeader header = envelope.addHeader();
		xoap::SOAPName relayName = envelope.createName(topNode, prefix, httpAdd);
		xoap::SOAPHeaderElement relayElement = header.addHeaderElement(relayName);
		
		// Add the actor attribute
		xoap::SOAPName actorName = envelope.createName("actor", envelope.getElementName().getPrefix(), envelope.getElementName().getURI());
		relayElement.addAttribute(actorName,httpAdd);
		
		std::string notifAtt="false";
		if ( notification ) 
		{
			notifAtt="true";
		}
		
		// Add the "to" node
		std::string childNode = "to";
		std::vector<xdaq::ApplicationDescriptor *>::iterator it;
		for ( it = apps.begin(); it != apps.end(); it++ ) 
		{
			xoap::SOAPName toName = envelope.createName(childNode, prefix, " ");
			xoap::SOAPElement childElement = relayElement.addChildElement(toName);
			xoap::SOAPName urlName = envelope.createName("url");
			xoap::SOAPName urnName = envelope.createName("urn");
			xoap::SOAPName notifName = envelope.createName("notify");
			childElement.addAttribute(urlName,(*it)->getContextDescriptor()->getURL());
			childElement.addAttribute(urnName,(*it)->getURN());
			childElement.addAttribute(notifName,notifAtt);
		}
	}
	
	xoap::SOAPBodyElement buildTheMessage
	(
		xoap::MessageReference msg, 
		const std::string & commandStr,
		const std::string & NS, 
		const std::string & urn
	)
	{
		xoap::SOAPPart soap = msg->getSOAPPart();
		xoap::SOAPEnvelope envelope = soap.getEnvelope();
		xoap::SOAPBody body = envelope.getBody();
		xoap::SOAPName command = envelope.createName(commandStr,NS,urn);
		return body.addBodyElement(command);
	}
	
	void sendMessage
	(
		xgi::Input * in, 
		xgi::Output * out 
	) 
		throw (xgi::exception::Exception)
	{
		wl_->submit(job_);		
		wl_->activate();
	}
	
	//
	// SOAP Callback  
	//
	xoap::MessageReference Test (xoap::MessageReference msg) 
		throw (xoap::exception::Exception)
	{	
		LOG4CPLUS_INFO(this->getApplicationLogger(), "Received Test");	
		xoap::MessageReference reply = xoap::createMessage();
		xoap::SOAPEnvelope envelope = reply->getSOAPPart().getEnvelope();
		xoap::SOAPName responseName = envelope.createName( "TestResponse", "xdaq", XDAQ_NS_URI);
		xoap::SOAPBodyElement e = envelope.getBody().addBodyElement ( responseName );
		return reply;		
	}
};

#endif
