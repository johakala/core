// $Id: SOAPStateMachineAutoFired.h,v 1.2 2008/07/18 15:26:44 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _SOAPStateMachineAutoFired_h_
#define _SOAPStateMachineAutoFired_h_

#include "xdaq/Application.h"
#include "xdaq/ApplicationContext.h"
#include "xdaq/ApplicationStubImpl.h"
#include "xdaq/exception/Exception.h"
#include "xdaq/NamespaceURI.h"

#include "xoap/MessageReference.h"
#include "xoap/MessageFactory.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPBody.h"
#include "xoap/domutils.h"
#include "xoap/Method.h"

#include "toolbox/fsm/FiniteStateMachine.h"
#include "toolbox/fsm/FailedEvent.h"

#include "toolbox/task/WorkLoopFactory.h"
#include "toolbox/task/WaitingWorkLoop.h"
#include "toolbox/task/Action.h"


class SOAPStateMachineAutoFired: public xdaq::Application  
{	
	public:
	
	XDAQ_INSTANTIATOR();
	
	SOAPStateMachineAutoFired(xdaq::ApplicationStub * s)
		throw (xdaq::exception::Exception);
		
	//
	// SOAP Callback triggers state change initialized -> working 
	//
	xoap::MessageReference Configure (xoap::MessageReference msg) 
		throw (xoap::exception::Exception);

	//
	// SOAP callback to reset state machine to initialized
	//		
	xoap::MessageReference Reset (xoap::MessageReference msg) 
		throw (xoap::exception::Exception);

	//
	// Finite State Machine callback for entring state
	//
	void stateChanged (toolbox::fsm::FiniteStateMachine & fsm) 
		throw (toolbox::fsm::exception::Exception);		
	
	//
	// WorkLoop function performing work for 5 seconds
	//
	bool working(toolbox::task::WorkLoop* wl);
	
	protected:
	
	toolbox::fsm::FiniteStateMachine fsm_; // the actual state machine
	
	toolbox::task::WorkLoop* workLoop_;
        toolbox::task::ActionSignature* job_;
};

#endif
