// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.			                			 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, A. Forrest										 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                        	 *
 * For the list of contributors see CREDITS.   			        		 *
 *************************************************************************/

#ifndef _examples_highcharts_application_h_
#define _examples_highcharts_application_h_

#include "xdaq/Application.h"
#include "xgi/framework/UIManager.h"

#include "xgi/Input.h"
#include "xgi/Output.h"

namespace highcharts
{
	class Application: public xdaq::Application, public xgi::framework::UIManager
	{
		public:
			XDAQ_INSTANTIATOR();

			Application (xdaq::ApplicationStub* c) throw (xdaq::exception::Exception);

			void Default (xgi::Input *in, xgi::Output *out) throw (xgi::exception::Exception);
			void getXML (xgi::Input *in, xgi::Output *out) throw (xgi::exception::Exception);

		protected:

	};
}
#endif
