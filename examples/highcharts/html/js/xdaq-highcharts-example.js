// We do this in several steps.

// 1. define some constants so we only have one place to change them

var chartWidth = 1200;
var chartHeight = 600;

// 2. create the options we want for our charts with the default settings. One set of options per chart. We add the dynamic content later

var columnOptions = {
    chart: {
    	type: 'column',
		width: chartWidth,
		height: chartHeight
    },
    legend: {
    	enabled: false
    },
    credits: {
    	enabled: false
    },
    tooltip: {
        formatter: function() {
        	// this is the hover over tooltip HTML code. this.x and this.y are provided by the highcharts API
            return '<b>' + this.x + '</b>: <b>' + this.y + '</b>';
        }
    },
    title: {
        text: 'Highcharts Column Test'
    },
    xAxis: {
    	// leave blank, we will add data later
        categories: []
    },
    yAxis: {
    	min: 0,
        title: {
            text: 'My Y Values'
        }
    },
    series: []
};

var pieOptions = {
    chart: {
		width: chartWidth,
		height: chartHeight
    },
    legend: {
    	enabled: false
    },
    credits: {
    	enabled: false
    },
    tooltip: {
        formatter: function() {
            return '<b>' + this.point.name + '</b>: <b>' + this.percentage.toFixed(1) + '%</b>';
        }
    },
    title: {
        text: 'Highcharts Pie Test'
    },
    plotOptions: {
		pie: {
			allowPointSelect: true,
			cursor: 'pointer',
			dataLabels: {
				enabled: true,
				format: '<b>{point.name}</b>: {point.percentage:.1f} %'
			}
		}
	},
    series: []
};

// now fetch the XML and parse it into the options
$.get('http://srv-c2d06-17.cms:20000/urn:xdaq-application:lid=10/getXML', function(xml) {
	
	$xml = $(xml);
	
	// do column chart stuff
	$xml.find('data item label').each(function(i, category) {
		columnOptions.xAxis.categories.push($(category).text());
	});
	
	var columnSeriesOptions = {
	    	animation: false,
			data: []
	}
	
	$xml.find('data item value').each(function(i, point) {
		columnSeriesOptions.data.push(parseInt($(point).text()));
	});

	// add it to the options
	columnOptions.series.push(columnSeriesOptions);

	// do pie chart stuff
	var pieSeriesOptions = {
		type: 'pie',
    	animation: false,
		data: []
	}
	
	$xml.find('data item').each(function(i, point) {
		var newEntry = {
				name: $(point).children("label").text(),
				y: parseInt($(point).children("value").text())
		};
		pieSeriesOptions.data.push(newEntry);
	});	

	pieOptions.series.push(pieSeriesOptions);
	
	// set sizes and intialize
	$('#columnChart').width(columnOptions.chart.width);
	$('#columnChart').height(columnOptions.chart.height);

	$('#pieChart').width(pieOptions.chart.width);
	$('#pieChart').height(pieOptions.chart.height);

	// doesnt matter which we use becuase all charts are the same size
	$('#dynamicChart').width(pieOptions.chart.width);
	$('#dynamicChart').height(pieOptions.chart.height);

	$('#columnChart').highcharts(columnOptions);
	$('#pieChart').highcharts(pieOptions);
	
	// start the dynamic tab off with the column chart
	$('#dynamicChart').highcharts(columnOptions);
});

// to demonstrate changing the chart after initialization, we toggle the chart type on a button
var usingColumn = true;

$("#changeChart").on("click", function() {
	if (usingColumn)
	{
		$('#dynamicChart').highcharts(pieOptions);
	}
	else
	{
		$('#dynamicChart').highcharts(columnOptions);
	}
	usingColumn = !usingColumn;
});