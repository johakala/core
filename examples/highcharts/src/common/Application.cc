// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.			                			 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, A. Forrest										 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                        	 *
 * For the list of contributors see CREDITS.   			        		 *
 *************************************************************************/

#include "examples/highcharts/Application.h"

#include "xgi/framework/Method.h"
#include "xgi/Method.h"

XDAQ_INSTANTIATOR_IMPL (highcharts::Application);

highcharts::Application::Application (xdaq::ApplicationStub* c) throw (xdaq::exception::Exception)
		: xdaq::Application(c), xgi::framework::UIManager(this)
{
	// this is how to set an application icon, remember it needs to be in the directory that the XDAQ_DOCUMENT_ROOT environment variable is configured to (normally /opt/xdaq/htdocs)
	getApplicationDescriptor()->setAttribute("icon", "/examples/highcharts/images/highcharts-logo.jpg");

	// bind the XGI callbacks
	// The usual Default
	xgi::framework::deferredbind(this, this, &highcharts::Application::Default, "Default");
	// and a function that returns some demo XML to play with
	xgi::deferredbind(this, this, &highcharts::Application::getXML, "getXML");
}

/*
 * We are going to display two different charts (a column chart and a pie chart), but using the same data.
 * We will also have a third tab to show dynamic changing of charts
 */
void highcharts::Application::Default (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	// we would like them on different tabs, so we need a tab-bar
	*out << "<div class=\"xdaq-tab-wrapper\">" << std::endl;

	// first tab for the column chart
	*out << "	<div class=\"xdaq-tab\" title=\"Column\">" << std::endl;
	// and the inner div to render the chart into
	*out << "		<div id=\"columnChart\"></div>" << std::endl;
	*out << "	</div>" << std::endl;

	// second tab for the pie chart
	*out << "	<div class=\"xdaq-tab\" title=\"Pie\">" << std::endl;
	// and the inner div to render the chart into
	*out << "		<div id=\"pieChart\"></div>" << std::endl;
	*out << "	</div>" << std::endl;

	// last tab where we demonstrate changing the chart dynamically
	*out << "	<div class=\"xdaq-tab\" title=\"Dynamic\">" << std::endl;
	// and the inner div to render the chart into
	*out << "		<div id=\"dynamicChart\"></div>" << std::endl;
	// and the button to change the chart
	*out << "		<br/><button id=\"changeChart\" class=\"xdaq-orange\">Change Chart</button>" << std::endl;
	*out << "	</div>" << std::endl;

	// close the tab bar
	*out << "</div>" << std::endl;

	// we need to include some additional javascript files
	// the highcharts file
	*out << "	<script type=\"text/javascript\" src=\"/highcharts/js/highcharts.js\"></script>" << std::endl;
	// this is an additional optional module which allows us to create PNG and other images from our charts in the browser
	*out << "	<script type=\"text/javascript\" src=\"/highcharts/js/modules/exporting.js\"></script>" << std::endl;
	// this contains all OUR code to render the charts
	*out << "	<script type=\"text/javascript\" src=\"/examples/highcharts/html/js/xdaq-highcharts-example.js\"></script>" << std::endl;
}

void highcharts::Application::getXML (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	// set the content type to XML (becuase we should)
	out->getHTTPResponseHeader().addHeader("Content-Type", "text/xml");

	// our demo XML code - if you want to view it nicely, view it in Firefox directly
	*out << "<?xml version=\"1.0\" encoding=\"utf-8\" ?><data><item><label>Committed</label><value>100</value></item><item><label>Used</label><value>402</value></item><item><label>Total Free</label><value>67</value></item><item><label>Formatted Free</label><value>102</value></item><item><label>Unformatted Free</label><value>277</value></item><item><label>Low Threshold</label><value>365</value></item><item><label>High Threshold</label><value>40</value></item></data>" << std::endl;
}

