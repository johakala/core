/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - Rows.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.util.FiberUtils;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.Event;
import flash.events.EventDispatcher;
import mx.binding.utils.ChangeWatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;
import mx.validators.ValidationResult;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_Rows extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
    }

    model_internal var _dminternal_model : _RowsEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal_category : String;
    private var _internal_module : String;
    private var _internal_dateTime : String;
    private var _internal_tag : String;
    private var _internal_notifier : String;
    private var _internal_line : String;
    private var _internal_severity : String;
    private var _internal_sessionid : String;
    private var _internal_type : String;
    private var _internal_version : String;
    private var _internal_message : String;
    private var _internal_schema : String;
    private var _internal_lid : String;
    private var _internal_uniqueid : String;
    private var _internal_occurrences : String;
    private var _internal_service : String;
    private var _internal_context : String;
    private var _internal_func : String;
    private var _internal_uuid : String;
    private var _internal_instance : String;
    private var _internal_identifier : String;
    private var _internal_zone : String;
    private var _internal_groups : String;
    private var _internal_count : String;

    private static var emptyArray:Array = new Array();


    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_Rows()
    {
        _model = new _RowsEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering
        model_internal::_changeWatcherArray.push(mx.binding.utils.ChangeWatcher.watch(this, "category", model_internal::setterListenerCategory));
        model_internal::_changeWatcherArray.push(mx.binding.utils.ChangeWatcher.watch(this, "module", model_internal::setterListenerModule));
        model_internal::_changeWatcherArray.push(mx.binding.utils.ChangeWatcher.watch(this, "line", model_internal::setterListenerLine));
        model_internal::_changeWatcherArray.push(mx.binding.utils.ChangeWatcher.watch(this, "func", model_internal::setterListenerFunc));
        model_internal::_changeWatcherArray.push(mx.binding.utils.ChangeWatcher.watch(this, "count", model_internal::setterListenerCount));

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get category() : String
    {
        return _internal_category;
    }

    [Bindable(event="propertyChange")]
    public function get module() : String
    {
        return _internal_module;
    }

    [Bindable(event="propertyChange")]
    public function get dateTime() : String
    {
        return _internal_dateTime;
    }

    [Bindable(event="propertyChange")]
    public function get tag() : String
    {
        return _internal_tag;
    }

    [Bindable(event="propertyChange")]
    public function get notifier() : String
    {
        return _internal_notifier;
    }

    [Bindable(event="propertyChange")]
    public function get line() : String
    {
        return _internal_line;
    }

    [Bindable(event="propertyChange")]
    public function get severity() : String
    {
        return _internal_severity;
    }

    [Bindable(event="propertyChange")]
    public function get sessionid() : String
    {
        return _internal_sessionid;
    }

    [Bindable(event="propertyChange")]
    public function get type() : String
    {
        return _internal_type;
    }

    [Bindable(event="propertyChange")]
    public function get version() : String
    {
        return _internal_version;
    }

    [Bindable(event="propertyChange")]
    public function get message() : String
    {
        return _internal_message;
    }

    [Bindable(event="propertyChange")]
    public function get schema() : String
    {
        return _internal_schema;
    }

    [Bindable(event="propertyChange")]
    public function get lid() : String
    {
        return _internal_lid;
    }

    [Bindable(event="propertyChange")]
    public function get uniqueid() : String
    {
        return _internal_uniqueid;
    }

    [Bindable(event="propertyChange")]
    public function get occurrences() : String
    {
        return _internal_occurrences;
    }

    [Bindable(event="propertyChange")]
    public function get service() : String
    {
        return _internal_service;
    }

    [Bindable(event="propertyChange")]
    public function get context() : String
    {
        return _internal_context;
    }

    [Bindable(event="propertyChange")]
    public function get func() : String
    {
        return _internal_func;
    }

    [Bindable(event="propertyChange")]
    public function get uuid() : String
    {
        return _internal_uuid;
    }

    [Bindable(event="propertyChange")]
    public function get instance() : String
    {
        return _internal_instance;
    }

    [Bindable(event="propertyChange")]
    public function get identifier() : String
    {
        return _internal_identifier;
    }

    [Bindable(event="propertyChange")]
    public function get zone() : String
    {
        return _internal_zone;
    }

    [Bindable(event="propertyChange")]
    public function get groups() : String
    {
        return _internal_groups;
    }

    [Bindable(event="propertyChange")]
    public function get count() : String
    {
        return _internal_count;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set category(value:String) : void
    {
        var oldValue:String = _internal_category;
        if (oldValue !== value)
        {
            _internal_category = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "category", oldValue, _internal_category));
        }
    }

    public function set module(value:String) : void
    {
        var oldValue:String = _internal_module;
        if (oldValue !== value)
        {
            _internal_module = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "module", oldValue, _internal_module));
        }
    }

    public function set dateTime(value:String) : void
    {
        var oldValue:String = _internal_dateTime;
        if (oldValue !== value)
        {
            _internal_dateTime = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "dateTime", oldValue, _internal_dateTime));
        }
    }

    public function set tag(value:String) : void
    {
        var oldValue:String = _internal_tag;
        if (oldValue !== value)
        {
            _internal_tag = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "tag", oldValue, _internal_tag));
        }
    }

    public function set notifier(value:String) : void
    {
        var oldValue:String = _internal_notifier;
        if (oldValue !== value)
        {
            _internal_notifier = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "notifier", oldValue, _internal_notifier));
        }
    }

    public function set line(value:String) : void
    {
        var oldValue:String = _internal_line;
        if (oldValue !== value)
        {
            _internal_line = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "line", oldValue, _internal_line));
        }
    }

    public function set severity(value:String) : void
    {
        var oldValue:String = _internal_severity;
        if (oldValue !== value)
        {
            _internal_severity = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "severity", oldValue, _internal_severity));
        }
    }

    public function set sessionid(value:String) : void
    {
        var oldValue:String = _internal_sessionid;
        if (oldValue !== value)
        {
            _internal_sessionid = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "sessionid", oldValue, _internal_sessionid));
        }
    }

    public function set type(value:String) : void
    {
        var oldValue:String = _internal_type;
        if (oldValue !== value)
        {
            _internal_type = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "type", oldValue, _internal_type));
        }
    }

    public function set version(value:String) : void
    {
        var oldValue:String = _internal_version;
        if (oldValue !== value)
        {
            _internal_version = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "version", oldValue, _internal_version));
        }
    }

    public function set message(value:String) : void
    {
        var oldValue:String = _internal_message;
        if (oldValue !== value)
        {
            _internal_message = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "message", oldValue, _internal_message));
        }
    }

    public function set schema(value:String) : void
    {
        var oldValue:String = _internal_schema;
        if (oldValue !== value)
        {
            _internal_schema = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "schema", oldValue, _internal_schema));
        }
    }

    public function set lid(value:String) : void
    {
        var oldValue:String = _internal_lid;
        if (oldValue !== value)
        {
            _internal_lid = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "lid", oldValue, _internal_lid));
        }
    }

    public function set uniqueid(value:String) : void
    {
        var oldValue:String = _internal_uniqueid;
        if (oldValue !== value)
        {
            _internal_uniqueid = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "uniqueid", oldValue, _internal_uniqueid));
        }
    }

    public function set occurrences(value:String) : void
    {
        var oldValue:String = _internal_occurrences;
        if (oldValue !== value)
        {
            _internal_occurrences = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "occurrences", oldValue, _internal_occurrences));
        }
    }

    public function set service(value:String) : void
    {
        var oldValue:String = _internal_service;
        if (oldValue !== value)
        {
            _internal_service = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "service", oldValue, _internal_service));
        }
    }

    public function set context(value:String) : void
    {
        var oldValue:String = _internal_context;
        if (oldValue !== value)
        {
            _internal_context = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "context", oldValue, _internal_context));
        }
    }

    public function set func(value:String) : void
    {
        var oldValue:String = _internal_func;
        if (oldValue !== value)
        {
            _internal_func = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "func", oldValue, _internal_func));
        }
    }

    public function set uuid(value:String) : void
    {
        var oldValue:String = _internal_uuid;
        if (oldValue !== value)
        {
            _internal_uuid = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "uuid", oldValue, _internal_uuid));
        }
    }

    public function set instance(value:String) : void
    {
        var oldValue:String = _internal_instance;
        if (oldValue !== value)
        {
            _internal_instance = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "instance", oldValue, _internal_instance));
        }
    }

    public function set identifier(value:String) : void
    {
        var oldValue:String = _internal_identifier;
        if (oldValue !== value)
        {
            _internal_identifier = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "identifier", oldValue, _internal_identifier));
        }
    }

    public function set zone(value:String) : void
    {
        var oldValue:String = _internal_zone;
        if (oldValue !== value)
        {
            _internal_zone = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "zone", oldValue, _internal_zone));
        }
    }

    public function set groups(value:String) : void
    {
        var oldValue:String = _internal_groups;
        if (oldValue !== value)
        {
            _internal_groups = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "groups", oldValue, _internal_groups));
        }
    }

    public function set count(value:String) : void
    {
        var oldValue:String = _internal_count;
        if (oldValue !== value)
        {
            _internal_count = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "count", oldValue, _internal_count));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */

    model_internal function setterListenerCategory(value:flash.events.Event):void
    {
        _model.invalidateDependentOnCategory();
    }

    model_internal function setterListenerModule(value:flash.events.Event):void
    {
        _model.invalidateDependentOnModule();
    }

    model_internal function setterListenerLine(value:flash.events.Event):void
    {
        _model.invalidateDependentOnLine();
    }

    model_internal function setterListenerFunc(value:flash.events.Event):void
    {
        _model.invalidateDependentOnFunc();
    }

    model_internal function setterListenerCount(value:flash.events.Event):void
    {
        _model.invalidateDependentOnCount();
    }


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;
        if (!_model.categoryIsValid)
        {
            propertyValidity = false;
            com.adobe.fiber.util.FiberUtils.arrayAdd(validationFailureMessages, _model.model_internal::_categoryValidationFailureMessages);
        }
        if (!_model.moduleIsValid)
        {
            propertyValidity = false;
            com.adobe.fiber.util.FiberUtils.arrayAdd(validationFailureMessages, _model.model_internal::_moduleValidationFailureMessages);
        }
        if (!_model.lineIsValid)
        {
            propertyValidity = false;
            com.adobe.fiber.util.FiberUtils.arrayAdd(validationFailureMessages, _model.model_internal::_lineValidationFailureMessages);
        }
        if (!_model.funcIsValid)
        {
            propertyValidity = false;
            com.adobe.fiber.util.FiberUtils.arrayAdd(validationFailureMessages, _model.model_internal::_funcValidationFailureMessages);
        }
        if (!_model.countIsValid)
        {
            propertyValidity = false;
            com.adobe.fiber.util.FiberUtils.arrayAdd(validationFailureMessages, _model.model_internal::_countValidationFailureMessages);
        }

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _RowsEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _RowsEntityMetadata) : void
    {
        var oldValue : _RowsEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }

    model_internal var _doValidationCacheOfCategory : Array = null;
    model_internal var _doValidationLastValOfCategory : String;

    model_internal function _doValidationForCategory(valueIn:Object):Array
    {
        var value : String = valueIn as String;

        if (model_internal::_doValidationCacheOfCategory != null && model_internal::_doValidationLastValOfCategory == value)
           return model_internal::_doValidationCacheOfCategory ;

        _model.model_internal::_categoryIsValidCacheInitialized = true;
        var validationFailures:Array = new Array();
        var errorMessage:String;
        var failure:Boolean;

        var valRes:ValidationResult;
        if (_model.isCategoryAvailable && _internal_category == null)
        {
            validationFailures.push(new ValidationResult(true, "", "", "category is required"));
        }

        model_internal::_doValidationCacheOfCategory = validationFailures;
        model_internal::_doValidationLastValOfCategory = value;

        return validationFailures;
    }
    
    model_internal var _doValidationCacheOfModule : Array = null;
    model_internal var _doValidationLastValOfModule : String;

    model_internal function _doValidationForModule(valueIn:Object):Array
    {
        var value : String = valueIn as String;

        if (model_internal::_doValidationCacheOfModule != null && model_internal::_doValidationLastValOfModule == value)
           return model_internal::_doValidationCacheOfModule ;

        _model.model_internal::_moduleIsValidCacheInitialized = true;
        var validationFailures:Array = new Array();
        var errorMessage:String;
        var failure:Boolean;

        var valRes:ValidationResult;
        if (_model.isModuleAvailable && _internal_module == null)
        {
            validationFailures.push(new ValidationResult(true, "", "", "module is required"));
        }

        model_internal::_doValidationCacheOfModule = validationFailures;
        model_internal::_doValidationLastValOfModule = value;

        return validationFailures;
    }
    
    model_internal var _doValidationCacheOfLine : Array = null;
    model_internal var _doValidationLastValOfLine : String;

    model_internal function _doValidationForLine(valueIn:Object):Array
    {
        var value : String = valueIn as String;

        if (model_internal::_doValidationCacheOfLine != null && model_internal::_doValidationLastValOfLine == value)
           return model_internal::_doValidationCacheOfLine ;

        _model.model_internal::_lineIsValidCacheInitialized = true;
        var validationFailures:Array = new Array();
        var errorMessage:String;
        var failure:Boolean;

        var valRes:ValidationResult;
        if (_model.isLineAvailable && _internal_line == null)
        {
            validationFailures.push(new ValidationResult(true, "", "", "line is required"));
        }

        model_internal::_doValidationCacheOfLine = validationFailures;
        model_internal::_doValidationLastValOfLine = value;

        return validationFailures;
    }
    
    model_internal var _doValidationCacheOfFunc : Array = null;
    model_internal var _doValidationLastValOfFunc : String;

    model_internal function _doValidationForFunc(valueIn:Object):Array
    {
        var value : String = valueIn as String;

        if (model_internal::_doValidationCacheOfFunc != null && model_internal::_doValidationLastValOfFunc == value)
           return model_internal::_doValidationCacheOfFunc ;

        _model.model_internal::_funcIsValidCacheInitialized = true;
        var validationFailures:Array = new Array();
        var errorMessage:String;
        var failure:Boolean;

        var valRes:ValidationResult;
        if (_model.isFuncAvailable && _internal_func == null)
        {
            validationFailures.push(new ValidationResult(true, "", "", "func is required"));
        }

        model_internal::_doValidationCacheOfFunc = validationFailures;
        model_internal::_doValidationLastValOfFunc = value;

        return validationFailures;
    }
    
    model_internal var _doValidationCacheOfCount : Array = null;
    model_internal var _doValidationLastValOfCount : String;

    model_internal function _doValidationForCount(valueIn:Object):Array
    {
        var value : String = valueIn as String;

        if (model_internal::_doValidationCacheOfCount != null && model_internal::_doValidationLastValOfCount == value)
           return model_internal::_doValidationCacheOfCount ;

        _model.model_internal::_countIsValidCacheInitialized = true;
        var validationFailures:Array = new Array();
        var errorMessage:String;
        var failure:Boolean;

        var valRes:ValidationResult;
        if (_model.isCountAvailable && _internal_count == null)
        {
            validationFailures.push(new ValidationResult(true, "", "", "count is required"));
        }

        model_internal::_doValidationCacheOfCount = validationFailures;
        model_internal::_doValidationLastValOfCount = value;

        return validationFailures;
    }
    

}

}
