package
{
	import flash.utils.Dictionary;
	
	import mx.collections.ArrayCollection;
	import mx.collections.ArrayList;
	import mx.messaging.MultiTopicConsumer;

	public class Model
	{
		private var pathCounter_:Number = 0; // start tagging nodes from 1, 0 is the other 
		private var _root:Node = null;
		private var _other:Node = null;
		private var severityLevels:Dictionary =new Dictionary();
		private var guardIndex:Array = new Array();
		private var exceptionIndex:Array = new Array();
		//private var exceptionArchiveTimeIndex:Array = new Array();
		
		public function Model(name:String, views:ArrayCollection)
		{
			// 0 level is no alarm
			severityLevels["warning"] = 1; // 1 to 9
			severityLevels["error"] = 10; // 10 to 19 
			severityLevels["fatal"] = 20; // 20 to infinite
			
			pathCounter_ = 1; // 1 is root and 0 is the other
		
			root =  new Node(name,null,"images/flame32.gif",1,severityLevels);
			 
			 root.properties.addItem({name: "label" , value: root.label });
			 root.properties.addItem({name: "image" , value: root.image });
			
			// Children is of type ArrayCollection
			root.children = this.createTreeModel(views, root);
			
			// Create and attach "Other" box
			//
			other =  new Node("Other",root,"other.png",0,severityLevels);
			
		
			other.properties.addItem({name: "label" , value: other.label });
			other.properties.addItem({name: "image" , value: other.image });
			
			root.children.addItem (other);
			
		}
		
		public function garbageCollect(current:Date, h:Number, m:Number): void
		{
			
			var range:Number = (current.valueOf() / 1000) - (( h * 60 * 60 ) + (m * 60));
			
			for (var key:String in exceptionIndex )
			{
				//Alert.show("Archive time:" +  (exceptionTable[key].archiveTime.valueOf() / 1000) + " Range  " + range + " hour " + h + " Minutes " +  m );
				//var t0:Number = current.valueOf();
				//var t1:Number = range * 1000;
				var d:Date = new Date((exceptionIndex[key]).archiveTime);
				//var t2:Number = d.valueOf();
				//var t3:Number = range * 1000;
				if ( d.valueOf() < (range * 1000) )
				{
					this.clear(exceptionIndex[key]);
					delete exceptionIndex[key];
				}
			}
		}   
		
		public function get other():Node
		{
			return _other;
		}

		public function set other(value:Node):void
		{
			_other = value;
		}

		protected function createTreeModel(views:ArrayCollection, father:Node): ArrayCollection
		{
			var nodes:ArrayCollection = new ArrayCollection();		
			
			if (views == null )
			{
				// father is a leaf (guard)
			
			}
			
			for each (var view:Object in views)
			{
				var label:String = view.label;
					var image:String = null;
					
					// inherit from father if not provided
					if (view.image != null)
					{
						image = view.image;
					} 
					else if ( father != null )
					{
						image = father.image;
					}
					
				
					var node:Node = new Node(label,father,image,pathCounter_++,severityLevels);
					
					
					node.properties.addItem({name: "label" , value: node.label });
					node.properties.addItem({name: "image" , value: node.image });
			
						
						/* TBD 
						var expList:XMLList = view.attributes();
						for each (var expItem:XML in expList)
						{
							// trace(	expItem.localName() + "/" + expItem.toString() + "/" + expItem.namespace().uri);
							if ( expItem.namespace().uri )
							{
							
								item.exp["urn:"+expItem.namespace().uri+":"+expItem.localName()] = new MultiRegex(expItem.toString())
							}	
						}
						*/
						
						// e.g. of namespaced filter
						// exp["urn:fedbuilder-frlcontroller:geoslot"] = new RegExp(".*","i");
						//<view jc:pid="2345" xmlns:jc="jobcontrol-application" />
						
						if (view.identifier != null )
						{							
							node.exp["identifier"] = new MultiRegex(view.identifier);
							node.properties.addItem({name: "identifier" , value: view.identifier });
						}
						if (view.type != null )
						{
							node.exp["class"] = new MultiRegex(view.type);
							node.properties.addItem({name: "class" , value: view.type });

						}
						if (view.instance!= null )
						{
							// LO RM optimiz  item.instance = new MultiRegex(viewElement.attribute("instance").toString());
							node.exp["instance"] = new MultiRegex(view.instance);
							node.properties.addItem({name: "instance" , value: view.instance });

						}
						if (view.notifier!= null)
						{
							// LO RM optimiz  item.notifier = new MultiRegex(viewElement.attribute("notifier").toString());
							node.exp["notifier"] = new MultiRegex(view.notifier);
							node.properties.addItem({name: "notifier" , value: view.notifier });

						}
						if (view.tag!= null )
						{
							// LO RM optimiz item.tag = new MultiRegex(viewElement.attribute("tag").toString());
							node.exp["tag"] = new MultiRegex(view.tag);
							node.properties.addItem({name: "tag" , value: view.tag });

						}
						if (view.context!= null )
						{
							// LO RM optimiz  item.context = new MultiRegex(viewElement.attribute("context").toString());
							node.exp["context"] = new MultiRegex(view.context);
							node.properties.addItem({name: "context" , value: view.context });

						}
						if (view.group!= null )
						{
							// LO RM optimiz item.group = new MultiRegex(viewElement.attribute("group").toString());
							node.exp["groups"] = new MultiRegex(view.group);
							node.properties.addItem({name: "groups" , value: view.group });

						} 
						if (view.service!= null )
						{
							// LO RM optimiz  item.service = new MultiRegex(viewElement.attribute("service").toString());
							node.exp["service"] = new MultiRegex(view.service);
							node.properties.addItem({name: "service" , value: view.service });

						}                 			
						nodes.addItem(node);
						if ( view.view != null )
						{
							node.children = this.createTreeModel(view.view, node );
							
						}
						else
						{
							// I am a leaf
							node.children = null;

						
							this.guardIndex.push(node);
							
						}
						

						
			} // for all views 	
			return nodes;	
		}
		
		
		

		[Bindable]
		public function get root():Node
		{
			return _root;
		}

		public function set root(value:Node):void
		{
			_root = value;
		}
		
		
		public function inject(exception:Object): void
		{
			
			if ( exception.type == "fire" )
			{
				if ( exceptionIndex[exception.exception] == undefined )
				{
					exception.archiveTime = new Date(); // remind this field has been added in the JSON return type of the lastStoredEvent call (as acknowledge)
					exceptionIndex[exception.exception] = exception;
					//exceptionArchiveTimeIndex[exception.exception] = new Date();
					
				}       
				// fire guards
				this.fire(exception);
				
			}
			else if ( exception.type == "revoke" )
			{
				if ( exceptionIndex[exception.exception] != undefined )
				{
					this.revoke(exception);
					delete exceptionIndex[exception.exception];
				}
			}
			else if ( exception.type == "rearm" )
			{
				if ( exceptionIndex[exception.exception] != undefined )
				{
					this.rearm(exception);
				}
			}
			
			
			
		
		}
		
		public function fire(exception:Object): void
		{
			var match:Boolean = false;
			for (var j:int=0;j<guardIndex.length;j++) 
			{
				var node:Node = guardIndex[j];
				
				
				if (! node.match(exception) )
				{
					continue;
				}
				
				
				// if I come here, I found a match
				match = true;
				
				// Navigate up in the tree to the top
				//	
				while ( node != null )
				{
					if (!node.fire(exception))
					{
						break;
					}
					
					node = node.parent;
				}
			}
			
			// If no match found, put exception into "other"
			if (!match)
			{
				other.fire(exception);
				root.fire(exception);
			}
		}
		public function clear(exception:Object): void
		{
			var match:Boolean = false;
			var uuid:String = exception.exception; // exception unique id
			for (var j:int=0;j<guardIndex.length;j++) 
			{
				var node:Node = guardIndex[j];
				
				// Navigate up in the tree to the top
				//	
				while ( node != null )
				{
					if (!node.clear( exception))
					{
						break;
					}
					
					node = node.parent;
				}
			}
			
			other.clear(exception);
			root.clear(exception);
		}
		
		public function revoke(exception:Object): void
		{
			var match:Boolean = false;
			var uuid:String = exception.exception; // exception unique id
			for (var j:int=0;j<guardIndex.length;j++) 
			{
				var node:Node = guardIndex[j];
				
				// Navigate up in the tree to the top
				//	
				while ( node != null )
				{
					if (!node.revoke( exception))
					{
						break;
					}
					
					node = node.parent;
				}
			}
			
			other.revoke(exception);
			root.revoke(exception);
		}
		
		public function rearm(exception:Object): void
		{
			var match:Boolean = false;
			var uuid:String = exception.exception; // unique id of exception
			for (var j:int=0;j<guardIndex.length;j++) 
			{
				var node:Object = guardIndex[j];
				
				// Navigate up in the tree to the top
				//	
				while ( node != null )
				{
					if (!node.rearm(exception))
					{
						break;
					}
					
					node = node.parent;
				}
			}
			
			other.rearm(exception);
			root.rearm(exception);
			
		}
		


	} // end of Model class
} // end of Package