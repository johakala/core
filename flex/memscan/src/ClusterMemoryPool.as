package
{
	// Class to represent a cluster memory pool to be used in the clusterMemoryPoolsList datagrid in main.mxml
	internal class ClusterMemoryPool
	{
		public var service:String;
		public var poolName:String;
    	public var allocatedBytes:Number;
    	public var freeBytes:Number;
    	public var totalBytes:Number;		// allocatedBytes + freeBytes
    	public var allocatedFraction:String;// String representing allocated memory, e.g. "64 KB / 2 MB"
    	public var usedPercent:Number;
    
		public function ClusterMemoryPool(service:String, poolName:String, allocatedBytes:Number, freeBytes:Number)
		{
			this.service = service;
			this.poolName = poolName;
			this.allocatedBytes = allocatedBytes;
			this.freeBytes = freeBytes;
			this.totalBytes = allocatedBytes + freeBytes;
			
			allocatedFraction = QualifiedSize(allocatedBytes) + " / " + QualifiedSize(totalBytes);
			
			// Avoid division by zero
			if (this.totalBytes == 0)
				usedPercent = 0;
			else
				this.usedPercent = this.allocatedBytes / this.totalBytes;
		}
		
		
		// Returns a human-readable representation of a given number of bytes
		// i.e. an input of 1024 returns "1 KB"
		private function QualifiedSize(bytes:Number):String
		{
			if (bytes > 0x40000000) // Gigabyte
			{
				return (bytes / 0x40000000).toFixed(2).toString() + " GB";
			}
			else if (bytes > 0x100000) // Megabyte
			{
				return (bytes / 0x100000).toFixed(2).toString() + " MB";
			}
			else if (bytes > 1024)
			{
				return (bytes / 1024).toFixed(2).toString() + " KB";
			}
			else
			{
				return bytes.toString() + " bytes";
			}
		}
	}
}