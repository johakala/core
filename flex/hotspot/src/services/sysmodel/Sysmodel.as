/**
 * This is a generated sub-class of _Sysmodel.as and is intended for behavior
 * customization.  This class is only generated when there is no file already present
 * at its target location.  Thus custom behavior that you add here will survive regeneration
 * of the super-class. 
 **/
 
package services.sysmodel
{
	import mx.rpc.http.Operation;
	
public class Sysmodel extends _Super_Sysmodel
{
    /**
     * Override super.init() to provide any initialization customization if needed.
     */
    protected override function preInitializeService():void
    {
        super.preInitializeService();
        // Initialization customization goes here
    }
	public function configureURL(command:String,url:String):void
	{
		// initialize service control
		
		for each ( var operation:mx.rpc.http.Operation in _serviceControl.operationList)
		{
			if ( operation.name == command )
			{
				operation.url = url;
				return;
			}
	
		}  
	}       
}

}
