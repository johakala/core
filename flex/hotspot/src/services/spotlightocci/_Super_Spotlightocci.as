/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this service wrapper you may modify the generated sub-class of this class - Spotlightocci.as.
 */
package services.spotlightocci
{
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.services.wrapper.HTTPServiceWrapper;
import mx.rpc.AbstractOperation;
import mx.rpc.AsyncToken;
import mx.rpc.http.HTTPMultiService;
import mx.rpc.http.Operation;

import com.adobe.serializers.json.JSONSerializationFilter;

[ExcludeClass]
internal class _Super_Spotlightocci extends com.adobe.fiber.services.wrapper.HTTPServiceWrapper
{
    private static var serializer0:JSONSerializationFilter = new JSONSerializationFilter();

    // Constructor
    public function _Super_Spotlightocci()
    {
        // initialize service control
        _serviceControl = new mx.rpc.http.HTTPMultiService("http://srv-c2d06-18:9979");
         var operations:Array = new Array();
         var operation:mx.rpc.http.Operation;
         var argsArray:Array;

         operation = new mx.rpc.http.Operation(null, "lastStoredEvents");
         operation.url = "/urn:xdaq-application:service=sentinelspotlight2g/lastStoredEvents";
         operation.method = "GET";
         argsArray = new Array("start");
         operation.argumentNames = argsArray;         
         operation.serializationFilter = serializer0;
         operation.resultType = String;
         operations.push(operation);

         operation = new mx.rpc.http.Operation(null, "lastStoredEventsEmpty");
         operation.url = "/urn:xdaq-application:service=sentinelspotlight2g/lastStoredEvents";
         operation.method = "GET";
         operation.serializationFilter = serializer0;
         operation.resultType = String;
         operations.push(operation);

         operation = new mx.rpc.http.Operation(null, "rearm");
         operation.url = "/urn:xdaq-application:service=sentinelspotlight2g/rearm";
         operation.method = "POST";
         argsArray = new Array("exception");
         operation.argumentNames = argsArray;         
         operation.contentType = "application/x-www-form-urlencoded";
         operation.resultType = Object;
         operations.push(operation);

         operation = new mx.rpc.http.Operation(null, "view");
         operation.url = "/urn:xdaq-application:service=sentinelspotlight2g/view";
         operation.method = "GET";
         argsArray = new Array("uniqueid");
         operation.argumentNames = argsArray;         
         operation.serializationFilter = serializer0;
         operation.resultType = String;
         operations.push(operation);

         _serviceControl.operationList = operations;  


         preInitializeService();
         model_internal::initialize();
    }
    
    //init initialization routine here, child class to override
    protected function preInitializeService():void
    {
      
    }
    

    /**
      * This method is a generated wrapper used to call the 'lastStoredEvents' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function lastStoredEvents(start:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("lastStoredEvents");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(start) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'lastStoredEventsEmpty' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function lastStoredEventsEmpty() : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("lastStoredEventsEmpty");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send() ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'rearm' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function rearm(exception:Object) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("rearm");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(exception) ;
        return _internal_token;
    }
     
    /**
      * This method is a generated wrapper used to call the 'view' operation. It returns an mx.rpc.AsyncToken whose 
      * result property will be populated with the result of the operation when the server response is received. 
      * To use this result from MXML code, define a CallResponder component and assign its token property to this method's return value. 
      * You can then bind to CallResponder.lastResult or listen for the CallResponder.result or fault events.
      *
      * @see mx.rpc.AsyncToken
      * @see mx.rpc.CallResponder 
      *
      * @return an mx.rpc.AsyncToken whose result property will be populated with the result of the operation when the server response is received.
      */
    public function view(uniqueid:String) : mx.rpc.AsyncToken
    {
        var _internal_operation:mx.rpc.AbstractOperation = _serviceControl.getOperation("view");
		var _internal_token:mx.rpc.AsyncToken = _internal_operation.send(uniqueid) ;
        return _internal_token;
    }
     
}

}
