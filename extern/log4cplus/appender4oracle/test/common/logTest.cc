
#include <log4cplus/logger.h>
#include <appender4oracle/OracleAppender.h>
#include <log4cplus/layout.h>
#include <log4cplus/ndc.h>
#include <log4cplus/helpers/loglog.h>

using namespace log4cplus;

int main(int argc, char**argv)
{
	if (argc < 5)
	{
		cout << argv[0] << "dbname username password sessionname" << endl;
		return 1;
	}

	std::string db = argv[1];
	std::string uname = argv[2];
	std::string pwd = argv[3];
	std::string session = argv[4];

    helpers::LogLog::getLogLog()->setInternalDebugging(true);
    SharedAppenderPtr a(new OracleAppender(db, uname, pwd, session));
	a->setName("OracleAppender");
    Logger::getRoot().addAppender(a);

    Logger root = Logger::getRoot();
    Logger test = Logger::getInstance("test");
    Logger subTest = Logger::getInstance("test.subtest");

	LOG4CPLUS_INFO(subTest, "Test log message");

    return 0;
}


