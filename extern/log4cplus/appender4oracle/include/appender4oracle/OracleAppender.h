// Module:  log4cplus/appender4oracle
// File:    OracleAppender.h
// Created: 6/2004
// Author:  Johannes Gutleber
//

/** @file */

#ifndef _log4cplus_OracleAppender_h_
#define _log4cplus_OracleAppender_h_

#include <log4cplus/config.h>
#include <log4cplus/appender.h>
#include <log4cplus/helpers/socket.h>

// ORACLE include files
#include <occi.h>

// Thread specific include files
#include <pthread.h>

using namespace oracle::occi;

namespace log4cplus 
{

void* commitThreadFunc(void* task);

void registerOracleAppenderFactory();

    /**
     * Sends {@link LoggingEvent} objects to an Oracle database server via OCCI.
     *
     * <p>The OracleAppender has the following properties:
     *
     * <ul>
     *   <p><li>Remote logging is non-intrusive as far as the log event is
     *   concerned. In other words, the event will be logged with the same
     *   time stamp, {@link org.apache.log4j.NDC}, location info as if it
     *   were logged locally by the client.
     *
     *   <p><li>The OracleAppender does not use a layout.
     *
     *   <p><li>Logging uses the OCCI library and embedded protocols. 
	 *   Consequently, if
     *   the database server is reachable, then log events will eventually arrive
     *   at the server.
     *
     *   <p><li>If the database server is down, the logging requests are
     *   simply dropped. However, if and when the server comes back up,
     *   then event transmission is resumed transparently. This
     *   transparent reconneciton is performed by a <em>connector</em>
     *   thread which periodically attempts to connect to the server.
     *
     *   <p><li>Logging events are automatically <em>buffered</em> by the
     *   OCCI implementation. This means that if the link to server
     *   is slow but still faster than the rate of (log) event production
     *   by the client, the client will not be affected by the slow
     *   network connection. However, if the network connection is slower
     *   then the rate of event production, then the client can only
     *   progress at the network rate. In particular, if the network link
     *   to the the server is down, the client will be blocked.
     *
     *   <p>On the other hand, if the network link is up, but the server
     *   is down, the client will not be blocked when making log requests
     *   but the log events will be lost due to server unavailability.
     */
	class LOG4CPLUS_EXPORT OracleAppender : public Appender 
	{
		public:
		// Ctors
		OracleAppender(const log4cplus::tstring& database,
			const log4cplus::tstring& username,
			const log4cplus::tstring& password,
			const log4cplus::tstring& sessionName,
			const log4cplus::tstring& serverName = tstring());
					   
		OracleAppender(const log4cplus::helpers::Properties properties);

		// Dtor
		~OracleAppender();

		// Methods
		virtual void close();
		
		void commit();

		// Returns the number of seconds for the
		// commit thread to sleep. Used as parameter
		// to ::sleep(unsigned int)
		//
		unsigned int getCommitInterval();

		// Public commit thread specific data
		pthread_attr_t commit_thread_attr_;
		pthread_t commit_thread_id_;
		bool commitThreadRunning_;

		protected:
		
		void openConnection();
		void prepareStatements();
		void createCommitThread(unsigned int seconds);
		
	        virtual void append(const spi::InternalLoggingEvent& event);

		// Data
		log4cplus::tstring database_;
		log4cplus::tstring username_;
		log4cplus::tstring password_;
		log4cplus::tstring serverName_;
		log4cplus::tstring sessionName_;
		log4cplus::tstring autocommit_; // if "true" commit each write operation to db
		size_t sessionId_;
		size_t commitEvery_; // make commit every commitEvery_ log insertions
		unsigned int commitInterval_; // commit after n seconds. log counter is reset to 0
		size_t logCounter_; // count the log statements inserted. Reset to - after commit
		
		// Oracle specific data
		Environment* env_;
		Connection * conn_;
		Statement  * insertLogEvent_;
		Statement  * getSessionId_;
		Statement  * insertSessionName_;	

		private:
		
		std::string generateId();
		
		// Disallow copying of instances of this class
		OracleAppender(const OracleAppender&);
		OracleAppender& operator=(const OracleAppender&);
	};

} // end namespace log4cplus

#endif // _log4cplus_OracleAppender_h_

