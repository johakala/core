%define _package __package__
%define _packagename __packagename__
%define _version __version__
%define _release __release__
%define _prefix  __prefix__
%define _tmppath /tmp
%define _packagedir __packagedir__
%define _os __os__
%define _platform __platform__
%define _project __project__
%define _author __author__
%define _summary __summary__
%define _url __url__
%define _buildarch __buildarch__

%define _unpackaged_files_terminate_build 0

#
# Binary RPM specified attributed (lib and bin)
#
Summary: %{_summary}
Name: %{_project}-%{_packagename}
Version: %{_version}
Release: %{_release}
Packager: %{_author}
#BuildArch: %{_buildarch}
License: BSD
Group: Applications/extern
URL: %{_url}
BuildRoot: %{_tmppath}/%{_packagename}-%{_version}-%{_release}-buildroot
Prefix: %{_prefix}

%description
__description__

%pre
#exists=`/sbin/chkconfig --list | grep slp`
#if [ x"$exists" != x ] ; then
#/sbin/service slp stop > /dev/null 2>&1
#/sbin/chkconfig --del slp  > /dev/null 2>&1
#/sbin/service slp stop 
#/sbin/chkconfig --del slp  
#fi

#
# Devel RPM specified attributed (extension to binary rpm with include files)
#
%package -n %{_project}-%{_packagename}-devel
Summary:  Development package for %{_summary}
Group:    Applications/extern

%description -n %{_project}-%{_packagename}-devel
__description__

#
# Devel RPM specified attributes (extension to binary rpm with include files)
#
%package -n %{_project}-%{_packagename}-debuginfo
Summary:  Debuginfo package for %{_summary}
Group:    Applications/XDAQ
 
%description -n %{_project}-%{_packagename}-debuginfo
__description__

#%prep

#%setup 

#%build

#
# Prepare the list of files that are the input to the binary and devel RPMs
#
%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/{etc,bin,sbin,lib,usr/include}
mkdir -p $RPM_BUILD_ROOT/etc
mkdir -p $RPM_BUILD_ROOT/etc/init.d
mkdir -p $RPM_BUILD_ROOT/etc/logrotate.d
mkdir -p $RPM_BUILD_ROOT/var/log
mkdir -p $RPM_BUILD_ROOT/usr/lib/debug/{bin,sbin,lib}
mkdir -p $RPM_BUILD_ROOT/usr/src/debug/%{_project}-%{_packagename}-%{_version}/

touch $RPM_BUILD_ROOT/var/log/slpd.log
chmod 644 $RPM_BUILD_ROOT/var/log/slpd.log


if [ -s %{_packagedir}/scripts/slp ]; then 
	install -m 755 %{_packagedir}/scripts/slp $RPM_BUILD_ROOT/etc/init.d
	install -m 755 %{_packagedir}/scripts/slp.logrotate $RPM_BUILD_ROOT/etc/logrotate.d/slp
fi
if [ -d %{_packagedir}/%{_platform}/etc ]; then 
	cd %{_packagedir}/%{_platform}/etc; \
	find ./ -mindepth 1 -name "*.*" -exec install -m 655 -D {} $RPM_BUILD_ROOT/etc/{} \;
fi
if [ -d %{_packagedir}/%{_platform}/bin ]; then 
 	cd %{_packagedir}/%{_platform}/bin; \
	find ./ -mindepth 1 -exec ${XDAQ_ROOT}/${BUILD_SUPPORT}/install.sh {} /bin/{} 755 $RPM_BUILD_ROOT %{_packagedir} %{_project}-%{_packagename} %{_version} \;
fi
if [ -d %{_packagedir}/%{_platform}/sbin ]; then 
 	cd %{_packagedir}/%{_platform}/sbin; \
	find ./ -mindepth 1 -exec ${XDAQ_ROOT}/${BUILD_SUPPORT}/install.sh {} /sbin/{} 755 $RPM_BUILD_ROOT %{_packagedir} %{_project}-%{_packagename} %{_version} \;
fi
if [ -d %{_packagedir}/%{_platform}/lib ]; then
 	cd %{_packagedir}/%{_platform}/lib; \
	find ./ -mindepth 1 -exec ${XDAQ_ROOT}/${BUILD_SUPPORT}/install.sh {} /lib/{} 655 $RPM_BUILD_ROOT %{_packagedir} %{_project}-%{_packagename} %{_version} \;
fi
if [ -d %{_packagedir}/%{_platform}/include ]; then
	cd %{_packagedir}/%{_platform}/include; \
	find ./ \( -name "*.[hic]" -o -name "*.hpp" -o -name "*.hh" -o -name "*.hxx" \) -exec install -m 655 -D {} $RPM_BUILD_ROOT/usr/include/{} \;
fi

if [ -e %{_packagedir}/ChangeLog ]; then
	install -m 655 %{_packagedir}/ChangeLog %{_packagedir}/rpm/RPMBUILD/BUILD/ChangeLog
else
	touch %{_packagedir}/rpm/RPMBUILD/BUILD/ChangeLog
fi

if [ -e %{_packagedir}/README ]; then
	install -m 655 %{_packagedir}/README %{_packagedir}/rpm/RPMBUILD/BUILD/README
else
	touch %{_packagedir}/rpm/RPMBUILD/BUILD/README
fi

if [ -e %{_packagedir}/MAINTAINER ]; then
	install -m 655 %{_packagedir}/MAINTAINER %{_packagedir}/rpm/RPMBUILD/BUILD/MAINTAINER
else
	touch %{_packagedir}/rpm/RPMBUILD/BUILD/MAINTAINER
fi

# Copy all sources and include files for debug RPMs
cat %{_packagedir}/rpm/debug.source | sort -z -u | egrep -v -z '(<internal>|<built-in>)$' | ( cd %{_packagedir} ; cpio -pd0mL "$RPM_BUILD_ROOT/usr/src/debug/%{_project}-%{_packagename}-%{_version}" 2>/dev/null )
# correct permissions on the created directories
cd "$RPM_BUILD_ROOT/usr/src/debug/"
find ./ -type d -exec chmod 755 {} \;

%clean
rm -rf $RPM_BUILD_ROOT

#
# Files that go in the binary RPM
#
#
%files
%defattr(-,root,root,-)
%attr(644,root,root) /etc/slp.conf
%attr(644,root,root) /etc/slp.reg
%attr(644,root,root) /etc/slp.spi
%attr(755,root,root) /lib/libslp.so.1.0.0
%attr(755,root,root) /lib/libslp.la
%attr(755,root,root) /lib/libslp.a
%attr(777,root,root) /lib/libslp.so.1
%attr(777,root,root) /lib/libslp.so
%attr(750,root,root) /sbin/slpd
%attr(755,root,root) /bin/slptool
%attr(750,root,root) /etc/init.d/slp
%attr(644,root,root) /etc/logrotate.d/slp
%attr(644,root,root) /var/log/slpd.log


# 
# Files required by Quattor
#
%doc MAINTAINER ChangeLog README

%dir

#
# Log files
#
%ghost /var/log/slpd.log

#
# Files that go in the devel RPM
#
%files -n %{_project}-%{_packagename}-devel
%defattr(-,root,root,-)
%attr(644,root,root) /usr/include/slp.h

#%changelog

# 
# Files required by Quattor
#
%doc MAINTAINER ChangeLog README

%post
#/sbin/chkconfig --add slp 
#/sbin/service slp start 

%preun
#exists=`/sbin/chkconfig --list | grep slp`
#if [ x"$exists" != x ] ; then
#/sbin/service slp stop > /dev/null 2>&1
#/sbin/chkconfig --del slp  > /dev/null 2>&1
#/sbin/service slp stop 
#/sbin/chkconfig --del slp 
#fi

#
# Files that go in the debuginfo RPM
#
%files -n %{_project}-%{_packagename}-debuginfo
%defattr(-,root,root,-)
/usr/lib/debug
/usr/src/debug

#%changelog

# 
# Files required by Quattor
#
%doc MAINTAINER ChangeLog README
