/* 
 * frl_test.c
 *  
 * simple test program 
 * setup: GIII-FRL in 64b/66 MHz PCIbus
 *        bigphys memory area
 *
 * $Header: /afs/cern.ch/project/cvs/reps/tridas/TriDAS/daq/extern/frlgen/test/frl2host_test.c,v 1.1 2004/07/12 12:01:07 frans Exp $ 
 *
 * mods:
 * 09-may-2003 change 'page' to 'block'
 * 12-may-2003 soft-reset, FED header size 3 -> 2, usage of block addresses
 */

/*************************************************************************
 *
 * system headers 
 *
 *************************************************************************/

#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <sys/time.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <stdarg.h>
#include <sys/resource.h>
#include <sched.h>


/*************************************************************************
 *
 * local headers 
 *
 *************************************************************************/

#include "physmem_api.h"

#include "frl.h"
#include "zfrl.h"

/*************************************************************************
 *
 * definitions and global vars 
 *
 *************************************************************************/

#define DEBUG
#define CHECK
//#define HACK

/* time between intervals for statistics */
#define INT_TIME 5

#define FRL_HEADER_WORDS32 6
#define FRL_PAYLOAD_SIZE (4096-4*FRL_HEADER_WORDS32)
#define BLOCK_SIZE 4096
//#define BLOCK_SIZE 8192


#define FED_HEADER_W64 1
#define FED_TRAILER_W64 1

/* ********************************************************************* */


#define SIGINT 2
#define SIGTERM  15

/* default values for command line args */

int arg_unit = 0;
int arg_nmb_blocks = 32 ;
int arg_len = -1 ;  /* default is varying size */
int arg_max_nmb_trigs = 0 ;  /* 0 = unlimited */

/* other global vars */

static int ufrl = 0 ;   /* FRL unit number  */

static frl_control_t *p_frlc ;

static int trigger_trigno = 0 ;  /* current trigger number generated */
static int read_trigno ;  /* last read trigno */

/* statistics */

static int recv_nmb_segm = 0 ;
static int recv_nmb_frags = 0 ;
static int  gen_nmb_trigs = 0 ;
static long long gen_nmb_bytes = 0 ; 

/* some forward function declarations */

/*************************************************************************
 *
 * 
 *
 *************************************************************************/

void 
handler_sig (int s)
{
  int dt ;

  printf("trapped signal %d .. \n",s) ;

  printf("exiting \n") ;

  exit();
}

/*************************************************************************
 *
 * 
 *
 *************************************************************************/

void 
subtractTime(struct timeval *t, struct timeval *sub)
{
  signed long sec, usec;

  sec = t->tv_sec - sub->tv_sec;
  usec = t->tv_usec - sub->tv_usec;
  if (usec < 0) {
    sec--;
    usec += 1000000;
  }
  if (sec < 0) {
    t->tv_sec = 0;
    t->tv_usec = 0;
  }
  else {
    t->tv_sec = (unsigned long) sec;
    t->tv_usec = (unsigned long) usec;
  }
}

/*************************************************************************
 *
 * 
 *
 *************************************************************************/

void 
die(char *fmt,...)
{
  va_list ap;
  va_start(ap, fmt);
  fprintf(stderr, "options:\n");
  fprintf(stderr, "-u unit\n");
  fprintf(stderr, "-b number of blocks\n");
  fprintf(stderr, "-l FED date record length in bytes (including FED haed+tail)\n");
  fprintf(stderr, "-n max number of triggers (0=unlimited)\n");
  //fprintf(stderr, "last words:\n");
  vfprintf(stderr, fmt, ap);
  fprintf(stderr, "\n");
  va_end(ap);
  exit(1);
}

/*************************************************************************
 *
 * 
 *
 *************************************************************************/

void 
command_line (int argc, char **argv)
{
  int i;

  for (i = 1; i < argc; i++) {

    if (!strcmp(argv[i], "-u")) {
      if (++i == argc)
	die("no unit");

      if (1 != sscanf(argv[i], "%d", &arg_unit))
	die("couldn't convert %s into a unit", argv[i]);
    }
    else if (!strcmp(argv[i], "-l")) {
      if (++i == argc)
	die("no length.");

      if (1 != sscanf(argv[i], "%d", &arg_len))
	die("couldn't convert %s into length");
    }
    else if (!strcmp(argv[i], "-b")) {
      if (++i == argc)
	die("no nmb blocks.");

      if (1 != sscanf(argv[i], "%d", &arg_nmb_blocks))
	die("couldn't convert %s into number");
    }
    else if (!strcmp(argv[i], "-n")) {
      if (++i == argc)
	die("no max number of triggers.");

      if (1 != sscanf(argv[i], "%d", &arg_max_nmb_trigs))
	die("couldn't convert %s into number");
    }
    else
      die("couldn't figure out what %s means", argv[i]);
  }
}

/*************************************************************************
 *
 *  generate fraglen according to trigger number
 *  if command line arg < 0 vary the size
 *     otherwise fixed 
 *
 *************************************************************************/


inline int get_fraglen(int trigno)
{
  int l ;
  if (arg_len >= 0) {
    l = arg_len ;
  }
  else {
    l = trigno ;
    l += (FED_HEADER_W64 + FED_TRAILER_W64) ;
    l = l << 3 ;  /* words64 to bytes */ 
  }
  l &= FRL_FRAGLEN_MASK  ;  
  return (l) ;
}

#ifdef OUT
inline int fraglen_w64_payload_to_bytes_total(int w64)
{
  int l = w64 ;
  l += (FED_HEADER_W64 + FED_TRAILER_W64) ;
  l = l << 3 ;  /* words64 to bytes */ 
  return (l) ;
}
#endif

/*************************************************************************
 *
 * 
 *
 *************************************************************************/


int init_frl () 
{
  int num_units;
  int rt ;
  struct zfrl_board_info binfo, *p_bi ;

  /*
   * open the FRL devices, 
   * there is a single call for any number of NICs 
   */
  rt = frl_zfrl_open (&num_units);
  //  printf("frl_zfrl_open() rt=%d=x%08X, num_units=%d \n",rt,rt,num_units);
  if(num_units <= 0) {
    printf("not FRL board found \n");
    goto error ;
  }

  ufrl = arg_unit ;

  if (ufrl < 0 || ufrl > num_units-1) {
    printf("invalid FRL unit %d \n",ufrl);
    goto error ;
  }

  /* get board info and set pointers for memory mapped access */
  
  p_bi = &binfo ;
  rt = zfrl_get_board_info (ufrl,p_bi);
  printf("unit %d zfrl_get_board_info() rt=%d pci_bus %d iobase pci x%08X user x%08X \n"
	 ,ufrl,rt,p_bi->pci_busnmb,p_bi->iobase_pci,p_bi->iobase_user);  

  p_frlc = (frl_control_t * ) p_bi->iobase_user ;

  return 0 ;
error:
    return -1;

}

/*************************************************************************
 *
 * 
 *
 *************************************************************************/

/* structure to record block characteristics */

typedef struct frl_block {
  unsigned int frlhead_adr_user ; // FRL header adr seen from user
  unsigned int frlhead_adr_bus ; // FRL header adr seen from bus
  unsigned int frlpayl_adr_user ; // FRL payload adr seen from user 
} frl_block_t ;


static frl_block_t *frl_block_arr ; /* pointer to aray of block descriptors */

/*************************************************************************
 *
 * 
 *
 *************************************************************************/

int allocate_memory()
{
  int rt ;
  unsigned int physmem_user, physmem_kern, physmem_bus, physmem_size ;
  int nmb_recv_bufs ;
  int i ;
  unsigned int free_block_adr ;

  /* open physmem and take all of it */

  rt = physmem_open (&physmem_size, &physmem_user , &physmem_bus, &physmem_kern) ;
  if (rt) {
    printf("physmem_open() FAILED rt %d \n",rt);     
    goto error ;
  }
  //  printf("physmem size %d user x%08X bus x%08X \n",physmem_size,physmem_user,physmem_bus); 
  
  /* is it big enough ? */

  nmb_recv_bufs = arg_nmb_blocks ;

  if ( physmem_size < (nmb_recv_bufs * BLOCK_SIZE) ) { 
    printf("not enough memory %d=x%X for %d blocks of %d=x%X  bytes\n"
	   ,physmem_size, physmem_size, nmb_recv_bufs, BLOCK_SIZE, BLOCK_SIZE) ;
    goto error ;
  }

  /* divide it up in blocks */

  frl_block_arr = (frl_block_t *) malloc( nmb_recv_bufs * sizeof (frl_block_t) ) ;

  for (i=0 ; i < nmb_recv_bufs ; i++) {
    free_block_adr = physmem_user + i * BLOCK_SIZE ;    
    frl_block_arr[i].frlhead_adr_user = free_block_adr ;
    frl_block_arr[i].frlpayl_adr_user = free_block_adr + 4*FRL_HEADER_WORDS32 ;
    free_block_adr = physmem_bus + i * BLOCK_SIZE ;    
    frl_block_arr[i].frlhead_adr_bus = free_block_adr ;

  }

  /* happy */

  return 0 ;
error:
  return -1;
}


/*************************************************************************
 *
 * 
 *
 *************************************************************************/

static struct timeval time_alrm;

struct timeval time_start;
struct timeval time_stop;


void 
handler_sigalrm()
{
  static int nmb_sig ;
  struct timeval time_alrm_last ;
  struct timeval time_alrm_temp ;
  int dt ;
  int  current_trigno ;
  static int gen_nmb_trigs_last = 0 ;
  static long long gen_nmb_bytes_last = 0 ;
  int nmb_trigs ;
  int last_len ;
  int avg_len ;
  long long nmb_bytes ;
  double frag_time ;
  double frag_thru10 ;

  nmb_sig++ ;

  time_alrm_last = time_alrm ;

  gettimeofday (&time_alrm, NULL);

  time_alrm_temp = time_alrm ;

  subtractTime (&time_alrm_temp, &time_alrm_last);
  dt = time_alrm_temp.tv_sec * 1000000 + time_alrm_temp.tv_usec;

  current_trigno = getPCI32swap(&p_frlc->current_trigno) ;

  printf("dt %d generated: nmb_trigs %d  last-trigno %d=x%X recv: nmb_segm %d nmb_frags %d last-trigno %d=x%X \n"
	 , dt, gen_nmb_trigs, trigger_trigno,trigger_trigno,  recv_nmb_segm, recv_nmb_frags, read_trigno,read_trigno) ;
  print_frl_status (p_frlc) ;


  nmb_trigs = gen_nmb_trigs - gen_nmb_trigs_last ;
  gen_nmb_trigs_last =  gen_nmb_trigs ;

  last_len = get_fraglen (current_trigno) ;

  nmb_bytes = gen_nmb_bytes - gen_nmb_bytes_last ;
  gen_nmb_bytes_last =  gen_nmb_bytes ;

  if (nmb_trigs > 0) {
    avg_len = nmb_bytes / nmb_trigs ;
    frag_time = (double) dt / (double) nmb_trigs ; 
    frag_thru10 = (double) avg_len / frag_time ;
  }
  else {
    frag_time = 0 ;
    frag_thru10 = 0 ;
  }


     

  printf("dt %d trigno %d us len-last %d=x%X B len-avg %d=x%X B nfrag %d time %6.4f us thru10 %6.2f MB/s \n"
	 ,dt, current_trigno, last_len, last_len, avg_len, avg_len, nmb_trigs, frag_time, frag_thru10) ;
}


/*************************************************************************
 *
 * 
 *
 *************************************************************************/

print_frl_status (frl_control_t *p)
{
  int nmb_segments ;
  int nmb_triggers ;
  int current_trigno ;
  int nmb_triggers_pending ;
  int nmb_blocks_pending ;
  
  nmb_segments = getPCI32swap(&p_frlc->nmb_segments) ;
  nmb_triggers = getPCI32swap(&p_frlc->nmb_triggers) ;
  current_trigno = getPCI32swap(&p_frlc->current_trigno) ;
  nmb_triggers_pending = getPCI32swap(&p_frlc->nmb_triggers_pending) ;
  nmb_blocks_pending = getPCI32swap(&p_frlc->nmb_blocks_pending) ;
  
  printf("FRL status: nmb_segments %d nmb_triggers %d current_trigno %d nmb_triggers_pending %d nmb_blocks_pending %d\n",
	 nmb_segments, nmb_triggers, current_trigno, nmb_triggers_pending, nmb_blocks_pending) ;
}


/*************************************************************************
 *
 * 
 *
 *************************************************************************/

print_frlh (frlh_t *ph)
{ 
  int source, trigno, segno, segsize ;

  source = getPCI32swap(&ph->source) ;
  trigno = getPCI32swap(&ph->trigno) ;
  segno  = getPCI32swap(&ph->segno) ;
  segsize = getPCI32swap(&ph->segsize) ;
  printf ("src %d=x%X  trigno %d=x%X  segno %d=x%X  segsize %d=x%X \n"
	  ,source,source,trigno,trigno,segno,segno,segsize,segsize) ;
}


/*************************************************************************
 *
 * 
 *
 *************************************************************************/

generate_next_trigger()
{
  unsigned long trigger_entry ;
  int trigger_source, trigger_fraglen, trigger_fraglen_w64;

  if (arg_max_nmb_trigs != 0 && gen_nmb_trigs >= arg_max_nmb_trigs)
    return ;

  trigger_trigno ++ ;

  trigger_source = 7 ;

  trigger_trigno &= FRL_TRIGNO_MASK ;

  trigger_source &= FRL_SOURCE_MASK ;
  trigger_fraglen = get_fraglen (trigger_trigno) ;  
  trigger_fraglen_w64 = trigger_fraglen >> 3 ;  /* byes to 8-byte words */

  trigger_entry = (trigger_trigno<<20) | (trigger_source<<16) | trigger_fraglen_w64 ;


#ifdef DEBUG

  printf("about to push trigger x%08X \n",trigger_entry) ;

#endif

  setPCI32swap(&p_frlc->fifo_trigger_entry,trigger_entry) ;

  gen_nmb_trigs ++ ;
  gen_nmb_bytes += trigger_fraglen ;
}



/*************************************************************************
 *
 * 
 *
 *************************************************************************/


dump_frl_data (unsigned int adr_user, int len) 
{
  int nw32 ;
  int *pl ;
  int l ;
  int d, ds;
  static int ndump = 0 ;
  
  if (ndump > 0) 
    return ;

  nw32 = len >> 2 ;

  /* FRL header */
  pl = (int *) adr_user ;
  for (l=0 ; l < 6 ; l++) {
    d = *pl ++ ;
    ds = ntohl(d) ;
    printf("%4d x%08X x%08X \n",l,d,ds) ;
  }
  /* FRL data */
  for (l=0 ; l < nw32 ; l++) {
    d = *pl ++ ;
    ds = ntohl(d) ;
    printf("%4d x%08X x%08X \n",l,d,ds) ;
  }

  ndump ++ ;
}

/*************************************************************************
 *
 * 
 *
 *************************************************************************/

test()
{
  int rt ;
  int status ;
  int segsize ;
  frlh_t *ph ;
  unsigned long free_block_adr ;
  int frag_len ;
  int arg_len_tot ;
  int trigger_trigno_first = 1 ;
  int input_trigno ;
  int read_segno ;
  int n ;
  int segno ;

  int iblock = 0 ;
  int data ;
  int len, reclen ;
  unsigned int frlhead_adr_user ;
  int fraglen_w64 ;

  //  printf("arg_nmb_blocks %d \n",arg_nmb_blocks) ;

  /* configure FRL */

  data = FRL_CONTROL_SOFTRESET ;
  setPCI32swap(&p_frlc->control,data) ;
  /* looks like after softreset ha sto settle a bit 
   * so do read of status register */
  data = getPCI32swap(&p_frlc->control) ;
  //  printf("FRL control x%08x \n",data) ;

  data = FRL_CONTROL_GENERATOR ;
  setPCI32swap(&p_frlc->control,data) ;


 
  setPCI32swap(&p_frlc->payload_size,FRL_PAYLOAD_SIZE) ;
  setPCI32swap(&p_frlc->header_size,FRL_HEADER_WORDS32) ;

  print_frl_status (p_frlc) ;

  /* zero len in each block header */

  for (iblock=0 ; iblock < arg_nmb_blocks ; iblock++) {

    /* put marker in first word */
    * ((int *) frl_block_arr[iblock].frlpayl_adr_user) = 0xdeadface ;

    ph = (frlh_t *) frl_block_arr[iblock].frlhead_adr_user ;
    ph->segsize = 0 ;
  }

  /* push all free blocks into FRL fifo */

  for (iblock=0 ; iblock < arg_nmb_blocks ; iblock++) {
    free_block_adr =  frl_block_arr[iblock].frlhead_adr_bus ;    
    setPCI32swap(&p_frlc->fifo_free_block_entry,free_block_adr) ;
  }

  iblock = 0 ; /* set index back to first block for loop */ 

  /* generate batch of xx triggers initially */

  input_trigno = 1 ;

  for (n=0 ; n < 8 ; n++) {
    generate_next_trigger();
  }

  frag_len = 0 ;
  segno = 0 ;

  for (;;) {

    ph = (frlh_t *) frl_block_arr[iblock].frlhead_adr_user ;
    frlhead_adr_user = frl_block_arr[iblock].frlhead_adr_user ;

    /* poll till next segment */
    for (;;) {
      segsize = getPCI32swap(&ph->segsize) ;
      if (segsize)
	break ;
    }
    
#ifdef DEBUG
    data = * ((int *) frl_block_arr[iblock].frlpayl_adr_user) ;
    printf("segment size x%x data x%x \n",segsize,data) ;
#endif    

    read_trigno = getPCI32swap(&ph->trigno) ;
    if (input_trigno != read_trigno) {
      printf("error trigno expect: %d=x%X  got: %d=x%X \n",input_trigno,input_trigno,read_trigno,read_trigno) ;
      goto error ;
    }

    read_segno =  getPCI32swap(&ph->segno) ;
    if (segno != read_segno) {
      printf("error segno expect: %d  got: %d \n",segno,read_segno) ;
      goto error ;
    }

    segno ++ ;

    recv_nmb_segm ++ ;

    len = segsize & ~FRL_LAST_SEGM ;

#ifdef DEBUG
    //    dump_frl_data ( frlhead_adr_user, len) ; 
#endif

    frag_len += segsize & ~FRL_LAST_SEGM ;
    if (segsize & FRL_LAST_SEGM) {  /* fragment is complete */

#ifdef CHECK

      arg_len_tot = get_fraglen (read_trigno) ;
      if (frag_len != arg_len_tot) {
	printf("error is %d trigno %d frag_len  %d=x%X got: %d=x%X \n"
	       ,recv_nmb_segm,read_trigno,arg_len_tot,arg_len_tot,frag_len,frag_len) ;

//	dump_frl_data ( frlhead_adr_user, len) ; 
	goto error ;     
      }


      /* search fragment length in data trailer */
      reclen = len + 4*FRL_HEADER_WORDS32 ;
      data = * (int *) (frlhead_adr_user + reclen - 4) ;
      len = ntohl(data) ;
      len &= 0xFFFFF ;   /* 24 bits  - len in bytes fed data */
      
      if (frag_len != len) {
	printf("data error trigno %d segno %d : frag_len from hd %d=x%X from trail %d=x%X data x%X \n"
	       ,read_trigno,read_segno,frag_len,frag_len,len,len,data) ;
	goto error ;     
      }
#endif
    
      recv_nmb_frags ++ ;

      frag_len = 0 ;  /* zero for next fragment */
      segno = 0 ;
      input_trigno ++ ;
      input_trigno &= FRL_TRIGNO_MASK ;


      generate_next_trigger();
    }

    /* recycle block */
    ph->segsize = 0 ;
    free_block_adr =  frl_block_arr[iblock].frlhead_adr_bus ;    
    setPCI32swap(&p_frlc->fifo_free_block_entry,free_block_adr) ;

    iblock ++ ;
    if (iblock == arg_nmb_blocks) iblock = 0 ;
    
  }

  print_frl_status (p_frlc) ;


  return 0 ;
error:
    return -1;
}




/*************************************************************************
 *
 * 
 *
 *************************************************************************/

int main(argc,argv)
     int    argc;
     char **argv;
{
  int rt;
  struct itimerval itim, oitim ;
  int minsize ;

  /* get cmd line args */

  command_line (argc, argv) ;

  /* install signal handler */

  signal (SIGINT,handler_sig);
  signal (SIGTERM,handler_sig);

  /* sanity check of args */

  if (arg_nmb_blocks < 0 || arg_nmb_blocks > FRL_MAX_FREE_BLOCKS) 
    die ("invalid # blocks ") ; 

  minsize = 8 *(FED_HEADER_W64 + FED_TRAILER_W64) ;
  if (arg_len > 0 && arg_len < minsize ) 
    die ("invalid FED length ") ; 

  /* initialise frl driver and unit uamze */
 
  rt = init_frl() ;
  if (rt) {
    printf("init_frl() error \n") ;
    exit(1);
  }

  /* 
   * allocate blocks in DMAable memory
   */
  
  rt = allocate_memory() ;
  if (rt) {
    printf("allocate_memory() error \n") ;
    exit(1);
  }

  signal (SIGALRM,handler_sigalrm) ;

  /* start interval timer alarm */

  gettimeofday (&time_alrm, NULL);

  itim.it_interval.tv_sec = INT_TIME ;
  itim.it_interval.tv_usec = 0 ;
  itim.it_value = itim.it_interval ;

  setitimer (ITIMER_REAL, &itim, &oitim) ;

  gettimeofday (&time_start, NULL);

  /* perform test */

  test();

  printf("exiting ..") ;
  printf("\n") ;
  exit (0);
}


/* eof */




