/*
 * $Header: /afs/cern.ch/project/cvs/reps/tridas/TriDAS/daq/extern/frlgen/src/zfrl_dev.c,v 1.1 2007/02/04 14:48:16 frans Exp $
 *
 */


/*************************************************************************
 * 
 * system headers 
 *
 *************************************************************************/


#include <stdio.h>
#include <stdlib.h>

#include <asm/page.h>
#include <sys/mman.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/file.h>
#include <sys/fcntl.h>
#include <sys/ioctl.h>
#include <sys/param.h>
#include <sys/socket.h>
#include <net/if.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>

/*************************************************************************
 * 
 * private headers 
 *
 *************************************************************************/

#include "frl_hw.h"
#include "frl_api.h"
#include "zfrl.h"

/*************************************************************************
 * 
 * .. 
 *
 *************************************************************************/


static unsigned int LANAI_fd[16] =
{-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1};

static struct zfrl_board_info BINFO[16];

static int zfrl_opened = 0 ;
static int zfrl_num_units_open = 0 ;        


//#define printf	printf


int
frl_zfrl_open (int *p_num_units)
{
  int rv, fd, i;
  char buf[30];
  int map_len_bytes = 0;
  void *mb_map ;
  //  int revision ;
  int dma_block_bus ;
  int dma_block_size ;

  fd_set mosmask;
  int moswidth;
  int unit ;

  uint32_t pci_rsrc0_size_fpga_main ; 



  if (zfrl_opened) {  // was here - return with stored info from first time
    *p_num_units = zfrl_num_units_open ;
    return (0) ;
  }
  else { // first time
    zfrl_opened = 1 ;
  }
     


  FD_ZERO(&mosmask);
  moswidth = 1;

  unit = 0;

  for (i = 0; i < 16; i++) {
    sprintf(buf, "/dev/zfrl%d", i);
    if ((fd = open(buf, O_RDWR)) < 0) {
      continue;
    }

    FD_SET(fd, &mosmask);
    if (fd >= moswidth)
      moswidth = fd + 1;

    dma_block_size = 0 ;

    bzero(&BINFO[unit], sizeof(struct zfrl_board_info));

    if ((rv = ioctl(fd, ZFRL_GET_BOARD_INFO, &BINFO[unit])) < 0) {
      perror("ioctl: ");
      return (0);
    }

    //    revision = BINFO[unit].revision ;
    dma_block_bus = 0 ;
    dma_block_size = 0 ;

    LANAI_fd[unit] = fd;

    /* take resource-0 len */

    pci_rsrc0_size_fpga_main = BINFO[unit].pci_rsrc0_size_fpga_main ; 
    map_len_bytes =  pci_rsrc0_size_fpga_main + dma_block_size ;

    // mmap() returns (void *) and MAP_FAILED or failure
    mb_map = mmap(NULL, (size_t) map_len_bytes,
			   PROT_READ | PROT_WRITE,
			   MAP_SHARED,
			   fd,
			   (off_t) 0);

    if (mb_map == MAP_FAILED) {
      printf("mmap(): u=%d %s fd=%d len=%d=x%X \n",unit,buf,fd,map_len_bytes,map_len_bytes) ;
      perror("mmap "); 
      return (-1);
    }


#ifdef OUT
 {
   struct zfrl_load_fpga load_fpga ;
   /* reload fpga for frl+generator */
   

   load_fpga.blockno = 1 ;
   if ((rv = ioctl(fd, ZFRL_LOAD_FPGA, &load_fpga)) < 0) {
     perror("ioctl: ");
     return (0);
   }

   printf("u %d loaded FPGA  FRL+generator\n",unit) ;
 }
#endif

   /* can put here initialisation code - now device is user mapped */

    BINFO[unit].iobase_main_user = (unsigned long) mb_map ;

    unit ++ ;
  }


  zfrl_num_units_open = unit ;   // store

  *p_num_units = zfrl_num_units_open ;       

  return (0);
}


int zfrl_get_board_info (int unit, struct zfrl_board_info *p_bi)
{
  if (zfrl_opened == 0)
    return (-1) ;
  if (unit < 0 || unit >= zfrl_num_units_open)
    return (-1) ;

  *p_bi =  BINFO[unit] ;
  
  return (0) ;
}



int zfrl_fpga (int unit, int blockno)
{
  int fd ; 
  struct zfrl_load_fpga load_fpga ;
  int rv ;

  if (zfrl_opened == 0)
    return (-1) ;
  if (unit < 0 || unit >= zfrl_num_units_open)
    return (-1) ;

  fd = LANAI_fd[unit] ;

  /* reload fpga */

  load_fpga.blockno = blockno ;
  if ((rv = ioctl(fd, ZFRL_LOAD_FPGA, &load_fpga)) < 0) {
    perror("ioctl: ");
    return (-1);
  }
  
  /* read board info again */

  if ((rv = ioctl(fd, ZFRL_GET_BOARD_INFO, &BINFO[unit])) < 0) {
    perror("ioctl: ");
    return (-1);
  }


  return (0) ;
}







