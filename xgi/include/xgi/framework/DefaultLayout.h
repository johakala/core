// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A.Petrucci, A.Forrest								 *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _xdaq_ui_DefaultLayout_h_
#define _xdaq_ui_DefaultLayout_h_


#include "xgi/framework/AbstractLayout.h"

namespace xgi
{
	namespace framework
	{
		class UIManager;

		class DefaultLayout: public xgi::framework::AbstractLayout
		{
			public:
	
				void getHTMLHeader (xgi::framework::UIManager * manager, xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception);
				void getHTMLFooter (xgi::framework::UIManager * manager, xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception);

				void Default (xgi::framework::UIManager * manager, xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception);
                void noCallbackFound (xgi::framework::UIManager * manager, xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception);
                void errorPage (xgi::framework::UIManager * manager, xgi::Input * in, xgi::Output * out, xcept::Exception& e) throw (xgi::exception::Exception);
		};
	}
}

#endif
