// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A.Petrucci, A.Forrest								 *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _xgi_UIManager_h_
#define _xgi_UIManager_h_

#include "xgi/exception/Exception.h"
#include "xgi/Output.h"
#include "xgi/Input.h"

#include "xgi/framework/AbstractLayout.h"

namespace xdaq
{
	class Application;
}

namespace xgi
{
	namespace framework
	{
		class UIManager
		{
			public:
				UIManager(xdaq::Application * application);
	
				void getHTMLHeader (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception);
				void getHTMLFooter (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception);


				void Default (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception);
				void noCallbackFound (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception);
				void errorPage (xgi::Input * in, xgi::Output * out, xcept::Exception& e) throw (xgi::exception::Exception);

	
				xdaq::Application * getApplication();

				static void setLayout(xgi::framework::AbstractLayout * layout);
				static xgi::framework::AbstractLayout *  getLayout();

				static xgi::framework::AbstractLayout *  getDefaultLayout();
	
			protected:
				xdaq::Application * application_;
	
				static xgi::framework::AbstractLayout * layout_;
		};
	}
}

#endif
