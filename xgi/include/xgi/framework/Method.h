// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A.Petrucci, A.Forrest								 *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _xgi_framework_Method_h_
#define _xgi_framework_Method_h_

#include <iostream>
#include <string.h>

#include "xdaq/Application.h"

#include "xgi/exception/Exception.h"
#include "toolbox/lang/Method.h"
#include "xgi/Input.h"
#include "xgi/Output.h"
#include "xgi/Method.h"

#include "xgi/framework/UIManager.h"

namespace xgi
{
	namespace framework
	{

		template<class LISTENER>
		class Method : public xgi::MethodSignature
		{
			public:
				Method()
				{
				}

				virtual ~Method ()
				{
				}

				std::string type ()
				{
					return "cgi";
				}

				void invoke (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
				{
					// signature is void f(xgi::Input,xgi::Output)

					// get the header bits
					obj_->getHTMLHeader(in, out);

					try
					{
						(obj_->*func_)(in, out);
					}
					catch (xgi::exception::Exception& e)
					{
						out->str("");
						out->clear();
						obj_->errorPage(in, out, e);
						return;
					}
					obj_->getHTMLFooter(in, out);
				}

				std::string name ()
				{
					return name_;
				}

				void name (std::string name)
				{
					name_ = name;
				}

				LISTENER * obj_;
				void (LISTENER::*func_) (xgi::Input*, xgi::Output*) throw (xgi::exception::Exception);
				std::string name_;
		};

		//! binds a HTTP callback methd to a name and registers it
		template<class LISTENER>
		void deferredbind (xdaq::Application * application, LISTENER * obj, void (LISTENER::*func) (xgi::Input*, xgi::Output*) throw (xgi::exception::Exception), const std::string & messageName)
		{
			xgi::framework::Method<LISTENER> * f = new xgi::framework::Method<LISTENER>();
			f->obj_ = obj;
			f->func_ = func;
			f->name_ = messageName;
			application->addMethod(f, messageName);
		}

	}
} // end  of namespace

#endif
