// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A.Petrucci, A.Forrest								 *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _xgi_framework_AbstractLayout_h_
#define _xgi_framework_AbstractLayout_h_

#include "xgi/exception/Exception.h"
#include "xgi/Output.h"
#include "xgi/Input.h"

namespace xgi
{
	namespace framework
	{
		class UIManager;

		class AbstractLayout
		{
			public:
	
				virtual void getHTMLHeader (xgi::framework::UIManager * manager, xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception) = 0;
				virtual void getHTMLFooter (xgi::framework::UIManager * manager, xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception) = 0;
				virtual void Default (xgi::framework::UIManager * manager, xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception) = 0;
				virtual void noCallbackFound (xgi::framework::UIManager * manager, xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception) = 0;
				virtual void errorPage (xgi::framework::UIManager * manager, xgi::Input * in, xgi::Output * out, xcept::Exception& e) throw (xgi::exception::Exception) = 0;
		};
	}
}

#endif
