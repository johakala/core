window.onload = function() {
  favicon.change("http://ajaxlocal/run/favicon/a.ico");
  document.onkeypress = function(ev) {
    ev = ev || window.event;
    var unicode=ev.keyCode? ev.keyCode : ev.charCode
    if (unicode>=65 && unicode<=90) { // Lowercase it if it's capital
      unicode+=32;
    }
    if (unicode>=97 && unicode<=122) { // It's a letter - handle it!
      var letter = String.fromCharCode(unicode);
      var call = "favicon.change('Icons/" + letter + ".ico'" +
        (ev.shiftKey ? ", 'Brought to you by the letter " + letter + "'" : "") +
        ")";
      eval(call);
      $("log").innerHTML= "Called <tt>" + call + "</tt>;<br/>" +
        $("log").innerHTML;
    }
  }
}
