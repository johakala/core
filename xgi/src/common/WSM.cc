
#include "xgi/WSM.h"

#include <map>
#include <string>
#include "xgi/exception/Exception.h"


xgi::WSM::WSM()
{
}


xgi::WSM::~WSM()
{
	// delete all functors
}

void  xgi::WSM::displayPage(xgi::Output* out)
{
	pages_[currentState_]->invoke(out);
}

void xgi::WSM::setInitialState(xgi::WSM::State s)
{
	if ( states_.find(s) != states_.end() )
	{
		currentState_ = s;

	}
	else
	{
		std::string msg = "State: ";
		msg += s;
		msg += "undeclared ";
		XCEPT_RAISE (xgi::exception::Exception, msg); 

	}
}

std::string xgi::WSM::getStateName(xgi::WSM::State s) throw (xgi::exception::Exception)
{
	if ( states_.find(s) != states_.end() )
	{
		return states_[s];
	}
	else
	{
		std::string msg = "Cannot find name for state: ";
		msg += s;
		msg += ", unknwon";

		XCEPT_RAISE (xgi::exception::Exception, msg); 
	}


}

//
std::vector<xgi::WSM::State> xgi::WSM::getStates()
{
	std::vector<xgi::WSM::State> states;
	std::map< xgi::WSM::State, std::string, std::less<xgi::WSM::State> >::iterator i;

	for (i = states_.begin(); i != states_.end(); i++ )
	{
		states.push_back((*i).first);
	}

	return states;
}


std::set<xgi::WSM::Input> xgi::WSM::getInputs(xgi::WSM::State s)
{
	std::set<xgi::WSM::Input> inputs;
	if ( stateTransitionTable_.find(s) != stateTransitionTable_.end() )
	{
		std::map<xgi::WSM::Input, xgi::WSM::State, std::less<xgi::WSM::Input> >::iterator i;
		for ( i = stateTransitionTable_[s].begin(); i != stateTransitionTable_[s].end(); i++ )
		{
			inputs.insert((*i).first);
		}

	}
	return inputs;
}


std::set<xgi::WSM::Input> xgi::WSM::getInputs()
{
	return inputs_;
}


//


xgi::WSM::State xgi::WSM::getCurrentState()
{
	return currentState_;

}

void xgi::WSM::addStateTransition(xgi::WSM::State from, xgi::WSM::State to, xgi::WSM::Input i) throw (xgi::exception::Exception)
{
	if (states_.find(from) == states_.end()) 
	{
		std::string msg = "'From' state: ";
		msg += from;
		msg += " undeclared ";
		XCEPT_RAISE (xgi::exception::Exception, msg); 
	}	

	if (states_.find(to) == states_.end()) 
	{
		std::string msg = "'To' state: ";
		msg += to;
		msg += " undeclared ";
		XCEPT_RAISE (xgi::exception::Exception, msg); 
	}	

	inputs_.insert(i);
	stateTransitionTable_[from][i] = to;
	actionTable_[from][i] = 0;
	failures_[i] = 0;
}


void xgi::WSM::fireEvent(const xgi::WSM::Input & i, xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{

	xgi::WSM::State newState = stateTransitionTable_[currentState_][i];
	if ( newState == 0 )
	{
		std::string msg = "Invalid input event: ";
		msg += i;
		msg += ", current state: ";
		msg += currentState_;
		if ( failures_[i] != 0 )
		{	
				xgi::exception::Exception e("xgi::exception::Exception", msg, __FILE__, __LINE__, __FUNCTION__ );
				failures_[i]->invoke(out, e);
				return;
			
		}
		else
		{	// standard failure or raise
			XCEPT_RAISE (xgi::exception::Exception, msg); 
		}
	}
	if ( actionTable_[currentState_][i] != 0 ) {
		try 
		{	
			// invoke input call back
			actionTable_[currentState_][i]->invoke(in);
		}
		catch(xgi::exception::Exception & e)
		{
			// display failure page associated
			if ( failures_[i] != 0 )
			{
				failures_[i]->invoke(out, e);
				return;
			
			}
			else
			{
				// standard failure page here
				XCEPT_RAISE (xgi::exception::Exception, e.what()); 
			}
			
		}
		
	}	
	currentState_ = newState;
	// display page entering the new state
	pages_[currentState_]->invoke(out);

}

