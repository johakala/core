// $Id: Table.cc,v 1.3 2008/07/18 15:28:16 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xgi/Table.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTMLClasses.h"

xgi::Table::Row::Row(const std::string& id)
{
	id_ = id;
}

xgi::Table::Row::~Row()
{
}

const std::string& xgi::Table::Row::getId()
{
	return id_;
}

void xgi::Table::Row::addColumn(const std::string& name)
{
	columns_.push_back(name);
}
			
std::vector<std::string>& xgi::Table::Row::getColumns()
{
	return columns_;
}

void xgi::Table::Row::render (std::ostream& out, bool even)
{
	out << cgicc::tr() << std::endl;
	
	for (std::vector<std::string>::size_type i = 0; i < columns_.size(); ++i)
	{
		out << cgicc::td ( columns_.at(i) );
	}
	
	out << std::endl << cgicc::tr() << std::endl;
}


xgi::Table::Table(const std::string& id)
{
	id_ = id;
}

xgi::Table::~Table()
{
}


std::vector<xgi::Table::Row>::iterator xgi::Table::addRow(const std::string& id)
{
	rows_.push_back( Row (id) );
	return (--rows_.end());
}

std::vector<xgi::Table::Row>& xgi::Table::getRows()
{
	return rows_;
}

void xgi::Table::addColumn (const std::string& name)
{
	columns_.push_back(name);
}

std::vector<std::string>& xgi::Table::getColumns()
{
	return columns_;
}

void xgi::Table::render (std::ostream& out)
{
	out << cgicc::table().set("class","xdaq-table") << std::endl;	       
	
	out << cgicc::thead();	
	out << cgicc::tr();	
	for (std::vector<std::string>::size_type h = 0; h < columns_.size(); ++h)
	{
		out << cgicc::th(columns_[h]).set("title",columns_[h]);
	}	
	out << cgicc::tr();
	out << cgicc::thead();	
        out << cgicc::tbody() << std::endl;

	bool row_even = false;
	for (std::vector<std::string>::size_type i = 0; i < rows_.size(); ++i)
	{		
		rows_[i].render(out, row_even);
		row_even = !row_even;
	}	
	
	out << cgicc::tbody() << std::endl; 
	out << cgicc::table() << std::endl;
}

std::ostream& xgi::operator<<(std::ostream& out, xgi::Table& obj)
{
	obj.render(out);
	return out; 
}

