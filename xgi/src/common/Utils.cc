// $Id: Utils.cc,v 1.34 2008/07/18 15:28:16 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/
#include <iostream>
#include <iomanip>
#include <string>
#include <sstream>
#include <time.h>

#include "xcept/tools.h"
#include "toolbox/utils.h"
#include "toolbox/string.h"
#include <sstream>
#include "xgi/Utils.h"
#include "cgicc/HTMLClasses.h"

//const size_t ReadSegmentSize = 8192;
//const size_t ReadHeaderSize = 2048;

const char* xgi::Utils::DaysOfWeek [] = {"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"};				
const char* xgi::Utils::MonthsOfYear [] = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};

std::string xgi::Utils::getResponsePhrase(int code)
{
	switch (code)
	{
		case 200:
			return "OK";
			break;
		case 201:
			return "Created";
			break;
		case 202:
			return "Accepted";
			break;
		case 203:
			return "Partial Information";
			break;
		case 204:
			return "No Response";
			break;
		case 301:
			return "Moved Permanently";
			break;
		case 302:
			return "Found";
			break;
		case 400:
			return "Bad Request";
			break;
		case 401:
			return "Unauthorized";
			break;
		case 402:
			return "Payment Required";
			break;
		case 403:
			return "Forbidden";
			break;
		case 404:
			return "Not Found";
			break;
		case 405:
			return "Method Not Allowed";
			break;
		case 500:
			return "Internal Server Error";
			break;
		case 501:
			return "Not Implemented";
			break;
		case 502:
			return "Service temporarily overloaded";
			break;
		case 503:
			return "Gateway timeout";
			break;
		default:
			return "Invalid Status Code";
			break;
	}
}



std::string xgi::Utils::getFailurePage(const std::string& failure, const std::string& comment)
{
	std::ostringstream page;
	page << "<html><head>";
	page << "<title>" << failure << "</title></head><body>";
	page << "<font size=2>If you think, this page has been displayed because of a processing error, " << std::endl;
	page << "you may have a look at the exception trace below.<p></font>" << std::endl;

	page << "<table border=0 width=70% id=failure_table>";
	page << "<tr>";
	page << "<td align=center valign=top>";
	page << "<img align=\"center\" src=\"/xgi/images/safe.gif\" border=0>";
	page << "</td>";
	page << "<td align=left valign=top>";
	page << "<h2><font face=\"Arial\">" << failure << "</font></h2><p>";
	page << "<font face=\"Arial\" size=\"2\">" << comment << "</font></br>";
	page << "</td>";
	page << "</tr>";
	page << "</table>";
	page << "<p>" << std::endl;
	page << "</body></html>";
	return page.str();
}

//! getSimpleFailurePage provides a failure page without any links. This page must be used in case of authentication
//! failure where no images and other information may be accessible
std::string xgi::Utils::getSimpleFailurePage(const std::string& failure, const std::string& comment)
{
	std::ostringstream page;
	page << "<html><head>";
	page << "<title>" << failure << "</title></head><body>";

	page << "<table border=0 width=70% id=failure_table>";
	page << "<tr>";
	page << "<td align=left valign=top>";
	page << "<h2><font face=\"Arial\">" << failure << "</font></h2><p>";
	page << "<font face=\"Arial\" size=\"2\">" << comment << "</font></br>";
	page << "</td>";
	page << "</tr>";
	page << "</table>";
	page << "<p>" << std::endl;
	page << "</body></html>";
	return page.str();
}

void xgi::Utils::ErrorDialog(std::ostream* out, xcept::Exception& e, const std::string& comment, const std::string& url)
{
	*out << "<html><head>";
	*out << "<link rel=\"stylesheet\" type=\"text/css\" href=\"/xgi/html/xgi.css\">";
	*out << "<title>" << comment << "</title></head><body>";

	*out << "<table border=0 width=70% id=failure_table>" << std::endl;
	*out << "<tr>" << std::endl;
		*out << "<td align=center valign=top>" << std::endl;
		*out << "<img align=\"center\" src=\"/xgi/images/safe.gif\" border=0>" << std::endl;
		*out << "</td>" << std::endl;
		*out << "<td align=left valign=top>" << std::endl;
		*out << "<h2><font face=\"Arial\">" << comment << "</font></h2><p>" << std::endl;
		*out << "<font face=\"Arial\" size=\"2\">" << xcept::htmlformat_exception_history(e) << "</font></br>" << std::endl;
		*out << "</td>" << std::endl;
	*out << "</tr>" << std::endl;
	
	*out << "<tr><td/><td>" << std::endl;
	
	*out << "<form method=\"get\" action=\"" << url << "\">" << std::endl;
	*out << "<input type=\"submit\" value=\"OK\">" << std::endl;
	*out << "</form>" << std::endl;
	
	*out << "</td></tr>" << std::endl;
	
	*out << "</table>" << std::endl;
	
	*out << "<p>" << std::endl;
	*out << "</body></html>" << std::endl;
}



std::string xgi::Utils::getCurrentTime()
{
	time_t nTime = time( 0 );
	//char psTime[30];
	struct tm oTm;
	 gmtime_r (&nTime, &oTm);
	/*std::snprintf (psTime, 30, "%s, %02d %s %04d %02d:%02d:%02d GMT",
		xgi::Utils::DaysOfWeek[ oTm->tm_wday ],
		oTm->tm_mday,
		xgi::Utils::MonthsOfYear [ oTm->tm_mon ],
		oTm->tm_year + 1900,
		oTm->tm_hour,
		oTm->tm_min,
		oTm->tm_sec );
	*/	
	std::stringstream t;
	t.fill('0');
	t << std::right;
	t << xgi::Utils::DaysOfWeek[ oTm.tm_wday ] << ", ";
	t << std::setw(2) << oTm.tm_mday << " ";
	t << xgi::Utils::MonthsOfYear [ oTm.tm_mon ] << " ";
	t << std::setw(4) << oTm.tm_year + 1900 << " ";
	t << std::setw(2) <<  oTm.tm_hour << ":" << oTm.tm_min << ":" << oTm.tm_sec << " GMT";
	
	
	return t.str();
}		

cgicc::const_form_iterator xgi::Utils::getFormElement(const cgicc::Cgicc& cgi, const std::string& name) throw (xgi::exception::Exception)
{
	cgicc::const_form_iterator i = cgi.getElement(name);
	if (i != cgi.getElements().end())
	{
		return i;
	}
	else
	{
		std::string msg = "Could not find form element: ";
		msg += name;
		XCEPT_RAISE (xgi::exception::Exception, msg);
	}
}

bool xgi::Utils::hasFormElement(const cgicc::Cgicc& cgi, const std::string& name)
{
	cgicc::const_form_iterator i = cgi.getElement(name);
	if (i != cgi.getElements().end())
	{
		return true;
	}
	
	return false;
	
}

/*
void xgi::Utils::getWidget
(
	std::ostream      * out, 
	const std::string & text, 
	const std::string & tooltip,
	const std::string & url, 
	const std::string & command, 
	const std::string & icon
)
{
	std::string shortcut;
	if ( text.size() > 20 )
	{
		shortcut = text.substr(0, 15);
		shortcut += "[...]";
	}
	else
	{
		shortcut = text;
	}
	
	*out << "<div style=\"padding: 3px; border: 3px solid #FFFFFF; width: 120px; height: 120px; float:left;\" align=\"center\">" << std::endl;
	*out << "<div style=\"padding: 3px; border: 1px solid #FFFFFF; width: 64; height: 100; \" align=\"center\">" << std::endl;
	std::stringstream applicationlink;
	applicationlink << "<a href=\"" << url << "/" << command << "\">";
	applicationlink << "<img title=\"" << tooltip << "\" src=\"" << icon << "\" alt=\"" << text << "\" width=\"64\" height=\"64\" border=\"0\" ></a>";
	*out << std::endl;
	*out << applicationlink.str() << std::endl;
	*out << "</div>";
	*out << std::endl;
	std::stringstream titlelink;
	
	titlelink <<  "<a title=\"" << tooltip << "\" href=\"" << url << "/" << command << "\">" << shortcut << "</a>";
	*out << titlelink.str() <<  cgicc::br() << std::endl;
	// *out << "<font size=1>" << tooltip << "</font>" << std::endl;
	*out << "</div>";
}
*/
