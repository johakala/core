// $Id: Input.cc,v 1.3 2008/07/18 15:28:16 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xgi/Input.h"

xgi::Input::Input(const char* buffer, size_t length)
	: instream_(std::string(buffer, length))
{
	length_ = length;
}

size_t xgi::Input::read(char* data, size_t length)
{
	instream_.read(data, length);
	return instream_.gcount();
}

void xgi::Input::write(const char* buffer, size_t length)
{
	instream_.write(buffer, length);
	length_ += length;
}

std::string xgi::Input::getenv(const char* varName)
{
	return environment_[varName];
}

void xgi::Input::putenv(const std::string& name, const std::string& value)
{
	environment_[name] = value;
}

std::istringstream& xgi::Input::cin()
{
	return (std::istringstream&)instream_;
}

