// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/
#include "xgi/framework/DefaultLayout.h"
#include "xgi/framework/UIManager.h"

#include "xcept/tools.h"

void xgi::framework::DefaultLayout::getHTMLHeader (xgi::framework::UIManager * manager, xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	*out << "<!doctype html>" 									<< std::endl;
	*out << "<html xmlns=\"http://www.w3.org/1999/xhtml\">" 					<< std::endl;
	*out << "<head>" 										<< std::endl;
	*out << "       <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />" 	<< std::endl;
	*out << "       <title>XDAQ HyperDAQ</title>" 							<< std::endl;
	*out << "</head>" 										<< std::endl;
	*out << "<body>" 										<< std::endl;
}

void xgi::framework::DefaultLayout::getHTMLFooter (xgi::framework::UIManager * manager, xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	*out << "</body>"     << std::endl;
	*out << "</html>"    << std::endl; 
}

void xgi::framework::DefaultLayout::Default (xgi::framework::UIManager * manager, xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception) 
{
	this->getHTMLHeader(manager, in, out);
	this->getHTMLFooter(manager, in, out);
}

void xgi::framework::DefaultLayout::noCallbackFound (xgi::framework::UIManager * manager, xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception) 
{
	this->getHTMLHeader(manager, in, out);
	this->getHTMLFooter(manager, in, out);
}

void xgi::framework::DefaultLayout::errorPage (xgi::framework::UIManager * manager, xgi::Input * in, xgi::Output * out, xcept::Exception& e) throw (xgi::exception::Exception)
{
	this->getHTMLHeader(manager, in, out);
	*out << xcept::htmlformat_exception_history(e);
	this->getHTMLFooter(manager, in, out);
}

