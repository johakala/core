// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/
#include "xgi/framework/UIManager.h"
#include "xgi/framework/DefaultLayout.h"
#include "xgi/framework/Method.h"

xgi::framework::AbstractLayout * xgi::framework::UIManager::layout_ = xgi::framework::UIManager::getDefaultLayout() ;

xgi::framework::UIManager::UIManager (xdaq::Application * application):
application_(application)
{
	xgi::framework::deferredbind(application, this, &xgi::framework::UIManager::noCallbackFound, "noCallbackFound" );
	xgi::framework::deferredbind(application, this, &xgi::framework::UIManager::Default, "Default" );
}

void xgi::framework::UIManager::getHTMLHeader (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	layout_->getHTMLHeader(this,in,out);
}

void xgi::framework::UIManager::getHTMLFooter (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	layout_->getHTMLFooter(this,in,out);
}

// xgi HTTP callbacks  
void xgi::framework::UIManager::Default (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	layout_->Default(this,in,out);
}

void xgi::framework::UIManager::noCallbackFound (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	layout_->noCallbackFound(this,in,out);
}

void xgi::framework::UIManager::errorPage (xgi::Input * in, xgi::Output * out, xcept::Exception& e) throw (xgi::exception::Exception)
{
	layout_->errorPage(this,in,out,e);
}

xgi::framework::AbstractLayout * xgi::framework::UIManager::getDefaultLayout()
{
	static xgi::framework::AbstractLayout * defaultLayout_ = 0;
	if ( defaultLayout_ == 0 )
	{
		defaultLayout_ = new xgi::framework::DefaultLayout();
	}
        return defaultLayout_;
}


void xgi::framework::UIManager::setLayout(xgi::framework::AbstractLayout * layout)
{
	xgi::framework::UIManager::layout_ = layout;
}

xgi::framework::AbstractLayout *  xgi::framework::UIManager::getLayout()
{
	return xgi::framework::UIManager::layout_;
}


xdaq::Application *  xgi::framework::UIManager::getApplication()
{
	return application_;
}
