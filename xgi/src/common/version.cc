// $Id: version.cc,v 1.3 2008/07/18 15:28:16 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "config/version.h"
#include "xcept/version.h"
#include "pt/version.h"
#include "toolbox/version.h"
#include "xgi/version.h"

GETPACKAGEINFO(xgi)

void xgi::checkPackageDependencies() throw (config::PackageInfo::VersionException)
{
	CHECKDEPENDENCY(config);
	CHECKDEPENDENCY(xcept);
	CHECKDEPENDENCY(toolbox); 
	CHECKDEPENDENCY(pt);  
}

std::set<std::string, std::less<std::string> > xgi::getPackageDependencies()
{
	std::set<std::string, std::less<std::string> > dependencies;

	ADDDEPENDENCY(dependencies,config); 
	ADDDEPENDENCY(dependencies,xcept); 
	ADDDEPENDENCY(dependencies,toolbox); 
	ADDDEPENDENCY(dependencies,pt); 
  
	return dependencies;
}	
	
