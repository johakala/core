// $Id: Output.cc,v 1.4 2008/07/18 15:28:16 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xgi/Output.h"

xgi::Output::Output():
	 std::ostringstream(),
	 header_("HTTP/1.1", 200, "OK")
{
}


cgicc::HTTPResponseHeader& xgi::Output::getHTTPResponseHeader()
{
	return header_;
}

void xgi::Output::setHTTPResponseHeader(const cgicc::HTTPResponseHeader& header)
{
	header_ = header;
}
