// $Id: PackageInfo.cc,v 1.5 2008/07/18 15:26:43 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "config/PackageInfo.h"
#include <sstream>
#include <string>
#include <iomanip>

config::PackageInfo::PackageInfo(const  std::string & name, const  std::string & vset,
			 const std::string & description, const std::string & link, const std::string & authors, 
			  const std::string & summary, const std::string & date, const std::string & time)
{
	name_ = name;
	vset_ = vset;
	description_ = description;
	link_ = link;
	date_ = date;
	time_ = time;
	authors_ = authors;
	summary_ = summary;
}

std::string config::PackageInfo::getDescription()
{
	return description_;
}

std::string config::PackageInfo::getLink()
{
	return link_;
}

void config::PackageInfo::setDescription (const std::string & description)
{
	 description_ = description;
}

void config::PackageInfo::setLink (const std::string & link)
{
	 link_ = link;
}

std::string config::PackageInfo::getLatestVersion()
{
    std::set<std::string, std::less<std::string> > implemented = stringToVersionSet(vset_);

/* debugging
 	std::map<std::string, std::string, std::less<std::string> > test;
	for (  std::set<std::string, std::less<std::string> >::iterator j = implemented.begin(); j != implemented.end(); j++ )
	{
		std::cout << ">" << *j ;
		test[*j] = "";
	}
	for (  std::map<std::string, std::string, std::less<std::string> >::iterator k = test.begin(); k != test.end(); k++ )
	{
		std::cout << "->" << (*k).first ;
	}
	
	std::cout  << std::endl;
*/

    std::set<std::string, std::less<std::string> >::iterator lasti;
    lasti = implemented.end();
    lasti--;
    //std::cout << "Latest [" << *lasti << "]" << std::endl;
    return *lasti;
}

std::set<std::string, std::less<std::string> > config::PackageInfo::getSupportedVersions()
{
	return stringToVersionSet(vset_);

}


std::string config::PackageInfo::getName()
{
	return name_;
}	

std::string config::PackageInfo::getAuthors()
{
	return authors_;
}

std::string config::PackageInfo::getSummary()
{
	return summary_;
}

std::string config::PackageInfo::getDate()
{
	return date_;
}	




std::string config::PackageInfo::getTime()
{
	return time_;
}	


void config::PackageInfo::checkVersions(const std::string& vset) throw (ImplementationTooOldException,ImplementationNewerException)
{
    // make set intersection
    std::set<std::string, std::less<std::string> > expected = stringToVersionSet(vset);
    std::set<std::string, std::less<std::string> > implemented = stringToVersionSet(vset_);

    //cout << "Size of expected:" << expected.size() << endl;
    //cout << "Size of implemented:" << implemented.size() << endl;
    // check if too old or too recent
    std::set<std::string, std::less<std::string> >::iterator lasti;
    lasti = implemented.end();
    lasti--;

    std::set<std::string, std::less<std::string> >::iterator laste;
    laste = expected.end();
    laste--;


    //cout << "The most recent implementation version is:" << *lasti << endl;
    //cout << "The most recent expected version is:" << *laste << endl;
    if (*laste == *lasti ) 
    {
        return; // compatible
    }        
    else if (*laste > *lasti)
    {

        //definition version is newer than implementation version

        if (*(expected.begin()) <= *lasti) 
        {
            return; // compatible
        }    
        else
        { 
            std::string msg = "Found latest implementation for package ";
            msg += name_;
	    msg += " (";
	    msg += *lasti;
	    msg += ") older than oldest expected (";
	    msg += *(expected.begin());
            msg += ")";
            //throw ImplementationTooOldException(msg); // Found implementation of package older than expected
	    XCEPT_RAISE(config::PackageInfo::ImplementationTooOldException,msg);
        }
    }        
    else
    {

        //definition version is older than implementation version*/

        if ((*implemented.begin()) <= *laste) 
        {
            return; // compatible
        }    
        else
        {
            std::string msg = "Found oldest implementation for package ";
            msg += name_;
	    msg += " (";
	    msg += *(implemented.begin());
	    msg += ") newer than the most recent expected (";
	    msg += *laste;
            msg += ")";
            //throw ImplementationNewerException(msg); // Found implementation of package newer that expected"
	    XCEPT_RAISE(config::PackageInfo::ImplementationNewerException,msg);
        }
    }
}


std::set<std::string, std::less<std::string> > config::PackageInfo::stringToVersionSet(const std::string & vset )
{           
	std::set<std::string, std::less<std::string> > versions;
        versions.clear();

	// add versions to set
	std::string::const_iterator curPos = vset.begin();
	std::string::const_iterator end = vset.end();
	while ( curPos < end )
	{
		std::string::const_iterator nextDelim = std::find(curPos,end,',');
		while (isspace(*curPos)) // ignore leading space
			++curPos;
		std::string source(curPos, nextDelim);
		std::istringstream is(source.c_str());

		size_t major = 0;
		size_t minor = 0;
		size_t patch = 0;
		char c = 0;
		
		is >> major >> c >> minor >> c >> patch;
		
		curPos = ++nextDelim;
		
		std::stringstream normalized;
		
		normalized << std::setfill('0') << std::setw(2) << major << ".";
		normalized << std::setfill('0') << std::setw(2) << minor << ".";
		normalized << std::setfill('0') << std::setw(2) << patch;
		
		versions.insert(normalized.str());
		
		//std::cout << "Version [" << normalized.str() << "]," ;
         }
	//std::cout << std::endl;
        return versions;
}
  
