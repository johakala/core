# $Id$

#########################################################################
# XDAQ Components for Distributed Data Acquisition                      #
# Copyright (C) 2000-2018, CERN.                                        #
# All rights reserved.                                                  #
# Authors: L. Orsini and D. Simelevicius                                #
#                                                                       #
# For the licensing terms see LICENSE.                                  #
# For the list of contributors see CREDITS.                             #
#########################################################################

##
# !!!! This macros are used only for core tools development       !!!
# !!!! They point always to the packages in the development tree  !!!
# !!!! These paths never point to an installation                 !!!
##

ifndef B2IN_UTILS_PREFIX
B2IN_UTILS_PREFIX=$(BUILD_HOME)/b2in/utils
endif
B2IN_UTILS_INCLUDE_PREFIX=$(B2IN_UTILS_PREFIX)/include $(B2IN_UTILS_PREFIX)/include/$(XDAQ_OS)
B2IN_UTILS_LIB_PREFIX=$(B2IN_UTILS_PREFIX)/lib/$(XDAQ_OS)/$(XDAQ_PLATFORM)

ifndef CONFIG_PREFIX
CONFIG_PREFIX=$(BUILD_HOME)/config
endif
CONFIG_INCLUDE_PREFIX=$(CONFIG_PREFIX)/include $(CONFIG_PREFIX)/include/$(XDAQ_OS)
CONFIG_LIB_PREFIX=$(CONFIG_PREFIX)/lib/$(XDAQ_OS)/$(XDAQ_PLATFORM)

ifndef XCEPT_PREFIX
XCEPT_PREFIX=$(BUILD_HOME)/xcept
endif
XCEPT_INCLUDE_PREFIX=$(XCEPT_PREFIX)/include $(XCEPT_PREFIX)/include/$(XDAQ_OS)
XCEPT_LIB_PREFIX=$(XCEPT_PREFIX)/lib/$(XDAQ_OS)/$(XDAQ_PLATFORM)

ifndef PT_PREFIX
PT_PREFIX=$(BUILD_HOME)/pt
endif
PT_INCLUDE_PREFIX=$(PT_PREFIX)/include $(PT_PREFIX)/include/$(XDAQ_OS)
PT_LIB_PREFIX=$(PT_PREFIX)/lib/$(XDAQ_OS)/$(XDAQ_PLATFORM)

ifndef TOOLBOX_PREFIX
TOOLBOX_PREFIX=$(BUILD_HOME)/toolbox
endif
TOOLBOX_INCLUDE_PREFIX=$(TOOLBOX_PREFIX)/include $(TOOLBOX_PREFIX)/include/$(XDAQ_OS)
TOOLBOX_LIB_PREFIX=$(TOOLBOX_PREFIX)/lib/$(XDAQ_OS)/$(XDAQ_PLATFORM)

ifndef XOAP_PREFIX
XOAP_PREFIX=$(BUILD_HOME)/xoap
endif
XOAP_INCLUDE_PREFIX=$(XOAP_PREFIX)/include $(XOAP_PREFIX)/include/$(XDAQ_OS)
XOAP_LIB_PREFIX=$(XOAP_PREFIX)/lib/$(XDAQ_OS)/$(XDAQ_PLATFORM)

ifndef LOG_UDPAPPENDER_PREFIX
LOG_UDPAPPENDER_PREFIX=$(BUILD_HOME)/log/udpappender
endif
LOG_UDPAPPENDER_INCLUDE_PREFIX=$(LOG_UDPAPPENDER_PREFIX)/include $(LOG_UDPAPPENDER_PREFIX)/include/$(XDAQ_OS)
LOG_UDPAPPENDER_LIB_PREFIX=$(LOG_UDPAPPENDER_PREFIX)/lib/$(XDAQ_OS)/$(XDAQ_PLATFORM)

ifndef LOG_XMLAPPENDER_PREFIX
LOG_XMLAPPENDER_PREFIX=$(BUILD_HOME)/log/xmlappender
endif
LOG_XMLAPPENDER_INCLUDE_PREFIX=$(LOG_XMLAPPENDER_PREFIX)/include $(LOG_XMLAPPENDER_PREFIX)/include/$(XDAQ_OS)
LOG_XMLAPPENDER_LIB_PREFIX=$(LOG_XMLAPPENDER_PREFIX)/lib/$(XDAQ_OS)/$(XDAQ_PLATFORM)

ifndef XOAP_FILTER_PREFIX
XOAP_FILTER_PREFIX=$(BUILD_HOME)/xoap/filter
endif
XOAP_FILTER_INCLUDE_PREFIX=$(XOAP_FILTER_PREFIX)/include $(XOAP_FILTER_PREFIX)/include/$(XDAQ_OS)
XOAP_FILTER_LIB_PREFIX=$(XOAP_FILTER_PREFIX)/lib/$(XDAQ_OS)/$(XDAQ_PLATFORM)

ifndef XGI_PREFIX
XGI_PREFIX=$(BUILD_HOME)/xgi
endif
XGI_INCLUDE_PREFIX=$(XGI_PREFIX)/include $(XGI_PREFIX)/include/$(XDAQ_OS)
XGI_LIB_PREFIX=$(XGI_PREFIX)/lib/$(XDAQ_OS)/$(XDAQ_PLATFORM)

ifndef HYPERDAQ_PREFIX
HYPERDAQ_PREFIX=$(BUILD_HOME)/hyperdaq
endif
HYPERDAQ_INCLUDE_PREFIX=$(HYPERDAQ_PREFIX)/include $(HYPERDAQ_PREFIX)/include/$(XDAQ_OS)
HYPERDAQ_LIB_PREFIX=$(HYPERDAQ_PREFIX)/lib/$(XDAQ_OS)/$(XDAQ_PLATFORM)

ifndef XDATA_PREFIX
XDATA_PREFIX=$(BUILD_HOME)/xdata
endif
XDATA_INCLUDE_PREFIX=$(XDATA_PREFIX)/include $(XDATA_PREFIX)/include/$(XDAQ_OS)
XDATA_LIB_PREFIX=$(XDATA_PREFIX)/lib/$(XDAQ_OS)/$(XDAQ_PLATFORM)

ifndef XI2O_PREFIX
XI2O_PREFIX=$(BUILD_HOME)/i2o
endif
XI2O_INCLUDE_PREFIX=$(XI2O_PREFIX)/include $(XI2O_PREFIX)/include/$(XDAQ_OS)
XI2O_LIB_PREFIX=$(XI2O_PREFIX)/lib/$(XDAQ_OS)/$(XDAQ_PLATFORM)

ifndef XI2O_UTILS_PREFIX
XI2O_UTILS_PREFIX=$(BUILD_HOME)/i2o/utils
endif
XI2O_UTILS_INCLUDE_PREFIX=$(XI2O_UTILS_PREFIX)/include $(XI2O_UTILS_PREFIX)/include/$(XDAQ_OS)
XI2O_UTILS_LIB_PREFIX=$(XI2O_UTILS_PREFIX)/lib/$(XDAQ_OS)/$(XDAQ_PLATFORM)

ifndef B2IN_NUB_PREFIX
B2IN_NUB_PREFIX=$(BUILD_HOME)/b2in/nub
endif
B2IN_NUB_INCLUDE_PREFIX=$(B2IN_NUB_PREFIX)/include $(B2IN_NUB_PREFIX)/include/$(XDAQ_OS)
B2IN_NUB_LIB_PREFIX=$(B2IN_NUB_PREFIX)/lib/$(XDAQ_OS)/$(XDAQ_PLATFORM)

ifndef XDAQ_PREFIX
XDAQ_PREFIX=$(BUILD_HOME)/xdaq
endif
XDAQ_INCLUDE_PREFIX=$(XDAQ_PREFIX)/include $(XDAQ_PREFIX)/include/$(XDAQ_OS)
XDAQ_LIB_PREFIX=$(XDAQ_PREFIX)/lib/$(XDAQ_OS)/$(XDAQ_PLATFORM)

ifndef EXECUTIVE_PREFIX
EXECUTIVE_PREFIX=$(BUILD_HOME)/executive
endif
EXECUTIVE_INCLUDE_PREFIX=$(EXECUTIVE_PREFIX)/include $(EXECUTIVE_PREFIX)/include/$(XDAQ_OS)
EXECUTIVE_LIB_PREFIX=$(EXECUTIVE_PREFIX)/lib/$(XDAQ_OS)/$(XDAQ_PLATFORM)

ifndef XERCES_PREFIX
XERCES_PREFIX=$(BUILD_HOME)/extern/xerces/$(XDAQ_PLATFORM)
endif
XERCES_INCLUDE_PREFIX=$(XERCES_PREFIX)/include
XERCES_LIB_PREFIX=$(XERCES_PREFIX)/lib

ifndef XALAN_PREFIX
XALAN_PREFIX=$(BUILD_HOME)/extern/xalan/$(XDAQ_PLATFORM)
endif
XALAN_INCLUDE_PREFIX=$(XALAN_PREFIX)/include
XALAN_LIB_PREFIX=$(XALAN_PREFIX)/lib

ifndef LOG4CPLUS_PREFIX
LOG4CPLUS_PREFIX=$(BUILD_HOME)/extern/log4cplus/$(XDAQ_PLATFORM)
endif
LOG4CPLUS_INCLUDE_PREFIX=$(LOG4CPLUS_PREFIX)/include
LOG4CPLUS_LIB_PREFIX=$(LOG4CPLUS_PREFIX)/lib


ifndef ASYNCRESOLV_PREFIX
ASYNCRESOLV_PREFIX=$(BUILD_HOME)/extern/asyncresolv/$(XDAQ_PLATFORM)
endif
ASYNCRESOLV_INCLUDE_PREFIX=$(ASYNCRESOLV_PREFIX)/include
ASYNCRESOLV_LIB_PREFIX=$(ASYNCRESOLV_PREFIX)/lib

ifndef I2O_PREFIX
I2O_PREFIX=$(BUILD_HOME)/extern/i2o
endif
I2O_INCLUDE_PREFIX=$(I2O_PREFIX)/include
I2O_LIB_PREFIX=$(I2O_PREFIX)/lib


ifndef CGICC_PREFIX
CGICC_PREFIX=$(BUILD_HOME)/extern/cgicc/$(XDAQ_PLATFORM)
endif
CGICC_INCLUDE_PREFIX=$(CGICC_PREFIX)/include
CGICC_LIB_PREFIX=$(CGICC_PREFIX)/lib


ifndef MIMETIC_PREFIX
MIMETIC_PREFIX=$(BUILD_HOME)/extern/mimetic/$(XDAQ_PLATFORM)
endif
MIMETIC_INCLUDE_PREFIX=$(MIMETIC_PREFIX)/include
MIMETIC_LIB_PREFIX=$(MIMETIC_PREFIX)/lib

ifndef PSTREAMS_PREFIX
PSTREAMS_PREFIX=$(BUILD_HOME)/extern/pstreams/$(XDAQ_PLATFORM)
endif
PSTREAMS_INCLUDE_PREFIX=$(PSTREAMS_PREFIX)/include
PSTREAMS_LIB_PREFIX=$(PSTREAMS_PREFIX)/lib

ifndef SQLITE_PREFIX
SQLITE_PREFIX=$(BUILD_HOME)/extern/sqlite/$(XDAQ_PLATFORM)
endif
SQLITE_INCLUDE_PREFIX=$(SQLITE_PREFIX)/include
SQLITE_LIB_PREFIX=$(SQLITE_PREFIX)/lib

ifndef SLP_PREFIX
SLP_PREFIX=$(BUILD_HOME)/extern/slp/$(XDAQ_PLATFORM)
endif
SLP_INCLUDE_PREFIX=$(SLP_PREFIX)/include
SLP_LIB_PREFIX=$(SLP_PREFIX)/lib

ifndef JANSSON_PREFIX
JANSSON_PREFIX=$(BUILD_HOME)/extern/jansson/$(XDAQ_PLATFORM)
endif
JANSSON_INCLUDE_PREFIX=$(JANSSON_PREFIX)/include
JANSSON_LIB_PREFIX=$(JANSSON_PREFIX)/lib

ifndef EVENTING_API_PREFIX
EVENTING_API_PREFIX=$(BUILD_HOME)/eventing/api
endif
EVENTING_API_INCLUDE_PREFIX=$(EVENTING_API_PREFIX)/include $(EVENTING_API_PREFIX)/include/$(XDAQ_OS)
EVENTING_API_LIB_PREFIX=$(EVENTING_API_PREFIX)/lib/$(XDAQ_OS)/$(XDAQ_PLATFORM)

ifndef XSLP_PREFIX
XSLP_PREFIX=$(BUILD_HOME)/xslp
endif
XSLP_INCLUDE_PREFIX=$(XSLP_PREFIX)/include $(XSLP_PREFIX)/include/$(XDAQ_OS)
XSLP_LIB_PREFIX=$(XSLP_PREFIX)/lib/$(XDAQ_OS)/$(XDAQ_PLATFORM)

ifndef XPLORE_PREFIX
XPLORE_PREFIX=$(BUILD_HOME)/xplore
endif
XPLORE_INCLUDE_PREFIX=$(XPLORE_PREFIX)/include $(XPLORE_PREFIX)/include/$(XDAQ_OS)
XPLORE_LIB_PREFIX=$(XPLORE_PREFIX)/lib/$(XDAQ_OS)/$(XDAQ_PLATFORM)

ifndef XPLORE_UTILS_PREFIX
XPLORE_UTILS_PREFIX=$(BUILD_HOME)/xplore/utils
endif
XPLORE_UTILS_INCLUDE_PREFIX=$(XPLORE_UTILS_PREFIX)/include $(XPLORE_UTILS_PREFIX)/include/$(XDAQ_OS)
XPLORE_UTILS_LIB_PREFIX=$(XPLORE_UTILS_PREFIX)/lib/$(XDAQ_OS)/$(XDAQ_PLATFORM)

ifndef WS_ADDRESSING_PREFIX
WS_ADDRESSING_PREFIX=$(BUILD_HOME)/ws/addressing
endif
WS_ADDRESSING_INCLUDE_PREFIX=$(WS_ADDRESSING_PREFIX)/include $(WS_ADDRESSING_PREFIX)/include/$(XDAQ_OS)
WS_ADDRESSING_LIB_PREFIX=$(WS_ADDRESSING_PREFIX)/lib/$(XDAQ_OS)/$(XDAQ_PLATFORM)

ifndef TCPLA_PREFIX
TCPLA_PREFIX=$(BUILD_HOME)/tcpla
endif
TCPLA_INCLUDE_PREFIX=$(TCPLA_PREFIX)/include
TCPLA_LIB_PREFIX=$(TCPLA_PREFIX)/lib/$(XDAQ_OS)/$(XDAQ_PLATFORM)

ifndef PTBLIT_PREFIX
PTBLIT_PREFIX=$(BUILD_HOME)/pt/blit
endif
PTBLIT_INCLUDE_PREFIX=$(PTBLIT_PREFIX)/include
PTBLIT_LIB_PREFIX=$(PTBLIT_PREFIX)/lib/$(XDAQ_OS)/$(XDAQ_PLATFORM)

ifndef PTTCP_PREFIX
PTTCP_PREFIX=$(BUILD_HOME)/pt/tcp
endif
PTTCP_INCLUDE_PREFIX=$(PTTCP_PREFIX)/include $(PTTCP_PREFIX)/include/$(XDAQ_OS)
PTTCP_LIB_PREFIX=$(PTTCP_PREFIX)/lib/$(XDAQ_OS)/$(XDAQ_PLATFORM)

ifndef PTUTCP_PREFIX
PTUTCP_PREFIX=$(BUILD_HOME)/pt/utcp
endif
PTUTCP_INCLUDE_PREFIX=$(PTUTCP_PREFIX)/include $(PTUTCP_PREFIX)/include/$(XDAQ_OS)
PTUTCP_LIB_PREFIX=$(PTUTCP_PREFIX)/lib/$(XDAQ_OS)/$(XDAQ_PLATFORM)

