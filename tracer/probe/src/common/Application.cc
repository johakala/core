// $Id: $

/*************************************************************************
 * XDAQ Tracer Probe                      								 *
 * Copyright (C) 2000-2013, CERN.			                  			 *
 * All rights reserved.                                                  *
 * Authors: A. Forrest and L. Orsini				 					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                       		 *
 * For the list of contributors see CREDITS.   			         		 *
 *************************************************************************/

#include "tracer/probe/Application.h"

#include "b2in/nub/Method.h"

#include "xdaq/Event.h"
#include "xdaq/EndpointAvailableEvent.h"
#include "xdaq/InstantiateApplicationEvent.h"

#include "xdaq/ApplicationContextImpl.h"

#include "xgi/framework/Method.h"
#include "xgi/Utils.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTMLClasses.h"

#include "toolbox/task/TimerFactory.h"

#include "toolbox/regex.h"
#include "toolbox/utils.h"
#include "toolbox/Runtime.h"

#include "xcept/tools.h"

#include "xoap/DOMParserFactory.h"
#include "xoap/domutils.h"

#include "xdata/exdr/FixedSizeOutputStreamBuffer.h"
#include "xdata/exdr/Serializer.h"

XDAQ_INSTANTIATOR_IMPL (tracer::probe::Application)

tracer::probe::Application::Application (xdaq::ApplicationStub * s) throw (xdaq::exception::Exception)
	: xdaq::Application(s), xgi::framework::UIManager(this), repositoryLock_(toolbox::BSem::FULL)
{
        events_ = toolbox::rlist<xdata::Properties>::create("tracer-probe-events");

	this->outgoingTraceCounter_ = 0;
	this->outgoingTraceLostCounter_ = 0;
	this->incomingTraceCounter_ = 0;

	this->maxPending_ = 100;

	this->jelFileName_ = "";

	this->IPCMessageSize_ = 2048;

	s->getDescriptor()->setAttribute("icon", "/tracer/probe/images/tracer-probe-icon.png");
	s->getDescriptor()->setAttribute("icon16x16", "/tracer/probe/images/tracer-probe-icon.png");

	s->getDescriptor()->setAttribute("service", "tracerprobe");

	this->getApplicationInfoSpace()->fireItemAvailable("jelFileName", &jelFileName_);

	this->getApplicationInfoSpace()->fireItemAvailable("maxPending", &maxPending_);

	this->getApplicationInfoSpace()->fireItemAvailable("IPCMessageSize", &IPCMessageSize_);

	xgi::framework::deferredbind(this, this, &tracer::probe::Application::Default, "Default");
	xgi::framework::deferredbind(this, this,  &tracer::probe::Application::inject, "inject");

	// Listen to events indicating the setting of the application's default values
	this->getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");
}

tracer::probe::Application::~Application ()
{
}

void tracer::probe::Application::timeExpired (toolbox::task::TimerEvent& e)
{
	std::string name = e.getTimerTask()->name;
	if (name == "tracer-processpending")
	{
		this->processPendingEvents();
	}
}

/*
 * Setup communications with the broker and eventing
 */
void tracer::probe::Application::actionPerformed (toolbox::Event& e)
{
	if (e.type() == "xdaq::EndpointAvailableEvent")
	{

	}
	else
	{
		repositoryLock_.take();

		std::stringstream debugss;
		debugss << "Captured event : " << e.type();
		LOG4CPLUS_DEBUG(this->getApplicationLogger(), debugss.str());

		incomingTraceCounter_ = incomingTraceCounter_ + 1;

		// AT THE VERY BEGINNING, THERE IS NO REAL XDAQ ENVIRONMENT STUFF SO EVENTS HAVE HALF ARSED INFORMATION, WHICH CRASHES THINGS BELOW

		xdata::Properties eventProperties;

		try
		{
			xdaq::Event & xdaqEvent = dynamic_cast<xdaq::Event&>(e);
			xdata::Properties & refP = dynamic_cast<xdata::Properties&>(xdaqEvent);
			eventProperties = refP;

			if (xdaqEvent.getOwnerApplication() == 0)
			{
				if (eventProperties.hasProperty("urn:xdaq-event:baseurl"))
				{
					eventProperties.setProperty("urn:tracer-event:notifier", eventProperties.getProperty("urn:xdaq-event:baseurl"));
				}
				else
				{
					eventProperties.setProperty("urn:tracer-event:notifier", "Unknown");
				}
			}
			else
			{
				std::stringstream url;
				url << xdaqEvent.getOwnerApplication()->getApplicationDescriptor()->getContextDescriptor()->getURL() << "/" << xdaqEvent.getOwnerApplication()->getApplicationDescriptor()->getURN();
				eventProperties.setProperty("urn:tracer-event:notifier", url.str());
			}
		}
		catch (const std::bad_cast& e)
		{
			LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Bad cast - event lost");
			outgoingTraceLostCounter_ = outgoingTraceLostCounter_ + 1;
			repositoryLock_.give();
			return;
		}

		eventProperties.setProperty("urn:tracer-event:type", e.type());

		eventProperties.setProperty("urn:xdaq-context:session", this->getApplicationContext()->getSessionId());
		eventProperties.setProperty("urn:tracer-event:timestamp", toolbox::TimeVal::gettimeofday().toString(toolbox::TimeVal::gmt));

		if (e.type() == "urn:xdaq-event:InstantiateApplication")
		{
			// get properties for instantiated application
			xdaq::InstantiateApplicationEvent & iae = dynamic_cast<xdaq::InstantiateApplicationEvent&>(e);

			eventProperties.setProperty("urn:xdaq-application:qualifiedEventSchemaURI", "http://xdaq.web.cern.ch/xdaq/xsd/2013/QualifiedTracerEvent-10.xsd");

			// Optional fields
			eventProperties.setProperty("urn:xdaq-application:service", iae.getApplicationDescriptor()->getAttribute("service"));
			std::set < std::string > zones = this->getApplicationContext()->getZoneNames();
			eventProperties.setProperty("urn:xdaq-application:zone", toolbox::printTokenSet(zones, ","));
			eventProperties.setProperty("urn:xdaq-application:group", iae.getApplicationDescriptor()->getAttribute("group"));
			eventProperties.setProperty("urn:xdaq-application:uuid", iae.getApplicationDescriptor()->getAttribute("uuid"));
			eventProperties.setProperty("urn:xdaq-application:class", iae.getApplicationDescriptor()->getClassName());
			eventProperties.setProperty("urn:xdaq-application:id", iae.getApplicationDescriptor()->getAttribute("id"));
			eventProperties.setProperty("urn:xdaq-application:context", iae.getApplicationDescriptor()->getContextDescriptor()->getURL());

			if (iae.getApplicationDescriptor()->hasInstanceNumber())
			{
				eventProperties.setProperty("urn:xdaq-application:instance", iae.getApplicationDescriptor()->getAttribute("instance"));
			}
			else
			{
				eventProperties.setProperty("urn:xdaq-application:instance", "");
			}

			if (eventProperties.getProperty("urn:tracer-event:notifier") == "Unknown")
			{
				// Originator is set to 'Unknown', but we can construct it from other available information
				// e.g. http://srv-c2d06-17.cms:30404/urn:xdaq-application:lid=7
				// from ;
				//urn:xdaq-event:InstantiateApplication
				//urn:xdaq-application:context=['http://srv-c2d06-17.cms:30404']
				//urn:xdaq-application:id=['7']
				std::stringstream url;
				url << eventProperties.getProperty("urn:xdaq-application:context") << "/urn:xdaq-application:lid=" << eventProperties.getProperty("urn:xdaq-application:id");
				eventProperties.setProperty("urn:tracer-event:notifier", url.str());
			}
		}

		LOG4CPLUS_DEBUG(this->getApplicationLogger(), this->toString(eventProperties));

		if (applyJEL(eventProperties) || e.type() == "urn:xdaq-event:injectTestEvent")
		{
			if (events_->elements() > maxPending_)
			{
				events_->pop_front();
				outgoingTraceLostCounter_ = outgoingTraceLostCounter_ + 1;
				LOG4CPLUS_WARN(this->getApplicationLogger(), "Event pending queue full, oldest event discarded");
			}
			events_->push_back(eventProperties);
		}
		else
		{
			LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Did not pass filter - event lost");
			outgoingTraceLostCounter_ = outgoingTraceLostCounter_ + 1;
			repositoryLock_.give();
			return;
		}

		repositoryLock_.give();
	}
}

void tracer::probe::Application::processPendingEvents ()
{
	repositoryLock_.take();
	while (!events_->empty())
	{
		try
		{
			xdata::Properties item = events_->front();
			this->publishEvent(item);

			events_->pop_front();
		}
		catch (tracer::probe::exception::Exception & e)
		{
			LOG4CPLUS_FATAL(this->getApplicationLogger(), "Send failure :" + e.message() + " - event lost");
			repositoryLock_.give();
			return;
		}
	}
	repositoryLock_.give();
}

void tracer::probe::Application::publishEvent (xdata::Properties& plist) throw (tracer::probe::exception::Exception)
{
	//plist.setProperty("urn:b2in-protocol:service", "tracerd");
	plist.setProperty("urn:b2in-eventing:topic", "tracer");
	plist.setProperty("urn:b2in-eventing:action", "notify");
	plist.setProperty("urn:b2in-protocol:action", "trace");

	//send
	char buffer[(xdata::UnsignedIntegerT) IPCMessageSize_];
	xdata::exdr::Serializer serializer;
	xdata::exdr::FixedSizeOutputStreamBuffer outBuffer(buffer, IPCMessageSize_);

	try
	{
		serializer.exportAll(&plist, &outBuffer);
	}
	catch (xdata::exception::Exception & e)
	{
		XCEPT_RETHROW(tracer::probe::exception::Exception, "Could not serialize event", e);
	}

	errno = 0;
	int ret = mq_send(daemonQueue_, buffer, outBuffer.tellp(), 1);

	if (ret == -1)
	{
		std::stringstream ss;
		ss << "Failed to send ipc message to daemon : " << strerror(errno);
		XCEPT_RAISE(tracer::probe::exception::Exception, ss.str());
	}
}

void tracer::probe::Application::actionPerformed (xdata::Event& e)
{
	if (e.type() == "urn:xdaq-event:setDefaultValues")
	{
		// load JEL
		if (jelFileName_.toString() != "") // for backward compatibility if paramater not specified
		{
			this->loadJEL(jelFileName_.toString());
		}

		errno = 0;
		std::stringstream qname;
		qname << "/tracerd_queue_" << this->getApplicationContext()->getDefaultZoneName();
		//std::cout << "Opening queue : " << qname.str() << std::endl;
		daemonQueue_ = mq_open(qname.str().c_str(), O_WRONLY);

		if (daemonQueue_ == (mqd_t) - 1)
		{
			std::stringstream ss;
			ss << "Failed to open ipc message queue to talk to daemon : " << strerror(errno);
			XCEPT_RAISE(xdaq::exception::Exception, ss.str());
		}

		// check that our configured size is <= allowed message size
		// if this check passes, all messages should be small enough to fit
		struct mq_attr attrs;
		mq_getattr(daemonQueue_, &attrs);

		if ((xdata::UnsignedIntegerT) IPCMessageSize_ > attrs.mq_msgsize)
		{
			std::stringstream ss;
			ss << "Bad configuration, an IPCMessageSize of '" << IPCMessageSize_ << "' is larger than the allowed size of '" << attrs.mq_msgsize;
			LOG4CPLUS_ERROR(this->getApplicationLogger(), ss.str());
		}

		getApplicationContext()->addActionListener(this); // attach to available events

		if (!toolbox::task::getTimerFactory()->hasTimer("tracer-processpending"))
		{
			(void) toolbox::task::getTimerFactory()->createTimer("tracer-processpending");
		}
		toolbox::task::Timer * timer_processpending = toolbox::task::getTimerFactory()->getTimer("tracer-processpending");

		// submit tasks
		toolbox::TimeInterval interval_processpending(0, 100000);
		try
		{
			toolbox::TimeVal start;
			start = toolbox::TimeVal::gettimeofday();

			timer_processpending->scheduleAtFixedRate(start, this, interval_processpending, 0, "tracer-processpending");
		}
		catch (toolbox::task::exception::InvalidListener& e)
		{
			// Failed to submit to timer
			LOG4CPLUS_FATAL(this->getApplicationLogger(), xcept::stdformat_exception_history(e));
		}
		catch (toolbox::task::exception::InvalidSubmission& e)
		{
			// Failed to submit to timer
			LOG4CPLUS_FATAL(this->getApplicationLogger(), xcept::stdformat_exception_history(e));
		}
		catch (toolbox::task::exception::NotActive& e)
		{
			// Failed to submit to timer
			LOG4CPLUS_FATAL(this->getApplicationLogger(), xcept::stdformat_exception_history(e));
		}
		catch (toolbox::exception::Exception& e)
		{
			// Failed to parse the watchdog time string
			LOG4CPLUS_FATAL(this->getApplicationLogger(), xcept::stdformat_exception_history(e));
		}
	}
	else
	{
		std::stringstream msg;
		msg << "Failed to process unknown event type '" << e.type() << "'";
		LOG4CPLUS_FATAL(this->getApplicationLogger(), msg.str());
	}
}

// Hyperdaq

void tracer::probe::Application::Default (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	*out << "<div class=\"xdaq-tab-wrapper\" id=\"tabPane1\">" << std::endl;

	// Tabbed pages

	*out << "<div class=\"xdaq-tab\" title=\"Events\" id=\"tabPage2\">" << std::endl;
	this->EventsTabPage(out);
	*out << "</div>";

	*out << "</div>";
}

void tracer::probe::Application::EventsTabPage (xgi::Output * out)
{
	*out << cgicc::table().set("class", "xdaq-table-vertical") << std::endl;
	*out << cgicc::tbody() << std::endl;

	// Total Events
	*out << cgicc::tr();
	*out << cgicc::th();
	*out << "Incoming Events";
	*out << cgicc::th();
	*out << cgicc::td();
	*out << incomingTraceCounter_;
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	// Published Events
	*out << cgicc::tr();
	*out << cgicc::th();
	*out << "Send Events";
	*out << cgicc::th();
	*out << cgicc::td();
	*out << outgoingTraceCounter_;
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	// Lost Events
	*out << cgicc::tr();
	*out << cgicc::th();
	*out << "Lost Events";
	*out << cgicc::th();
	*out << cgicc::td();
	*out << outgoingTraceLostCounter_;
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	*out << cgicc::tbody() << std::endl;
	*out << cgicc::table() << std::endl;
	/*

	std::stringstream to;
	to << this->getApplicationDescriptor()->getContextDescriptor()->getURL() << "/" << this->getApplicationDescriptor()->getURN() << "/inject";

	// inject button
	*out << "<br />" << std::endl;
	*out << "<form>" << std::endl;
	*out << "	<input type=\"button\" onClick=\"parent.location='" << to.str() << "'\" value=\"Inject test event\">" << std::endl;
	*out << "</form>" << std::endl;
	*out */
}

// Test function!!!
void tracer::probe::Application::inject (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	xdaq::Event e("urn:xdaq-event:injectTestEvent", this);
	dynamic_cast<xdaq::ApplicationContextImpl*>(this->getApplicationContext())->fireEvent(e);
}

//////////
// JEL FUNCTIONS
//////////

void tracer::probe::Application::loadJEL (const std::string & fname)
{
	std::vector < std::string > files;
	try
	{
		files = toolbox::getRuntime()->expandPathName(fname);
	}
	catch (toolbox::exception::Exception& e)
	{
		std::stringstream msg;
		msg << "Failed to expand filter filename from '" << fname << "', ";
		XCEPT_DECLARE_NESTED(tracer::probe::exception::Exception, q, msg.str(), e);
		this->notifyQualified("fatal", q);
		return;
	}

	try
	{
		LOG4CPLUS_INFO(this->getApplicationLogger(), "Load JEL from '" << files[0] << "'");
		DOMDocument* doc = xoap::getDOMParserFactory()->get("configure")->loadXML(files[0]);
		DOMNodeList* list = doc->getElementsByTagNameNS(xoap::XStr("http://xdaq.web.cern.ch/xdaq/xsd/2013/tracer"), xoap::XStr("rule"));

		for (XMLSize_t j = 0; j < list->getLength(); j++)
		{
			//std::cout << "set rule:" << std::endl;
			DOMNode* node = list->item(j);
			DOMNamedNodeMap* attributes = node->getAttributes();
			xdata::Properties p;
			for (unsigned int i = 0; i < attributes->getLength(); i++)
			{
				std::string prefixStr = xoap::XMLCh2String(attributes->item(i)->getPrefix());

				if (prefixStr != "xmlns")
				{
					std::string valueStr = xoap::XMLCh2String(attributes->item(i)->getNodeValue());
					std::string nameStr = xoap::XMLCh2String(attributes->item(i)->getLocalName());
					std::string nsStr = xoap::XMLCh2String(attributes->item(i)->getNamespaceURI());

					std::stringstream ss;
					ss << "RULE : valueStr='" << valueStr << "' nameStr='" << nameStr << "' nsStr='" << nsStr << "'";
					LOG4CPLUS_DEBUG(this->getApplicationLogger(), ss.str());

					if (nsStr != "") nsStr += ":";
					p.setProperty(nsStr + nameStr, valueStr);
				}
			}
			jel_.push_back(p);
		}
		doc->release();
		LOG4CPLUS_INFO(this->getApplicationLogger(), "Loaded JEL from '" << files[0] << "'");
	}
	catch (xoap::exception::Exception& e)
	{
		std::stringstream msg;
		msg << "Failed to load configuration file from '" << files[0] << "'";
		LOG4CPLUS_FATAL(this->getApplicationLogger(), xcept::stdformat_exception_history(e));
	}

}

bool tracer::probe::Application::applyJEL (xdata::Properties & pList)
{
	//LOG4CPLUS_DEBUG(this->getApplicationLogger(), "check JEL rules");

	for (std::list<xdata::Properties>::iterator i = jel_.begin(); i != jel_.end(); ++i)
	{
		if (this->match(pList, (*i)))
		{
			return true;
		}
	}

	return false;
}

// Return true IFF all filters are found within the pList
bool tracer::probe::Application::match (xdata::Properties & pList, std::map<std::string, std::string>& filter)
{
	// Algorithm requires that all filter expressions match!

	std::stringstream ss;

	std::map<std::string, std::string>::iterator i;
	for (i = filter.begin(); i != filter.end(); ++i)
	{
		try
		{
			std::string name = (*i).first;

			ss << "Match jel rule : '" << name << "' value '" << pList.getProperty(name) << "' : '" << (*i).second << "' = ";
			//LOG4CPLUS_DEBUG(this->getApplicationLogger(), ss.str());

			if (!toolbox::regx_match_nocase(pList.getProperty(name), (*i).second))
			{
				ss << "false";
				LOG4CPLUS_DEBUG(this->getApplicationLogger(), ss.str());
				return false;
			}
		}
		catch (xdata::exception::Exception& ex)
		{
			// doesn't match the column name

			ss << "false";
			LOG4CPLUS_DEBUG(this->getApplicationLogger(), ss.str());
			return false;
		}
	}

	ss << "true";
	LOG4CPLUS_DEBUG(this->getApplicationLogger(), ss.str());
	return true;
}

std::string tracer::probe::Application::toString (xdata::Properties& propList)
{
	std::stringstream ss;

	ss << "Property List : ";

	for (std::map<std::string, std::string, std::less<std::string> >::iterator i = propList.begin(); i != propList.end(); ++i)
	{
		ss << ", " << (*i).first << "=['" << (*i).second << "']";
	}
	return ss.str();
}
