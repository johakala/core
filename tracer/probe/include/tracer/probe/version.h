/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: R. Moser and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _tracer_probe_version_h_
#define _tracer_probe_version_h_

#include "config/PackageInfo.h"
// !!! Edit this line to reflect the latest package version !!!

#define TRACERPROBE_VERSION_MAJOR 2
#define TRACERPROBE_VERSION_MINOR 1
#define TRACERPROBE_VERSION_PATCH 0
// If any previous versions available E.g. #define TRACERPROBE_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#undef TRACERPROBE_PREVIOUS_VERSIONS 

//
// Template macros
//
#define TRACERPROBE_VERSION_CODE PACKAGE_VERSION_CODE(TRACERPROBE_VERSION_MAJOR,TRACERPROBE_VERSION_MINOR,TRACERPROBE_VERSION_PATCH)
#ifndef TRACERPROBE_PREVIOUS_VERSIONS
#define TRACERPROBE_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(TRACERPROBE_VERSION_MAJOR,TRACERPROBE_VERSION_MINOR,TRACERPROBE_VERSION_PATCH)
#else 
#define TRACERPROBE_FULL_VERSION_LIST  TRACERPROBE_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(TRACERPROBE_VERSION_MAJOR,TRACERPROBE_VERSION_MINOR,TRACERPROBE_VERSION_PATCH)
#endif 

namespace tracerprobe
{
	const std::string package = "tracerprobe";
	const std::string versions = TRACERPROBE_FULL_VERSION_LIST;
	const std::string summary = "Publish XDAQ events";
	const std::string description = "Publish XDAQ events";
	const std::string authors = "Andy Forrest, Luciano Orsini";
	const std::string link = "http://xdaq.web.cern.ch/";
	config::PackageInfo getPackageInfo ();
	void checkPackageDependencies () throw (config::PackageInfo::VersionException);
	std::set<std::string, std::less<std::string> > getPackageDependencies ();
}

#endif

