// $Id: $

/*************************************************************************
 * XDAQ Tracer Probe                      								 *
 * Copyright (C) 2000-2013, CERN.			                  			 *
 * All rights reserved.                                                  *
 * Authors: A. Forrest and L. Orsini				 					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                       		 *
 * For the list of contributors see CREDITS.   			         		 *
 *************************************************************************/

#ifndef _tracer_probe_Application_h_
#define _tracer_probe_Application_h_

#include "xdaq/Application.h"
#include "xgi/framework/UIManager.h"

#include "toolbox/ActionListener.h"
#include "xdata/ActionListener.h"
#include "toolbox/task/TimerListener.h"

#include "xgi/Method.h"
#include "xgi/Output.h"
#include "xgi/exception/Exception.h"

#include "b2in/utils/EventingDialup.h"
#include "b2in/nub/exception/Exception.h"

#include "xdata/Double.h"
#include "xdata/String.h"
#include "xdata/UnsignedInteger32.h"

#include "tracer/probe/exception/Exception.h"

#include <fcntl.h>
#include <sys/stat.h>
#include <mqueue.h>

namespace tracer
{
	namespace probe
	{
		class Application : public xdaq::Application, public xgi::framework::UIManager, public toolbox::ActionListener, public xdata::ActionListener, public toolbox::task::TimerListener
		{
			public:

				XDAQ_INSTANTIATOR();

				Application (xdaq::ApplicationStub* s) throw (xdaq::exception::Exception);
				~Application ();

				void actionPerformed (toolbox::Event& event);
				void actionPerformed (xdata::Event& e);

				void timeExpired (toolbox::task::TimerEvent& e);

				void Default (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception);
				void inject (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception);

				void EventsTabPage (xgi::Output * out);
				void StatisticsTabPage (xgi::Output * out);

				void publishEvent (xdata::Properties& pList) throw (tracer::probe::exception::Exception);

				std::string toString (xdata::Properties& propList);

				void processPendingEvents ();

			protected:

				bool applyJEL (xdata::Properties & pList);
				void loadJEL (const std::string & fname);
				bool match (xdata::Properties & pList, std::map<std::string, std::string>& filter);

				// Exported variables
				xdata::UnsignedInteger32 maxEventMessageSize_;

				toolbox::BSem repositoryLock_;

				toolbox::task::ActionSignature* process_;

				//////////

				xdata::String jelFileName_;
				std::list<xdata::Properties> jel_;
				std::map<std::string, std::map<std::string, size_t> > jec_;

				toolbox::rlist<xdata::Properties> * events_;

				xdata::UnsignedInteger64 outgoingTraceCounter_;
				xdata::UnsignedInteger64 incomingTraceCounter_;
				xdata::UnsignedInteger64 outgoingTraceLostCounter_;

				xdata::UnsignedInteger IPCMessageSize_;

				xdata::UnsignedInteger32 maxPending_;

				mqd_t daemonQueue_;
		};
	}
}

#endif

