// $Id: Application.h,v 1.12 2008/11/25 13:21:03 lorsini Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _timeline_Application_h_
#define _timeline_Application_h_

#include "xdaq/Application.h"
#include "xgi/framework/UIManager.h"

#include "toolbox/ActionListener.h"
#include "xdata/ActionListener.h"
#include "toolbox/task/TimerListener.h"

#include "toolbox/task/AsynchronousEventDispatcher.h"

#include "xgi/Method.h"
#include "xgi/Output.h"
#include "xgi/exception/Exception.h"

#include "tracer/timeline/exception/Exception.h"

#include "xdata/String.h"
#include "xdata/UnsignedInteger64.h"

#include "b2in/nub/Method.h"
#include "b2in/utils/MessengerCache.h"
#include "b2in/utils/ServiceProxy.h"

namespace tracer
{
	namespace timeline
	{
		class Application : public xdaq::Application, public xgi::framework::UIManager, public toolbox::ActionListener, public xdata::ActionListener, public toolbox::task::TimerListener, public b2in::utils::MessengerCacheListener
		{
			public:

				XDAQ_INSTANTIATOR();

				Application (xdaq::ApplicationStub* s) throw (xdaq::exception::Exception);
				~Application ();

				void actionPerformed (xdata::Event& e);
				void actionPerformed (toolbox::Event& event);

				void timeExpired (toolbox::task::TimerEvent& e);

				void Default (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception);

				void onMessage (toolbox::mem::Reference * msg, xdata::Properties & plist) throw (b2in::nub::exception::Exception);

				void asynchronousExceptionNotification (xcept::Exception&);

				void runBenchmark (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception);
				void resetBenchmark (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception);
				void refreshBenchmark (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception);

			protected:

				void refreshSubscriptionsToEventing () throw (timeline::exception::Exception);

				void consoleTabPage (xgi::Output * out) throw (xgi::exception::Exception);
				void StatisticsTabPage (xgi::Output * out) throw (xgi::exception::Exception);
				void benchmarkTabPage (xgi::Output * out) throw (xgi::exception::Exception);

				void getConsoleData (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception);

			private:

				toolbox::task::AsynchronousEventDispatcher dispatcher_;
				xdata::UnsignedInteger64T eventsLostCounter_;
				xdata::String topic_;

				xdata::String subscribeGroup_; // one or more comma separated groups hosting a ws-eventing service for events

				/*
				 xdata::String maintenanceInterval_; // interval at which the database switchover happens (> 1 minute)
				 xdata::String repositoryPath_; // folder in which to store the spotlight database files (/var/run/spotlight)

				 xdata::String exceptionsTimeWindow_;
				 xdata::String archiveWindow_; // determine the name of the file for storing archived exceptions (default 1H )
				 */

				xdata::String jelFileName_;
				std::list<xdata::Properties> jel_;
				std::map<std::string, std::map<std::string, size_t> > jec_;

				b2in::utils::ServiceProxy* b2inEventingProxy_;
				xdata::String scanPeriod_;
				xdata::String subscribeExpiration_;

				std::map<std::string, xdata::Properties> subscriptions_; // indexed by topic

				xdata::UnsignedInteger eventsIn_;

				toolbox::rlist<xdata::Properties> * consoleData_;

				// BENCHMARKING

				size_t assertCount_;
				std::string assertType_;
				toolbox::TimeVal startTime_;
				toolbox::TimeVal endTime_;

		};
	}
}

#endif
