// $Id: version.h,v 1.13 2009/04/27 13:26:05 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _tracer_timeline_version_h_
#define _tracer_timeline_version_h_

#include "config/PackageInfo.h"

// !!! Edit this line to reflect the latest package version !!!

#define TIMELINE_VERSION_MAJOR 2
#define TIMELINE_VERSION_MINOR 1
#define TIMELINE_VERSION_PATCH 2
// If any previous versions available E.g. #define TIMELINE_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#undef TIMELINE_PREVIOUS_VERSIONS
#define TIMELINE_PREVIOUS_VERSIONS "2.1.0,2.1.1"

//
// Template macros
//
#define TIMELINE_VERSION_CODE PACKAGE_VERSION_CODE(TIMELINE_VERSION_MAJOR,TIMELINE_VERSION_MINOR,TIMELINE_VERSION_PATCH)
#ifndef TIMELINE_PREVIOUS_VERSIONS
#define TIMELINE_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(TIMELINE_VERSION_MAJOR,TIMELINE_VERSION_MINOR,TIMELINE_VERSION_PATCH)
#else 
#define TIMELINE_FULL_VERSION_LIST  TIMELINE_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(TIMELINE_VERSION_MAJOR,TIMELINE_VERSION_MINOR,TIMELINE_VERSION_PATCH)
#endif 

namespace timeline
{
	const std::string package = "timeline";
	const std::string versions = TIMELINE_FULL_VERSION_LIST;
	const std::string summary = "Server for events";
	const std::string description = "";
	const std::string authors = "Andy Forrest, Luciano Orsini";
	const std::string link = "http://xdaqwiki.cern.ch/index.php/Power_Pack";
	config::PackageInfo getPackageInfo ();
	void checkPackageDependencies () throw (config::PackageInfo::VersionException);
	std::set<std::string, std::less<std::string> > getPackageDependencies ();
}

#endif
