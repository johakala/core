// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                  *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "tracer/timeline/Application.h"

#include "toolbox/task/WorkLoopFactory.h"
#include "toolbox/task/Timer.h"
#include "toolbox/task/TimerFactory.h"

#include "xdaq/Event.h"

#include "xcept/tools.h"

#include "xgi/framework/Method.h"
#include "xgi/Utils.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTMLClasses.h"

XDAQ_INSTANTIATOR_IMPL (tracer::timeline::Application)

tracer::timeline::Application::Application (xdaq::ApplicationStub* s) throw (xdaq::exception::Exception)
	: xdaq::Application(s), xgi::framework::UIManager(this), dispatcher_("urn:xdaq-workloop:timeline", "waiting", 0.8)
{ 
	consoleData_ = toolbox::rlist<xdata::Properties>::create("tracer-console-queue");

	s->getDescriptor()->setAttribute("icon", "/tracer/timeline/images/timeline-icon.png");
	s->getDescriptor()->setAttribute("icon16x16", "/tracer/timeline/images/timeline-icon.png");
	s->getDescriptor()->setAttribute("service", "timeline");

	subscribeGroup_ = "";
	this->getApplicationInfoSpace()->fireItemAvailable("subscribeGroup", &subscribeGroup_);

	jelFileName_ = "";

	this->getApplicationInfoSpace()->fireItemAvailable("jelFileName", &jelFileName_);

	// Activates work loop for dashboard asynchronous operations (SOAP messages)
	dispatcher_.addActionListener(this);
	(void) toolbox::task::getWorkLoopFactory()->getWorkLoop("urn:xdaq-workloop:timeline", "waiting")->activate();

	// Which flashlist tags to retrieve from the ws-eventing
	topic_ = "tracer";
	this->getApplicationInfoSpace()->fireItemAvailable("topic", &topic_);

	scanPeriod_ = "PT10S";
	this->getApplicationInfoSpace()->fireItemAvailable("scanPeriod", &scanPeriod_);
	subscribeExpiration_ = "PT30S";
	this->getApplicationInfoSpace()->fireItemAvailable("subscribeExpiration", &subscribeExpiration_);

	b2in::nub::bind(this, &tracer::timeline::Application::onMessage);

	xgi::framework::deferredbind(this, this, &tracer::timeline::Application::Default, "Default");
	xgi::bind(this, &tracer::timeline::Application::getConsoleData, "getConsoleData");

	xgi::bind(this, &tracer::timeline::Application::runBenchmark, "runBenchmark");
	xgi::bind(this, &tracer::timeline::Application::resetBenchmark, "resetBenchmark");
	xgi::bind(this, &tracer::timeline::Application::refreshBenchmark, "refreshBenchmark");

	getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");

	eventsLostCounter_ = 0;

	std::stringstream callback;
    callback << this->getApplicationDescriptor()->getContextDescriptor()->getURL() << "/" << this->getApplicationDescriptor()->getURN() << "/getConsoleData";

	assertCount_ = 0;
	assertType_ = "";
}

tracer::timeline::Application::~Application ()
{
}

//
// Infoapace listener
//
void tracer::timeline::Application::actionPerformed (xdata::Event& event)
{
	if (event.type() == "urn:xdaq-event:setDefaultValues")
	{

		b2inEventingProxy_ = new b2in::utils::ServiceProxy(this, "b2in-eventing", subscribeGroup_.toString(), this);

		try
		{
			if (!toolbox::task::getTimerFactory()->hasTimer("timeline"))
			{
				(void) toolbox::task::getTimerFactory()->createTimer("timeline");
			}
			toolbox::task::Timer * timer = toolbox::task::getTimerFactory()->getTimer("timeline");
			toolbox::TimeVal start;
			start = toolbox::TimeVal::gettimeofday();

			LOG4CPLUS_INFO(this->getApplicationLogger(), "Schedule immediate archivation at startup");
			timer->schedule(this, start, 0, "timeline-archive-staging-now");

			// submit scan task
			toolbox::TimeInterval interval2;
			interval2.fromString(scanPeriod_);
			LOG4CPLUS_INFO(this->getApplicationLogger(), "Schedule discovery timer for " << interval2.toString());
			timer->scheduleAtFixedRate(start, this, interval2, 0, "timeline-discovery-staging");

		}
		catch (toolbox::task::exception::InvalidListener& e)
		{
			LOG4CPLUS_FATAL(this->getApplicationLogger(), xcept::stdformat_exception_history(e));
		}
		catch (toolbox::task::exception::InvalidSubmission& e)
		{
			LOG4CPLUS_FATAL(this->getApplicationLogger(), xcept::stdformat_exception_history(e));
		}
		catch (toolbox::task::exception::NotActive& e)
		{
			LOG4CPLUS_FATAL(this->getApplicationLogger(), xcept::stdformat_exception_history(e));
		}
		catch (toolbox::exception::Exception& e)
		{
			LOG4CPLUS_FATAL(this->getApplicationLogger(), xcept::stdformat_exception_history(e));
		}

		if (jelFileName_.toString() != "") // for backward compatibility if paramater not specified
		{
			//this->loadJEL(jelFileName_.toString());
		}
	}
	else
	{
		LOG4CPLUS_ERROR(this->getApplicationLogger(), "Received unsupported event type '" << event.type() << "'");
	}
}

void tracer::timeline::Application::actionPerformed (toolbox::Event& event)
{
	// Properties of esource event are;

	/*
	urn:timeline-event:tracer
	urn:b2in-eventing:action=['notify']
	urn:b2in-eventing:id=['14ef2054-1497-4e0d-a238-671c708f23b7']
	urn:b2in-eventing:topic=['tracer']
	urn:b2in-protocol-tcp:connection=['keep-alive']
	urn:b2in-protocol-tcp:destination=['utcp://srv-c2d06-17.cms:0']
	urn:b2in-protocol-tcp:originator=['utcp://10.176.37.220:1977']
	urn:b2in-protocol:originator=['http://srv-c2d06-17.cms:4002']
	urn:b2in-protocol:service=['timeline']
	urn:esource-afsm-event:stateChange=['End']
	urn:tracer-event:notifier=['http://srv-c2d06-17.cms:30405/urn:xdaq-application:lid=42']
	urn:tracer-event:timestamp=['2013-08-05T07:57:46.895041Z']
	urn:tracer-event:type=['urn:xdaq-event:esourceAFSMEvent']
	urn:xdaq-context:session=['']
	*/

	if (event.type() == "urn:timeline-event:tracer")
	{
		xdata::Properties props;
		std::stringstream ss;
		//ss << event.type();

		xdaq::Event & xdaqEvent = dynamic_cast<xdaq::Event&>(event);
		xdata::Properties & refP = dynamic_cast<xdata::Properties&>(xdaqEvent);
		xdata::Properties eventProperties = refP;

		if (eventProperties.getProperty("urn:xdaq-context:session") != "")
		{
			props.setProperty("id", eventProperties.getProperty("urn:xdaq-context:session") + " : " + eventProperties.getProperty("urn:tracer-event:timestamp"));
		}
		else
		{
			props.setProperty("id", eventProperties.getProperty("urn:tracer-event:timestamp"));
		}


		props.setProperty("src", eventProperties.getProperty("urn:tracer-event:notifier"));

		ss << eventProperties.getProperty("urn:tracer-event:type");

		for (std::map<std::string, std::string, std::less<std::string> >::iterator i = eventProperties.begin(); i != eventProperties.end(); ++i)
		{
			// atm, output all events, removing the b2in properties
			std::string f = (*i).first;

			/*
			if (f == "urn:b2in-eventing:action") continue;
			if (f == "urn:b2in-eventing:id") continue;
			if (f == "urn:b2in-eventing:topic") continue;
			if (f == "urn:b2in-protocol-tcp:connection") continue;
			if (f == "urn:b2in-protocol-tcp:destination") continue;
			if (f == "urn:b2in-protocol-tcp:originator") continue;
			if (f == "urn:b2in-protocol:originator") continue;
			if (f == "urn:b2in-protocol:service") continue;

			if (f == "urn:tracer-event:type") continue;
			if (f == "urn:tracer-event:timestamp") continue;
			if (f == "urn:tracer-event:notifier") continue;
			if (f == "urn:xdaq-context:session") continue;
			*/

			ss << ", " << (*i).first << "=['" << (*i).second << "']";
		}

		//LOG4CPLUS_INFO(this->getApplicationLogger(), ss.str());

		if (assertCount_ > 0)
		{
			if (eventProperties.getProperty("urn:tracer-event:type").find(assertType_) != std::string::npos)
			{
				assertCount_--;
				if (assertCount_ == 0)
				{
					endTime_ = toolbox::TimeVal::gettimeofday();
					double delta = (double)endTime_ - (double) startTime_;
					toolbox::TimeInterval t (delta);

					std::stringstream bs;
					bs << "Benchmark complete" << std::endl;
					bs << "Start Time : " << startTime_.toString(startTime_.tz()) << std::endl;
					bs << "End Time   : " << endTime_.toString(endTime_.tz()) << std::endl;
					bs << "Test Time : " << t.toString() << ":" << t.millisec() << std::endl;

					LOG4CPLUS_INFO(this->getApplicationLogger(), bs.str());
				}
			}
		}

		props.setProperty("msg", ss.str());
		consoleData_->push_back(props);

		eventsIn_++;
	}
	else
	{
		std::stringstream ss;
		ss << "Unknown event : " << event.type() << std::endl;
		LOG4CPLUS_WARN(this->getApplicationLogger(), ss.str());
	}
}

void tracer::timeline::Application::onMessage (toolbox::mem::Reference * msg, xdata::Properties & pList) throw (b2in::nub::exception::Exception)
{
	// Currently, tracer sends an empty mem ref with a plist, with the following properties

	// plist.setProperty("urn:b2in-protocol:service", "b2in-eventing"); 	//ignore once found
	// plist.setProperty("urn:b2in-eventing:topic", "tracer"); 				//ignore once found
	// plist.setProperty("urn:b2in-eventing:action", "notify"); 			//ignore once found
	// plist.setProperty("urn:tracer-event:name", e.type());

	std::stringstream ss;
	ss << "tracer::timeline::Application::onMessage" << std::endl;

	if ( msg != 0 )
	{
		msg->release();
	}

	std::string action = pList.getProperty("urn:b2in-eventing:action");
	if (action != "notify")
	{
		ss << ": action == '" << action << "', returning...";
		std::cout << ss.str() << std::endl;
		return;
	}

	toolbox::Event * event;

	event = new xdaq::Event("urn:timeline-event:tracer", 0);

	// add in pList
	dynamic_cast<xdata::Properties&>(*event) = pList;

	// process exception in a separate thread, to release the HTTP emebedded server

	toolbox::task::EventReference e(event);
	try
	{
		dispatcher_.fireEvent(e);
	}
	catch (toolbox::task::exception::Overflow & e)
	{
		eventsLostCounter_++;
		std::stringstream msg;
		msg << "Events lost: " << eventsLostCounter_ << ", " << e.what();
		LOG4CPLUS_FATAL(this->getApplicationLogger(), msg.str());

	}
	catch (toolbox::task::exception::OverThreshold & e)
	{
		// message is lost, keep counting
		eventsLostCounter_++;
		ss << ": event lost";
		std::cout << ss.str() << std::endl;
	}
	catch (toolbox::task::exception::InternalError & e)
	{
		// dead band ignore
		eventsLostCounter_++;
		std::stringstream msg;
		msg << "Events lost: " << eventsLostCounter_ << ", " << e.what();
		LOG4CPLUS_FATAL(this->getApplicationLogger(), msg.str());
	}

}

void tracer::timeline::Application::refreshSubscriptionsToEventing () throw (tracer::timeline::exception::Exception)
{
	if (subscriptions_.empty())
	{
		LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Prepare subscription records");
		const xdaq::Network * network = 0;
		std::string networkName = this->getApplicationDescriptor()->getAttribute("network");
		try
		{
			network = this->getApplicationContext()->getNetGroup()->getNetwork(networkName);
		}
		catch (xdaq::exception::NoNetwork& nn)
		{
			std::stringstream msg;
			msg << "Failed to access b2in network " << networkName << ", not configured";
			XCEPT_RETHROW(tracer::timeline::exception::Exception, msg.str(), nn);
		}
		pt::Address::Reference localAddress = network->getAddress(this->getApplicationContext()->getContextDescriptor());

		std::set < std::string > topics = toolbox::parseTokenSet(topic_.toString(), ",");
		std::set<std::string>::iterator i;
		for (i = topics.begin(); i != topics.end(); ++i)
		{
			if (subscriptions_.find(*i) == subscriptions_.end())
			{
				xdata::Properties plist;
				toolbox::net::UUID identifier;
				plist.setProperty("urn:b2in-protocol:service", "b2in-eventing"); // for remote dispatching

				plist.setProperty("urn:b2in-eventing:action", "subscribe");
				plist.setProperty("urn:b2in-eventing:id", identifier.toString());
				plist.setProperty("urn:b2in-eventing:topic", (*i)); // Subscribe to collected data
				plist.setProperty("urn:b2in-eventing:subscriberurl", localAddress->toString());
				plist.setProperty("urn:b2in-eventing:subscriberservice", "timeline");
				plist.setProperty("urn:b2in-eventing:expires", subscribeExpiration_.toString("xs:duration"));
				subscriptions_[(*i)] = plist;
			}
		}
	}

	LOG4CPLUS_DEBUG(this->getApplicationLogger(), "subscribe/Renew");
	b2in::utils::MessengerCache * messengerCache = 0;
	try
	{
		messengerCache = b2inEventingProxy_->getMessengerCache();
	}
	catch (b2in::utils::exception::Exception & e)
	{
		XCEPT_RETHROW(tracer::timeline::exception::Exception, "cannot access messenger cache", e);
	}

	// This lock is available all the time excepted when an asynchronous send will fail and a cache invalidate is performed
	// Therefore there is no loss of efficiency.

	std::list < std::string > destinations = messengerCache->getDestinations();

	for (std::list<std::string>::iterator j = destinations.begin(); j != destinations.end(); j++)
	{
		//
		// Subscribe to OR Renew all existing subscriptions
		//
		std::map<std::string, xdata::Properties>::iterator i;
		for (i = subscriptions_.begin(); i != subscriptions_.end(); ++i)
		{
			try
			{
				// plist is already prepared for a subscribe/resubscribe message
				//
				LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Subscribe to topic " << (*i).first << " on url " << (*j));
				messengerCache->send((*j), 0, (*i).second);
			}
			catch (b2in::nub::exception::InternalError & e)
			{
				std::stringstream msg;
				msg << "failed to send subscription for topic (internal error) " << (*i).first << " to url " << (*j);

				LOG4CPLUS_ERROR(this->getApplicationLogger(), msg.str());

				XCEPT_DECLARE_NESTED(tracer::timeline::exception::Exception, ex, msg.str(), e);
				this->notifyQualified("fatal", ex);
				return;
			}
			catch (b2in::nub::exception::QueueFull & e)
			{
				std::stringstream msg;
				msg << "failed to send subscription for topic (queue full) " << (*i).first << " to url " << (*j);

				LOG4CPLUS_ERROR(this->getApplicationLogger(), msg.str());

				XCEPT_DECLARE_NESTED(tracer::timeline::exception::Exception, ex, msg.str(), e);
				this->notifyQualified("error", ex);
				return;
			}
			catch (b2in::nub::exception::OverThreshold & e)
			{
				// ignore just count to avoid verbosity
				std::stringstream msg;
				msg << "failed to send subscription for topic (over threshold) " << (*i).first << " to url " << (*j);
				LOG4CPLUS_ERROR(this->getApplicationLogger(), msg.str());
				return;
			}
		}
		LOG4CPLUS_DEBUG(this->getApplicationLogger(), "done with subscription");
	}
}

void tracer::timeline::Application::timeExpired (toolbox::task::TimerEvent& e)
{
	std::string name = e.getTimerTask()->name;

	if (name == "timeline-discovery-staging")
	{
		try
		{
			b2inEventingProxy_->scan();
		}
		catch (b2in::utils::exception::Exception& e)
		{
			LOG4CPLUS_FATAL(this->getApplicationLogger(), xcept::stdformat_exception_history(e));
		}

		try
		{
			this->refreshSubscriptionsToEventing();
		}
		catch (tracer::timeline::exception::Exception& e)
		{
			LOG4CPLUS_FATAL(this->getApplicationLogger(), xcept::stdformat_exception_history(e));
		}

	}
	else if ((name == "timeline-archive-staging-now") || (name == "timeline-archive-staging"))
	{
		/*
		 // archive event
		 // If we are in maintenance phase and the repository_ is NULL, just return
		 if (repository_)
		 {
		 try
		 {
		 LOG4CPLUS_INFO (this->getApplicationLogger(), "Time for archiving");
		 toolbox::TimeInterval window;
		 window.fromString(exceptionsTimeWindow_);
		 repository_->archive(window);
		 }
		 catch(tracer::timeline::exception::FailedToArchive & e)
		 {
		 LOG4CPLUS_FATAL (this->getApplicationLogger(), xcept::stdformat_exception_history(e));
		 }
		 } */
	}
}

void tracer::timeline::Application::resetBenchmark (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	toolbox::TimeVal emptyTime;
	startTime_ = emptyTime;
	endTime_ = emptyTime;

	assertCount_ = 0;
	assertType_ = "";
}

void tracer::timeline::Application::refreshBenchmark (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	*out << "{" << std::endl;
	*out << "\"messages\": [" << std::endl;

	*out << "{" << std::endl;

	*out << "\"asserttype\": \"" << assertType_ << "\"," << std::endl;
	*out << "\"assertcount\": \"" << assertCount_ << "\"," << std::endl;
	*out << "\"starttime\": \"" << startTime_.toString(startTime_.tz()) << "\"," << std::endl;
	*out << "\"endtime\": \"" << endTime_.toString(startTime_.tz()) << "\"," << std::endl;

	double delta = (double)endTime_ - (double) startTime_;
	toolbox::TimeInterval t (delta);
	*out << "\"delta\": \"" << t.toString() << ":" << t.millisec() << "\"" << std::endl;

	*out << "}" << std::endl;

	*out << "]" << std::endl;
	*out << "}" << std::endl;
}

void tracer::timeline::Application::runBenchmark (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	cgicc::Cgicc cgi(in);
	// n = number of processes to start
	size_t n = 0;
	std::string type = "";
	try
	{
		cgicc::form_iterator rl = cgi.getElement("n");
		if(rl != cgi.getElements().end())
		{
			n = cgi["n"]->getIntegerValue();
		}

		rl = cgi.getElement("type");
		if(rl != cgi.getElements().end())
		{
			type = cgi["type"]->getValue();
		}
	}
	catch (const std::exception & e)
	{
		std::cout << "Failed to process runBenchmark parameters, xgi error" << std::endl;
		return;
	}

	assertCount_ = n;
	assertType_ = type;

	std::stringstream ss;
	ss << "Benchmark starting for " << n << " processes, waiting for '" << type << "'";
	LOG4CPLUS_INFO(this->getApplicationLogger(), ss.str());

	startTime_ = toolbox::TimeVal::gettimeofday();
}

void tracer::timeline::Application::Default (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	*out << "<div class=\"xdaq-tab-wrapper\" id=\"tabPane1\">" << std::endl;

	// Tabbed panels

	*out << "<div class=\"xdaq-tab\" title=\"Statistics\" id=\"tabPage2\">" << std::endl;
	this->StatisticsTabPage(out);
	*out << "</div>";

	*out << "<div class=\"xdaq-tab\" title=\"Console\" id=\"tabPage2\">" << std::endl;
	this->consoleTabPage(out);
	*out << "</div>";

	*out << "<div class=\"xdaq-tab\" title=\"Benchmark\" id=\"tabPage3\">" << std::endl;
	this->benchmarkTabPage(out);
	*out << "</div>";

	*out << "</div>"; // end of tab pane
}

void tracer::timeline::Application::StatisticsTabPage (xgi::Output * out) throw (xgi::exception::Exception)
{
	*out << cgicc::table().set("class", "xdaq-table-vertical") << std::endl;
	*out << cgicc::tbody() << std::endl;

	// Total Events
	//
	*out << cgicc::tr();
	*out << cgicc::th();
	*out << "Total Events In";
	*out << cgicc::th();
	*out << cgicc::td();
	*out << eventsIn_;
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;


	*out << cgicc::tr();
	*out << cgicc::th();
	*out << "Events Lost";
	*out << cgicc::th();
	*out << cgicc::td();
	*out << eventsLostCounter_;
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	*out << cgicc::tbody() << std::endl;
	*out << cgicc::table() << std::endl;
}

void tracer::timeline::Application::benchmarkTabPage (xgi::Output * out) throw (xgi::exception::Exception)
{
	*out << cgicc::table().set("class", "xdaq-table-vertical") << std::endl;
	*out << cgicc::tbody() << std::endl;

	*out << cgicc::tr();
	*out << cgicc::th();
	*out << "Filter Criteria";
	*out << cgicc::th();
	*out << cgicc::td();
	*out << assertType_;
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	*out << cgicc::tr();
	*out << cgicc::th();
	*out << "Remaining Count";
	*out << cgicc::th();
	*out << cgicc::td();
	*out << assertCount_;
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	*out << cgicc::tr();
	*out << cgicc::th();
	*out << "Start Time";
	*out << cgicc::th();
	*out << cgicc::td();
	*out << startTime_.toString(startTime_.tz());
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	*out << cgicc::tr();
	*out << cgicc::th();
	*out << "End Time";
	*out << cgicc::th();
	*out << cgicc::td().set("id", "endtime_value");
	*out << endTime_.toString(endTime_.tz());
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	*out << cgicc::tr();
	*out << cgicc::th();
	*out << "Result";
	*out << cgicc::th();
	*out << cgicc::td();

	double delta = (double)endTime_ - (double) startTime_;
	toolbox::TimeInterval t (delta);
	*out << t.toString() << ":" << t.millisec();

	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	*out << cgicc::tbody() << std::endl;
	*out << cgicc::table() << std::endl;

	// FORM

	*out << "<br>";
	//*out << "<input type=\"button\" value=\"Refresh\" onclick=\"refresh()\">" << std::endl;
	//*out << "<input type=\"button\" value=\"Reset\" onclick=\"reset()\">" << std::endl;
	*out << "<input type=\"button\" class=\"xdaq-button-disabled\" value=\"Refresh\">" << std::endl;
	*out << "<input type=\"button\" class=\"xdaq-button-disabled\" value=\"Reset\">" << std::endl;

	*out << "<div id=\"resultsDiv\"></div>" << std::endl;

	std::stringstream baseurl;
	baseurl << this->getApplicationDescriptor()->getContextDescriptor()->getURL() << "/" << this->getApplicationDescriptor()->getURN() << "/";

	std::string callbackURL_ = baseurl.str() + "resetBenchmark";

	*out << "<script>" << std::endl;
	*out << "		function reset()" << std::endl;
	*out << "		{" << std::endl;
	*out << "			var xmlhttp;" << std::endl;
	*out << "			if (window.XMLHttpRequest)" << std::endl;
	*out << "			{" << std::endl;
	*out << "				xmlhttp=new XMLHttpRequest();" << std::endl;
	*out << " 			}" << std::endl;

	*out << " 			xmlhttp.open(\"GET\",'" << callbackURL_ << "',true);" << std::endl;
	*out << " 			xmlhttp.send();" << std::endl;
	*out << " 			refresh();" << std::endl;
	*out << " 			document.getElementById(\"resultsDiv\").innerHTML='Reset sent';" << std::endl;

	*out << " 		}" << std::endl;
	*out << "</script>" << std::endl;

	callbackURL_ = baseurl.str() + "refreshBenchmark";

	*out << "<script>" << std::endl;
	*out << "		function refresh()" << std::endl;
	*out << "		{" << std::endl;
	*out << "			var xmlhttp;" << std::endl;
	*out << "			if (window.XMLHttpRequest)" << std::endl;
	*out << "			{" << std::endl;
	*out << "				xmlhttp=new XMLHttpRequest();" << std::endl;
	*out << " 			}" << std::endl;

	*out << " 			xmlhttp.onreadystatechange=function()" << std::endl;
	*out << " 			{" << std::endl;
	*out << " 				if (xmlhttp.readyState==4 && xmlhttp.status==200)" << std::endl;
	*out << " 				{		" << std::endl;

	*out << "					document.getElementById(\"resultsDiv\").innerHTML='Refresh sent';" << std::endl;

	*out << " 					data = JSON.parse(xmlhttp.responseText);" << std::endl;

	*out << " 					for (var i = 0; i < data.messages.length; i++) {" << std::endl;

	*out << "						document.getElementById(\"asserttype_value\").innerHTML=data.messages[i].asserttype;" << std::endl;
	*out << "						document.getElementById(\"assertcount_value\").innerHTML=data.messages[i].assertcount;" << std::endl;
	*out << "						document.getElementById(\"starttime_value\").innerHTML=data.messages[i].starttime;" << std::endl;
	*out << "						document.getElementById(\"endtime_value\").innerHTML=data.messages[i].endtime;" << std::endl;
	*out << "						document.getElementById(\"delta_value\").innerHTML=data.messages[i].delta;" << std::endl;

	*out << " 					};" << std::endl;

	*out << " 				}" << std::endl;
	*out << " 			}" << std::endl;

	*out << " 			xmlhttp.open(\"GET\",'" << callbackURL_ << "',true);" << std::endl;
	*out << " 			xmlhttp.send();" << std::endl;
	*out << " 		}" << std::endl;
	*out << "</script>" << std::endl;
}

void tracer::timeline::Application::consoleTabPage (xgi::Output * out) throw (xgi::exception::Exception)
{
	*out << "	<script type=\"text/javascript\" src=\"/tracer/timeline/html/js/xdaq-tracer-timeline-console.js\"></script>" 					<< std::endl;

	std::stringstream callback;
	callback << this->getApplicationDescriptor()->getContextDescriptor()->getURL() << "/" << this->getApplicationDescriptor()->getURN() << "/getConsoleData";

	*out << "<div id=\"xdaq-tracer-timeline-console\" class=\"xdaq-console\" data-callback=\"" << callback.str() << "\" autosize></div>" << std::endl;
}

void tracer::timeline::Application::getConsoleData (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	*out << "{" << std::endl;
	*out << "\"messages\": [" << std::endl;

	while (consoleData_->elements() > 0)
	{
		*out << "{" << std::endl;

		xdata::Properties p = consoleData_->front();
		consoleData_->pop_front();

		*out << "\"msg\": \"" << p.getProperty("msg") << "\"," << std::endl;
		*out << "\"id\": \"" << p.getProperty("id") << "\"," << std::endl;
		*out << "\"src\": \"" << p.getProperty("src") << "\"" << std::endl;

		*out << "}" << std::endl;

		if (consoleData_->elements() > 0) *out << "," << std::endl;
	}

	*out << "]" << std::endl;
	*out << "}" << std::endl;
}

void tracer::timeline::Application::asynchronousExceptionNotification (xcept::Exception&)
{
	LOG4CPLUS_INFO(this->getApplicationLogger(), "void tracer::timeline::Application::asynchronousExceptionNotification(xcept::Exception&) NOT IMPLEMENTED");
}
