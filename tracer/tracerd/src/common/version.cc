// $Id: version.cc,v 1.4 2008/07/18 15:27:27 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "tracer/tracerd/version.h"
#include "config/version.h"
#include "xcept/version.h"
#include "toolbox/version.h"
#include "pt/version.h"
#include "xoap/version.h"
#include "xdata/version.h"
#include "xdaq/version.h"

GETPACKAGEINFO (tracerd)

void tracerd::checkPackageDependencies () throw (config::PackageInfo::VersionException)
{
	CHECKDEPENDENCY (config);
	CHECKDEPENDENCY (xcept);
	CHECKDEPENDENCY (toolbox);
	CHECKDEPENDENCY (pt);
	CHECKDEPENDENCY (xdata);
	CHECKDEPENDENCY (xoap);
	CHECKDEPENDENCY (xdaq);
}

std::set<std::string, std::less<std::string> > tracerd::getPackageDependencies ()
{
	std::set<std::string, std::less<std::string> > dependencies;

	ADDDEPENDENCY(dependencies, config);
	ADDDEPENDENCY(dependencies, xcept);
	ADDDEPENDENCY(dependencies, toolbox);
	ADDDEPENDENCY(dependencies, pt);
	ADDDEPENDENCY(dependencies, xdata);
	ADDDEPENDENCY(dependencies, xoap);
	ADDDEPENDENCY(dependencies, xdaq);

	return dependencies;
}

