// $Id: $

/*************************************************************************
 * XDAQ Tracer Daemon                      								 *
 * Copyright (C) 2000-2013, CERN.			                  			 *
 * All rights reserved.                                                  *
 * Authors: A. Forrest and L. Orsini				 					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                       		 *
 * For the list of contributors see CREDITS.   			         		 *
 *************************************************************************/

#include "tracer/tracerd/Application.h"

#include "b2in/nub/Method.h"

#include "xdaq/Event.h"
#include "xdaq/EndpointAvailableEvent.h"
#include "xdaq/InstantiateApplicationEvent.h"

#include "xdaq/ApplicationContextImpl.h"

#include "xgi/Utils.h"
#include "xgi/framework/Method.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTMLClasses.h"

#include "toolbox/task/TimerFactory.h"

#include "toolbox/regex.h"

#include "xcept/tools.h"

#include "xoap/DOMParserFactory.h"
#include "xoap/domutils.h"

#include "xdata/exdr/FixedSizeInputStreamBuffer.h"
#include "xdata/exdr/Serializer.h"

#include "toolbox/task/WorkLoopFactory.h"

XDAQ_INSTANTIATOR_IMPL (tracer::tracerd::Application)

tracer::tracerd::Application::Application (xdaq::ApplicationStub * s) throw (xdaq::exception::Exception)
	: xdaq::Application(s), xgi::framework::UIManager(this), dialup_(0)
{
	this->process_ = toolbox::task::bind(this, &tracer::tracerd::Application::process, "process");

	this->totalCounter_ = 0;
	this->publishedCounter_ = 0;
	this->lostCounter_ = 0;
	this->resetOnStart_ = false;

	this->dialup_ = 0;

	this->brokerWatchdog_ = "PT30S"; // 30 seconds bys default
	this->publishGroup_ = "eventing";
	this->brokerURL_ = "";
	this->eventingURL_ = "";

	this->maxIPCQueueSize_ = 10;
	this->maxIPCMessageSize_ = 2048;

	s->getDescriptor()->setAttribute("icon", "/tracer/tracerd/images/tracerd-icon.png");
	s->getDescriptor()->setAttribute("icon16x16", "/tracer/tracerd/images/tracerd-icon.png");

	s->getDescriptor()->setAttribute("service", "tracerd");

	this->getApplicationInfoSpace()->fireItemAvailable("brokerWatchdog", &brokerWatchdog_);

	this->getApplicationInfoSpace()->fireItemAvailable("brokerURL", &brokerURL_);
	this->getApplicationInfoSpace()->fireItemAvailable("publishGroup", &publishGroup_);

	this->getApplicationInfoSpace()->fireItemAvailable("eventingURL", &eventingURL_);
	this->getApplicationInfoSpace()->fireItemAvailable("resetOnStart", &resetOnStart_);

	this->getApplicationInfoSpace()->fireItemAvailable("maxIPCQueueSize", &maxIPCQueueSize_);
	this->getApplicationInfoSpace()->fireItemAvailable("maxIPCMessageSize", &maxIPCMessageSize_);

	xgi::framework::deferredbind(this, this,  &tracer::tracerd::Application::Default, "Default");
	//xgi::bind(this, &tracer::tracerd::Application::unlinkmq, "unlinkmq");

	b2in::nub::bind(this, &tracer::tracerd::Application::onMessage);

	// Listen to events indicating the setting of the application's default values
	this->getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");
	getApplicationContext()->addActionListener(this); // attach to endpoint available events
}

tracer::tracerd::Application::~Application ()
{
}

void tracer::tracerd::Application::actionPerformed (xdata::Event& e)
{
	if (e.type() == "urn:xdaq-event:setDefaultValues")
	{
		errno = 0;

		struct mq_attr attr;
		attr.mq_flags = 0;
		attr.mq_maxmsg = maxIPCQueueSize_;
		attr.mq_msgsize = maxIPCMessageSize_;
		attr.mq_curmsgs = 0;

		std::stringstream qname;
		qname << "/tracerd_queue_" << this->getApplicationContext()->getDefaultZoneName();

		if (resetOnStart_)
		{
			LOG4CPLUS_INFO(this->getApplicationLogger(), "IPC Queue '" << qname.str() << "' unlinked");
			mq_unlink(qname.str().c_str()); // make sure you create a queue and not use an existing one
		}
		else
		{
			LOG4CPLUS_INFO(this->getApplicationLogger(), "IPC Queue '" << qname.str() << "' reused if exists");
		}

		LOG4CPLUS_INFO(this->getApplicationLogger(), "Creating IPC Queue '" << qname.str() << "' with max_msgsize '" << attr.mq_msgsize << "' and queue_size '" << attr.mq_maxmsg << "'");

		// DO NOT umask THIS, or people wont be able to write into it
		mode_t omask;
		omask = umask(0);
		daemonQueue_ = mq_open(qname.str().c_str(), O_CREAT | O_RDONLY, 0777, &attr);
		umask(omask);

		//std::cout << "daemonQueue_ : " << daemonQueue_ << std::endl;

		if (daemonQueue_ == (mqd_t) - 1)
		{
			std::stringstream ss;
			ss << "Failed to open ipc message queue to talk to daemon : " << strerror(errno);
			XCEPT_RAISE(xdaq::exception::Exception, ss.str());
		}

		struct mq_attr attrs;
		mq_getattr(daemonQueue_, &attrs);

		LOG4CPLUS_INFO(this->getApplicationLogger(), "IPC Queue '" << qname.str() << "' created, size = " << attrs.mq_msgsize);

		if ((xdata::UnsignedIntegerT) maxIPCMessageSize_ < attrs.mq_msgsize)
		{
			std::stringstream ss;
			ss << "Bad configuration, an maxIPCMessageSize of '" << maxIPCMessageSize_ << "' is smaller than the allowed size of '" << attrs.mq_msgsize;
			LOG4CPLUS_ERROR(this->getApplicationLogger(), ss.str());
		}

		std::string name = "tracerd";
		try
		{
			toolbox::task::getWorkLoopFactory()->getWorkLoop(name, "waiting")->activate();
			toolbox::task::getWorkLoopFactory()->getWorkLoop(name, "waiting")->submit(process_);
		}
		catch (toolbox::task::exception::Exception& e)
		{
			std::stringstream ss;
			ss << "Failed to submit workloop " << name;
			//XCEPT_DECLARE_NESTED(tcpla::exception::WorkLoopSubmitFailed, ex, ss.str(), e);
			XCEPT_RETHROW(xdaq::exception::Exception, ss.str(), e);
		}
		catch (std::exception& se)
		{
			std::stringstream ss;
			ss << "Failed to submit notification to worker thread, caught standard exception : " << se.what();
			XCEPT_RAISE(xdaq::exception::Exception, ss.str());
		}
		catch (...)
		{
			XCEPT_RAISE(xdaq::exception::Exception, "Failed to submit notification to worker pool, caught unknown exception");
		}

	}
	else
	{
		std::stringstream msg;
		msg << "Failed to process unknown event type '" << e.type() << "'";
		LOG4CPLUS_FATAL(this->getApplicationLogger(), msg.str());
	}
}

bool tracer::tracerd::Application::process (toolbox::task::WorkLoop * w1)
{
	char buffer[(xdata::UnsignedIntegerT) maxIPCMessageSize_];
	while (true)
	{
		errno = 0;
		ssize_t len = mq_receive(daemonQueue_, buffer, maxIPCMessageSize_, NULL);

		if (len == -1)
		{
			LOG4CPLUS_ERROR(this->getApplicationLogger(), "Failed to receive on ipc queue : " << strerror(errno));
			continue;
		}

		xdata::exdr::FixedSizeInputStreamBuffer inBuffer(buffer, len);
		xdata::Properties plist;

		try
		{
			xdata::exdr::Serializer serializer;
			serializer.import(&plist, &inBuffer);
		}
		catch (xdata::exception::Exception & e)
		{
			std::stringstream msg;
			msg << "Failed to parse event properties in message from peer";

			LOG4CPLUS_ERROR(this->getApplicationLogger(), msg.str());
			continue;
		}

		std::cout << "received event to publish" << std::endl;

		try
		{
			this->onMessage(0, plist);
		}
		catch (b2in::nub::exception::Exception & e)
		{
			std::stringstream msg;
			msg << "Failed to deliver message : " << e.what();

			LOG4CPLUS_ERROR(this->getApplicationLogger(), msg.str());
			continue;
		}
	}

	return false;
}

/*
 * Setup communications with the broker and eventing
 */
void tracer::tracerd::Application::actionPerformed (toolbox::Event& e)
{
	if (e.type() == "xdaq::EndpointAvailableEvent")
	{
		toolbox::TimeInterval interval;
		try
		{
			interval.fromString(brokerWatchdog_.toString());
		}
		catch (toolbox::exception::Exception& e)
		{
			// Failed to parse the watchdog time string
			LOG4CPLUS_FATAL(this->getApplicationLogger(), "Failed to parse brokerWatchdog property '" << brokerWatchdog_.toString() << "'");
		}

		// If the available endpoint is a b2in endpoint we are ready to
		// discover other b2in endpoints on the network
		//
		std::string networkName = this->getApplicationDescriptor()->getAttribute("network");

		xdaq::EndpointAvailableEvent& ie = dynamic_cast<xdaq::EndpointAvailableEvent&>(e);
		const xdaq::Network* network = ie.getNetwork();

		LOG4CPLUS_INFO(this->getApplicationLogger(), "Received endpoint available event for network " << network->getName());

		if (network->getName() == networkName)
		{
			try
			{
				dialup_ = new b2in::utils::EventingDialup(this, publishGroup_, brokerURL_, eventingURL_, networkName, interval);
			}
			catch (b2in::utils::exception::Exception & ex)
			{
				LOG4CPLUS_FATAL(this->getApplicationLogger(), "Failed to create dialup object '" << xcept::stdformat_exception_history(ex) << "'");
			}
		}
	}
}

void tracer::tracerd::Application::onMessage (toolbox::mem::Reference * msg, xdata::Properties & plist) throw (b2in::nub::exception::Exception)
{
	if (plist.getProperty("urn:b2in-protocol:action") == "trace")
	{
		this->publishEvent(plist);
		if (msg != 0)
		{
			msg->release();
		}
	}
	else
	{
		std::string action = plist.getProperty("urn:xmasbroker2g:action");
		if (action == "allocate")
		{
			std::string model = plist.getProperty("urn:xmasbroker2g:model");
			if (model == "eventing" && dialup_ != 0)
			{
				dialup_->onMessage(msg, plist);
			}
			else
			{
				LOG4CPLUS_INFO(this->getApplicationLogger(), "Unknown broker response : " << model);
			}
		}
	}

	if (msg != 0) msg->release();
}

void tracer::tracerd::Application::publishEvent (xdata::Properties& plist) throw (tracer::tracerd::exception::Exception)
{
	totalCounter_ = totalCounter_ + 1;
	if (dialup_ == 0)
	{
		lostNoNetworkCounter_ = lostNoNetworkCounter_ + 1;
		return;
	}
	else if (!dialup_->isEstablished())
	{
		lostNotEstablishedCounter_ = lostNotEstablishedCounter_ + 1;
		return;
	}

	plist.setProperty("urn:b2in-protocol:service", "b2in-eventing");
	plist.setProperty("urn:b2in-eventing:topic", "tracer");
	plist.setProperty("urn:b2in-eventing:action", "notify");

	try
	{
		LOG4CPLUS_INFO(this->getApplicationLogger(), "Going to publish");
		dialup_->send(0, plist);
		publishedCounter_ = publishedCounter_ + 1;
		LOG4CPLUS_INFO(this->getApplicationLogger(), "Event published");
	}
	catch (b2in::utils::exception::Exception & e)
	{
		//XCEPT_RETHROW(tracer::exception::Exception, "Failed to publish exception to " + dialup_->getEventingURL() + ": " + e.message(), e);
		lostCounter_ = lostCounter_ + 1;
		return;
	}
}

void tracer::tracerd::Application::timeExpired (toolbox::task::TimerEvent& e)
{
	std::string name = e.getTimerTask()->name;

	if (name == "tracerd-staging")
	{

	}
}

// Hyperdaq

void tracer::tracerd::Application::Default (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	*out << "<div class=\"xdaq-tab-wrapper\">" << std::endl;

	// Tabbed pages

	*out << "<div class=\"xdaq-tab\" title=\"Statistics\">" << std::endl;
	this->StatisticsTabPage(out);
	*out << "</div>";

	*out << "</div>";
}

/*
 * B2IN Eventing statistics
 */
void tracer::tracerd::Application::StatisticsTabPage (xgi::Output * out)
{
	*out << cgicc::table().set("class", "xdaq-table-vertical") << std::endl;
	*out << cgicc::caption("Dialup");
	*out << cgicc::tbody() << std::endl;

	// State
	//
	*out << cgicc::tr();
	*out << cgicc::th("State");
	*out << cgicc::td().set("style", "min-width: 100px");
	if (dialup_ != 0)
	{
		*out << dialup_->getStateName();
	}
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	// Broker URL
	//
	*out << cgicc::tr();
	*out << cgicc::th();
	*out << "Broker address";
	*out << cgicc::th();
	*out << cgicc::td();
	*out << brokerURL_.toString();
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	// Broker Lost counter
	//
	*out << cgicc::tr();
	*out << cgicc::th();
	*out << "Broker messages";
	*out << cgicc::th();
	*out << cgicc::td();
	if (dialup_ != 0)
	{
		*out << dialup_->getBrokerCounter();
	}
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	// Broker Lost counter
	//
	*out << cgicc::tr();
	*out << cgicc::th();
	*out << "Broker messages lost";
	*out << cgicc::th();
	*out << cgicc::td();
	if (dialup_ != 0)
	{
		*out << dialup_->getBrokerLostCounter();
	}
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	// Eventing URL
	//
	*out << cgicc::tr();
	*out << cgicc::th();
	*out << "Eventing address";
	*out << cgicc::th();
	*out << cgicc::td();
	if (dialup_ != 0)
	{
		*out << dialup_->getEventingURL();
	}
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	// Exceptions sent
	//
	*out << cgicc::tr();
	*out << cgicc::th();
	*out << "Total Outgoing Reports";
	*out << cgicc::th();
	*out << cgicc::td();
	if (dialup_ != 0)
	{
		*out << dialup_->getOutgoingReportCounter();
	}
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	// Exceptions lost
	//
	*out << cgicc::tr();
	*out << cgicc::th();
	*out << "Total Reports lost";
	*out << cgicc::th();
	*out << cgicc::td();
	if (dialup_ != 0)
	{
		*out << dialup_->getOutgoingReportLostCounter();
	}
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	*out << cgicc::tbody() << std::endl;
	*out << cgicc::table() << std::endl;

	*out << cgicc::table().set("class", "xdaq-table-vertical") ;
	*out << cgicc::caption("Counters");
	*out << cgicc::tbody() << std::endl;

	// Total Events
	*out << cgicc::tr();
	*out << cgicc::th();
	*out << "Total Events";
	*out << cgicc::th();
	*out << cgicc::td().set("style", "min-width: 100px");
	*out << totalCounter_;
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	// Published Events
	*out << cgicc::tr();
	*out << cgicc::th();
	*out << "Published Events";
	*out << cgicc::th();
	*out << cgicc::td();
	*out << publishedCounter_;
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	// Lost Events
	*out << cgicc::tr();
	*out << cgicc::th();
	*out << "Lost Events";
	*out << cgicc::th();
	*out << cgicc::td();
	*out << lostCounter_;
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	// Lost No Network Events
	*out << cgicc::tr();
	*out << cgicc::th();
	*out << "Lost Events (No Network)";
	*out << cgicc::th();
	*out << cgicc::td();
	*out << lostNoNetworkCounter_;
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	// Lost Not Established Events
	*out << cgicc::tr();
	*out << cgicc::th();
	*out << "Lost Events (Not Established)";
	*out << cgicc::th();
	*out << cgicc::td();
	*out << lostNotEstablishedCounter_;
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	*out << cgicc::tbody() << std::endl;
	*out << cgicc::table() << std::endl;
}

/*
void tracer::tracerd::Application::unlinkmq (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	
	  //Note to devs
	 //
	  //To view message queues, you need to mount the directory somewhere.
	 //
	  //e.g. sudo mount -t mqueue none /usr/tmp/mqueues/
	 //
	bool relink = false;
	std::string mqName;
	try
	{
		cgicc::Cgicc cgi(in);
		std::string relinkString = "";
		// retrieve value and update exported variable

		cgicc::form_iterator rl = cgi.getElement("relink");
		if (rl != cgi.getElements().end())
		{
			relinkString = cgi("relink");
			if (relinkString == "true")
			{
				relink = true;
			}
		}

		rl = cgi.getElement("name");
		if (rl != cgi.getElements().end())
		{
			mqName = cgi("name");
		}
		if (mqName == "")
		{
			LOG4CPLUS_WARN(this->getApplicationLogger(), "Attempt to unlink message queue - no valid name supplied");
			*out << "<html><head><title>EXPERT ONLY PAGE - mq_unlink</title></head>";
			*out << "<body>Could not find a value for 'name' in query, no queue unlinked</body>";
			*out << "</html>";
			return;
			
		}
	}
	catch (const std::exception & e)
	{
		LOG4CPLUS_WARN(this->getApplicationLogger(), "Attempt to unlink message queue - CGI failure : " << e.what());
		*out << "<html><head><title>EXPERT ONLY PAGE - mq_unlink FAIL</title></head>";
		*out << "<body>Error occured : " << e.what() << "</body>";
		*out << "</html>";
		return;
	}

	int ret = mq_unlink(mqName.c_str()); // make sure you create a queue and not use an existing one
	if (ret == -1)
	{
		LOG4CPLUS_WARN(this->getApplicationLogger(), "Attempt to unlink message queue - '" << mqName << "' could not be unlinked : " << strerror(errno));
		*out << "<html><head><title>EXPERT ONLY PAGE - mq_unlink FAIL</title></head>";
		*out << "<body>Failed to unlink queue '" << mqName << "', errno = '" << strerror(errno) << "'</body>";
		*out << "</html>";
		return;
	}

	LOG4CPLUS_WARN(this->getApplicationLogger(), "Attempt to unlink message queue - '" << mqName << "'");

	*out << "<html><head><title>EXPERT ONLY PAGE - mq_unlink</title></head>";
	*out << "<body>Message queue '" << mqName << "', unlinked";

	if (relink)
	{
		LOG4CPLUS_WARN(this->getApplicationLogger(), "relink not supported in mqunlink");
		*out << " </br>WARNING ! ! relink not supported";
	}
	*out << "</body></html>";
}
*/

