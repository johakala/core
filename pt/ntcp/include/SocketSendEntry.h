#ifndef __SocketSendEntry_h__
#define __SocketSendEntry_h__

#include "SAP.h"
#include "pta.h"
#include "rlist.h"
#include "xdaqIOException.h"
#include "Transmitter.h"


class SocketSendEntry : public SAP
{
	public:
	


    	SocketSendEntry(Address* local, Address * remote, pta * ptap, Transmitter * transmitter, Logger logger) :logger_ (Logger::getInstance(logger.getName() + ".SocketSendEntry")) 
	{
		transmitter_ = transmitter;
		ptap_ = ptap;
		handle_ = new vxtcphandle();
		try {
			//GUT Added local binding for sending over specific NIC
			//
			// For binding a send object to a local address, we must only
			// specify a local IP address. The port is assigned by the OS.
			tcpAddr l = *((tcpAddr*)local);
			l.port = 0;
			if (local != 0) handle_->bind ( &l);
			address_ = *(tcpAddr*)remote;
		
		} catch (toolbox::Exception e ) {
			string msg = toolbox::toString("cannot bind local ip address: %s port:%d",((tcpAddr*)local)->host,((tcpAddr*)local)->port);
			XDAQ_LOG_AND_RAISE (xdaqIOException, logger_, msg);
		}
		
	}
	
	
	virtual ~SocketSendEntry() 
	{
		delete handle_;
	}
    
	//
	//! Send a character buffer. The function
	//! does not have control over the memory
	//! and does therefore not release it automatically
	//
	void put(Buffer* buffer, int len) throw (xdaqException)
	{
		try {
			handle_->connect(&address_);
		}
		catch (toolbox::Exception e ) {
			//cout << e;
			XDAQ_LOG_AND_RAISE (xdaqIOException, logger_, e.what());
		}
		transmitter_->post(buffer, len, 0, handle_);
			
	}	
	
	//
	//! Send a buffer and free it. The buffer
	//! is automatically released in case of
	//! error, too. If the user wants to retry,
	//! he has to re-allocate a new buffer reference
	//
	void put (BufRef * ref, int len ) throw (xdaqException) 
	{
	    this->put(ref->buffer(), len, ref);
	}
	
	void put(Buffer* buffer, int len, BufRef * freeRef)  throw (xdaqException)
	{
		try {
			handle_->connect(&address_);
		}
		catch (toolbox::Exception e ) {
			//cout << e;
			executive->frameFree(freeRef);
			XDAQ_LOG_AND_RAISE (xdaqIOException,logger_, e.what());
		}
		
		transmitter_->post(buffer, len, freeRef, handle_);

			
	}
	
	//
	// Provide two dummy implementations for the SAP interface.
	// The Send Entry will not implement a receive function.
	//
	// Used for polled output

	bool pollRecv() {
		LOG4CPLUS_ERROR (logger_,"cannot call receive on SendEntry");
		return false;
	}
	
	
	void waitRecvLoop()	{
		LOG4CPLUS_ERROR (logger_,"cannot call receive on SendEntry");
	}
	
	
		
	protected:
	
		vxtcphandle * handle_;
		tcpAddr  address_;
		pta * ptap_;
		BSem * sync_;
		Transmitter * transmitter_;
		Logger logger_;
	
};


#endif
