#ifndef _Transmitter_h_
#define _Transmitter_h_

#include "rlist.h"
#include "xdaq.h"
#include "toolbox.h"
#include "Task.h"

class Transmitter: public Task
{

	public:

	Transmitter(unsigned long queueSize, Logger logger) :Task("Transmitter"), logger_ (Logger::getInstance(logger.getName() + ".Transmitter")) 
	{

		outputQueue_.resize(queueSize);

	}

	typedef struct PostDescriptor {
		BufRef * ref;
		vxtcphandle * handle;
		unsigned long size;
		Buffer * buffer;
	} PostDescriptorType; 

	void post(Buffer * buffer,  unsigned long size, BufRef * freeRef, vxtcphandle * handle )
	{
		PostDescriptorType outputDescriptor;
		outputDescriptor.ref =freeRef;
		outputDescriptor.size = size;
		outputDescriptor.handle = handle;
		outputDescriptor.buffer = buffer;
		outputQueue_.push_back(outputDescriptor);
	}

	int svc()
	{

		for (;;)
		{

			if ( ! outputQueue_.empty() )
			{
				PostDescriptorType outputDescriptor = outputQueue_.front();
				outputQueue_.pop_front();
				
				try {
				    outputDescriptor.handle->write((char*)outputDescriptor.buffer->userAddr(),outputDescriptor.size);	

				}
				catch (toolbox::Exception e ) {
					//cout << e;
					if ( outputDescriptor.ref != 0 )
						executive->frameFree(outputDescriptor.ref);
					XDAQ_RAISE (xdaqIOException, e.what());
				}
				
				if ( outputDescriptor.ref != 0 )
					xdaq::frameFree(outputDescriptor.ref);
								
			} else
			{
				this->yield(1);
			
			}
		}


	}

	rlist<PostDescriptorType> outputQueue_;
	Logger logger_;


};

#endif
