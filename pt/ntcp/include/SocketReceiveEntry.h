#ifndef __SocketReceiveEntry_h__
#define __SocketReceiveEntry_h__


#include "pta.h"
#include "SmartBufPool.h"
#include "BufRef.h"
#include "STL.h"
#include "pt.h"
#include "vxioslib.h"
#include "i2o/i2o.h"
#include "i2o/i2oObjLib.h"
#include "i2o/i2oDdmLib.h"
#include "i2o/shared/i2oexec.h"
#include "i2oBinding.h"
#include "BSem.h"
#include "xdaqDebug.h"
#include "i2oEndian.h"
#include "i2oProtocolIn.h"
#include "SmartBufPool.h"
#include "xdaqIOException.h"
#include "xdaqExecutive.h"

class SocketReceiveEntry: public SAP {
	public:
	
	enum { MaxNoChannels = 1024 };
	
	SocketReceiveEntry(Address * addr, pta * ptap, i2oProtocolIn * pin, bool pollingMode, Logger logger) :logger_ (Logger::getInstance(logger.getName() + ".SocketReceiveEntry")) 
	{
	
		mtuSize_ = BUF_SIZE_65536;
		
		ptap_ = ptap;
		pin_ = pin;
		handle_ = new vxtcphandle();
		pin_ = pin;

    	address_ = *(tcpAddr*)addr;
		
		LOG4CPLUS_DEBUG(logger_,toolbox::toString("address for this socket is host:%s port:%ld",address_.host, address_.port));
		
	   	handle_->bind(&address_);
	   	listenfd_ = handle_->listen(MaxNoChannels);
	   	FD_ZERO(&allset_);
	   	FD_SET(listenfd_,&allset_);
	   	accepted_ = 0;
	   	nochannels_ =0;
	   	fdv_.resize(MaxNoChannels);
		
		for (int j = 0; j < MaxNoChannels; j++)
		{
			handlev_.push_back( (vxtcphandle*) 0);
		}

		
		if (! pollingMode) {

				timeoutp_ = 0;
    		}
    		else
    		{
            	timeout_.tv_sec =  0;
            	timeout_.tv_usec = 0;
	    	timeoutp_ = &timeout_;
		}
		
    	maxfd_ = listenfd_;
    	FD_ZERO(&fdset_);

    	for (unsigned int i=0; i<fdv_.size(); i++ )
            	 fdv_[i] = -1;

	}
	
	
	virtual ~SocketReceiveEntry()
	{
		
		delete handle_;
		
		vector<vxtcphandle*>::iterator i = handlev_.begin();
		while (i != handlev_.end())
		{
			if ((*i) != 0) {
				delete (*i);
			}
			i++;
		}
		
	}
	
	inline void waitRecvLoop () {
		for(;;)
			this->pollRecv();

	}
	
	inline bool pollRecv () {
	
		try
  		{	
			fdset_ = allset_;
			
			nready_  = select(maxfd_+1, &fdset_, 0,  0, timeoutp_);
			
			if (nready_ == 0 ) return false; // nothing to read
			
			if ( FD_ISSET(listenfd_,&fdset_) ) {  // accept connection and return FALSE
				tcpAddr addr;
  				int newsock = handle_->accept(&addr);				
				if (newsock > maxfd_ ) maxfd_ = newsock;
				FD_SET(newsock,&allset_);
				vxtcphandle * h = new vxtcphandle(newsock);
				
				//handlev_[nochannels_] = h;
				//fdv_[nochannels_] = newsock;
				//pin_->addOriginator(nochannels_);
				
				handlev_[newsock] = h;
				fdv_[newsock] = newsock;
				pin_->addOriginator(newsock);
				
				accepted_++;
				//nochannels_++;
				return true;    // the ref has not been consumed
			}
			for (unsigned long i=0; i< fdv_.size(); i++ ) { //check all client for data
				if (fdv_[i] < 0 ) {
					continue;
				}	
				if ( FD_ISSET(fdv_[i],&fdset_) )
				{
					current_ = i;
					
					BufRef * ref = executive->frameAlloc(mtuSize_);
					safeTcpRead(handlev_[current_],ref->buf_, sizeof(I2O_MESSAGE_FRAME), mtuSize_);
					I2O_MESSAGE_FRAME i2oframe = *((PI2O_MESSAGE_FRAME)ref->buf_);
					swapU32((U32*)&i2oframe,sizeof(I2O_MESSAGE_FRAME));
					int len = i2oframe.MessageSize << 2;
					if ( len > mtuSize_) {
						string msg = toolbox::toString("Fatal, received frame size of %d cannot fit in MTU of %d (increase mtuSize for ptNTCP)",len,mtuSize_);
						XDAQ_LOG_AND_RAISE( xdaqException, logger_, msg);
					}
					safeTcpRead(handlev_[current_],&ref->buf_[sizeof(I2O_MESSAGE_FRAME)], len-sizeof(I2O_MESSAGE_FRAME),mtuSize_- sizeof(I2O_MESSAGE_FRAME));
					
					len = pin_->in(ref,current_);
					
					LOG4CPLUS_DEBUG (logger_,toolbox::toString("I2O Protocol in requests to read: %d bytes more.", len));
					
					while ( len > 0 ) {
						ref = executive->frameAlloc(mtuSize_);
						safeTcpRead(handlev_[current_],ref->buf_, len, mtuSize_);	
						len = pin_->in(ref,current_);
					}
					FD_CLR(fdv_[current_], &fdset_);
					if (--nready_ <= 0 )
						break;    // no more descriptors available
					
				}
				
			}
			return true; // if here nothing happened
  		}
  		catch(toolbox::Exception e)
  		{
			delete handlev_[current_];
			handlev_[current_] = 0;
			FD_CLR(fdv_[current_], &allset_);
			fdv_[current_] = -1; // entry lost or unused
			return true;
  		}
	
		return false;
	}
	
	inline void safeTcpRead(vxtcphandle * handle, char * buf, int len, int maxlen ) {
		unsigned long totalBytes = 0;
		int nBytes;
		int size = len;
        	do {
                	nBytes = size - totalBytes; // missing bytes to read
			if ( nBytes > maxlen ) {
				string msg = toolbox::toString("fatal, buffer overflow in TCP read (check your I20 frame size is not larger than MTU size)");
				XDAQ_LOG_AND_RAISE(xdaqException,logger_,msg);
			}
			totalBytes += handle->read(&buf[totalBytes],nBytes);
			//cout << "read :" << totalBytes << endl;
         	} while (totalBytes < (unsigned long)size );
		
		//dump(buf,len);
	}
	void put (BufRef * ref, int len )  throw (xdaqException){
		XDAQ_LOG_AND_RAISE(xdaqException, logger_, "not implemented");
	}
	
	/*
	void put (BufRef * ref, int len ) {
	
		try {
		
			handlev_[current_]->write(ref->buf_,len);
			executive->frameFree(ref);
				
		} catch (vxException e) {
			executive->frameFree(ref);
			XDAQ_LOG_AND_RAISE (xdaqIOException, logger_, e.what_);
		}
	
	}
	*/
	void put (Buffer* buffer, int len, BufRef * freeRef ) throw (xdaqException)
	{	
		try {
			handlev_[current_]->write((char*)buffer->userAddr(),len);
			executive->frameFree(freeRef);
		} catch (toolbox::Exception e) 
		{
			executive->frameFree(freeRef);
			XDAQ_LOG_AND_RAISE (xdaqIOException, logger_, e.what());
		}
	
	}
	
	void put (Buffer* buffer, int len ) throw (xdaqException)
	{	
		try {
			handlev_[current_]->write((char*)buffer->userAddr(),len);
		} catch (toolbox::Exception e) 
		{
			XDAQ_LOG_AND_RAISE (xdaqIOException, logger_,e.what());
		}
	
	}
	
	protected:
		  
        vxtcphandle * handle_;
        vector<vxtcphandle*> handlev_;
        vector<int> fdv_;
        tcpAddr  address_;
        fd_set fdset_;
        fd_set allset_;
        int maxfd_;
        int current_;
        int listenfd_;
        int accepted_;
        int nochannels_;
        int nready_;
        int len_;
        struct timeval timeout_; // nonblocking select
	pta * ptap_;
	i2oProtocolIn * pin_;
	struct timeval* timeoutp_;
	int mtuSize_;
	Logger logger_;
};

#endif
