//
// Version definition for ptTCP
//
#ifndef ptNTCPV_H
#define ptNTCPV_H

#include "PackageInfo.h"

namespace ptNTCP {
    const string package  =  "pt/ntcp";
    const string versions =  "1.0";
    const string description = "TCP peer transport supporting non blocking message delivery";
    toolbox::PackageInfo getPackageInfo();
    void checkPackageDependencies() throw (toolbox::PackageInfo::VersionException);
    set<string, less<string> > getPackageDependencies();
}

#endif
