#ifndef __ptTCP_h__
#define __ptTCP_h__

#include "pt.h"
#include "Task.h"
#include "xdaqSO.h"
#include "xdaqExecutive.h"


class pta;
class i2oProtocolOut;
class i2oProtocolIn;
class SocketSendEntry;
class SocketReceiveEntry;
class Transmitter;

class ptNTCP: public pt, public Task 
{

	public:
	ptNTCP(pta * ptap);
	~ptNTCP();

	SendObj * createSendObj(Address * local, Address * remote);
	Address * createAddress (DOMNode *addressNode);
	RecvObj * createRecvObj(Address * address);
	void ParameterSetDefault(list<string> & paramNames) ;
	int svc();
	void plugin();
	
	protected:
	
	vector<SocketReceiveEntry*> recvSocketTable_;
	vector<SocketSendEntry*> sendSocketTable_;
	
	i2oProtocolOut* pout_;
	vector<i2oProtocolIn*>  pinTable_;
	Transmitter * transmitter_;
	

};



class ptNTCPSO: public xdaqSO 
{
    public:

    virtual ~ptNTCPSO() 
    {
    }

    void init() 
	{	  		
        pt_ = new ptNTCP ( executive->peerTransportAgent()); 						 
        
		executive->install(pt_);
		
    }

    void shutdown() 
	{
            delete pt_;
    }

    protected:

    	ptNTCP * pt_;
};


#endif
