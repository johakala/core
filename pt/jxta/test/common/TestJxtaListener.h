#ifndef _TestJxtaListener_h_
#define _TestJxtaListener_h_

#include <string>
#include <exception>

#include "pt/JxtaListener.h"

class TestJxtaListener : public pt::JxtaListener {

virtual void processIncomingMessage(std::string message) throw (std::exception) {
	std::cout << "\n-----\n";
	std::cout << "processIncomingMessage: got message:\n" << message << std::endl;
	std::cout << "-----\n\n";
}
};

#endif
