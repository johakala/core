
#########################################################################
# XDAQ Components for Distributed Data Acquisition                      #
# Copyright (C) 2000-2004, CERN.                                        #
# All rights reserved.                                                  #
# Authors: J. Gutleberi, G. Lo Presti and L. Orsini                     #
#                                                                       #
# For the licensing terms see LICENSE.                                  #
# For the list of contributors see CREDITS.                             #
#########################################################################

##
#
# Package level makefile
#
##
XDAQ_BACK_TO_ROOT:=../../..

include $(XDAQ_BACK_TO_ROOT)/$(BUILD_SUPPORT)/mfAutoconf.rules

include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfDefs.$(XDAQ_OS)

Project=$(PROJECT_NAME)
Package=pt/jxta

Sources=\
	Address.cc \
	AdvertisementImpl.cc \
	AdvertisementListImpl.cc \
	DiscoveryServiceImpl.cc \
	RdvServiceImpl.cc \
	PipeServiceImpl.cc \
	PeerGroupImpl.cc \
	PlatformImpl.cc \
	JxtaMessenger.cc \
	PeerTransportReceiver.cc \
	PeerTransportSender.cc \
	ptjxtaV.cc \
	PeerTransportJxta.cc

IncludeDirs = \
	$(XDAQ_ROOT)/daq/extern/log4cplus/$(XDAQ_OS)$(XDAQ_PLATFORM)/include \
	$(XDAQ_ROOT)/daq/extern/xerces/$(XDAQ_OS)$(XDAQ_PLATFORM)/include \
	$(XDAQ_ROOT)/daq/extern/cgicc/$(XDAQ_OS)$(XDAQ_PLATFORM)/include \
	$(XDAQ_ROOT)/daq/toolbox/include \
	$(XDAQ_ROOT)/daq/toolbox/include/$(XDAQ_OS) \
	$(XDAQ_ROOT)/daq/toolbox/include/solaris \
	$(XDAQ_ROOT)/daq/xdaq/include \
	$(XDAQ_ROOT)/daq \
	$(XDAQ_ROOT)/daq/xgi/include \
	$(XDAQ_ROOT)/daq/pt/include \
	$(XDAQ_ROOT)/daq/xdata/include \
	$(XDAQ_ROOT)/daq/xcept/include \
	$(XDAQ_ROOT)/daq/xoap/include \
	$(XDAQ_ROOT)/daq/extern/i2o/include \
	$(XDAQ_ROOT)/daq/extern/i2o/include/i2o/shared \
	$(XDAQ_ROOT)/daq/extern/jxta/$(XDAQ_OS)$(XDAQ_PLATFORM)/src \
	$(XDAQ_ROOT)/daq/extern/jxta/$(XDAQ_OS)$(XDAQ_PLATFORM)/lib/apr/include/apr-0

LibraryDirs = \
	$(XDAQ_ROOT)/daq/extern/jxta/$(XDAQ_OS)$(XDAQ_PLATFORM)/lib/apr/lib \
	$(XDAQ_ROOT)/daq/extern/jxta/$(XDAQ_OS)$(XDAQ_PLATFORM)/src/.libs \
	$(XDAQ_ROOT)/daq/extern/jxta/$(XDAQ_OS)$(XDAQ_PLATFORM)/src/jpr \
	$(XDAQ_ROOT)/daq/extern/jxta/$(XDAQ_OS)$(XDAQ_PLATFORM)/src/jpr/.libs \
	$(XDAQ_ROOT)/daq/xdata/lib/$(XDAQ_OS)/$(XDAQ_PLATFORM) \
	$(XDAQ_ROOT)/daq/pt/lib/$(XDAQ_OS)/$(XDAQ_PLATFORM) \
	$(XDAQ_ROOT)/daq/toolbox/lib/$(XDAQ_OS)/$(XDAQ_PLATFORM) \
	$(XDAQ_ROOT)/daq/xcept/lib/$(XDAQ_OS)/$(XDAQ_PLATFORM) \
	$(XDAQ_ROOT)/daq/xdaq/lib/$(XDAQ_OS)/$(XDAQ_PLATFORM) \
	$(XDAQ_ROOT)/daq/xoap/lib/$(XDAQ_OS)/$(XDAQ_PLATFORM) \
	$(XDAQ_ROOT)/daq/xgi/lib/$(XDAQ_OS)/$(XDAQ_PLATFORM)

TestLibraryDirs = \
	$(XDAQ_ROOT)/daq/extern/cgicc/$(XDAQ_OS)$(XDAQ_PLATFORM)/lib \
	$(XDAQ_ROOT)/daq/extern/xerces/$(XDAQ_OS)$(XDAQ_PLATFORM)/lib \
	$(XDAQ_ROOT)/daq/extern/log4cplus/$(XDAQ_OS)$(XDAQ_PLATFORM)/lib \
	$(XDAQ_ROOT)/daq/toolbox/lib/$(XDAQ_OS)/$(XDAQ_PLATFORM) \
	$(XDAQ_ROOT)/daq/xdaq/lib/$(XDAQ_OS)/$(XDAQ_PLATFORM) \
	$(XDAQ_ROOT)/daq/xoap/lib/$(XDAQ_OS)/$(XDAQ_PLATFORM) \
	$(XDAQ_ROOT)/daq/xgi/lib/$(XDAQ_OS)/$(XDAQ_PLATFORM) \
	$(XDAQ_ROOT)/daq/xdata/lib/$(XDAQ_OS)/$(XDAQ_PLATFORM) \
	$(XDAQ_ROOT)/daq/pt/lib/$(XDAQ_OS)/$(XDAQ_PLATFORM) \
	$(XDAQ_ROOT)/daq/pt/jxta/lib/$(XDAQ_OS)/$(XDAQ_PLATFORM) \
	$(XDAQ_ROOT)/daq

ExternalObjects= \
	$(XDAQ_ROOT)/daq/extern/jxta/$(XDAQ_OS)$(XDAQ_PLATFORM)/src/.libs/libjxta.a
# for the time being they are directly linked to xdaq.exe
#$(XDAQ_ROOT)/daq/extern/jxta/$(XDAQ_OS)$(XDAQ_PLATFORM)/lib/apr/lib/libaprutil-0.a \
#$(XDAQ_ROOT)/daq/extern/jxta/$(XDAQ_OS)$(XDAQ_PLATFORM)/lib/apr/lib/libapr-0.a \
#$(XDAQ_ROOT)/daq/extern/jxta/$(XDAQ_OS)$(XDAQ_PLATFORM)/src/jpr/.libs/libjpr.a 

UserCFlags =
UserCCFlags =
UserDynamicLinkFlags =
UserStaticLinkFlags =
UserExecutableLinkFlags =


# These libraries can be platform specific and
# potentially need conditional processing
#
Libraries =

#
# Compile the source files and create a shared library
#
DynamicLibrary=ptjxta
StaticLibrary=
Executables=
TestExecutables= JxtaPTMain.cc
TestLibraries= peer xcept xerces-c cgicc xoap xgi log4cplus xdata xdaq toolbox apr-0 aprutil-0 jpr jxta ptjxta 

include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/Makefile.rules
