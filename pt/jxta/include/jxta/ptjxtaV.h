
/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber, G. Lo Presti and L. Orsini                      *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************

 * Last modified: February 28, 2005
 *
 * Version history:
 *  1.7 - Feb 28, 2005: beta release, Jxta-C 2.1 code freeze snapshot, revised discovery API
 *  1.6 - Dec  9, 2004: adapted to the final XDAQ3 API
 *  1.5 - Nov 19, 2004: added support for publishing custom advs
 *                      refactored pipe service, PT and references to make use of the new toolbox 
 *  1.1 - Nov  4, 2004: first XDAQ3 integrated release with a XDAQ application 
 *  1.0 - Oct 28, 2004: first official release in the CMS CVS;
 *                      added messaging for proper shutdown, decoupled code from patched Jxta-C
 *  0.7 - Oct 22, 2004: pre-alpha release with peer groups and services
 *  0.6 - Oct  5, 2004: refactoring peergroup, discovery service, rdv service, pipe service
 *  0.5 - Sep 24, 2004: refactoring PT with pipe service
 *  0.4 - Sep 20, 2004: custom rdv connections
 *  0.3 - Sep 10, 2004: partial finished refactoring, discovery listener
 *  0.2 - Aug 27, 2004: main refactoring, Platform class, Advertisement wrapping 
 *  0.1 - July,   2004: initial Jxta PT draft with pipe service
 *
 *************************************************************************/

//
// Version definition for Jxta peer transport
//
#ifndef _ptjxta_h_
#define _ptjxta_h_

#include "PackageInfo.h"


namespace ptjxta
{
    const string package  =  "pt/jxta";
    const string versions =  "1.6";
    const string description = "<a href='http://jxta-c.jxta.org'>Jxta</a> Peer Transport with edge peer support over TCP & HTTP";
    const string link = "http://cern.ch/lopresti/jxta/ptjxta-api/hierarchy.html";
    toolbox::PackageInfo getPackageInfo();
    void checkPackageDependencies() throw (toolbox::PackageInfo::VersionException);
    set<string, less<string> > getPackageDependencies();
}

#endif

