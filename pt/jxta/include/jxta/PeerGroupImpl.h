
/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber, G. Lo Presti and L. Orsini                      *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _jxta_PeerGroupImpl_h
#define _jxta_PeerGroupImpl_h

#include "jxta/PeerGroup.h"
#include "jxta/exception/Exception.h"
#include "jxta/DiscoveryServiceImpl.h"
#include "jxta/RdvServiceImpl.h"
#include "jxta/PipeServiceImpl.h"

#include "jxta/BootstrapRdvDiscListener.h"

#include "jxta.h"
#include "jxta_peergroup.h"
#include "jxta_listener.h"
#include "jxta_vector.h"
#include "jpr/jpr_thread.h"


namespace jxta {

class PeerGroupImpl: public jxta::PeerGroup {

	public:
	
	PeerGroupImpl() throw (jxta::exception::Exception);
	PeerGroupImpl(std::string newGroupName) throw (jxta::exception::Exception);
	~PeerGroupImpl();
	
	std::string getPeerName();
	std::string getPeerID();
	std::string getPeerGroupName();
	std::string getPeerGroupID();
	
	DiscoveryService* getDiscoveryService();
	RdvService* getRdvService();
	PipeService* getPipeService();
	
	private:
	
	Jxta_PG* j_pg_;
	std::string peerName_, peerID_;
	std::string peerGroupName_, peerGroupID_;

	DiscoveryService* discSvc_;
	RdvService* rdvSvc_;
	PipeService* pipeSvc_;

	static PeerGroupImpl* netPG_;
};

}


#endif

