
/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber, G. Lo Presti and L. Orsini                      *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _jxta_JxtaMessenger_h
#define _jxta_JxtaMessenger_h

#include "pt/JxtaMessenger.h"
#include "pt/exception/Exception.h"
#include "jxta/Address.h"

#include "jxta_pipe_service.h"

namespace jxta
{

//! This class represents a XDAQ compliant messenger over the Jxta network.
//! It wraps a Jxta output pipe.
class JxtaMessenger: public pt::JxtaMessenger
{
	public:

	//! Constructs a Jxta Messenger. Shall not be called directly by the user;
	//! to obtain a JxtaMessenger instance use the PeerTransportSender or the PipeService.
	JxtaMessenger(pt::Address::Reference destination, Jxta_outputpipe* j_op);
	virtual ~JxtaMessenger();

	//! Returns the destination address for which this Messenger has been configured.
	//@return a reference to the destination address.
	pt::Address::Reference getDestinationAddress();

	//! Sends a message over the Jxta network to the configured destination.
	//! @param string message a text message to be sent. Can contain XML tags.
	void send (std::string message) throw (pt::exception::Exception);

	//! Sends a message as above. Users should use only the previous function.
	//! @param tagName is the tag name used to send the message; can be customized for internal purposes.
	void send (std::string tagName, std::string message) throw (pt::exception::Exception);

	private:

	Jxta_outputpipe* j_op_;
	pt::Address::Reference destination_;
};

}


#endif
