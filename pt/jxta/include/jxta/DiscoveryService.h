
/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber, G. Lo Presti and L. Orsini                      *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _jxta_DiscoveryService_h
#define _jxta_DiscoveryService_h

#include "jxta/exception/Exception.h"
#include "jxta/Advertisement.h"
#include "jxta/AdvertisementList.h"
#include "jxta/DiscoveryListener.h"

namespace jxta
{

//! This class wraps the Jxta Discovery Service. This service is responsible for all discovery queries over the Jxta network.
class DiscoveryService
{
	public:

	static int PEER;
	static int GROUP;
	static int ADV;
	
	//! Queries the local cache getting all known running peers, peer groups or pipes.
	//! @param int advType the requested advertisement type; one of DiscoveryService::PEER, DiscoveryService::GROUP, DiscoveryService::ADV
	//! @return a list of advertisements, or an empty list if nothing is present in the local cache.s
	virtual jxta::AdvertisementList::Reference getKnownAdvertisements(int advType = 0) throw (jxta::exception::Exception) = 0;

	//! Remotely and asynchronously queries the network getting all running peers, peer groups or pipes.
	//! To get the discovered advertisements use a DiscoveryListener or call getKnownAdvertisements()
	//! @param int advType the requested advertisement type; one of DiscoveryService::PEER, DiscoveryService::GROUP, DiscoveryService::ADV
	virtual void searchRemoteAdvertisements(int advType = 0) = 0;

	//! Flushes the local cache
	//! @param int advType the requested advertisement type; one of DiscoveryService::PEER, DiscoveryService::GROUP, DiscoveryService::ADV
	virtual void flushAdvertisements(int advType = 0) = 0;

	
	//! Publishes the given advertisement to the Jxta network. Currently used to publish pipe advertisements.
	virtual bool publishAdvertisement(Advertisement::Reference adv) = 0;

	//! Gives the peer which is running the requested service svcName (i.e. published the related advertisement).
	//! @param string advType the advertisement type searched for; one of Advertisement::PEER, Advertisement::GROUP, Advertisement::PIPE
	//! @param string svcName the queried service name; it can contain '*' wildcard characters
	//! @param bool remote if true forces a synchronous remote discovery, otherwise searches only on the local cache
	//! @return the first matching advertisement, or an empty AdvertisementReference if the target advertisement is not found
	virtual jxta::Advertisement::Reference getAdvertisementByName(std::string advType, std::string svcName, bool remote) = 0;

	
	//! Registers a listener to the discovery service. It is called back whenever a new peer is discovered.
	virtual void addServiceListener(DiscoveryListener* listener) throw (jxta::exception::Exception) = 0;

	//! Removes a previously registered discovery listener.
	virtual void removeServiceListener(DiscoveryListener* listener) = 0;

	//! Removes all previously registered discovery listeners.
	virtual void removeAllServiceListeners() = 0;
};

}


#endif
