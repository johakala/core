
/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber, G. Lo Presti and L. Orsini                      *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _jxta_Address_h
#define _jxta_Address_h

#include "pt/Address.h"
#include "pt/exception/InvalidAddress.h"

#include "toolbox/net/URL.h"
#include <netinet/in.h>

namespace jxta
{

//! This class provides the network transport specific information.
//! In general the format of an address takes the form
//! <protocol>:://<network address>/<service name>/<service parameters>
//! This implementation is identical to the http::Address class plus some
//! Jxta specific methods.

class Address: public pt::Address
{
	public:
	
	//! Create address from url
	Address (const std::string& url) throw (pt::exception::InvalidAddress);
	
	virtual ~Address();
	
	//! Get the host part of the url
	std::string getHost();
	
	//! Get the port number of the url as int
	int getPort();

	//! Get the address in the Jxta pipe format, i.e. jxtapipe:<IP>:<port>
	std::string getPipeName();

	
	// --- Inherited methods: ---
	
	//! Retrieve the protocol part of the address, e.g. jxta
	std::string getProtocol();
	
	//! Retrieve the service name, e.g. "pipe"
	std::string getService();
	
	//! Get a string representation of the address, e.g. a URL
	std::string toString();
	
	//! Retrieve additional service parameters
	std::string getServiceParameters();
	
	//! Address comparison
	bool equals(pt::Address::Reference addressRef);

	protected:
	
	toolbox::net::URL * url_;
};

}

#endif

