
/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber, G. Lo Presti and L. Orsini                      *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _jxta_PeerTransportSender_h
#define _jxta_PeerTransportSender_h

#include <string>

#include "pt/PeerTransportSender.h"
#include "jxta/Address.h"
#include "jxta/JxtaMessenger.h"
#include "jxta/PlatformImpl.h"
#include "jxta/RdvService.h"
#include "jxta/PipeService.h"


namespace jxta
{

//! This class represents a XDAQ compliant PeerTransportSender over the Jxta network
class PeerTransportSender: public pt::PeerTransportSender
{
	public:

	PeerTransportSender();
	virtual ~PeerTransportSender();

	pt::TransportType getType();

	pt::Address::Reference createAddress( const std::string& url ) throw (pt::exception::InvalidAddress);
	pt::Address::Reference createAddress( std::map<std::string, std::string, std::less<std::string> >& address ) throw (pt::exception::InvalidAddress);

	std::string getProtocol();

	std::vector<std::string> getSupportedServices();
	bool isServiceSupported(const std::string& service);

	//! Creates or returns a JxtaMessenger to send messages to the given destination.
	//! @param destination a reference to the destination address.
	//! @param local a reference to the local address; it is ignored in this context.
	pt::Messenger::Reference getMessenger (pt::Address::Reference destination, pt::Address::Reference local) throw (pt::exception::UnknownProtocolOrService);

	private:
	
	jxta::PlatformImpl* jpl_;     // a Jxta sender NEEDS the platform, which wraps the PeerGroup

	//std::vector<jxta::JxtaMessenger*> jxtaMessengers_;
};

}

#endif
