
/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber, G. Lo Presti and L. Orsini                      *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _jxta_PeerTransportReceiver_h
#define _jxta_PeerTransportReceiver_h

#include <string>
#include <vector>

#include "pt/PeerTransportReceiver.h"
#include "pt/JxtaListener.h"
#include "jxta/Address.h"
#include "jxta/PlatformImpl.h"
#include "jxta/PipeService.h"
#include "jxta/RdvConnectionListener.h"
#include "jxta/exception/Exception.h"

namespace jxta
{

//! This class represents a XDAQ compliant PeerTransportReceiver over the Jxta network.
//! It wraps a Jxta input pipe.
//##ModelId=416A98F101F8
class PeerTransportReceiver: public pt::PeerTransportReceiver, public jxta::RdvConnectionListener
{
	public:

    //##ModelId=416A98F2025E
	PeerTransportReceiver();
    //##ModelId=416A98F2025F
	virtual ~PeerTransportReceiver();

    //##ModelId=416A98F20260
	pt::TransportType getType();

    //##ModelId=416A98F20261
	pt::Address::Reference createAddress( const std::string& url ) throw (pt::exception::InvalidAddress);
    //##ModelId=416A98F20268
	pt::Address::Reference createAddress( std::map<std::string, std::string, std::less<std::string> >& address ) throw (pt::exception::InvalidAddress);
	
    //! Adds a listener to get messages from the Jxta network.
    //##ModelId=416A98F20272
    void addServiceListener (pt::Listener* listener) throw (pt::exception::Exception);
    //##ModelId=416A98F2027C
	bool isExistingListener (std::string service);
    //##ModelId=416A98F2027E
	void removeServiceListener (pt::Listener* listener) throw (pt::exception::Exception);
    //##ModelId=416A98F20287
	void removeAllServiceListeners();

    //##ModelId=416A98F20290
	std::string getProtocol();
    //##ModelId=416A98F20291
	std::vector<std::string> getSupportedServices();
    //##ModelId=416A98F20292
	bool isServiceSupported(const std::string& service);

	//! Configures the listening address. Starts the Jxta platform if not yet done.
	//! This method can be used as a bootstrap for the Jxta platform.
	//##ModelId=416A98F2029A
    void config (pt::Address::Reference address) throw (pt::exception::Exception);

	private:
	
    //##ModelId=417B6E91012F
	void rdvConnectionEvent();

    //##ModelId=416A98F202A4
	void onMessage (Jxta_object* obj);
    //##ModelId=416A98F202AF
	friend void j_on_message_callback(Jxta_object* obj, void* arg);

    //##ModelId=416A98F2022E
	jxta::Platform* jpl_;   // a Jxta receiver NEEDS the platform, which wraps the PeerGroup
    //##ModelId=416A98F20237
	jxta::PipeService* pipeSvc_;
	
    //##ModelId=416A98F2024A
	Jxta_listener* j_listener_;
    //##ModelId=416A98F2024B
	Jxta_inputpipe* j_ip_;
    //##ModelId=416A98F20255
	pt::JxtaListener* listener_;
};

}

void j_on_message_callback(Jxta_object* obj, void* arg);

#endif
