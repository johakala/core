
/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber, G. Lo Presti and L. Orsini                      *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _jxta_PipeService_h
#define _jxta_PipeService_h

#include "jxta/exception/Exception.h"
#include "jxta/Address.h"
#include "jxta/JxtaMessenger.h"

namespace jxta
{

//! This class wraps the Jxta Pipe Service, which is responsible for the messaging over the Jxta network.
//! This interface is provided for documentation purposes only. The user should not call 
//! its methods directly, but use instead the PeerTransportReceiver and PeerTransportSender classes.
class PipeService
{
	public:
	//! Creates a JxtaMessenger to send messages over a Jxta pipe to the given address.
	//! @return a JxtaMessenger instance to send messages to the given destination.
	virtual jxta::JxtaMessenger* createMessenger(pt::Address::Reference destination) throw (jxta::exception::Exception) = 0;
};

}


#endif
