
/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber, G. Lo Presti and L. Orsini                      *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _jxta_Advertisement_h
#define _jxta_Advertisement_h

#include <string>

#include "toolbox/mem/CountingPtr.h"
#include "toolbox/mem/ThreadSafeReferenceCount.h"
#include "toolbox/mem/StandardObjectPolicy.h"

namespace jxta
{

//! This class wraps a Jxta advertisement and handles peer advs, peergroup advs and pipe advs.
class Advertisement
{
	public:
	static std::string PEER;
	static std::string GROUP;
	static std::string PIPE;
	static std::string SVC;
	
    typedef toolbox::mem::CountingPtr<Advertisement, toolbox::mem::SimpleReferenceCount, toolbox::mem::StandardObjectPolicy> Reference;
	
	virtual ~Advertisement() {};
	
	//! Returns the advertisement type. Either "jxta:PA", "jxta:PGA", "jxta:PipeAdvertisement" or "jxta:SvcAdv".
	virtual std::string getType() = 0;
	//! Returns the internal ID associated with this advertisement.
	virtual std::string getID() = 0;
	//! Returns the name associated with this advertisement, either the peer name, the peergroup name or the pipe name.
	virtual std::string getName() = 0;
	
	//! Dumps the entire XML representation of this advertisement.
	virtual std::string getXmlDocument() = 0;
};


//! Creates a module advertisement to advertise external services.
//! @param string name The name of the advertised service.
//! @param string desc An optional description of the advertised service.
jxta::Advertisement::Reference newModuleAdv(std::string name, std::string desc);

}

#endif


