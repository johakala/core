
/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber, G. Lo Presti and L. Orsini                      *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _jxta_Platform_h
#define _jxta_Platform_h

#include <string>

#include "jxta/exception/Exception.h"
#include "jxta/Address.h"
#include "jxta/PeerGroup.h"

//! if this macro is not defined, the Jxta PlatformConfig file will be regenerated each time.
#define KEEP_PLATFORM_CONFIG


namespace jxta
{

//! This class represents the Jxta platform. It's a singleton, and wraps a Jxta NetPeerGroup. @see jxta::PeerGroup.
class Platform
{
	public:
	//! Shutdown the Jxta platform. Explicitly sends a message to all connected rdvs to flush their caches.
	virtual ~Platform() {};
	
	//! Return the local IP address and the port on which the Jxta is running. The address is in the form jxta://IPAddress:port/Platform.
	virtual Address* getLocalAddress() = 0;
	//! Return the NetPeerGroup, i.e. the main group to which every peer must belong. @see jxta::PeerGroup.
	virtual PeerGroup* getNetPeerGroup() = 0;

	//! Search and join a given peer group. Throws exception if the group cannot be found on the local cache.
	//! To remotely discover a group use the Discovery Service.
	virtual PeerGroup* joinPeerGroup(std::string groupName) throw (jxta::exception::Exception) = 0;
	//! Return the PeerGroup instance which represent the given peer group.
	//! Return null if the group cannot be found.
	virtual PeerGroup* getPeerGroup(std::string groupName) = 0;
	//! Leave the given peer group. Cannot be used to leave the NetPeerGroup, because it's equivalent to shutdown the platform.
	//! To leave Jxta delete this object.
	virtual void leavePeerGroup(std::string groupName) = 0;
};


//! static function to get the Platform singleton or configure it with the provided port number if not yet done.
//! @param port the optional port number to listen for Jxta connections over TCP. By default, Jxta connection over HTTP are received on port-1.
//! Values <= 1 are not accepted. If such a value is passed, the singleton instance is returned if already initialized, otherwise NULL is returned.
//! @return the singleton Jxta Platform instance.
Platform* getPlatform(int port = 9701);

}


#endif
