
/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber, G. Lo Presti and L. Orsini                      *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _jxta_PipeServiceImpl_h
#define _jxta_PipeServiceImpl_h

#include <sstream>

#include "jxta/PipeService.h"
#include "jxta/DiscoveryService.h"
#include "jxta/PeerGroup.h"
#include "jxta/Platform.h"

#include "jxta_pipe_service.h"

namespace jxta
{

class PipeServiceImpl: public jxta::PipeService
{
	public:
	PipeServiceImpl(Jxta_pipe_service* j_pipe_svc);

	//! Publishes an input pipe and starts it in order to listen for messages from other peers.
	//! @return an implementation specific structure pointer to the input pipe.
	Jxta_inputpipe* publishInputPipe() throw (jxta::exception::Exception);

	//! Searches for an output pipe on the given address. Eventually queries the Discovery Service in order to find it.
	//! @return an implementation specific structure pointer to the output pipe.
	Jxta_outputpipe* searchOutputPipe(jxta::Address& address) throw (jxta::exception::Exception);

	jxta::JxtaMessenger* createMessenger(pt::Address::Reference destination) throw (jxta::exception::Exception);
	
	private:
	Jxta_pipe_service* j_pipe_svc_;
};

}


#endif

