
/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber, G. Lo Presti and L. Orsini                      *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _jxta_PlatformImpl_h
#define _jxta_PlatformImpl_h

#include <vector>
#include <sstream>

#include "jxta/exception/Exception.h"
#include "jxta/Platform.h"
#include "jxta/PeerGroupImpl.h"

#include "jxta/DiscoveryService.h"
#include "jxta/Advertisement.h"
#include "jxta/AdvertisementList.h"

namespace jxta
{

//##ModelId=416A98F10248
class PlatformImpl: public jxta::Platform
{
	public:

    //##ModelId=416A98F2039E
	static Platform* instance(int port);

    //##ModelId=416A98F203B2
	PlatformImpl(int port) throw (jxta::exception::Exception);
    //##ModelId=416A98F203BC
	virtual ~PlatformImpl();

    //##ModelId=416A98F203DB
	jxta::Address* getLocalAddress();
    //##ModelId=416A98F203BD
	jxta::PeerGroup* getNetPeerGroup();

    //##ModelId=416A98F203BE
	jxta::PeerGroup* joinPeerGroup(std::string groupName) throw (jxta::exception::Exception);
    //##ModelId=416A98F203C7
	jxta::PeerGroup* getPeerGroup(std::string groupName);
    //##ModelId=416A98F203D0
	void leavePeerGroup(std::string groupName);
	
	private:

	void makePlatformConfig(int tcp_port, int mcast_port, const char* peername_prefix) throw (jxta::exception::Exception);
	char* getLocalIP();

    //##ModelId=416A98F2037A
	static PlatformImpl* instance_;

    //##ModelId=416A98F20381
	jxta::PeerGroup* netPG_;
    //##ModelId=416A98F20395
	jxta::Address* localAddress_;
    //##ModelId=417B6E91016B
	std::vector<jxta::PeerGroup*> groups_;
};

}

#endif
