
/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber, G. Lo Presti and L. Orsini                      *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _jxta_RdvServiceImpl_h
#define _jxta_RdvServiceImpl_h

#include <string>

#include "jxta/RdvService.h"
#include "jxta.h"
#include "jxta_rdv_service.h"
#include "jxta_listener.h"

namespace jxta
{

//##ModelId=416A98F10266
class RdvServiceImpl: public RdvService
{
	public:

    //##ModelId=416A98F30010
	RdvServiceImpl(Jxta_rdv_service* j_rdv_svc);

    //##ModelId=416A98F30012
	void addRdvPeer(std::string rdvIPAddr);
    //##ModelId=416A98F3001B
	bool isConnectedToRdv();
	
    //##ModelId=417B6E9101A7
	void addRdvConnectionListener(RdvConnectionListener* listener) throw (jxta::exception::Exception);
    //##ModelId=417B6E9101A9
	void removeRdvConnectionListener();

	private:

    //##ModelId=417FB7410016
	void onRdvEvent(Jxta_rdv_event* event);
    //##ModelId=417FB741002A
	friend void j_on_rdv_event_callback(Jxta_rdv_event* event, void* arg);
	
    //##ModelId=416A98F30008
	Jxta_rdv_service* j_rdv_svc_;
    //##ModelId=417FB741000C
	Jxta_listener* j_listener_;
    //##ModelId=417B73C10338
	RdvConnectionListener* listener_;
};

void j_on_rdv_event_callback(Jxta_rdv_event* event, void* arg);

}

#endif

