
/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber, G. Lo Presti and L. Orsini                      *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _jxta_DiscoveryServiceImpl_h
#define _jxta_DiscoveryServiceImpl_h

#include "jxta/DiscoveryService.h"
#include "jxta/AdvertisementImpl.h"
#include "jxta/AdvertisementListImpl.h"

#include <vector>

#include "jxta.h"
#include "jxta_discovery_service.h"
#include "jxta_dr.h"
#include "jxta_listener.h"

namespace jxta
{

//##ModelId=416A98F1016C
class DiscoveryServiceImpl: public DiscoveryService
{
	public:
	
    //##ModelId=416A98F200C3
	DiscoveryServiceImpl(Jxta_discovery_service* j_disc_svc);
    //##ModelId=416A98F200C5
	virtual ~DiscoveryServiceImpl();
	
    //##ModelId=417B6E91008F
	jxta::AdvertisementList::Reference getKnownAdvertisements(int advType = 0) throw (jxta::exception::Exception);
    //##ModelId=416A98F200CF
	void searchRemoteAdvertisements(int advType = 0);
    //##ModelId=416A98F200CE
	void flushAdvertisements(int advType = 0);
	
    //##ModelId=416A98F200E2
	bool publishAdvertisement(Advertisement::Reference adv);
    //##ModelId=417B6E9100AD
	jxta::Advertisement::Reference getAdvertisementByName(std::string advType, std::string svcName, bool remote);

    //##ModelId=416A98F200D0
	void addServiceListener(jxta::DiscoveryListener* listener) throw (jxta::exception::Exception);
    //##ModelId=416A98F200D8
	void removeServiceListener(jxta::DiscoveryListener* listener);
    //##ModelId=416A98F200E1
	void removeAllServiceListeners();
	
	private:
	
    //##ModelId=416A98F200F5
	void onDiscovery(Jxta_object* obj);
    //##ModelId=416A98F200FF
	friend void j_on_discovery_callback(Jxta_object* obj, void* arg);
	
    //##ModelId=416A98F200A6
	Jxta_discovery_service* j_disc_svc_;
    //##ModelId=416A98F200AF
	Jxta_listener* j_disc_listener_;
    //##ModelId=419C78340094
	std::vector<DiscoveryListener*> discListeners_;
};

void j_on_discovery_callback(Jxta_object* obj, void* arg);

}


#endif

