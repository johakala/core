
/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber, G. Lo Presti and L. Orsini                      *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _jxta_RdvService_h
#define _jxta_RdvService_h

#include "jxta/RdvConnectionListener.h"
#include "jxta/exception/Exception.h"

namespace jxta
{

//! This class wraps the Jxta Rendezvous Service. This service is responsible to the first connection to a rdv peer.
class RdvService
{
	public:
	//! Explicitly connects to a given rendezvous peer.
	//! @param string rdvIPAddr the IP address of the remote rdv peer. If the port is not specified, the default TCP port 9701 is assumed.
	virtual void addRdvPeer(std::string rdvIPAddr) = 0;
	//! Returns the current status of the rdv connection.
	//! @return true if there is at least one rdv connection.
	virtual bool isConnectedToRdv() = 0;

	//! Adds a rdv connection listener. It's called back when a rdv connection is in place.
	virtual void addRdvConnectionListener(RdvConnectionListener* listener) throw (jxta::exception::Exception) = 0;
	//! Removes a previously registered rdv connection listener.
	virtual void removeRdvConnectionListener() = 0;
};

}


#endif
