
/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber, G. Lo Presti and L. Orsini                      *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _PeerTransportJxta_h_
#define _PeerTransportJxta_h_

#include "xdaq/Application.h"
#include "jxta/PeerTransportSender.h"
#include "jxta/PeerTransportReceiver.h"
#include "jxta/Platform.h"

#include "xgi/Utils.h"
#include "xgi/Method.h"


//! This is the XDAQ Peer Transport Appliction Wrapper for Jxta.
//! It contains the Jxta PeerTransportReceiver and PeerTransportSender, and the Platform.
//! It also includes xgi methods to control the Jxta peer via HyperDAQ.
class PeerTransportJxta : public xdaq::Application
{
	public:

	PeerTransportJxta(xdaq::ApplicationStub * s);
	~PeerTransportJxta();

	//! Shows the local published services and links to other commands.
	void serviceView(xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception);
	//! Shows the discovered peers view and links to other commands.
	//! Default method, called by the '/' URL associated with this URN. 
	void peerView(xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception);
	//! Forces a remote discovery to refresh peer view.
	void refreshPView(xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception);
	//! Shows the add rendezvous form page.
	void addRdvForm(xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception);
	//! Adds a static rendezvous as specified in input parameters.
	void addRdv(xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception);
	//! Shows the publish advertisement form page.
	void publishAdvForm(xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception);
	//! Publishes a custom Jxta module class advertisement (MCA) as specified in input parameters.
	void publishAdv(xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception);

	private:

	void displayPeers(xgi::Output& out);
	void displayServices(xgi::Output& out);
	void printConfirmMessage(xgi::Output& out, std::string message, std::string refresh = "");
	void printPageFooter(xgi::Output& out);

	jxta::PeerTransportSender* pts_;
	jxta::PeerTransportReceiver* ptr_;
	jxta::Platform* jpl_;
	jxta::PeerGroup* netPG_;
	
	std::string localUrn_;
};

#endif
