
/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber, G. Lo Presti and L. Orsini                      *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include "jxta/DiscoveryServiceImpl.h"


// static initialization
int jxta::DiscoveryService::PEER = DISC_PEER;
int jxta::DiscoveryService::GROUP = DISC_GROUP;
int jxta::DiscoveryService::ADV = DISC_ADV;


//##ModelId=416A98F200C3
jxta::DiscoveryServiceImpl::DiscoveryServiceImpl(Jxta_discovery_service* j_disc_svc): j_disc_svc_(j_disc_svc)
{
	discListeners_.clear();
}

//##ModelId=416A98F200C5
jxta::DiscoveryServiceImpl::~DiscoveryServiceImpl()
{
	removeAllServiceListeners();
}


//##ModelId=417B6E91008F
jxta::AdvertisementList::Reference jxta::DiscoveryServiceImpl::getKnownAdvertisements(int advType) throw (jxta::exception::Exception)
{
	Jxta_vector* res_vec;
	
	discovery_service_get_local_advertisements(j_disc_svc_, advType /* DISC_PEER, DISC_GROUP, DISC_ADV */, NULL, NULL, &res_vec);
	return AdvertisementList::Reference(new AdvertisementListImpl(res_vec));
}

//##ModelId=416A98F200CF
void jxta::DiscoveryServiceImpl::searchRemoteAdvertisements(int advType)
{
	// broadcasts a discovery query
	discovery_service_get_remote_advertisements(j_disc_svc_, NULL /* propagate */, advType, "", "", 10000 /*responses*/, (Jxta_discovery_listener*) j_disc_listener_);
}

//##ModelId=416A98F200CE
void jxta::DiscoveryServiceImpl::flushAdvertisements(int advType)
{
	discovery_service_flush_advertisements(j_disc_svc_, NULL, advType);
}

//##ModelId=416A98F200E2
bool jxta::DiscoveryServiceImpl::publishAdvertisement(Advertisement::Reference adv)
{
	Jxta_status res;
	res = discovery_service_publish(j_disc_svc_, ((AdvertisementImpl&)(*adv)).getJxtaAdv(), DISC_ADV, (long)DEFAULT_LIFETIME, (long)DEFAULT_EXPIRATION);
	res = discovery_service_remote_publish(j_disc_svc_, NULL, ((AdvertisementImpl&)(*adv)).getJxtaAdv(), DISC_ADV, (long)DEFAULT_EXPIRATION);

	return (res == JXTA_SUCCESS);
}


//##ModelId=417B6E9100AD
jxta::Advertisement::Reference jxta::DiscoveryServiceImpl::getAdvertisementByName(std::string advType, std::string svcName, bool remote)
{
	Jxta_status res;
	Jxta_listener* listener = jxta_listener_new(NULL, NULL, 1, 10);
	Jxta_vector* responses = NULL;
	Jxta_DiscoveryResponseElement* element = NULL;
	Jxta_DiscoveryResponse* dr = NULL;

	// check first the local cache
	discovery_service_get_local_advertisements(j_disc_svc_, DISC_ADV, "Name", (char *)svcName.c_str(), &responses);
	
	if (responses == NULL) {
		if(!remote)
			return Advertisement::Reference(0);
		// else search remotely
		jxta_listener_start(listener);
		discovery_service_get_remote_advertisements(j_disc_svc_, NULL, DISC_ADV, "Name", (char *)svcName.c_str(), 1, (Jxta_discovery_listener*) listener);
		res = jxta_listener_wait_for_event(listener, 6*1000*1000 /* timeout */, (Jxta_object**) &dr);
		
		if (res != JXTA_SUCCESS) {
		  JXTA_OBJECT_RELEASE (listener);
		  return Advertisement::Reference(0);
		}
		
		jxta_discovery_response_get_responses(dr, &responses);
		JXTA_OBJECT_RELEASE(listener);
		JXTA_OBJECT_RELEASE(dr);
		if (responses == NULL)
		  return Advertisement::Reference(0);
		
		res = jxta_vector_get_object_at (responses, (Jxta_object**) &element, 0);   // get only the first one
		JXTA_OBJECT_RELEASE(responses);
		if (res != JXTA_SUCCESS)
		  return Advertisement::Reference(0);
		
		// Builds the advertisement
		Advertisement* adv = new AdvertisementImpl(advType, jstring_get_string (element->response));
		JXTA_OBJECT_RELEASE(responses);
		JXTA_OBJECT_RELEASE(element);
		return Advertisement::Reference(adv);
	}
	else {
		Jxta_advertisement* jadv;
		res = jxta_vector_get_object_at(responses, (Jxta_object**) &jadv, 0);    
		JXTA_OBJECT_RELEASE(responses);
		if (res == JXTA_SUCCESS)
			return Advertisement::Reference(new AdvertisementImpl(jadv));
		else
			return Advertisement::Reference(0);
	}
}


//##ModelId=416A98F200D0
void jxta::DiscoveryServiceImpl::addServiceListener (jxta::DiscoveryListener* listener) throw (jxta::exception::Exception)
{
	if(discListeners_.empty()) {
		j_disc_listener_ = jxta_listener_new( (Jxta_listener_func) j_on_discovery_callback, this, 1 /* max # of concurrent invokes */, 100 /* queue length */);
		if(j_disc_listener_ == 0) {
			XCEPT_RAISE (jxta::exception::Exception, "Cannot create the Jxta discovery listener");
		}
		jxta_listener_start(j_disc_listener_);
	}
	discListeners_.push_back(listener);
	
	discovery_service_get_remote_advertisements(j_disc_svc_, NULL, DISC_PEER, "", "", 10000 /*responses*/, (Jxta_discovery_listener*) j_disc_listener_);
}

//##ModelId=416A98F200D8
void jxta::DiscoveryServiceImpl::removeServiceListener (jxta::DiscoveryListener* listener) 
{
	std::vector<jxta::DiscoveryListener*>::iterator it;
	for (it = discListeners_.begin(); it != discListeners_.end(); it++)
		if (*it == listener) {
			discListeners_.erase(it);
			break;   // need to stop loop since erase() affects the sequence
		}
	//if(discListeners_.empty())
	//	removeAllServiceListeners();
}

//##ModelId=416A98F200E1
void jxta::DiscoveryServiceImpl::removeAllServiceListeners()
{
	discListeners_.clear();
	discovery_service_remove_discovery_listener(j_disc_svc_, (Jxta_discovery_listener*) j_disc_listener_);
	JXTA_OBJECT_RELEASE(j_disc_listener_);
}


//##ModelId=416A98F200F5
void jxta::DiscoveryServiceImpl::onDiscovery(Jxta_object* obj)
{
	Jxta_vector* vec = 0;
	jxta_discovery_response_get_advertisements((Jxta_DiscoveryResponse*)obj, &vec);
	AdvertisementListImpl* advList = new AdvertisementListImpl(vec);
	unsigned int s = discListeners_.size();
	for (unsigned int i = 0; i < s; i++) {   // cannot use iterator to allow the callee to remove itself without consequences
		discListeners_[i]->discoveryEvent(AdvertisementList::Reference(advList));
		if(discListeners_.size() != s) {        // the callee removed itself, update counter
			s = discListeners_.size();
			i--;
		}
	}
}

void jxta::j_on_discovery_callback(Jxta_object* obj, void* arg)
{
	((jxta::DiscoveryServiceImpl*)arg)->onDiscovery(obj);
}

