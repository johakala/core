
/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber, G. Lo Presti and L. Orsini                      *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include <iostream>
#include <sstream>

#include "jxta/PeerTransportReceiver.h"
#include "jxta.h"
#include "jxta_pipe_service.h"
#include "jxta_listener.h"


//##ModelId=416A98F2025E
jxta::PeerTransportReceiver::PeerTransportReceiver()
{
	j_listener_ = 0;
	listener_ = 0;
	jpl_ = 0;
	pipeSvc_ = 0;
}

//##ModelId=416A98F2025F
jxta::PeerTransportReceiver::~PeerTransportReceiver()
{
	removeAllServiceListeners();
}

//##ModelId=416A98F20260
pt::TransportType jxta::PeerTransportReceiver::getType()
{
	return pt::Receiver;
}

//##ModelId=416A98F20261
pt::Address::Reference jxta::PeerTransportReceiver::createAddress( const std::string& url ) throw (pt::exception::InvalidAddress)
{
	return pt::Address::Reference(new jxta::Address(url));
}

//##ModelId=416A98F20268
pt::Address::Reference jxta::PeerTransportReceiver::createAddress( std::map<std::string, std::string, std::less<std::string> >& address ) throw (pt::exception::InvalidAddress)
{
	std::string protocol = address["protocol"];

	if (protocol == "jxta")
	{
		std::string url = protocol;
		url += "://";
		url += address["hostname"];
		url += ":";
		url += address["port"];

		std::string service = address["service"];
		if (service != "")
		{
			url += "/";
			url += service;
		}

		return this->createAddress(url);
	}
	else
	{
		XCEPT_RAISE (jxta::exception::Exception, "Protocol " + protocol + " not supported.");
	}
}

//##ModelId=416A98F20290
std::string jxta::PeerTransportReceiver::getProtocol()
{
	return "jxta";
}

//##ModelId=416A98F20291
std::vector<std::string> jxta::PeerTransportReceiver::getSupportedServices()
{
	std::vector<std::string> s;
	s.push_back("pipe");
	return s;
}

//##ModelId=416A98F20292
bool jxta::PeerTransportReceiver::isServiceSupported(const std::string& service)
{
	if (service == "pipe") return true;
	else return false;
}

//##ModelId=416A98F2029A
void jxta::PeerTransportReceiver::config(pt::Address::Reference address) throw (pt::exception::Exception)
{
	// get the platform and create it if not yet done
	if(jpl_ == 0)
	{
		jpl_ = jxta::getPlatform((dynamic_cast<jxta::Address&>(*address)).getPort());
		pipeSvc_ = jpl_->getNetPeerGroup()->getPipeService();
	}
	
	// attempt to add listener if present
	if ( isExistingListener("pipe") )
	{
		// I can safely get it now
		pt::JxtaListener* l = listener_;
		addServiceListener(l);
	}
}

//##ModelId=416A98F2027C
bool jxta::PeerTransportReceiver::isExistingListener (std::string service)
{
	return (listener_ != 0 && listener_->getService() == service);
}

//##ModelId=416A98F202A4
void jxta::PeerTransportReceiver::onMessage(Jxta_object* obj)
{
	Jxta_message* msg = (Jxta_message*) obj;
	Jxta_message_element* el = NULL;
	std::string message;

	jxta_message_get_element_1(msg, "JxtaXDAQMsg", &el);

	if (el) {
		Jxta_bytevector* val = jxta_message_element_get_value(el);
		JString* m = jstring_new_3(val);
		message = jstring_get_string(m);

		listener_->processIncomingMessage(message);

		JXTA_OBJECT_RELEASE(val);
		JXTA_OBJECT_RELEASE(m);
	}
	JXTA_OBJECT_RELEASE(el);
	JXTA_OBJECT_RELEASE(msg);
}

//##ModelId=416A98F20272
void jxta::PeerTransportReceiver::addServiceListener (pt::Listener* listener) throw (pt::exception::Exception)
{
	if (jpl_ == 0 || (jpl_->getNetPeerGroup() != 0 && !jpl_->getNetPeerGroup()->getRdvService()->isConnectedToRdv())) {
		listener_ = dynamic_cast<pt::JxtaListener *>(listener);     // save the reference for next usage
		if(jpl_ != 0)
			jpl_->getNetPeerGroup()->getRdvService()->addRdvConnectionListener(this);
		return;
	}
	if(j_listener_ != NULL) {
		#warning "should we register the new listener overriding the previous one? should we support vector of listeners?"
		XCEPT_RAISE (jxta::exception::Exception, "Jxta listener already registered");
		return;
	}

	if (listener->getService() == "pipe") {
		//if(!jpl_->getNetPeerGroup()->getRdvService()->isConnectedToRdv()) {
		//	XCEPT_RAISE(pt::exception::Exception, "Cannot send messages over the Jxta network until a Rendezvous peer has not been contacted.");
		//}
		listener_ = dynamic_cast<pt::JxtaListener *>(listener);
		
		j_listener_ = jxta_listener_new( (Jxta_listener_func) j_on_message_callback, this, 1 /* max # of concurrent invokes */, 100 /* queue length */);
		if (j_listener_ == NULL) {
			listener_ = 0;
			XCEPT_RAISE (jxta::exception::Exception, "Cannot create jxta_listener");
		}
		
		if(j_ip_ == NULL)
			j_ip_ = ((PipeServiceImpl*)pipeSvc_)->publishInputPipe();

		if (jxta_inputpipe_add_listener (j_ip_, j_listener_) != JXTA_SUCCESS) {
		//if (jxta_endpoint_service_add_listener(jpl_->getEPService(), "xdaq-ep", "x", j_listener_) != JXTA_SUCCESS) {
			listener_ = 0;
			JXTA_OBJECT_RELEASE(j_listener_);
			XCEPT_RAISE (jxta::exception::Exception, "Cannot add jxta_listener");
		}
		jxta_listener_start (j_listener_);
	}
}

//##ModelId=416A98F2027E
void jxta::PeerTransportReceiver::removeServiceListener(pt::Listener* listener) throw (pt::exception::Exception)
{
	if (j_listener_ == 0)
		return;     // nothing to do, assuming that listener and j_listener_ work as twin

	if (listener->getService() == "pipe" && listener == listener_)
	{
		jxta_inputpipe_remove_listener(j_ip_, j_listener_);
		//jxta_endpoint_service_remove_listener(jpl_->getEPService(), "jxta:EP", "EPParam");
		listener_ = 0;
		JXTA_OBJECT_RELEASE(j_listener_);
	}
}

//##ModelId=416A98F20287
void jxta::PeerTransportReceiver::removeAllServiceListeners()
{
	removeServiceListener(listener_);
}

//##ModelId=417B6E91012F
//this listener is used to registered a pipe listener AFTER a rdv connection has been established
void jxta::PeerTransportReceiver::rdvConnectionEvent()
{
	addServiceListener(listener_);
	jpl_->getNetPeerGroup()->getRdvService()->removeRdvConnectionListener();
}

void jxta::j_on_message_callback(Jxta_object* obj, void* arg)
{
	((jxta::PeerTransportReceiver*)arg)->onMessage(obj);
}


