
/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber, G. Lo Presti and L. Orsini                      *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include "jxta/RdvServiceImpl.h"


//##ModelId=416A98F30010
jxta::RdvServiceImpl::RdvServiceImpl(Jxta_rdv_service* j_rdv_svc): j_rdv_svc_(j_rdv_svc)
{
	listener_ = 0;
}


//##ModelId=416A98F30012
void jxta::RdvServiceImpl::addRdvPeer(std::string rdvIPAddr)
{
	if(rdvIPAddr.find(":") == std::string::npos)
		rdvIPAddr += ":9701";     // assume that rdv peers use the default Jxta port
	printf("RdvServiceImpl::addRdvPeer: getting rdv at %s\n", rdvIPAddr.c_str());
	Jxta_endpoint_address* addr = jxta_endpoint_address_new2("tcp", (char *)rdvIPAddr.c_str(), NULL, NULL);
	Jxta_peer* rpeer = jxta_peer_new();
	jxta_peer_set_address(rpeer, addr);
	jxta_rdv_service_add_peer(j_rdv_svc_, rpeer);   // ignores the result as the rdv connection is made asynchronously - check isConnectedToRdv()

	JXTA_OBJECT_RELEASE(addr);
	JXTA_OBJECT_RELEASE(rpeer);
}


//##ModelId=416A98F3001B
bool jxta::RdvServiceImpl::isConnectedToRdv()
{
    Jxta_vector* peers = NULL;
	Jxta_peer* peer;
    Jxta_status res = jxta_rdv_service_get_peers(j_rdv_svc_, &peers);
    if (res != JXTA_SUCCESS)
		return false;
    for (int i = 0; i < jxta_vector_size(peers); i++) {
        res = jxta_vector_get_object_at(peers, (Jxta_object**) &peer, i);
		if(res != JXTA_SUCCESS)
			continue;
		JXTA_OBJECT_CHECK_VALID(peer);
		if(jxta_rdv_service_peer_is_connected(j_rdv_svc_, peer)) {
			JXTA_OBJECT_RELEASE(peers);
			return true;       // fine, we've got at least one connection to a rdv
		}
	}
	JXTA_OBJECT_RELEASE(peers);
	return false;
}


//##ModelId=417B6E9101A7
void jxta::RdvServiceImpl::addRdvConnectionListener(RdvConnectionListener* listener) throw (jxta::exception::Exception)
{
	listener_ = listener;
	if(j_listener_ != 0)
		return;     // the listener is already registered, this way the previous one is discarded
	j_listener_ = jxta_listener_new( (Jxta_listener_func) j_on_rdv_event_callback, this, 1 /* max # of concurrent invokes */, 100 /* queue length */);
	if (j_listener_ == 0) {
		listener_ = 0;
		XCEPT_RAISE (jxta::exception::Exception, "Cannot create a Jxta rdv listener");
	}
	jxta_listener_start(j_listener_);

	if(jxta_rdv_service_add_event_listener(j_rdv_svc_, "JxtaXDAQ", "", (Jxta_rdv_event_listener*)j_listener_) != JXTA_SUCCESS) {
		JXTA_OBJECT_RELEASE(j_listener_);
		listener_ = 0;
		XCEPT_RAISE (jxta::exception::Exception, "Cannot start the Jxta rdv listener");
	}
}

//##ModelId=417B6E9101A9
void jxta::RdvServiceImpl::removeRdvConnectionListener()
{
	jxta_rdv_service_remove_event_listener(j_rdv_svc_, "JxtaXDAQ", "");
	JXTA_OBJECT_RELEASE(j_listener_);
	listener_ = 0;
}


//##ModelId=417FB7410016
void jxta::RdvServiceImpl::onRdvEvent(Jxta_rdv_event* event)
{
	if(listener_ != 0 && event->event == JXTA_RDV_CONNECTED)
		listener_->rdvConnectionEvent();
	//else if(event->event == JXTA_RDV_xxx) ...
}

void jxta::j_on_rdv_event_callback(Jxta_rdv_event* event, void* arg)
{
	((jxta::RdvServiceImpl*)arg)->onRdvEvent(event);
}

