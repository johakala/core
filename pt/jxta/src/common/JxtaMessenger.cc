
/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber, G. Lo Presti and L. Orsini                      *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include "jxta/JxtaMessenger.h"
#include "jxta_errno.h"

jxta::JxtaMessenger::JxtaMessenger(pt::Address::Reference destination, Jxta_outputpipe* j_op): j_op_(j_op), destination_(destination)
{
	JXTA_OBJECT_CHECK_VALID (j_op_);
	//destination_ = dynamic_cast<jxta::Address*>(destination);
	//local_ = dynamic_cast<jxta::Address*>(local);
}

jxta::JxtaMessenger::~JxtaMessenger()
{
	JXTA_OBJECT_RELEASE(j_op_);
	//if(local_ != destination_) delete local_;
	//delete destination_;
}

pt::Address::Reference jxta::JxtaMessenger::getDestinationAddress()
{
	return destination_;
}

void jxta::JxtaMessenger::send(std::string message) throw (pt::exception::Exception)
{
	send("JxtaXDAQMsg", message);    // this is the only message "type" exposed to the user
}

void jxta::JxtaMessenger::send(std::string tagName, std::string message) throw (pt::exception::Exception)
{
	Jxta_message* msg = jxta_message_new();
	Jxta_message_element* el = NULL;
	Jxta_status res;

	el = jxta_message_element_new_1((char *)tagName.c_str(), "text/xml", (char *)message.c_str(), message.length(), NULL);
	jxta_message_add_element(msg, el);
	JXTA_OBJECT_RELEASE(el);

    // sends the message
    //res = jxta_endpoint_service_send(jpl_->getPeerGroup(), jpl_->getEPService(), msg, destinationEP_);
	res = jxta_outputpipe_send (j_op_, msg);
  	if (res != JXTA_SUCCESS) {
		XCEPT_RAISE(pt::exception::Exception, "Could not send Jxta message");
	}
}

