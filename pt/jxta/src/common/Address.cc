
/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber, G. Lo Presti and L. Orsini                      *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include <iostream>
#include <sstream>

#include "jxta/Address.h"
#include "toolbox/net/URL.h"
#include "toolbox/net/Utils.h"
#include "toolbox/exception/Exception.h"


// only for debugging inet_ntoa
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>


jxta::Address::Address(const std::string& url) throw (pt::exception::InvalidAddress) 
{
	try {
		url_ = new toolbox::net::URL(url);
	}
	catch (toolbox::net::exception::MalformedURL& mfu) {
		XCEPT_RETHROW (pt::exception::InvalidAddress, "Cannot create jxta address", mfu);
	}
	
	if ((url_->getProtocol()) != "jxta")
	{
		std::string msg = "Cannot create jxta address from url ";
		msg += url;
		msg += ", unsupported protocol";
		XCEPT_RAISE (pt::exception::InvalidAddress, msg.c_str());
	}
}

jxta::Address::~Address()
{
	delete url_;
}


int jxta::Address::getPort()
{
	return url_->getPort();
}

std::string jxta::Address::getHost()
{
	return url_->getHost();
}

std::string jxta::Address::getPipeName()
{
    std::ostringstream o;
	o << "jxtapipe:" << url_->getHost();
    if(url_->getPort() > 0)
        o << ":" << url_->getPort();
    return o.str();
}


//! The following methods are taken from the http::Address class

std::string jxta::Address::getService()
{
	// May require a trimming, e.g. jxta://xxx:xxx/soap/something_else
	return url_->getPath();
}

std::string jxta::Address::getProtocol()
{
	return url_->getProtocol();
}

std::string jxta::Address::toString()
{
	return url_->toString();
}
/*
std::string jxta::Address::getPort()
{
    std::ostringstream o;
    if(url_->getPort() > 0)
        o <<  url_->getPort();
    return o.str();
}*/

std::string jxta::Address::getServiceParameters()
{
	return url_->getPath();
}

bool jxta::Address::equals(pt::Address::Reference address)
{
	return (this->toString() == address->toString());
}

