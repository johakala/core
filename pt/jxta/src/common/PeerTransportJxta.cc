
/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber, G. Lo Presti and L. Orsini                      *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include <string.h>

#include "pt/PeerTransportAgent.h"
#include "jxta/PeerTransportJxta.h"
#include "jxta/DiscoveryService.h"
#include "jxta/RdvService.h"

#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"


//! This defines the offset between the XDAQ web interface port number and the JxtaPT TCP transport.
//! The JxtaPT HTTP transport uses the same offset + 1.
#define PORT_OFFSET 999


PeerTransportJxta::PeerTransportJxta(xdaq::ApplicationStub * s): xdaq::Application(s)
{
	try {
		// start up the Platform with a port dependant on the XDAQ HTTP port
		std::string xdaqUrl = s->getContext()->getContextDescriptor()->getURL();
		jpl_ = jxta::getPlatform(atoi(xdaqUrl.substr(xdaqUrl.find(":", 5)+1).c_str()) - PORT_OFFSET);
	}
	catch (jxta::exception::Exception &all) {
		XCEPT_RETHROW (jxta::exception::Exception, "Exception starting JxtaPT", all);
	}
	netPG_ = jpl_->getNetPeerGroup();
	
	pts_ = new jxta::PeerTransportSender();
	ptr_ = new jxta::PeerTransportReceiver();
	pt::PeerTransportAgent* pta = pt::getPeerTransportAgent();
	pta->addPeerTransport(pts_);
	pta->addPeerTransport(ptr_);
	std::cout << "Work loop name: JxtaPT\n";
	
	xgi::bind(this, &PeerTransportJxta::peerView, "Default");
	xgi::bind(this, &PeerTransportJxta::refreshPView, "refresh");
	xgi::bind(this, &PeerTransportJxta::addRdvForm, "addRdvForm");
	xgi::bind(this, &PeerTransportJxta::addRdv, "addRdv");
	xgi::bind(this, &PeerTransportJxta::serviceView, "svc");
	xgi::bind(this, &PeerTransportJxta::publishAdvForm, "publishAdvForm");
	xgi::bind(this, &PeerTransportJxta::publishAdv, "publishAdv");

	localUrn_ = "/" + getApplicationDescriptor()->getURN();
}

PeerTransportJxta::~PeerTransportJxta()
{
	pt::PeerTransportAgent* pta = pt::getPeerTransportAgent();
	pta->removePeerTransport(pts_);
	pta->removePeerTransport(ptr_);
	delete pts_;
	delete ptr_;
	delete jpl_;
}


void PeerTransportJxta::serviceView(xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	*out << cgicc::HTTPHTMLHeader();
	*out << cgicc::HTMLDoctype(cgicc::HTMLDoctype::eStrict) << std::endl;
	*out << cgicc::html().set("lang", "en").set("dir","ltr") << std::endl;
	*out << cgicc::title("XDAQ Jxta Peer Transport") << std::endl;
	
	xgi::Utils::getPageHeader(*out, "Jxta PeerTransport Home");
	
	displayServices(*out);

	*out << "<p align=center>Rendezvous connection: <b><font color=";
	*out << (netPG_->getRdvService()->isConnectedToRdv() ? "#00C000>ok" : "#C00000>not yet established") << "</font></b></p>" << std::endl;

	*out << "<li>" << cgicc::a("Go to peer view").set("href", localUrn_ + "/") << "</li><p>" << std::endl;
	*out << "<li>" << cgicc::a("Publish service").set("href", localUrn_ + "/publishAdvForm") << "</li><p>" << std::endl;
	*out << "<li>" << cgicc::a("Add static rendezvous peer").set("href", localUrn_ + "/addRdvForm") << "</li><p>" << std::endl;

	*out << "<br><br>" << std::endl;
	printPageFooter(*out);
}


void PeerTransportJxta::peerView(xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	*out << cgicc::HTTPHTMLHeader();
	*out << cgicc::HTMLDoctype(cgicc::HTMLDoctype::eStrict) << std::endl;
	*out << cgicc::html().set("lang", "en").set("dir","ltr") << std::endl;
	*out << cgicc::title("XDAQ Jxta Peer Transport") << std::endl;
	*out << "<meta http-equiv=\"Refresh\" content=\"120; URL='" << localUrn_ << "'\"></head>";
	
	xgi::Utils::getPageHeader(*out, "Jxta PeerTransport Home");
	
	displayPeers(*out);

	*out << "<p align=center>Rendezvous connection: <b><font color=";
	*out << (netPG_->getRdvService()->isConnectedToRdv() ? "#00C000>ok" : "#C00000>not yet established") << "</font></b></p>" << std::endl;

	*out << "<li>" << cgicc::a("Go to local services view").set("href", localUrn_ + "/svc") << "</li><p>" << std::endl;
	*out << "<li>" << cgicc::a("Refresh peer view").set("href", localUrn_ + "/refresh") << "</li><p>" << std::endl;
	*out << "<li>" << cgicc::a("Add static rendezvous peer").set("href", localUrn_ + "/addRdvForm") << "</li><p>" << std::endl;

	*out << "<br><br>" << std::endl;
	printPageFooter(*out);
}


void PeerTransportJxta::refreshPView(xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	jxta::DiscoveryService* discSvc = netPG_->getDiscoveryService();  
	discSvc->flushAdvertisements();
	discSvc->searchRemoteAdvertisements(jxta::DiscoveryService::PEER);
	//discSvc->searchRemoteAdvertisements(jxta::DiscoveryService::GROUP);

	printConfirmMessage(*out, "Refreshing local advertisement cache...", "1");
}


void PeerTransportJxta::addRdvForm(xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	*out << cgicc::HTTPHTMLHeader();
	*out << cgicc::HTMLDoctype(cgicc::HTMLDoctype::eStrict) << std::endl;
	*out << cgicc::html().set("lang", "en").set("dir","ltr") << std::endl;
	*out << cgicc::title("XDAQ Jxta PeerTransport - Add static rdv peer") << std::endl;

	*out << cgicc::h3("Add a static rendezvous peer").set("style", "font-family: arial").set("align", "center") << std::endl;
		 
	*out << cgicc::form().set("method","post")
			.set("action", localUrn_ + "/addRdv")
			.set("enctype","multipart/form-data") << std::endl;

	*out << "<p align=center>" << std::endl;
	*out << cgicc::label("IP address: ") << std::endl;
	*out << cgicc::input().set("type","text")
				.set("name","rdvAddress")
				.set("size","16")
				.set("value","")  << std::endl;
	
	*out << "<p align=center>" << std::endl;
	*out << cgicc::input().set("type", "submit")
	 			.set("name", "submit")
				.set("value", "Submit");
				
	*out << cgicc::form() << std::endl;
	*out << "<br><br>" << std::endl;
	printPageFooter(*out);
}

void PeerTransportJxta::addRdv(xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	cgicc::Cgicc cgi(in);
	std::string rdv = cgi["rdvAddress"]->getValue();
	if(rdv.length() > 0)
		netPG_->getRdvService()->addRdvPeer(rdv);

	printConfirmMessage(*out, "Rendezvous peer added successfully.", "");
	
	*out << "<p align=center>" << cgicc::a("Refresh view").set("href", localUrn_ + "/refresh") << "</p>" << std::endl;
}


void PeerTransportJxta::publishAdvForm(xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	*out << cgicc::HTTPHTMLHeader();
	*out << cgicc::HTMLDoctype(cgicc::HTMLDoctype::eStrict) << std::endl;
	*out << cgicc::html().set("lang", "en").set("dir","ltr") << std::endl;
	*out << cgicc::title("XDAQ Jxta PeerTransport - Publish service advertisement") << std::endl;
	
	*out << cgicc::h3("Publish service advertisement").set("style", "font-family: arial").set("align", "center") << std::endl;
	
	*out << cgicc::form().set("method","post")
			.set("action", localUrn_ + "/publishAdv")
			.set("enctype","multipart/form-data") << std::endl;
	
	*out << "<p align=center>" << std::endl;
	*out << cgicc::label("Service identifier (without spaces): ") << std::endl;
	*out << cgicc::input().set("type","text")
				.set("name","name")
				.set("size","16")
				.set("value","")  << std::endl;
	/*
	*out << "<p align=center>" << std::endl;
	*out << cgicc::label("Service description: ") << std::endl;
	*out << cgicc::input().set("type","text")
				.set("name","desc")
				.set("size","40")
				.set("value","")  << std::endl;
	*/
	*out << "<p align=center>" << std::endl;
	*out << cgicc::input().set("type", "submit")
	 			.set("name", "submit")
				.set("value", "Submit");
				
	*out << cgicc::form() << std::endl;
	*out << "<br><br>" << std::endl;
	printPageFooter(*out);
}

void PeerTransportJxta::publishAdv(xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	cgicc::Cgicc cgi(in);
	std::string n = cgi["name"]->getValue();
	//std::string d = cgi["desc"]->getValue();

	jxta::Advertisement::Reference adv = jxta::newModuleAdv(n, "");
	netPG_->getDiscoveryService()->publishAdvertisement(adv);
	
	printConfirmMessage(*out, "Service advertisement '" + adv->getName() + "' added successfully.", "");	

	*out << "<p align=center>" << cgicc::a("Refresh view").set("href", localUrn_ + "/svc") << "</p>" << std::endl;
}


/*
void PeerTransportJxta::shutdown(xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	*out << cgicc::HTTPHTMLHeader();
	*out << cgicc::HTMLDoctype(cgicc::HTMLDoctype::eStrict) << std::endl;
	*out << cgicc::title("XDAQ Jxta Peer Transport") << std::endl;
	*out << "<meta http-equiv=\"Refresh\" content=\"0; URL='/'\"></head>";
	//TODO we should detach this from our Context!
	delete this;
}
*/

void PeerTransportJxta::displayPeers(xgi::Output& out)
{
	out << cgicc::h3("Discovered Jxta peers list").set("style", "font-family: arial") << std::endl;
		
	jxta::DiscoveryService* discSvc = netPG_->getDiscoveryService();
	jxta::AdvertisementList::Reference advList = discSvc->getKnownAdvertisements(jxta::DiscoveryService::PEER);
	if(advList->getLength() == 0) {
			out << "No advertisements retrieved\n";
			return;
	}

	out << cgicc::table()
			.set("cellpadding","5")
			.set("cellspacing","0")
			.set("border","")
			.set("style","text-align: left; width: 100%; background-color: rgb(255,255,255);");

	out << cgicc::tbody();

	// Write the header line: Peer IP, Peer type, ...
	out << cgicc::tr();
	out << cgicc::td("Peer IP").set("style","vertical-align: top; font-weight: bold;");
	out << cgicc::td("Type").set("style","vertical-align: top; font-weight: bold;");
	out << cgicc::tr() << std::endl;

	int xdaqPort = jpl_->getLocalAddress()->getPort() + PORT_OFFSET;    // calculate the XDAQ HTTP port from the Jxta port 
	
	for (int i = 0; i < advList->getLength(); i++ ) {
		jxta::Advertisement::Reference adv = advList->getItem(i);
		std::string name = adv->getName();
		std::string type = name.substr(0, name.find('@'));
		name = name.substr(name.find('@')+1);
		out << cgicc::tr();
		
		std::ostringstream peerlink;
		if(type == "xdaq") {
			peerlink << "<a href=\"http://" << name << ":";
			peerlink << xdaqPort << localUrn_;   // assume that all peers have the same JxtaPT app. descriptor
			peerlink << "\">" << name << "</a>";
		}
		else if(type == "rdv") {
			peerlink << "<a href=\"http://" << name << ":9780/\">";   // all xdaq rdv peers provide web interface on port 9780
			peerlink << name << "</a>";
		}
		else
			peerlink << name;    // other peers (for instance netmappers) does not have a HyperDAQ interface
		out << cgicc::td( peerlink.str() ).set("style","vertical-align: top; font-weight: bold;");

		out << cgicc::td( type ).set("style","vertical-align: top;");
		out << cgicc::tr() << std::endl;
	}

	out << cgicc::tbody();
	out << cgicc::table();
	out << "</p>";
}


void PeerTransportJxta::displayServices(xgi::Output& out)
{
	out << cgicc::h3("Local services published as Jxta advertisements").set("style", "font-family: arial") << std::endl;
		
	jxta::DiscoveryService* discSvc = netPG_->getDiscoveryService();
	jxta::AdvertisementList::Reference advList = discSvc->getKnownAdvertisements(jxta::DiscoveryService::ADV);
	for (int i = 0; i < advList->getLength(); i++ ) {         // filter out only SVC advertisements
		jxta::Advertisement::Reference adv = advList->getItem(i);
		if(adv->getType() != jxta::Advertisement::SVC)
			advList->removeItem(i--);
	}
	if(advList->getLength() == 0) {
			out << "No advertisements retrieved\n";
			return;
	}

	out << cgicc::table()
			.set("cellpadding","5")
			.set("cellspacing","0")
			.set("border","")
			.set("style","text-align: left; width: 100%; background-color: rgb(255,255,255);");

	out << cgicc::tbody();

	// Write the header line: Service name, timestamp, ...
	out << cgicc::tr();
	out << cgicc::td("Service name").set("style","vertical-align: top; font-weight: bold;");
	out << cgicc::td("Published since").set("style","vertical-align: top; font-weight: bold;");
	out << cgicc::tr() << std::endl;

	for (int i = 0; i < advList->getLength(); i++ ) {
		jxta::Advertisement::Reference adv = advList->getItem(i);
		std::string name = adv->getName();
		std::string ts = "TBI";
		out << cgicc::tr();
		out << cgicc::td( name ).set("style","vertical-align: top;");

		out << cgicc::td( ts ).set("style","vertical-align: top;");
		out << cgicc::tr() << std::endl;
	}

	out << cgicc::tbody();
	out << cgicc::table();
	out << "</p>";
}


void PeerTransportJxta::printConfirmMessage(xgi::Output& out, std::string message, std::string refresh)
{
	out << cgicc::HTTPHTMLHeader();
	out << cgicc::HTMLDoctype(cgicc::HTMLDoctype::eStrict) << std::endl;
	out << cgicc::title("XDAQ Jxta Peer Transport") << std::endl;
	if(refresh.length() > 0)
		out << "<meta http-equiv=\"Refresh\" content=\"" << refresh << "; URL='" << localUrn_ << "'\"></head>";

	out << "<body><br><p align=center><font face=Arial,Helvetica><b>" << message << "<b></font></p>";
}

void PeerTransportJxta::printPageFooter(xgi::Output& out)
{
	out << "<center>" << std::endl;
	out << cgicc::hr();
	out << cgicc::div().set("id","footer") << std::endl;	
	out << cgicc::br() << std::endl;
	out << cgicc::div() << std::endl;
	out << "<small style=\"font-family: helvetica,arial,sans-serif;\">Authors: ";
	out << "<a href=\"mailto:Johannes.Gutleber@cern.ch\">Johannes Gutleber</a>, <a href=\"mailto:Giuseppe.LoPresti@cern.ch\">Giuseppe Lo Presti</a> and <a href=\"mailto:Luciano.Orsini@cern.ch\">Luciano Orsini<a/></small>";
	out << "<br style=\"font-family: arial; color: rgb(255, 0, 0);\">" << std::endl;
	out << "<small style=\"font-family: helvetica,arial,sans-serif;\">";
	out << "Web site: ";
	out << cgicc::a().set("href","http://cern.ch/xdaq") << "xdaq.web.cern.ch" << cgicc::a() << std::endl;
	out << "- Jxta PT based on the " << cgicc::a("Jxta-C project").set("href","http://jxta-c.jxta.org") << std::endl;
	out << "</small>";	
	out << "<small style=\"font-family: helvetica,arial,sans-serif;\">";
	out << "<br style=\"font-family: arial; color: rgb(255, 0, 0);\">" << std::endl;
	out << cgicc::span("<br>Copyright &copy; 2004 CERN, European Organization for Nuclear Research");
	out << "</small>";
	out << "</center>";
}

