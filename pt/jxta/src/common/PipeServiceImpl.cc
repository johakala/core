
/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber, G. Lo Presti and L. Orsini                      *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/


#include "jxta/PipeServiceImpl.h"
#include "jxta/AdvertisementImpl.h"


//##ModelId=416A98F2033B
jxta::PipeServiceImpl::PipeServiceImpl(Jxta_pipe_service* j_pipe_svc): j_pipe_svc_(j_pipe_svc)
{
}

//##ModelId=416A98F20344
Jxta_inputpipe* jxta::PipeServiceImpl::publishInputPipe() throw (jxta::exception::Exception)
{
	Jxta_status res;
	Jxta_pipe_adv* padv;
	Jxta_pipe* j_pipe;
	Jxta_inputpipe* j_ip;
	Platform* jpl = jxta::getPlatform(0);
	if(jpl == 0) return 0;
	DiscoveryService* ds = jpl->getNetPeerGroup()->getDiscoveryService();

	Advertisement::Reference adv = ds->getAdvertisementByName(jxta::Advertisement::PIPE, jpl->getLocalAddress()->getPipeName(), false);
	if(adv.isNull()) {
		//printf("\npublishIPipe: adv not found, creating it...\n");
		Jxta_id *pipeid, *gid;
		JString* tmpString;
		
		tmpString = jstring_new_2(jpl->getNetPeerGroup()->getPeerGroupID().c_str());
		jxta_id_from_jstring(&gid, tmpString);
		JXTA_OBJECT_RELEASE(tmpString);
		jxta_id_pipeid_new_1(&pipeid, gid);
		jxta_id_to_jstring(pipeid, &tmpString);
	
		padv = jxta_pipe_adv_new();
		jxta_pipe_adv_set_Id(padv, (char*) jstring_get_string(tmpString));
		jxta_pipe_adv_set_Name(padv, (char*) jpl->getLocalAddress()->getPipeName().c_str());
		jxta_pipe_adv_set_Type(padv, (char*) JXTA_UNICAST_PIPE);
		JXTA_OBJECT_RELEASE(tmpString);
		JXTA_OBJECT_RELEASE(gid);
		JXTA_OBJECT_RELEASE(pipeid);
		
		ds->publishAdvertisement(Advertisement::Reference(new AdvertisementImpl((Jxta_advertisement*) padv)));
	}
	else
		padv = (Jxta_pipe_adv*)((AdvertisementImpl&)(*adv)).getJxtaAdv();
	
	res = jxta_pipe_service_timed_accept (j_pipe_svc_, padv, 3*1000*1000 /* timeout, make parameter? */, &j_pipe);
	JXTA_OBJECT_RELEASE(padv);

	if (res != JXTA_SUCCESS) {
		std::ostringstream o;
		o << "Cannot get Jxta pipe, error #" << res;
		XCEPT_RAISE (jxta::exception::Exception, o.str());
	}
	res = jxta_pipe_get_inputpipe (j_pipe, &j_ip);
	if (res != JXTA_SUCCESS) {
		std::ostringstream o;
		o << "Cannot get Jxta input pipe, error #" << res;
		XCEPT_RAISE (jxta::exception::Exception, o.str());
	}
	
	return j_ip;
}


//##ModelId=419C78340102
jxta::JxtaMessenger* jxta::PipeServiceImpl::createMessenger(pt::Address::Reference destination) throw (jxta::exception::Exception)
{
	return new jxta::JxtaMessenger(destination, searchOutputPipe(dynamic_cast<jxta::Address&>(*destination)));
}
	
//##ModelId=416A98F20345
Jxta_outputpipe* jxta::PipeServiceImpl::searchOutputPipe(jxta::Address& address) throw (jxta::exception::Exception)
{
	Jxta_status res;
	Jxta_pipe_adv* padv;
	Jxta_pipe* rempipe;
	Jxta_outputpipe* outp;
	
	Platform* jpl = jxta::getPlatform(0);
	if(jpl == 0) return 0;
	DiscoveryService* ds = jpl->getNetPeerGroup()->getDiscoveryService();
	Advertisement::Reference adv = ds->getAdvertisementByName(jxta::Advertisement::PIPE, address.getPipeName(), true);
	if(adv.isNull()) {
		XCEPT_RAISE (jxta::exception::Exception, "Cannot find the output pipe");
	}
	padv = (Jxta_pipe_adv*)((AdvertisementImpl&)(*adv)).getJxtaAdv();
	
	// now get the pipe
	res = jxta_pipe_service_timed_connect (j_pipe_svc_, padv, 10*1000*1000 /* timeout */, NULL, &rempipe);
	JXTA_OBJECT_RELEASE(padv);
	
	if (res != JXTA_SUCCESS) {
		std::ostringstream o;
		o << "Cannot get Jxta pipe, error #" << res << " for address: " << address.toString();
		XCEPT_RAISE (jxta::exception::Exception, o.str());
	}
	
	res = jxta_pipe_get_outputpipe (rempipe, &outp);
	if (res != JXTA_SUCCESS) {
		std::ostringstream o;
		o << "Cannot get Jxta output pipe, error #" << res << " for address: " << address.toString();
		XCEPT_RAISE (jxta::exception::Exception, o.str());
	}
	
	//printf("PTSender::searchOutputPipe: done, outp = %d\n", outp);
	return outp;
}

