
/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber, G. Lo Presti and L. Orsini                      *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include "jxta/PlatformImpl.h"
#include "jxta/PipeService.h"

#include "netinet/in.h"
#include "jxtaapr.h"
#include "jxta.h"
#include "jpr/jpr_excep.h"
#include "jxta_id.h"
#include "jxta_pa.h"
#include "jxta_pga.h"
#include "jxta_svc.h"
#include "jxta_id.h"
#include "jxta_xml_util.h"

jxta::Platform* jxta::getPlatform(int port)
{
	return jxta::PlatformImpl::instance(port);
}

// singleton static implementation
//##ModelId=416A98F2037A
jxta::PlatformImpl* jxta::PlatformImpl::instance_ = 0;

//##ModelId=416A98F2039E
jxta::Platform* jxta::PlatformImpl::instance(int port)
{
	// Would actually need a Thread lock for making it thread safe
	if (instance_ == 0)
	{
		if(port <= 1)     // don't start a platform with an invalid port
			return 0;
		instance_ = new jxta::PlatformImpl(port);
	}
	return instance_;
}
// static initialization end


//##ModelId=416A98F203B2
jxta::PlatformImpl::PlatformImpl(int port) throw (jxta::exception::Exception)
{
	// Start the Jxta platform
	apr_initialize();
	makePlatformConfig(port, 1976 /* multicast port, constant for all peers */, "xdaq");
	try {
		netPG_ = new PeerGroupImpl();
	}
	catch(jxta::exception::Exception& failed) {
		std::ostringstream o;
		o << failed.what() << ", port: " << port;
		XCEPT_RAISE (jxta::exception::Exception, o.str());
	}

	// get the listening address
	std::ostringstream ass;
	std::string pname = netPG_->getPeerName();
	ass << "jxta://" << pname.substr(pname.find("@",0)+1) << ":" << port << "/Platform";
	localAddress_ = new jxta::Address(ass.str());
	groups_.clear();
}


//##ModelId=416A98F203BC
jxta::PlatformImpl::~PlatformImpl() 
{
	// send a shutdown message to all connected rdvs to force update the peer view
	std::string shutdownMsg = "PeerShutdown@" + localAddress_->getHost();
	jxta::AdvertisementList::Reference advList = netPG_->getDiscoveryService()->getKnownAdvertisements(jxta::DiscoveryService::PEER);
	
	for (int i = 0; i < advList->getLength(); i++) {
		jxta::Advertisement::Reference adv = advList->getItem(i);
		std::string pname = adv->getName();
		pname = pname.substr(pname.find('@')+1);
		if(pname.find("rdv") == 0) {
			std::ostringstream ass;
			ass << "jxta://" << pname << ":" << localAddress_->getPort() << "/Platform";   //assuming destination port = local port
			jxta::Address* dest = new jxta::Address(ass.str());
			try {
				jxta::JxtaMessenger* msgr = netPG_->getPipeService()->createMessenger(pt::Address::Reference(dest));
				msgr->send("JxtaSys", shutdownMsg);
				//printf("Shutdown msg sent to %s\n", dest->getHost().c_str());
				delete msgr;
			}
			catch(std::exception& ignored) {}    // ignores any error, this is supposed to be an unreliable trial to nicely shutdown
		}
	}
	
	delete netPG_;		// this stops everything
}


//##ModelId=416A98F203DB
jxta::Address* jxta::PlatformImpl::getLocalAddress()
{
	return localAddress_;
}


//##ModelId=416A98F203BD
jxta::PeerGroup* jxta::PlatformImpl::getNetPeerGroup()
{
	return netPG_;     // it's like a singleton
}


//##ModelId=416A98F203BE
jxta::PeerGroup* jxta::PlatformImpl::joinPeerGroup(std::string groupName) throw (jxta::exception::Exception)
{
	PeerGroup* pg = getPeerGroup(groupName);
	if(pg != 0)
		return pg;
	pg = new PeerGroupImpl(groupName);   // can throw exception if it fails to join
	groups_.push_back(pg);
	return pg;
}

//##ModelId=416A98F203C7
jxta::PeerGroup* jxta::PlatformImpl::getPeerGroup(std::string groupName)
{
	if(groupName == netPG_->getPeerGroupName())
		return netPG_;
	for (unsigned int i = 0; i < groups_.size(); i++)
		if (groups_[i]->getPeerGroupName() == groupName)
			return groups_[i];

	return 0;
}

//##ModelId=416A98F203D0
void jxta::PlatformImpl::leavePeerGroup(std::string groupName)
{
	if(groupName == netPG_->getPeerGroupName())
		return;     // ...or raise exception? or shutdown?

	// brute force scan and remove
	std::vector<jxta::PeerGroup*>::iterator it;
	
	for (it = groups_.begin(); it != groups_.end(); it++) {
		PeerGroup* pg = *it;
		if (pg->getPeerGroupName() == groupName) {
			groups_.erase(it);
			delete pg;
			break;   // need to stop loop since erase() affects the sequence
		}
	}
}



//! Generates a minimal working PlatformConfig with TCP/multicast settings based on the provided parameters.
//! The HTTP transport listens by default on tcp_port-1.
void jxta::PlatformImpl::makePlatformConfig(int tcp_port, int mcast_port, const char* peername_prefix) throw (jxta::exception::Exception)
{
	char *ip, *buf;
	Jxta_in_addr iface_addr = 0, mcast_addr = 0;
	JString* def_name;
	Jxta_PA* cfgadv = NULL;
	Jxta_id* pid = NULL;
	Jxta_id* gid_to_use = NULL;
	Jxta_TCPTransportAdvertisement* tta = NULL;
	Jxta_HTTPTransportAdvertisement* hta = NULL;
	Jxta_RelayAdvertisement* rla = NULL;

	Jxta_svc* rdvsvc;
	Jxta_svc* tcpsvc;
	Jxta_svc* htsvc;
	Jxta_svc* rlsvc;
	Jxta_vector* services;
	
	FILE* platfile = fopen("PlatformConfig", "r");
	if(platfile != 0) {       // PlatformConfig file found
		fclose(platfile);
#ifdef KEEP_PLATFORM_CONFIG
		return;
#endif
	}
	
	buf = (char*)malloc(512);
	ip = getLocalIP();     // identical to jxta_id_ipv6_get_local_ip() in the patched jxta-c
	extract_ip(ip, strlen(ip), &iface_addr);
	
	sprintf(buf, "%s@%s", peername_prefix, ip);  // , tcp_port);
	def_name = jstring_new_2(buf);
	
	mcast_addr = htonl(0xE0000001);
//#ifdef PATCHED_JXTA_C
//	jxta_id_set_id_instance("ipv6");     // can work only with the patched jxta-c, otherwise it's not defined
//#endif
	jxta_id_peergroupid_new_1(&gid_to_use);
	
	tta = jxta_TCPTransportAdvertisement_new();
	hta = jxta_HTTPTransportAdvertisement_new();
	rla = jxta_RelayAdvertisement_new();

	rdvsvc = jxta_svc_new();
	tcpsvc = jxta_svc_new();
	htsvc = jxta_svc_new();
	rlsvc = jxta_svc_new();
	
	jxta_svc_set_MCID(rdvsvc, jxta_rendezvous_classid);

	jxta_TCPTransportAdvertisement_set_Protocol(tta, jstring_new_2("tcp"));
	jxta_TCPTransportAdvertisement_set_InterfaceAddress(tta, iface_addr);
	jxta_TCPTransportAdvertisement_set_Port(tta, tcp_port);
	jxta_TCPTransportAdvertisement_set_ConfigMode(tta, jstring_new_2("auto"));
	jxta_TCPTransportAdvertisement_set_ServerOff(tta, (Jxta_boolean)false);
	jxta_TCPTransportAdvertisement_set_ClientOff(tta, (Jxta_boolean)false);
	jxta_TCPTransportAdvertisement_set_MulticastAddr(tta, mcast_addr);
	jxta_TCPTransportAdvertisement_set_MulticastPort(tta, mcast_port);
	jxta_TCPTransportAdvertisement_set_MulticastSize(tta, 16384);
	jxta_svc_set_TCPTransportAdvertisement(tcpsvc, tta);
	jxta_svc_set_MCID(tcpsvc, jxta_tcpproto_classid);

	jxta_HTTPTransportAdvertisement_set_Protocol(hta, jstring_new_2("http"));
	jxta_HTTPTransportAdvertisement_set_InterfaceAddress(hta, iface_addr);
	jxta_HTTPTransportAdvertisement_set_ConfigMode(hta, jstring_new_2("auto"));
	jxta_HTTPTransportAdvertisement_set_Port(hta, tcp_port-1);
	jxta_HTTPTransportAdvertisement_set_ServerOff(hta, (Jxta_boolean)true);
	jxta_HTTPTransportAdvertisement_set_ProxyOff(hta, (Jxta_boolean)true);
	jxta_svc_set_HTTPTransportAdvertisement(htsvc, hta);
	jxta_svc_set_MCID(htsvc, jxta_httpproto_classid);

	jxta_RelayAdvertisement_set_IsClient(rla, jstring_new_2("false"));
	jxta_RelayAdvertisement_set_IsServer(rla, jstring_new_2("false"));
	jxta_svc_set_RelayAdvertisement(rlsvc, rla);
	jxta_svc_set_MCID(rlsvc, jxta_relayproto_classid);

	cfgadv = jxta_PA_new();
	services = jxta_PA_get_Svc(cfgadv);
	jxta_vector_add_object_last(services, (Jxta_object*) rdvsvc);
	jxta_vector_add_object_last(services, (Jxta_object*) tcpsvc);
	jxta_vector_add_object_last(services, (Jxta_object*) htsvc);
	jxta_vector_add_object_last(services, (Jxta_object*) rlsvc);

	jxta_id_peerid_new_1(&pid, gid_to_use);
	jxta_PA_set_PID(cfgadv, pid);
	jxta_PA_set_Name(cfgadv, def_name);
	JXTA_OBJECT_RELEASE(gid_to_use);

	JString* advs;
	platfile = fopen("PlatformConfig", "w");    // eventually overwrite previous existing config. At this point we want to start from scratch.
	if (platfile == 0) {
		XCEPT_RAISE(jxta::exception::Exception, "Fatal: cannot create PlatformConfig file for Jxta.");
	}
	jxta_PA_get_xml(cfgadv, &advs);
	fwrite(jstring_get_string(advs), jstring_length(advs), 1, platfile);
	fclose(platfile);

	JXTA_OBJECT_RELEASE(advs);
	JXTA_OBJECT_RELEASE(cfgadv);
	free(buf);
}


//! Extracts the first local IP address. Makes use of APR, which is included in the Jxta-C implementation.

char* jxta::PlatformImpl::getLocalIP()
{
	char *ip_addr, *local_host = (char *) malloc(128);
	apr_pool_t *pool;
	apr_sockaddr_t *localsa;
	apr_pool_create(&pool, NULL);

	apr_gethostname(local_host, 128, pool);
	apr_sockaddr_info_get(&localsa, local_host, APR_INET, 0, 0, pool);
	free(local_host);
	apr_sockaddr_ip_get(&ip_addr, localsa);

	apr_pool_destroy(pool);
	return ip_addr;
}

