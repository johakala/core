
/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber, G. Lo Presti and L. Orsini                      *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include "jxta/PeerTransportSender.h"

jxta::PeerTransportSender::PeerTransportSender()
{
	//sync_  = new BSem(BSem::EMPTY);
	//mutex_ = new BSem(BSem::FULL);
}

jxta::PeerTransportSender::~PeerTransportSender()
{
	//delete sync_;
	//delete mutex_;
}


pt::Messenger::Reference jxta::PeerTransportSender::getMessenger (pt::Address::Reference destination, pt::Address::Reference local) throw (pt::exception::UnknownProtocolOrService)
{
	if(jpl_ == 0) {
		jpl_ = (PlatformImpl*)jxta::getPlatform(0);       // get it only if it is already configured
		if(jpl_ == 0) {
			XCEPT_RAISE (pt::exception::UnknownProtocolOrService, "Jxta Peer Transport platform not found or not configured, cannot send messages over the Jxta network.");
			// we cannot run the platform at this point because we don't know the listening port
		}
	}

	// Accepts the service to be null: in this case, it assumes that it is the Pipe service. Ignores the local address.
	if (((destination->getService()  == "pipe") || (destination->getService() == "")) && (destination->getProtocol()  == "jxta"))
	{
		PeerGroup* netPG = jpl_->getNetPeerGroup();
		if(netPG->getRdvService()->isConnectedToRdv()) {
			try {
				jxta::JxtaMessenger* m = netPG->getPipeService()->createMessenger(destination);
				//jxtaMessengers_.push_back(m);
				return pt::Messenger::Reference(m);
			}
			catch (jxta::exception::Exception& notfound) {
				XCEPT_RAISE(pt::exception::UnknownProtocolOrService, notfound.what());
			}
		}
		else {
			XCEPT_RAISE(pt::exception::UnknownProtocolOrService, "Cannot send messages over the Jxta network until a Rendezvous peer has not been contacted.");
		}
	}
	else {
		std::string msg = "Cannot handle protocol service combination, destination protocol was: ";
		msg +=  destination->getProtocol();
		msg += " and destination service was: ";
		msg +=  destination->getService();
		XCEPT_RAISE(pt::exception::UnknownProtocolOrService, msg);
	}
}


pt::TransportType jxta::PeerTransportSender::getType()
{
	return pt::Sender;
}

pt::Address::Reference jxta::PeerTransportSender::createAddress( const std::string& url ) throw (pt::exception::InvalidAddress)
{
	return pt::Address::Reference(new jxta::Address(url));
}

pt::Address::Reference jxta::PeerTransportSender::createAddress( std::map<std::string, std::string, std::less<std::string> >& address ) throw (pt::exception::InvalidAddress)
{
	std::string protocol = address["protocol"];

	if (protocol == "jxta")
	{
		std::string url = protocol;
		url += "://";
		url += address["hostname"];
		url += ":";
		url += address["port"];

		std::string service = address["service"];
		if (service != "")
		{
			url += "/";
			url += service;
		}

		return this->createAddress(url);
	}
	else
	{
		XCEPT_RAISE (jxta::exception::Exception, "Protocol " + protocol + " not supported.");
	}
}

std::string jxta::PeerTransportSender::getProtocol()
{
	return "jxta";
}

std::vector<std::string> jxta::PeerTransportSender::getSupportedServices()
{
	std::vector<std::string> s;
	s.push_back("pipe");         // asynchronous messaging
	return s;
}

bool jxta::PeerTransportSender::isServiceSupported(const std::string& service )
{
	if (service == "pipe") return true;
	else return false;
}

