
/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber, G. Lo Presti and L. Orsini                      *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include "jxta/PeerGroupImpl.h"

// singleton static implementation
jxta::PeerGroupImpl* jxta::PeerGroupImpl::netPG_ = 0;
	
jxta::PeerGroupImpl::PeerGroupImpl() throw (jxta::exception::Exception)
{
	Jxta_status res;
	res = jxta_PG_new_netpg(&j_pg_);      // start the platform
	if (res != JXTA_SUCCESS) {
		std::ostringstream o;
		o << "Jxta platform bootstrap failed with error #" << res;
		XCEPT_RAISE (jxta::exception::Exception, o.str());
	}

	// get the IDs and the names
	Jxta_PID* pid;
	Jxta_id* gid;
	JString *temp;
	jxta_PG_get_PID(j_pg_, &pid);
	jxta_id_to_jstring(pid, &temp);
	peerID_ = jstring_get_string(temp);
	JXTA_OBJECT_RELEASE(temp);
	JXTA_OBJECT_RELEASE(pid);
	
	jxta_PG_get_GID(j_pg_, &gid);
	jxta_id_to_jstring(gid, &temp);
	peerGroupID_ = jstring_get_string(temp);
	JXTA_OBJECT_RELEASE(temp);
	JXTA_OBJECT_RELEASE(gid);

	jxta_PG_get_peername(j_pg_, &temp);
	peerName_ = jstring_get_string(temp);
	JXTA_OBJECT_RELEASE(temp);

	jxta_PG_get_groupname(j_pg_, &temp);
	peerGroupName_ = jstring_get_string(temp);
	JXTA_OBJECT_RELEASE(temp);
	
	// get the services
	Jxta_discovery_service* j_disc_svc;
	Jxta_rdv_service* j_rdv_svc;
	Jxta_pipe_service* j_pipe_svc;
	jxta_PG_get_discovery_service(j_pg_, &j_disc_svc);
	jxta_PG_get_rendezvous_service(j_pg_, &j_rdv_svc);
	jxta_PG_get_pipe_service(j_pg_, &j_pipe_svc);
	//jxta_PG_get_endpoint_service(j_pg_, &j_ep_svc_);
	
	discSvc_ = new DiscoveryServiceImpl(j_disc_svc);
	rdvSvc_ = new RdvServiceImpl(j_rdv_svc);
	pipeSvc_ = new PipeServiceImpl(j_pipe_svc);
	netPG_ = this;
	
	// start the bootstrap listener to connect to the first rdv. Don't care to save the pointer, the listener will detach itself
	discSvc_->addServiceListener(new BootstrapRdvDiscListener(discSvc_, rdvSvc_));
}

jxta::PeerGroupImpl::PeerGroupImpl(std::string newGroupName) throw (jxta::exception::Exception)
{
	if(netPG_ == 0) {
		XCEPT_RAISE(jxta::exception::Exception, "Cannot create a new peer group if the NetPeerGroup has not been initialized yet.");
	}
    Jxta_PGA * pga = 0;
    Jxta_vector * resources = 0;
	Jxta_status res;
	
	AdvertisementList::Reference advList = netPG_->discSvc_->getKnownAdvertisements(jxta::DiscoveryService::GROUP);
	for(int i = 0; i < advList->getLength(); i++) {
		Advertisement::Reference adv = advList->getItem(i);
		if(adv->getName() == newGroupName) {
			pga = (Jxta_PGA*)((AdvertisementImpl&)(*adv)).getJxtaAdv();
			break;
		}
	}
	if(pga == 0) {
		XCEPT_RAISE(jxta::exception::Exception, "Failed to find the requested group.");
	}
		
    /* Reuse the resource groups of our parent. */
    jxta_PG_get_resourcegroups(netPG_->j_pg_, &resources);
    /* add the new group's parent */
    jxta_vector_add_object_last(resources, (Jxta_object*) netPG_->j_pg_);
	
    res = jxta_PG_newfromadv(netPG_->j_pg_, (Jxta_advertisement*) pga, resources, &j_pg_);
    if( res != JXTA_SUCCESS ) {
		XCEPT_RAISE(jxta::exception::Exception, "Failed to join the new group.");
	}
}

jxta::PeerGroupImpl::~PeerGroupImpl()
{
	if(j_pg_ == 0)
		return;      // should not happen
	if(this == netPG_) {	// shut down everything
		delete discSvc_;
		delete rdvSvc_;
		delete pipeSvc_;
		jxta_terminate();
		//jxta_module_stop((Jxta_module *) j_pg_);
	}
	else {					// leave the group
        Jxta_membership_service * membership; 
        jxta_PG_get_membership_service(j_pg_, &membership);
        if(membership != 0)
            jxta_membership_service_resign(membership);
	}
	
	JXTA_OBJECT_RELEASE(j_pg_);
}


std::string jxta::PeerGroupImpl::getPeerName()
{
	return peerName_;
}

std::string jxta::PeerGroupImpl::getPeerID()
{
	return peerID_;
}

std::string jxta::PeerGroupImpl::getPeerGroupName()
{
	return peerGroupName_;
}

std::string jxta::PeerGroupImpl::getPeerGroupID()
{
	return peerGroupID_;
}

jxta::DiscoveryService* jxta::PeerGroupImpl::getDiscoveryService()
{
	return netPG_->discSvc_;   // currently only the main netpg services are supported, even if each peer group has his own set of services
}

jxta::RdvService* jxta::PeerGroupImpl::getRdvService()
{
	return netPG_->rdvSvc_;
}

jxta::PipeService* jxta::PeerGroupImpl::getPipeService()
{
	return netPG_->pipeSvc_;
}

