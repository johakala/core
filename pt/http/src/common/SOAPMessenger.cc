// $Id: SOAPMessenger.cc,v 1.9 2008/07/18 15:27:17 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include<map>

#include "toolbox/string.h"
#include "pt/http/SOAPMessenger.h"
#include "pt/http/PeerTransportSender.h"
#include "pt/http/Utils.h"
#include "pt/http/Address.h"
#include "xoap/MessageReference.h"
#include "xoap/MessageFactory.h"
#include "xoap/exception/Exception.h"
#include "pt/http/exception/Exception.h"

pt::http::SOAPMessenger::SOAPMessenger(Logger & logger, pt::Address::Reference destination, pt::Address::Reference local) 
throw (pt::http::exception::Exception ) :
	logger_(logger)
{
	// Check for address correct already done in PeerTRansportSender::getMessenger()
	//
	//destination_ = dynamic_cast<http::Address>(destination);
	destination_ = destination;
	local_ = local;
	
	try 
	{
		//local_ = dynamic_cast<http::Address*>(local);
		channel_ = new http::ClientChannel(destination_);
	}
	catch (pt::http::exception::Exception & e )
	{
		XCEPT_RETHROW(pt::http::exception::Exception, "cannot create SOAP messenger", e);
	}
}

pt::http::SOAPMessenger::~SOAPMessenger()
{
	delete channel_;
}

pt::Address::Reference pt::http::SOAPMessenger::getLocalAddress()
{
	return local_;
}

pt::Address::Reference pt::http::SOAPMessenger::getDestinationAddress()
{
	return destination_;
}

xoap::MessageReference pt::http::SOAPMessenger::send (xoap::MessageReference message) 
	throw (pt::exception::Exception)
{
	std::string requestBuffer;
		
	try
	{
		// serialize message
		message->writeTo (requestBuffer);
 	}
	catch (xoap::exception::Exception& e)
	{		
		// Invalid request, cannot handle it
		XCEPT_RETHROW(pt::exception::Exception, "SOAP request message invalid, cannot be serialized", e); 	
	}
		
	size_t size = requestBuffer.size();
	
	// WRITE TO SOCKET
	char* replyBuffer = 0;
	
	// Fill the headers
	xoap::MimeHeaders httpMimeHeadersRequest;
	std::multimap<std::string, std::string, std::less<std::string> >& allHeaders = message->getMimeHeaders()->getAllHeaders();
	std::multimap<std::string, std::string, std::less<std::string> >::iterator i;
	
	std::string headers = "";
	std::string path = "";
	for (i = allHeaders.begin(); i != allHeaders.end(); i++)
	{
		if ((*i).first == "xdaq-pthttp-connection-timeout" )
		{
			try
			{
				channel_->setConnectionTimeout(toolbox::toLong((*i).second));	
			}
			catch(toolbox::exception::Exception & e)
			{
				XCEPT_RETHROW(pt::exception::Exception, "invalid connection timeout value", e);
			}
		}
		else
		{
			/*
			if (toolbox::tolower((*i).first) == "soapaction" )
			{
				path = (*i).second;
			}
			*/

			httpMimeHeadersRequest.setHeader((*i).first,(*i).second);
		}
	}
	
	channel_->lock();
	try
	{
		// Connect is intelligent, tries to connect only if socket not already connected
		//
		channel_->connect();
		
		http::Address& d = dynamic_cast<http::Address&>(*destination_);
		
		path = d.getPath(); //+ path;

		httpMimeHeadersRequest.setHeader("Connection","keep-alive");
		httpMimeHeadersRequest.setHeader("Content-length",toolbox::toString("%d",size));

		if ( message->countAttachments() > 0 )
		{
			httpMimeHeadersRequest.setHeader("Accept","text/html, image/gif, image/jpeg, *; q=.2, */*; q=.2");
			httpMimeHeadersRequest.setHeader("Content-Description","XDAQ SOAP with attachments.");
			httpMimeHeadersRequest.setHeader("Content-type", "multipart/related; type=" + message->getImplementationFactory()->getMediaType() + ";charset=utf-8; boundary=" + message->getMimeBoundary());


		}
		else
		{
			httpMimeHeadersRequest.setHeader("Content-Description","XDAQ SOAP");
			httpMimeHeadersRequest.setHeader("Content-type",message->getImplementationFactory()->getMediaType() + ";charset=utf-8");
		}

		http::Utils::sendTo( channel_, 
				(char*)path.c_str(),
				(char*)d.getHost().c_str(),
				(char*)d.getPort().c_str(),
				(char*)requestBuffer.c_str(), 
				size,
				httpMimeHeadersRequest);
	
		size = 0;
		xoap::MimeHeaders httpMimeHeadersResponse;
		replyBuffer = http::Utils::receiveFrom ( channel_, &size, httpMimeHeadersResponse);

		channel_->disconnect();
	}
	catch (pt::http::exception::Exception& httpe)
	{
		if (replyBuffer != 0)
			delete [] replyBuffer;
		channel_->unlock();
		std::stringstream msg;
		msg << "SOAP request to " << destination_->toString() << " failed";
		XCEPT_RETHROW (pt::exception::Exception, msg.str(), httpe);
	}
	channel_->unlock();
	
	if (size == 0)
	{
		if (replyBuffer != 0)
			delete [] replyBuffer;

		xoap::MessageReference reply(0);
		return reply;
	}

	try
	{
		xoap::MessageReference reply = xoap::createMessage(replyBuffer , size);
		delete [] replyBuffer;
		return reply;
	} 
	catch (xoap::exception::Exception& e)
	{
		delete [] replyBuffer;
		// Invalid reply, cannot handle it
		XCEPT_RETHROW(pt::exception::Exception, "SOAP reply cannot be processed, may be corrupted", e); 	
	}
}

