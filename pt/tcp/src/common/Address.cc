// $Id: Address.cc,v 1.9 2009/03/04 08:51:33 lorsini Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include <iostream>
#include <sstream>

#include "pt/tcp/Address.h"
#include "toolbox/net/URL.h"
#include "toolbox/net/Utils.h"
#include "toolbox/exception/Exception.h"


// only for debugging inet_ntoa
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>




pt::tcp::Address::Address(const std::string & url, const std::string& service): url_(url)
{
	if ((service_ != "i2o") && (service_ != "b2in"))
	{
		// std::string msg = "Cannot create tcp::Address from url, unsupported service ";
		// msg += service;
		// XCEPT_RAISE (pt::exception::Exception, msg);
	}
	service_ = service;

	socketIsBound_ = false;
	socket_ = -3;
}

pt::tcp::Address::~Address()
{
}

struct sockaddr_in pt::tcp::Address::getSocketAddress()
{
	struct sockaddr_in writeAddr;
	try 
	{
		writeAddr = toolbox::net::getSocketAddressByName (url_.getHost(), url_.getPort());
	
		//std::cout << "port number " <<  writeAddr.sin_port << std::endl;
		//std::cout << "IP addressin host format: " << inet_ntoa(writeAddr.sin_addr) << std::endl;
	} catch (toolbox::exception::Exception& e)
	{
		std::cout << e.what() << std::endl;
	}
	return writeAddr;
}

bool pt::tcp::Address::isSocketBound()
{
	return socketIsBound_;
}

int pt::tcp::Address::getBoundSocket()
{
	return socket_;
}

void pt::tcp::Address::bindSocket(int s)
{
	socketIsBound_ = true;
	socket_ = s;
}

std::string pt::tcp::Address::getService()
{
	return service_;
}

std::string pt::tcp::Address::getProtocol()
{
	return url_.getProtocol();
}

std::string pt::tcp::Address::toString()
{
	return url_.toString();
}

std::string pt::tcp::Address::getURL()
{
	return url_.toString();
}

std::string pt::tcp::Address::getHost()
{
	return url_.getHost();
}

std::string pt::tcp::Address::getPort()
{
    std::ostringstream o;
    if(url_.getPort() > 0)
        o <<  url_.getPort();
    return o.str();
}
std::string pt::tcp::Address::getPath()
{
	std::string path = url_.getPath();
        if ( path.empty()  ) 
	{	
		return "/";
	}
	else
	{
		if ( path[0] == '/' )
		{
			return path;
		}
		else
		{
			path.insert(0,"/");
			return path;
		}				
	}	
}

std::string pt::tcp::Address::getServiceParameters()
{
	return url_.getPath();
}

bool pt::tcp::Address::equals( pt::Address::Reference address )
{
	return (( this->toString() == address->toString()) && (this->getService() == address->getService()) );
}
