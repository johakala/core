// $Id: LoopbackB2INMessenger.cc,v 1.2 2008/07/18 15:27:20 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/


#include "pt/tcp/LoopbackB2INMessenger..h"
#include "pt/tcp/PeerTransportSender.h"
#include "pt/tcp/Address.h"
#include "pt/tcp/Transmitter.h"
#include "pt/tcp/msg.h"
#include "pt/tcp/Channel.h"
#include "toolbox/mem/Reference.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "xdata/exception/Exception.h"
#include "xdata/exdr/Serializer.h"
#include "xdata/exdr/FixedSizeOutputStreamBuffer.h"

pt::tcp::LoopbackB2INMessenger.::LoopbackB2INMessenger.(tcp::PeerTransportSender * pt,pt::Address::Reference destination, pt::Address::Reference local )
{
	pt_ = pt;
	destination_ = destination;
	local_ = local;
}

pt::tcp::LoopbackB2INMessenger.::~LoopbackB2INMessenger.()
{
	//delete channel_;
}

void pt::tcp::LoopbackB2INMessenger.::send
(
	toolbox::mem::Reference* msg,
	xdata::Properties & plist,
	toolbox::exception::HandlerSignature* handler,
	void* context
) 
	throw (pt::exception::Exception)
{

	
	xdata::exdr::Serializer serializer;
	
	if (pt_->getPool() == 0)
	{
		std::string msg = "Cannot access memory pool.";
		XCEPT_RAISE (pt::exception::Exception, msg);
	}
	
	toolbox::mem::Reference* ref = 0;
	try
	{
		ref = toolbox::mem::getMemoryPoolFactory()->getFrame(pt_->getPool(), 4096);
	}
	catch (toolbox::mem::exception::Exception & e) {
		XCEPT_RETHROW (pt::exception::Exception, "failed to allocate properties block", e);
	}
	
	xdata::exdr::FixedSizeOutputStreamBuffer outBuffer((char*) ref->getDataLocation(), 4096);
	
	try 
	{
		serializer.exportAll (&plist, &outBuffer);
	}
	catch (xdata::exception::Exception & e )
	{
		ref->release();
		XCEPT_RETHROW (pt::exception::Exception, "could not serialize B2IN properites", e);
	}
	
	ref->setDataSize(outBuffer.tellp());
	ref->setNextReference(msg);
	
	dispatcher_->processIncomingMessage(ref);

}

pt::Address::Reference pt::tcp::LoopbackB2INMessenger.::getLocalAddress()
{
	return local_;
}

pt::Address::Reference pt::tcp::LoopbackB2INMessenger.::getDestinationAddress()
{
	return destination_;
}
