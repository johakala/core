// $Id: ReceiverLoop.cc,v 1.46 2009/03/04 09:07:48 lorsini Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "pt/tcp/ReceiverLoop.h"

#include <limits.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/poll.h>
#include <unistd.h>
#include <iostream>
#include <errno.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>

#include "xcept/tools.h"
#include  "i2o/i2o.h"
#include  "pt/tcp/msg.h"
#include "i2o/utils/endian.h"
#include "b2in/nub/exception/Exception.h"
#include "pt/exception/Exception.h"
#include "pt/tcp/exception/Exception.h"
#include "pt/tcp/exception/CannotConnect.h"
#include "toolbox/exception/Processor.h"
#include "pt/exception/MaxMessageSizeExceeded.h"
#include "pt/exception/NoListener.h"
#include "pt/exception/ReceiveFailure.h"
#include "toolbox/task/WorkLoopFactory.h"
#include "toolbox/task/WorkLoop.h"
#include "toolbox/utils.h"
#include "toolbox/TimeVal.h"
#include "toolbox/hexdump.h"
#include "xdata/Properties.h"
#include "xdata/exdr/FixedSizeInputStreamBuffer.h"
#include "xdata/exdr/Serializer.h"

void pt::tcp::ReceiverLoop::setPool(toolbox::mem::Pool * pool)
{
	pool_ = pool;
}

void pt::tcp::ReceiverLoop::setMaxPacketSize(xdata::UnsignedLongT maxPacketSize )
{
	maxPacketSize_ = maxPacketSize;
}
	
void pt::tcp::ReceiverLoop::setAutoSize(xdata::BooleanT autoSize )
{
	autoSize_ = autoSize;
}

void pt::tcp::ReceiverLoop::setMaxClients( xdata::UnsignedInteger32T maxClients )
{
	maxClients_ = (int) maxClients;
}

pt::tcp::ReceiverLoop::ReceiverLoop(pt::Address::Reference  address, xdaq::Object* obj) 
	throw (pt::tcp::exception::Exception) : 
	pt::tcp::Channel(address), xdaq::Object(obj->getOwnerApplication())
{
	deadBand_ = false;

	// select timeout is 1 second (1000 milliseconds)
	timeout_ = 1000;

	manager_ = toolbox::mem::getMemoryPoolFactory();

	pool_ = 0;
	autoSize_ = true;
	maxPacketSize_ = 0;
	listener_ = 0;
	b2inListener_ = 0;
	address_ = address;
	maxClients_ = 1000; // default value
	
	
	// bind worklopp job method
	process_ = toolbox::task::bind (this, &ReceiverLoop::process, "process");
	
	// Preparation for monitoring:
	// Each receiver loop creates an infospace names "urn:pt:tcp-XXX", where XXX is the
	// string represenation of the address
	receivedMessages_ = 0;
	kiloBytesReceived_ = 0;
	receiveTime_ = 0.0;
	endpointURL_ = address->toString();
	
	try
	{	
		toolbox::net::URN urn = this->getOwnerApplication()->createQualifiedInfoSpace("pt-tcp-receiver");
                monitorInfoSpace_ = xdata::getInfoSpaceFactory()->get(urn.toString());
	
		monitorInfoSpace_->fireItemAvailable("endpointURL", &endpointURL_);	
		monitorInfoSpace_->fireItemAvailable("receivedMessages", &receivedMessages_);	
		monitorInfoSpace_->fireItemAvailable("receiveTime", &receiveTime_);	
		monitorInfoSpace_->fireItemAvailable("kiloBytesReceived", &kiloBytesReceived_);
	}
	catch (xdata::exception::Exception & e )
	{
		XCEPT_DECLARE_NESTED(pt::tcp::exception::Exception, f,"failed to create monitoring infospace for tcp receiver", e );
		this->getOwnerApplication()->notifyQualified("fatal",f);
	}
}

void pt::tcp::ReceiverLoop::activate() throw (pt::tcp::exception::Exception)
{
	/* Initialize the socket receive subsystem */
	//sockets_.resize(maxClients_);
	originators_.resize(maxClients_);
	originatorsURL_.resize(maxClients_);
	
	clients_ = new pollfd[maxClients_]; // dynamically allocated enough socket receive descriptors
	
	for (int i = 0; i < maxClients_; ++i) 
	{
        	 //sockets_[i] = -1;
		 clients_[i].fd = -1; // init poll structure
	}
	
	current_ = -1;
	
	if (!  dynamic_cast<pt::tcp::Address&>(*address_).isSocketBound())
	{
		errno = 0;	
		if (::bind(socket_, (struct sockaddr *)&sockaddress_, sockaddressSize_) == -1)
   		{
			std::string msg = toolbox::toString("%s on url: %s",strerror(errno),address_->toString().c_str() );
			::close(socket_);
			socket_ = 0;
			XCEPT_RAISE(pt::tcp::exception::Exception, msg);
   		}
	}
	else	
	{
		//std::cout << "use bound socket for listening " << socket_;

	}

	errno = 0;
	if (::listen (socket_, maxClients_) == -1)
   	{ 
		std::string msg = toolbox::toString("%s on url: %s",strerror(errno),address_->toString().c_str() );
		::close(socket_);
		socket_ = 0;
		XCEPT_RAISE(pt::tcp::exception::Exception, msg);
   	}

	// Index 0 is used for listening to incoming connections
	//
	clients_[0].fd = socket_;
	clients_[0].events = POLLRDNORM;

	listenfd_ = socket_;	
	accepted_ = 0;
	nochannels_ = 0;	
	maxfd_ = 0; // max index is the first one in the clients_ array

	stop_ = false;
	done_ = false;
	std::string name = "urn:toolbox-task-workloop:";
	name += address_->toString();
	toolbox::task::getWorkLoopFactory()->getWorkLoop(name, "waiting")->submit(process_);
	toolbox::task::getWorkLoopFactory()->getWorkLoop(name, "waiting")->activate();
}
	
void pt::tcp::ReceiverLoop::cancel()
{
	done_ = false;
	stop_ = true;
	// wait for done
	while (! done_ )
	{
		toolbox::u_sleep(100);
		// busy waiting
	}	
	stop_ = false;
	std::string name = "urn:toolbox-task-workloop:";
	name += address_->toString();
	toolbox::task::getWorkLoopFactory()->getWorkLoop(name, "waiting")->cancel();
}

pt::tcp::ReceiverLoop::~ReceiverLoop()
{
/*
	for (std::vector<int>::size_type i = 0; i < sockets_.size(); i++ )
        {
                 if (( sockets_[i] != -1 ) && ( sockets_[i] != -2 ))
		 {
			::close(sockets_[i]);
		 }
        }
*/
	for (int i = 1; i < maxfd_ +1; i++ )
	{
		if (( clients_[i].fd != -1 ) && (clients_[i].fd != -2))
		{
			::close(clients_[i].fd);
		}
	}
	
	delete [] clients_;

}

void pt::tcp::ReceiverLoop::addServiceListener (pt::Listener* listener)
{
	if (listener->getService() == "i2o")
	{
		listener_ = dynamic_cast<i2o::Listener *>(listener);
	}
	else if (listener->getService() == "b2in")
	{
		b2inListener_ = dynamic_cast<b2in::nub::Listener *>(listener);
	}
}

void pt::tcp::ReceiverLoop::removeServiceListener (pt::Listener* listener)
{
	if (listener->getService() == "i2o")
	{
		listener_ = 0;
	}
	else if (listener->getService() == "b2in")
	{
		b2inListener_ = 0;
	}
}

void pt::tcp::ReceiverLoop::removeAllServiceListeners()
{
	listener_ = 0;
	b2inListener_ = 0;
}

pt::Address::Reference pt::tcp::ReceiverLoop::getAddress()
{
	return address_;
}
	

void pt::tcp::ReceiverLoop::disconnect() throw (pt::tcp::exception::Exception) 
{
	XCEPT_RAISE(pt::tcp::exception::Exception, "not implemented");
/*
	if ( current_ >= 0 ) 
	{
		::close(sockets_[current_]);
		FD_CLR(sockets_[current_], &allset_);
		sockets_[current_] = -2; // closed by sender, one can still reply by reconnecting
	}
	else 
	{
		XCEPT_RAISE(pt::tcp::exception::Exception, "Disconnect error, no receive socket available");
	}
*/	
}

	
// the implementation of connect for the server is a  re-connect to the client	
void pt::tcp::ReceiverLoop::connect() throw (pt::tcp::exception::Exception) 
{
	XCEPT_RAISE(tcp::exception::CannotConnect, "not implemented");
/*
	if ( current_ >= 0 ) 
	{
		errno = 0;
	    	if (::connect(sockets_[current_], (struct sockaddr *)&sockaddress_, sockaddressSize_) == -1)
	    	{
				std::string msg = toolbox::toString("%s: host %s port %d",strerror(errno),inet_ntoa(sockaddress_.sin_addr), ntohs(sockaddress_.sin_port) );
				::close(socket_);
				XCEPT_RAISE(tcp::exception::CannotConnect, msg);
	    	}
    	}
    	else
    	{
		XCEPT_RAISE(tcp::exception::CannotConnect, "re-connect error, no free socket available");
	}
*/		
}
	
	
void pt::tcp::ReceiverLoop::safeReceive (char* buf, size_t len) throw (pt::tcp::exception::ReceiveError, pt::tcp::exception::ConnectionReset) 
{
	if ( len == 0 ) return; // allow to accept chains with blocks with empty data Examaple of b2in minimal chain is Header-Piroperties -> Header-0
	size_t totalBytes = 0;
	size_t nBytes;
	size_t size = len;
                
	
	do 
	{
		try
		{
			nBytes = size - totalBytes; // missing bytes to read
			totalBytes += this->receive(&buf[totalBytes], nBytes);
		}
		catch (pt::tcp::exception::ReceiveError& e)
		{
			/*
			std::cout << "Receive error for a total of " << len << ", already received: " << totalBytes << std::endl;			
			toolbox::hexdump ((void*) buf, totalBytes);			
			std::cout << std::endl;
			*/
			
			std::stringstream msg;
			msg << "Receive error during read of a total of " << len << " bytes";
			XCEPT_RETHROW (pt::tcp::exception::ReceiveError, msg.str(), e);
		}
	} 
	while (totalBytes < size );
}
	
pt::Address::Reference  pt::tcp::ReceiverLoop::getOriginatorAddress()
{
	return originators_[current_];
}

size_t pt::tcp::ReceiverLoop::receive(char * buf, size_t len ) throw (pt::tcp::exception::ReceiveError, pt::tcp::exception::ConnectionReset) 
{
	ssize_t length;
	if ( current_ >= 0 ) 
	{
		errno = 0;
		length = ::recv(clients_[current_].fd, buf, len, 0);

		if (length == -1)
		{						
			if ((errno == EAGAIN) || (errno == EWOULDBLOCK))
			{
				// TIMEOUT
				std::stringstream msg;
				msg << "Timeout when receiving " << len << " bytes from " << originatorsURL_[current_];				
				XCEPT_RAISE(pt::tcp::exception::ReceiveError, msg.str() );
			}
			else if (errno == ECONNRESET)
			{
				// Connection reset by peer
				std::stringstream msg;
				msg << 	strerror(errno) << ", peer " << originatorsURL_[current_];						
				XCEPT_RAISE(pt::tcp::exception::ConnectionReset, msg.str() );
			}
			else 
			{
				std::stringstream msg;
				msg << 	strerror(errno) << ", peer " << originatorsURL_[current_];						
				XCEPT_RAISE(pt::tcp::exception::ReceiveError, msg.str() );
			}
		}
		if ( length == 0 ) 
		{ 
			// Connection close by peer
			std::stringstream msg;
			msg << "Connection reset by peer " << originatorsURL_[current_];
			XCEPT_RAISE(pt::tcp::exception::ConnectionReset, msg.str());
		}	    
    	}
    	else
    	{
		XCEPT_RAISE(pt::tcp::exception::ReceiveError, "receive error, no socket available");
	}	

	return length;
}
	
	
void pt::tcp::ReceiverLoop::close() throw (pt::tcp::exception::Exception) 
{
	if ( current_ >= 0 ) 
	{
		::close(clients_[current_].fd);
		clients_[current_].fd = -1;
	}
	else
    	{
		XCEPT_RAISE(pt::tcp::exception::Exception, "close error, no socket available");
	}	
}
	
	
void pt::tcp::ReceiverLoop::send(const char * buf, size_t len) throw (pt::tcp::exception::Exception) 
{
	XCEPT_RAISE(pt::tcp::exception::Exception, "not implemented");
	
}
	
	
bool pt::tcp::ReceiverLoop::isConnected() throw (pt::tcp::exception::Exception) 
{
	if ( current_ >= 0 ) 
	{
		// the channel is neither dead nor disconnected
		return (clients_[current_].fd != -1 && (clients_[current_].fd != -2));
	}
	else
    	{
		XCEPT_RAISE(pt::tcp::exception::Exception, "connection state, no socket available");
	}	
}
	
	
bool pt::tcp::ReceiverLoop::isActive() throw (pt::tcp::exception::Exception) 
{
	XCEPT_RAISE(pt::tcp::exception::Exception, "not implemented");

}

int pt::tcp::ReceiverLoop::accept(std::string & host, uint16_t & port) throw (pt::tcp::exception::Exception)
{
        int newsock;
        struct sockaddr_in  readAddr;
        socklen_t readAddrLen = sizeof(readAddr);

        errno = 0;
        if ( (newsock = ::accept( listenfd_, (struct sockaddr *) &readAddr, &readAddrLen) ) == -1 )
        {
                std::string msg = strerror(errno);
		if ( errno == EMFILE )
		{
                	XCEPT_RAISE(pt::tcp::exception::Exception, msg);
		}
		else if (errno == ECONNABORTED )
		{
                	XCEPT_RAISE(pt::tcp::exception::Exception, msg);
		}
		else if ( errno == ENFILE )
		{
                	XCEPT_RAISE(pt::tcp::exception::Exception, msg);
		}
		else
		{
                	::close(listenfd_);
                	listenfd_ = 0;
                	XCEPT_RAISE(pt::tcp::exception::Exception, msg);
		}
        }
	
	errno = 0;
	struct timeval tv = {10,0};	
  	if (setsockopt(newsock, SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof(timeval)) == -1) 
	{
		XCEPT_RAISE (pt::tcp::exception::Exception, strerror(errno));
        }	

/*
	 errno = 0;
         struct linger li = {1,1};
         if (setsockopt(newsock, SOL_SOCKET, SO_LINGER, &li, sizeof(linger)) == -1)
         {
                 XCEPT_RAISE (pt::tcp::exception::Exception, strerror(errno));
         }
*/

        host  = inet_ntoa(readAddr.sin_addr);
        port = ntohs(readAddr.sin_port);

        return newsock;
/*
        struct hostent * h = gethostbyaddr((char*)&readAddr.sin_addr.s_addr, sizeof(readAddr.sin_addr.s_addr), AF_INET);
        std::string remoteHostName;
        if ( h != 0 )
                hname = h->h_name;
        else
                hname = "";

        //std::cout << "-------[" << ip << "]-----------" << std::endl;
        //std::cout << "-------[" << hname << "]-----------" << std::endl;

    //strncpy(addr->host,inet_ntoa(readAddr_.sin_addr),256);
*/
}
	
bool pt::tcp::ReceiverLoop::process (toolbox::task::WorkLoop * wl) 
{
	if (stop_)
	{
		done_ = true;
		return false; // end loop
	}

	fdset_ = allset_;

	// Waiting for incoming connections and data. There is no timeout used right now
	//
	errno = 0;
	nready_ = poll(clients_, maxfd_+1, timeout_);

	toolbox::TimeVal startTime = toolbox::TimeVal::gettimeofday();

	if (nready_ == 0 ) 
	{
		// timeout!
		//LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), "Poll timeout");
		return true;	
	}
	else if (nready_ < 0)
	{
		// error
		LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), "Poll error: " << strerror(errno));
		return true;
	}
	else if (clients_[0].revents & POLLRDNORM)
	{
		// Check if it is an incoming connection if on descriptor 0 a read event was found			
		// accept connection and return FALSE
		//
		try
		{
			std::string  host;
			uint16_t   port;
			int newsock = this->accept(host,port);
			LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), "New connection from " << host);


			// Search a free poll file descriptor for using this connection
			int i;
			for (i = 1; i < maxClients_; ++i)
			{
				if (clients_[i].fd < 0)
				{ 
					clients_[i].fd = newsock;
					clients_[i].events = POLLRDNORM; // set mask to only let read events through
					break;
				}
			}

			if (i >= maxClients_ ) 
			{
				XCEPT_DECLARE (pt::tcp::exception::Exception, e, "Too many open channels, incoming connection refused");
				this->getOwnerApplication()->notifyQualified("warning", e);
				return true;
			}

			// Set the maximum index for used file descriptors in array
			if (i > maxfd_ ) maxfd_ = i;

			//sockets_[newsock] = newsock;
			std::stringstream url;
			url << "tcp://" << host << ":" << port;
        		pt::Address::Reference source(new pt::tcp::Address(url.str(), address_->getService()));
			originators_[i] = source;
			originatorsURL_[i] = url.str();
			accepted_++;

			toolbox::TimeVal stopTime = toolbox::TimeVal::gettimeofday();					
			receiveTime_ = receiveTime_ + ((double) stopTime - (double) startTime);

			return true;
		}
		catch(pt::tcp::exception::Exception & e)
		{
			LOG4CPLUS_WARN(this->getOwnerApplication()->getApplicationLogger(), "failed to accept connection: " <<  xcept::stdformat_exception_history(e));
			XCEPT_DECLARE_NESTED (pt::tcp::exception::Exception, ex, "Failed to accept incoming connection", e);
			this->getOwnerApplication()->notifyQualified("error", ex);
			return true;
		}   
	}

	// In all other cases, it's a read event 
	// Guarantee faireness by going through the whole list and checking for all read events
	//
	//for (std::vector<int>::size_type c = 0; c < sockets_.size(); ++c ) 
	int sockfd = 0;
	for (int c = 1; c <= maxfd_; ++c ) 
	{ 
		// check all clients for data
		//
		if ( (sockfd = clients_[c].fd) < 0)
		{
			continue; // not connected, try next one
		}

		if (clients_[c].revents & (POLLRDNORM | POLLERR))
		{
			// data ready or error
			//current_ = clients_[c].fd;
			//currentIndex_ = c;
			current_ = c;

			toolbox::TimeVal stopTime = toolbox::TimeVal::gettimeofday();					
			receiveTime_ = receiveTime_ + ((double) stopTime - (double) startTime);

			this->onRequest();

			startTime = toolbox::TimeVal::gettimeofday();

			if (--nready_ <= 0)
			{
				break; // no more pending events
			}
		}
		else if (clients_[c].revents & POLLHUP)
		{
			this->close();
			size_t erased = netFilter_.clear(current_);
			std::stringstream msg;
			msg << "Hangup has occured, cleared " << erased << " chains on socket " << current_;
			XCEPT_DECLARE (pt::exception::ReceiveFailure, e, msg.str());
			this->getOwnerApplication()->notifyQualified("error", e);	

			if (--nready_ <= 0)
			{
				break; // no more pending events
			}
		}
		else if (clients_[c].revents & POLLNVAL)
		{
			this->close();
			size_t erased = netFilter_.clear(current_);
			std::stringstream msg;
			msg << "Socket is not a valid file descriptor, cleared " << erased << " chains on socket " << current_;
			XCEPT_DECLARE (pt::exception::ReceiveFailure, e, msg.str());
			this->getOwnerApplication()->notifyQualified("error", e);	

			if (--nready_ <= 0)
			{
				break; // no more pending events
			}
		}
	}
	return true;
}

void pt::tcp::ReceiverLoop::onRequest() throw (pt::tcp::exception::Exception) 
{
	toolbox::TimeVal startTime = toolbox::TimeVal::gettimeofday();
			
	toolbox::mem::Reference* ref = 0;
	try 
	{
		size_t blockSize = maxPacketSize_;
		size_t length = 0;
		uint32_t signature;
		this->safeReceive((char*)&signature, sizeof(uint32_t)); 
		swapU32 ( (U32*) &signature, sizeof(uint32_t));
		
		// -- PTTCP_SIGNATURE PACKET HANDLING
		//
		if ( signature == PTTCP_SIGNATURE )
		{
			size_t bytesReceived = 0;
			
			PTTCP_MESSAGE_FRAME  header;
			header.signature = signature;
		 	char * offset = ((char*)&header) + sizeof(uint32_t);	
			this->safeReceive(offset, sizeof(PTTCP_MESSAGE_FRAME) - sizeof(uint32_t) );
			swapU32 ( (U32*) offset, sizeof(PTTCP_MESSAGE_FRAME) - sizeof(uint32_t));
			
			bytesReceived = sizeof(PTTCP_MESSAGE_FRAME) - sizeof(uint32_t); // monitoring
						
			length = header.length << 2;
			if (autoSize_)
			{
				blockSize = length;
			}
			
			if (length > maxPacketSize_)
			{
				this->close();
				size_t erased = netFilter_.clear(current_);
				std::stringstream msg;
				msg << "Incoming message with size " << length << " exceeded maximum packet size " << maxPacketSize_ << ", cleared " << erased << " chains on socket " << current_ << " from peer " << originatorsURL_[current_];
				XCEPT_DECLARE (pt::exception::MaxMessageSizeExceeded, e, msg.str());
				this->getOwnerApplication()->notifyQualified("error", e);
				return; // packet lost
			}
			
			try
			{
				ref = manager_->getFrame(pool_, blockSize);
			}	
			catch (toolbox::mem::exception::Exception & ex )
			{
				this->close();
				size_t erased = netFilter_.clear(current_);
				std::stringstream msg;
				msg << "Failed to allocate buffer for incoming message, cleared " << erased << " chains on socket " << current_ << " from peer " << originatorsURL_[current_];
				XCEPT_DECLARE_NESTED (pt::exception::ReceiveFailure, e, msg.str(), ex);
				this->getOwnerApplication()->notifyQualified("error", e);
				return;
			}

			// never receive packets larget than maxPacketSize_
			try
			{
				this->safeReceive ( (char*) ( reinterpret_cast<size_t>(ref->getDataLocation())), length);
			}
			catch (pt::tcp::exception::ReceiveError & ex)
			{			
				this->close();
                                size_t erased = netFilter_.clear(current_);
                                std::stringstream msg;
                                msg << "Failed to receive message, cleared " << erased << " chains on socket " << current_ << " from peer " << originatorsURL_[current_];
                                XCEPT_DECLARE_NESTED (pt::exception::ReceiveFailure, e, msg.str(), ex);
                                this->getOwnerApplication()->notifyQualified("warning", e);
                                return;

				//XCEPT_RETHROW(pt::tcp::exception::ReceiveError, "Receive error", ex);
			}
			
			ref->setDataSize(length);
			
			bytesReceived += length; // monitoring
			kiloBytesReceived_ = kiloBytesReceived_ + (bytesReceived / 1024.0);
	
			uint32_t chainKey = ((uint32_t) header.identification << 16);
			chainKey |= (uint32_t) current_;
			
			toolbox::mem::Reference* message = 0 ;
			
			try
			{
				message = netFilter_.append(ref, header, chainKey);
			}
			catch (pt::tcp::exception::Exception & ex )
			{			
				if (ref != 0) ref->release();
				this->close();
				size_t erased = netFilter_.clear(current_);
				std::stringstream msg;
				msg << "Failed to append message, cleared " << erased << " chains on socket " << current_ << " from peer " << originatorsURL_[current_];
				XCEPT_DECLARE_NESTED (pt::exception::ReceiveFailure, e, msg.str(), ex);
				this->getOwnerApplication()->notifyQualified("error", e);
				return;
			}			
			
			if (  message == 0 )
			{
				// chain is not completed yet
				toolbox::TimeVal stopTime = toolbox::TimeVal::gettimeofday();					
				receiveTime_ = receiveTime_ + ((double) stopTime - (double) startTime);
				return;
			}
			
			if (b2inListener_ != 0)
			{
				if ( pool_->isHighThresholdExceeded() )
				{
					message->release();
					this->close();
					size_t erased = netFilter_.clear(current_);
					std::stringstream msg;
					msg << "Out of memory in pt/tcp receiver, cleared " << erased << " chains on socket " << current_ << " from peer " << originatorsURL_[current_];
					XCEPT_DECLARE(pt::exception::Exception, e, msg.str());
					this->getOwnerApplication()->notifyQualified("error", e);
					return;
				}
				
				// close message in case of connection set to close
				// de-serialize properties, within first block
				xdata::exdr::FixedSizeInputStreamBuffer inBuffer((char*) message->getDataLocation(),message->getDataSize());
				xdata::Properties plist;
	
				try 
				{
					xdata::exdr::Serializer serializer;
					serializer.import(&plist, &inBuffer );
				}	
				catch(xdata::exception::Exception & e )
				{
					// ignore message and keep alive
					message->release();
					std::stringstream msg;
					msg << "Failed to parse B2IN properties in message from peer " << originatorsURL_[current_];
					XCEPT_DECLARE_NESTED (pt::exception::Exception, pte, msg.str(), e);
					this->getOwnerApplication()->notifyQualified("error", pte);
					return;
				}

				// Pass the frame and the socket file descriptor on which it has been received
				//
				try
				{
					++receivedMessages_; // monitoring
					
					toolbox::TimeVal stopTime = toolbox::TimeVal::gettimeofday();					
					receiveTime_ = receiveTime_ + ((double) stopTime - (double) startTime);
				
					b2inListener_->processIncomingMessage (message, plist);
				}
				catch (b2in::nub::exception::Exception &e)
				{
					message->release();
					std::stringstream msg;
					msg << "Failed to process B2IN message from peer " << originatorsURL_[current_];
					XCEPT_DECLARE_NESTED (pt::exception::Exception, pte, msg.str(), e);
					this->getOwnerApplication()->notifyQualified("error", pte);
				}

				std::string connection = plist.getProperty("urn:b2in-protocol-tcp:connection");
				if( connection == "close")
				{
					this->close();
					(void) netFilter_.clear(current_);
				}
			}
			else
			{
				message->release();
				
				toolbox::TimeVal stopTime = toolbox::TimeVal::gettimeofday();					
				receiveTime_ = receiveTime_ + ((double) stopTime - (double) startTime);

				std::stringstream msg;
				msg << "No listener found for TCP/B2IN message from peer " << originatorsURL_[current_] << ", message discarded";
				XCEPT_DECLARE (pt::exception::NoListener, e, msg.str());
				this->getOwnerApplication()->notifyQualified("warning", e);
			}
		}
		else
		{
			std::stringstream msg;
			msg << "Unknown package signature " << signature << " from peer " << originatorsURL_[current_];
			LOG4CPLUS_FATAL (this->getOwnerApplication()->getApplicationLogger(), msg.str());
                        XCEPT_DECLARE (pt::tcp::exception::ReceiveError, pte, msg.str());
                        this->getOwnerApplication()->notifyQualified("error", pte);
			return;

			// ------------- DEAD CODE -------------------------------------------------------------------
			
			
		// -- I2O PACKAGE HANDLING
		//	
			I2O_MESSAGE_FRAME header;
			char * offset = ((char*)&header) + sizeof(uint32_t);
			this->safeReceive(offset, sizeof(I2O_MESSAGE_FRAME) - sizeof(uint32_t) );
			swapU32 ( (U32*) offset, sizeof(I2O_MESSAGE_FRAME) - sizeof(uint32_t) );
			memcpy (&header, &signature, sizeof(uint32_t));
			
			length = header.MessageSize << 2;
			
			if (autoSize_)
			{
				blockSize = length;
			}
			if (length > maxPacketSize_)
			{
				std::string msg = toolbox::toString ("Incoming packet with size %d exceeded maximum packet size %d", length, maxPacketSize_);
				this->close();
				XCEPT_DECLARE (pt::exception::MaxMessageSizeExceeded, e, msg);
				this->getOwnerApplication()->notifyQualified("error", e);
				return; // packet lost
			}
			ref = manager_->getFrame(pool_, blockSize);
			memcpy (ref->getDataLocation(), &header, sizeof(I2O_MESSAGE_FRAME));
			// never receive packets larget than maxPacketSize_
			this->safeReceive ( (char*) ( reinterpret_cast<size_t>(ref->getDataLocation()) + sizeof(I2O_MESSAGE_FRAME)), length - sizeof(I2O_MESSAGE_FRAME));
			
			
			if (listener_ != 0)
			{
				ref->setDataSize(length);
				listener_->processIncomingMessage ( ref );
			} 
			else
			{
				ref->release();
				XCEPT_DECLARE (pt::exception::NoListener, e, "No listener for TCP/B2IN found, message discarded");
				this->getOwnerApplication()->notifyQualified("warning", e);	
			}
		}
	}
	catch (pt::tcp::exception::ReceiveError & ex )
	{
		if (ref != 0) ref->release();
		this->close();
		size_t erased = netFilter_.clear(current_);
		std::stringstream msg;
		msg << "Failed to receive, cleared " << erased << " chains on socket " << current_ << " from peer " << originatorsURL_[current_];
		XCEPT_DECLARE_NESTED (pt::exception::ReceiveFailure, e, msg.str(), ex);
		this->getOwnerApplication()->notifyQualified("error", e);	
	}
	catch (pt::tcp::exception::ConnectionReset & e)
	{
		if (ref != 0) ref->release();
		this->close();
		
		size_t erased = netFilter_.clear(current_);
		std::stringstream msg;
		msg << "Connection reset by peer, cleared " << erased << " chains on socket " << current_ << " from peer " << originatorsURL_[current_];
		LOG4CPLUS_DEBUG (this->getOwnerApplication()->getApplicationLogger(), msg.str());
	} 
	catch (pt::tcp::exception::Exception & ex )
	{
		if (ref != 0) ref->release();
		this->close();
		size_t erased = netFilter_.clear(current_);
		std::stringstream msg;
		msg << "General receiver error, cleared " << erased << " chains on socket " << current_ << " from peer " << originatorsURL_[current_];
		XCEPT_DECLARE_NESTED (pt::exception::ReceiveFailure, e, msg.str(), ex);
		this->getOwnerApplication()->notifyQualified("error", e);	
	}
	catch (pt::exception::Exception& pte)
	{
		if (ref != 0) ref->release();
		this->close();
		size_t erased = netFilter_.clear(current_);
		std::stringstream msg;
		msg << "Failed to process incoming message, cleared " << erased << " chains on socket " << current_ << " from peer " << originatorsURL_[current_];
		XCEPT_DECLARE_NESTED (pt::exception::ReceiveFailure, e, msg.str(), pte);
		this->getOwnerApplication()->notifyQualified("error", e);	
	}
	catch (...) 
	{
		if (ref != 0) ref->release();
		this->close();
		size_t erased = netFilter_.clear(current_);
		std::stringstream msg;
		msg << "Unknown exception upon incoming message, cleared " << erased << " chains on socket " << current_ << " from peer " << originatorsURL_[current_];
		XCEPT_DECLARE (pt::exception::ReceiveFailure, e, msg.str());
		this->getOwnerApplication()->notifyQualified("error", e);		
	}	
}

