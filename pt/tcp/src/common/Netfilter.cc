// $Id: Netfilter.cc,v 1.5 2008/07/18 15:27:20 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "pt/tcp/Netfilter.h"
#include "toolbox/hexdump.h"

toolbox::mem::Reference * pt::tcp::Netfilter::append
(
	toolbox::mem::Reference* msg, 
	PTTCP_MESSAGE_FRAME & header,	
	uint32_t key
)
	throw (pt::tcp::exception::Exception)
{
	
	// For now assume that with TCP all chain elements from a "key" arrive in
	// sequence, e.g. 1/3, 2/3, 3/3
	
	// Trivial case: packet == total == 1 -> no chain
	if (header.total == 1)
	{
		if (header.packet == 1)
		{
			return msg;
		}
		else
		{
			std::stringstream msg;
			msg << "Packet (" << header.packet << ") and total packets (" << header.total << ")  do not match in single frame message";
			XCEPT_RAISE (pt::tcp::exception::Exception, msg.str());
		}
	}
	
	std::map<uint32_t, ChainDescriptor>::iterator i = chains_.find(key);
	if (i == chains_.end())
	{
		// Start a new chain
		if (header.packet == 1)
		{
		
			ChainDescriptor descriptor;
			descriptor.last = msg;
			descriptor.first = msg;
			descriptor.total = header.total;
			descriptor.packet = 1;
			descriptor.sequence = header.sequence;
			chains_[key] = descriptor;
			
			return 0; // chain not completed
		}
		else
		{
			std::stringstream msg;
			msg << "New chain: current packet number " << (size_t) header.packet << ", total expected " <<  (size_t) header.total;
			msg << ", sequence number: " << (size_t) header.sequence;
			
			
			XCEPT_RAISE (pt::tcp::exception::Exception, msg.str());
		}
	}
	else
	{
		toolbox::mem::Reference* last = (*i).second.last;
		if (header.packet == ((*i).second.packet + 1))
		{
			last->setNextReference(msg); // Chain the incoming reference to the existing one
			(*i).second.last = msg;
			
			if (header.packet == ( (*i).second.total))
			{
				toolbox::mem::Reference* reference = (*i).second.first;
				// Chain has been completely build: erase from internal map and return it
				chains_.erase(i);
				return reference;
			}
			else
			{
				// Chain not yet completed
				(*i).second.packet++;
				return 0;
			}
		}
		else
		{
			std::stringstream msg;
			msg << "Current packet number " << (size_t) header.packet << ", total expected " <<  (size_t) header.total;
			msg << ", last received: " <<(size_t)((*i).second.packet);
			msg << ", sequence number: " << (size_t) header.sequence;
			msg << ", expected sequence number: " << (size_t)((*i).second.sequence);
			

			//std::cout << msg.str() << std::endl;
			//toolbox::mem::Reference* ref = (*i).second.first;
			//toolbox::hexdump ((void*) ref->getDataLocation(),  ref->getDataSize());
			//std::cout << std::endl;
						
			XCEPT_RAISE (pt::tcp::exception::Exception, msg.str());
		}
	}
}

size_t pt::tcp::Netfilter::clear(uint16_t originator)
{
	std::list<uint32_t> eraseList; 
	size_t erased = 0;
	
	for  (std::map<uint32_t, ChainDescriptor>::iterator i = chains_.begin(); i != chains_.end(); i++)
	{
		if ( (uint16_t)((*i).first & 0x0000FFFF) == originator)
		{
			eraseList.push_back((*i).first);
		}
	}

	for (std::list<uint32_t>::iterator j = eraseList.begin(); j != eraseList.end(); j++ )
	{
			chains_[*j].first->release();
			chains_.erase(*j);
			++erased;
	}
	return erased;	
}
