// $Id: Channel.cc,v 1.8 2009/03/04 08:51:33 lorsini Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "pt/tcp/Channel.h"
#include "pt/tcp/Address.h"
#include "pt/tcp/exception/Exception.h"
#include "pt/Address.h"

#include <string.h>
#include <string>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netinet/tcp.h>
#include <netdb.h>

pt::tcp::Channel::Channel(pt::Address::Reference address) throw (pt::tcp::exception::Exception)
{
	pt::tcp::Address & a = dynamic_cast<tcp::Address&>(*address);
	
	sockaddress_ = a.getSocketAddress();
	sockaddressSize_ = sizeof (sockaddress_);

	if ( ! a.isSocketBound() )
	{
	
		errno = 0;
		socket_ = socket(AF_INET, SOCK_STREAM, 0);
   		if (socket_ == -1)
  		{
			XCEPT_RAISE (pt::tcp::exception::Exception, strerror(errno));
		}

		int optval = 1;
		errno = 0;
   		if (setsockopt(socket_, SOL_SOCKET, SO_REUSEADDR, (char *)&optval, sizeof(optval)) < 0)
  		{
			::close(socket_);
			socket_ = 0;
			XCEPT_RAISE (pt::tcp::exception::Exception, strerror(errno));
  		}
	
	}
	else
	{
		socket_ = a.getBoundSocket();

	}

/*

	errno = 0;
	struct linger li = {1,1};	
  	if (setsockopt(socket_, SOL_SOCKET, SO_LINGER, &li, sizeof(linger)) == -1) 
	{
		XCEPT_RAISE (pt::tcp::exception::Exception, strerror(errno));
        }
*/
	
	errno = 0;
	struct timeval tv = {20,0};	
  	if (setsockopt(socket_, SOL_SOCKET, SO_SNDTIMEO, &tv, sizeof(timeval)) == -1) 
	{
		XCEPT_RAISE (pt::tcp::exception::Exception, strerror(errno));
        }
}


pt::tcp::Channel::~Channel() 
{
	if (socket_ > 0) ::close(socket_);
}


