// $Id: B2INMessenger.cc,v 1.10 2008/08/26 15:00:53 lorsini Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/


#include "pt/tcp/B2INMessenger.h"
#include "pt/tcp/PeerTransportSender.h"
#include "pt/tcp/Address.h"
#include "pt/tcp/Transmitter.h"
#include "pt/tcp/msg.h"
#include "pt/tcp/Channel.h"
#include "toolbox/mem/Reference.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "xdata/exception/Exception.h"
#include "xdata/exdr/Serializer.h"
#include "xdata/exdr/FixedSizeOutputStreamBuffer.h"

pt::tcp::B2INMessenger::B2INMessenger(tcp::PeerTransportSender * pt,pt::Address::Reference destination, pt::Address::Reference local )
	throw (pt::tcp::exception::Exception)
{
	pt_ = pt;
	destination_ = destination;
	local_ = local;
	//channel_ = new tcp::Transmitter(destination_, local_);
	
	try
	{
		channel_ = pt->createChannel(destination_, local_);
	}
	catch (pt::tcp::exception::Exception & e)
	{
		XCEPT_RETHROW(pt::tcp::exception::Exception,"failed to create B2IN messenger", e);
	}
}

pt::tcp::B2INMessenger::~B2INMessenger()
{
	pt_->destroyChannel(channel_);
}

void pt::tcp::B2INMessenger::invalidate()
{
}

void pt::tcp::B2INMessenger::send
(
	toolbox::mem::Reference* msg,
	xdata::Properties & plist,
	toolbox::exception::HandlerSignature* handler,
	void* context
) 
	throw (b2in::nub::exception::InternalError, b2in::nub::exception::QueueFull, b2in::nub::exception::OverThreshold)
{
	xdata::exdr::Serializer serializer;
	
	if (pt_->getPool() == 0)
	{
		std::string msg = "Cannot access memory pool.";
		XCEPT_RAISE (b2in::nub::exception::InternalError, msg);
	}
	
	toolbox::mem::Reference* ref = 0;
	try
	{
		ref = toolbox::mem::getMemoryPoolFactory()->getFrame(pt_->getPool(), 4096);
	}
	catch (toolbox::mem::exception::Exception & e) 
	{
		XCEPT_RETHROW (b2in::nub::exception::InternalError, "failed to allocate properties block for b2in", e);
	}
	// insert addressing used for destination and originator
	plist.setProperty("urn:b2in-protocol-tcp:destination",destination_->toString());
	plist.setProperty("urn:b2in-protocol-tcp:originator",local_->toString());
	
	xdata::exdr::FixedSizeOutputStreamBuffer outBuffer((char*) ref->getDataLocation(), 4096);
	
	try 
	{
		serializer.exportAll (&plist, &outBuffer);
	}
	catch (xdata::exception::Exception & e )
	{
		ref->release();
		XCEPT_RETHROW (b2in::nub::exception::InternalError, "could not serialize B2IN properites", e);
	}
	
	ref->setDataSize(outBuffer.tellp());
	ref->setNextReference(msg);
	
	std::string connection =  plist.getProperty("urn:b2in-protocol-tcp:connection");
	pt::tcp::ConnectionType connectionType = pt::tcp::KeepAlive; //default
	if ( connection == "keep-alive" || connection == "" )
	{  
		connectionType = pt::tcp::KeepAlive;
	}
	else if (connection == "close" )
	{
		connectionType = pt::tcp::Close;
	}
 	else  	
	{
		XCEPT_RAISE(b2in::nub::exception::InternalError, "invalid connection attribute " + connection);
	}
	
	try
	{
		pt_->post (ref, channel_, handler, context, PTTCP_SIGNATURE, connectionType); // signature is b2in
	}
	catch (pt::tcp::exception::QueueFull & e)
	{
		ref->setNextReference(0); // property list buffer need to be free, the rest of the chain is responsibility of the caller
		ref->release();
		XCEPT_RETHROW (b2in::nub::exception::QueueFull, "failed to post packet", e);
	}
	catch (pt::tcp::exception::OverThreshold  & e)
	{
		ref->setNextReference(0); // property list buffer need to be free, the rest of the chain is responsibility of the caller
		ref->release();
		XCEPT_RETHROW (b2in::nub::exception::OverThreshold, "failed to post packet", e);
	}
	catch (pt::tcp::exception::NotActive & e)
	{
		ref->setNextReference(0); // property list buffer need to be free, the rest of the chain is responsibility of the caller
		ref->release();
		XCEPT_RETHROW (b2in::nub::exception::InternalError, "failed to post packet", e);
	}
	
}

pt::Address::Reference pt::tcp::B2INMessenger::getLocalAddress()
{
	return local_;
}

pt::Address::Reference pt::tcp::B2INMessenger::getDestinationAddress()
{
	return destination_;
}
