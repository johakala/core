// $Id: PeerTransportReceiver.cc,v 1.25 2009/03/04 09:07:48 lorsini Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/
#include <sys/ioctl.h> 
#include <sys/types.h>
#include <time.h>
#include <arpa/inet.h>
#include <ctype.h>
#include <dirent.h>
#include <dlfcn.h>
#include <fcntl.h>
#include <grp.h> 
#include <errno.h>
#include <libgen.h>
#include <limits.h>
#include <netdb.h>
#include <net/if.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <netinet/ip.h>
#include <pthread.h> 
#include <pwd.h> 


#include "pt/tcp/PeerTransportReceiver.h"
#include "pt/tcp/Address.h"
#include <iostream>
#include "i2o/Listener.h"
#include "b2in/nub/Listener.h"
#include "toolbox/net/Utils.h"

std::string pt::tcp::PeerTransportReceiver::getHostBySubnet(const std::string & subnet)
	throw (pt::exception::InvalidAddress)
{
	struct sockaddr_in      addr, mask; //bcast
	struct ifreq            *ifrp, *endp;
	struct ifconf           ifc;
	// 16 bytes for IPV4
	
	//char addrStr[16];
	//char bcastStr[16];
	//char maskStr[16];
	int  sockFd;
	
	struct sockaddr_in socka;
	inet_aton(subnet.c_str(), &socka.sin_addr);
	in_addr_t snet = inet_network(subnet.c_str());
	//std::cout << "snet is " << std::hex << snet << " compared to " << socka.sin_addr.s_addr  << " maps to " << inet_ntoa(socka.sin_addr)  << std::dec << std::endl;
	if ( snet == INADDR_NONE )
	{
  		std::stringstream msg;
         	msg << "malformed subnet specification " << subnet ;
        	XCEPT_RAISE(pt::exception::InvalidAddress, msg.str());
	}

	//  extract family mask from subnet ( limited to 255 precision  only)
	std::string fmask;
 	char* dq;
       	dq = (char*) &(socka.sin_addr.s_addr);
	for (int i = 0; i < 4; i++ )
	{
		if (((short) dq[i] & 0x00ff) != 0)
		{
			fmask += "255";
		}
		else
		{
		fmask += "0";
		}
		if ( i < 3 ) fmask += ".";
	}

	in_addr_t mnet = inet_network(fmask.c_str());
	//std::cout << "mnet is " << std::hex << mnet << " maps to " << fmask  << std::dec << std::endl;


	sockFd = socket(AF_INET, SOCK_DGRAM, 0);
	
	ifc.ifc_len = 16 * sizeof(struct ifreq);
	ifc.ifc_buf = (char*) new struct ifreq [16];
	if (ioctl(sockFd, SIOCGIFCONF, &ifc) < 0) 
	{
			
		close(sockFd);
  		std::stringstream msg;
         	msg << "cannot open interface device for search of subnet" << subnet ;
        	XCEPT_RAISE(pt::exception::InvalidAddress, msg.str());
	}
	
	ifrp = (struct ifreq*) ifc.ifc_buf;
	endp = (struct ifreq*) (&ifc.ifc_buf[ifc.ifc_len]);
	for (; ifrp < endp; ifrp++) 
	{
		
		if (ioctl(sockFd, SIOCGIFADDR, ifrp) < 0) 
		{
			continue;
		}
		memcpy(&addr, &ifrp->ifr_addr, sizeof(struct sockaddr));
		std::string host = inet_ntoa(addr.sin_addr);
		
		
		if (ioctl(sockFd, SIOCGIFNETMASK, ifrp) < 0) 
		{
			continue;
		}
		memcpy(&mask, &ifrp->ifr_addr, sizeof(struct sockaddr));
		in_addr_t foundaddr = addr.sin_addr.s_addr; 
		//std::cout << "found " << std::hex << foundaddr << " converted to ntoh " <<  ntohl(foundaddr) << " maps to "  << host <<  " and match is " << (snet  &  ntohl(foundaddr))<< std::dec << std::endl;
		if ((mnet  &  ntohl(foundaddr))  == snet  )
		{
			delete ifc.ifc_buf;
			close(sockFd);
			//std::cout << "match true for " << host <<  std::endl;
			return host;
		}		
	}
	delete ifc.ifc_buf;
	close(sockFd);
	std::stringstream msg;
	msg << "no IP found for subnet " << subnet;
	XCEPT_RAISE(pt::exception::InvalidAddress, msg.str());

	//      loopback = htonl(0x7F000000) & mask; 
}   

std::string pt::tcp::PeerTransportReceiver::getHostByInterface(const std::string & interface)
	throw (pt::exception::InvalidAddress)
{
	struct sockaddr_in      addr;
	//, bcast, mask;
	struct ifreq            *ifrp, *endp;
	struct ifconf           ifc;
	// 16 bytes for IPV4
	
	//char addrStr[16];
	//char bcastStr[16];
	//char maskStr[16];
	int  sockFd;
	
	sockFd = socket(AF_INET, SOCK_DGRAM, 0);
	
	ifc.ifc_len = 16 * sizeof(struct ifreq);
	ifc.ifc_buf = (char*) new struct ifreq [16];
	if (ioctl(sockFd, SIOCGIFCONF, &ifc) < 0) 
	{
			
		close(sockFd);
  		std::stringstream msg;
         	msg << "cannot open interface " << interface;
        	XCEPT_RAISE(pt::exception::InvalidAddress, msg.str());
	}
	
	ifrp = (struct ifreq*) ifc.ifc_buf;
	endp = (struct ifreq*) (&ifc.ifc_buf[ifc.ifc_len]);
	for (; ifrp < endp; ifrp++) 
	{
		
		if (ioctl(sockFd, SIOCGIFADDR, ifrp) < 0) 
		{
			continue;
		}
		memcpy(&addr, &ifrp->ifr_addr, sizeof(struct sockaddr));
		//mprInetNtoa(addrStr, sizeof(addrStr), addr.sin_addr);
		std::string host = inet_ntoa(addr.sin_addr);
		
		/*
		if (ioctl(sockFd, SIOCGIFBRDADDR, ifrp) < 0) 
		{
			continue;
		}
		memcpy(&bcast, &ifrp->ifr_broadaddr, sizeof(struct sockaddr));
		mprInetNtoa(bcastStr, sizeof(bcastStr), bcast.sin_addr);
		
		if (ioctl(sockFd, SIOCGIFNETMASK, ifrp) < 0) 
		{
			continue;
		}
		memcpy(&mask, &ifrp->ifr_addr, sizeof(struct sockaddr));
		mprInetNtoa(maskStr, sizeof(maskStr), mask.sin_addr);
		
		
		ipList.insert(new MprInterface(ifrp->ifr_name, addrStr, bcastStr,
									   maskStr));
		*/
		if (interface == ifrp->ifr_name)
		{
			delete ifc.ifc_buf;
			close(sockFd);
			return host;
		}		
	}
	delete ifc.ifc_buf;
	close(sockFd);
	std::stringstream msg;
	msg << "no IP found for interface " << interface;
	XCEPT_RAISE(pt::exception::InvalidAddress, msg.str());

	//      loopback = htonl(0x7F000000) & mask; 
}   

pt::tcp::PeerTransportReceiver::PeerTransportReceiver
(
	xdaq::Application* app,
	xdata::BooleanT autoSize, 
	xdata::UnsignedLongT maxPacketSize,
	xdata::UnsignedInteger32T maxClients
)
	:xdaq::Object(app)	
{
	autoSize_ = autoSize;
	maxPacketSize_ = maxPacketSize;
	maxClients_ = maxClients;
	pool_ = 0;
}

pt::tcp::PeerTransportReceiver::~PeerTransportReceiver()
{	
	// Remove all messengers. At this point none of them
	// should be touched by a user application anymore
	
	for (std::vector<tcp::ReceiverLoop*>::size_type i = 0; i < loop_.size(); i++)
	{
		// stop and delete the tasks
		#warning "Missing implementation for stopping and deleting receiver tasks."
		delete loop_[i];
	}
	
}

pt::TransportType pt::tcp::PeerTransportReceiver::getType()
{
	return pt::Receiver;
}

pt::Address::Reference pt::tcp::PeerTransportReceiver::createAddress ( const std::string& url, const std::string& service )
	throw (pt::exception::InvalidAddress)
{
	return pt::Address::Reference(new tcp::Address(url, service));
}

pt::Address::Reference 
pt::tcp::PeerTransportReceiver::createAddress( std::map<std::string, std::string, std::less<std::string> >& address )
	throw (pt::exception::InvalidAddress)
{
	bool isSocketBound = false;
	int listenfd = -3;

	std::string protocol = address["protocol"];
	
	if (protocol == "tcp")
	{
		std::stringstream url;
		url << protocol;
		
		std::string hostname = "";
		//XCEPT_ASSERT (address["hostname"] != "",pt::exception::InvalidAddress , "Cannot create address, hostname not specified");
		if ( address.find("hostname") !=  address.end())
		{
			hostname = address["hostname"];
		}
		else if (address.find("subnet") !=  address.end())
		{
			hostname = this->getHostBySubnet(address["subnet"]); // it re-throw the same exeption
		}
		else if (address.find("interface") !=  address.end())
		{
			hostname = this->getHostByInterface(address["interface"]); // it re-throw the same exeption
		}
		else
		{
			XCEPT_RAISE(pt::exception::InvalidAddress, "missing hostname/interface attribute");
		}
		
		XCEPT_ASSERT (address["port"] != "",pt::exception::InvalidAddress , "Cannot create address, port number not specified");
		std::string autoscan = "";
		if ( address.find("autoscan") !=  address.end())
		{
			autoscan = address["autoscan"];
		}
		
		url << "://" << hostname << ":";

		if ( autoscan == "true" )
		{
			uint16_t port = toolbox::toUnsignedLong(address["port"]);
			uint16_t maxport = port + 100;
			if ( address.find("maxport") !=  address.end())
			{
				maxport = toolbox::toUnsignedLong(address["maxport"]);
			}
			listenfd = socket(AF_INET,SOCK_STREAM,0);
			//int optval = 1;

/*
			if (setsockopt(listenfd, SOL_SOCKET, SO_REUSEADDR, (char *)&optval, sizeof(optval)) < 0)
			{
				XCEPT_RAISE (pt::exception::InvalidAddress, strerror(errno));
			}
*/

			errno = 0;
        		struct linger li = {1,1};
        		if (setsockopt(listenfd, SOL_SOCKET, SO_LINGER, &li, sizeof(linger)) == -1)
        		{
                		XCEPT_RAISE (pt::tcp::exception::Exception, strerror(errno));
        		}
			
			errno = 0;
			struct timeval tv = {10,0};	
  			if (setsockopt(listenfd, SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof(timeval)) == -1) 
			{
				XCEPT_RAISE (pt::tcp::exception::Exception, strerror(errno));
        		}	

			struct sockaddr_in serveraddr;
			uint16_t p;
			for (p = port; p <= maxport; p++ )
			{
				bzero(&serveraddr,sizeof(serveraddr));
				serveraddr = toolbox::net::getSocketAddressByName(hostname,p);
				errno = 0;
				if (::bind(listenfd, (struct sockaddr *)&serveraddr, sizeof(serveraddr)) == 0)
				{
					isSocketBound = true;
					break;
					//close(listenfd);
				}

							
			}
			if (p > maxport)
			{
				close(listenfd);
				XCEPT_RAISE (pt::exception::InvalidAddress, "cannot find free port in range");
			}
			//
			url << p;
		}
		else
		{
			url << address["port"];
		}	
		
		std::string service = address["service"];
		if (service != "")
		{
			if (!this->isServiceSupported(service))
			{
				std::string msg = "Cannot create address, specified service for protocol ";
				msg += protocol;
				msg += " not supported: ";
				msg += service;
				XCEPT_RAISE(pt::exception::InvalidAddress, msg);
			}
			
			//url += "/";
			//url += service;
		}
		
		pt::Address::Reference newAddress = this->createAddress(url.str(), service);
		if (isSocketBound)
		{
			dynamic_cast<pt::tcp::Address&>(*newAddress).bindSocket(listenfd);
		}
		return newAddress;
		
	}
	else 
	{
		std::string msg = "Cannot create address, protocol not supported: ";
		msg += protocol;
		XCEPT_RAISE(pt::exception::InvalidAddress, msg);
	}	
}

std::string pt::tcp::PeerTransportReceiver::getProtocol()
{
	return "tcp";
}

std::vector<std::string> pt::tcp::PeerTransportReceiver::getSupportedServices()
{
	std::vector<std::string> s;
	s.push_back("i2o");
	s.push_back("b2in");
	return s;
}

bool pt::tcp::PeerTransportReceiver::isServiceSupported(const std::string& service )
{
	if (service == "i2o") return true;
	if (service == "b2in") return true;
	else return false;
}


void pt::tcp::PeerTransportReceiver::config( pt::Address::Reference address) throw (pt::exception::Exception)
{
	// check if a receiver 
	for (std::vector<tcp::ReceiverLoop*>::size_type i = 0; i < loop_.size(); i++)
	{
		if (loop_[i]->getAddress()->equals(address))
		{
			std::string msg = "Receiver for address already configured: ";
			msg += address->toString();
			XCEPT_RAISE (pt::exception::Exception, msg);
		}
	}
	
	// Create a new receiver loop only with the address.
	// The pool, maxPacketSize and autosize parameters use only the default values inside the ReceiverLoop.
	// Therefore the ReceiverLoop is created, but the thread is not yet started.
	tcp::ReceiverLoop* loop = new tcp::ReceiverLoop(address, this);
	
	// attempt to add listener if present
	if ( this->isExistingListener(address->getService()) ) 
	{
		// I can safely get it now
		pt::Listener* l = this->getListener(address->getService());
	
		try
		{
			loop->addServiceListener(l);
		}
		catch(pt::exception::Exception & ex1)
		{
			// Fatal error, it should never happen
			XCEPT_RETHROW (pt::exception::Exception, "Cannot configure peer transport tcp", ex1);
		}
	}
	
	loop_.push_back(loop);	

	// Activate the new loop
	//
	if (pool_ == 0)
	{
		std::string msg = "Activation of TCP receiver thread failed. Missing memory pool.";
		XCEPT_RAISE (pt::exception::Exception, msg);
	}
	
	loop->setPool(pool_);
	loop->setMaxPacketSize(maxPacketSize_);
	loop->setAutoSize(autoSize_);
	loop->setMaxClients(maxClients_);
	
	try
	{
		loop->activate();
	}
	catch (pt::tcp::exception::Exception& pte)
	{
		XCEPT_RETHROW (pt::exception::Exception, "Failed to activate peer transport tcp", pte);
	}
	
}

void pt::tcp::PeerTransportReceiver::setPool(toolbox::mem::Pool * pool)
{
	pool_ = pool;
}

void pt::tcp::PeerTransportReceiver::setMaxPacketSize(xdata::UnsignedLongT maxPacketSize )
{
	maxPacketSize_ = maxPacketSize;
}
	
void pt::tcp::PeerTransportReceiver::setAutoSize(xdata::BooleanT autoSize )
{
	autoSize_ = autoSize;
}

void pt::tcp::PeerTransportReceiver::setMaxClients(xdata::UnsignedInteger32T maxClients )
{
	maxClients_ = maxClients;
}

void pt::tcp::PeerTransportReceiver::stop() throw (pt::exception::Exception)
{
	for (std::vector<tcp::ReceiverLoop*>::size_type i = 0; i < loop_.size(); i++)
	{
		loop_[i]->cancel(); // busy waiting until work loop terminated
	}
}

void pt::tcp::PeerTransportReceiver::start() throw (pt::exception::Exception)
{
	if (pool_ == 0)
	{
		std::string msg = "Activation of TCP receiver thread failed. Missing memory pool.";
		XCEPT_RAISE (pt::exception::Exception, msg);
	}

	// activate receiver loop as a thread at config point
	try 
	{
		for (std::vector<tcp::ReceiverLoop*>::size_type i = 0; i < loop_.size(); i++)
		{
			loop_[i]->setPool(pool_);
			loop_[i]->setMaxPacketSize(maxPacketSize_);
			loop_[i]->setAutoSize(autoSize_);
			loop_[i]->activate();
			
		}
	} 
	catch (...)
	{
		std::string msg = "Activation of TCP receiver thread failed.";
		XCEPT_RAISE (pt::exception::Exception, msg);
	}
}

// Overriding function to asynchrounously set the listener
//
void pt::tcp::PeerTransportReceiver::addServiceListener (pt::Listener* listener) throw (pt::exception::Exception)
{
	pt::PeerTransportReceiver::addServiceListener(listener);
	for (std::vector<tcp::ReceiverLoop*>::size_type i = 0; i < loop_.size(); i++)
	{
		loop_[i]->addServiceListener(listener);
	}
}

void pt::tcp::PeerTransportReceiver::removeServiceListener (pt::Listener* listener ) throw (pt::exception::Exception)
{
	pt::PeerTransportReceiver::removeServiceListener(listener);
	for (std::vector<tcp::ReceiverLoop*>::size_type i = 0; i < loop_.size(); i++)
	{
		loop_[i]->removeServiceListener(listener);
	}
}

void pt::tcp::PeerTransportReceiver::removeAllServiceListeners()
{
	pt::PeerTransportReceiver::removeAllServiceListeners();
	for (std::vector<tcp::ReceiverLoop*>::size_type i = 0; i < loop_.size(); i++)
	{
		loop_[i]->removeAllServiceListeners();
	}
}



///

    
