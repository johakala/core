// $Id: Transmitter.cc,v 1.15 2008/07/18 15:27:21 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "toolbox/hexdump.h"
#include "pt/tcp/Transmitter.h"
#include "pt/tcp/exception/Exception.h"

#include <string.h>
#include <string>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <arpa/inet.h>
#include <netinet/tcp.h>
#include <netdb.h>


pt::tcp::Transmitter::~Transmitter()
{
}

pt::tcp::Transmitter::Transmitter
(
	pt::Address::Reference remote,
	pt::Address::Reference local
) 
throw (pt::tcp::exception::Exception): tcp::Channel (remote)
{
	isConnected_ = false;
	retry_ = 0;

	errno = 0;
	struct sockaddr_in sockaddress_;
	tcp::Address & a = dynamic_cast<tcp::Address &>(*local);
	struct sockaddr_in  sockaddress = a.getSocketAddress();
	sockaddress.sin_port = htons(0);
	socklen_t sockaddressSize = sizeof (sockaddress_);
  
	if (::bind(socket_, (struct sockaddr *)&sockaddress, sockaddressSize) == -1)
	{
                std::string msg = "Cannot bind local address in tcp::Transmitter. ";
                msg += strerror(errno);
                ::close(socket_);
                socket_ = 0;
                XCEPT_RAISE(pt::tcp::exception::Exception, msg);
	}


}

bool pt::tcp::Transmitter::isConnected()  throw (pt::tcp::exception::Exception)
{
	return isConnected_;
}

void pt::tcp::Transmitter::connect()  throw (pt::tcp::exception::Exception)
{
	if ( isConnected_ ) return;
	
	if (socket_ == -1)
	{
		// recreate socket
		errno = 0;
		socket_ = socket(AF_INET, SOCK_STREAM, 0);
   		if (socket_ == -1)
  		{
			XCEPT_RAISE (pt::tcp::exception::Exception, strerror(errno));
		}
		int optval = 1;

 		int lowdelay = IPTOS_LOWDELAY;
		errno = 0;		
   		if (setsockopt(socket_, IPPROTO_IP, IP_TOS, (char *)&lowdelay, sizeof(optval)) < 0)
  		{
			XCEPT_RAISE (pt::tcp::exception::Exception, strerror(errno));
  		}		

		errno = 0;		
   		if (setsockopt(socket_, IPPROTO_TCP, TCP_NODELAY, (char *)&optval, sizeof(optval)) < 0)
  		{
			XCEPT_RAISE (pt::tcp::exception::Exception, strerror(errno));
  		}
		
		errno = 0;
		struct linger li = {1,1};	
  		if (setsockopt(socket_, SOL_SOCKET, SO_LINGER, &li, sizeof(linger)) == -1) 
		{
			XCEPT_RAISE (pt::tcp::exception::Exception, strerror(errno));
        	}
		
		errno = 0;
		struct timeval tv = {20,0};	
  		if (setsockopt(socket_, SOL_SOCKET, SO_SNDTIMEO, &tv, sizeof(timeval)) == -1) 
		{
			XCEPT_RAISE (pt::tcp::exception::Exception, strerror(errno));
        	}		
	}
	
	errno = 0;
        // algorithm for non-blocking connect from pt::atcp
        size_t ntry = 0;
        while (::connect(socket_, (struct sockaddr *)&sockaddress_, sockaddressSize_) == -1)
        {
                if( errno == EINPROGRESS || errno == EALREADY )
                {
                        // Tolerated one of the above
                }
                else
                {
                        ::close(socket_);
                        socket_ = -1; // force re-create of socket next time

                        std::stringstream msg;
                        msg << "Failed to connect to tcp://" << inet_ntoa(sockaddress_.sin_addr) << ":" << ntohs(sockaddress_.sin_port) << ", " << strerror(errno);
                        XCEPT_RAISE (pt::tcp::exception::Exception, msg.str());
                }

                ntry++;

                if( ntry >= 2000 )
                {
                        ::close(socket_);
                        socket_ = -1; // force re-create of socket next time

                        std::stringstream msg;
                        msg << "Failed to connect to tcp://" << inet_ntoa(sockaddress_.sin_addr) << ":" << ntohs(sockaddress_.sin_port) << " after " << ntry << " retries";
                        XCEPT_RAISE (pt::tcp::exception::Exception, msg.str());
                }

                // wait 10 milliseconds before retrying
                ::usleep(10000);
                errno = 0;                                                                                                                                       
        }                            
	
	isConnected_ = true;
}
	
	
void pt::tcp::Transmitter::disconnect()  throw (pt::tcp::exception::Exception)
{
	::close(socket_);
	socket_ = -1;
	isConnected_ = false;

}

void pt::tcp::Transmitter::close()  throw (pt::tcp::exception::Exception)
{
	this->disconnect();
}	
	
	
size_t pt::tcp::Transmitter::receive(char * buf ,size_t len ) throw (pt::tcp::exception::Exception)
{
	errno = 0;
	ssize_t r = ::recv (socket_, buf, len, 0);
	if (r < 0)
	{
		isConnected_ = false;
		XCEPT_RAISE (pt::tcp::exception::Exception, strerror(errno));
		
	}
	return r;
}
	
void pt::tcp::Transmitter::send(const char * buf, size_t len)  throw (pt::tcp::exception::Exception)
{
		this->connect();

		size_t toWrite = len;
        	ssize_t nBytes = 0;

        	while (toWrite > 0) 
		{
			errno = 0;
			nBytes = ::write(socket_,(char*)&buf[nBytes],toWrite);
			if (nBytes < 0)
			{
				std::stringstream msg;
				if ((errno == EAGAIN) || (errno == EWOULDBLOCK))
				{
					// TIMEOUT
					//std::cout << "SEND TIMEOUT on socket " << socket_ << " when sending " << len << " bytes" << std::endl;
					//toolbox::hexdump ((void*) buf, len);
					//std::cout << std::endl;
					std::stringstream msg;
				 	msg << "Send timeout for sending " << len << " bytes, "; 
					
				}

				msg << strerror(errno);
				this->close();
				XCEPT_RAISE (pt::tcp::exception::Exception, msg.str());
			}
			toWrite -= nBytes;
        	}
}
