// $Id: PeerTransportSender.cc,v 1.42 2009/02/16 14:39:34 rmoser Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xcept/tools.h"
#include "pt/tcp/PeerTransportSender.h"
#include "pt/tcp/I2OMessenger.h"
#include "pt/tcp/B2INMessenger.h"
#include "i2o/i2o.h"
#include "i2o/utils/endian.h"
#include "pt/tcp/msg.h"
#include "toolbox/BSem.h"
#include <string>
#include "pt/exception/InvalidFrame.h"
#include "toolbox/exception/Processor.h"
#include "toolbox/task/Action.h"
#include "toolbox/task/WorkLoopFactory.h"
#include "toolbox/task/WorkLoop.h"
#include "toolbox/utils.h"
#include "toolbox/TimeVal.h"

pt::tcp::PeerTransportSender::PeerTransportSender(xdaq::Application* owner, size_t qsize): 
Task("tcp/PeerTransportSender"), xdaq::Object(owner), outputQueue_(qsize,0.8), channelMutex_(toolbox::BSem::FULL,true)
{
	sentMessages_ = 0;
	sendTime_ = 0.0;
	kiloBytesSent_ = 0.0;
	
	try
	{	
 		toolbox::net::URN urn = this->getOwnerApplication()->createQualifiedInfoSpace("pt-tcp-sender");
  		monitorInfoSpace_ = xdata::getInfoSpaceFactory()->get(urn.toString());

		monitorInfoSpace_->fireItemAvailable("sentMessages", &sentMessages_);
		monitorInfoSpace_->fireItemAvailable("sendTime", &sendTime_);
		monitorInfoSpace_->fireItemAvailable("kiloBytesSent", &kiloBytesSent_);
	}
	catch (xdata::exception::Exception & e )
	{
		// ignore, signature has no throw
		XCEPT_DECLARE_NESTED(pt::tcp::exception::Exception, f,"failed to create monitoring infospace for sender", e );
		this->getOwnerApplication()->notifyQualified("fatal",f);
	}

	activated_ = false;
	pool_ = 0;
}

pt::tcp::PeerTransportSender::~PeerTransportSender()
{
	
}

void pt::tcp::PeerTransportSender::start()
{
	if ( ! activated_ ) 
	{
		this->activate();
		activated_ = true;
	}
}

void pt::tcp::PeerTransportSender::stop()
{
	if ( ! activated_)
	{
		return;
	}
	
	activated_ = false;
	
	done_ = false;
	// push empty record
	pt::tcp::PostDescriptor outputDescriptor;
	outputDescriptor.ref = 0;
	
	for(;;)
	{
		try
		{
			outputQueue_.push(outputDescriptor);
			break;
		} 
		catch (toolbox::exception::QueueFull& qfe)
		{
			// ignore and retry
		}
	}	
	
	while (! done_) 
	{
		// wait the task has really exited
		toolbox::u_sleep(100); // busy waiting		
	}
}

pt::Messenger::Reference pt::tcp::PeerTransportSender::getMessenger (pt::Address::Reference destination, pt::Address::Reference local)
 throw (pt::exception::UnknownProtocolOrService)
{
	// Look if a messenger from the local to the remote destination exists and return it.	
	// If it doesn't exist, create it and return it.
	// It accept the service to be null, in this case, it assume that it is soap service. Of course this is not secure but it provide a
	// useful omission as default
	if (((destination->getService()  == "i2o") || (destination->getService()  == "")) && 
	    ((local->getService() == "i2o")        || (local->getService() == "")) &&
		(destination->getProtocol()  == "tcp") && (local->getProtocol() == "tcp"))
	{
		
		// Create new messenger
		if ( local->equals(destination) )
		{
			// Creating a local messenger (within same logical host == executive) should not be supported.
			// In this case another transport protocol should be used.
			//
			// http::SOAPLoopbackMessenger* m = new http::SOAPLoopbackMessenger(destination, local);
			// soapMessengers_.push_back(m);
			// return m;
			
			std::string msg = "TCP protocol communication within the same XDAQ process is not supported. Use the fifo transport for local communication.";
			XCEPT_RAISE(pt::exception::UnknownProtocolOrService,msg);
		}
		else 
		{		
			// create remote messenger
			tcp::I2OMessenger* m = new tcp::I2OMessenger (this,destination, local);
			//i2oMessengers_.push_back(m);
			return pt::Messenger::Reference(m);
		}
	} else if (((destination->getService()  == "b2in") || (destination->getService()  == "")) &&
            ((local->getService() == "b2in")        || (local->getService() == "")) &&
                (destination->getProtocol()  == "tcp") && (local->getProtocol() == "tcp"))
	{
		 if ( local->equals(destination) )
                {
                        std::string msg = "TCP protocol communication within the same XDAQ process is not supported. Use the fifo transport for local communication.";
                        XCEPT_RAISE(pt::exception::UnknownProtocolOrService,msg);
                }
                else
                {
                        // create remote messenger
			try
			{
                        	pt::tcp::B2INMessenger* m = new pt::tcp::B2INMessenger (this,destination, local);
                        	return pt::Messenger::Reference(m);
			}
			catch (pt::tcp::exception::Exception & e)
			{
				XCEPT_RETHROW(pt::exception::UnknownProtocolOrService,"failed to get B2IN messenger",e );
			}
                }

	}
	else
	{
		std::string msg = "Cannot handle protocol service combination, destination protocol was :";
		msg +=  destination->getProtocol();
		msg += " destination service was:";
		msg +=  destination->getService();
		msg += " while local protocol was:";
		msg += local->getProtocol();
		msg += "and local service was:";
		msg += local->getService();
		
		XCEPT_RAISE(pt::exception::UnknownProtocolOrService,msg);
	}
}


pt::TransportType pt::tcp::PeerTransportSender::getType()
{
	return pt::Sender;
}

// The supported URL looks as follows: tcp://<hostname>:<port>[/<service>]
//
pt::Address::Reference pt::tcp::PeerTransportSender::createAddress( const std::string& url, const std::string& service )
	throw (pt::exception::InvalidAddress)
{
	return pt::Address::Reference (new tcp::Address(url, service));
}

pt::Address::Reference 
pt::tcp::PeerTransportSender::createAddress( std::map<std::string, std::string, std::less<std::string> >& address )
	throw (pt::exception::InvalidAddress)
{
	std::string protocol = address["protocol"];
	
	if (protocol == "tcp")
	{
		std::string url = protocol;
		
		XCEPT_ASSERT (address["hostname"] != "",pt::exception::InvalidAddress , "Cannot create address, hostname not specified");
		XCEPT_ASSERT (address["port"] != "",pt::exception::InvalidAddress , "Cannot create address, port number not specified");
		
		url += "://";
		url += address["hostname"];
		url += ":";
		url += address["port"];
		
		std::string service = address["service"];
		if (service != "")
		{
			if (!this->isServiceSupported(service))
			{
				std::string msg = "Cannot create address, specified service for protocol ";
				msg += protocol;
				msg += " not supported: ";
				msg += service;
				XCEPT_RAISE(pt::exception::InvalidAddress, msg);
			}
			
			//url += "/";
			//url += service;
		}
		
		return this->createAddress(url, service);
	}
	else 
	{
		std::string msg = "Cannot create address, protocol not supported: ";
		msg += protocol;
		XCEPT_RAISE(pt::exception::InvalidAddress, msg);
	}	
}

std::string pt::tcp::PeerTransportSender::getProtocol()
{
	return "tcp";
}

std::vector<std::string> pt::tcp::PeerTransportSender::getSupportedServices()
{
	std::vector<std::string> s;
	s.push_back("i2o");
	s.push_back("b2in");
	return s;
}

bool pt::tcp::PeerTransportSender::isServiceSupported(const std::string& service )
{
	if (service == "i2o") return true;
	if (service == "b2in") return true;
	else return false;
}


// sender services
void pt::tcp::PeerTransportSender::post
(
	toolbox::mem::Reference * ref,
	tcp::Channel * channel,
	toolbox::exception::HandlerSignature * handler,
	void * context, uint32_t signature,  const pt::tcp::ConnectionType& connection
)
 throw ( pt::tcp::exception::OverThreshold, pt::tcp::exception::QueueFull, pt::tcp::exception::NotActive)
{
	if ( ! activated_)
	{
		XCEPT_RAISE (pt::tcp::exception::NotActive, "peer transport sender loop is not activated");
	}
	
	tcp::PostDescriptor outputDescriptor;
	outputDescriptor.channel = channel;
	outputDescriptor.ref = ref;
	outputDescriptor.handler = handler;
	outputDescriptor.context = context;
	outputDescriptor.signature = signature;
	outputDescriptor.connection = connection;


	try
	{
		outputQueue_.push(outputDescriptor);
	} 
	catch (toolbox::exception::QueueFull& qfe)
	{
		XCEPT_RETHROW (pt::tcp::exception::QueueFull, "Failed to post message for sending, outputQueue full", qfe);
	}
	catch (toolbox::exception::OverThreshold& ote)
	{
		XCEPT_RETHROW (pt::tcp::exception::OverThreshold, "Failed to post message for sending, dead band", ote);
	}
	
}

int pt::tcp::PeerTransportSender::svc()
{
	for (;;)
	{
		// if no elements are found. it waits
		pt::tcp::PostDescriptor request = outputQueue_.pop();
		
		toolbox::TimeVal startTime = toolbox::TimeVal::gettimeofday();
		
		// Enqued reference 0 means exiting condition
		if (request.ref == 0 ) {
			done_ = true;
			return 0;
		}	
		
		channelMutex_.take();

		if ( ! this->hasChannel(request.channel) )
		{
			// skip
			if ( request.handler != 0 )
			{
				XCEPT_DECLARE(pt::tcp::exception::Exception, e, "invalid channel (message discared)");
				channelMutex_.give();
				request.handler->invoke(e,request.context);
			}
			else
			{			
				XCEPT_DECLARE(pt::tcp::exception::Exception, e, "invalid channel (message discared)");
				this->getOwnerApplication()->notifyQualified("error",e);
				channelMutex_.give();
			}
			
			request.ref->release();	
			continue;
		}
		
			
		try
		{
			toolbox::mem::Reference* ref = request.ref;
			uint8_t totalPackets = 0;
			while (ref != 0) { ref = ref->getNextReference(); totalPackets++;}
			
			identification_++; // identify the chain , increment for each chain being sent
			uint8_t packet = 1;	
			ref = request.ref;
			while (ref != 0)
			{
				size_t size = ref->getDataSize();

				// Check if size is 32 bit aligned
				if ((size & 0x3) != 0x0)
				{
					std::stringstream msg;
					msg << "Message size (" << size << ") is not 32 bit aligned. Program halts and needs inspection";
					XCEPT_DECLARE(pt::tcp::exception::Exception, e, msg.str());
					this->getOwnerApplication()->notifyQualified("fatal",e);
					request.ref->release();
					channelMutex_.give();
					return 0;
				}

				if ( request.signature == PTTCP_SIGNATURE )
				{
					PTTCP_MESSAGE_FRAME frame;
					frame.signature  = PTTCP_SIGNATURE;
					frame.length  = size >> 2;
					frame.packet  = packet++; // current packet out of total
					frame.total  = totalPackets;
					frame.identification = identification_; 
					frame.version = 0x1;
					frame.flags = 0x0;
					frame.qos = 0x00;
					frame.sequence  = sequence_++; // increment for each frame being sent
					frame.protocol = 0x6202696e; //hardcoded in messenger to 'b2in''   

				 	swapU32 ( (uint32_t*) &frame, sizeof(PTTCP_MESSAGE_FRAME));	
					
					request.channel->send((const char*) &frame, sizeof(PTTCP_MESSAGE_FRAME));					
					request.channel->send((const char*) ref->getDataLocation(), size);
					
					kiloBytesSent_ = kiloBytesSent_ + ((double)(size + sizeof(PTTCP_MESSAGE_FRAME))/1024.0);
					
					ref = ref->getNextReference();
				}
				else
				{
					XCEPT_DECLARE(pt::tcp::exception::Exception, e, "Attempt to send I2O message, not supported (use ptATCP)");
					this->getOwnerApplication()->notifyQualified("fatal",e);
					
					/*
					PI2O_MESSAGE_FRAME frame = (PI2O_MESSAGE_FRAME) ref->getDataLocation();

					if (size == static_cast<size_t>(frame->MessageSize << 2))
					{
						request.channel->send((const char*) frame, size);
						ref = ref->getNextReference();
						
						kiloBytesSent_ = kiloBytesSent_ + (size/1024.0); // monitoring
					}
					else
					{
						std::string msg =  toolbox::toString("Frame size %d and Reference data size %d  do not match (frame dropped)",frame->MessageSize << 2, size);
						request.ref->release();	
												
						XCEPT_DECLARE(pt::exception::InvalidFrame, e, msg);
						this->getOwnerApplication()->notifyQualified("error",e);
						
						break;
					}
					*/
				}
			}
		
			++sentMessages_; // monitoring

			// now release the chain
			request.ref->release();
			
			// keep-alive or close according
			if ( request.connection == pt::tcp::Close )
			{
				request.channel->disconnect();
			}
			channelMutex_.give();
		}
		catch (pt::tcp::exception::Exception & e ) 
		{
			LOG4CPLUS_ERROR( this->getOwnerApplication()->getApplicationLogger(), xcept::stdformat_exception_history(e));

			channelMutex_.give();

			this->destroyChannel(request.channel);
		
			if ( request.handler != 0 )
			{
				request.handler->invoke(e,request.context);
			}
			else
			{			
				std::stringstream msg; 
				msg << "Failed to send I2O message over ptTCP";
				XCEPT_DECLARE_NESTED(pt::tcp::exception::Exception, ex, msg.str(), e);
				this->getOwnerApplication()->notifyQualified("error",ex);
			}

			request.ref->release();	
		}

		toolbox::TimeVal stopTime = toolbox::TimeVal::gettimeofday();					
		sendTime_ = sendTime_ + ((double) stopTime - (double) startTime);	
	}
}

void pt::tcp::PeerTransportSender::setPool(toolbox::mem::Pool * pool)
{
	pool_ = pool;
}

toolbox::mem::Pool * pt::tcp::PeerTransportSender::getPool()
{
	return pool_;
}

pt::tcp::Channel * pt::tcp::PeerTransportSender::createChannel(pt::Address::Reference destination, pt::Address::Reference local)
	throw(pt::tcp::exception::Exception)
{
	channelMutex_.take();	
	try
	{
		pt::tcp::Channel * channel =  new tcp::Transmitter( destination,  local);
		channels_.insert(channel);
		channelMutex_.give();	
		return channel;
	}
	catch (pt::tcp::exception::Exception & e)
	{
		channelMutex_.give();
		XCEPT_RETHROW(pt::tcp::exception::Exception,"failed to crate channel for sending",e);
	}

}

bool pt::tcp::PeerTransportSender::hasChannel(pt::tcp::Channel * channel)
{
	channelMutex_.take();
	if ( channels_.find(channel) != channels_.end() )
	{
		channelMutex_.give();	
		return true;
	}
	else
	{
		channelMutex_.give();	
		return false;
	}
	
}

void pt::tcp::PeerTransportSender::destroyChannel(pt::tcp::Channel * channel)
{
	channelMutex_.take();
	std::set<pt::tcp::Channel*>::iterator i = channels_.find(channel);
	if ( i != channels_.end() )
	{
		delete (*i);
		channels_.erase(i);
	}
	channelMutex_.give();	
}






