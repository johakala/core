// $Id: I2OMessenger.cc,v 1.9 2008/07/18 15:27:20 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "pt/tcp/I2OMessenger.h"
#include "pt/tcp/PeerTransportSender.h"
#include "pt/tcp/Address.h"
#include "pt/tcp/Transmitter.h"

pt::tcp::I2OMessenger::I2OMessenger(tcp::PeerTransportSender * pt,pt::Address::Reference destination, pt::Address::Reference local )
{
	pt_ = pt;
	destination_ = destination;
	local_ = local;
	//channel_ = new tcp::Transmitter(destination_, local_);
	channel_ = pt->createChannel(destination_, local_);
}

pt::tcp::I2OMessenger::~I2OMessenger()
{
	//delete channel_;
}

void pt::tcp::I2OMessenger::send (toolbox::mem::Reference* msg, toolbox::exception::HandlerSignature* handler,  void* context) throw (pt::exception::Exception)
{
	pt_->post (msg, channel_, handler, context, 0x00000000, pt::tcp::KeepAlive); // i2o is not defined
}

pt::Address::Reference pt::tcp::I2OMessenger::getLocalAddress()
{
	return local_;
}

pt::Address::Reference pt::tcp::I2OMessenger::getDestinationAddress()
{
	return destination_;
}
