// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "pt/tcp/PeerTransportTCP.h"
#include "pt/PeerTransportAgent.h"
#include "xoap/SOAPName.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/domutils.h"
#include "toolbox/fsm/FailedEvent.h"
#include "toolbox/mem/HeapAllocator.h"
#include "toolbox/mem/CommittedHeapAllocator.h"
#include "toolbox/TimeVal.h"
#include "xoap/Method.h"
#include "xdaq/NamespaceURI.h"
#include "xcept/tools.h"
#include "xdata/InfoSpaceFactory.h"

XDAQ_INSTANTIATOR_IMPL(pt::tcp::PeerTransportTCP)

pt::tcp::PeerTransportTCP::PeerTransportTCP(xdaq::ApplicationStub * s)
 throw(xdaq::exception::Exception)
 : xdaq::Application(s), xgi::framework::UIManager(this)
{	
	

	s->getDescriptor()->setAttribute("icon","/pt/tcp/images/pt-tcp-icon.png");
	// Bind CGI callbacks
	xgi::framework::deferredbind(this, this, &PeerTransportTCP::Default, "Default");
	xgi::framework::deferredbind(this, this, &PeerTransportTCP::apply, "apply");

	committedPoolSize_ = 0x100000 * 50; // 5 MB
	highThreshold_  = 0.9;
	lowThreshold_ = 0.8;
	maxClients_ = 1000;
	outputQueueSize_ = 4096;
	getApplicationInfoSpace()->fireItemAvailable("autoSize",&autosize_);
	getApplicationInfoSpace()->fireItemAvailable("maxPacketSize",&maxPacketSize_);
	getApplicationInfoSpace()->fireItemAvailable("maxClients",&maxClients_);
	getApplicationInfoSpace()->fireItemAvailable("poolName",&poolName_);
	getApplicationInfoSpace()->fireItemAvailable("committedPoolSize",&committedPoolSize_);
	getApplicationInfoSpace()->fireItemAvailable("lowThreshold",&lowThreshold_);
	getApplicationInfoSpace()->fireItemAvailable("highThreshold",&highThreshold_);
	getApplicationInfoSpace()->fireItemAvailable("outputQueueSize",&outputQueueSize_);

	autosize_ = true;
	maxPacketSize_ = 65628;
	poolName_ = "urn:toolbox-mem-pool-gc:PeerTransportTCP/heap";
	
		// Add infospace listeners for exporting data values
	getApplicationInfoSpace()->addItemChangedListener ("autoSize", this);
	getApplicationInfoSpace()->addItemChangedListener ("maxPacketSize", this);
	getApplicationInfoSpace()->addItemChangedListener ("poolName", this);
	getApplicationInfoSpace()->addItemChangedListener ("maxClients", this);
	
	// Bind setting of default parameters
	getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");

}

void pt::tcp::PeerTransportTCP::actionPerformed (xdata::Event& e) 
{ 
	if (e.type() == "urn:xdaq-event:setDefaultValues")
	{
                toolbox::net::URN urn(poolName_);

		try 
		{
			//toolbox::mem::HeapAllocator* a = new toolbox::mem::HeapAllocator();
			toolbox::mem::CommittedHeapAllocator* a = new toolbox::mem::CommittedHeapAllocator(committedPoolSize_);
			toolbox::mem::Pool* pool = toolbox::mem::getMemoryPoolFactory()->createPool(urn, a);	
			pool->setHighThreshold ( (unsigned long) (committedPoolSize_ * highThreshold_));
			pool->setLowThreshold ((unsigned long) (committedPoolSize_ * lowThreshold_));
			
		}
		catch (toolbox::mem::exception::Exception & e)
		{
			XCEPT_RETHROW (xdaq::exception::Exception, "Cannot create default memory pool", e);
		}
		
		pts_ = new pt::tcp::PeerTransportSender(this,outputQueueSize_);
		ptr_ = new pt::tcp::PeerTransportReceiver(this, autosize_,maxPacketSize_, maxClients_);
		pt::PeerTransportAgent* pta = pt::getPeerTransportAgent();
		pta->addPeerTransport(pts_);
		pta->addPeerTransport(ptr_);
		
		try
		{
			this->reset();
		}
		catch (pt::exception::Exception & e)
		{
			XCEPT_RETHROW (xdaq::exception::Exception, "Cannot reset peer transport", e);
		}
		
	}
		
}		

void pt::tcp::PeerTransportTCP::Default(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception)
{

	std::string url = "/";
	url += getApplicationDescriptor()->getURN();
	url += "/apply";	

	this->StatisticsTabPage(out);	
	*out << "<hr><p>" << std::endl;

	*out << cgicc::form().set("method","post")
			.set("action", url)
			.set("enctype","multipart/form-data") << std::endl;


	// Receiver configuration
	//
	*out << cgicc::fieldset().set("style","font-size: 10pt; font-family: arial; background-color: rgb(255,255,255);") << std::endl;
	*out << cgicc::legend("Receiver Configuration") << std::endl;
	
	*out << cgicc::table();
	*out << cgicc::tbody();

    *out << cgicc::tr();
    *out << cgicc::td();
	// Get all pool names and put them into a drop down box
	// The first entry 'default' will create or assign a default pool
	*out << cgicc::label("Receiving Memory Pool") << std::endl;
    *out << cgicc::td();
    *out << cgicc::td();
	*out << cgicc::select().set("name","poolName") << std::endl;
	
	std::vector<std::string> poolNames = toolbox::mem::getMemoryPoolFactory()->getMemoryPoolNames();

	//*out << cgicc::option("PeerTransportTCP/heap").set("selected") << std::endl;

	for (std::vector<std::string>::size_type i = 0; i < poolNames.size(); i++)
	{
		if ( poolName_ == poolNames[i] )
			*out << cgicc::option(poolNames[i]).set("selected") << std::endl;
		else
			*out << cgicc::option(poolNames[i]) << std::endl;	
	}
	
	*out << cgicc::select();

    *out << cgicc::td();
	*out << cgicc::tr() <<  std::endl;


	*out << cgicc::tr() <<  std::endl;
    *out << cgicc::td();
	// autosize=true
	*out << cgicc::label("Adjust receiver buffer to incoming message size") << std::endl;
    *out << cgicc::td();
    *out << cgicc::td();
	if ( autosize_ )
	{
		*out << cgicc::input().set("type", "checkbox")
				.set("name","autosize")
				.set("checked", "true") << std::endl;
	}
	else
	{
		*out << cgicc::input().set("type", "checkbox")
				.set("name","autosize") << std::endl;
	}			

    *out << cgicc::td();
    *out << cgicc::tr() <<  std::endl;
    *out << cgicc::tr() <<  std::endl;
    *out << cgicc::td();

	// maxpacketsize
	*out << cgicc::label("Maximum allowed incoming message size (Bytes)");
    *out << cgicc::td();
    *out << cgicc::td();
	*out << cgicc::input().set("type","text")
				.set("name","maxPacketSize")
				.set("size","6")
				.set("value", maxPacketSize_.toString()) << std::endl;

    *out << cgicc::tr() <<  std::endl;
 	*out << cgicc::tbody();
        *out << cgicc::table();
	*out << cgicc::fieldset() << std::endl;

	*out << cgicc::form() << std::endl;
}

void pt::tcp::PeerTransportTCP::StatisticsTabPage
(
	xgi::Output * out
) 
throw (xgi::exception::Exception)
{
   	xdata::getInfoSpaceFactory()->lock();


	 *out << cgicc::table().set("class", "xdaq-table");
 	*out << cgicc::thead();
        *out << cgicc::tr();
        *out << cgicc::th("Endpoint URL");
        *out << cgicc::th("Receive Time");
        *out << cgicc::th("Received Messages");
        *out << cgicc::th("kB Received");
        *out << cgicc::tr(); 
        *out << cgicc::thead();

        *out << cgicc::tbody();

	std::map<std::string, xdata::Serializable *, std::less<std::string> > spaces = xdata::getInfoSpaceFactory()->match("pt-tcp-receiver");
	for (  std::map<std::string, xdata::Serializable *, std::less<std::string> >::iterator i = spaces.begin(); i != spaces.end(); ++i)
         {
		xdata::InfoSpace* is = dynamic_cast<xdata::InfoSpace *> ((*i).second);
		xdata::String * endpointURL = dynamic_cast<xdata::String*>(is->find("endpointURL"));
		xdata::Double* receiveTime = dynamic_cast<xdata::Double*>(is->find("receiveTime"));
		xdata::UnsignedInteger32* messagesReceived = dynamic_cast<xdata::UnsignedInteger32*>(is->find("receivedMessages"));
		xdata::Double* kiloBytesReceived = dynamic_cast<xdata::Double*>(is->find("kiloBytesReceived"));
		*out << cgicc::tr();
                *out << cgicc::td(endpointURL->toString());
                *out << cgicc::td(receiveTime->toString());
                *out << cgicc::td(messagesReceived->toString());
                *out << cgicc::td(kiloBytesReceived->toString());
  		*out << cgicc::tr();
	 }

 	*out << cgicc::tbody();
        *out << cgicc::table();

    	*out << "<br />";

    	*out << cgicc::table().set("class", "xdaq-table");
        *out << cgicc::thead();
        *out << cgicc::tr();
        *out << cgicc::th("Send Time");
        *out << cgicc::th("Sent Messages");
        *out << cgicc::th("kB Sent");
        *out << cgicc::tr(); 
        *out << cgicc::thead();

        *out << cgicc::tbody();

	 spaces = xdata::getInfoSpaceFactory()->match("pt-tcp-sender");
	 for (  std::map<std::string, xdata::Serializable *, std::less<std::string> >::iterator i = spaces.begin(); i != spaces.end(); ++i)
         {
		xdata::InfoSpace* is = dynamic_cast<xdata::InfoSpace *> ((*i).second);
		xdata::Double* sendTime = dynamic_cast<xdata::Double*>(is->find("sendTime"));
		xdata::UnsignedInteger32* messagesSent = dynamic_cast<xdata::UnsignedInteger32*>(is->find("sentMessages"));
		xdata::Double* kiloBytesSent = dynamic_cast<xdata::Double*>(is->find("kiloBytesSent"));
		*out << cgicc::tr();
                *out << cgicc::td(sendTime->toString());
                *out << cgicc::td(messagesSent->toString());
                *out << cgicc::td(kiloBytesSent->toString());
  		*out << cgicc::tr();
	 }
	*out << cgicc::tbody();
        *out << cgicc::table();

    	*out << "<br />";


   	xdata::getInfoSpaceFactory()->unlock();
}


// called back when pt configuration form is submitted
void pt::tcp::PeerTransportTCP::apply(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception)
{

	try 
	 {
		// Create a new Cgicc object containing all the CGI data
		cgicc::Cgicc cgi(in);
		
		bool hasAutosize = cgi.queryCheckbox("autosize");
		if (hasAutosize)
		{
			autosize_ = true;
		}
		else
		{
			autosize_ = false;
		}
		poolName_ = cgi["poolName"]->getValue();
		maxPacketSize_ = cgi["maxPacketSize"]->getIntegerValue();
		
		this->reset();
		
		this->Default(in,out);
		
	}
	catch(pt::exception::Exception& xe) 
	{
		this->failurePage(out, xe);

	}
	catch(const std::exception& e) 
	{
		xgi::exception::Exception nex("xgi::exception::Exception", e.what(), __FILE__, __LINE__,__FUNCTION__);
		this->failurePage(out, nex);

	}
}

	
	
//
// Failure Pages
//
void pt::tcp::PeerTransportTCP::failurePage(xgi::Output * out, xcept::Exception & e)  throw (xgi::exception::Exception)
{
	*out << cgicc::br() << e.what() << cgicc::br() << std::endl;
	std::string url = "/";
	url += getApplicationDescriptor()->getURN();
	
	*out << cgicc::br() << "<a href=\"" << url << "\">" << "retry" << "</a>" << cgicc::br() << std::endl;
}

pt::tcp::PeerTransportTCP::~PeerTransportTCP()
{
	pt::PeerTransportAgent* pta = pt::getPeerTransportAgent();	
	pta->removePeerTransport(pts_);
	pta->removePeerTransport(ptr_);	
	delete pts_;	
	delete ptr_;		
}


void pt::tcp::PeerTransportTCP::reset() throw (pt::exception::Exception)
{
	LOG4CPLUS_INFO (getApplicationLogger(), "stop receiver and sender threads");
	// stop receiver and sender
	ptr_->stop();
	pts_->stop();
	
	// apply new paramaterization
	ptr_->setMaxPacketSize(maxPacketSize_);
	ptr_->setAutoSize(autosize_);
	
	try 
	{
		// try to retrieve indicated pool through manager
		toolbox::net::URN urn(poolName_);
		toolbox::mem::Pool * p = toolbox::mem::getMemoryPoolFactory()->findPool(urn);
		ptr_->setPool(p);
		pts_->setPool(p);
	}
	catch (toolbox::net::exception::MalformedURN & e)
	{
		std::string msg = "Malformed URN ";
		msg += poolName_;
		XCEPT_RETHROW (pt::exception::Exception, msg, e);
	}
	catch (toolbox::mem::exception::MemoryPoolNotFound & e)
	{
		std::string msg = "Cannot access memory pool named ";
		msg += poolName_;
		XCEPT_RETHROW (pt::exception::Exception, msg, e);
	}	
	
	// re-start the reading thread with the provided configuration
	ptr_->start();
	pts_->start();
	
	LOG4CPLUS_INFO (getApplicationLogger(), "start receiver and sender threads");
}
