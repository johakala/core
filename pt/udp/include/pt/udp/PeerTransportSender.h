// $Id: PeerTransportSender.h,v 1.2 2007/11/13 10:06:05 lorsini Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _pt_udp_PeerTransportSender_h
#define _pt_udp_PeerTransportSender_h

#include <vector>
#include <map>
#include <string>

#include "pt/PeerTransportSender.h"
#include "pt/PeerTransportReceiver.h"
#include "pt/PeerTransportAgent.h"
#include "toolbox/squeue.h"
#include "pt/udp/Channel.h"
#include "pt/udp/PostDescriptor.h"
#include "toolbox/mem/Reference.h"
#include "i2o/Messenger.h"
#include "toolbox/Task.h"

#include "xdata/InfoSpaceFactory.h"
#include "xdata/UnsignedInteger32.h"
#include "xdata/Double.h"
#include "xdata/Table.h"

#include "xdaq/Object.h"

namespace pt 
{
namespace udp 
{

class PeerTransportSender: public pt::PeerTransportSender, public toolbox::Task, public xdaq::Object
{
	public:
	
	PeerTransportSender(xdaq::Application*);
	virtual ~PeerTransportSender();
	
	pt::TransportType getType();
	
	pt::Address::Reference createAddress( const std::string& url, const std::string& service )
		throw (pt::exception::InvalidAddress);
	
	pt::Address::Reference createAddress( std::map<std::string, std::string, std::less<std::string> >& address )
		throw (pt::exception::InvalidAddress);
	
	std::string getProtocol();
	
	std::vector<std::string> getSupportedServices();	

	bool isServiceSupported(const std::string& service );
	
	pt::Messenger::Reference getMessenger (pt::Address::Reference destination, pt::Address::Reference local)
		throw (pt::exception::UnknownProtocolOrService);
	
	//! sender loop service routine, run in a thread
	int svc();
	
	//! queue a request for send
	void post(toolbox::mem::Reference * ref, 
			pt::udp::Channel * channel, 
			toolbox::exception::HandlerSignature * handler, 
			void * context)
		throw (pt::exception::Exception);

	void start();
	void stop();
	
	private:
	
		toolbox::squeue<pt::udp::PostDescriptor> outputQueue_;
		bool activated_;
		bool done_;

		// Monitoring Variables
		xdata::InfoSpace* monitorInfoSpace_;
		xdata::UnsignedInteger32 sentMessages_;
		xdata::Double kiloBytesSent_;
		xdata::Double sendTime_;

		//use to store packetsize for each endpoint?
		xdata::Table sendTable_;
};

}
}

#endif
