// $Id: CannotConnect.h,v 1.2 2007/11/13 10:06:05 lorsini Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _pt_udp_exception_CannotConnect_h_
#define _pt_udp_exception_CannotConnect_h_

#include "pt/udp/exception/Exception.h"

namespace pt {
namespace udp {
	namespace exception { 
		class CannotConnect: public pt::udp::exception::Exception 
		{
			public: 
			CannotConnect ( std::string name, std::string message, std::string module, int line, std::string function ): 
					pt::udp::exception::Exception(name, message, module, line, function) 
			{} 
			
			CannotConnect ( std::string name, std::string message, std::string module, int line, std::string function,
				xcept::Exception& e ): 
					pt::udp::exception::Exception(name, message, module, line, function, e) 
			{} 
		}; 
	} 
}
}
#endif
