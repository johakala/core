// $Id: version.h,v 1.1 2007/11/12 10:20:34 lorsini Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _pt_udp_version_h_
#define _pt_udp_version_h_

#include "config/PackageInfo.h"
// !!! Edit this line to reflect the latest package version !!!

#define PTUDP_VERSION_MAJOR 1
#define PTUDP_VERSION_MINOR 0
#define PTUDP_VERSION_PATCH 0
// If any previous versions available E.g. #define PTUDP_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#undef PTUDP_PREVIOUS_VERSIONS 


//
// Template macros
//
#define PTUDP_VERSION_CODE PACKAGE_VERSION_CODE(PTUDP_VERSION_MAJOR,PTUDP_VERSION_MINOR,PTUDP_VERSION_PATCH)
#ifndef PTUDP_PREVIOUS_VERSIONS
#define PTUDP_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(PTUDP_VERSION_MAJOR,PTUDP_VERSION_MINOR,PTUDP_VERSION_PATCH)
#else 
#define PTUDP_FULL_VERSION_LIST  PTUDP_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(PTUDP_VERSION_MAJOR,PTUDP_VERSION_MINOR,PTUDP_VERSION_PATCH)
#endif 


namespace ptudp
{
	const std::string package  =  "ptudp";
   	const std::string versions =  PTUDP_FULL_VERSION_LIST;
	const std::string summary = "UDP peer transport with I2O service and SOAP implementation";
	const std::string description = "Does not support multicasting";
	const std::string authors = "Johannes Gutleber, Luciano Orsini";
	const std::string link = "http://xdaqwiki.cern.ch/index.php/xdaq";
	config::PackageInfo getPackageInfo();
	void checkPackageDependencies() throw (config::PackageInfo::VersionException);
	std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
