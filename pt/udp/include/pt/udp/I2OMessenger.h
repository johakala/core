// $Id: I2OMessenger.h,v 1.2 2007/11/13 10:06:04 lorsini Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _pt_udp_I2OMessenger_h
#define _pt_udp_I2OMessenger_h

#include "i2o/Messenger.h"
#include "pt/Address.h"
#include "pt/udp/PeerTransportSender.h"
#include "pt/udp/Channel.h"
#include "toolbox/mem/Reference.h"

namespace pt
{
namespace udp
{

class PeerTransport;

class I2OMessenger: public i2o::Messenger
{
	public:
	
	I2OMessenger(pt::udp::PeerTransportSender * pt, pt::Address::Reference destination, pt::Address::Reference local);
	virtual ~I2OMessenger();
	
	pt::Address::Reference getLocalAddress();
	pt::Address::Reference getDestinationAddress();
	
	void send (toolbox::mem::Reference* msg, toolbox::exception::HandlerSignature* handler, void* context) 
		throw (pt::exception::Exception);
	
	private:
	
	pt::udp::PeerTransportSender * pt_;
	pt::udp::Channel * channel_;
	pt::Address::Reference destination_;
	pt::Address::Reference local_;
};

}
}

#endif

