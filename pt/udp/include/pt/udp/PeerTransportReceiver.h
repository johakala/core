    // $Id: PeerTransportReceiver.h,v 1.2 2007/11/13 10:06:05 lorsini Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _pt_udp_PeerTransportReceiver_h
#define _pt_udp_PeerTransportReceiver_h

#include <string>
#include <vector>

#include "toolbox/mem/Pool.h"
#include "pt/PeerTransportReceiver.h"
#include "pt/exception/Exception.h"
#include "pt/udp/Address.h"
#include "pt/udp/ReceiverLoop.h"

#include "xdaq/Object.h"

// Log4CPLUS
#include "log4cplus/logger.h"
#include "log4cplus/consoleappender.h"
#include "log4cplus/helpers/appenderattachableimpl.h"
#include "log4cplus/helpers/loglog.h"
#include "log4cplus/helpers/pointer.h"
#include "log4cplus/spi/loggingevent.h"
#include "log4cplus/layout.h"

using namespace log4cplus;
using namespace log4cplus::helpers;
using namespace log4cplus::spi;

namespace pt
{
namespace udp 
{

//! There is only one peer transport receiver. It has a number of ReceiverLoop
//! objects, one per receiver port
//
class PeerTransportReceiver: public pt::PeerTransportReceiver, xdaq::Object
{
	public:
	
	PeerTransportReceiver(xdaq::Application*, xdata::UnsignedLongT maxPacketSize, Logger& logger);
	~PeerTransportReceiver();
	
	pt::TransportType getType();
	
	pt::Address::Reference createAddress( const std::string& url, const std::string& service )
		throw (pt::exception::InvalidAddress);
	
	pt::Address::Reference 
	createAddress( std::map<std::string, std::string, std::less<std::string> >& address )
		throw (pt::exception::InvalidAddress);
	
	void addServiceListener (pt::Listener* listener) throw (pt::exception::Exception);
	void removeServiceListener (pt::Listener* listener ) throw (pt::exception::Exception);
	void removeAllServiceListeners();
	
	std::string getProtocol();
	
	std::vector<std::string> getSupportedServices();	

	bool isServiceSupported(const std::string& service );
	
	void config (pt::Address::Reference address) throw (pt::exception::Exception);
	void start() throw (pt::exception::Exception);
	void stop() throw (pt::exception::Exception);
	
	void setPool(toolbox::mem::Pool * pool);
	void setMaxPacketSize(xdata::UnsignedLongT maxPacketSize );
		
	protected:
	
	xdata::UnsignedLongT maxPacketSize_;			
	std::vector<pt::udp::ReceiverLoop*> loop_;
	toolbox::mem::Pool * pool_;
	Logger	logger_;
};

}
}

#endif
