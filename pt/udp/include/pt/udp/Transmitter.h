// $Id: Transmitter.h,v 1.2 2007/11/13 10:06:05 lorsini Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _pt_udp_Transmitter_h_
#define _pt_udp_Transmitter_h_

#include <netinet/in.h>

#include "pt/udp/Address.h"
#include "pt/udp/exception/Exception.h"
#include "pt/udp/Channel.h"

namespace pt
{
namespace udp
{

const unsigned int MaxRetry = 3;

class Transmitter : public udp::Channel
{
	public:
	
	Transmitter(pt::Address::Reference remote, pt::Address::Reference local) throw (pt::udp::exception::Exception);
		
	~Transmitter();
	
	//! connect channel according configuration
	void connect() throw (pt::udp::exception::Exception);
	
	//! disconnect but keep channel alive
	void disconnect() throw (pt::udp::exception::Exception);
	
	//! receive len characters into buf
	size_t receive(char * buf ,size_t len ) throw (pt::udp::exception::Exception);
	
	//! send buffer of given lenght
	void send(const char * buf, size_t len) throw (pt::udp::exception::Exception);
	
	//! Close a connection definitely
	void close() throw (pt::udp::exception::Exception);
		
	//! Check if the connection is up
	bool isConnected() throw (pt::udp::exception::Exception);
	
	bool isConnected_;
	unsigned int retry_;

	struct sockaddr_in localSockAddress_, remoteSockAddress_;
	socklen_t sockAddressSize_;
};


}
}

#endif
