// $Id: PostDescriptor.h,v 1.2 2007/11/13 10:06:05 lorsini Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _pt_udp_PostDescriptor_h_
#define _pt_udp_PostDescriptor_h_

#include "toolbox/mem/Reference.h"
#include "pt/udp/Channel.h"
#include "toolbox/exception/Handler.h"

namespace pt {
namespace udp {
	
	
	struct PostDescriptor {
		toolbox::mem::Reference * ref;
		pt::udp::Channel * channel;
		toolbox::exception::HandlerSignature * handler;
		void * context;
	}; 
	
	
}
}
#endif
