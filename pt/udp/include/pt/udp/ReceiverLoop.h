// $Id: ReceiverLoop.h,v 1.2 2007/11/13 10:06:05 lorsini Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _pt_udp_ReceiverLoop_h_
#define _pt_udp_ReceiverLoop_h_

#include <vector>
#include <sys/select.h>

#include "pt/udp/Address.h"
#include "pt/udp/Channel.h"
#include "pt/udp/exception/Exception.h"
#include "pt/Listener.h" 
#include "i2o/Listener.h" 
#include "toolbox/mem/MemoryPoolFactory.h"
#include "toolbox/mem/Pool.h"
#include "toolbox/task/WorkLoop.h"
#include "toolbox/lang/Class.h"

#include "xdata/UnsignedLong.h"
#include "xdata/Boolean.h"
#include "xdata/UnsignedInteger32.h"
#include "xdata/String.h"
#include "xdata/Double.h"
#include "xdata/InfoSpaceFactory.h"
#include "xdata/Table.h"

#include "xdaq/Object.h"

// Log4CPLUS
#include "log4cplus/logger.h"
#include "log4cplus/consoleappender.h"
#include "log4cplus/helpers/appenderattachableimpl.h"
#include "log4cplus/helpers/loglog.h"
#include "log4cplus/helpers/pointer.h"
#include "log4cplus/spi/loggingevent.h"
#include "log4cplus/layout.h"

#include  "i2o/i2o.h"

using namespace log4cplus;
using namespace log4cplus::helpers;
using namespace log4cplus::spi;

namespace pt 
{
namespace udp
{

class ReceiverLoop:  public pt::udp::Channel, public toolbox::lang::Class, xdaq::Object
{
	enum { MaxNoChannels = 1024 };

	public:
	
	ReceiverLoop(pt::Address::Reference address, Logger& logger, xdaq::Object* obj)
		throw (pt::udp::exception::Exception);
	
	~ReceiverLoop();
		
	// override channel receive
	size_t receive(char * buf ,size_t len ) throw (pt::udp::exception::Exception);
	
	void send(const char * buf, size_t len) throw (pt::udp::exception::Exception) {;}
	
	void disconnect() throw (pt::udp::exception::Exception);
	
	void connect() throw (pt::udp::exception::Exception) {;}
	
	void close() throw (pt::udp::exception::Exception);
	
	//! work loop job routine
	bool process(toolbox::task::WorkLoop * wl);

	//! Get the address of this receiver (can be casted to a tcp::Address&)
	pt::Address::Reference getAddress();
	
	void addServiceListener (pt::Listener* listener);
	void removeServiceListener (pt::Listener* listener);
	void removeAllServiceListeners();
	
	void setPool(toolbox::mem::Pool * pool);
	void setMaxPacketSize(xdata::UnsignedLongT maxPacketSize );
	void setAutoSize(xdata::BooleanT autoSize );
	void activate();
	void cancel();
	
	private:
	
	int accept() throw (pt::udp::exception::Exception);
	
	bool isConnected() throw (pt::udp::exception::Exception) { return true; } // there is no connection
	bool isActive() throw (pt::udp::exception::Exception);
	
	void onRequest() throw (pt::udp::exception::Exception);
	
	void safeReceive(char * buf, size_t len) throw (pt::udp::exception::Exception);

	std::vector<int> sockets_;
	pt::Address::Reference  address_; // reference counted pt address
	fd_set fdset_;
	fd_set allset_;
	struct timeval timeout_;
	bool done_;
	bool stop_;
	int maxfd_;
	int current_;
	int listenfd_;
	int accepted_;
	int nochannels_;
	int nready_;
	xdata::UnsignedLongT maxPacketSize_;
	
	i2o::Listener* i2oListener_;
	toolbox::mem::Pool* pool_;
	toolbox::mem::MemoryPoolFactory* manager_;
	toolbox::task::ActionSignature* process_;		
	Logger logger_;	

	//struct to hold reply details
	struct sockaddr_in sockaddressReply_;

	// Monitoring Variables
	xdata::InfoSpace * monitorInfoSpace_;
	xdata::UnsignedInteger32 receivedMessages_;
	xdata::Double kiloBytesReceived_;
	xdata::Double receiveTime_;
	xdata::String endpointURL_;

	//std::map<int, int> temp_;
};

}
}
#endif
