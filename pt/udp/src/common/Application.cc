// $Id: Application.cc,v 1.1 2007/11/12 14:40:40 lorsini Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                             *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					                 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                             *
 * For the list of contributors see CREDITS.   			                 *
 *************************************************************************/

#include "pt/udp/Application.h"
#include "pt/PeerTransportAgent.h"
#include "xoap/SOAPName.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/domutils.h"
#include "toolbox/fsm/FailedEvent.h"
#include "toolbox/mem/HeapAllocator.h"
#include "xoap/Method.h"
#include "xdaq/NamespaceURI.h"
#include "xcept/tools.h"

XDAQ_INSTANTIATOR_IMPL(pt::udp::Application)

pt::udp::Application::Application(xdaq::ApplicationStub * s)
 throw(xdaq::exception::Exception)
 : xdaq::WebApplication(s)
{	
	

	getApplicationDescriptor()->setAttribute("icon","/pt/images/PeerTransport64x64.gif");
	// Bind CGI callbacks
	xgi::bind(this, &Application::Default, "Default");
	xgi::bind(this, &Application::apply, "apply");

	getApplicationInfoSpace()->fireItemAvailable("autoSize",&autosize_);
	getApplicationInfoSpace()->fireItemAvailable("maxPacketSize",&maxPacketSize_);
	getApplicationInfoSpace()->fireItemAvailable("poolName",&poolName_);
	autosize_ = true;
	maxPacketSize_ = 65628;

	toolbox::net::URN urn("toolbox-mem-pool", "pt-udp-default-heap");
	poolName_ = urn.toString();
	try 
	{
		toolbox::mem::HeapAllocator* a = new toolbox::mem::HeapAllocator();
		toolbox::mem::getMemoryPoolFactory()->createPool(urn, a);			
	}
	catch (toolbox::mem::exception::Exception & e)
	{
		XCEPT_RETHROW (xdaq::exception::Exception, "Cannot create default memory pool", e);
	}

	pts_ = new pt::udp::PeerTransportSender();
	ptr_ = new pt::udp::PeerTransportReceiver(autosize_,maxPacketSize_, getApplicationLogger());
	pt::PeerTransportAgent* pta = pt::getPeerTransportAgent();
	pta->addPeerTransport(pts_);
	pta->addPeerTransport(ptr_);
	
	try
	{
		this->reset();
	}
	catch (pt::exception::Exception & e)
	{
		XCEPT_RETHROW (xdaq::exception::Exception, "Cannot reset peer transport", e);
	}
	// Add infospace listeners for exporting data values
	getApplicationInfoSpace()->addItemChangedListener ("autoSize", this);
	getApplicationInfoSpace()->addItemChangedListener ("maxPacketSize", this);
	getApplicationInfoSpace()->addItemChangedListener ("poolName", this);	
}

void pt::udp::Application::actionPerformed (xdata::Event& e) 
{ 
	// update measurements monitors		
	if (e.type() == "ItemChangedEvent")
	{
		std::string item = dynamic_cast<xdata::ItemChangedEvent&>(e).itemName();
		if ( item == "poolName" || item == "autoSize" || item == "maxPacketSize")
		{
			try 
			{
				this->reset();	
			}
			catch(pt::exception::Exception & e)
			{
				LOG4CPLUS_ERROR (getApplicationLogger(), xcept::stdformat_exception_history(e));
			}
		}
	}	
}		

void pt::udp::Application::Default(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception)
{
	out->getHTTPResponseHeader().addHeader("Content-Type", "text/html");
	*out << cgicc::HTMLDoctype(cgicc::HTMLDoctype::eStrict) << std::endl;
	*out << cgicc::html().set("lang", "en").set("dir","ltr") << std::endl;
	*out << cgicc::title("Configure TCP Peer Transport") << std::endl;
	
	xgi::Utils::getPageHeader
		(out, 
		"Configure TCP Peer Transport", 
		getApplicationDescriptor()->getContextDescriptor()->getURL(),
		getApplicationDescriptor()->getURN(),
		"/pt/images/PeerTransport32x32.gif"
		);
	
	std::string url = "/";
	url += getApplicationDescriptor()->getURN();
	url += "/apply";	

	*out << cgicc::form().set("method","post")
			.set("action", url)
			.set("enctype","multipart/form-data") << std::endl;

	// Receiver configuration
	//
	*out << cgicc::fieldset().set("style","font-size: 10pt; font-family: arial; background-color: rgb(255,255,255);") << std::endl;
	*out << cgicc::legend("Receiver Configuration") << std::endl;
	
	// Get all pool names and put them into a drop down box
	// The first entry 'default' will create or assign a default pool
	*out << cgicc::label("Receiving Memory Pool") << std::endl;
	*out << cgicc::select().set("name","poolName") << std::endl;
	
	std::vector<std::string> poolNames = toolbox::mem::getMemoryPoolFactory()->getMemoryPoolNames();

	//*out << cgicc::option("Application/heap").set("selected") << std::endl;

	for (std::vector<std::string>::size_type i = 0; i < poolNames.size(); i++)
	{
		if ( poolName_ == poolNames[i] )
			*out << cgicc::option(poolNames[i]).set("selected") << std::endl;
		else
			*out << cgicc::option(poolNames[i]) << std::endl;	
	}
	
	*out << cgicc::select()  <<  cgicc::p() <<  std::endl;	
	
	
	// autosize=true
	*out << cgicc::label("Adjust receiver buffer to incoming message size") << std::endl;
	if ( autosize_ )
	{
		*out << cgicc::input().set("type", "checkbox")
				.set("name","autosize")
				.set("checked", "true") << cgicc::p() << std::endl;
	}
	else
	{
		*out << cgicc::input().set("type", "checkbox")
				.set("name","autosize") << cgicc::p() << std::endl;
	}			
	// maxpacketsize
	*out << cgicc::label("Maximum allowed incoming message size (Bytes)");
	*out << cgicc::input().set("type","text")
				.set("name","maxPacketSize")
				.set("size","6")
				.set("value", maxPacketSize_.toString()) << cgicc::p() << std::endl;
	
	*out << cgicc::fieldset() << std::endl;
	*out << cgicc::p() << std::endl;

	// Sender configuration
	//
	*out << cgicc::fieldset().set("style","font-size: 10pt; font-family: arial; background-color: rgb(255,255,255);") << std::endl;
	*out << cgicc::legend("Sender Configuration") << std::endl;
	
	*out << cgicc::label("No configuration in this version of ptTCP");
	
	*out << cgicc::fieldset() << std::endl;
	*out << cgicc::p() << std::endl;

	
	*out << cgicc::input().set("type", "submit")
	 			.set("name", "apply")
				.set("value", "Apply")
				.set("style","font-size: 10pt; font-family: arial; background-color: rgb(255,255,255);");
				
	*out << cgicc::p() << std::endl;
	*out << cgicc::form() << std::endl;

	xgi::Utils::getPageFooter(*out);
}


// called back when pt configuration form is submitted
void pt::udp::Application::apply(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception)
{

	try 
	 {
		// Create a new Cgicc object containing all the CGI data
		cgicc::Cgicc cgi(in);
		
		bool hasAutosize = cgi.queryCheckbox("autosize");
		if (hasAutosize)
		{
			autosize_ = true;
		}
		else
		{
			autosize_ = false;
		}
		poolName_ = cgi["poolName"]->getValue();
		maxPacketSize_ = cgi["maxPacketSize"]->getIntegerValue();
		
		this->reset();
		
		this->Default(in,out);
		
	}
	catch(pt::exception::Exception& xe) 
	{
		this->failurePage(out, xe);

	}
	catch(const std::exception& e) 
	{
		xgi::exception::Exception nex("xgi::exception::Exception", e.what(), __FILE__, __LINE__,__FUNCTION__);
		this->failurePage(out, nex);

	}
}

	
	
//
// Failure Pages
//
void pt::udp::Application::failurePage(xgi::Output * out, xcept::Exception & e)  throw (xgi::exception::Exception)
{
	out->getHTTPResponseHeader().addHeader("Content-Type", "text/html");
	*out << cgicc::HTMLDoctype(cgicc::HTMLDoctype::eStrict) << std::endl;
	*out << cgicc::html().set("lang", "en").set("dir","ltr") << std::endl;

	xgi::Utils::getPageHeader
		(out, 
		"Peer Transport TCP Failure", 
		getApplicationDescriptor()->getContextDescriptor()->getURL(),
		getApplicationDescriptor()->getURN(),
		"/pt/tcp/images/Application.gif"
		);

	*out << cgicc::br() << e.what() << cgicc::br() << std::endl;
	std::string url = "/";
	url += getApplicationDescriptor()->getURN();
	
	*out << cgicc::br() << "<a href=\"" << url << "\">" << "retry" << "</a>" << cgicc::br() << std::endl;

	xgi::Utils::getPageFooter(*out);
}

pt::udp::Application::~Application()
{
	pt::PeerTransportAgent* pta = pt::getPeerTransportAgent();	
	pta->removePeerTransport(pts_);
	pta->removePeerTransport(ptr_);	
	delete pts_;	
	delete ptr_;		
}


void pt::udp::Application::reset() throw (pt::exception::Exception)
{
	LOG4CPLUS_INFO (getApplicationLogger(), "stop receiver and sender threads");
	// stop receiver and sender
	ptr_->stop();
	pts_->stop();
	
	// apply new paramaterization
	ptr_->setMaxPacketSize(maxPacketSize_);
	ptr_->setAutoSize(autosize_);
	
	try 
	{
		// try to retrieve indicated pool through manager
		toolbox::net::URN urn(poolName_);
		toolbox::mem::Pool * p = toolbox::mem::getMemoryPoolFactory()->findPool(urn);
		ptr_->setPool(p);
	}
	catch (toolbox::net::exception::MalformedURN & e)
	{
		std::string msg = "Malformed URN ";
		msg += poolName_;
		XCEPT_RETHROW (pt::exception::Exception, msg, e);
	}
	catch (toolbox::mem::exception::MemoryPoolNotFound & e)
	{
		std::string msg = "Cannot access memory pool named ";
		msg += poolName_;
		XCEPT_RETHROW (pt::exception::Exception, msg, e);
	}	
	
	// re-start the reading thread with the provided configuration
	ptr_->start();
	pts_->start();
	
	LOG4CPLUS_INFO (getApplicationLogger(), "start receiver and sender threads");
}
