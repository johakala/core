// $Id: ReceiverLoop.cc,v 1.3 2007/11/13 10:06:06 lorsini Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                             *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					                 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                             *
 * For the list of contributors see CREDITS.   			                 *
 *************************************************************************/

#include "pt/udp/ReceiverLoop.h"

#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <iostream>
#include <errno.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>

#include  "i2o/i2o.h"
#include "i2o/utils/endian.h"

#include "pt/exception/Exception.h"
#include "pt/udp/exception/Exception.h"
#include "pt/udp/exception/CannotConnect.h"
#include "toolbox/exception/Processor.h"
#include "pt/exception/MaxMessageSizeExceeded.h"
#include "pt/exception/NoListener.h"
#include "pt/exception/ReceiveFailure.h"
#include "toolbox/task/WorkLoopFactory.h"
#include "toolbox/task/WorkLoop.h"
#include "toolbox/utils.h"
#include "toolbox/hexdump.h"
#include "toolbox/TimeVal.h"


void pt::udp::ReceiverLoop::setPool(toolbox::mem::Pool * pool)
{
	pool_ = pool;
}

void pt::udp::ReceiverLoop::setMaxPacketSize(xdata::UnsignedLongT maxPacketSize )
{
	maxPacketSize_ = maxPacketSize;
}

pt::udp::ReceiverLoop::ReceiverLoop(pt::Address::Reference  address, Logger& logger, xdaq::Object* obj)
	throw (pt::udp::exception::Exception) : 
	pt::udp::Channel(address), xdaq::Object(*obj), logger_(logger)
{
	manager_ = toolbox::mem::getMemoryPoolFactory();

	pool_ = 0;
	i2oListener_ = 0;
	address_ = address;
	
	errno = 0;
	
	if (::bind(socket_, (struct sockaddr *)&sockaddress_, sockaddressSize_) == -1)
   	{
		std::string msg = toolbox::toString("%s on url: %s",strerror(errno),address->toString().c_str() );
		::close(socket_);
		socket_ = 0;
		XCEPT_RAISE(pt::udp::exception::Exception, msg);
   	}
	
	// bind workloop job method
	process_ = toolbox::task::bind (this, &ReceiverLoop::process, "process");

	receivedMessages_ = 0;
	kiloBytesReceived_ = 0;
	receiveTime_ = 0.0;
	endpointURL_ = address->toString();

	try
	{
		toolbox::net::URN urn = this->getOwnerApplication()->createQualifiedInfoSpace("pt-udp-receiver");
		monitorInfoSpace_ = xdata::getInfoSpaceFactory()->get(urn.toString());

		monitorInfoSpace_->fireItemAvailable("endpointURL", &endpointURL_);
		monitorInfoSpace_->fireItemAvailable("receivedMessages", &receivedMessages_);
		monitorInfoSpace_->fireItemAvailable("receiveTime", &receiveTime_);
		monitorInfoSpace_->fireItemAvailable("kiloBytesReceived", &kiloBytesReceived_);
	}
	catch (xdata::exception::Exception & e )
	{
		XCEPT_DECLARE_NESTED(pt::udp::exception::Exception, f,"failed to create monitoring infospace for udp receiver", e );
		this->getOwnerApplication()->notifyQualified("fatal",f);
	}

/*	temp_[23] = 0;
	temp_[24] = 0;*/
}

pt::udp::ReceiverLoop::~ReceiverLoop()
{
	this->disconnect();
}

void pt::udp::ReceiverLoop::addServiceListener (pt::Listener* listener)
{
	if (listener->getService() == "i2o")
	{
		i2oListener_ = dynamic_cast<i2o::Listener *>(listener);
	}
	else if (listener->getService() == "soap")
	{
	//	soapListener_ = dynamic_cast<soap::Listener *>(listener);
	}
}

void pt::udp::ReceiverLoop::removeServiceListener (pt::Listener* listener)
{
	if (listener->getService() == "i2o")
	{
		i2oListener_ = 0;
	}
	if (listener->getService() == "soap")
	{
	//	soapListener_ = 0;
	}
	
}

void pt::udp::ReceiverLoop::removeAllServiceListeners()
{
	i2oListener_ = 0;
    //soapListener_ = 0;

}

void pt::udp::ReceiverLoop::activate()
{
	stop_ = false;
	done_ = false;
	std::string name = "urn:toolbox-task-workloop:";
	name += address_->toString();
	toolbox::task::getWorkLoopFactory()->getWorkLoop(name, "waiting")->submit(process_);
	toolbox::task::getWorkLoopFactory()->getWorkLoop(name, "waiting")->activate();
}

void pt::udp::ReceiverLoop::cancel()
{
	done_ = false;
	stop_ = true;
	// wait for done
	while (! done_ )
	{
		toolbox::u_sleep(100);
		// busy waiting
	}
	stop_ = false;
	std::string name = "urn:toolbox-task-workloop:";
	name += address_->toString();
	toolbox::task::getWorkLoopFactory()->getWorkLoop(name, "waiting")->cancel();
}

pt::Address::Reference pt::udp::ReceiverLoop::getAddress()
{
	return address_;
}
	

	
	
size_t pt::udp::ReceiverLoop::receive(char * buf, size_t len ) throw (pt::udp::exception::Exception) 
{
	ssize_t length = 0;

	if ( socket_ >= 0 )
	{
		errno = 0;

		//std::cout << "Going to Receive: " << len << std::endl;

		length = ::recvfrom(socket_, buf, len, 0, NULL, NULL);

		if (length == -1)
		{
			std::string msg = strerror(errno);
			XCEPT_RAISE(pt::udp::exception::Exception, msg );

		}

	}

	return length;

}

void pt::udp::ReceiverLoop::disconnect()  throw (pt::udp::exception::Exception)
{
	::close(socket_);
	socket_ = -1;
}

void pt::udp::ReceiverLoop::close()  throw (pt::udp::exception::Exception)
{
	this->disconnect();
}
	
	
bool pt::udp::ReceiverLoop::process (toolbox::task::WorkLoop * wl) 
{
	if (stop_)
	{
		done_ = true;

		//std::cout << "stopping receiver" << std::endl;

		return false; // end loop
	}
	//std::cout << "Going to onRequest" << std::endl;
	this->onRequest();
	return true;
}

void pt::udp::ReceiverLoop::onRequest() throw (pt::udp::exception::Exception) 
{
	toolbox::TimeVal startTime = toolbox::TimeVal::gettimeofday();

	toolbox::mem::Reference* ref = 0;
	I2O_PRIVATE_MESSAGE_FRAME* framePtr = 0;

	try 
	{
		size_t blockSize = maxPacketSize_;
		ref = manager_->getFrame(pool_, blockSize);

		//receive the message from the socket
		//std::cout << "Going to Socket Receive" << std::endl;
		this->receive((char*)ref->getDataLocation(), blockSize);
		framePtr = (I2O_PRIVATE_MESSAGE_FRAME*) ref->getDataLocation();


		size_t length = framePtr->StdMessageFrame.MessageSize << 2;

		//send the reference to the i20 listener if it exists
		if (i2oListener_ != 0)
		{
			ref->setDataSize(length);

			kiloBytesReceived_ = kiloBytesReceived_ + (length / 1000.0);

			++receivedMessages_; // monitoring

			/*int tid = framePtr->StdMessageFrame.InitiatorAddress;

			temp_[tid] = temp_[tid] + 1;*/

			receiveTime_ = receiveTime_ + ((double) toolbox::TimeVal::gettimeofday() - (double) startTime);
			//toolbox::hexdump ((void*) ref->getDataLocation(), length);

			i2oListener_->processIncomingMessage ( ref );

			//std::cout << temp_[23] << " " << temp_[24] << std::endl;
		} 
		else
		{
			ref->release();

			receiveTime_ = receiveTime_ + ((double) toolbox::TimeVal::gettimeofday() - (double) startTime);

			pt::exception::NoListener e("pt::exception::NoListener", "no listener for UDP/I2O available, message discared.", __FILE__, __LINE__, __FUNCTION__);
			toolbox::exception::getProcessor()->push(e, 0, 0 );	
		}
	}
	catch (pt::exception::Exception& pte)
	{
		std::cout << pte.what() << std::endl;
		if (ref != 0)
		{
			//std::cout << "crash " << ref << std::endl;
			ref->release();

		}
		this->close();
		toolbox::exception::getProcessor()->push(pte, 0, 0 );	
	}
	catch (...) 
	{
	//debug
			//std::cout << ref << std::endl;
		if (ref != 0) ref->release();
		std::string msg = "Caught unknown exception in processIncomingMessage, failed to receive packet in peer transport UDP";
		this->close();
		pt::exception::ReceiveFailure e("pt::exception::ReceiveFailure", msg, __FILE__, __LINE__, __FUNCTION__);
		toolbox::exception::getProcessor()->push(e, 0, 0 );	
	}	
	//std::cout << "All requests finished" << std::endl;
}

