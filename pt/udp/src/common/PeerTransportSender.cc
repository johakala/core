// $Id: PeerTransportSender.cc,v 1.2 2007/11/12 14:39:49 lorsini Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                             *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					                 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                             *
 * For the list of contributors see CREDITS.   			                 *
 *************************************************************************/

#include "xcept/tools.h"
#include "pt/udp/PeerTransportSender.h"
#include "pt/udp/I2OMessenger.h"
#include "i2o/i2o.h"
#include "toolbox/BSem.h"
#include <string>
#include "pt/exception/InvalidFrame.h"
#include "toolbox/exception/Processor.h"
#include "toolbox/task/Action.h"
#include "toolbox/task/WorkLoopFactory.h"
#include "toolbox/task/WorkLoop.h"
#include "toolbox/utils.h"
#include "toolbox/TimeVal.h"

pt::udp::PeerTransportSender::PeerTransportSender(xdaq::Application* owner): Task("udp/PeerTransportSender"), xdaq::Object(owner), outputQueue_(100000)
{
	activated_ = false;

	//monitoring
	sentMessages_ = 0;
	sendTime_ = 0.0;
	kiloBytesSent_ = 0.0;

	try
	{
		toolbox::net::URN urn = this->getOwnerApplication()->createQualifiedInfoSpace("pt-udp-sender");
		monitorInfoSpace_ = xdata::getInfoSpaceFactory()->get(urn.toString());

		monitorInfoSpace_->fireItemAvailable("sentMessages", &sentMessages_);
		monitorInfoSpace_->fireItemAvailable("sendTime", &sendTime_);
		monitorInfoSpace_->fireItemAvailable("kiloBytesSent", &kiloBytesSent_);
		monitorInfoSpace_->fireItemAvailable("sendTable", &sendTable_);
	}
	catch (xdata::exception::Exception & e )
	{
		// ignore, signature has no throw
		XCEPT_DECLARE_NESTED(pt::udp::exception::Exception, f,"failed to create monitoring infospace for sender", e );
		this->getOwnerApplication()->notifyQualified("fatal",f);
	}
}

pt::udp::PeerTransportSender::~PeerTransportSender()
{
	
}

void pt::udp::PeerTransportSender::start()
{
	if (!activated_)
	{
		this->activate(); //start task (thread)
		activated_ = true;
	}
}

void pt::udp::PeerTransportSender::stop()
{
	if (!activated_)
	{
		return;
	}
	
	activated_ = false;
	
	done_ = false;
	// push empty record to stop once last packet has been sent see svc
	pt::udp::PostDescriptor outputDescriptor;
	outputDescriptor.ref = 0;
	
	for(;;)
	{
		try
		{
			outputQueue_.push(outputDescriptor);
			break;
		} 
		catch (toolbox::exception::QueueFull& qfe)
		{
			// ignore and retry
		}
	}	
	
	while (! done_) 
	{
		// wait the task has really exited
		toolbox::u_sleep(100); // busy waiting		
	}
}

pt::Messenger::Reference pt::udp::PeerTransportSender::getMessenger (pt::Address::Reference destination, pt::Address::Reference local)
 throw (pt::exception::UnknownProtocolOrService)
{
	// Look if a messenger from the local to the remote destination exists and return it.	
	// If it doesn't exist, create it and return it.
	// It accept the service to be null, in this case, it assume that it is soap service. Of course this is not secure but it provide a
	// useful omission as default
	if (((destination->getService()  == "i2o") || (destination->getService()  == "")) && 
	    ((local->getService() == "i2o")        || (local->getService() == "")) &&
		(destination->getProtocol()  == "udp") && (local->getProtocol() == "udp"))
	{
		
		// Create new messenger
		if ( local->equals(destination) )
		{
			// Creating a local messenger (within same logical host == executive) should not be supported.
			// In this case another transport protocol should be used.
			//
			// http::SOAPLoopbackMessenger* m = new http::SOAPLoopbackMessenger(destination, local);
			// soapMessengers_.push_back(m);
			// return m;
			
			std::string msg = "UDP protocol communication within the same XDAQ process is not supported. Use the fifo transport for local communication.";
			XCEPT_RAISE(pt::exception::UnknownProtocolOrService,msg);
		}
		else 
		{		
			// create remote messenger
			udp::I2OMessenger* m = new udp::I2OMessenger (this, destination, local);
			//i2oMessengers_.push_back(m);
			return pt::Messenger::Reference(m);
		}
	} else
	{
		std::string msg = "Cannot handle protocol service combination, destination protocol was :";
		msg +=  destination->getProtocol();
		msg += " destination service was:";
		msg +=  destination->getService();
		msg += " while local protocol was:";
		msg += local->getProtocol();
		msg += "and local service was:";
		msg += local->getService();
		
		XCEPT_RAISE(pt::exception::UnknownProtocolOrService,msg);
	}
}


pt::TransportType pt::udp::PeerTransportSender::getType()
{
	return pt::Sender;
}

// The supported URL looks as follows: udp://<hostname>:<port>[/<service>] e.g udp://<host>:<port>/i2o
//
pt::Address::Reference pt::udp::PeerTransportSender::createAddress( const std::string& url, const std::string& service )
	throw (pt::exception::InvalidAddress)
{
	//creates an address reference, the url object is also created (toolbox::net::URL)
	return pt::Address::Reference (new udp::Address(url, service));
}

//create the protocol string eg udp://<host>:<port>/i2o
pt::Address::Reference 
pt::udp::PeerTransportSender::createAddress( std::map<std::string, std::string, std::less<std::string> >& address )
	throw (pt::exception::InvalidAddress)
{
	std::string protocol = address["protocol"];
	
	if (protocol == "udp")
	{
		std::string url = protocol;
		
		XCEPT_ASSERT (address["hostname"] != "",pt::exception::InvalidAddress , "Cannot create address, hostname not specified");
		XCEPT_ASSERT (address["port"] != "",pt::exception::InvalidAddress , "Cannot create address, port number not specified");
		
		url += "://";
		url += address["hostname"];
		url += ":";
		url += address["port"];
		
		std::string service = address["service"];
		if (service != "")
		{
			if (!this->isServiceSupported(service))
			{
				std::string msg = "Cannot create address, specified service for protocol ";
				msg += protocol;
				msg += " not supported: ";
				msg += service;
				XCEPT_RAISE(pt::exception::InvalidAddress, msg);
			}
			
			//url += "/";
			//url += service;
		}
		
		return this->createAddress(url, service);
	}
	else 
	{
		std::string msg = "Cannot create address, protocol not supported: ";
		msg += protocol;
		XCEPT_RAISE(pt::exception::InvalidAddress, msg);
	}	
}

std::string pt::udp::PeerTransportSender::getProtocol()
{
	return "udp";
}

std::vector<std::string> pt::udp::PeerTransportSender::getSupportedServices()
{
	std::vector<std::string> s;
	s.push_back("i2o");
	s.push_back("soap");

	return s;
}

bool pt::udp::PeerTransportSender::isServiceSupported(const std::string& service )
{
	if (service == "i2o") return true;
	else if (service == "soap") return true;
	else return false;
}


// sender services
void pt::udp::PeerTransportSender::post(toolbox::mem::Reference * ref, udp::Channel * channel, toolbox::exception::HandlerSignature * handler, void * context)
 throw (pt::exception::Exception)
{
	if ( ! activated_)
	{
		XCEPT_RAISE (pt::exception::Exception, "peer transport sender loop is not activated");
	}
	
	udp::PostDescriptor outputDescriptor;
	outputDescriptor.channel = channel;
	outputDescriptor.ref = ref;
	outputDescriptor.handler = handler;
	outputDescriptor.context = context;
	try
	{
		//std::cout << "Before pushing ...  a" << std::endl;
		outputQueue_.push(outputDescriptor);
		//std::cout << "After pushing ...  a" << std::endl;
	} 
	catch (toolbox::exception::QueueFull& qfe)
	{
		XCEPT_RETHROW (pt::exception::Exception, "Failed to post message for sending, outputQueue full", qfe);
	}
	
}

int pt::udp::PeerTransportSender::svc()
{
	for (;;)
	{
		// if no elements are found. it waits (blocking)
		//std::cout << "popping ... " << std::endl;
		pt::udp::PostDescriptor request = outputQueue_.pop();
		
		toolbox::TimeVal startTime = toolbox::TimeVal::gettimeofday();

		//std::cout << "After popping ... " << std::endl;

		// Enqueued reference 0 means exiting condition
		if (request.ref == 0 ) {
			//std::cout << "After popping ...  a" << std::endl;
			done_ = true;
			return 0;
		}	
		
		try
		{
			//get the memory reference to send
			toolbox::mem::Reference* ref = request.ref;
			size_t size;

			//std::cout << "After popping ...  b" << std::endl;
			while (ref != 0)
			{
			//std::cout << "After popping ...  c" << std::endl;
				size = ref->getDataSize();
				PI2O_MESSAGE_FRAME frame = (PI2O_MESSAGE_FRAME) ref->getDataLocation();

				//std::cout << "After popping ...  d " << size << std::endl;
				//debug
				//I2O_PRIVATE_MESSAGE_FRAME * framePtr = (I2O_PRIVATE_MESSAGE_FRAME*) ref->getDataLocation();

				//std::cout << "SVC tid target " << framePtr->StdMessageFrame.TargetAddress << std::endl;
				//std::cout << "SVC tid from " << framePtr->StdMessageFrame.InitiatorAddress << std::endl;
				//std::cout << "SVC size" << size << std::endl;
				//std::cout << "SVC channel" << std::hex << (unsigned long) request.channel << std::dec << std::endl;
				//debug

				//use bitwise operator to check if the frame size


				if (size == static_cast<size_t>(frame->MessageSize << 2))
				{
					kiloBytesSent_ = kiloBytesSent_ + (double)(size / 1000.0);
					request.channel->send((const char*) frame, size);
					ref = ref->getNextReference();
				}
				else
				{
					std::string msg =  toolbox::toString("Frame size %d and Reference data size %d  do not match (frame dropped)", frame->MessageSize << 2, size);
					request.ref->release();	
					pt::exception::InvalidFrame e("pt::exception::InvalidFrame", msg, __FILE__, __LINE__, __FUNCTION__);
					toolbox::exception::getProcessor()->push(e, request.handler, request.context );
					break;
				}
			}
			sentMessages_++;
			// now release the chain
			request.ref->release();			
		}
		catch (pt::udp::exception::Exception & e ) 
		{
			request.ref->release();	
			toolbox::exception::getProcessor()->push(e, request.handler, request.context );
		}

		sendTime_ = sendTime_ + ((double) toolbox::TimeVal::gettimeofday() - (double) startTime);
	
	}

}

