// $Id: Address.cc,v 1.2 2007/11/13 10:06:06 lorsini Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include <iostream>
#include <sstream>

#include "pt/udp/Address.h"
#include "toolbox/net/URL.h"
#include "toolbox/net/Utils.h"
#include "toolbox/exception/Exception.h"


// only for debugging inet_ntoa
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>




pt::udp::Address::Address(const std::string & url, const std::string& service): url_(url)
{
	service_ = service;
}

pt::udp::Address::~Address()
{
}

struct sockaddr_in pt::udp::Address::getSocketAddress()
{
	struct sockaddr_in writeAddr;
	try 
	{
		writeAddr = toolbox::net::getSocketAddressByName (url_.getHost(), url_.getPort());
	
	} catch (toolbox::exception::Exception& e)
	{
		std::cout << e.what() << std::endl;
	}
	return writeAddr;
}


std::string pt::udp::Address::getService()
{
	return service_;
}

std::string pt::udp::Address::getProtocol()
{
	return url_.getProtocol();
}

std::string pt::udp::Address::toString()
{
	return url_.toString();
}

std::string pt::udp::Address::getURL()
{
	return url_.toString();
}

std::string pt::udp::Address::getHost()
{
	return url_.getHost();
}

std::string pt::udp::Address::getPort()
{
    std::ostringstream o;
    if(url_.getPort() > 0)
        o <<  url_.getPort();
    return o.str();
}
std::string pt::udp::Address::getPath()
{
	std::string path = url_.getPath();
        if ( path.empty()  ) 
	{	
		return "/";
	}
	else
	{
		if ( path[0] == '/' )
		{
			return path;
		}
		else
		{
			path.insert(0,"/");
			return path;
		}				
	}	
}

std::string pt::udp::Address::getServiceParameters()
{
	return url_.getPath();
}

bool pt::udp::Address::equals( pt::Address::Reference address )
{
	return (( this->toString() == address->toString()) && (this->getService() == address->getService()) );
}
