// $Id: SOAPMessenger.cc,v 1.1 2007/11/13 12:49:08 lorsini Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include<map>

#include "pt/udp/SOAPMessenger.h"
#include "pt/udp/PeerTransportSender.h"
#include "pt/udp/Address.h"
#include "xoap/MessageReference.h"
#include "xoap/MessageFactory.h"
#include "xoap/exception/Exception.h"
#include "pt/udp/exception/Exception.h"

pt::udp::SOAPMessenger::SOAPMessenger(Logger & logger, pt::Address::Reference destination, pt::Address::Reference local) 
throw (pt::http::exception::Exception ) :
	logger_(logger)
{
	// Check for address correct already done in PeerTRansportSender::getMessenger()
	//
	//destination_ = dynamic_cast<http::Address>(destination);
	destination_ = destination;
	local_ = local;
	
	try 
	{
		//local_ = dynamic_cast<http::Address*>(local);
		channel_ = new udp::ClientChannel(destination_);
	}
	catch (pt::http::exception::Exception & e )
	{
		XCEPT_RETHROW(pt::udp::exception::Exception, "cannot create SOAP messenger", e);
	}
}

pt::udp::SOAPMessenger::~SOAPMessenger()
{
	delete channel_;
}

pt::Address::Reference pt::udp::SOAPMessenger::getLocalAddress()
{
	return local_;
}

pt::Address::Reference pt::udp::SOAPMessenger::getDestinationAddress()
{
	return destination_;
}

xoap::MessageReference pt::udp::SOAPMessenger::send (xoap::MessageReference message) 
	throw (pt::exception::Exception)
{
	std::string requestBuffer;
		
	try
	{
		// serialize message
		message->writeTo (requestBuffer);
 	}
	catch (xoap::exception::Exception& e)
	{		
		// Invalid request, cannot handle it
		XCEPT_RETHROW(pt::exception::Exception, "SOAP request message invalid, cannot be serialized", e); 	
	}
		
	size_t size = requestBuffer.size();
	
	
	// Fill the headers
	std::multimap<std::string, std::string, std::less<std::string> >& allHeaders = message->getMimeHeaders()->getAllHeaders();
	std::multimap<std::string, std::string, std::less<std::string> >::iterator i;
	
	std::string headers = "";
	for (i = allHeaders.begin(); i != allHeaders.end(); i++)
	{
		headers += (*i).first;
		headers += ": ";
		headers += (*i).second;
		headers += "\r\n";
	}
	
	channel_->lock();
	try
	{
		// Connect is intelligent, tries to connect only if socket not already connected
		//
		channel_->connect();
		
		pt::udp::Address& d = dynamic_cast<http::Address&>(*destination_);
	/*	 TO BE REPLACED (WITH UDP ) one way no reply
		http::Utils::sendTo( channel_, 
				(char*)d.getPath().c_str(),
				(char*)d.getHost().c_str(),
				(char*)d.getPort().c_str(),
				(char*)requestBuffer.c_str(), 
				size,
				(char*) headers.c_str() );
	
		size = 0;
		std::string header;
		replyBuffer = http::Utils::receiveFrom ( channel_, &size, &header);
		channel_->disconnect();
*/
	}
	catch (pt::http::exception::Exception& e)
	{
		channel_->unlock();
		XCEPT_RETHROW (pt::exception::Exception, "SOAP request failed", e);
	}
	channel_->unlock();
	
}

