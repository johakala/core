// $Id: PeerTransportUDP.cc,v 1.1 2007/11/12 10:20:37 lorsini Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "pt/udp/PeerTransportUDP.h"
#include "pt/PeerTransportAgent.h"
#include "xoap/SOAPName.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/domutils.h"
#include "toolbox/fsm/FailedEvent.h"
#include "toolbox/mem/HeapAllocator.h"
#include "xoap/Method.h"
#include "xdaq/NamespaceURI.h"
#include "xcept/tools.h"

#include "xdata/InfoSpaceFactory.h"

XDAQ_INSTANTIATOR_IMPL(pt::udp::PeerTransportUDP)

pt::udp::PeerTransportUDP::PeerTransportUDP(xdaq::ApplicationStub * s)
 throw(xdaq::exception::Exception)
 : xdaq::WebApplication(s)
{	
	

	getApplicationDescriptor()->setAttribute("icon","/pt/images/PeerTransport64x64.gif");
	// Bind CGI callbacks
	xgi::bind(this, &PeerTransportUDP::Default, "Default");
	xgi::bind(this, &PeerTransportUDP::resetCounters, "resetCounters");

	getApplicationInfoSpace()->fireItemAvailable("maxPacketSize",&maxPacketSize_);
	getApplicationInfoSpace()->fireItemAvailable("poolName",&poolName_);
	autosize_ = true;
	maxPacketSize_ = 65628;

	toolbox::net::URN urn("toolbox-mem-pool", "PeerTransportUDP/heap");
	poolName_ = urn.toString();
	try 
	{
		toolbox::mem::HeapAllocator* a = new toolbox::mem::HeapAllocator();
		toolbox::mem::getMemoryPoolFactory()->createPool(urn, a);			
	}
	catch (toolbox::mem::exception::Exception & e)
	{
		XCEPT_RETHROW (xdaq::exception::Exception, "Cannot create default memory pool", e);
	}

	//peer transport sender and receiver
	pts_ = new pt::udp::PeerTransportSender(this);
	ptr_ = new pt::udp::PeerTransportReceiver(this, maxPacketSize_, getApplicationLogger());
	pt::PeerTransportAgent* pta = pt::getPeerTransportAgent();
	pta->addPeerTransport(pts_);
	pta->addPeerTransport(ptr_);
	
/*	try
	{
		this->reset();
	}
	catch (pt::exception::Exception & e)
	{
		XCEPT_RETHROW (xdaq::exception::Exception, "Cannot reset peer transport", e);
	}*/

	// Add infospace listeners for exporting data values
	getApplicationInfoSpace()->addItemChangedListener ("maxPacketSize", this);
	getApplicationInfoSpace()->addItemChangedListener ("poolName", this);
}

void pt::udp::PeerTransportUDP::actionPerformed (xdata::Event& e)
{
	// update measurements monitors
	if (e.type() == "ItemChangedEvent")
	{
		std::string item = dynamic_cast<xdata::ItemChangedEvent&>(e).itemName();
		if ( item == "poolName" || item == "maxPacketSize")
		{
			try
			{
				this->reset();
			}
			catch(pt::exception::Exception & e)
			{
				LOG4CPLUS_ERROR (getApplicationLogger(), xcept::stdformat_exception_history(e));
			}
		}
	}
}

void pt::udp::PeerTransportUDP::Default(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception)
{
	out->getHTTPResponseHeader().addHeader("Content-Type", "text/html");
	*out << cgicc::HTMLDoctype(cgicc::HTMLDoctype::eStrict) << std::endl;
	*out << cgicc::html().set("lang", "en").set("dir","ltr") << std::endl;
	*out << cgicc::head();
	*out << cgicc::title("Peer Transport UDP") << std::endl;
	
	*out << "<style>" << std::endl;

	*out << "th {" << std::endl;
	*out << "	background: #C0C0C0;" << std::endl;
	*out << "	border-top-left-radius: 4px;" << std::endl;
	*out << "	border-top-right-radius: 4px;" << std::endl;
	*out << "	text-align: left;" << std::endl;
	*out << "	padding: 3px 5px;" << std::endl;
	*out << "}" << std::endl;
	*out << "tr.td1 td {" << std::endl;
		*out << "	background: #F8F8F8;" << std::endl;
		*out << "	padding: 1px 3px;" << std::endl;
	*out << "}" << std::endl;
	*out << "tr.td2 td {" << std::endl;
		*out << "	background: #FFFFFF;" << std::endl;
		*out << "	padding: 1px 3px;" << std::endl;
	*out << "}" << std::endl;
	*out << "</style>" << std::endl;

	*out << cgicc::head();
	*out << cgicc::body();
	xgi::Utils::getPageHeader
		(out, 
		"Peer Transport UDP",
		getApplicationDescriptor()->getContextDescriptor()->getURL(),
		getApplicationDescriptor()->getURN(),
		"/pt/images/PeerTransport32x32.gif"
		);
	
	this->StatisticsTabPage(out);

	xgi::Utils::getPageFooter(*out);
	*out << cgicc::body();
	*out << cgicc::html();
}

	
//
// Failure Pages
//
void pt::udp::PeerTransportUDP::failurePage(xgi::Output * out, xcept::Exception & e)  throw (xgi::exception::Exception)
{
	out->getHTTPResponseHeader().addHeader("Content-Type", "text/html");
	*out << cgicc::HTMLDoctype(cgicc::HTMLDoctype::eStrict) << std::endl;
	*out << cgicc::html().set("lang", "en").set("dir","ltr") << std::endl;

	xgi::Utils::getPageHeader
		(out, 
		"Peer Transport UDP Failure",
		getApplicationDescriptor()->getContextDescriptor()->getURL(),
		getApplicationDescriptor()->getURN(),
		"/pt/tcp/images/Application.gif"
		);

	*out << cgicc::br() << e.what() << cgicc::br() << std::endl;
	std::string url = "/";
	url += getApplicationDescriptor()->getURN();
	
	*out << cgicc::br() << "<a href=\"" << url << "\">" << "retry" << "</a>" << cgicc::br() << std::endl;

	xgi::Utils::getPageFooter(*out);
}

pt::udp::PeerTransportUDP::~PeerTransportUDP()
{
	pt::PeerTransportAgent* pta = pt::getPeerTransportAgent();	
	pta->removePeerTransport(pts_);
	pta->removePeerTransport(ptr_);	
	delete pts_;	
	delete ptr_;		
}


void pt::udp::PeerTransportUDP::reset() throw (pt::exception::Exception)
{
	LOG4CPLUS_INFO (getApplicationLogger(), "stop receiver and sender threads");
	// stop receiver and sender
	ptr_->stop();
	pts_->stop();
	
	// apply new paramaterization
	//std::cout << "Set maxpacket size" << std::endl;
	ptr_->setMaxPacketSize(maxPacketSize_);
	//std::cout << "Maxpacket size set" << std::endl;
	try 
	{
		// try to retrieve indicated pool through manager
		toolbox::net::URN urn(poolName_);
		toolbox::mem::Pool * p = toolbox::mem::getMemoryPoolFactory()->findPool(urn);
		ptr_->setPool(p);
	}
	catch (toolbox::net::exception::MalformedURN & e)
	{
		std::string msg = "Malformed URN ";
		msg += poolName_;
		XCEPT_RETHROW (pt::exception::Exception, msg, e);
	}
	catch (toolbox::mem::exception::MemoryPoolNotFound & e)
	{
		std::string msg = "Cannot access memory pool named ";
		msg += poolName_;
		XCEPT_RETHROW (pt::exception::Exception, msg, e);
	}	
	
	// re-start the reading thread with the provided configuration
	ptr_->start();
	pts_->start();
	
	LOG4CPLUS_INFO (getApplicationLogger(), "start receiver and sender threads");
}

void pt::udp::PeerTransportUDP::StatisticsTabPage
(
	xgi::Output * out
)
throw (xgi::exception::Exception)
{
   	xdata::getInfoSpaceFactory()->lock();


   	*out << cgicc::table();
 	*out << cgicc::thead();
 	*out << cgicc::tr();
		*out << cgicc::th("endpointURL");
		*out << cgicc::th("receiveTime");
		*out << cgicc::th("receivedMessages");
		*out << cgicc::th("kiloBytesReceived");
	*out << cgicc::tr();
	*out << cgicc::thead();

	*out << cgicc::tbody();

	std::string htmlStyle = "td1";

	std::map<std::string, xdata::Serializable *, std::less<std::string> > spaces = xdata::getInfoSpaceFactory()->match("pt-udp-receiver");


	//std::cout << "infospaces" << spaces.size() << std::endl;

	for (  std::map<std::string, xdata::Serializable *, std::less<std::string> >::iterator i = spaces.begin(); i != spaces.end(); ++i)
         {
		xdata::InfoSpace* is = dynamic_cast<xdata::InfoSpace *> ((*i).second);
		xdata::String * endpointURL = dynamic_cast<xdata::String*>(is->find("endpointURL"));
		xdata::Double* receiveTime = dynamic_cast<xdata::Double*>(is->find("receiveTime"));
		xdata::UnsignedInteger32* messagesReceived = dynamic_cast<xdata::UnsignedInteger32*>(is->find("receivedMessages"));
		xdata::Double* kiloBytesReceived = dynamic_cast<xdata::Double*>(is->find("kiloBytesReceived"));
		*out << cgicc::tr().set("class", htmlStyle);
                *out << cgicc::td(endpointURL->toString());
                *out << cgicc::td(receiveTime->toString());
                *out << cgicc::td(messagesReceived->toString());
                *out << cgicc::td(kiloBytesReceived->toString());
  		*out << cgicc::tr();

  		htmlStyle = (htmlStyle.compare("td1") == 0) ? "td2" : "td1";
	 }

 	*out << cgicc::tbody();
        *out << cgicc::table();

    	*out << cgicc::table();
        *out << cgicc::thead();
        *out << cgicc::tr();
        *out << cgicc::th("sendTime");
        *out << cgicc::th("sentMessages");
        *out << cgicc::th("kiloBytesSent");
        *out << cgicc::tr();
        *out << cgicc::thead();

        *out << cgicc::tbody();

     htmlStyle = "td1";

	 spaces = xdata::getInfoSpaceFactory()->match("pt-udp-sender");
	 for (  std::map<std::string, xdata::Serializable *, std::less<std::string> >::iterator i = spaces.begin(); i != spaces.end(); ++i)
         {
		xdata::InfoSpace* is = dynamic_cast<xdata::InfoSpace *> ((*i).second);
		xdata::Double* sendTime = dynamic_cast<xdata::Double*>(is->find("sendTime"));
		xdata::UnsignedInteger32* messagesSent = dynamic_cast<xdata::UnsignedInteger32*>(is->find("sentMessages"));
		xdata::Double* kiloBytesSent = dynamic_cast<xdata::Double*>(is->find("kiloBytesSent"));
		*out << cgicc::tr().set("class", htmlStyle);
                *out << cgicc::td(sendTime->toString());
                *out << cgicc::td(messagesSent->toString());
                *out << cgicc::td(kiloBytesSent->toString());
  		*out << cgicc::tr();

  		htmlStyle = (htmlStyle.compare("td1") == 0) ? "td2" : "td1";
	 }
	*out << cgicc::tbody();
        *out << cgicc::table();

	std::string url = "/";
	url += getApplicationDescriptor()->getURN();
	url += "/resetCounters";

	*out << cgicc::form().set("method","post")
		.set("action", url)
		.set("enctype","multipart/form-data") << std::endl;
	*out << cgicc::fieldset().set("style","font-size: 10pt; font-family: arial; background-color: rgb(255,255,255);") << std::endl;
	*out << cgicc::legend("Reset Counters") << std::endl;
	*out << cgicc::p() << std::endl;

	*out << cgicc::input().set("type", "submit")
				.set("name", "resetCounters")
				.set("value", "Reset Counters")
				.set("style","font-size: 10pt; font-family: arial; background-color: rgb(255,255,255);");
	*out << cgicc::p() << std::endl;
	*out << cgicc::legend() << std::endl;
	*out << cgicc::fieldset() << std::endl;
	*out << cgicc::form() << std::endl;

   	xdata::getInfoSpaceFactory()->unlock();
}
//reset the PT counters
void pt::udp::PeerTransportUDP::resetCounters(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception)
{

	try
	 {
		std::map<std::string, xdata::Serializable *, std::less<std::string> > spaces = xdata::getInfoSpaceFactory()->match("pt-udp-receiver");

		for (std::map<std::string, xdata::Serializable *, std::less<std::string> >::iterator i = spaces.begin(); i != spaces.end(); ++i)
		{
			xdata::InfoSpace* is = dynamic_cast<xdata::InfoSpace *> ((*i).second);

			xdata::Double* receiveTime = dynamic_cast<xdata::Double*>(is->find("receiveTime"));
			xdata::UnsignedInteger32* messagesReceived = dynamic_cast<xdata::UnsignedInteger32*>(is->find("receivedMessages"));
			xdata::Double* kiloBytesReceived = dynamic_cast<xdata::Double*>(is->find("kiloBytesReceived"));

			*receiveTime = 0.0;
			*messagesReceived = 0;
			*kiloBytesReceived = 0.0;
		}

		spaces = xdata::getInfoSpaceFactory()->match("pt-udp-sender");
		for (std::map<std::string, xdata::Serializable *, std::less<std::string> >::iterator i = spaces.begin(); i != spaces.end(); ++i)
		{
			xdata::InfoSpace* is = dynamic_cast<xdata::InfoSpace *> ((*i).second);

			xdata::Double* sendTime = dynamic_cast<xdata::Double*>(is->find("sendTime"));
			xdata::UnsignedInteger32* messagesSent = dynamic_cast<xdata::UnsignedInteger32*>(is->find("sentMessages"));
			xdata::Double* kiloBytesSent = dynamic_cast<xdata::Double*>(is->find("kiloBytesSent"));

			*sendTime = 0.0;
			*messagesSent = 0;
			*kiloBytesSent = 0.0;
		}

		this->Default(in,out);
	}
	catch(pt::exception::Exception& xe)
	{
		this->failurePage(out, xe);

	}
	catch(const std::exception& e)
	{
		xgi::exception::Exception nex("xgi::exception::Exception", e.what(), __FILE__, __LINE__,__FUNCTION__);
		this->failurePage(out, nex);

	}
}
