// $Id: Transmitter.cc,v 1.2 2007/11/13 10:06:06 lorsini Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "pt/udp/Transmitter.h"
#include "pt/udp/exception/Exception.h"

#include <string>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netinet/tcp.h>
#include <netdb.h>


pt::udp::Transmitter::~Transmitter()
{
}

pt::udp::Transmitter::Transmitter(pt::Address::Reference remote, pt::Address::Reference local) throw (pt::udp::exception::Exception): pt::udp::Channel (remote)
{
	isConnected_ = false;
	retry_ = 0;

	errno = 0;

	pt::udp::Address & localAddress = dynamic_cast<pt::udp::Address &>(*local);
	pt::udp::Address & remoteAddress = dynamic_cast<pt::udp::Address &>(*remote);

	localSockAddress_ = localAddress.getSocketAddress();
	remoteSockAddress_ = remoteAddress.getSocketAddress();

	sockaddressSize_ = sizeof (remoteSockAddress_);
}

bool pt::udp::Transmitter::isConnected()  throw (pt::udp::exception::Exception)
{
	return isConnected_;
}

void pt::udp::Transmitter::connect()  throw (pt::udp::exception::Exception)
{
	if ( isConnected_ ) return;
	
	if (socket_ == -1)
	{
		// recreate socket
		errno = 0;
		socket_ = socket(AF_INET, SOCK_DGRAM, 0);
   		if (socket_ == -1)
  		{
			XCEPT_RAISE (pt::udp::exception::Exception, strerror(errno));
		}

		int optval = 1;

		errno = 0;
   		if (setsockopt(socket_, SOL_SOCKET, SO_REUSEADDR, (char *)&optval, sizeof(optval)) < 0)
  		{
			XCEPT_RAISE (pt::udp::exception::Exception, strerror(errno));
  		}		
	}
	
	isConnected_ = true;
}
	
	
void pt::udp::Transmitter::disconnect()  throw (pt::udp::exception::Exception)
{
	::close(socket_);
	socket_ = -1;
	isConnected_ = false;

}

void pt::udp::Transmitter::close()  throw (pt::udp::exception::Exception)
{
	this->disconnect();
}	
	
	
size_t pt::udp::Transmitter::receive(char * buf ,size_t len ) throw (pt::udp::exception::Exception)
{
	//always return 1 as the socket never connects to transmit
	return 1;
}
	
void pt::udp::Transmitter::send(const char * buf, size_t len)  throw (pt::udp::exception::Exception)
{
		errno = 0;

		//std::cout << "Send bytes " << len   << std::endl;

		size_t nBytes = ::sendto(socket_, (char*)buf, len, 0, (struct sockaddr*)&remoteSockAddress_, sockaddressSize_);

		if (nBytes < 0)
		{
			this->close();
			XCEPT_RAISE (pt::udp::exception::Exception, strerror(errno));
		}

}
