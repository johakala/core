// $Id: PeerTransportReceiver.cc,v 1.3 2007/11/13 10:06:06 lorsini Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                             *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					                 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                             *
 * For the list of contributors see CREDITS.   			                 *
 *************************************************************************/

#include "pt/udp/PeerTransportReceiver.h"
#include "pt/udp/Address.h"
#include <iostream>
#include "i2o/Listener.h"

pt::udp::PeerTransportReceiver::PeerTransportReceiver(xdaq::Application* owner, xdata::UnsignedLongT maxPacketSize, Logger& logger):
	xdaq::Object(owner),
	logger_(logger)
{
	maxPacketSize_ = maxPacketSize;
	pool_ = 0;
}

pt::udp::PeerTransportReceiver::~PeerTransportReceiver()
{	
	// Remove all messengers. At this point none of them
	// should be touched by a user application anymore
	
	for (std::vector<pt::udp::ReceiverLoop*>::size_type i = 0; i < loop_.size(); i++)
	{
		// stop and delete the tasks
		#warning "Missing implementation for stopping and deleting receiver tasks."
		delete loop_[i];
	}
}

pt::TransportType pt::udp::PeerTransportReceiver::getType()
{
	return pt::Receiver;
}

pt::Address::Reference pt::udp::PeerTransportReceiver::createAddress ( const std::string& url, const std::string& service )
	throw (pt::exception::InvalidAddress)
{
	return pt::Address::Reference(new pt::udp::Address(url, service));
}

pt::Address::Reference 
pt::udp::PeerTransportReceiver::createAddress( std::map<std::string, std::string, std::less<std::string> >& address )
	throw (pt::exception::InvalidAddress)
{
	std::string protocol = address["protocol"];
	
	if (protocol == "udp")
	{
		std::string url = protocol;
		
		XCEPT_ASSERT (address["hostname"] != "",pt::exception::InvalidAddress , "Cannot create address, hostname not specified");
		XCEPT_ASSERT (address["port"] != "",pt::exception::InvalidAddress , "Cannot create address, port number not specified");
		
		url += "://";
		url += address["hostname"];
		url += ":";
		url += address["port"];
		
		std::string service = address["service"];
		if (service != "")
		{
			if (!this->isServiceSupported(service))
			{
				std::string msg = "Cannot create address, specified service for protocol ";
				msg += protocol;
				msg += " not supported: ";
				msg += service;
				XCEPT_RAISE(pt::exception::InvalidAddress, msg);
			}
			
			//url += "/";
			//url += service;
		}
		
		return this->createAddress(url, service);
	}
	else 
	{
		std::string msg = "Cannot create address, protocol not supported: ";
		msg += protocol;
		XCEPT_RAISE(pt::exception::InvalidAddress, msg);
	}	
}

std::string pt::udp::PeerTransportReceiver::getProtocol()
{
	return "udp";
}

std::vector<std::string> pt::udp::PeerTransportReceiver::getSupportedServices()
{
	std::vector<std::string> s;
	s.push_back("i2o");
	return s;
}

bool pt::udp::PeerTransportReceiver::isServiceSupported(const std::string& service )
{
	if (service == "i2o") return true;
	else return false;
}


void pt::udp::PeerTransportReceiver::config( pt::Address::Reference address) throw (pt::exception::Exception)
{
	// check if a receiver 
	for (std::vector<pt::udp::ReceiverLoop*>::size_type i = 0; i < loop_.size(); i++)
	{
		if (loop_[i]->getAddress()->equals(address))
		{
			std::string msg = "Receiver for address already configured: ";
			msg += address->toString();
			XCEPT_RAISE (pt::exception::Exception, msg);
		}
	}
	
	// Create a new receiver loop only with the address.
	// The pool, maxPacketSize and autosize parameters use only the default values inside the ReceiverLoop.
	// Therefore the ReceiverLoop is created, but the thread is not yet started.
	udp::ReceiverLoop* loop = new udp::ReceiverLoop(address, logger_, this);
	
	// attempt to add listener if present
	if (this->isExistingListener("i2o"))
	{
		// I can safely get it now
		i2o::Listener* l = dynamic_cast<i2o::Listener*>(this->getListener("i2o"));
	
		try
		{
			loop->addServiceListener(l);
		}
		catch(pt::exception::Exception & ex1)
		{
			// Fatal error, it should never happen
			XCEPT_RETHROW (pt::exception::Exception, "Cannot configure peer transport udp", ex1);
		}
	}
	
	loop_.push_back(loop);	

	// Activate the new loop
	//
	if (pool_ == 0)
	{
		std::string msg = "Activation of UDP receiver thread failed. Missing memory pool.";
		XCEPT_RAISE (pt::exception::Exception, msg);
	}
	loop->setPool(pool_);
	loop->setMaxPacketSize(maxPacketSize_);
	loop->activate();

	//std::cout << "Receiver Loops: " << loop_.size() << std::endl;
}

void pt::udp::PeerTransportReceiver::setPool(toolbox::mem::Pool * pool)
{
	pool_ = pool;
}

void pt::udp::PeerTransportReceiver::setMaxPacketSize(xdata::UnsignedLongT maxPacketSize )
{
	maxPacketSize_ = maxPacketSize;
}

void pt::udp::PeerTransportReceiver::stop() throw (pt::exception::Exception)
{
	for (std::vector<pt::udp::ReceiverLoop*>::size_type i = 0; i < loop_.size(); i++)
	{
		loop_[i]->cancel(); // busy waiting until work loop terminated
	}
}

void pt::udp::PeerTransportReceiver::start() throw (pt::exception::Exception)
{
	if (pool_ == 0)
	{
		std::string msg = "Activation of UDP receiver thread failed. Missing memory pool.";
		XCEPT_RAISE (pt::exception::Exception, msg);
	}

	// activate receiver loop as a thread at config point
	try 
	{
		for (std::vector<pt::udp::ReceiverLoop*>::size_type i = 0; i < loop_.size(); i++)
		{
			loop_[i]->setPool(pool_);
			loop_[i]->setMaxPacketSize(maxPacketSize_);
			loop_[i]->activate();
			
		}
	} catch (...)
	{
		std::string msg = "Activation of UDP receiver thread failed.";
		XCEPT_RAISE (pt::exception::Exception, msg);
	}
}

// Overriding function to asynchrounously set the listener
//
void pt::udp::PeerTransportReceiver::addServiceListener (pt::Listener* listener) throw (pt::exception::Exception)
{
	pt::PeerTransportReceiver::addServiceListener(listener);
	for (std::vector<pt::udp::ReceiverLoop*>::size_type i = 0; i < loop_.size(); i++)
	{
		loop_[i]->addServiceListener(listener);
	}
}

void pt::udp::PeerTransportReceiver::removeServiceListener (pt::Listener* listener ) throw (pt::exception::Exception)
{
	pt::PeerTransportReceiver::removeServiceListener(listener);
	for (std::vector<pt::udp::ReceiverLoop*>::size_type i = 0; i < loop_.size(); i++)
	{
		loop_[i]->removeServiceListener(listener);
	}
}

void pt::udp::PeerTransportReceiver::removeAllServiceListeners()
{
	pt::PeerTransportReceiver::removeAllServiceListeners();
	for (std::vector<pt::udp::ReceiverLoop*>::size_type i = 0; i < loop_.size(); i++)
	{
		loop_[i]->removeAllServiceListeners();
	}
}
