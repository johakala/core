// $Id: Channel.cc,v 1.2 2007/11/13 10:06:06 lorsini Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "pt/udp/Channel.h"
#include "pt/udp/Address.h"
#include "pt/udp/exception/Exception.h"
#include "pt/Address.h"

#include <string>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netinet/tcp.h>
#include <netdb.h>

pt::udp::Channel::Channel(pt::Address::Reference address) throw (pt::udp::exception::Exception)
{
	pt::udp::Address & a = dynamic_cast<pt::udp::Address&>(*address);
	
	sockaddress_ = a.getSocketAddress();
	sockaddressSize_ = sizeof (sockaddress_);
	
	//std::cout << "Creating channel for " << a.toString() << std::endl;

	errno = 0;

	//create the socket for sending datagrams
	socket_ = socket(AF_INET, SOCK_DGRAM, 0);
   	if (socket_ == -1)
  	{
		XCEPT_RAISE (pt::udp::exception::Exception, strerror(errno));
	}
	int optval = 1;

	errno = 0;

	//SOL_SOCKET   - allows setting of API
	//SO_REUSEADDR - prevents error when trying to re-open the same address:port after it has been closed (overrides time out)
	//optval - 0 for false, 1 for true
   	if (setsockopt(socket_, SOL_SOCKET, SO_REUSEADDR, (char *)&optval, sizeof(optval)) < 0)
  	{
		XCEPT_RAISE (pt::udp::exception::Exception, strerror(errno));
  	}

   	//increase the socket buffer to prevent packets being discarded
   	int buffSize = 65535 * 100;
   	if (setsockopt(socket_, SOL_SOCKET, SO_RCVBUF, &buffSize, sizeof(int)) < 0)
	{
		XCEPT_RAISE (pt::udp::exception::Exception, strerror(errno));
	}

   	//stop the socket blocking on recvfrom
//   	struct timeval tv;
//
//   	tv.tv_sec = 5;
//   	tv.tv_usec = 0;
//
//  	if (setsockopt(socket_, SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof(tv)) < 0)
//  	{
//		XCEPT_RAISE (pt::udp::exception::Exception, strerror(errno));
//  	}
}


pt::udp::Channel::~Channel() 
{
	if (socket_ > 0) ::close(socket_);
}


