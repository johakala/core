// $Id: I2OMessenger.cc,v 1.2 2007/11/13 10:06:06 lorsini Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "pt/udp/I2OMessenger.h"
#include "pt/udp/PeerTransportSender.h"
#include "pt/udp/Address.h"
#include "pt/udp/Transmitter.h"

pt::udp::I2OMessenger::I2OMessenger(pt::udp::PeerTransportSender * pt,pt::Address::Reference destination, pt::Address::Reference local )
{
	pt_ = pt;
	destination_ = destination;
	local_ = local;
	channel_ = new pt::udp::Transmitter(destination_, local_);
}

pt::udp::I2OMessenger::~I2OMessenger()
{
	delete channel_;
}

void pt::udp::I2OMessenger::send (toolbox::mem::Reference* msg, toolbox::exception::HandlerSignature* handler,  void* context) throw (pt::exception::Exception)
{
	//std::cout << "Messenger Sending" << std::endl;
	if (msg->getDataSize() >= 65508)
		XCEPT_RAISE (pt::udp::exception::Exception, "Packet size too big: Maximum 65508");

	pt_->post (msg, channel_, handler, context);
	//std::cout << "Messenger Sent" << std::endl;
}

pt::Address::Reference pt::udp::I2OMessenger::getLocalAddress()
{
	return local_;
}

pt::Address::Reference pt::udp::I2OMessenger::getDestinationAddress()
{
	return destination_;
}
