// $Id: B2INMessenger.h,v 1.6 2008/08/26 15:00:53 lorsini Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2012, CERN.			                 		 *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A.Petrucci, C.Wakefield					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         	 *
 *************************************************************************/

#ifndef _pt_utcp_B2INMessenger_h
#define _pt_utcp_B2INMessenger_h

#include "b2in/nub/Messenger.h"
#include "pt/Address.h"
#include "toolbox/mem/Reference.h"
#include "b2in/nub/exception/InternalError.h"
#include "b2in/nub/exception/QueueFull.h"
#include "b2in/nub/exception/OverThreshold.h"

#include "tcpla/Address.h"
#include "tcpla/EndPoint.h"
#include "tcpla/InterfaceAdapter.h"

#include "pt/utcp/Messenger.h"
#include "pt/utcp/PeerTransport.h"

namespace pt
{
	namespace utcp
	{

		class B2INMessenger : public b2in::nub::Messenger, public pt::utcp::Messenger
		{
				friend class PeerTransport;

			public:

				//static const size_t B2IN_MTU_SIZE = 4096;
				size_t b2inMTUSize_;

				B2INMessenger (pt::utcp::PeerTransport * pt, tcpla::InterfaceAdapter * ia, pt::Address::Reference local, pt::Address::Reference destination) throw (pt::exception::Exception);

				virtual ~B2INMessenger ();

				void send (toolbox::mem::Reference* msg, xdata::Properties & plist, toolbox::exception::HandlerSignature* handler, void* context) throw (b2in::nub::exception::InternalError, b2in::nub::exception::QueueFull, b2in::nub::exception::OverThreshold);

				pt::Address::Reference getLocalAddress ();
				pt::Address::Reference getDestinationAddress ();

				void reset();

				void invalidate();

				bool fail_; // set true upon connection failure. Once set to true, cannot be set to false

			protected:

		};
	}
}

#endif

