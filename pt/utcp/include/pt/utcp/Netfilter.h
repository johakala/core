// $Id: Netfilter.h,v 1.3 2008/07/18 15:27:20 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _pt_utcp_Netfilter_h_
#define _pt_utcp_Netfilter_h_

#include <string>
#include <exception>
#include <stdint.h>
#include <map>
#include "pt/Address.h"
#include "pt/BinaryListener.h"
#include "pt/utcp/exception/Exception.h"
#include "tcpla/EndPoint.h"
#include "toolbox/mem/Reference.h"
#include "toolbox/net/UUID.h"

namespace pt
{
	namespace utcp
	{
		class Netfilter
		{
			public:

				toolbox::mem::Reference * append (toolbox::mem::Reference* msg, uint16_t packet, uint16_t total, std::pair<uint16_t, toolbox::net::UUID> & key, size_t &totalSize) throw (pt::utcp::exception::Exception);

				size_t clear (toolbox::net::UUID val);

			private:

				typedef struct
				{
						uint16_t packet;
						uint16_t total;
						size_t identification;
						toolbox::mem::Reference* first;
						toolbox::mem::Reference* last;
						size_t totalSize;
				} ChainDescriptor;

				std::map<std::pair<uint16_t, toolbox::net::UUID>, ChainDescriptor> chains_;
		};
	}
}

#endif
