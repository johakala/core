// $Id: Application.h,v 1.2 2008/07/18 15:26:42 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _pt_utcp_Application_h_
#define _pt_utcp_Application_h_

#include "xdaq/Application.h"
#include "xgi/framework/UIManager.h"

#include "i2o/i2o.h"

#include "xdata/UnsignedInteger.h"
#include "xdata/UnsignedInteger64.h"
#include "xdata/UnsignedLong.h"
#include "xdata/Double.h"
#include "xdata/String.h"
#include "xdata/Boolean.h"
#include "xdata/Integer.h"
#include "xdata/Bag.h"
#include "xdata/Vector.h"
#include "xdaq/ApplicationGroup.h"
#include "xdaq/ApplicationStub.h"
#include "xdaq/ApplicationContext.h"

#include "i2o/Method.h"
#include "toolbox/mem/Reference.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "toolbox/mem/Pool.h"
#include "toolbox/mem/CommittedHeapAllocator.h"
#include "toolbox/exception/Handler.h"
#include "toolbox/net/URN.h"
#include "toolbox/fsm/FiniteStateMachine.h"
#include <list>
#include <algorithm>
#include <map>
#include "toolbox/PerformanceMeter.h"

#include "toolbox/task/Timer.h"
#include "toolbox/task/TimerEvent.h"
#include "toolbox/task/TimerListener.h"

#include "pt/utcp/PeerTransport.h"

#include "xgi/WSM.h"
//
// cgicc
//
#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"
#include "xgi/Utils.h"
#include "xgi/Input.h"
#include "xgi/Output.h"

#include "xoap/MessageReference.h"
#include "xoap/MessageFactory.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPBody.h"
#include "xoap/domutils.h"
#include "xoap/Method.h"

#include "tcpla/WaitingWorkLoop.h"

namespace pt
{
	namespace utcp
	{

		const std::string UTCP_PROTOCOL = "utcp";

		class Application : public xdaq::Application, public xgi::framework::UIManager, public xdata::ActionListener, public toolbox::task::TimerListener
		{

			public:

				XDAQ_INSTANTIATOR();

				Application (xdaq::ApplicationStub* stub) throw (xdaq::exception::Exception);

				//
				// Callback for requesting current exported parameter values
				//
				void actionPerformed (xdata::Event& e);

				// Web callback functions
				void Default (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception);
				void init (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception);
				xoap::MessageReference fireEvent (xoap::MessageReference msg) throw (xoap::exception::Exception);

				pt::utcp::PeerTransport * pt_;
			private:
				void SenderTabPage (xgi::Output * out);
				void ReceiverTabPage (xgi::Output * out);
				void SettingsTabPage (xgi::Output * out);
				void EventsTabPage (xgi::Output * out);
				void timeExpired (toolbox::task::TimerEvent& e);
				void autoConnect ();
				void setpipe (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception);
				void setNetworkPipe (xdaq::ApplicationDescriptor * destination) throw (pt::exception::Exception);
				void WorkloopsTabPage (xgi::Output * out);

			protected:

				xdata::Double committedPoolSize_; //socket pool size
				xdata::UnsignedInteger maxClients_;
				xdata::UnsignedInteger ioQueueSize_;
				xdata::UnsignedInteger eventQueueSize_;
				xdata::UnsignedInteger maxReceiveBuffers_; //socket
				xdata::UnsignedInteger maxBlockSize_; //socket
				xdata::Integer maxFD_;
				xdata::Boolean autoConnect_;
				xdata::String protocol_;
		};
	}
}

#endif
