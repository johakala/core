// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2011, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A.Petrucci, C.Wakefield							 *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _pt_utcp_I2OEventHandler_h_
#define _pt_utcp_I2OEventHandler_h_

#include <iostream>
#include <string>

#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"

#include "xdaq/Object.h"

#include "pt/utcp/EventHandler.h"

namespace pt
{
	namespace utcp
	{

		class I2OEventHandler : public pt::utcp::EventHandler, public xdaq::Object
		{
			public:
				I2OEventHandler (xdaq::Application * owner, toolbox::mem::Pool * pool, size_t maxClients, size_t maxReceiveBuffers, size_t ioQueueSize, size_t maxBlockSize) throw (pt::utcp::exception::Exception);
				virtual ~I2OEventHandler ();

				pt::Messenger::Reference getMessenger (pt::Address::Reference destination, pt::Address::Reference local, tcpla::InterfaceAdapter * ia, pt::utcp::PeerTransport * pt) throw (pt::exception::UnknownProtocolOrService);

				//! Internal function to add a message processing listener for this peer transport
				//
				void addServiceListener (pt::Listener * listener) throw (pt::exception::Exception);

				//! Internal function to remove a message processing listener for this peer transport
				//
				void removeServiceListener (pt::Listener * listener) throw (pt::exception::Exception);

				//! Internal function to remove all message processing listeners for this peer transport
				//
				void removeAllServiceListeners ();

				void handleEvent (tcpla::Event & e);

				std::string getService ();

				std::string toHTML ();

				void gc();

			protected:

				toolbox::BSem mutex_;

				i2o::Listener * listener_;

				toolbox::mem::Pool * pool_;
				toolbox::mem::MemoryPoolFactory * factory_;

		};

	}
}

#endif
