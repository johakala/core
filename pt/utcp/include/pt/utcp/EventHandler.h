// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2011, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A.Petrucci, C.Wakefield							 *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _pt_utcp_EventHandler_h_
#define _pt_utcp_EventHandler_h_

#include <iostream>
#include <string>
#include <list>

#include "tcpla/Event.h"
#include "tcpla/EventHandler.h"
#include "pt/exception/UnknownProtocolOrService.h"
#include "pt/Listener.h"
#include "pt/Messenger.h"
#include "pt/Address.h"

namespace pt
{
	namespace utcp
	{
		class PeerTransport;
		class InterfaceAdapter;

		class EventHandler : public tcpla::EventHandler
		{
			public:

				virtual ~EventHandler ()
				{
				}

				//virtual void handleEvent(tcpla::Event & e) = 0;

				virtual pt::Messenger::Reference getMessenger (pt::Address::Reference destination, pt::Address::Reference local, tcpla::InterfaceAdapter * ia, pt::utcp::PeerTransport * pt) throw (pt::exception::UnknownProtocolOrService) = 0;

				virtual void addServiceListener (pt::Listener * listener) throw (pt::exception::Exception) = 0;

				virtual void removeServiceListener (pt::Listener * listener) throw (pt::exception::Exception) = 0;

				virtual void removeAllServiceListeners () = 0;

				//virtual std::string toHTML () = 0;

				virtual std::string getService () = 0;

				virtual void gc() = 0;

				/*friend std::ostream& operator<< ( std::ostream &sout, pt::utcp::EventHandler & eh )
				 {
				 sout << eh.toHTML();

				 return sout;
				 }
				 */

			protected:

				std::list<pt::Messenger::Reference> messengers_;
				//std::list<tcpla::EndPoint *> accepted_;

				//std::vector<pt::utcp::PublicServicePoint*> psp_;
				size_t eventCounter_[tcpla::MAX_TCPLA_EVENT_NUMBER];

				size_t maxClients_;
				size_t maxReceiveBuffers_;
				size_t ioQueueSize_;
				size_t maxBlockSize_;
		};

	}
}

#endif
