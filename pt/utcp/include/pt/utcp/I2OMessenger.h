// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2012, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A.Petrucci, C.Wakefield				 	 *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _pt_utcp_I2OMessenger_h
#define _pt_utcp_I2OMessenger_h

#include "i2o/Messenger.h"
#include "toolbox/mem/Reference.h"

#include "tcpla/Address.h"
#include "tcpla/EndPoint.h"
#include "tcpla/InterfaceAdapter.h"

#include "pt/utcp/Messenger.h"

#include "pt/utcp/exception/Exception.h"

namespace pt
{
	namespace utcp
	{

		class PeerTransport;

		class I2OMessenger : public i2o::Messenger, public pt::utcp::Messenger
		{
			public:

				I2OMessenger (pt::utcp::PeerTransport * pt, tcpla::InterfaceAdapter * ia, pt::Address::Reference local, pt::Address::Reference destination);

				virtual ~I2OMessenger ();

				void send (toolbox::mem::Reference* msg, toolbox::exception::HandlerSignature* handler, void* context) throw (pt::exception::Exception);

				pt::Address::Reference getLocalAddress ();
				pt::Address::Reference getDestinationAddress ();

				void reset ();

			private:
				bool connectOnRequest_;
				size_t  numberOfRetries_; // connection timeout

		};

	}
}

#endif

