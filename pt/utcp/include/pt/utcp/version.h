// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2012, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A.Petrucci, C.Wakefield, A.Forrest			 	 *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

//
// Version definition for UDAPL peer transport
//
#ifndef _ptutcp_version_h_
#define _ptutcp_version_h_

#include "config/PackageInfo.h"
// !!! Edit this line to reflect the latest package version !!!

#define PTUTCP_VERSION_MAJOR 4
#define PTUTCP_VERSION_MINOR 3
#define PTUTCP_VERSION_PATCH 0
// If any previous versions available E.g. #define PTUTCP_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#define PTUTCP_PREVIOUS_VERSIONS "4.0.0,4.1.0,4.1.1,4.1.2,4.1.3,4.1.4,4.1.5,4.2.0,4.2.1,4.2.2,4.2.3,4.2.4,4.2.5,4.2.6"


//
// Template macros
//
#define PTUTCP_VERSION_CODE PACKAGE_VERSION_CODE(PTUTCP_VERSION_MAJOR,PTUTCP_VERSION_MINOR,PTUTCP_VERSION_PATCH)
#ifndef PTUTCP_PREVIOUS_VERSIONS
#define PTUTCP_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(PTUTCP_VERSION_MAJOR,PTUTCP_VERSION_MINOR,PTUTCP_VERSION_PATCH)
#else 
#define PTUTCP_FULL_VERSION_LIST  PTUTCP_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(PTUTCP_VERSION_MAJOR,PTUTCP_VERSION_MINOR,PTUTCP_VERSION_PATCH)
#endif 
namespace ptutcp
{
    const std::string package  =  "ptutcp";
    const std::string versions =  PTUTCP_FULL_VERSION_LIST;
    const std::string summary = "ptuTCP";
    const std::string description = "Universal TCP peer transport";
    const std::string authors = "Luciano Orsini, Andrea Petrucci, Chris Wakefield, Andy Forrest";
    const std::string link = "http://xdaq.web.cern.ch";
    config::PackageInfo getPackageInfo();
    void checkPackageDependencies() throw (config::PackageInfo::VersionException);
    std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif

