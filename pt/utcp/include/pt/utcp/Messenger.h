// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2012, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A.Petrucci, C.Wakefield				 	 *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _pt_utcp_Messenger_h
#define _pt_utcp_Messenger_h

#include "i2o/Messenger.h"
#include "toolbox/mem/Reference.h"

#include "tcpla/Address.h"
#include "tcpla/EndPoint.h"
#include "tcpla/InterfaceAdapter.h"
#include "tcpla/Event.h"

#include "pt/utcp/exception/Exception.h"
#include "tcpla/exception/ConnectionNotEstablished.h"
#include "tcpla/exception/CannotConnect.h"
#include "tcpla/exception/CorruptMessageFrame.h"

#include "toolbox/BSem.h"
//#include "pt/utcp/PeerTransport.h"

namespace pt
{
	namespace utcp
	{

		class PeerTransport;
		class Application;
		class I2OEventHandler;
		class B2INEventHandler;

		class Messenger
		{
				friend class PeerTransport;
				friend class Application;
				friend class I2OEventHandler;
				friend class FRLEventHandler;

			public:

				Messenger (pt::utcp::PeerTransport * pt, tcpla::InterfaceAdapter * ia, pt::Address::Reference local, pt::Address::Reference destination);

				virtual ~Messenger ();

				bool match (tcpla::EndPoint & ep);
				//void reset ();
				void connectionEstablished (tcpla::EndPoint & ep);
				void postConnect () throw (pt::utcp::exception::Exception);

				bool established_;

			protected:

				pt::utcp::PeerTransport * pt_;
				pt::Address::Reference destination_;
				pt::Address::Reference local_;

				uint16_t identification_;
				toolbox::BSem mutex_;

			public:

				tcpla::EndPoint ep_;
				tcpla::InterfaceAdapter * ia_;
		};
	}
}

#endif

