// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2011, CERN.			                 		 *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A.Petrucci, C.Wakefield				 	 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                   	 *
 * For the list of contributors see CREDITS.   			      	 *
 *************************************************************************/

#ifndef _pt_utcp_PeerTransport_h
#define _pt_utcp_PeerTransport_h

#include <map>

#include "pt/PeerTransportSender.h"
#include "pt/PeerTransportReceiver.h"
#include "pt/PeerTransportAgent.h"
#include "i2o/Listener.h"
#include "pt/exception/Exception.h"

#include "pt/utcp/I2OMessenger.h"

#include "pt/Address.h"
#include "xdaq/Object.h"
#include "toolbox/mem/Reference.h"
#include "toolbox/BSem.h"
#include "toolbox/mem/MemoryPoolFactory.h"

//#include "pt/utcp/ErrorHandler.h"
#include "pt/utcp/EventHandler.h"

#include "tcpla/InterfaceAdapter.h"
#include "tcpla/EndPoint.h"

#include "xdata/Double.h"
#include "xdata/UnsignedInteger.h"

//#define UTCP_PROTOCOL "atcp" // not used

namespace pt
{

	namespace utcp
	{

		class PeerTransport : public pt::PeerTransportSender, public pt::PeerTransportReceiver, public xdaq::Object //,public pt::utcp::dat::ErrorHandler
		{
			public:

				PeerTransport (xdaq::Application * parent, toolbox::mem::Pool * pool, size_t maxClients, size_t maxReceiveBuffers, size_t ioSize, size_t maxBlockSize, size_t eventQueueSize, const std::string & protocol);

				~PeerTransport ();

				friend std::ostream& operator<< (std::ostream &sout, pt::utcp::PeerTransport & pt);

				//! Enqueue a binary message to the peer transport's work loop. Raise an exception if queue is full
				//
				//void post (toolbox::mem::Reference * ref, pt::utcp::EndPoint * ep, toolbox::exception::HandlerSignature * handler, void * context)
				//throw (pt::exception::Exception );

				//! Retrieve the type of peer transport (Sender or Receiver or both)
				//
				pt::TransportType getType ();

				void addHandler (pt::utcp::EventHandler * handler);

				pt::Address::Reference createAddress (const std::string& url, const std::string& service) throw (pt::exception::InvalidAddress);

				pt::Address::Reference createAddress (std::map<std::string, std::string, std::less<std::string> > & address) throw (pt::exception::InvalidAddress);

				//! Returns the implemented protocol ("loopback" in this version)
				//
				std::string getProtocol ();

				//! Retrieve a list of supported services (i2o, b2in, frl)
				//
				std::vector<std::string> getSupportedServices ();

				//! Returns true if a service is supported (i2o, b2in, frl), false otherwise
				//
				bool isServiceSupported (const std::string & service);

				//! Retrieve a loopback messenger for the fifo peer transport that allows context internal application communication
				//
				pt::Messenger::Reference getMessenger (pt::Address::Reference destination, pt::Address::Reference local) throw (pt::exception::UnknownProtocolOrService);

				//! Internal function to add a message processing listener for this peer transport
				//
				void addServiceListener (pt::Listener * listener) throw (pt::exception::Exception);

				//! Internal function to remove a message processing listener for this peer transport
				//
				void removeServiceListener (pt::Listener * listener) throw (pt::exception::Exception);

				//! Internal function to remove all message processing listeners for this peer transport
				//
				void removeAllServiceListeners ();

				//! Function to configure this peer transport with a loopback address
				//
				void config (pt::Address::Reference address) throw (pt::exception::Exception);

				//void handleError (xcept::Exception & s);

				//void connect (pt::utcp::EndPoint * ep, const std::string & host, size_t port)

				//throw (pt::exception::Exception);
				//std::list<pt::Messenger::Reference> messengers_;
				//std::list<pt::utcp::EndPoint *> accepted_;

				std::map<std::string, tcpla::InterfaceAdapter *> iav_;
				std::vector<tcpla::PublicServicePoint*> psp_;
				std::map<std::string, size_t> eventCounter_;
				std::string lastEvent_;

				toolbox::mem::Pool * pool_;
				size_t maxBlockSize_;

				void gc();

			protected:

				toolbox::BSem mutex_;

				toolbox::mem::MemoryPoolFactory * factory_;

				size_t maxClients_;
				size_t maxReceiveBuffers_;
				size_t ioQueueSize_;
				size_t eventQueueSize_;

				std::map<std::string, pt::utcp::EventHandler *> handlers_;

				std::string protocol_;

			private:

				tcpla::InterfaceAdapter * getInterfaceAdapter (pt::Address::Reference address, pt::utcp::EventHandler * eh) throw (pt::utcp::exception::Exception);

		};

	}
}

#endif
