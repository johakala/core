// $Id: CommittedHeapBuffer.cc,v 1.5 2008/07/18 15:27:40 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A.Petrucci, C.Wakefield					 *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include "pt/utcp/PeerTransport.h"
#include "tcpla/PublicServicePoint.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "pt/exception/ReceiveFailure.h"
#include "i2o/i2o.h"
#include "xcept/tools.h"

#include "pt/utcp/I2OEventHandler.h"
#include "pt/utcp/I2OMessenger.h"

#include "tcpla/Utils.h"

#include "toolbox/hexdump.h"

pt::utcp::I2OEventHandler::I2OEventHandler (xdaq::Application * owner, toolbox::mem::Pool * pool, size_t maxClients, size_t maxReceiveBuffers, size_t ioQueueSize, size_t maxBlockSize) throw (pt::utcp::exception::Exception)
	: xdaq::Object(owner), mutex_(toolbox::BSem::FULL, true), pool_(pool)
{
	this->maxReceiveBuffers_ = maxReceiveBuffers;

	this->ioQueueSize_ = ioQueueSize;
	this->maxClients_ = maxClients;
	this->maxBlockSize_ = maxBlockSize;

	for (size_t i = 0; i < tcpla::MAX_TCPLA_EVENT_NUMBER; i++)
	{
		eventCounter_[i] = 0;
	}

	factory_ = toolbox::mem::getMemoryPoolFactory();
}

pt::utcp::I2OEventHandler::~I2OEventHandler ()
{

}

void pt::utcp::I2OEventHandler::gc()
{

}

pt::Messenger::Reference pt::utcp::I2OEventHandler::getMessenger (pt::Address::Reference destination, pt::Address::Reference local, tcpla::InterfaceAdapter * ia, pt::utcp::PeerTransport * pt) throw (pt::exception::UnknownProtocolOrService)
{
	mutex_.take();

	for (std::list<pt::Messenger::Reference>::iterator mi = messengers_.begin(); mi != messengers_.end(); mi++)
	{
		pt::Messenger::Reference refcnt = *mi;
		pt::utcp::I2OMessenger * messenger = dynamic_cast<pt::utcp::I2OMessenger*>(&(*refcnt));
		// this is not precise enough, it fails if you have two processes on the same machine
		//if ( messenger->getDestinationAddress()->toString() == destination->toString() )

		tcpla::Address & currentAddr = dynamic_cast<tcpla::Address&>(*(messenger->getDestinationAddress()));
		tcpla::Address & destAddr = dynamic_cast<tcpla::Address&>(*destination);

		if ((currentAddr.getHost() == destAddr.getHost()) && (currentAddr.getConfigurePort() == destAddr.getConfigurePort()))
		{
			LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), "Messenger already created then use cached");

			pt::Messenger::Reference mr;
			mr = *mi;

			mutex_.give();

			return mr;
		}
	}

	std::stringstream ss;
	ss << "create Messenger for endpoints:" << destination->toString() << " - " << local->toString();
	LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), ss.str());

	pt::utcp::I2OMessenger* m = new pt::utcp::I2OMessenger(pt, ia, local, destination);
	pt::Messenger::Reference mr(m);

	messengers_.push_back(mr); // we remember any created messenger

	mutex_.give();

	return mr;
}

void pt::utcp::I2OEventHandler::addServiceListener (pt::Listener* listener) throw (pt::exception::Exception)
{

	if (listener->getService() == "i2o")
	{
		listener_ = dynamic_cast<i2o::Listener *>(listener);
	}
}

void pt::utcp::I2OEventHandler::removeServiceListener (pt::Listener* listener) throw (pt::exception::Exception)
{
	if (listener->getService() == "i2o")
	{
		listener_ = 0;
	}
}

std::string pt::utcp::I2OEventHandler::getService ()
{
	return "i2o";
}

void pt::utcp::I2OEventHandler::removeAllServiceListeners ()
{
	listener_ = 0;
}

/*void pt::utcp::PeerTransport::handleError(xcept::Exception & e)
 {
 std::cerr << "Asynchronous error " << xcept::stdformat_exception_history(e) << std::endl;
 }*/

// this callback can be invoked by several threads
void pt::utcp::I2OEventHandler::handleEvent (tcpla::Event & event)
{
	// All event handling goes here
	//std::cout << "Event Number: " << pt::utcp::eventToString(event.event_number) << std::endl;

	eventCounter_[event.event_number]++;
	//std::cout << "event: " << pt::utcp::eventToString(event.event_number) << " " << eventCounter_[pt::utcp::eventToString(event.event_number)] << std::endl;

	switch (event.event_number)
	{
		case tcpla::TCPLA_DTO_RCV_COMPLETION_EVENT:
		{

			toolbox::mem::Reference * ref = (toolbox::mem::Reference*) event.event_data.dto_completion_event_data.user_cookie.as_ptr;

			size_t blockSize = maxBlockSize_;

			if (event.event_data.dto_completion_event_data.psp->getAddress()->hasProperty("recvBlockSize"))
			{
				try
				{
					blockSize = toolbox::toUnsignedLong(event.event_data.dto_completion_event_data.psp->getAddress()->getProperty("recvBlockSize"));
				}
				catch (toolbox::exception::Exception & ex)
				{
					std::stringstream msg;
					msg << "Failed to parse recvBlockSize in tcpla Address";
					XCEPT_DECLARE_NESTED(pt::exception::ReceiveFailure, e, msg.str(), ex);
					this->getOwnerApplication()->notifyQualified("fatal", e);
					return;
				}
			}

			toolbox::mem::Reference * reference;
			try
			{
				reference = factory_->getFrame(pool_, blockSize); // 256KB max size for i2o messages
			}
			catch (toolbox::mem::exception::Exception & ex)
			{
				std::stringstream msg;
				msg << "Failed to allocate buffer for incoming message";
				XCEPT_DECLARE_NESTED(pt::exception::ReceiveFailure, e, msg.str(), ex);
				this->getOwnerApplication()->notifyQualified("fatal", e);
				return;
			}

			tcpla::TCPLA_CONTEXT cookie;
			cookie.as_ptr = reference; // rememeber which reference

			try
			{
				//LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), "ppost fresh buffer to receiver");
				event.event_data.dto_completion_event_data.psp->postRecvBuffer(event.event_data.dto_completion_event_data.ep_handle, cookie, (char *) reference->getDataLocation(), blockSize);
			}
			catch (tcpla::exception::FailedToPostSend & e)
			{
				ref->release();
				reference->release();
				std::stringstream msg;
				msg << "Failed to provide buffer for incoming message";
				XCEPT_DECLARE_NESTED(pt::exception::ReceiveFailure, ex, msg.str(), e);
				this->getOwnerApplication()->notifyQualified("fatal", ex);
				return;
			}
			catch (tcpla::exception::InvalidEndPoint & e)
			{
				ref->release();
				reference->release();
				std::stringstream msg;
				msg << "Failed to provide buffer for incoming message";
				XCEPT_DECLARE_NESTED(pt::exception::ReceiveFailure, ex, msg.str(), e);
				this->getOwnerApplication()->notifyQualified("fatal", ex);
				return;
			}

			if (listener_ != 0)
			{
				//toolbox::hexdump ((void*) ref->getDataLocation(), 256);
				try
				{
					PI2O_MESSAGE_FRAME frame = (PI2O_MESSAGE_FRAME) ref->getDataLocation();
					ref->setDataSize(frame->MessageSize << 2);

					//mutex_.take();

					listener_->processIncomingMessage(ref);

					//mutex_.give();
				}
				catch (pt::exception::Exception & ex)
				{
					//mutex_.give();
					std::cout << xcept::stdformat_exception_history(ex) << std::endl;
				}
			}
			else
			{
				std::string msg = toolbox::toString("No Listener for i2o found in ReceiverLoop of pt::utcp");
				//std::cout << msg << std::endl;
				if (ref) ref->release();
				XCEPT_DECLARE(pt::exception::ReceiveFailure, e, msg);
				this->getOwnerApplication()->notifyQualified("fatal", e);
				return;
			}
		}
			break;

		case tcpla::TCPLA_DTO_SND_COMPLETION_EVENT:
		{
			toolbox::mem::Reference * ref = (toolbox::mem::Reference*) event.event_data.dto_completion_event_data.user_cookie.as_ptr;

			if (ref != 0)
			{
				ref->release();
			}
		}
			break;

		case tcpla::TCPLA_CONNECTION_REQUEST_EVENT: // server only
		{
			LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), "Connection request");

			mutex_.take();

			tcpla::EndPoint ep_handle = event.event_data.cr_event_data.psp->ia_->createEndPoint(event.event_data.cr_event_data);

			size_t blockSize = maxBlockSize_;

			if (event.event_data.cr_event_data.psp->getAddress()->hasProperty("recvBlockSize"))
			{
				try
				{
					blockSize = toolbox::toUnsignedLong(event.event_data.cr_event_data.psp->getAddress()->getProperty("recvBlockSize"));
				}
				catch (toolbox::exception::Exception & ex)
				{
					std::stringstream msg;
					msg << "Failed to parse recvBlockSize in tcpla Address";
					XCEPT_DECLARE_NESTED(pt::exception::ReceiveFailure, e, msg.str(), ex);
					this->getOwnerApplication()->notifyQualified("fatal", e);
					return;
				}
			}

			// post 12 buffers of 256 kB on establised receiver endpoint
			for (size_t k = 0; k < this->maxReceiveBuffers_; k++)
			{
				//std::cout << "going to push buffer on receiver endpoint" << std::endl;
				toolbox::mem::Reference * reference;

				try
				{
					reference = factory_->getFrame(pool_, blockSize); // 256KB max size for i2o messages
				}
				catch (toolbox::mem::exception::Exception & ex)
				{
					std::stringstream msg;
					msg << "Failed to allocate buffer for incoming message";
					XCEPT_DECLARE_NESTED(pt::exception::ReceiveFailure, e, msg.str(), ex);
					this->getOwnerApplication()->notifyQualified("fatal", e);
					return;
				}

				tcpla::TCPLA_CONTEXT cookie;
				cookie.as_ptr = reference; // rememeber which reference

				try
				{
					ep_handle.psp_->postRecvBuffer(ep_handle, cookie, (char *) reference->getDataLocation(), blockSize);
				}
				catch (tcpla::exception::FailedToPostSend & e)
				{
					std::stringstream msg;
					msg << "Failed to allocate buffer for incoming message";
					XCEPT_DECLARE_NESTED(pt::exception::ReceiveFailure, ex, msg.str(), e);
					this->getOwnerApplication()->notifyQualified("fatal", ex);
					reference->release();
					return;
				}
				catch (tcpla::exception::InvalidEndPoint & e)
				{
					std::stringstream msg;
					msg << "Failed to allocate buffer for incoming message";
					XCEPT_DECLARE_NESTED(pt::exception::ReceiveFailure, ex, msg.str(), e);
					this->getOwnerApplication()->notifyQualified("fatal", ex);
					reference->release();
					return;
				}
			}

			try
			{
				tcpla::TCPLA_CONTEXT cookie;
				cookie.as_ptr = 0;

				event.event_data.cr_event_data.psp->accept(ep_handle, event.event_data.cr_event_data.cr_handle, cookie);
			}
			catch (tcpla::exception::PSPCommandQueueFull & ex)
			{
				std::stringstream msg;
				msg << "Failed to accept connection";

				try
				{
					event.event_data.cr_event_data.psp->ia_->destroyEndPoint(ep_handle);
				}
				catch (tcpla::exception::IACommandQueueFull & ey)
				{
					msg << ", then failed to destroy created EndPoint (tcpla::exception::IACommandQueueFull not shown in exception stack trace)";
				}

				XCEPT_DECLARE_NESTED(pt::utcp::exception::Exception, e, msg.str(), ex);
				this->getOwnerApplication()->notifyQualified("fatal", e);

				mutex_.give();

				return;
			}

			// keep record of 0 connections
			// accepted_.push_back(ep_handle);

			mutex_.give();

			//std::cout << "Connection accepted done" << std::endl;

		}
			break;

		case tcpla::TCPLA_CONNECTION_EVENT_ESTABLISHED:
		{
			LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), "Connection event established");

			mutex_.take();

			for (std::list<pt::Messenger::Reference>::iterator mi = messengers_.begin(); mi != messengers_.end(); mi++)
			{
				pt::Messenger::Reference refcnt = *mi;
				pt::utcp::I2OMessenger * messenger = dynamic_cast<pt::utcp::I2OMessenger*>(&(*refcnt));

				if (messenger->match(event.event_data.connect_event_data.ep_handle))
				{
					messenger->connectionEstablished(event.event_data.connect_event_data.ep_handle); // pass connected endpoint
					break;
				}
			}

			mutex_.give();
		}
			break;

		case tcpla::TCPLA_CONNECTION_EVENT_ACCEPTED:
		{

			LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), "Establishing connection for receiver");
			/*
			 // post 12 buffers of 256 kB on establised receiver endpoint
			 for ( size_t k = 0; k < this->maxReceiveBuffers_; k++ )
			 {
			 //std::cout << "going to push buffer on receiver endpoint" << std::endl;
			 toolbox::mem::Reference * reference;

			 try
			 {
			 reference = factory_->getFrame(pool_, maxBlockSize_); // 256KB max size for i2o messages
			 }
			 catch ( toolbox::mem::exception::Exception & ex )
			 {
			 std::stringstream msg;
			 msg << "Failed to allocate buffer for incoming message";
			 XCEPT_DECLARE_NESTED(pt::exception::ReceiveFailure, e, msg.str(), ex);
			 this->getOwnerApplication()->notifyQualified("fatal", e);
			 return;
			 }

			 tcpla::TCPLA_CONTEXT cookie;
			 cookie.as_ptr = reference; // rememeber which reference

			 try
			 {
			 event.event_data.connect_event_data.ep_handle.psp_->postRecvBuffer(event.event_data.connect_event_data.ep_handle,cookie, (char *) reference->getDataLocation(), maxBlockSize_);
			 }
			 catch ( tcpla::exception::Exception & e )
			 {
			 std::stringstream msg;
			 msg << "Failed to allocate buffer for incoming message";
			 XCEPT_DECLARE_NESTED(pt::exception::ReceiveFailure, ex, msg.str(), e);
			 this->getOwnerApplication()->notifyQualified("fatal", ex);
			 reference->release();
			 return;

			 }

			 }
			 */
			return;
		}
			break;

		case tcpla::TCPLA_CONNECTION_EVENT_PEER_REJECTED:
		{
			if (event.event_data.asynch_error_event_data.exception != 0)
			{
				LOG4CPLUS_WARN(this->getOwnerApplication()->getApplicationLogger(), xcept::stdformat_exception_history(*(event.event_data.asynch_error_event_data.exception)));

				delete event.event_data.asynch_error_event_data.exception;
			}
		}
			break;

		case tcpla::TCPLA_CONNECTION_EVENT_NON_PEER_REJECTED:
		{

		}
			break;

		case tcpla::TCPLA_CONNECTION_EVENT_ACCEPT_COMPLETION_ERROR:
		{
			if (event.event_data.asynch_error_event_data.exception != 0)
			{
				LOG4CPLUS_WARN(this->getOwnerApplication()->getApplicationLogger(), xcept::stdformat_exception_history(*(event.event_data.asynch_error_event_data.exception)));

				delete event.event_data.asynch_error_event_data.exception;
			}
		}
			break;

		case tcpla::TCPLA_CONNECTION_EVENT_DISCONNECTED:
		{
			// sender connected endpoint
			LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), "disconnection event");

			if (event.event_data.connection_broken_event_data.exception != 0)
			{
					LOG4CPLUS_WARN(this->getOwnerApplication()->getApplicationLogger(), xcept::stdformat_exception_history(*(event.event_data.connection_broken_event_data.exception)));
					delete event.event_data.connection_broken_event_data.exception;
			}

			try
			{
				mutex_.take();

				for (std::list<pt::Messenger::Reference>::iterator mi = messengers_.begin(); mi != messengers_.end(); mi++)
				{
					pt::Messenger::Reference refcnt = *mi;
					pt::utcp::I2OMessenger * messenger = dynamic_cast<pt::utcp::I2OMessenger*>(&(*refcnt));

					if (messenger->match(event.event_data.connection_broken_event_data.ep_handle))
					{
						//std::cout << "C UTCP_CONNECTION_EVENT_DISCONNECTED" << std::endl;
						messenger->reset();
						break;
					}
				}

				mutex_.give();
			}
			catch (pt::utcp::exception::Exception & e)
			{
				mutex_.give();
				std::stringstream msg;
				msg << "Failed to destroy endpoint";
				XCEPT_DECLARE_NESTED(pt::exception::ReceiveFailure, ex, msg.str(), e);
				this->getOwnerApplication()->notifyQualified("fatal", ex);
				return;

			}
		}
			break;

		case tcpla::TCPLA_CONNECTION_CLOSED_BY_PEER:
		{
			if (event.event_data.connection_broken_event_data.exception != 0)
			{
				LOG4CPLUS_WARN(this->getOwnerApplication()->getApplicationLogger(), xcept::stdformat_exception_history(*(event.event_data.connection_broken_event_data.exception)));
				delete event.event_data.connection_broken_event_data.exception;
			}

			try
			{
				event.event_data.connection_broken_event_data.ia->destroyEndPoint(event.event_data.connection_broken_event_data.ep_handle);
			}
			catch (tcpla::exception::IACommandQueueFull & e)
			{
				std::stringstream msg;
				msg << "Failed to destroy endpoint";
				XCEPT_DECLARE_NESTED(pt::exception::Exception, ex, msg.str(), e);
				this->getOwnerApplication()->notifyQualified("fatal", ex);
				return;

			}
		}
			break;

		case tcpla::TCPLA_CONNECTION_EVENT_BROKEN: // server only
		{
			if (event.event_data.connection_broken_event_data.exception != 0)
			{
				LOG4CPLUS_WARN(this->getOwnerApplication()->getApplicationLogger(), xcept::stdformat_exception_history(*(event.event_data.connection_broken_event_data.exception)));
				delete event.event_data.connection_broken_event_data.exception;
			}

			try
			{
				event.event_data.connection_broken_event_data.ia->destroyEndPoint(event.event_data.connection_broken_event_data.ep_handle);
			}
			catch (tcpla::exception::IACommandQueueFull & e)
			{
				std::stringstream msg;
				msg << "Failed to destroy endpoint";
				XCEPT_DECLARE_NESTED(pt::exception::Exception, ex, msg.str(), e);
				this->getOwnerApplication()->notifyQualified("fatal", ex);
				return;

			}
		}
			break;

		case tcpla::TCPLA_CONNECTION_RESET_BY_PEER: //server only
		{
			if (event.event_data.connection_broken_event_data.exception != 0)
			{
				LOG4CPLUS_WARN(this->getOwnerApplication()->getApplicationLogger(), xcept::stdformat_exception_history(*(event.event_data.connection_broken_event_data.exception)));

				delete event.event_data.connection_broken_event_data.exception;
			}

			try
			{
				event.event_data.connection_broken_event_data.ia->destroyEndPoint(event.event_data.connection_broken_event_data.ep_handle);
			}
			catch (tcpla::exception::IACommandQueueFull & e)
			{
				std::stringstream msg;
				msg << "Failed to destroy endpoint";
				XCEPT_DECLARE_NESTED(pt::exception::Exception, ex, msg.str(), e);
				this->getOwnerApplication()->notifyQualified("fatal", ex);
				return;
			}
		}
			break;

		case tcpla::TCPLA_CONNECTION_EVENT_TIMED_OUT:

			break;

		case tcpla::TCPLA_CONNECTION_EVENT_UNREACHABLE:
		{	//server unreachable in IA
			LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), "Host unreachable");

			try
			{
				if (!event.event_data.connect_error_event_data.ep_handle.isNull())
				{
					event.event_data.connect_error_event_data.ep_handle.ia_->destroyEndPoint(event.event_data.connect_error_event_data.ep_handle);
				}
			}
			catch (tcpla::exception::IACommandQueueFull & e)
			{
				std::stringstream msg;
				msg << "Failed to destroy endpoint";
				XCEPT_DECLARE_NESTED(pt::exception::Exception, ex, msg.str(), e);
				this->getOwnerApplication()->notifyQualified("fatal", ex);
				return;
			}

			if (event.event_data.connect_error_event_data.exception != 0)
			{
				LOG4CPLUS_WARN(this->getOwnerApplication()->getApplicationLogger(), xcept::stdformat_exception_history(*(event.event_data.connect_error_event_data.exception)));

				delete event.event_data.connect_error_event_data.exception;
			}

		}
			break;

		case tcpla::TCPLA_ASYNC_ERROR_EVD_OVERFLOW:
		{
			if (event.event_data.asynch_error_event_data.exception != 0)
			{
				LOG4CPLUS_FATAL(this->getOwnerApplication()->getApplicationLogger(), xcept::stdformat_exception_history(*(event.event_data.asynch_error_event_data.exception)));

				delete event.event_data.asynch_error_event_data.exception;
			}
		}
			break;

		case tcpla::TCPLA_ASYNC_ERROR_IA_CATASTROPHIC:
		{

			if (event.event_data.asynch_error_event_data.exception != 0)
			{
				LOG4CPLUS_FATAL(this->getOwnerApplication()->getApplicationLogger(), xcept::stdformat_exception_history(*(event.event_data.asynch_error_event_data.exception)));

				delete event.event_data.asynch_error_event_data.exception;
			}
		}
			break;

		case tcpla::TCPLA_ASYNC_ERROR_EP_BROKEN:
		{
			if (event.event_data.asynch_error_event_data.exception != 0)
			{
				LOG4CPLUS_WARN(this->getOwnerApplication()->getApplicationLogger(), xcept::stdformat_exception_history(*(event.event_data.asynch_error_event_data.exception)));

				delete event.event_data.asynch_error_event_data.exception;
			}
		}
			break;

		case tcpla::TCPLA_ASYNC_ERROR_TIMED_OUT:

			break;

		case tcpla::TCPLA_ASYNC_ERROR_PROVIDER_INTERNAL_ERROR:
		{

			// broken pipe
			if (event.event_data.asynch_error_event_data.exception != 0)
			{
				LOG4CPLUS_WARN(this->getOwnerApplication()->getApplicationLogger(), xcept::stdformat_exception_history(*(event.event_data.asynch_error_event_data.exception)));

				delete event.event_data.asynch_error_event_data.exception;
			}

			try
			{
				if (!event.event_data.asynch_error_event_data.ep_handle.isNull())
				{
					event.event_data.asynch_error_event_data.ep_handle.ia_->destroyEndPoint(event.event_data.asynch_error_event_data.ep_handle);
				}
			}
			catch (tcpla::exception::IACommandQueueFull & e)
			{
				std::stringstream msg;
				msg << "Failed to destroy endpoint";
				XCEPT_DECLARE_NESTED(pt::exception::Exception, ex, msg.str(), e);
				this->getOwnerApplication()->notifyQualified("fatal", ex);
				return;
			}

			// attempt to recover this error by resetting messengers
			try
			{
				mutex_.take();

				for (std::list<pt::Messenger::Reference>::iterator mi = messengers_.begin(); mi != messengers_.end(); mi++)
				{
					pt::Messenger::Reference refcnt = *mi;
					pt::utcp::I2OMessenger * messenger = dynamic_cast<pt::utcp::I2OMessenger*>(&(*refcnt));

					if (messenger->match(event.event_data.asynch_error_event_data.ep_handle))
					{
						//std::cout << "C UTCP_CONNECTION_EVENT_DISCONNECTED" << std::endl;
						messenger->reset();
						break;
					}
				}

				mutex_.give();
			}
			catch (pt::utcp::exception::Exception & e)
			{
				mutex_.give();
				std::stringstream msg;
				msg << "Failed to destroy endpoint";
				XCEPT_DECLARE_NESTED(pt::exception::ReceiveFailure, ex, msg.str(), e);
				this->getOwnerApplication()->notifyQualified("fatal", ex);
				return;

			}



		}
			break;

		case tcpla::TCPLA_SOFTWARE_EVENT:

			break;

		case tcpla::TCPLA_POST_BLOCK_FAILED:
		case tcpla::TCPLA_DTO_SND_UNDELIVERABLE_BLOCK:
		{
			std::stringstream ss;
			ss << "Event: " << tcpla::eventToString(event.event_number);

			LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), ss.str());
			if (event.event_data.tcpla_dto_block_event_data.handler != 0)
			{
				XCEPT_DECLARE(pt::utcp::exception::Exception, e, "Undeliverable message");
				event.event_data.tcpla_dto_block_event_data.handler->invoke(e, event.event_data.tcpla_dto_block_event_data.context);
			}

			if (event.event_data.tcpla_dto_block_event_data.user_cookie.as_ptr != 0)
			{
				toolbox::mem::Reference * ref = (toolbox::mem::Reference*) event.event_data.tcpla_dto_block_event_data.user_cookie.as_ptr;

				ref->release();
			}
		}
			break;

		case tcpla::TCPLA_DTO_RCV_UNUSED_BLOCK:
		{

			/*pt::utcp::Event event;

			 event.event_number = UTCP_DTO_RCV_UNUSED_BLOCK;
			 event.event_data.utcp_dto_block_event_data.user_cookie = d.cookie;
			 event.event_data.utcp_dto_block_event_data.block_length = d.size;
			 event.event_data.utcp_dto_block_event_data.buffer = d.local_iov;*/

			//LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), "Recovering unused receive block");
			toolbox::mem::Reference * ref = (toolbox::mem::Reference*) event.event_data.tcpla_dto_block_event_data.user_cookie.as_ptr;

			ref->release();
		}
			break;

		default:
		{
			LOG4CPLUS_ERROR(this->getOwnerApplication()->getApplicationLogger(), "Unknown event");
		}
			break;
	}
}

std::string pt::utcp::I2OEventHandler::toHTML ()
{
	std::stringstream ss;

	ss << "			<table class=\"xdaq-table\">" << std::endl;
	ss << "				<caption>I2O Events</caption>";
	ss << "				<thead>" << std::endl;
	ss << "				<tr>" << std::endl;
	ss << "				<th>Name</th>" << std::endl;
	ss << "				<th>Count</th>" << std::endl;
	ss << "				</tr>" << std::endl;
	ss << "				</thead>" << std::endl;
	ss << "				<tbody>" << std::endl;

	for (size_t i = 0; i < tcpla::MAX_TCPLA_EVENT_NUMBER; i++)
	{
		ss << "					<tr>" << std::endl;
		ss << "					<td>" << tcpla::eventToString(i) << "</td>" << std::endl;
		ss << "					<td>" << toolbox::toString("%d", eventCounter_[i]) << "</td>" << std::endl;
		ss << "					</tr>" << std::endl;
	}

	ss << "				</tbody>" << std::endl;
	ss << "			</table>" << std::endl;

	return ss.str();
}
