// $Id: CommittedHeapBuffer.cc,v 1.5 2008/07/18 15:27:40 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A.Petrucci, C.Wakefield					 *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include "pt/utcp/PeerTransport.h"
#include "tcpla/PublicServicePoint.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "pt/exception/ReceiveFailure.h"
#include "xcept/tools.h"

#include "xdata/Properties.h"
#include "xdata/exdr/FixedSizeInputStreamBuffer.h"
#include "xdata/exdr/Serializer.h"

#include "pt/utcp/B2INEventHandler.h"
#include "pt/utcp/B2INMessenger.h"

#include "pt/utcp/B2INHeaderInfo.h"

#include "tcpla/Utils.h"

#include "toolbox/utils.h"

#include "toolbox/hexdump.h"

#include "pt/exception/SendFailure.h"

pt::utcp::B2INEventHandler::B2INEventHandler (xdaq::Application * owner, toolbox::mem::Pool * pool, size_t maxClients, size_t maxReceiveBuffers, size_t ioQueueSize, size_t maxBlockSize) throw (pt::utcp::exception::Exception)
	: xdaq::Object(owner), mutex_(toolbox::BSem::FULL), pool_(pool)
{
	this->maxReceiveBuffers_ = maxReceiveBuffers;
	this->ioQueueSize_ = ioQueueSize;
	this->maxClients_ = maxClients;
	this->maxBlockSize_ = maxBlockSize;

	for (size_t i = 0; i < tcpla::MAX_TCPLA_EVENT_NUMBER; i++)
	{
		eventCounter_[i] = 0;
	}

	factory_ = toolbox::mem::getMemoryPoolFactory();
}

pt::utcp::B2INEventHandler::~B2INEventHandler ()
{

}

pt::Messenger::Reference pt::utcp::B2INEventHandler::getMessenger (pt::Address::Reference destination, pt::Address::Reference local, tcpla::InterfaceAdapter * ia, pt::utcp::PeerTransport * pt) throw (pt::exception::UnknownProtocolOrService)
{
	mutex_.take();
	tcpla::Address & dest = dynamic_cast<tcpla::Address &>(*destination);
	tcpla::Address & loc = dynamic_cast<tcpla::Address &>(*local);

	std::stringstream ss;
	ss << "create messenger local: " << loc.toString() << " remote: " << dest.getProtocol() << "://" << dest.getHost() << ":" << dest.getConfigurePort();
	LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), ss.str());

	pt::Messenger* m = 0;
	try
	{
		m = new pt::utcp::B2INMessenger(pt, ia, local, destination);
	}
	catch(pt::exception::Exception & e)
	{
		mutex_.give();
		std::stringstream se;
		se << "Failed to create messenger, local: " << loc.toString() << " remote: " << dest.getProtocol() << "://" << dest.getHost() << ":" << dest.getConfigurePort();
		XCEPT_RETHROW(pt::exception::UnknownProtocolOrService, se.str(), e);
	}
	pt::Messenger::Reference mr(m);

	messengers_.push_back(mr);
    mutex_.give();

	return mr;
}

void pt::utcp::B2INEventHandler::addServiceListener (pt::Listener* listener) throw (pt::exception::Exception)
{

	if (listener->getService() == "b2in")
	{
		listener_ = dynamic_cast<b2in::nub::Listener *>(listener);
	}
}

void pt::utcp::B2INEventHandler::removeServiceListener (pt::Listener* listener) throw (pt::exception::Exception)
{
	if (listener->getService() == "b2in")
	{
		listener_ = 0;
	}
}

std::string pt::utcp::B2INEventHandler::getService ()
{
	return "b2in";
}

void pt::utcp::B2INEventHandler::removeAllServiceListeners ()
{
	listener_ = 0;
}

/*void pt::utcp::PeerTransport::handleError(xcept::Exception & e)
 {
 std::cerr << "Asynchronous error " << xcept::stdformat_exception_history(e) << std::endl;
 }*/

// this callback can be invoked by several threads
void pt::utcp::B2INEventHandler::handleEvent (tcpla::Event & event)
{
	eventCounter_[event.event_number]++;

	switch (event.event_number)
	{
		case tcpla::TCPLA_DTO_RCV_COMPLETION_EVENT:
		{
			//std::cout << "Received a message from FD #" << event.event_data.dto_completion_event_data.ep_handle.getFD() << std::endl;
			toolbox::mem::Reference * ref = 0;

			size_t headerSize = event.event_data.dto_completion_event_data.psp->ia_->ghf_();

			ref = (toolbox::mem::Reference*) event.event_data.dto_completion_event_data.user_cookie.as_ptr;
			PB2IN_MESSAGE_FRAME header = (PB2IN_MESSAGE_FRAME) ref->getDataLocation();

			toolbox::net::UUID chainUUID (event.event_data.dto_completion_event_data.ep_handle.uuid_);

			if (header->version != B2IN_VERSION)
			{
				// no resources available for connection requets, therefore reject connection
				try
				{
					ref->release();

					mutex_.take();
					this->netFilter_.clear(chainUUID);
					mutex_.give();

					event.event_data.dto_completion_event_data.psp->ia_->destroyEndPoint(event.event_data.dto_completion_event_data.ep_handle);

					std::stringstream msg;
					msg << "Incoming message using invalid protocol version (connection is closed), expected " << std::hex << (size_t)B2IN_VERSION << std::dec << ", received " << std::hex << (size_t)header->version << std::dec;
					LOG4CPLUS_ERROR(this->getOwnerApplication()->getApplicationLogger(), msg.str());
					XCEPT_DECLARE(pt::exception::ReceiveFailure, e, msg.str());
					this->getOwnerApplication()->notifyQualified("error", e);
					return;

				}
				catch (tcpla::exception::IACommandQueueFull & e)
				{
						std::stringstream msg;
						msg << "Failed to destroy endpoint for reject connection";
						XCEPT_DECLARE_NESTED(pt::exception::Exception, ex, msg.str(), e);
						this->getOwnerApplication()->notifyQualified("fatal", ex);
						return;
				}

			}

			if (header->flags != tcpla::TCPLA_CONNECTION_CLOSE) // user requested a connection keep alive false therefore do not post buffer to prevent error
			{
				toolbox::mem::Reference * reference;

				size_t blockSize = maxBlockSize_;

				if (event.event_data.dto_completion_event_data.psp->getAddress()->hasProperty("recvBlockSize"))
				{
					try
					{
						blockSize = toolbox::toUnsignedLong(event.event_data.dto_completion_event_data.psp->getAddress()->getProperty("recvBlockSize"));
					}
					catch (toolbox::exception::Exception & ex)
					{
						mutex_.take();
						this->netFilter_.clear(chainUUID);
						mutex_.give();
						std::stringstream msg;
						msg << "Failed to parse recvBlockSize in tcpla Address";
						XCEPT_DECLARE_NESTED(pt::exception::ReceiveFailure, e, msg.str(), ex);
						this->getOwnerApplication()->notifyQualified("fatal", e);
						return;
					}
				}

				try
				{
					reference = factory_->getFrame(pool_, blockSize); // maxBlockSize == MTU
				}
				catch (toolbox::mem::exception::Exception & ex)
				{
					mutex_.take();
					this->netFilter_.clear(chainUUID);
					mutex_.give();
					ref->release();
					std::stringstream msg;
					msg << "Failed to allocate buffer for incoming message";
					XCEPT_DECLARE_NESTED(pt::exception::ReceiveFailure, e, msg.str(), ex);
					this->getOwnerApplication()->notifyQualified("fatal", e);
					return;
				}

				tcpla::TCPLA_CONTEXT cookie;
				cookie.as_ptr = reference; // rememeber which reference

				try
				{
					//LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), "ppost fresh buffer to receiver");
					event.event_data.dto_completion_event_data.psp->postRecvBuffer(event.event_data.dto_completion_event_data.ep_handle, cookie, (char *) reference->getDataLocation(), blockSize);
				}
				catch (tcpla::exception::FailedToPostSend & e)
				{
					mutex_.take();
					this->netFilter_.clear(chainUUID);
					mutex_.give();
					ref->release();
					reference->release();
					std::stringstream msg;
					msg << "Failed to provide buffer for incoming message";
					XCEPT_DECLARE_NESTED(pt::exception::ReceiveFailure, ex, msg.str(), e);
					this->getOwnerApplication()->notifyQualified("fatal", ex);
					return;
				}
				catch (tcpla::exception::InvalidEndPoint & e)
				{
					mutex_.take();
					this->netFilter_.clear(chainUUID);
					mutex_.give();
					ref->release();
					reference->release();
					std::stringstream msg;
					msg << "Failed to provide buffer for incoming message";
					XCEPT_DECLARE_NESTED(pt::exception::ReceiveFailure, ex, msg.str(), e);
					this->getOwnerApplication()->notifyQualified("fatal", ex);
					return;
				}
			}

			if (listener_ != 0)
			{
				std::pair<uint16_t, toolbox::net::UUID> chainKey(header->identification, toolbox::net::UUID(event.event_data.dto_completion_event_data.ep_handle.uuid_));

				toolbox::mem::Reference * chain = 0;

				size_t chainTotalSize;

				try
				{
					mutex_.take();

					//skip the header
					ref->setDataOffset(headerSize);
					ref->setDataSize((header->length) - headerSize);

					//std::cout << "Appending... header->length = " << header->length << std::endl;
					chain = this->netFilter_.append(ref, header->packet, header->total, chainKey, chainTotalSize);
					mutex_.give();
				}
				catch (pt::utcp::exception::Exception & e)
				{
					this->netFilter_.clear(chainUUID);
					ref->release();
					mutex_.give();
					std::stringstream msg;
					msg << "Failed to build chain";

					XCEPT_DECLARE_NESTED(pt::utcp::exception::Exception, pte, msg.str(), e);
					this->getOwnerApplication()->notifyQualified("error", pte);
				}

				if (chain == 0)
				{
					// chain is building up, still need to receive other packets to complete and deliver
					return;
				}

				xdata::exdr::FixedSizeInputStreamBuffer inBuffer((char*) chain->getDataLocation(), chain->getDataSize());
				xdata::Properties plist;

				try
				{
					xdata::exdr::Serializer serializer;
					serializer.import(&plist, &inBuffer);
				}
				catch (xdata::exception::Exception & e)
				{
					// ignore message and keep alive <- does this release ref?
					chain->release();

					std::stringstream msg;
					msg << "Failed to parse B2IN properties in message from peer";

					XCEPT_DECLARE_NESTED(pt::utcp::exception::Exception, pte, msg.str(), e);
					this->getOwnerApplication()->notifyQualified("error", pte);

					return;
				}

				if (plist.getProperty("urn:b2in-protocol-tcp:connection") == "close")
				{

					mutex_.take();
					this->netFilter_.clear(toolbox::net::UUID(event.event_data.dto_completion_event_data.ep_handle.uuid_));
					mutex_.give();

					try
					{
						LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), "Server connection close request, destroy EndPoint");

						event.event_data.dto_completion_event_data.psp->ia_->destroyEndPoint(event.event_data.dto_completion_event_data.ep_handle);
					}
					catch (tcpla::exception::IACommandQueueFull & e)
					{
						std::stringstream msg;
						msg << "Failed to destroy endpoint";
						XCEPT_DECLARE_NESTED(pt::exception::Exception, ex, msg.str(), e);
						this->getOwnerApplication()->notifyQualified("fatal", ex);
						return;
					}
				}

				// process chain to create contiguous block
				toolbox::mem::Reference * chainRef = chain->getNextReference();

				// copy payload chain into a contiguous block ( a chain of 2 elements or more )
				if ( (chainRef != 0) && (chainRef->getNextReference() != 0 ) )
				{
					toolbox::mem::Reference * payloadRef = 0;
					try
					{
						payloadRef = factory_->getFrame(pool_, chainTotalSize);
					}
					catch (toolbox::mem::exception::Exception & ex)
					{
						chain->release();
						std::stringstream msg;
						msg << "Failed to allocate buffer for incoming message payload";
						XCEPT_DECLARE_NESTED(pt::exception::ReceiveFailure, e, msg.str(), ex);
						this->getOwnerApplication()->notifyQualified("fatal", e);
						return;
					}

					size_t payloadOffset = 0;

					// build contiguous block

					std::stringstream debugStream;
					debugStream << "Received complete message, size = " << chainTotalSize;

					int testCounter = 1;
					while (chainRef != 0)
					{
						//std::cout << "Block #" << testCounter++ << ", payloadOffset:" << payloadOffset << ", size:" << chainRef->getDataSize() << std::endl;
						testCounter++;

						::memcpy(((char*) (payloadRef->getDataLocation())) + payloadOffset, chainRef->getDataLocation(), chainRef->getDataSize());
						payloadOffset += chainRef->getDataSize();

						chainRef = chainRef->getNextReference();
					}

					debugStream << ", message delivered in " << testCounter << " blocks";
					LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), debugStream.str());

					payloadRef->setDataSize(chainTotalSize);

					// free the piece of chain that contain the original payload
					toolbox::mem::Reference * payloadChain = chain->getNextReference();
					payloadChain->release();

					// insert block containing properties as first in chain to make listener happy
					chain->setNextReference(payloadRef);
				}

				try
				{
					listener_->processIncomingMessage(chain, plist);
				}
				catch (b2in::nub::exception::Exception & e)
				{
					chain->release();

					std::stringstream msg;
					msg << "Failed to process b2in message from peer";

					XCEPT_DECLARE_NESTED(pt::utcp::exception::Exception, pte, msg.str(), e);
					this->getOwnerApplication()->notifyQualified("error", pte);

					return;
				}
			}
			else
			{
				if (ref)
				{
					ref->release();
				}

				XCEPT_DECLARE(pt::exception::ReceiveFailure, e, "No Listener for b2in found in ReceiverLoop of pt::utcp");
				this->getOwnerApplication()->notifyQualified("fatal", e);

				return;
			}
		}
			break;

		case tcpla::TCPLA_DTO_SND_COMPLETION_EVENT:
		{
			//std::cout << "Sent a message to FD #" << event.event_data.dto_completion_event_data.ep_handle.getFD() << std::endl;

			toolbox::mem::Reference * ref = (toolbox::mem::Reference*) event.event_data.dto_completion_event_data.user_cookie.as_ptr;

			uint32_t flags = event.event_data.dto_completion_event_data.flags;

			if (ref != 0)
			{
				//std::cout << "Releasing MESSENGER  BUFFER ----------------" << std::endl;

				ref->release();
			}

			//LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), "Flags for this send event are:" << flags );
			if (flags & tcpla::TCPLA_CONNECTION_CLOSE)
			{
				LOG4CPLUS_TRACE(this->getOwnerApplication()->getApplicationLogger(), "Connection close request flags tcpla::TCPLA_CONNECTION_CLOSE performed");
				//send completion successful user request close of connection
				try
				{
					event.event_data.dto_completion_event_data.ep_handle.ia_->destroyEndPoint(event.event_data.dto_completion_event_data.ep_handle);
				}
				catch (tcpla::exception::IACommandQueueFull & e)
				{
					std::stringstream msg;
					msg << "Failed to destroy endpoint after receiving tcpla::TCPLA_CONNECTION_CLOSE flag";
					XCEPT_DECLARE_NESTED(pt::exception::Exception, ex, msg.str(), e);
					this->getOwnerApplication()->notifyQualified("warn", ex);
					return;
				}
			}
		}
			break;

		case tcpla::TCPLA_CONNECTION_REQUEST_EVENT: // server only
		{
			LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), "Connection request");

			mutex_.take();

			tcpla::EndPoint ep_handle = event.event_data.cr_event_data.psp->ia_->createEndPoint(event.event_data.cr_event_data);

			// post 12 buffers of 256 kB on establised receiver endpoint
			for (size_t k = 0; k < this->maxReceiveBuffers_; k++)
			{
				toolbox::mem::Reference * reference;

				size_t blockSize = maxBlockSize_;

				if (event.event_data.cr_event_data.psp->getAddress()->hasProperty("recvBlockSize"))
				{
					try
					{
						blockSize = toolbox::toUnsignedLong(event.event_data.cr_event_data.psp->getAddress()->getProperty("recvBlockSize"));
					}
					catch (toolbox::exception::Exception & ex)
					{
						mutex_.give();
						std::stringstream msg;
						msg << "Failed to parse recvBlockSize in tcpla Address";
						XCEPT_DECLARE_NESTED(pt::exception::ReceiveFailure, e, msg.str(), ex);
						this->getOwnerApplication()->notifyQualified("fatal", e);
						return;
					}
				}

				try
				{
					reference = factory_->getFrame(pool_, blockSize); // maxBlockSize == MTU
				}
				catch (toolbox::mem::exception::Exception & ex)
				{

					// no resources available for connection requets, therefore reject connection
					try
					{
							LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), "no resources available for connection requets, therefore reject connection");

							event.event_data.cr_event_data.psp->ia_->destroyEndPoint(ep_handle);
					}
					catch (tcpla::exception::IACommandQueueFull & e)
					{
							mutex_.give();
							std::stringstream msg;
							msg << "Failed to destroy endpoint for reject connection";
							XCEPT_DECLARE_NESTED(pt::exception::Exception, ex, msg.str(), e);
							this->getOwnerApplication()->notifyQualified("fatal", ex);
							return;
					}


					// end handling of connection rejection
					mutex_.give();
					std::stringstream msg;
					msg << "Failed to provide buffer for incoming connection request, insufficient resources, connection is rejected";
					XCEPT_DECLARE_NESTED(pt::exception::ReceiveFailure, e, msg.str(), ex);
					this->getOwnerApplication()->notifyQualified("warning", e);
					return;
				}

				tcpla::TCPLA_CONTEXT cookie;
				cookie.as_ptr = reference; // rememeber which reference

				try
				{
					//std::cout << "Posting to receive on endpoint UUID = '" << event.event_data.connect_event_data.ep_handle.toString() << "'" << std::endl;
					event.event_data.cr_event_data.psp->postRecvBuffer(ep_handle, cookie, (char *) reference->getDataLocation(), blockSize);
				}
				catch (tcpla::exception::FailedToPostSend & e)
				{
					std::cout << "Failed posting to receive on endpoint UUID = '" << event.event_data.connect_event_data.ep_handle.toString() << "'" << std::endl;
					std::stringstream msg;
					msg << "Failed to allocate buffer for incoming message";
					XCEPT_DECLARE_NESTED(pt::exception::ReceiveFailure, ex, msg.str(), e);
					this->getOwnerApplication()->notifyQualified("fatal", ex);
					reference->release();
					mutex_.give();
					return;
				}
				catch (tcpla::exception::InvalidEndPoint & e)
				{
					std::cout << "Failed posting to receive on endpoint UUID = '" << event.event_data.connect_event_data.ep_handle.toString() << "'" << std::endl;
					std::stringstream msg;
					msg << "Failed to allocate buffer for incoming message";
					XCEPT_DECLARE_NESTED(pt::exception::ReceiveFailure, ex, msg.str(), e);
					this->getOwnerApplication()->notifyQualified("fatal", ex);
					reference->release();
					mutex_.give();
					return;
				}
			}

			try
			{
				tcpla::TCPLA_CONTEXT cookie;
				cookie.as_ptr = 0;

				event.event_data.cr_event_data.psp->accept(ep_handle, event.event_data.cr_event_data.cr_handle, cookie);
			}
			catch (tcpla::exception::PSPCommandQueueFull & ex)
			{
				std::stringstream msg;
				msg << "Failed to accept connection";

				try
				{
					event.event_data.cr_event_data.psp->ia_->destroyEndPoint(ep_handle);
				}
				catch (tcpla::exception::IACommandQueueFull & ey)
				{
					msg << ", then failed to destroy created EndPoint (tcpla::exception::IACommandQueueFull not shown in exception stack trace)";
				}

				XCEPT_DECLARE_NESTED(pt::utcp::exception::Exception, e, msg.str(), ex);
				this->getOwnerApplication()->notifyQualified("fatal", e);

				mutex_.give();

				return;
			}

			// keep record of 0 connections
			// accepted_.push_back(ep_handle);

			mutex_.give();
		}
			break;

		case tcpla::TCPLA_CONNECTION_EVENT_ESTABLISHED:
		{
			LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), "Connection event established");

			mutex_.take();

			//pt::utcp::Messenger * messenger = static_cast<pt::utcp::Messenger *>(event.event_data.connect_event_data.user_cookie.as_ptr);

			std::list<pt::Messenger::Reference>::iterator mi;
			for (mi = messengers_.begin(); mi != messengers_.end(); mi++)
			{
				pt::Messenger::Reference refcnt = *mi;
				pt::utcp::B2INMessenger * messenger = dynamic_cast<pt::utcp::B2INMessenger*>(&(*refcnt));

				//std::cout << "Attempting match : " << event.event_data.connect_event_data.ep_handle << " : " << messenger->ep_ << std::endl;

				if (messenger->match(event.event_data.connect_event_data.ep_handle))
				{
					//std::cout << "Removing messenger based upon uuid " << event.event_data.connect_event_data.ep_handle << std::endl;
					messenger->connectionEstablished(event.event_data.connect_event_data.ep_handle); // pass connected endpoint
					//messengers_.erase(mi);
					break;
				}
			}

			if (mi == messengers_.end())
			{
				LOG4CPLUS_WARN(this->getOwnerApplication()->getApplicationLogger(), "Could not match messenger to delete from connection cache, ep = " << event.event_data.connect_event_data.ep_handle);
			}

			mutex_.give();
		}
			break;

		case tcpla::TCPLA_CONNECTION_EVENT_ACCEPTED:
		{
			LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), "Establishing connection for receiver - #" << event.event_data.connect_event_data.ep_handle.getFD());

			/*
			 // post 12 buffers of 256 kB on establised receiver endpoint
			 for ( size_t k = 0; k < this->maxReceiveBuffers_; k++ )
			 {
			 toolbox::mem::Reference * reference;

			 try
			 {
			 reference = factory_->getFrame(pool_, maxBlockSize_); // maxBlockSize == MTU
			 }
			 catch ( toolbox::mem::exception::Exception & ex )
			 {
			 std::stringstream msg;
			 msg << "Failed to allocate buffer for incoming message";
			 XCEPT_DECLARE_NESTED(pt::exception::ReceiveFailure, e, msg.str(), ex);
			 this->getOwnerApplication()->notifyQualified("fatal", e);
			 return;
			 }

			 tcpla::TCPLA_CONTEXT cookie;
			 cookie.as_ptr = reference; // rememeber which reference

			 try
			 {
			 //std::cout << "Posting to receive on endpoint UUID = '" << event.event_data.connect_event_data.ep_handle.toString() << "'" << std::endl;
			 event.event_data.connect_event_data.ep_handle.psp_->safePostRecvBuffer(event.event_data.connect_event_data.ep_handle,cookie, (char *) reference->getDataLocation(), maxBlockSize_);
			 }
			 catch ( tcpla::exception::Exception & e )
			 {
			 std::cout << "FAILED posting to receive on endpoint UUID = '" << event.event_data.connect_event_data.ep_handle.toString() << "'" << std::endl;
			 std::stringstream msg;
			 msg << "Failed to allocate buffer for incoming message";
			 XCEPT_DECLARE_NESTED(pt::exception::ReceiveFailure, ex, msg.str(), e);
			 this->getOwnerApplication()->notifyQualified("fatal", ex);
			 reference->release();
			 return;
			 }
			 }
			 */

		}
			break;

		case tcpla::TCPLA_CONNECTION_EVENT_PEER_REJECTED:
		{
			// Connection failed to establish, the messenger will time out and clear the end point reference. The end point is destroyed here.
			// This could be handled instead like the broken pipe (TCPLA_ASYNC_ERROR_PROVIDER_INTERNAL_ERROR) handling.

			try
			{
				if (!event.event_data.asynch_error_event_data.ep_handle.isNull())
				{
					event.event_data.asynch_error_event_data.ep_handle.ia_->destroyEndPoint(event.event_data.asynch_error_event_data.ep_handle);
				}
			}
			catch (tcpla::exception::IACommandQueueFull & e)
			{
				std::stringstream msg;
				msg << "Failed to destroy endpoint";
				XCEPT_DECLARE_NESTED(pt::exception::Exception, ex, msg.str(), e);
				this->getOwnerApplication()->notifyQualified("fatal", ex);
				return;
			}


			if (event.event_data.asynch_error_event_data.exception != 0)
			{
				LOG4CPLUS_WARN(this->getOwnerApplication()->getApplicationLogger(), xcept::stdformat_exception_history(*(event.event_data.asynch_error_event_data.exception)));

				delete event.event_data.asynch_error_event_data.exception;
			}
		}
			break;

		case tcpla::TCPLA_CONNECTION_EVENT_NON_PEER_REJECTED:
		{
		}
			break;

		case tcpla::TCPLA_CONNECTION_EVENT_ACCEPT_COMPLETION_ERROR:
		{
			if (event.event_data.asynch_error_event_data.exception != 0)
			{
				LOG4CPLUS_WARN(this->getOwnerApplication()->getApplicationLogger(), xcept::stdformat_exception_history(*(event.event_data.asynch_error_event_data.exception)));

				delete event.event_data.asynch_error_event_data.exception;
			}
		}
			break;

		case tcpla::TCPLA_CONNECTION_EVENT_DISCONNECTED: //client
		{
			// sender connected endpoint
			LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), "disconnection event");
		}
			break;

		case tcpla::TCPLA_CONNECTION_CLOSED_BY_PEER: //server only
		{
			mutex_.take();
			this->netFilter_.clear(toolbox::net::UUID(event.event_data.connection_broken_event_data.ep_handle.uuid_));
			mutex_.give();

			if (event.event_data.connection_broken_event_data.exception != 0)
			{
				LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), xcept::stdformat_exception_history(*(event.event_data.connection_broken_event_data.exception)));
				//tolerant behaviour - this event happens when the client explicitly closes the connection through the flags, output disabled to avoid spam
				delete event.event_data.connection_broken_event_data.exception;
			}

			try
			{
				event.event_data.connection_broken_event_data.ia->destroyEndPoint(event.event_data.connection_broken_event_data.ep_handle);
			}
			catch (tcpla::exception::IACommandQueueFull & e)
			{
				std::stringstream msg;
				msg << "Failed to destroy endpoint";
				XCEPT_DECLARE_NESTED(pt::exception::Exception, ex, msg.str(), e);
				this->getOwnerApplication()->notifyQualified("fatal", ex);
				return;

			}
		}
			break;

		case tcpla::TCPLA_CONNECTION_EVENT_BROKEN: // server only
		{
			mutex_.take();
			this->netFilter_.clear(toolbox::net::UUID(event.event_data.connection_broken_event_data.ep_handle.uuid_));
			mutex_.give();

			if (event.event_data.connection_broken_event_data.exception != 0)
			{
				LOG4CPLUS_WARN(this->getOwnerApplication()->getApplicationLogger(), xcept::stdformat_exception_history(*(event.event_data.connection_broken_event_data.exception)));

				delete event.event_data.connection_broken_event_data.exception;
			}

			try
			{
				event.event_data.connection_broken_event_data.ia->destroyEndPoint(event.event_data.connection_broken_event_data.ep_handle);
			}
			catch (tcpla::exception::IACommandQueueFull & e)
			{
				std::stringstream msg;
				msg << "Failed to destroy endpoint";
				XCEPT_DECLARE_NESTED(pt::exception::Exception, ex, msg.str(), e);
				this->getOwnerApplication()->notifyQualified("fatal", ex);
				return;
			}
		}
			break;

		case tcpla::TCPLA_CONNECTION_RESET_BY_PEER: //server only
		{
			mutex_.take();
			this->netFilter_.clear(toolbox::net::UUID(event.event_data.connection_broken_event_data.ep_handle.uuid_));
			mutex_.give();

			if (event.event_data.connection_broken_event_data.exception != 0)
			{
				LOG4CPLUS_WARN(this->getOwnerApplication()->getApplicationLogger(), xcept::stdformat_exception_history(*(event.event_data.connection_broken_event_data.exception)));

				delete event.event_data.connection_broken_event_data.exception;
			}

			try
			{
				event.event_data.connection_broken_event_data.ia->destroyEndPoint(event.event_data.connection_broken_event_data.ep_handle);
			}
			catch (tcpla::exception::IACommandQueueFull & e)
			{
				std::stringstream msg;
				msg << "Failed to destroy endpoint";
				XCEPT_DECLARE_NESTED(pt::exception::Exception, ex, msg.str(), e);
				this->getOwnerApplication()->notifyQualified("fatal", ex);
				return;
			}
		}
			break;

		case tcpla::TCPLA_CONNECTION_EVENT_TIMED_OUT:
		{
		}
			break;

		case tcpla::TCPLA_CONNECTION_EVENT_UNREACHABLE: //client
		{
			//server unreachable in IA

			try
			{
				if (!event.event_data.connect_error_event_data.ep_handle.isNull())
				{
					event.event_data.connect_error_event_data.ep_handle.ia_->destroyEndPoint(event.event_data.connect_error_event_data.ep_handle);
				}
			}
			catch (tcpla::exception::IACommandQueueFull & e)
			{
				std::stringstream msg;
				msg << "Failed to destroy endpoint";
				XCEPT_DECLARE_NESTED(pt::exception::Exception, ex, msg.str(), e);
				this->getOwnerApplication()->notifyQualified("fatal", ex);
				return;
			}


			if (event.event_data.connect_error_event_data.exception != 0)
			{
				std::stringstream ss;
				ss << "Failed to connect as peer was unreachable, exception history = " << xcept::stdformat_exception_history(*(event.event_data.connect_error_event_data.exception));
				LOG4CPLUS_WARN(this->getOwnerApplication()->getApplicationLogger(), ss.str());

				delete event.event_data.connect_error_event_data.exception;
			}

			LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), "TCPLA_CONNECTION_EVENT_UNREACHABLE : Peer unreachable");
		}
			break;

		case tcpla::TCPLA_ASYNC_ERROR_EVD_OVERFLOW:
		{
			if (event.event_data.asynch_error_event_data.exception != 0)
			{
				LOG4CPLUS_FATAL(this->getOwnerApplication()->getApplicationLogger(), xcept::stdformat_exception_history(*(event.event_data.asynch_error_event_data.exception)));

				if (event.event_data.asynch_error_event_data.reason == 13)
				{
					// TCPLA_DTO_RCV_COMPLETION_EVENT

					if (event.event_data.asynch_error_event_data.user_cookie.as_ptr != 0)
					{
						toolbox::mem::Reference * ref = (toolbox::mem::Reference*) event.event_data.asynch_error_event_data.user_cookie.as_ptr;
						ref->release();
					}

					std::stringstream msg;
					msg << "Failed to dispatch received block to user, insufficient resources";
					XCEPT_DECLARE_NESTED(pt::exception::Exception, ex, msg.str(), *(event.event_data.asynch_error_event_data.exception));
					this->getOwnerApplication()->notifyQualified("fatal", ex);
				}

				delete event.event_data.asynch_error_event_data.exception;
			}
			else
			{
				std::stringstream msg;
				msg << "TCPLA_ASYNC_ERROR_EVD_OVERFLOW with no exception, cannot do anything";
				XCEPT_DECLARE(pt::exception::Exception, ex, msg.str());
				this->getOwnerApplication()->notifyQualified("fatal", ex);
			}
		}
			break;

		case tcpla::TCPLA_ASYNC_ERROR_IA_CATASTROPHIC:
		{
			if ( event.event_data.asynch_error_event_data.reason == 2 ) // block too small to receive
			{
				mutex_.take();
                        	this->netFilter_.clear(toolbox::net::UUID(event.event_data.asynch_error_event_data.ep_handle.uuid_));
                        	mutex_.give();

                        	try
                        	{
                                	event.event_data.asynch_error_event_data.ep_handle.ia_->destroyEndPoint(event.event_data.asynch_error_event_data.ep_handle);
                        	}
                        	catch (tcpla::exception::IACommandQueueFull & e)
                        	{
                                	std::stringstream msg;
                                	msg << "Failed to destroy endpoint";
                                	XCEPT_DECLARE_NESTED(pt::exception::Exception, ex, msg.str(), e);
                                	this->getOwnerApplication()->notifyQualified("fatal", ex);
                                	return;
                        	}
			}

			if (event.event_data.asynch_error_event_data.exception != 0)
			{
				LOG4CPLUS_FATAL(this->getOwnerApplication()->getApplicationLogger(), xcept::stdformat_exception_history(*(event.event_data.asynch_error_event_data.exception)));

				delete event.event_data.asynch_error_event_data.exception;
			}
		}
			break;

		case tcpla::TCPLA_ASYNC_ERROR_EP_BROKEN:
		{
			if (event.event_data.asynch_error_event_data.exception != 0)
			{
				LOG4CPLUS_WARN(this->getOwnerApplication()->getApplicationLogger(), xcept::stdformat_exception_history(*(event.event_data.asynch_error_event_data.exception)));

				delete event.event_data.asynch_error_event_data.exception;
			}
		}
			break;

		case tcpla::TCPLA_ASYNC_ERROR_TIMED_OUT:
		{
			LOG4CPLUS_TRACE(this->getOwnerApplication()->getApplicationLogger(), "Received TCPLA_ASYNC_ERROR_TIMED_OUT event");
		}
			break;

		case tcpla::TCPLA_ASYNC_ERROR_PROVIDER_INTERNAL_ERROR:
		{
			//
			//std::cout << "BROKEN PIPE, PANIC : ep_handle = " << event.event_data.asynch_error_event_data.ep_handle.toString() << std::endl;
			//event.event_data.asynch_error_event_data.ep_handle.ia_->destroyEndPoint(event.event_data.asynch_error_event_data.ep_handle);
			// This is (most likely) due to a broken pipe. Research shows that this is un-recoverbale here, and it should propogate up to the user level for handling.
			std::stringstream ss;
			ss << "Event: " << tcpla::eventToString(event.event_number);
			LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), ss.str());

			if (event.event_data.asynch_error_event_data.reason == 2) // failed to accept
			{
				if (event.event_data.asynch_error_event_data.exception != 0)
				{
					LOG4CPLUS_WARN(this->getOwnerApplication()->getApplicationLogger(), xcept::stdformat_exception_history(*(event.event_data.asynch_error_event_data.exception)));

					delete event.event_data.asynch_error_event_data.exception;
				}
			}
			else if (event.event_data.asynch_error_event_data.reason == 3) // failed to send
			{
				if (event.event_data.asynch_error_event_data.handler != 0)
				{
					XCEPT_DECLARE(pt::utcp::exception::Exception, e, "Undeliverable message (broken pipe)");
					event.event_data.asynch_error_event_data.handler->invoke(e, event.event_data.asynch_error_event_data.context);
				}

				if (event.event_data.asynch_error_event_data.user_cookie.as_ptr != 0)
				{
					// message removed from the ( by tcpla)  queue so we cannot delete
					toolbox::mem::Reference * ref = (toolbox::mem::Reference*) event.event_data.asynch_error_event_data.user_cookie.as_ptr;
					ref->release();
				}

				if (event.event_data.asynch_error_event_data.exception != 0)
				{
					LOG4CPLUS_WARN(this->getOwnerApplication()->getApplicationLogger(), xcept::stdformat_exception_history(*(event.event_data.asynch_error_event_data.exception)));

					delete event.event_data.asynch_error_event_data.exception;
				}
			}
			else
			{
				LOG4CPLUS_WARN(this->getOwnerApplication()->getApplicationLogger(), "Unknown async provider internal error reason found, unable to do anything");
			}
		}
			break;

		case tcpla::TCPLA_SOFTWARE_EVENT:
		{
			// the following events are sent here;
			// 1. failed to post buffer to receive

			// at the moment, just throw it as an pt::exception with the event exception attached
			std::stringstream msg;
			msg << "TCPLA software event";
			XCEPT_DECLARE_NESTED(pt::exception::Exception, ex, msg.str(), *(event.event_data.software_event_data.exception));
			this->getOwnerApplication()->notifyQualified("warn", ex);
		}
			break;

		case tcpla::TCPLA_POST_BLOCK_FAILED:
		case tcpla::TCPLA_DTO_SND_UNDELIVERABLE_BLOCK:
		{
			std::stringstream ss;
			ss << "Event: " << tcpla::eventToString(event.event_number);

			LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), ss.str());
			if (event.event_data.tcpla_dto_block_event_data.handler != 0)
			{
				XCEPT_DECLARE(pt::exception::SendFailure, e, "Undeliverable block");
				event.event_data.tcpla_dto_block_event_data.handler->invoke(e, event.event_data.tcpla_dto_block_event_data.context);
			}

			if (event.event_data.tcpla_dto_block_event_data.user_cookie.as_ptr != 0)
			{
				toolbox::mem::Reference * ref = (toolbox::mem::Reference*) event.event_data.tcpla_dto_block_event_data.user_cookie.as_ptr;

				ref->release();
			}
		}
			break;

		case tcpla::TCPLA_DTO_RCV_UNUSED_BLOCK:
		{
			/*pt::utcp::Event event;

			 event.event_number = UTCP_DTO_RCV_UNUSED_BLOCK;
			 event.event_data.utcp_dto_block_event_data.user_cookie = d.cookie;
			 event.event_data.utcp_dto_block_event_data.block_length = d.size;
			 event.event_data.utcp_dto_block_event_data.buffer = d.local_iov;*/

			//LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), "Recovering unused receive block");
			if (event.event_data.tcpla_dto_block_event_data.user_cookie.as_ptr != 0)
			{
				toolbox::mem::Reference * ref = (toolbox::mem::Reference*) event.event_data.tcpla_dto_block_event_data.user_cookie.as_ptr;

				ref->release();
			}
		}
			break;

		default:
		{
			LOG4CPLUS_ERROR(this->getOwnerApplication()->getApplicationLogger(), "Unknown event");
		}
			break;
	}
}

void pt::utcp::B2INEventHandler::gc()
{
	mutex_.take();
	std::list<pt::Messenger::Reference>::iterator mi = messengers_.begin();
	while(mi != messengers_.end())
	{
		pt::Messenger::Reference refcnt = *mi;
		pt::utcp::B2INMessenger * messenger = dynamic_cast<pt::utcp::B2INMessenger*>(&(*refcnt));
		pt::Address::Reference a = messenger->getDestinationAddress();

		if (messenger->fail_)
		{
			LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), "Garbage collection found failed messenger");
			mi = messengers_.erase(mi);
		}
		else
		{
			mi++;
		}
	}
	mutex_.give();
}

std::string pt::utcp::B2INEventHandler::toHTML ()
{
	std::stringstream ss;

	ss << "			<table>" << std::endl;
	ss << "				<tbody>" << std::endl;
	ss << "					<tr>" << std::endl;
	ss << "						<td style=\"vertical-align: top; padding-right: 10px;\">" << std::endl;

	ss << "			<table class=\"xdaq-table\">" << std::endl;
	ss << "				<caption>B2IN Events</caption>";
	ss << "				<thead>" << std::endl;
	ss << "				<tr>" << std::endl;
	ss << "				<th>Name</th>" << std::endl;
	ss << "				<th>Count</th>" << std::endl;
	ss << "				</tr>" << std::endl;
	ss << "				</thead>" << std::endl;

	ss << "				<tbody>" << std::endl;

	for (size_t i = 0; i < tcpla::MAX_TCPLA_EVENT_NUMBER; i++)
	{
		ss << "					<tr>" << std::endl;
		ss << "					<td>" << tcpla::eventToString(i) << "</td>" << std::endl;
		ss << "					<td>" << toolbox::toString("%d", eventCounter_[i]) << "</td>" << std::endl;
		ss << "					</tr>" << std::endl;
	}

	ss << "				</tbody>" << std::endl;
	ss << "			</table>" << std::endl;

	ss << "						</td>" << std::endl;

	if (!messengers_.empty())
	{
		ss << "						<td style=\"vertical-align: top;\">" << std::endl;
		ss << "			<table class=\"xdaq-table\">" << std::endl;
		ss << "				<caption>Messenger Cache</caption>";
		ss << "				<thead>" << std::endl;
		ss << "				<tr>" << std::endl;
		ss << "				<th>Name</th>" << std::endl;
		ss << "				<th>Fail Status</th>" << std::endl;
		ss << "				<th>Connection Status</th>" << std::endl;
		ss << "				</tr>" << std::endl;
		ss << "				</thead>" << std::endl;
		ss << "				<tbody>" << std::endl;

		mutex_.take();
		std::list<pt::Messenger::Reference>::iterator mi;
		for (mi = messengers_.begin(); mi != messengers_.end(); mi++)
		{
			pt::Messenger::Reference refcnt = *mi;
			pt::utcp::B2INMessenger * messenger = dynamic_cast<pt::utcp::B2INMessenger*>(&(*refcnt));
			pt::Address::Reference a = messenger->getDestinationAddress();

			ss << "					<tr>" << std::endl;
			ss << "						<td>" << a->toString() << "</td>" << std::endl;
			if (messenger->fail_)
			{
				ss << "					<td>" << "FAILED" << "</td>" << std::endl;
			}
			else
			{
				ss << "					<td>" << "Alive" << "</td>" << std::endl;
			}
			if (messenger->established_)
			{
				ss << "					<td>" << "Connected" << "</td>" << std::endl;
			}
			else
			{
				ss << "					<td>" << "Not connected" << "</td>" << std::endl;
			}
			ss << "					</tr>" << std::endl;
		}
		mutex_.give();

		ss << "				</tbody>" << std::endl;
		ss << "			</table>" << std::endl;
		ss << "						</td>" << std::endl;
	}

	ss << "					</tr>" << std::endl;
	ss << "				</tbody>" << std::endl;
	ss << "			</table>" << std::endl;

	return ss.str();
}
