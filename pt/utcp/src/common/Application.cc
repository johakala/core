// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2011, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A.Petrucci, C.Wakefield							 *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include "toolbox/string.h"
#include "pt/utcp/Application.h"
#include "i2o/Method.h"
#include "i2o/utils/AddressMap.h"
#include "pt/PeerTransportAgent.h"
#include "xoap/Method.h"

#include "xdaq/NamespaceURI.h"
#include "xoap/MessageReference.h"
#include "xoap/MessageFactory.h"
#include "xoap/SOAPPart.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPBody.h"
#include "xoap/domutils.h"
#include "xcept/tools.h"

#include "toolbox/task/TimerFactory.h"
#include "toolbox/task/WorkLoopFactory.h"

#include "tcpla/InterfaceAdapter.h"
#include "tcpla/PublicServicePoint.h"
#include "tcpla/WaitingWorkLoop.h"
#include "tcpla/Address.h"

#include "pt/utcp/PeerTransport.h"
#include "pt/utcp/Messenger.h"

#include "pt/utcp/I2OEventHandler.h"
#include "pt/utcp/B2INEventHandler.h"
#include "xgi/framework/Method.h"

XDAQ_INSTANTIATOR_IMPL (pt::utcp::Application)

pt::utcp::Application::Application (xdaq::ApplicationStub* stub) throw (xdaq::exception::Exception)
	: xdaq::Application(stub), xgi::framework::UIManager(this)
{
	stub->getDescriptor()->setAttribute("icon", "/pt/utcp/images/pt-utcp-icon.png");
	stub->getDescriptor()->setAttribute("icon16x16", "/pt/utcp/images/pt-utcp-icon.png");
	// Bind CGI callbacks
	xgi::framework::deferredbind(this, this, &pt::utcp::Application::Default, "Default");
	xgi::framework::deferredbind(this, this, &pt::utcp::Application::init, "init");
	xgi::framework::deferredbind(this, this, &pt::utcp::Application::setpipe, "setpipe");

	this->maxClients_ = 1024;
	this->autoConnect_ = true;
	this->ioQueueSize_ = 2048;
	this->eventQueueSize_ = 10000;
	this->maxReceiveBuffers_ = 1024;
	this->maxBlockSize_ = 0x40000; //256kb
	this->maxFD_ = 1024;
	this->protocol_ = UTCP_PROTOCOL;
	//this->committedPoolSize_ = 0x100000 * 100;
	this->committedPoolSize_ = (maxReceiveBuffers_ * maxBlockSize_) * 1.2; // to allow for the pool threshold

	getApplicationInfoSpace()->fireItemAvailable("committedPoolSize", &committedPoolSize_);
	getApplicationInfoSpace()->fireItemAvailable("maxClients", &maxClients_);
	getApplicationInfoSpace()->fireItemAvailable("ioQueueSize", &ioQueueSize_);
	getApplicationInfoSpace()->fireItemAvailable("eventQueueSize", &eventQueueSize_);
	getApplicationInfoSpace()->fireItemAvailable("maxReceiveBuffers", &maxReceiveBuffers_);
	getApplicationInfoSpace()->fireItemAvailable("autoConnect", &autoConnect_);
	getApplicationInfoSpace()->fireItemAvailable("maxBlockSize", &maxBlockSize_);

	getApplicationInfoSpace()->fireItemAvailable("maxFD", &maxFD_);
	getApplicationInfoSpace()->fireItemAvailable("protocol", &protocol_);

	// Bind setting of default parameters
	getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");

	xoap::bind(this, &pt::utcp::Application::fireEvent, "Configure", XDAQ_NS_URI);
	xoap::bind(this, &pt::utcp::Application::fireEvent, "configure", XDAQ_NS_URI);
	xoap::bind(this, &pt::utcp::Application::fireEvent, "Init", XDAQ_NS_URI);
	xoap::bind(this, &pt::utcp::Application::fireEvent, "init", XDAQ_NS_URI);
	xoap::bind(this, &pt::utcp::Application::fireEvent, "enable", XDAQ_NS_URI);
	xoap::bind(this, &pt::utcp::Application::fireEvent, "Enable", XDAQ_NS_URI);
}

xoap::MessageReference pt::utcp::Application::fireEvent (xoap::MessageReference msg) throw (xoap::exception::Exception)
{
	xoap::SOAPPart part = msg->getSOAPPart();
	xoap::SOAPEnvelope env = part.getEnvelope();
	xoap::SOAPBody body = env.getBody();
	DOMNode* node = body.getDOMNode();
	DOMNodeList* bodyList = node->getChildNodes();

	for (unsigned int i = 0; i < bodyList->getLength(); i++)
	{
		DOMNode* command = bodyList->item(i);

		if (command->getNodeType() == DOMNode::ELEMENT_NODE)
		{

			std::string commandName = toolbox::tolower(xoap::XMLCh2String(command->getLocalName()));

			if ((commandName == "enable") || (commandName == "init"))
			{
				this->autoConnect();
			}
			else if (commandName == "configure")
			{
				::sleep(1); //just for Andrea
			}

			xoap::MessageReference reply = xoap::createMessage();
			xoap::SOAPEnvelope envelope = reply->getSOAPPart().getEnvelope();
			xoap::SOAPName responseName = envelope.createName(commandName + "Response", "xdaq", XDAQ_NS_URI);

			(void) envelope.getBody().addBodyElement(responseName);

			return reply;
		}
	}

	XCEPT_RAISE(xcept::Exception, "SOAP command not found");
}

//
// run control requests current paramater values
//
void pt::utcp::Application::actionPerformed (xdata::Event& e)
{

	if (e.type() == "urn:xdaq-event:setDefaultValues")
	{

		//poolName_ = urn.toString();

		toolbox::mem::Pool * pool = 0;
		try
		{
			toolbox::net::URN urn("toolbox-mem-pool-utcp", "utcp");
			toolbox::mem::CommittedHeapAllocator* a = new toolbox::mem::CommittedHeapAllocator(committedPoolSize_);
			pool = toolbox::mem::getMemoryPoolFactory()->createPool(urn, a);

			pool->setHighThreshold((unsigned long) (committedPoolSize_ * 0.9));
			pool->setLowThreshold((unsigned long) (committedPoolSize_ * 0.5));
		}
		catch (toolbox::mem::exception::Exception & e)
		{
			XCEPT_RETHROW(xdaq::exception::Exception, "Cannot create default memory pool", e);
		}

		pt_ = 0;

		try
		{
			pt_ = new pt::utcp::PeerTransport(this, pool, (xdata::UnsignedIntegerT) maxClients_, (xdata::UnsignedIntegerT) maxReceiveBuffers_, (xdata::UnsignedIntegerT) ioQueueSize_, (xdata::UnsignedIntegerT) maxBlockSize_, (xdata::UnsignedIntegerT) eventQueueSize_, protocol_);

		}
		catch (pt::exception::Exception & e)
		{
			this->notifyQualified("fatal", e);
			return;
		}

		pt_->addHandler(new pt::utcp::I2OEventHandler(this, pool, (xdata::UnsignedIntegerT) maxClients_, (xdata::UnsignedIntegerT) maxReceiveBuffers_, (xdata::UnsignedIntegerT) ioQueueSize_, (xdata::UnsignedIntegerT) maxBlockSize_));
		pt_->addHandler(new pt::utcp::B2INEventHandler(this, pool, (xdata::UnsignedIntegerT) maxClients_, (xdata::UnsignedIntegerT) maxReceiveBuffers_, (xdata::UnsignedIntegerT) ioQueueSize_, (xdata::UnsignedIntegerT) maxBlockSize_));

		pt::PeerTransportAgent* pta = pt::getPeerTransportAgent();
		pta->addPeerTransport(pt_);

		//auto connect timer

		toolbox::task::Timer * timer = 0;

		if (!toolbox::task::getTimerFactory()->hasTimer("urn:pt::utcp-timer"))
		{
			timer = toolbox::task::getTimerFactory()->createTimer("urn:pt::utcp-timer");
		}
		else
		{
			timer = toolbox::task::getTimerFactory()->getTimer("urn:pt::utcp-timer");
		}

		if (autoConnect_)
		{
			toolbox::TimeInterval interval;
			interval.fromString("PT5S");

			toolbox::TimeVal start;

			timer->scheduleAtFixedRate(start, this, interval, 0, "urn:utcp-watchdog:autoconnect");
		}

		toolbox::TimeInterval interval;
		interval.fromString("PT5S");

		toolbox::TimeVal start;

		timer->scheduleAtFixedRate(start, this, interval, 0, "urn:utcp-watchdog:gc");
	}
}

void pt::utcp::Application::Default (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	// Begin of tabs
	*out << "<div class=\"xdaq-tab-wrapper\" id=\"tabPane1\">" << std::endl;

	// Tab 1
	*out << "<div class=\"xdaq-tab\" title=\"Output\">" << std::endl;
	this->SenderTabPage(out);
	*out << "</div>";

	// Tab 2
	*out << "<div class=\"xdaq-tab\" title=\"Input\">" << std::endl;
	this->ReceiverTabPage(out);
	*out << "</div>";

	// Tab 3
	*out << "<div class=\"xdaq-tab\" title=\"Settings\">" << std::endl;
	this->SettingsTabPage(out);
	*out << "</div>";

	// Tab 4
	*out << "<div class=\"xdaq-tab\" title=\"Events\">" << std::endl;
	this->EventsTabPage(out);
	*out << "</div>";

	// Tab 5
	*out << "<div class=\"xdaq-tab\" title=\"Workloops\">" << std::endl;
	this->WorkloopsTabPage(out);
	*out << "</div>";

	*out << "</div>"; // end of tab pane
}

void pt::utcp::Application::SenderTabPage (xgi::Output * out)
{
	for (std::map<std::string, tcpla::InterfaceAdapter *>::iterator i = pt_->iav_.begin(); i != pt_->iav_.end(); i++)
	{
		*out << (*(*i).second) << std::endl;
	}
}

void pt::utcp::Application::ReceiverTabPage (xgi::Output * out)
{
	for (size_t i = 0; i < pt_->psp_.size(); i++)
	{
		*out << (*pt_->psp_[i]);
	}
}

void pt::utcp::Application::SettingsTabPage (xgi::Output * out)
{
	*out << cgicc::table().set("class", "xdaq-table-vertical") << std::endl;
	*out << cgicc::tbody() << std::endl;

	*out << cgicc::tr();
	*out << cgicc::th("Memory Pool");
	*out << cgicc::td(committedPoolSize_.toString());
	*out << cgicc::tr() << std::endl;

	*out << cgicc::tr();
	*out << cgicc::th("Maximum Clients Per Address");
	*out << cgicc::td();
	*out << maxClients_.toString();
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	*out << cgicc::tr();
	*out << cgicc::th("I/O Queue");
	*out << cgicc::td();
	*out << ioQueueSize_.toString();
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	*out << cgicc::tr();
	*out << cgicc::th("Event Queue");
	*out << cgicc::td();
	*out << eventQueueSize_.toString();
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	*out << cgicc::tr();
	*out << cgicc::th("Receive Buffers");
	*out << cgicc::td();
	*out << maxReceiveBuffers_.toString();
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	*out << cgicc::tr();
	*out << cgicc::th("Default Receive Buffer Block Size");
	*out << cgicc::td();
	*out << maxBlockSize_.toString();
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	*out << cgicc::tr();
	*out << cgicc::th("Auto Re-Connect");
	*out << cgicc::td();
	*out << ((autoConnect_) ? "true" : "false");
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	/*
	*out << cgicc::tr();
	*out << cgicc::th("Cached Messenger Count");
	*out << cgicc::td();
	mutex_.take();
	*out << ();
	mutex_.give();
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;
	*/

	*out << cgicc::tbody() << std::endl;
	*out << cgicc::table();
}

void pt::utcp::Application::EventsTabPage (xgi::Output * out)
{
	*out << (*pt_) << std::endl;
}

void pt::utcp::Application::WorkloopsTabPage (xgi::Output * out)
{
	std::list<toolbox::task::WorkLoop *> workloops = toolbox::task::getWorkLoopFactory()->getWorkLoops();

	*out << cgicc::table().set("class", "xdaq-table") << std::endl;
	*out << "<caption>pt::utcp workloops</caption>";

	*out << cgicc::thead() << std::endl;
	*out << cgicc::tr();
	*out << cgicc::th("Name");
	*out << cgicc::th("Type");
	*out << cgicc::tr() << std::endl;
	*out << cgicc::thead() << std::endl;

	*out << cgicc::tbody() << std::endl;
	for (std::list<toolbox::task::WorkLoop *>::iterator i = workloops.begin(); i != workloops.end(); i++)
	{
		*out << cgicc::tr();
		*out << cgicc::td((*i)->getName());
		*out << cgicc::td((*i)->getType());
		*out << cgicc::tr();
	}

	*out << cgicc::tbody() << std::endl;
	*out << cgicc::table();
}

void pt::utcp::Application::timeExpired (toolbox::task::TimerEvent& e)
{
	if (e.getTimerTask()->name == "urn:utcp-watchdog:autoconnect")
	{
		this->autoConnect();
	}
	else if (e.getTimerTask()->name == "urn:utcp-watchdog:gc")
	{
		pt_->gc(); // garbage collection for zombie messengers
		//std::cout << "Invoking GC" << std::endl;
	}
}

void pt::utcp::Application::init (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	this->autoConnect();
}

void pt::utcp::Application::autoConnect ()
{
	std::vector<const xdaq::Network*> networks = this->getApplicationContext()->getNetGroup()->getNetworks();
	for (std::vector<const xdaq::Network*>::iterator n = networks.begin(); n != networks.end(); n++)
	{
		if ((*n)->getProtocol() == this->protocol_.toString())
		{
			if ((*n)->isEndpointExisting(this->getApplicationDescriptor()->getContextDescriptor()))
			{
				pt::Address::Reference local = (*n)->getAddress(this->getApplicationDescriptor()->getContextDescriptor());

				std::stringstream ss;
				ss << "Found local network : " << (*n)->getName() << " - " << (*n)->getProtocol() << " - " << local->toString();

				LOG4CPLUS_DEBUG(this->getApplicationLogger(), ss.str());

				std::vector<xdaq::ContextDescriptor*> contexts = this->getApplicationContext()->getContextTable()->getContextDescriptors();

				for (std::vector<xdaq::ContextDescriptor*>::iterator c = contexts.begin(); c != contexts.end(); c++)
				{
					if ((*c)->getURL() != this->getApplicationDescriptor()->getContextDescriptor()->getURL())
					{
						if ((*n)->isEndpointExisting((*c)))
						{
							LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Found Endpoint matching in network " << (*n)->getName() << " for remote context " << (*c)->getURL());

							pt::Address::Reference remote;
							try
							{
								remote = (*n)->getAddress(*c);
							}
							catch (...)
							{
								// there is no matching address for this network
								continue;
							}
							if (!remote->equals(local))
							{
								tcpla::Address & rAddr = dynamic_cast<tcpla::Address&>(*remote);
								std::stringstream ss;
								ss << "Found remote network (cacheing messenger) : " << (*n)->getName() << " - " << (*n)->getProtocol() << " url: " << rAddr.getHost() << ":" << rAddr.getConfigurePort();

								LOG4CPLUS_DEBUG(this->getApplicationLogger(), ss.str());

								pt::Messenger::Reference mr = pt::getPeerTransportAgent()->getMessenger(remote, local);
								pt::utcp::Messenger & m = dynamic_cast<pt::utcp::Messenger&>(*mr);
								try
								{
									m.postConnect();
								}
								catch (pt::utcp::exception::Exception & e)
								{
									LOG4CPLUS_ERROR(this->getApplicationLogger(), "failed to connect cached messenger");
									this->notifyQualified("error", e);
								}
							}
						}
						else
						{
							LOG4CPLUS_DEBUG(this->getApplicationLogger(), "No Endpoint matching in network " << (*n)->getName() << " for remote context " << (*c)->getURL());
						}
					}
				}
			}
		}
	}
}

void pt::utcp::Application::setpipe (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	std::string originator = in->getenv("from");
	std::string destination = in->getenv("to");

	std::set<const xdaq::ApplicationDescriptor*> dlist;
	std::set<const xdaq::ApplicationDescriptor*> olist;

	try
	{
		dlist = getApplicationContext()->getDefaultZone()->getApplicationDescriptors(destination);
		LOG4CPLUS_INFO(getApplicationLogger(), "Found " << dlist.size() << " servers");
	}
	catch (xdaq::exception::Exception& e)
	{
		LOG4CPLUS_FATAL(getApplicationLogger(), xcept::stdformat_exception_history(e));
		XCEPT_RAISE(xgi::exception::Exception, "No Server application instance found. Client cannot be configured.");
	}

	try
	{
		olist = getApplicationContext()->getDefaultZone()->getApplicationDescriptors(this->getApplicationDescriptor()->getContextDescriptor());
		LOG4CPLUS_INFO(getApplicationLogger(), "Found " << olist.size() << " servers");
	}
	catch (xdaq::exception::Exception& e)
	{
		LOG4CPLUS_FATAL(getApplicationLogger(), xcept::stdformat_exception_history(e));
		XCEPT_RAISE(xgi::exception::Exception, "No Server application instance found. Client cannot be configured.");
	}

	for (std::set<const xdaq::ApplicationDescriptor*>::iterator i = olist.begin(); i != olist.end(); i++)
	{
		if ((*i)->getClassName() == originator)
		{
			for (std::set<const xdaq::ApplicationDescriptor*>::iterator j = dlist.begin(); j != dlist.end(); j++)
			{
				try
				{
					std::string networkName = this->getApplicationContext()->getRoutingTable()->getNetworkPath((*i), (*j));

					const xdaq::Network * network = this->getApplicationContext()->getNetGroup()->getNetwork(networkName);

					pt::Address::Reference localAddress = network->getAddress((*i)->getContextDescriptor());

					pt::Address::Reference remoteAddress = network->getAddress((*j)->getContextDescriptor());

					pt::Messenger::Reference mr = pt::getPeerTransportAgent()->getMessenger(remoteAddress, localAddress);

					pt::utcp::Messenger & m = dynamic_cast<pt::utcp::Messenger&>(*mr);
					try
					{
						m.postConnect();
					}
					catch (pt::utcp::exception::Exception & e)
					{
						XCEPT_RETHROW(xgi::exception::Exception, "failed to connect cached messenger", e);
					}
				}
				catch (xdaq::exception::NoRoute & e)
				{
					std::stringstream errorMessage;
					errorMessage << "Canot create messenger from application class '" << (*i)->getClassName();
					errorMessage << "' in context '" << (*i)->getContextDescriptor()->getURL();
					errorMessage << "' to application class '" << (*j)->getClassName();
					errorMessage << "' in context '" << (*j)->getContextDescriptor()->getURL() << "'";

					XCEPT_RETHROW(xgi::exception::Exception, errorMessage.str(), e);
				}
				catch (xdaq::exception::NoNetwork & e)
				{
					std::stringstream errorMessage;
					errorMessage << "Canot create messenger from application class '" << (*i)->getClassName();
					errorMessage << "' in context '" << (*i)->getContextDescriptor()->getURL();
					errorMessage << "' to application class '" << (*j)->getClassName();
					errorMessage << "' in context '" << (*j)->getContextDescriptor()->getURL() << "'";

					XCEPT_RETHROW(xgi::exception::Exception, errorMessage.str(), e);
				}
				catch (xdaq::exception::ApplicationDescriptorNotFound & e)
				{
					std::stringstream errorMessage;
					errorMessage << "Canot create messenger from application class '" << (*i)->getClassName();
					errorMessage << "' in context '" << (*i)->getContextDescriptor()->getURL();
					errorMessage << "' to application class '" << (*j)->getClassName();
					errorMessage << "' in context '" << (*j)->getContextDescriptor()->getURL() << "'";

					XCEPT_RETHROW(xgi::exception::Exception, errorMessage.str(), e);
				}
				catch (xdaq::exception::NoEndpoint & e)
				{
					std::stringstream errorMessage;
					errorMessage << "Canot create messenger from application class '" << (*i)->getClassName();
					errorMessage << "' in context '" << (*i)->getContextDescriptor()->getURL();
					errorMessage << "' to application class '" << (*j)->getClassName();
					errorMessage << "' in context '" << (*j)->getContextDescriptor()->getURL() << "'";

					XCEPT_RETHROW(xgi::exception::Exception, errorMessage.str(), e);
				}
				catch (pt::exception::NoMessenger & e)
				{
					std::stringstream errorMessage;
					errorMessage << "Canot create messenger from application class '" << (*i)->getClassName();
					errorMessage << "' in context '" << (*i)->getContextDescriptor()->getURL();
					errorMessage << "' to application class '" << (*j)->getClassName();
					errorMessage << "' in context '" << (*j)->getContextDescriptor()->getURL() << "'";

					XCEPT_RETHROW(xgi::exception::Exception, errorMessage.str(), e);
				}
			}
		}
	}
}
