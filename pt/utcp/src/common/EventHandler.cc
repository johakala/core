#include "pt/utcp/EventHandler.h"

namespace pt
{
	namespace utcp
	{
		friend std::ostream& operator<< (std::ostream &sout, pt::utcp::EventHandler & eh)
		{
			sout << eh.toHTML();

			return sout;
		}
	}
}
