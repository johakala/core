// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2012, CERN.			                		 *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A.Petrucci, C.Wakefield					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			        	 *
 *************************************************************************/

#include "pt/utcp/B2INMessenger.h"
#include "tcpla/Address.h"

#include "pt/utcp/B2INHeaderInfo.h"

#include "toolbox/mem/Reference.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "xdata/exception/Exception.h"
#include "xdata/exdr/Serializer.h"
#include "xdata/exdr/FixedSizeOutputStreamBuffer.h"

#include "toolbox/utils.h"

#include "tcpla/Event.h"

#include "xcept/tools.h"

#include "b2in/nub/exception/ConnectionRequestTimeout.h"
#include "pt/exception/ReceiveFailure.h"

pt::utcp::B2INMessenger::B2INMessenger (pt::utcp::PeerTransport * pt, tcpla::InterfaceAdapter * ia, pt::Address::Reference local, pt::Address::Reference destination) throw (pt::exception::Exception)
	: pt::utcp::Messenger(pt, ia, local, destination)
{
	this->b2inMTUSize_ = pt->maxBlockSize_;

	tcpla::Address & loc = dynamic_cast<tcpla::Address &>(*local);

	if (loc.hasProperty("recvBlockSize"))
	{
		try
		{
			this->b2inMTUSize_ = toolbox::toUnsignedLong(loc.getProperty("recvBlockSize"));
		}
		catch (toolbox::exception::Exception & ex)
		{
			std::stringstream msg;
			msg << "Failed to parse recvBlockSize in tcpla Address";
			XCEPT_RETHROW(pt::exception::ReceiveFailure, msg.str(), ex);
		}
	}

	this->fail_ = false;

	std::stringstream ss;
	ss << "B2INMessenger created with MTU set to " << this->b2inMTUSize_;
	LOG4CPLUS_DEBUG(pt->getOwnerApplication()->getApplicationLogger(), ss.str());
}

pt::utcp::B2INMessenger::~B2INMessenger ()
{
	this->reset();
}

void pt::utcp::B2INMessenger::send (toolbox::mem::Reference * msg, xdata::Properties & plist, toolbox::exception::HandlerSignature* handler, void* context) throw (b2in::nub::exception::InternalError, b2in::nub::exception::QueueFull, b2in::nub::exception::OverThreshold)
{
	if (msg != 0)
	{
		if (msg->getDataSize() == 0)
		{
			std::stringstream ss;
			ss << "Invalid payload, message has 0 data, b2in properties attached : ";
			for (std::map<std::string, std::string>::iterator i = plist.begin(); i != plist.end(); i++)
			{
				ss << (*i).first << " = " << (*i).second << std::endl;
			}
			XCEPT_RAISE(b2in::nub::exception::InternalError, ss.str());
		}
		if (msg->getNextReference() != 0)
		{
			XCEPT_RAISE(b2in::nub::exception::InternalError, "Invalid payload, message chain is not supported");
		}
	}

	const size_t headerSize = ia_->ghf_();

	if (headerSize > b2inMTUSize_)
	{
		XCEPT_RAISE(b2in::nub::exception::InternalError, "maxBlockSize is too small");
	}

	toolbox::mem::Reference * ref = msg;
	size_t totalSize = 0; // this is the size everything AFTER the B2IN frame holding the plist
	uint16_t totalPackets = 1; //including plist
	while (ref != 0)
	{
		totalSize += ref->getDataSize(); // + headerSize;

		ref = ref->getNextReference();
	}

	if (totalSize > 0) // there is a payload
	{
		if ((size_t)(totalSize % (b2inMTUSize_ - headerSize) == 0))
		{
			totalPackets += (size_t)(totalSize / (b2inMTUSize_ - headerSize)); // if modulo has no remainder, we do not need to add another packet
		}
		else
		{
			totalPackets += 1 + (size_t)(totalSize / (b2inMTUSize_ - headerSize));
		}

		std::stringstream ss;
		ss << "Sending payload, totalSize = " << totalSize << ", totalPackets = " << (size_t) totalPackets;
		LOG4CPLUS_DEBUG(pt_->getOwnerApplication()->getApplicationLogger(), ss.str());
	}

	//std::cout << "Going to send " << (size_t) totalPackets << " packets" << std::endl;

	mutex_.take();

	if (!established_)
	{
		//LOG4CPLUS_INFO(pt_->getOwnerApplication()->getApplicationLogger(), "Messenger not established, going to post connect");
		try
		{
			this->postConnect();
		}
		catch (pt::utcp::exception::Exception & e)
		{
			mutex_.give();

			std::stringstream msg;
			msg << "Failed to post connection request to destination address " << dynamic_cast<tcpla::Address &>(*destination_).toString();

			XCEPT_RETHROW(b2in::nub::exception::InternalError, msg.str(), e);
		}

		size_t retries = 0;
		size_t maxRetries = 150;

		if (plist.hasProperty("urn:b2in-protocol-tcp:connection-timeout"))
		{
			maxRetries = ((size_t) toolbox::toLong(plist.getProperty("urn:b2in-protocol-tcp:connection-timeout"))) * 10;
		}

		while (!established_ && retries++ < maxRetries)
		{
			toolbox::u_sleep(100000);
		}

		if (retries++ >= maxRetries)
		{
			LOG4CPLUS_DEBUG(pt_->getOwnerApplication()->getApplicationLogger(), "connection timeout");
			std::string msg = "Connection request time out";
			this->reset();

			mutex_.give();

			XCEPT_DECLARE(b2in::nub::exception::ConnectionRequestTimeout, e, msg);
			XCEPT_RETHROW(b2in::nub::exception::InternalError, "Connection not established, cannot send", e);
		}
		//LOG4CPLUS_INFO(pt_->getOwnerApplication()->getApplicationLogger(), "Messenger connection established");
	}

	if (!ep_.canPost(totalPackets))
	{
		mutex_.give();
		std::stringstream ss;
		ss << "Insufficient resource, failed to enqueue " << (size_t)(totalPackets + 1) << " packets for index " << ep_.pollfdsKey_ << " for InterfaceAdapter " << ia_->hostname_;

		XCEPT_RAISE(b2in::nub::exception::QueueFull, ss.str());
	}

	xdata::exdr::Serializer serializer;

	if (pt_->pool_ == 0)
	{
		mutex_.give();

		std::string msg = "Cannot access memory pool.";
		XCEPT_RAISE(b2in::nub::exception::InternalError, msg);
	}

	toolbox::mem::Reference * plistRef = 0;
	toolbox::mem::Reference * sendRef = 0;

	try
	{
		for (size_t i = 0; i < totalPackets; i++)
		{
			plistRef = toolbox::mem::getMemoryPoolFactory()->getFrame(pt_->pool_, b2inMTUSize_);
			if (sendRef == 0)
			{
				sendRef = plistRef;
			}
			else
			{
				plistRef->setNextReference(sendRef);
				sendRef = plistRef;
			}
		}
	}
	catch (toolbox::mem::exception::Exception & e)
	{
		mutex_.give();
		if (sendRef != 0)
		{
			sendRef->release();
		}
		XCEPT_RETHROW(b2in::nub::exception::InternalError, "failed to allocate memory for b2in", e);
	}

	if (msg == 0 && sendRef->getNextReference() != 0)
	{
		mutex_.give();
		XCEPT_RAISE(b2in::nub::exception::InternalError, "b2inmessenger internal error, invalid allocated chain for empty payload");
	}

	// insert addressing used for destination and originator
	tcpla::Address & destination = dynamic_cast<tcpla::Address &>(*destination_);
	plist.setProperty("urn:b2in-protocol-tcp:destination", destination.toURL() + ":" + destination.getConfigurePort());
	plist.setProperty("urn:b2in-protocol-tcp:originator", local_->toString());

	//xdata::exdr::FixedSizeOutputStreamBuffer outBuffer((char*) sendRef->getDataLocation() + headerSize, 4096);
	xdata::exdr::FixedSizeOutputStreamBuffer outBuffer((char*) sendRef->getDataLocation() + headerSize, b2inMTUSize_ - headerSize);

	try
	{
		serializer.exportAll(&plist, &outBuffer);
	}
	catch (xdata::exception::Exception & e)
	{
		mutex_.give();

		sendRef->release();
		XCEPT_RETHROW(b2in::nub::exception::InternalError, "could not serialize B2IN properties", e);
	}

	sendRef->setDataSize(outBuffer.tellp() + headerSize); //size of plist + header
	//plistRef->setNextReference(msg);

	std::string connection = plist.getProperty("urn:b2in-protocol-tcp:connection");
	uint8_t flags = 0;
	//std::cout << "using connection " << connection << std::endl;
	if (connection == "keep-alive" || connection == "")
	{
		flags = tcpla::TCPLA_CONNECTION_KEEPALIVE;
	}
	else if (connection == "close")
	{
		flags = tcpla::TCPLA_CONNECTION_CLOSE;
	}
	else
	{
		mutex_.give();

		sendRef->release();

		XCEPT_RAISE(b2in::nub::exception::InternalError, "invalid connection attribute " + connection);
	}

	identification_++; //per chain
	uint16_t packet = 1;

	PB2IN_MESSAGE_FRAME header = (PB2IN_MESSAGE_FRAME) sendRef->getDataLocation();
	header->signature = B2IN_SIGNATURE;
	if (totalSize == 0)
	{
		header->flags = flags;
	}
	else
	{
		header->flags = 0;
	}
	header->identification = identification_;
	header->length = plistRef->getDataSize();
	header->packet = packet++;
	header->total = totalPackets;
	header->version = B2IN_VERSION;
	header->protocol = 0x6202696E;

	if (msg != 0)
	{
		toolbox::mem::Reference * currentBlockRef = sendRef->getNextReference();

		char * payloadBuf = (char*) msg->getDataLocation();
		size_t payloadSize = msg->getDataSize();

		// At the moment, chains of msg's is not supported
		while (currentBlockRef != 0)
		{
			char * buf = (char*) currentBlockRef->getDataLocation();
			PB2IN_MESSAGE_FRAME h = (PB2IN_MESSAGE_FRAME) buf;
			h->signature = B2IN_SIGNATURE;
			h->flags = 0;
			h->identification = identification_;
			h->version = B2IN_VERSION;
			h->protocol = 0x6202696E;
			h->packet = packet++;
			h->total = totalPackets;

			buf += headerSize;

			size_t copySize = payloadSize;
			if (payloadSize > (b2inMTUSize_ - headerSize))
			{
				copySize = b2inMTUSize_ - headerSize;
			}

			size_t size = copySize + headerSize;
			h->length = size;

			::memcpy(buf, payloadBuf, copySize);

			payloadSize -= copySize;
			payloadBuf += copySize;
			if (currentBlockRef->getNextReference() == 0)
			{
				h->flags = flags; //last packet sends flags
			}

			currentBlockRef->setDataSize(size);
			currentBlockRef = currentBlockRef->getNextReference();

			//std::cout << "Preparing packet #" << (size_t) h->packet << ", copysize = " << copySize << ", headersize = " << headerSize << " packetlen = " << size << std::endl;
		}
	}

	toolbox::mem::Reference * currentRef = 0;

	//int testcounter = 1;
	while (sendRef != 0)
	{
		try
		{
			currentRef = sendRef;
			sendRef = sendRef->getNextReference();

			struct iovec postv[tcpla::TCPLA_MAX_IOV_SIZE];
			tcpla::TCPLA_CONTEXT cookie;
			cookie.as_ptr = currentRef; // the ref that will be released once the block has been sent
			currentRef->setNextReference(0); // break the chain so that only that mem block is released

			postv[0].iov_base = currentRef->getDataLocation();
			postv[0].iov_len = currentRef->getDataSize();

			size_t iovs = 1; // size of postv

			//std::cout << "Send packet #" << testcounter++ << ", flags = " << (size_t)flags << std::endl;

			uint8_t lflags = 0;
			if (sendRef != 0)
			{
				lflags = tcpla::TCPLA_CONNECTION_KEEPALIVE;
			}
			else
			{
				lflags = flags;
			}

			ia_->postSendBuffer(ep_, cookie, postv, iovs, handler, context, 0, lflags);

		}
		catch (tcpla::exception::FailedToPostSend & e)
		{
			if (sendRef != 0)
			{
				sendRef->release();
			}
			currentRef->release();

			this->reset();

			mutex_.give();

			std::stringstream ss;
			ss << "Failed to post message send to destination address " << dynamic_cast<tcpla::Address &>(*destination_).toString();

			XCEPT_RETHROW(b2in::nub::exception::QueueFull, ss.str(), e);
		}
		catch (tcpla::exception::InvalidEndPoint & e)
		{
			if (sendRef != 0)
			{
				sendRef->release();
			}
			currentRef->release();

			this->reset();

			mutex_.give();

			std::stringstream ss;
			ss << "Invalid EndPoint. Failed to post message send to destination address " << dynamic_cast<tcpla::Address &>(*destination_).toString();

			XCEPT_RETHROW(b2in::nub::exception::InternalError, ss.str(), e);
		}
		catch (tcpla::exception::EndPointNotConnected & e)
		{
			if (sendRef != 0)
			{
				sendRef->release();
			}
			currentRef->release();

			this->reset();

			mutex_.give();

			std::stringstream ss;
			ss << "EndPoint not connected. Failed to post message send to destination address " << dynamic_cast<tcpla::Address &>(*destination_).toString();

			XCEPT_RETHROW(b2in::nub::exception::InternalError, ss.str(), e);
		}
	}

	if (flags & tcpla::TCPLA_CONNECTION_CLOSE)
	{
		ep_.clear(); //recreated as soon for a subsequent send
		established_ = false;
	}

	mutex_.give();

	//std::cout << "Releasing USER BUFFER ----------------" << std::endl;
	if (msg != 0)
	{
		msg->release();
	}
}

void pt::utcp::B2INMessenger::invalidate ()
{
	fail_ = true;
}

void pt::utcp::B2INMessenger::reset ()
{
	LOG4CPLUS_DEBUG(pt_->getOwnerApplication()->getApplicationLogger(), "Reset called for messenger with ep : " << ep_.toString());
	mutex_.take();

	try
	{
		if (!ep_.isNull())
		{
			LOG4CPLUS_DEBUG(pt_->getOwnerApplication()->getApplicationLogger(), "Reset destroying ep : " << ep_.toString());
			ia_->destroyEndPoint(ep_);
		}
	}
	catch (tcpla::exception::InvalidEndPoint & e)
	{
		// this is expected behaviour, so we are not normally notifiying the user.
		LOG4CPLUS_TRACE(pt_->getOwnerApplication()->getApplicationLogger(), "Messenger reset holds invalid endpoint");
	}
	catch (tcpla::exception::IACommandQueueFull & e)
	{
		LOG4CPLUS_FATAL(pt_->getOwnerApplication()->getApplicationLogger(), "Failed to reset in Messenger, could not post to destroy EndPoint : exception trace = " << xcept::stdformat_exception_history(e));
	}

	ep_.clear();
	established_ = false;

	mutex_.give();
}

pt::Address::Reference pt::utcp::B2INMessenger::getLocalAddress ()
{
	return local_;
}

pt::Address::Reference pt::utcp::B2INMessenger::getDestinationAddress ()
{
	return destination_;
}
