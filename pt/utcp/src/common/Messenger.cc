// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2012, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A.Petrucci, C.Wakefield				 	 *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include "pt/utcp/Messenger.h"
#include "pt/utcp/PeerTransport.h"
#include "tcpla/Address.h"

#include "xcept/tools.h"

pt::utcp::Messenger::Messenger (pt::utcp::PeerTransport * pt, tcpla::InterfaceAdapter * ia, pt::Address::Reference local, pt::Address::Reference destination)
	: pt_(pt), destination_(destination), local_(local), identification_(0), mutex_(toolbox::BSem::FULL, true), ia_(ia)
{
	this->established_ = false;
	ep_.clear();
}

pt::utcp::Messenger::~Messenger ()
{
	//this->reset();
}

void pt::utcp::Messenger::postConnect () throw (pt::utcp::exception::Exception)
{
	mutex_.take();
	if (ep_.isNull())
	{
		ep_ = ia_->createEndPoint();
		tcpla::Address & destination = dynamic_cast<tcpla::Address &>(*destination_);
		tcpla::Address & local = dynamic_cast<tcpla::Address &>(*local_);

		tcpla::TCPLA_CONTEXT cookie;

		cookie.as_ptr = 0; //connection cookie

		LOG4CPLUS_DEBUG(pt_->getOwnerApplication()->getApplicationLogger(), "Going to post connect messenger local: " << local.toString() << " remote: " << destination.getProtocol() << "://" << destination.getHost() << ":" << destination.getConfigurePort());
		established_ = false;

		try
		{
			ia_->connect(ep_, &local, &destination, cookie); // asynchronous call, if EP object failed to connect it returns in the corresponding event
		}
		catch (tcpla::exception::IACommandQueueFull & e)
		{
			std::stringstream ss;
			ss << "Failed to post to connect EndPoint in Messenger for local address " << local.toString() << " to destination address " << destination.toString();
			mutex_.give();

			XCEPT_RETHROW(pt::utcp::exception::Exception, ss.str(), e);
		}
		catch (tcpla::exception::InvalidFamily & e)
		{
			std::stringstream ss;
			ss << "Failed to post to connect EndPoint in Messenger for local address " << local.toString() << " to destination address " << destination.toString();
			mutex_.give();

			XCEPT_RETHROW(pt::utcp::exception::Exception, ss.str(), e);
		}
		catch (tcpla::exception::HostnameResolveFailed & e)
		{
			std::stringstream ss;
			ss << "Failed to post to connect EndPoint in Messenger for local address " << local.toString() << " to destination address " << destination.toString();
			mutex_.give();

			XCEPT_RETHROW(pt::utcp::exception::Exception, ss.str(), e);
		}

		mutex_.give();

		return;
	}

	mutex_.give();
}

/*
void pt::utcp::Messenger::reset ()
{
	std::cout << "superclass implementation called" << std::endl;
	mutex_.take();

	//std::cout << "Messenger reset: " << ep_.toString() << " - " << ep_.isNull() << std::endl;
	if (!ep_.isNull())// && established_ == true)
	{
		//std::cout << "Messenger reset destroying endpoint, pollsdfkey = " << ep_.pollfdsKey_ << std::endl;
		try
		{
			ia_->destroyEndPoint(ep_);
		}
		catch (tcpla::exception::IACommandQueueFull & e)
		{
			mutex_.give();

			LOG4CPLUS_FATAL(pt_->getOwnerApplication()->getApplicationLogger(), "Failed to reset in Messenger, could not post to destroy EndPoint : exception trace = " << xcept::stdformat_exception_history(e));
		}
	}

	ep_.clear();
	established_ = false;
	mutex_.give();
}
*/

bool pt::utcp::Messenger::match (tcpla::EndPoint & ep)
{
	//pointer comparison
	return ep == ep_;
}

void pt::utcp::Messenger::connectionEstablished (tcpla::EndPoint & ep)
{
	//mutex_.take();
	ep_ = ep;
	established_ = true;
	//mutex_.give();

}
