// $Id: CommittedHeapBuffer.cc,v 1.5 2008/07/18 15:27:40 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A.Petrucci, C.Wakefield					 *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include "pt/utcp/PeerTransport.h"
#include "tcpla/PublicServicePoint.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "pt/exception/ReceiveFailure.h"
#include "i2o/i2o.h"
#include "xcept/tools.h"
#include "tcpla/InterfaceAdapterPoll.h"
#include "tcpla/InterfaceAdapterSelect.h"

#include "tcpla/Utils.h"

#include "pt/utcp/B2INHeaderInfo.h"
#include "pt/utcp/I2OHeaderInfo.h"

#include "toolbox/hexdump.h"

pt::utcp::PeerTransport::PeerTransport (xdaq::Application * owner, toolbox::mem::Pool * pool, size_t maxClients, size_t maxReceiveBuffers, size_t ioQueueSize, size_t maxBlockSize, size_t eventQueueSize, const std::string & protocol)
	: xdaq::Object(owner), pool_(pool), mutex_(toolbox::BSem::FULL)
{
	this->maxReceiveBuffers_ = maxReceiveBuffers;
	this->ioQueueSize_ = ioQueueSize;
	this->eventQueueSize_ = eventQueueSize;
	this->maxClients_ = maxClients;
	this->maxBlockSize_ = maxBlockSize;
	this->protocol_ = protocol;

	/*this->handlers_["i2o"] = new pt::utcp::I2OEventHandler(owner, pool, maxClients, maxReceiveBuffers, ioQueueSize, maxBlockSize);
	 this->handlers_["b2in"] = new pt::utcp::B2INEventHandler(owner, pool, maxClients, maxReceiveBuffers, ioQueueSize, maxBlockSize);
	 this->handlers_["frl"] = new pt::utcp::FRLEventHandler(owner, pool, maxClients, maxReceiveBuffers, ioQueueSize, maxBlockSize, memPools);*/
}

pt::utcp::PeerTransport::~PeerTransport ()
{
	for (std::map<std::string, tcpla::InterfaceAdapter *>::iterator i = iav_.begin(); i != iav_.end(); i++)
	{
		delete (*i).second;
		iav_.erase(i);
	}

	for (std::map<std::string, pt::utcp::EventHandler *>::iterator i = handlers_.begin(); i != handlers_.end(); i++)
	{
		delete (*i).second;
		handlers_.erase(i);
	}
}

void pt::utcp::PeerTransport::addHandler (pt::utcp::EventHandler * handler)
{
	this->handlers_[handler->getService()] = handler;
}

pt::Messenger::Reference pt::utcp::PeerTransport::getMessenger (pt::Address::Reference destination, pt::Address::Reference local) throw (pt::exception::UnknownProtocolOrService)
{
	if (destination->getProtocol() != this->protocol_ || (local->getProtocol() != this->protocol_))
	{
		std::stringstream ss;
		ss << "Cannot handle protocol service combination, destination protocol was :" << destination->getProtocol() << " destination service was:" << destination->getService() << " while local protocol was:" << local->getProtocol() << "and local service was:" << local->getService();

		XCEPT_RAISE(pt::exception::UnknownProtocolOrService, ss.str());
	}

	// Look if a messenger from the local to the remote destination exists and return it.
	// If it doesn't exist, create it and return it.
	// It accept the service to be null, in this case, it assume that it is soap service. Of course this is not secure but it provide a
	// useful omission as default
	if (((destination->getService() == "i2o") && (local->getService() == "i2o")) || ((destination->getService() == "b2in") && (local->getService() == "b2in")))

	{

		// Create new messenger
		if (local->equals(destination))
		{
			// Creating a local messenger (within same logical host == executive) should not be supported.
			// In this case another transport protocol should be used.
			//
			// http::SOAPLoopbackMessenger* m = new http::SOAPLoopbackMessenger(destination, local);
			// soapMessengers_.push_back(m);
			// return m;
			std::stringstream msg;
			msg << "UTCP protocol communication within the same XDAQ process is not supported. Use the fifo transport for local communication.";
			XCEPT_RAISE(pt::exception::UnknownProtocolOrService, msg.str());
		}
		else
		{

			tcpla::InterfaceAdapter * ia = 0;

			try
			{
				ia = this->getInterfaceAdapter(local, handlers_[destination->getService()]);
			}
			catch (pt::utcp::exception::Exception & e)
			{
				XCEPT_RETHROW(pt::exception::UnknownProtocolOrService, "Failed to get interface adapter", e);
			}
			// create remote messenger

			return handlers_[destination->getService()]->getMessenger(destination, local, ia, this);
		}
	}
	else
	{
		std::stringstream ss;
		ss << "Cannot handle protocol service combination, destination protocol was :" << destination->getProtocol() << " destination service was:" << destination->getService() << " while local protocol was:" << local->getProtocol() << "and local service was:" << local->getService();

		XCEPT_RAISE(pt::exception::UnknownProtocolOrService, ss.str());
	}

}

tcpla::InterfaceAdapter * pt::utcp::PeerTransport::getInterfaceAdapter (pt::Address::Reference address, pt::utcp::EventHandler * eh) throw (pt::utcp::exception::Exception)
{
	tcpla::Address & a = dynamic_cast<tcpla::Address &>(*address);
	tcpla::InterfaceAdapter * ia = 0;

	std::string id = a.getProperty("hostname");
	std::string service = a.getProperty("service");
	std::string smode = a.getProperty("smode");

	if (iav_.find(id) != iav_.end())
	{
		LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), "retrieving cached interface adapter for host " << id);
		ia = iav_[id];
	}
	else
	{
		try
		{
			if (service == "i2o")
			{
				if (smode == "poll")
				{
					LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), "creating interface adapter for host " << id);
					ia = new tcpla::InterfaceAdapterPoll(this->getOwnerApplication(), address, (tcpla::EventHandler*) eh, maxClients_, ioQueueSize_, eventQueueSize_, &pt::utcp::I2OHeaderInfo::getHeaderSize, &pt::utcp::I2OHeaderInfo::getLength); //new listening socket on config
				}
				else if (smode == "select")
				{
					ia = new tcpla::InterfaceAdapterSelect(this->getOwnerApplication(), address, eh, maxClients_, ioQueueSize_, eventQueueSize_, &pt::utcp::I2OHeaderInfo::getHeaderSize, &pt::utcp::I2OHeaderInfo::getLength); //new listening socket on config
				}
				else
				{
					XCEPT_RAISE(pt::utcp::exception::Exception, "invalid mode");
				}
			}
			else if (service == "b2in")
			{
				if (smode == "poll")
				{
					ia = new tcpla::InterfaceAdapterPoll(this->getOwnerApplication(), address, eh, maxClients_, ioQueueSize_, eventQueueSize_, &pt::utcp::B2INHeaderInfo::getHeaderSize, &pt::utcp::B2INHeaderInfo::getLength); //new listening socket on config
				}
				else if (smode == "select")
				{
					ia = new tcpla::InterfaceAdapterSelect(this->getOwnerApplication(), address, eh, maxClients_, ioQueueSize_, eventQueueSize_, &pt::utcp::B2INHeaderInfo::getHeaderSize, &pt::utcp::B2INHeaderInfo::getLength); //new listening socket on config
				}
				else
				{
					XCEPT_RAISE(pt::utcp::exception::Exception, "invalid mode");
				}
			}
		}
		catch (tcpla::exception::InvalidSndTimeOut & e)
		{
			XCEPT_RETHROW(pt::utcp::exception::Exception, "Bad configuration, failed to create interface adapter", e);
		}
		catch (tcpla::exception::Exception & e)
		{
			XCEPT_RETHROW(pt::utcp::exception::Exception, "Failed to create interface adapter", e);
		}

		iav_[id] = ia;
	}

	return ia;
}

pt::TransportType pt::utcp::PeerTransport::getType ()
{
	return pt::SenderReceiver;
}

pt::Address::Reference pt::utcp::PeerTransport::createAddress (const std::string& url, const std::string& service) throw (pt::exception::InvalidAddress)
{
	//std::cout << "pt::utcp::PeerTransport::createAddress URL:" << url << " Service: " << service << std::endl;
	std::map<std::string, std::string, std::less<std::string> > plist;

	toolbox::net::URL urltmp(url);

	plist["protocol"] = urltmp.getProtocol();
	plist["service"] = service;
	plist["hostname"] = urltmp.getHost();
	plist["port"] = toolbox::toString("%d", urltmp.getPort());

	/*
	for (std::map<std::string, std::string>::iterator i = plist.begin(); i != plist.end(); i++)
	{
		std::cout << "utcp address " << i->first << ": " << i->second << std::endl;
	}
	*/

	return this->createAddress(plist);
}

pt::Address::Reference pt::utcp::PeerTransport::createAddress (std::map<std::string, std::string, std::less<std::string> >& address) throw (pt::exception::InvalidAddress)
{

	std::string protocol = address["protocol"];

	if (protocol == this->protocol_)
	{
		//port scanning here

		return pt::Address::Reference(new tcpla::Address(address));
	}
	else
	{
		std::string msg = "Cannot create address, protocol not supported: ";
		msg += protocol;
		XCEPT_RAISE(pt::exception::InvalidAddress, msg);
	}
}

std::string pt::utcp::PeerTransport::getProtocol ()
{
	return this->protocol_;
}

void pt::utcp::PeerTransport::config (pt::Address::Reference address) throw (pt::exception::Exception)
{
	// just tell interface adapter we are list
	tcpla::PublicServicePoint* psp = 0;
	try
	{
		mutex_.take();

		tcpla::Address & a = dynamic_cast<tcpla::Address &>(*address);
		tcpla::InterfaceAdapter * ia = this->getInterfaceAdapter(address, handlers_[a.getService()]);

		psp = ia->listen(address);
		psp_.push_back(psp);

		mutex_.give();

	}
	catch (pt::utcp::exception::Exception &e)
	{
		mutex_.give();
		XCEPT_RETHROW(pt::exception::Exception, "Failed to configure receiver port in IA", e);

	}
}

std::vector<std::string> pt::utcp::PeerTransport::getSupportedServices ()
{

	std::vector < std::string > s;
	for (std::map<std::string, pt::utcp::EventHandler *>::iterator i = handlers_.begin(); i != handlers_.end(); i++)
	{
		s.push_back((*i).first);
	}

	return s;
}

bool pt::utcp::PeerTransport::isServiceSupported (const std::string& service)
{

	for (std::map<std::string, pt::utcp::EventHandler *>::iterator i = handlers_.begin(); i != handlers_.end(); i++)
	{
		if ((*i).first == service)
		{
			return true;
		}
	}

	return false;
}

void pt::utcp::PeerTransport::addServiceListener (pt::Listener* listener) throw (pt::exception::Exception)
{

	pt::PeerTransportReceiver::addServiceListener(listener);

	for (std::map<std::string, pt::utcp::EventHandler *>::iterator i = handlers_.begin(); i != handlers_.end(); i++)
	{
		if ((*i).first == listener->getService())
		{
			handlers_[(*i).first]->addServiceListener(listener);
			return;
		}
	}

	XCEPT_RAISE(pt::exception::Exception, "Failed to add listener of unknown type");
}

void pt::utcp::PeerTransport::removeServiceListener (pt::Listener* listener) throw (pt::exception::Exception)
{
	pt::PeerTransportReceiver::removeServiceListener(listener);

	for (std::map<std::string, pt::utcp::EventHandler *>::iterator i = handlers_.begin(); i != handlers_.end(); i++)
	{
		if ((*i).first == listener->getService())
		{
			handlers_[(*i).first]->removeServiceListener(listener);
			return;
		}
	}

	XCEPT_RAISE(pt::exception::Exception, "Failed to remove listener of unknown type");
}

void pt::utcp::PeerTransport::removeAllServiceListeners ()
{
	pt::PeerTransportReceiver::removeAllServiceListeners();

	for (std::map<std::string, pt::utcp::EventHandler *>::iterator i = handlers_.begin(); i != handlers_.end(); i++)
	{
		handlers_[(*i).first]->removeAllServiceListeners();
	}
}

void pt::utcp::PeerTransport::gc ()
{
	for (std::map<std::string, pt::utcp::EventHandler *>::iterator i = handlers_.begin(); i != handlers_.end(); i++)
		{
			handlers_[(*i).first]->gc();
		}
}

/*void pt::utcp::PeerTransport::handleError(xcept::Exception & e)
 {
 std::cerr << "Asynchronous error " << xcept::stdformat_exception_history(e) << std::endl;
 }*/

// this callback can be invoked by several threads

namespace pt
{
	namespace utcp
	{
		std::ostream& operator<< (std::ostream &sout, pt::utcp::PeerTransport & pt)
		{
			pt.mutex_.take();

			sout << "			<table>" << std::endl;
			sout << "				<tbody>" << std::endl;
			sout << "					<tr>" << std::endl;

			for (std::map<std::string, pt::utcp::EventHandler *>::iterator i = pt.handlers_.begin(); i != pt.handlers_.end(); i++)
			{
				sout << "						<td style=\"vertical-align: top; padding-right: 10px;\">" << std::endl;
				sout << (*(*i).second);
				sout << "						</td>" << std::endl;
			}

			sout << "					</tr>" << std::endl;
			sout << "				</tbody>" << std::endl;
			sout << "			</table>" << std::endl;

			pt.mutex_.give();

			return sout;
		}
	}
}
