// $Id: Netfilter.cc,v 1.5 2008/07/18 15:27:20 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "pt/utcp/Netfilter.h"
#include "toolbox/hexdump.h"

toolbox::mem::Reference * pt::utcp::Netfilter::append (toolbox::mem::Reference* msg, uint16_t packet, uint16_t total, std::pair<uint16_t, toolbox::net::UUID> & key, size_t &totalSize) throw (pt::utcp::exception::Exception)
{

	// For now assume that with utcp all chain elements from a "key" arrive in
	// sequence, e.g. 1/3, 2/3, 3/3

	// Trivial case: packet == total == 1 -> no chain

	if (total == 1)
	{
		if (packet == 1)
		{
			//toolbox::hexdump ((void*) msg->getDataLocation(),  msg->getDataSize());

			return msg;
		}
		else
		{
			std::stringstream msg;
			msg << "Packet (" << packet << ") and total packets (" << total << ")  do not match in single frame message";
			XCEPT_RAISE(pt::utcp::exception::Exception, msg.str());
		}
	}

	std::map<std::pair<uint16_t, toolbox::net::UUID>, ChainDescriptor>::iterator i = chains_.find(key);
	if (i == chains_.end())
	{
		// Start a new chain
		if (packet == 1)
		{

			ChainDescriptor descriptor;
			descriptor.last = msg;
			descriptor.first = msg;
			descriptor.total = total;
			descriptor.packet = 1;
			descriptor.identification = key.first;
			descriptor.totalSize = 0;
			chains_[key] = descriptor;

			totalSize = descriptor.totalSize;

			return 0; // chain not completed
		}
		else
		{
			std::stringstream msg;
			msg << "Netfilter : New chain: current packet number " << (size_t) packet << ", total expected " << (size_t) total;
			msg << ", sequence number: " << (size_t) key.first;

			XCEPT_RAISE(pt::utcp::exception::Exception, msg.str());
		}
	}
	else
	{
		toolbox::mem::Reference* last = (*i).second.last;
		if (packet == ((*i).second.packet + 1))
		{
			last->setNextReference(msg); // Chain the incoming reference to the existing one
			(*i).second.last = msg;
			(*i).second.totalSize += msg->getDataSize();

			if (packet == ((*i).second.total))
			{
				toolbox::mem::Reference* reference = (*i).second.first;

				totalSize = (*i).second.totalSize;
				// Chain has been completely build: erase from internal map and return it
				chains_.erase(i);
				return reference;
			}
			else
			{
				// Chain not yet completed
				(*i).second.packet++;
				totalSize = (*i).second.totalSize;
				return 0;
			}
		}
		else
		{
			std::stringstream msg;
			msg << "Netfilter : Current packet number " << (size_t) packet << ", total expected " << (size_t) total;
			msg << ", last received: " << (size_t)((*i).second.packet);
			msg << ", sequence number: " << (size_t) key.first;
			msg << ", expected sequence number: " << (size_t)((*i).second.identification);

			std::cout << msg.str() << std::endl;
			toolbox::mem::Reference* ref = (*i).second.first;
			toolbox::hexdump((void*) ref->getDataLocation(), ref->getDataSize());
			std::cout << std::endl;

			XCEPT_RAISE(pt::utcp::exception::Exception, msg.str());
		}
	}
}

size_t pt::utcp::Netfilter::clear (toolbox::net::UUID val)
{
	std::list < std::pair<uint16_t, toolbox::net::UUID> > eraseList;
	size_t erased = 0;

	for (std::map<std::pair<uint16_t, toolbox::net::UUID>, ChainDescriptor>::iterator i = chains_.begin(); i != chains_.end(); i++)
	{
		if ((*i).first.second == val)
		{
			eraseList.push_back((*i).first);
		}
	}

	for (std::list<std::pair<uint16_t, toolbox::net::UUID> >::iterator j = eraseList.begin(); j != eraseList.end(); j++)
	{
		chains_[*j].first->release();
		chains_.erase(*j);
		++erased;
	}
	return erased;
}
