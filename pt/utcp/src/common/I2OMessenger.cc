// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2012, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A.Petrucci, C.Wakefield				 	 *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include "pt/utcp/I2OMessenger.h"
#include "pt/utcp/PeerTransport.h"
#include "tcpla/Address.h"

#include "toolbox/hexdump.h"

#include "toolbox/utils.h"

#include "i2o/i2o.h"

#include "xcept/tools.h"

pt::utcp::I2OMessenger::I2OMessenger (pt::utcp::PeerTransport * pt, tcpla::InterfaceAdapter * ia, pt::Address::Reference local, pt::Address::Reference destination)
	: pt::utcp::Messenger(pt, ia, local, destination)
{
	tcpla::Address & l = dynamic_cast<tcpla::Address &>(*local);

	connectOnRequest_ = ((l.getProperty("connectOnRequest") == "true") ? true : false);
	const size_t NumberOfRetries = 500;
	numberOfRetries_ = NumberOfRetries;
	if ( l.hasProperty("numberOfRetries") )
	{
		numberOfRetries_ = toolbox::toLong(l.getProperty("numberOfRetries"));
	}

}

pt::utcp::I2OMessenger::~I2OMessenger ()
{
	this->reset();
}

void pt::utcp::I2OMessenger::send (toolbox::mem::Reference * reference, toolbox::exception::HandlerSignature * handler, void * context) throw (pt::exception::Exception)
{
	mutex_.take();

	tcpla::Address & destination = dynamic_cast<tcpla::Address &>(*destination_);
	tcpla::Address & local = dynamic_cast<tcpla::Address &>(*local_);

	if (!established_ || ep_.isNull())
	{
		if (connectOnRequest_)
		{
			try
			{
				this->postConnect();
			}
			catch (pt::utcp::exception::Exception & e)
			{
				mutex_.give();

				std::stringstream msg;
				msg << "Failed to post message send to destination address " << dynamic_cast<tcpla::Address &>(*destination_).toString();
				XCEPT_RETHROW(pt::exception::Exception, msg.str(), e);
			}

			size_t retries = 0;


			while (!established_ && retries++ < numberOfRetries_)
			{
				toolbox::u_sleep(100000);
			}

			if (retries++ >= numberOfRetries_)
			{
				this->reset();
				mutex_.give();

				std::string msg = "Connection request time out.";
				XCEPT_RAISE(pt::exception::Exception, msg);
			}
		}
		else
		{

			mutex_.give();

			std::stringstream ss;
			ss << "Cannot post send, connection not established for local address " << local.toString() << " to destination address " << destination.getProperty("protocol") << "://" << destination.getProperty("hostname") << ":" << destination.getProperty("port");
			XCEPT_DECLARE(tcpla::exception::ConnectionNotEstablished, e, ss.str());
			XCEPT_RETHROW(pt::exception::Exception, ss.str(), e);
		}
	}

	toolbox::mem::Reference * ref = reference;

	while (ref != 0)
	{

		size_t size = ref->getDataSize();
		PI2O_MESSAGE_FRAME frame = (PI2O_MESSAGE_FRAME) ref->getDataLocation();

		if (size == (size_t)(frame->MessageSize << 2))
		{
			tcpla::TCPLA_CONTEXT cookie;

			if (ref->getNextReference() == 0)
			{
				cookie.as_ptr = reference;
			}
			else
			{
				cookie.as_ptr = 0; // remember which reference
			}

			try
			{
				ia_->postSendBuffer(ep_, cookie, (char*) frame, size, 0, 0, identification_++, tcpla::TCPLA_CONNECTION_KEEPALIVE);

				//toolbox::hexdump ((void*) frame, 256);
			}
			catch (tcpla::exception::FailedToPostSend & e)
			{
				mutex_.give();

				std::stringstream ss;
				ss << "Failed to post message to send to destination address " << destination.toString();

				XCEPT_RETHROW(tcpla::exception::FailedToPostSend, ss.str(), e);
			}
			catch (tcpla::exception::InvalidEndPoint & e)
			{
				mutex_.give();

				std::stringstream ss;
				ss << "Failed to post message to send to destination address " << destination.toString();

				XCEPT_RETHROW(tcpla::exception::FailedToPostSend, ss.str(), e);
			}
			catch (tcpla::exception::EndPointNotConnected & e)
			{
				mutex_.give();

				std::stringstream ss;
				ss << "Failed to post message to send to destination address " << destination.toString();

				XCEPT_RETHROW(tcpla::exception::FailedToPostSend, ss.str(), e);
			}
		}
		else
		{
			std::stringstream ss;
			ss << "Message frame size " << (frame->MessageSize << 2) << " does not match reference data size " << size << ", cannot post to destination address " << destination.toString();
			reference->release();
			mutex_.give();
			XCEPT_RAISE(tcpla::exception::CorruptMessageFrame, ss.str());
		}

		ref = ref->getNextReference();
	}

	mutex_.give();
}

void pt::utcp::I2OMessenger::reset ()
{
	//std::cout << "calling virtual reset" << std::endl;
	//((pt::utcp::Messenger*)this)->reset();
	//dynamic_cast<pt::utcp::Messenger*>(this)->reset();

	mutex_.take();

	try
	{
		//std::cout << "Messenger reset: " << ep_.toString() << " - " << ep_.isNull() << std::endl;
		if (!ep_.isNull() && ep_.getFD() != -1)
		{
			//std::cout << "Messenger reset destroying endpoint, pollsdfkey = " << ep_.pollfdsKey_ << std::endl;
			ia_->destroyEndPoint(ep_);
		}
	}

	catch (tcpla::exception::InvalidEndPoint & e)
	{
		// this is expected behaviour, so we are not normally notifiying the user.
		LOG4CPLUS_TRACE(pt_->getOwnerApplication()->getApplicationLogger(), "Messenger reset holds invalid endpoint");
	}
	catch (tcpla::exception::IACommandQueueFull & e)
	{
		LOG4CPLUS_FATAL(pt_->getOwnerApplication()->getApplicationLogger(), "Failed to reset in Messenger, could not post to destroy EndPoint : exception trace = " << xcept::stdformat_exception_history(e));
	}

	ep_.clear();
	established_ = false;
	mutex_.give();
}

pt::Address::Reference pt::utcp::I2OMessenger::getLocalAddress ()
{
	return local_;
}

pt::Address::Reference pt::utcp::I2OMessenger::getDestinationAddress ()
{
	return destination_;
}
