// $Id: PeerTransportReceiver.cc,v 1.7 2008/07/18 15:27:19 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "pt/PeerTransportReceiver.h"

pt::PeerTransportReceiver::~PeerTransportReceiver()
{
}


void pt::PeerTransportReceiver::addServiceListener ( pt::Listener* listener ) throw (pt::exception::Exception)
{
	if (listeners_.find(listener->getService()) == listeners_.end())
	{
		listeners_[listener->getService()] = listener;
	}
	else 
	{
		std::string msg = "Listener for service exists already: ";
		msg += listener->getService();
		XCEPT_RAISE (pt::exception::Exception, msg);
	}
}


void pt::PeerTransportReceiver::removeServiceListener (pt::Listener* listener ) throw (pt::exception::Exception)
{
	if (listeners_.find(listener->getService()) != listeners_.end())
	{
		listeners_.erase(listener->getService());
	}
	else 
	{
		std::string msg = "Listener for service does not exist: ";
		msg += listener->getService();
		XCEPT_RAISE (pt::exception::Exception, msg);
	}
}

void pt::PeerTransportReceiver::removeAllServiceListeners()
{
	listeners_.clear();
}


pt::Listener* pt::PeerTransportReceiver::getListener (std::string service) throw (pt::exception::Exception)
{
	if (listeners_.find(service) != listeners_.end())
	{
		return listeners_[service];
	}
	else 
	{
		std::string msg = "Listener for service does not exist: ";
		msg += service;
		XCEPT_RAISE (pt::exception::Exception, msg);
	}
}


bool pt::PeerTransportReceiver::isExistingListener (std::string service)
{
	if (listeners_.find(service) != listeners_.end())
	{
		return true;
	}
	else 
	{
		return false;
	}
}


