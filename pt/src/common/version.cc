// $Id: version.cc,v 1.2 2008/07/18 15:27:19 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "pt/version.h"
#include "config/version.h"
#include "xcept/version.h"
#include "toolbox/version.h"
#include "xoap/version.h"

GETPACKAGEINFO(pt)

void pt::checkPackageDependencies() throw (config::PackageInfo::VersionException)
{
	CHECKDEPENDENCY(config);
	CHECKDEPENDENCY(xcept);
	CHECKDEPENDENCY(toolbox);  
	CHECKDEPENDENCY(xoap); 
}

std::set<std::string, std::less<std::string> > pt::getPackageDependencies()
{
	std::set<std::string, std::less<std::string> > dependencies;

	ADDDEPENDENCY(dependencies,config); 
	ADDDEPENDENCY(dependencies,xcept); 
	ADDDEPENDENCY(dependencies,toolbox); 
	ADDDEPENDENCY(dependencies,xoap); 
  
	return dependencies;
}	
	
