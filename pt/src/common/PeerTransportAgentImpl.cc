// $Id: PeerTransportAgentImpl.cc,v 1.16 2008/07/18 15:27:19 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "pt/PeerTransportAgentImpl.h"
#include "toolbox/FindSubstring.h"
#include "toolbox/net/URL.h"
#include "toolbox/string.h"
#include "pt/PeerTransportReceiver.h"
#include "pt/PeerTransportSender.h"

#include "pt/exception/PeerTransportNotFound.h"
#include "pt/exception/InvalidAddress.h"
#include <vector>
#include <map>
#include <algorithm>

pt::PeerTransportAgentImpl::~PeerTransportAgentImpl()
{
}

pt::PeerTransportAgent * pt::getPeerTransportAgent()
{
	return pt::PeerTransportAgentImpl::instance();
}

// singleton static implementation
pt::PeerTransportAgentImpl * pt::PeerTransportAgentImpl::instance_ = 0;

pt::PeerTransportAgent* pt::PeerTransportAgentImpl::instance()
{
	// Would actually need a Thread lock for making it threas safe

	if (instance_ == 0)
	{
		instance_ = new pt::PeerTransportAgentImpl();
	}
	return instance_;
}
// static initialization end

std::vector<pt::PeerTransport*> pt::PeerTransportAgentImpl::getPeerTransports()
{
	std::vector<pt::PeerTransport*> pts;	
	
	std::vector<pt::PeerTransport*>::size_type i;
	for (i = 0; i < receivers_.size(); i++)
	{
		pts.push_back( receivers_[i] );
	}
	
	for (i = 0; i < senders_.size(); i++)
	{
		pts.push_back( senders_[i] );
	}
	
	std::unique(pts.begin(), pts.end()); // remove duplicates
	
	return pts;
}


void pt::PeerTransportAgentImpl::addPeerTransport(pt::PeerTransport* pt)
throw (pt::exception::Exception)
{
	std::vector<pt::PeerTransport*>::size_type i;
	
	if (pt->getType() == pt::Sender)
	{
		for (i = 0; i < senders_.size(); i++)
		{
			if ((senders_[i]->getProtocol() == pt->getProtocol()) || (senders_[i] == pt))
			{
				std::string msg = "Sender peer transport already installed ";
				msg += pt->getType();
				XCEPT_RAISE (pt::exception::Exception, msg.c_str());
			}
		}
		
		senders_.push_back(pt);

	} else if ((pt->getType() == pt::Receiver) || (pt->getType() == pt::SenderReceiver))
	{
		for (i = 0; i < receivers_.size(); i++)
		{
			if ((receivers_[i]->getProtocol() == pt->getProtocol()) || (receivers_[i] == pt))
			{
				std::string msg = "Receiver peer transport already installed ";
				msg += pt->getType();
				XCEPT_RAISE (pt::exception::Exception, msg.c_str());				
			}
		}
		
		receivers_.push_back(pt);
		
		// don't perform checks for sender list, trust that check for receiver list is sufficient
		if (pt->getType() == pt::SenderReceiver)
		{
			senders_.push_back(pt);
		}
	
		std::vector<std::string> s = pt->getSupportedServices();
		
		for ( i = 0; i < s.size(); i++)
		{
			// if there is a listener for the service available, assign to peer transport
			if (services_.find(s[i]) != services_.end())
			{
				try
				{
					dynamic_cast<pt::PeerTransportReceiver*>(pt)->addServiceListener (services_[ s[i] ]);
				} catch (pt::exception::Exception& pte)
				{
					XCEPT_RETHROW (pt::exception::Exception, "Cannot install peer transport", pte);
				}
			}
		}
	} 
	else 
	{
		// ERROR
		std::string msg = "Cannot install peer transport ";
		msg += pt->getType();
		msg += ", it is not a receiver or sender type";
		XCEPT_RAISE(pt::exception::Exception, msg.c_str());
	}
}


void pt::PeerTransportAgentImpl::removePeerTransport (pt::PeerTransport* pt)
throw (pt::exception::PeerTransportNotFound)
{
	// brute force scan and remove
	std::vector<pt::PeerTransport*>::iterator i;
	bool found = false;
	
	for (i = receivers_.begin(); i != receivers_.end(); i++)
	{
		if (*i == pt)
		{
			receivers_.erase (i);
			found = true;
			break; // need to finish loop since erase(i) affects the sequence
		}
	}
	
	for (i = senders_.begin(); i != senders_.end(); i++)
	{
		if (*i == pt)
		{
			senders_.erase (i);
			found = true;
			break; // need to finish loop since erase(i) affects the sequence
		}
	}
	
	if (!found)
	{
		std::string msg = "Cannot remove peer transport, not found ";
		msg += pt->getType();
		XCEPT_RAISE (pt::exception::PeerTransportNotFound, msg.c_str());
	}
}


pt::PeerTransport* pt::PeerTransportAgentImpl::getPeerTransport (std::string protocol, std::string service, pt::TransportType t)
throw (pt::exception::PeerTransportNotFound)
{
	if (t == pt::Sender)
	{
		for (std::vector<pt::PeerTransport*>::size_type  i = 0; i < senders_.size(); i++)
		{
			if (senders_[i]->isServiceSupported(service) && (senders_[i]->getProtocol() == protocol))
			{
				// found matching peer transport
				return senders_[i];
			}		
		}	
		std::string msg = "Sender peer transport not found for protocol/service: ";
		msg += protocol;
		msg += "/";
		msg += service;
		XCEPT_RAISE (pt::exception::PeerTransportNotFound, msg);
	} 
	else if ((t == pt::Receiver) || (t==pt::SenderReceiver))
	{
		for (std::vector<pt::PeerTransport*>::size_type  i = 0; i < receivers_.size(); i++)
		{
			if (receivers_[i]->isServiceSupported(service) && (receivers_[i]->getProtocol() == protocol))
			{
				// found matching peer transport
				return receivers_[i];
			}		
		}	
		std::string msg = "Receiver or Sender/Receiver peer transport not found for protocol/service: ";
		msg += protocol;
		msg += "/";
		msg += service;
		XCEPT_RAISE (pt::exception::PeerTransportNotFound, msg);
	}
	else 
	{
		std::string msg = "Invalid tranport type. Not a sender/receiver or a combination of the two";
		XCEPT_RAISE (pt::exception::PeerTransportNotFound, msg);
	}
}


//
// An address lookas as follows: <service>.<protocol>://<hostname>/<service>/<other>
// RFC 2396 and RFC 3288
// Service.Protocol, 
// e.g. soap.http://
// e.g. AsyncI2O.tcp://
// ...
pt::Address::Reference 
pt::PeerTransportAgentImpl::createAddress( const std::string & url , const std::string & service)  
throw (pt::exception::InvalidAddress)
{
	try
	{
		toolbox::net::URL u(url);
	
		// Get all available transports
		std::vector<pt::PeerTransport*> transports = this->getPeerTransports();
	
		for (std::vector<pt::PeerTransport*>::size_type i = 0; i < transports.size(); i++ )
		{
			// the first transport that provide such a protocol and service knows how to make the address
			if ( transports[i]->getProtocol() == u.getProtocol() )
			{
				return transports[i]->createAddress(url, service);
			}
		}
	}
	catch(toolbox::net::exception::MalformedURL& e)
	{
		std::stringstream msg;
		msg << "Could not parse URL '" << url.c_str() << "'" << std::endl;
		XCEPT_RAISE(pt::exception::InvalidAddress, msg.str());
	}
	
	// did not find any transport for this address
	// throw an error
	std::string msg = toolbox::toString("Could not find peer transport for url: %s ,when creating an address",url.c_str());
	XCEPT_RAISE(pt::exception::InvalidAddress,msg);
}

//
// Alternatively pass parameters in format name = value, name = value, ....
//
pt::Address::Reference 
pt::PeerTransportAgentImpl::createAddress( std::map<std::string, std::string, std::less<std::string> >& address )
throw (pt::exception::InvalidAddress)
{
	std::string service;
	if ( address.find("service") != address.end() )
	{
		service = address["service"];
	}
	else
	{
		XCEPT_RAISE(pt::exception::InvalidAddress,"Service specification missing in address");
	}
	
	std::string protocol;
	if ( address.find("protocol") != address.end() )
	{
		protocol = address["protocol"];
	
	}
	else
	{
		XCEPT_RAISE(pt::exception::InvalidAddress,"Protocol specification missing in address");
	}
	
	// Get all available transports
	std::vector<pt::PeerTransport*> transports = this->getPeerTransports();
	
	for (std::vector<pt::PeerTransport*>::size_type i = 0; i < transports.size(); i++ )
	{
		// the first transport that provide such a protocol and service knows how to make the address
		if ( (transports[i]->getProtocol() == protocol ) && transports[i]->isServiceSupported(service) ) 
		{
			return transports[i]->createAddress(address);
		}
	}
	
	// did not find any transport for this address
	// throw an error
	std::string msg = toolbox::toString("Could not find peer transport for service: %s protocol: %s ,when creating an address",service.c_str(), protocol.c_str());
	XCEPT_RAISE(pt::exception::InvalidAddress,msg);
}


pt::Messenger::Reference pt::PeerTransportAgentImpl::getMessenger ( pt::Address::Reference dest , pt::Address::Reference local) throw (pt::exception::NoMessenger)
{
		std::string service = dest->getService();
		std::string protocol = dest->getProtocol();
		
		// look only for sender transport (e.g. SenderReceiver or Sender )
		try
		{
			pt::PeerTransport * pt = this->getPeerTransport(protocol,service, pt::Sender );
			return dynamic_cast<pt::PeerTransportSender*>(pt)->getMessenger(dest, local);
		} catch (pt::exception::PeerTransportNotFound& ptnf)
		{
			std::string msg = "Cannot retrieve peer transport for service ";
			msg += service;
			msg += ", protocol ";
			msg += protocol;
			XCEPT_RETHROW (pt::exception::NoMessenger, msg, ptnf);
		}
		catch (pt::exception::UnknownProtocolOrService & upos)
		{
			std::string msg = "Cannot retrieve messenger for service ";
			msg += service;
			msg += ", protocol ";
			msg += protocol;
			XCEPT_RETHROW (pt::exception::NoMessenger, msg, upos);
		
		}
}


void pt::PeerTransportAgentImpl::addListener ( pt::Listener* listener) throw (pt::exception::DuplicateListener)
{
		if (services_.find(listener->getService()) == services_.end())
		{
			// not existing yet
			services_[listener->getService()] = listener;
			
			// Look for all pts and set listener and add to all of them
			std::vector<pt::PeerTransport*>::iterator i = receivers_.begin();
			
			while (i != receivers_.end())
			{
				std::vector<std::string> s = (*i)->getSupportedServices();
				
				if (std::find (s.begin(), s.end(), listener->getService()) != s.end())
				{
					// found matching service
					dynamic_cast<pt::PeerTransportReceiver*>(*i)->addServiceListener(listener);
				}	
				
				i++;
			}
		}
		else
		{
			XCEPT_RAISE(pt::exception::DuplicateListener, toolbox::toString("listener for service: %s already existing",listener->getService().c_str()));
		}
}


void pt::PeerTransportAgentImpl::removeListener ( pt::Listener* listener)  throw (pt::exception::NoListener)
{
	if (services_.find(listener->getService()) != services_.end())
	{
		if ( services_[listener->getService()] == listener ) 
		{
			// not existing yet
			services_.erase(listener->getService());
			
			// Look for all pts and set listener and add to all of them
			std::vector<pt::PeerTransport*>::iterator i = receivers_.begin();
			
			while (i != receivers_.end())
			{
				std::vector<std::string> s = (*i)->getSupportedServices();
				
				if (std::find (s.begin(), s.end(), listener->getService()) != s.end())
				{
					// found matching service
					dynamic_cast<pt::PeerTransportReceiver*>(*i)->removeServiceListener(listener);
				}	
				
				i++;
			}
		}
		else
		{
			// no matching listeners pointer throw
			XCEPT_RAISE(pt::exception::NoListener, "listener not registered, cannot remove");
		}	
	}
	else
	{
			XCEPT_RAISE(pt::exception::NoListener, toolbox::toString("no listener for  service: %s installed",listener->getService().c_str()));
	}
	
}


// Look only for receivers. Onle these have listeners.
//
pt::Listener* pt::PeerTransportAgentImpl::getListener (const std::string & service) throw (pt::exception::NoListener)
{
	std::map<std::string, pt::Listener*, std::less<std::string> >::iterator i = services_.find(service);
	if (i != services_.end()) 
	{
		return (*i).second;
	}
	else
	{
		XCEPT_RAISE(pt::exception::NoListener, toolbox::toString("no listener found for service: %s",service.c_str()));
	}
	return 0; // makes the compiler happy		
}

std::vector<pt::Listener*> pt::PeerTransportAgentImpl::getListeners(const std::string & service)
{
	std::vector<pt::Listener*> listeners;
	std::map<std::string, pt::Listener*, std::less<std::string> >::iterator i;
	for (i = services_.begin(); i != services_.end(); i++)
	{
		listeners.push_back( (*i).second );
	}
	return listeners;
}


void pt::PeerTransportAgentImpl::removeAllListeners()
{
	// remove listeners from all receivers
	std::vector<pt::PeerTransport*>::iterator i = receivers_.begin();

	while (i != receivers_.end())
	{		
		dynamic_cast<pt::PeerTransportReceiver*>(*i)->removeAllServiceListeners();			
		i++;
	}

	services_.clear();
}
