// $Id: SecurityPolicyFactory.cc,v 1.2 2008/07/18 15:27:19 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber, G. Lo Presti and L. Orsini                      *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include <string>

#include "pt/exception/Exception.h"
#include "pt/SecurityPolicyFactory.h"


pt::SecurityPolicyFactory* pt::SecurityPolicyFactory::instance_ = 0;

pt::SecurityPolicyFactory* pt::getSecurityPolicyFactory()
{
	return pt::SecurityPolicyFactory::getInstance();
}

pt::SecurityPolicyFactory::SecurityPolicyFactory()
{
	
}

pt::SecurityPolicyFactory::~SecurityPolicyFactory()
{
	std::map<std::string, pt::SecurityPolicy*, std::less<std::string> >::iterator i;
	for (i = policies_.begin(); i != policies_.end(); i++)
	{
		delete (*i).second;
	}
}

pt::SecurityPolicyFactory* pt::SecurityPolicyFactory::getInstance()
{
	if (instance_ == 0)
	{
		instance_ = new pt::SecurityPolicyFactory();
	}
	return instance_;
}



	
void pt::SecurityPolicyFactory::destroyInstance()
{
	if (instance_ != 0)
	{
		delete instance_;
		instance_ = 0;
	}
}
	
pt::SecurityPolicy* pt::SecurityPolicyFactory::getSecurityPolicy(const std::string& name) throw (pt::exception::Exception)
{
	std::map<std::string, pt::SecurityPolicy*, std::less<std::string> >::iterator i;
	i = policies_.find(name);
	if (i != policies_.end())
	{
		return ((*i).second);
	} else
	{
		std::string msg = "Security Policy not found: ";
		msg += name;
		XCEPT_RAISE (pt::exception::Exception, msg);
	}
}
		
void pt::SecurityPolicyFactory::addSecurityPolicyImplementation(const std::string& name, pt::SecurityPolicy* p) throw (pt::exception::Exception)
{
	std::map<std::string, pt::SecurityPolicy*, std::less<std::string> >::iterator i;
	i = policies_.find(name);
	if (i != policies_.end())
	{
		std::string msg = "Security Policy already existing: ";
		msg += name;
		XCEPT_RAISE (pt::exception::Exception, msg);
	} else
	{
		policies_[name] = p;
	}
}
	
void pt::SecurityPolicyFactory::removeSecurityPolicyImplementation(const std::string & name) throw (pt::exception::Exception)
{
	std::map<std::string, pt::SecurityPolicy*, std::less<std::string> >::iterator i;
	i = policies_.find(name);
	if (i != policies_.end())
	{
		delete ((*i).second);
		policies_.erase(i);
	} else
	{
		std::string msg = "Security Policy not found: ";
		msg += name;
		XCEPT_RAISE (pt::exception::Exception, msg);
	}
}

std::vector<std::string>  pt::SecurityPolicyFactory::getSecurityPolicyNames()
{
	std::vector<std::string> names;
	std::map<std::string, pt::SecurityPolicy*, std::less<std::string> >::iterator i;
	for ( i =  policies_.begin(); i != policies_.end(); i++ )
	{
		names.push_back((*i).first);

	}
	return names;
	}	
