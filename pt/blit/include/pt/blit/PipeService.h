// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2015, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, D. Simelevicius                                    *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _pt_blit_PipeService_h_
#define _pt_blit_PipeService_h_

#include <list>
#include "pt/blit/PipeAdvertisement.h"
#include "pt/blit/PipeServiceListener.h"
#include "pt/blit/InputPipeListener.h"

#include "pt/blit/InputPipe.h"
#include "pt/blit/exception/Exception.h"

namespace pt
{
	namespace blit
	{

		class PipeService
		{
			public:
				 virtual pt::blit::InputPipe * createInputPipe(pt::blit::PipeAdvertisement & adv, pt::blit::InputPipeListener * listener ) throw (pt::blit::exception::Exception) = 0;
				 virtual void destroyInputPipe(pt::blit::InputPipe * ip) throw (pt::blit::exception::Exception) = 0;

				 virtual std::list<PipeAdvertisement> getPipeAdvertisements(const std::string & service, const std::string & protocol) = 0;

				 virtual void addPipeServiceListener(pt::blit::PipeServiceListener * listener) = 0;


		};

	}
}

#endif
