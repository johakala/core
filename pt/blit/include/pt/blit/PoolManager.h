// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2015, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, D. Simelevicius                                    *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _pt_blit_PoolManager_h_
#define _pt_blit_PoolManager_h_

#include <string>

#include "pt/blit/exception/Exception.h"

#include "toolbox/mem/Reference.h"
#include "toolbox/mem/Pool.h"

namespace pt
{
	namespace blit
	{
		class PoolManager
		{
			public:

				PoolManager (size_t psize, const std::string & tag, size_t maxFD);

				toolbox::mem::Pool * getPool (int fd);

				//void reset();

			private:

				size_t psize_;

				std::string tag_;

				std::vector<toolbox::mem::Pool *> usedPools_;

		};
	}
}

#endif
