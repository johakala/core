// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2015, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, D. Simelevicius                                    *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _pt_blit_InputPipe_h_
#define _pt_blit_InputPipe_h_


#include "pt/blit/PipeAdvertisement.h"
#include "pt/blit/InputPipeListener.h"
#include "pt/blit/exception/Exception.h"
#include "toolbox/mem/Reference.h"

namespace pt
{
	namespace blit
	{

		typedef struct {
				toolbox::mem::Reference * ref;
				int sid;
		}  BulkTransferEvent;

		class InputPipe
		{
				friend class PipeService;
				friend class Dispatcher;

			public:

				~InputPipe();

				pt::blit::PipeAdvertisement	getAdvertisement();

				bool dequeue(BulkTransferEvent * event);

				void grantBuffer(toolbox::mem::Reference * ref)  throw (pt::blit::exception::Exception);


			protected:

				void enqueue(toolbox::mem::Reference * ref, int sid)  throw (pt::blit::exception::Exception);

				InputPipe (pt::blit::PipeAdvertisement adv, toolbox::rlist<pt::blit::BulkTransferEvent> * iqueue , pt::blit::InputPipeListener * listener) ;

				void fireConnectionAccepted(pt::blit::PipeConnectionAccepted & pc);
				void fireConnectionClosedByPeer(pt::blit::PipeConnectionClosedByPeer & pc);
				void fireConnectionResetByPeer(pt::blit::PipeConnectionResetByPeer & pc);



				void close() throw (pt::blit::exception::Exception);




				pt::blit::PipeAdvertisement adv_;
				toolbox::rlist<pt::blit::BulkTransferEvent> * iqueue_;
				pt::blit::InputPipeListener * listener_;
				std::list<tcpla::EndPoint> accepted_;




		};

	}
}

#endif


