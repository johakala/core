// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2015, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, D. Simelevicius                                    *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/


#ifndef _pt_blit_Allocator_h_
#define _pt_blit_Allocator_h_

#include <list>

#include "toolbox/mem/Allocator.h"
#include "toolbox/mem/MemoryPartition.h"
#include "toolbox/mem/exception/FailedAllocation.h"
#include "toolbox/mem/exception/FailedCreation.h"
#include "toolbox/mem/exception/FailedDispose.h"


namespace pt
{
	namespace blit
	{

	class Allocator : public toolbox::mem::Allocator
	{
		public:

			Allocator (const std::string & name, size_t committedSize) throw (toolbox::mem::exception::FailedCreation);

			virtual ~Allocator ();

			//! Allocate a buffer from conventional memory of a given size
			toolbox::mem::Buffer * alloc (size_t size, toolbox::mem::Pool * pool) throw (toolbox::mem::exception::FailedAllocation);

			//! Call delete on the Buffer
			/*! This calls delete on the Buffer */
			void free (toolbox::mem::Buffer * buffer) throw (toolbox::mem::exception::FailedDispose);

			std::string type ();

			bool isCommittedSizeSupported ();

			size_t getCommittedSize ();

			/*! Retrieve the number of byte currently used
			 */
			size_t getUsed ();

		private:

			unsigned char * buffer_;
			size_t committedSize_;

			toolbox::mem::MemoryPartition memPartition_;
			std::string name_;
	};

}
}

#endif
