// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2015, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, D. Simelevicius                                    *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _pt_blit_PipeConnectionAccepted_h_
#define _pt_blit_PipeConnectionAccepted_h_

#include <string>
#include "tcpla/EndPoint.h"

namespace pt
{
	namespace blit
	{

		class PipeConnectionAccepted
		{

					friend class Dispatcher;
					friend class InputPipe;
					friend class PeerTransport;
			public:
					int getPipeIndex();
					int getSID();
					std::string getPeerName();
					size_t getPort();
			protected:

				PipeConnectionAccepted(tcpla::EndPoint ep_handle, int index, int sid, const std::string & peername, size_t port);
				tcpla::EndPoint getEndPoint();


			private:

				tcpla::EndPoint ep_handle_;
				int index_;
				int sid_;
				std::string peername_;
				size_t port_;


		};

	}
}

#endif
