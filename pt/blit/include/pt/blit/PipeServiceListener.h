// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2015, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, D. Simelevicius                                    *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _pt_blit_PipeServiceListener_h_
#define _pt_blit_PipeServiceListener_h_


#include "pt/blit/PipeAdvertisement.h"
#include "pt/blit/PipeConnectionAccepted.h"


namespace pt
{
	namespace blit
	{

		class PipeServiceListener
		{
			public:

				virtual void pipeServiceEvent(pt::blit::PipeAdvertisement  & adv) = 0;


		};

	}
}

#endif
