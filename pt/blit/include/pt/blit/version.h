// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2015, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, D.Simelevicius				 	 *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

//
// Version definition for bulk peer transport
//
#ifndef _ptblit_version_h_
#define _ptblit_version_h_

#include "config/PackageInfo.h"
// !!! Edit this line to reflect the latest package version !!!

#define PTBLIT_VERSION_MAJOR 1
#define PTBLIT_VERSION_MINOR 2
#define PTBLIT_VERSION_PATCH 3
// If any previous versions available
// #define PTBLIT_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#undef PTBLIT_PREVIOUS_VERSIONS


//
// Template macros
//
#define PTBLIT_VERSION_CODE PACKAGE_VERSION_CODE(PTBLIT_VERSION_MAJOR,PTBLIT_VERSION_MINOR,PTBLIT_VERSION_PATCH)
#ifndef PTBLIT_PREVIOUS_VERSIONS
#define PTBLIT_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(PTBLIT_VERSION_MAJOR,PTBLIT_VERSION_MINOR,PTBLIT_VERSION_PATCH)
#else 
#define PTBLIT_FULL_VERSION_LIST  PTBLIT_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(PTBLIT_VERSION_MAJOR,PTBLIT_VERSION_MINOR,PTBLIT_VERSION_PATCH)
#endif 
namespace ptblit
{
    const std::string package  =  "ptblit";
    const std::string versions =  PTBLIT_FULL_VERSION_LIST;
    const std::string summary = "Bulk Peer Transport";
    const std::string description = "";
    const std::string authors = "Luciano Orsini, Dainius Simelevicius";
    const std::string link = "http://xdaq.web.cern.ch";
    config::PackageInfo getPackageInfo();
    void checkPackageDependencies() throw (config::PackageInfo::VersionException);
    std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif

