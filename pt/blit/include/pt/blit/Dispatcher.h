// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2015, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, D. Simelevicius                                    *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _pt_blit_Dispatcher_h_
#define _pt_blit_Dispatcher_h_

#include <string>
#include <sstream>

#include "xdaq/ApplicationContext.h"
#include "xdaq/ApplicationRegistry.h"
#include "toolbox/mem/Reference.h"

#include "log4cplus/logger.h"
#include "pt/blit/exception/Exception.h"
#include "toolbox/BSem.h"
#include "pt/exception/UnknownProtocolOrService.h"

#include "xdaq/Object.h"
#include "pt/blit/PipeService.h"
#include "pt/blit/PipeServiceListener.h"
#include "pt/blit/PoolManager.h"

// created upon PSP creation
namespace pt
{
	namespace blit
	{
		class PeerTransport;

		class Dispatcher:  public xdaq::Object
		{
			public:

				Dispatcher(xdaq::Application * owner, int index, size_t maxClients, size_t maxReceiveBuffers, size_t maxBlockSize);
				virtual ~Dispatcher();

				//void processIncomingMessage (toolbox::mem::Reference* msg, int index, int sid, tcpla::EndPoint & ep_handle, size_t len)
				//		throw (pt::blit::exception::Exception);
				void processIncomingMessage (toolbox::mem::Reference* ref, size_t len, int sid) throw (pt::blit::exception::Exception);

				pt::blit::InputPipe *  createInputPipe(pt::blit::PipeAdvertisement & adv, pt::blit::InputPipeListener * listener) throw (pt::blit::exception::Exception);
				void destroyInputPipe(pt::blit::InputPipe * ip) throw (pt::blit::exception::Exception);

				void connectionAccepted( int index, int sid, int fd, tcpla::EndPoint & ep_handle) throw (pt::blit::exception::Exception);
				void connectionRequest( tcpla::InterfaceAdapter * ia, tcpla::PublicServicePoint * psp, int cr_handle) throw (pt::blit::exception::Exception);
				void connectionClosedByPeer( int index, int sid, int fd, tcpla::EndPoint & ep_handle) throw (pt::blit::exception::Exception);
				void connectionResetByPeer( int index, int sid, int fd, tcpla::EndPoint & ep_handle) throw (pt::blit::exception::Exception);


			protected:

				toolbox::BSem mutex_;
				size_t maxClients_;
				pt::blit::InputPipe* inputPipe_;
				std::vector<toolbox::mem::Pool *> memPools_;
				pt::blit::PoolManager* recvPoolManager_;
				int index_;
				size_t maxReceiveBuffers_;
				size_t maxBlockSize_;
				toolbox::rlist<BulkTransferEvent> *iqueue_;

		};
	}
}

#endif
