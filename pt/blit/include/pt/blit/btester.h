// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2015, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, D. Simelevicius                                    *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _pt_blit_btester_h_
#define _pt_blit_btester_h_

#include <set>
#include <streambuf>
#include <istream>

#include "toolbox/mem/Reference.h"
#include "toolbox/mem/Pool.h"
#include "toolbox/fsm/FiniteStateMachine.h"
#include "xdaq/Application.h"
#include "toolbox/PerformanceMeter.h"
#include "xdata/UnsignedLong.h"
#include "xdata/UnsignedInteger.h"
#include "xdata/String.h"
#include "xdata/Double.h"
#include "xdata/Boolean.h"
#include "xdata/Event.h"
#include "xdata/ActionListener.h"
#include "toolbox/task/Timer.h"
#include "pt/blit/exception/Exception.h"

#include "toolbox/hexdump.h"
//
// cgicc
//
#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"
#include "xgi/Utils.h"
#include "xgi/Input.h"
#include "xgi/Output.h"

#include "xgi/Method.h"

#include "toolbox/task/WorkLoop.h"
#include "pt/blit/PipeServiceListener.h"
#include "pt/blit/InputPipe.h"
namespace pt
{
	namespace blit
	{

		class btester:  public xdaq::Application, xdata::ActionListener, public toolbox::task::TimerListener, public pt::blit::PipeServiceListener, public pt::blit::InputPipeListener
		{
			public:
				XDAQ_INSTANTIATOR();

				btester(xdaq::ApplicationStub* c) throw(xdaq::exception::Exception);

				void start ( xgi::Input * in, xgi::Output * out ) throw ( xgi::exception::Exception );
				void stop  ( xgi::Input * in, xgi::Output * out ) throw ( xgi::exception::Exception );


				bool process(toolbox::task::WorkLoop* wl);

				void pipeServiceEvent(pt::blit::PipeAdvertisement  & adv);
				void connectionAcceptedEvent(pt::blit::PipeConnectionAccepted & pc);
				void connectionClosedByPeerEvent(pt::blit::PipeConnectionClosedByPeer & pc);
				void connectionResetByPeerEvent(pt::blit::PipeConnectionResetByPeer & pca);



			protected:

				void timeExpired (toolbox::task::TimerEvent& e);
				void actionPerformed( xdata::Event& event);
				double std_deviation(std::vector<double>& samples, double mean );


			protected:


				size_t    counter_;     // counter for all received messages
				toolbox::BSem mutex_;



				toolbox::TimeVal lastTime_; // used to measure time interval for measuring rate
				xdata::UnsignedLong numberOfSamples_;
				xdata::String sampleTime_;
				xdata::UnsignedLong  index_;     // current sample
				std::vector<double> measurementHistory_;



				toolbox::task::ActionSignature* process_;
				std::vector<pt::blit::InputPipe *> ipipes_;



		};
	}}

#endif
