// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2015, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, D. Simelevicius                                    *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _pt_blit_PeerTransport_h
#define _pt_blit_PeerTransport_h

#include "pt/blit/EventHandler.h"

#include "tcpla/InterfaceAdapter.h"
#include "tcpla/EndPoint.h"

#include <map>

#include "pt/PeerTransportSender.h"
#include "pt/PeerTransportReceiver.h"
#include "pt/PeerTransportAgent.h"
#include "i2o/Listener.h"
#include "pt/exception/Exception.h"

#include "pt/Address.h"
#include "xdaq/Object.h"

#include "toolbox/mem/Reference.h"
#include "toolbox/BSem.h"
#include "toolbox/mem/MemoryPoolFactory.h"

#include "xdata/Double.h"
#include "xdata/UnsignedInteger.h"

#include "pt/blit/exception/Exception.h"

#include "pt/blit/PipeService.h"
#include "pt/blit/PipeServiceListener.h"

#define BLIT_SERVICE_NAME "blit"
#define BLIT_PROTOCOL_NAME "btcp"


namespace pt
{

	namespace blit
	{

		class PeerTransport: public pt::blit::PipeService, public pt::PeerTransportSender, public pt::PeerTransportReceiver, public xdaq::Object
		{
			public:

				PeerTransport ( xdaq::Application * parent, size_t maxClients, size_t maxReceiveBuffers, size_t maxBlockSize, const std::string & protocol);

				~PeerTransport ();

				friend std::ostream& operator<< ( std::ostream &sout, pt::blit::PeerTransport & pt );


				//! Retrieve the type of peer transport (Sender or Receiver or both)
				//
				pt::TransportType getType ();

				void setHandler ( tcpla::EventHandler * handler );

				pt::Address::Reference createAddress ( const std::string& url, const std::string& service ) throw ( pt::exception::InvalidAddress );

				pt::Address::Reference createAddress ( std::map<std::string, std::string, std::less<std::string> > & address ) throw ( pt::exception::InvalidAddress );

				//! Returns the implemented protocol ("loopback" in this version)
				//
				std::string getProtocol ();

				//! Retrieve a list of supported services (i2o, b2in, blit)
				//
				std::vector<std::string> getSupportedServices ();

				//! Returns true if a service is supported (i2o, b2in, blit), false otherwise
				//
				bool isServiceSupported ( const std::string & service );

				//! Retrieve a loopback messenger for the fifo peer transport that allows context internal application communication
				//
				pt::Messenger::Reference getMessenger ( pt::Address::Reference destination, pt::Address::Reference local ) throw ( pt::exception::UnknownProtocolOrService );

				//! Internal function to add a message processing listener for this peer transport
				//
				void addServiceListener ( pt::Listener * listener ) throw ( pt::exception::Exception );

				//! Internal function to remove a message processing listener for this peer transport
				//
				void removeServiceListener ( pt::Listener * listener ) throw ( pt::exception::Exception );

				//! Internal function to remove all message processing listeners for this peer transport
				//
				void removeAllServiceListeners ();

				//! Function to configure this peer transport with a loopback address
				//
				void config ( pt::Address::Reference address ) throw ( pt::exception::Exception );


				std::map<std::string, tcpla::InterfaceAdapter *> iav_;
				std::vector<tcpla::PublicServicePoint*> psp_;

				std::map<std::string, size_t> eventCounter_;
				std::string lastEvent_;

				// pipe services

				pt::blit::InputPipe * createInputPipe(pt::blit::PipeAdvertisement & adv, pt::blit::InputPipeListener * listener ) throw (pt::blit::exception::Exception);
				void destroyInputPipe(pt::blit::InputPipe * ip) throw (pt::blit::exception::Exception);
				void addPipeServiceListener(pt::blit::PipeServiceListener * listener);
				std::list<PipeAdvertisement> getPipeAdvertisements(const std::string & service, const std::string & protocol);


			protected:

				size_t maxClients_;
				toolbox::BSem mutex_;
				std::string protocol_;
				size_t maxReceiveBuffers_;
				size_t maxBlockSize_;

			private:

				tcpla::InterfaceAdapter * getInterfaceAdapter (pt::Address::Reference address, pt::blit::EventHandler * eh) throw (pt::blit::exception::Exception);

				pt::blit::PipeServiceListener * pipeServiceListener_;

				pt::blit::EventHandler * handler_;


		};

	}
}

#endif
