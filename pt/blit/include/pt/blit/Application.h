// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2015, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, D. Simelevicius                                    *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _pt_blit_Application_h_
#define _pt_blit_Application_h_

#include "xdaq/Application.h"
#include "xgi/framework/UIManager.h"

#include "pt/blit/PeerTransport.h"
#include "tcpla/WaitingWorkLoop.h"
#include "tcpla/MemoryCache.h"

//#include "i2o/i2o.h"

#include "xdata/UnsignedInteger.h"
#include "xdata/UnsignedInteger64.h"
#include "xdata/UnsignedLong.h"
#include "xdata/Double.h"
#include "xdata/String.h"
#include "xdata/Boolean.h"
#include "xdata/Integer.h"
#include "xdata/Bag.h"
#include "xdata/Vector.h"

#include "xdaq/ApplicationGroup.h"
#include "xdaq/ApplicationStub.h"
#include "xdaq/ApplicationContext.h"

//#include "i2o/Method.h"
#include "toolbox/mem/Reference.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "toolbox/mem/Pool.h"
#include "toolbox/mem/CommittedHeapAllocator.h"
#include "toolbox/exception/Handler.h"
#include "toolbox/net/URN.h"
#include "toolbox/task/Timer.h"
#include "toolbox/task/TimerEvent.h"
#include "toolbox/task/TimerListener.h"

#include <list>
#include <algorithm>
#include <map>

//
// cgicc
//
#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"
#include "xgi/Utils.h"
#include "xgi/Input.h"
#include "xgi/Output.h"

#include "xoap/MessageReference.h"
#include "xoap/MessageFactory.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPBody.h"
#include "xoap/domutils.h"
#include "xoap/Method.h"

namespace pt
{

	namespace blit
	{

		class Application: public xdaq::Application, public xgi::framework::UIManager, public xdata::ActionListener
		{

			public:
				XDAQ_INSTANTIATOR();

				Application(xdaq::ApplicationStub* stub) throw (xdaq::exception::Exception);

				//
				// Callback for requesting current exported parameter values
				//
				void actionPerformed (xdata::Event& e);

				// Web callback functions
				void Default(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);

				pt::blit::PeerTransport * pt_;

				private:
				void SenderTabPage (xgi::Output * out);
				void ReceiverTabPage (xgi::Output * out);
				void SettingsTabPage (xgi::Output * out);
				void EventsTabPage (xgi::Output * out);

				protected:

				xdata::UnsignedInteger maxClients_; //maximum number of connections
				xdata::String protocol_; //the XDAQ protocol e.g btcp
				xdata::UnsignedInteger maxReceiveBuffers_;
				xdata::UnsignedInteger maxBlockSize_;

		};


	}

}

#endif
