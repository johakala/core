// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2015, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, D.Simelevicius                                     *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/
#include "pt/blit/InputPipe.h"
#include "pt/blit/Buffer.h"

//#define	MAX_RECEIVE_ENDPOINTS	1024

pt::blit::InputPipe::InputPipe (pt::blit::PipeAdvertisement adv, toolbox::rlist<pt::blit::BulkTransferEvent> * iqueue, pt::blit::InputPipeListener * listener): adv_(adv),iqueue_(iqueue),listener_(listener)
{

}

pt::blit::InputPipe::~InputPipe ()
{
}


pt::blit::PipeAdvertisement	pt::blit::InputPipe::getAdvertisement()
{
	return adv_;
}

bool pt::blit::InputPipe::dequeue(pt::blit::BulkTransferEvent * event)
{
	if ( ! iqueue_->empty() )
	{
		 *event = iqueue_->front();
	     iqueue_->pop_front();
	     //std::cout << "going true" << event->ref << std::endl;
	     return true;
	}
	//std::cout << "going false" << std::endl;
	return false;
}
//#define MAX_RECEIVE_BUFFER_SIZE 0x40000

void pt::blit::InputPipe::grantBuffer(toolbox::mem::Reference * ref )  throw (pt::blit::exception::Exception)
{
	pt::blit::Buffer * buffer = 0;
	try
	{
		buffer = dynamic_cast<pt::blit::Buffer*>(ref->getBuffer());
		if (buffer == 0)
		{
			ref->release();
			std::stringstream msg;
			msg << "Failed to cast, invalid memory allocator/pool, expected pt::blit memory pool";
			XCEPT_RAISE(pt::blit::exception::Exception, msg.str());
		}
	}
	catch (std::bad_cast & e)
	{
		XCEPT_RAISE(pt::blit::exception::Exception, e.what());
	}


	try
	{
		tcpla::TCPLA_CONTEXT cookie;
		cookie.as_ptr = ref; // rememeber which reference
		buffer->ep_handle_.psp_->postRecvBuffer(buffer->ep_handle_, cookie, (char *) ref->getDataLocation(), ref->getBuffer()->getSize());

	}
	catch ( tcpla::exception::FailedToPostSend & e)
	{
		std::stringstream msg;
		msg << "Failed to provide buffer for incoming message";
		XCEPT_RETHROW(pt::blit::exception::Exception, msg.str(), e);
	}
	catch ( tcpla::exception::InvalidEndPoint & e)
	{
		std::stringstream msg;
		msg << "Failed to provide buffer for incoming message";
		XCEPT_RETHROW(pt::blit::exception::Exception, msg.str(), e);

	}

}
//void grantBuffer(toolbox::mem::Reference * ref, pt::blt::BlukTransferCookie & cookie );

void pt::blit::InputPipe::fireConnectionAccepted(pt::blit::PipeConnectionAccepted & pca)
{
	accepted_.push_back(pca.getEndPoint());

	if ( listener_ != 0 )
		listener_->connectionAcceptedEvent(pca);
}

void pt::blit::InputPipe::fireConnectionClosedByPeer(pt::blit::PipeConnectionClosedByPeer & pca)
{

	if ( listener_ != 0 )
		listener_->connectionClosedByPeerEvent(pca);
}

void pt::blit::InputPipe::fireConnectionResetByPeer(pt::blit::PipeConnectionResetByPeer & pca)
{

	if ( listener_ != 0 )
		listener_->connectionResetByPeerEvent(pca);
}




void pt::blit::InputPipe::enqueue(toolbox::mem::Reference * ref, int sid)  throw (pt::blit::exception::Exception)
{
	try
	{
		BulkTransferEvent  event;
		event.ref = ref;
		event.sid = sid;

		iqueue_->push_back(event);
	}
	catch(toolbox::exception::RingListFull & e)
	{
		XCEPT_RETHROW(pt::blit::exception::Exception, "failed to enqueue received buffer", e);
	}
}

void pt::blit::InputPipe::close() throw (pt::blit::exception::Exception)
{

	 // disconnect all socket for this PSP

	// clear endpoints
	for ( std::list<tcpla::EndPoint>::iterator j = accepted_.begin(); j != accepted_.end(); j++ )
	{
		try
		{
			//std::cout << "destroy endpoint" << std::endl;
			(*j).ia_->destroyEndPoint((*j));
		}
		catch ( tcpla::exception::IACommandQueueFull & e )
		{
			std::stringstream msg;
			msg << "Failed to destroy endpoint";
			XCEPT_RETHROW(pt::blit::exception::Exception, msg.str(), e);

		}
	}

	 // free all buffer so that receiver goes to back pressure
	while ( ! iqueue_->empty() )
	{
			pt::blit::BulkTransferEvent  event = iqueue_->front();
		    std::cout << "free buffer"  << std::endl;
			event.ref->release();
			iqueue_->pop_front();
	}

	accepted_.clear();


}

