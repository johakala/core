// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2015, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, D. Simelevicius                                    *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include "pt/blit/Allocator.h"

#include "pt/blit/Buffer.h"

#include "toolbox/PolicyFactory.h"
#include "toolbox/AllocPolicy.h"

#include <sstream>
#include <string>
#include <limits>

#include "toolbox/string.h"

pt::blit::Allocator::Allocator ( const std::string & name, size_t committedSize) throw (toolbox::mem::exception::FailedCreation)
	: name_(name)
{
	committedSize_ = committedSize;

	if (committedSize > static_cast<size_t>(std::numeric_limits < ssize_t > ::max()))
	{
		std::stringstream msg;
		msg << "Failed to create committed heap allocator, requested size " << committedSize << ", allowed size " << std::numeric_limits < ssize_t > ::max();
		XCEPT_RAISE(toolbox::mem::exception::FailedCreation, msg.str());
	}

	toolbox::PolicyFactory * factory = toolbox::getPolicyFactory();
	toolbox::AllocPolicy * policy = 0;

	try
	{
		std::stringstream ss;
		ss << this->name_;
		toolbox::net::URN urn(ss.str(), this->type());
		policy = dynamic_cast<toolbox::AllocPolicy *>(factory->getPolicy(urn, "alloc"));
		buffer_ = (unsigned char*) (policy->alloc(committedSize));
	}
	catch (toolbox::exception::Exception & e)
	{
		XCEPT_RETHROW(toolbox::mem::exception::FailedCreation, "Failed to allocate memory", e);
	}

	try
	{

		//buffer_ = new unsigned char[committedSize];

		// physical, user and kernel address are all the same in this case
		memPartition_.addToPool(buffer_, buffer_, buffer_, committedSize);
	}
	catch (toolbox::mem::exception::Corruption& ce)
	{
		std::stringstream msg;
		msg << "Failed to create committed heap allocator with memory region of " << committedSize << " bytes";
		XCEPT_RETHROW(toolbox::mem::exception::FailedCreation, msg.str(), ce);
	}
	catch (toolbox::mem::exception::InvalidAddress& ia)
	{
		std::stringstream msg;
		msg << "Failed to create committed heap allocator with memory region of " << committedSize << " bytes";
		XCEPT_RETHROW(toolbox::mem::exception::FailedCreation, msg.str(), ia);
	}
	catch (toolbox::mem::exception::MemoryOverflow& mo)
	{
		std::stringstream msg;
		msg << "Failed to create committed heap allocator with memory region of " << committedSize << " bytes";
		XCEPT_RETHROW(toolbox::mem::exception::FailedCreation, msg.str(), mo);
	}
	catch (std::bad_alloc& e)
	{
		std::stringstream msg;
		msg << "Failed to create committed heap allocator with memory region of " << committedSize << " bytes, reason: " << e.what();
		XCEPT_RAISE(toolbox::mem::exception::FailedCreation, msg.str());
	}
}

pt::blit::Allocator::~Allocator ()
{
	delete buffer_;
}

toolbox::mem::Buffer * pt::blit::Allocator::alloc (size_t size, toolbox::mem::Pool * pool) throw (toolbox::mem::exception::FailedAllocation)
{
	if (size > static_cast<size_t>(std::numeric_limits < ssize_t > ::max()))
	{
		std::stringstream msg;
		msg << "Failed to allocate " << size << " bytes from committed region(pt::blit::Allocator) " << committedSize_ << ", maximum allowed is " << std::numeric_limits < ssize_t > ::max();
		XCEPT_RAISE(toolbox::mem::exception::FailedAllocation, msg.str());
	}

	unsigned char* buffer = 0;


	try
	{
		//size_t alignedSize = size + DAT_OPTIMAL_ALIGNMENT;
		size_t alignedSize = size;

		buffer = (unsigned char*) memPartition_.alloc(alignedSize);

		toolbox::mem::Buffer* bufPtr = new pt::blit::Buffer(pool, size, buffer);

		return bufPtr;
	}
	catch (toolbox::mem::exception::FailedAllocation& fa)
	{
		BufferSizeType curalloc, totfree, maxfree;
		BufferSizeType nget, nrel;
		memPartition_.stats(&curalloc, &totfree, &maxfree, &nget, &nrel);
		std::string msg = toolbox::toString("Out of memory in pt::blit::Allocator while allocating %d bytes. Bytes used %d, bytes free %d, max free %d, number allocated %d, number released %d", size, curalloc, totfree, maxfree, nget, nrel);
		XCEPT_RETHROW(toolbox::mem::exception::FailedAllocation, msg, fa);
	}
}

void pt::blit::Allocator::free (toolbox::mem::Buffer * buffer) throw (toolbox::mem::exception::FailedDispose)
{
	try
	{
		memPartition_.free((char*) buffer->getAddress());
		delete buffer;
	}
	catch (toolbox::mem::exception::InvalidAddress& ia)
	{
		// Retry possible, maybe pointer was only wrong, don't delete buffer
		std::string msg = toolbox::toString("Cannot free buffer %x(pt::blit::Allocator), invalid pointer", buffer);
		XCEPT_RETHROW(toolbox::mem::exception::FailedDispose, msg, ia);
	}
	catch (toolbox::mem::exception::Corruption& c)
	{
		delete buffer; // delete in any case, there's a corruption, continuatio is actually not possible anymore
		XCEPT_RETHROW(toolbox::mem::exception::FailedDispose, "Cannot free buffer, memory corruption", c);
	}
}

std::string pt::blit::Allocator::type ()
{
	return "blit";
}

bool pt::blit::Allocator::isCommittedSizeSupported ()
{
	return true;
}

size_t pt::blit::Allocator::getCommittedSize ()
{
	return committedSize_;
}

size_t pt::blit::Allocator::getUsed ()
{
	return memPartition_.getUsed();
}
