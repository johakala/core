// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2015, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, D. Simelevicius                                    *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include "pt/blit/Dispatcher.h"
#include "toolbox/mem/Reference.h"
#include "toolbox/string.h"
#include "xdata/exdr/FixedSizeInputStreamBuffer.h"
#include "xdata/exdr/Serializer.h"
#include "xdaq/ApplicationContext.h"
#include "xdaq/ApplicationRegistry.h"

//#include "interface/shared/i2ogevb2g.h"

#include "toolbox/hexdump.h"
#include "xcept/tools.h"
#include <byteswap.h>
#include "toolbox/task/Guard.h"
#include "pt/blit/Buffer.h"

pt::blit::Dispatcher::Dispatcher(xdaq::Application * owner, int index, size_t maxClients, size_t maxReceiveBuffers, size_t maxBlockSize ):xdaq::Object(owner),
		mutex_(toolbox::BSem::FULL, false),
		maxClients_(maxClients),
		inputPipe_(0),
		index_(index),
		maxReceiveBuffers_(maxReceiveBuffers),
		maxBlockSize_(maxBlockSize)
{

		memPools_.resize(maxClients_);

		for ( std::vector<toolbox::mem::Pool *>::iterator j = memPools_.begin(); j != memPools_.end(); j++ )
		{
			(*j) = 0;
		}

		std::stringstream iname;
		iname << "socket-" << index_;
		size_t maxPoolSize = (maxReceiveBuffers + 1) * maxBlockSize; // one additional block for bookeeping
		recvPoolManager_ = new pt::blit::PoolManager(maxPoolSize, iname.str(), maxClients_);

		size_t inputPipeSize = maxReceiveBuffers_ * maxClients_; // can keep all buffers from all connected input

		std::cout << "create rlist of size  " << inputPipeSize << std::endl;
		std::stringstream ss;
		ss << "pt-blit-inputpipe-rlist-" << index;
		iqueue_ = toolbox::rlist<BulkTransferEvent>::create(ss.str(), inputPipeSize);

}


pt::blit::Dispatcher::~Dispatcher()
{
}



void pt::blit::Dispatcher::processIncomingMessage (toolbox::mem::Reference* ref, size_t len, int sid) throw (pt::blit::exception::Exception)

//void pt::blit::Dispatcher::processIncomingMessage (toolbox::mem::Reference* ref, int index, int sid, tcpla::EndPoint & ep_handle, size_t len) throw (pt::blit::exception::Exception)
{
	toolbox::task::Guard < toolbox::BSem > guard(mutex_);
	//std::cout << "received index" << index << " sid " << sid << " size " << len <<  std::endl;
	if ( inputPipe_ == 0 )
	{
		std::stringstream msg;
		//msg << "No input pipe  allocated for sid " << sid << " index " << index ;
		msg << "No input pipe  allocated for sid " << sid;

		XCEPT_RAISE(pt::blit::exception::Exception, msg.str());
	}

	ref->setDataSize(len);

	try
	{
		inputPipe_->enqueue(ref,sid);
	}
	catch ( pt::exception::Exception & ex )
	{
		std::stringstream msg;
		msg << "No input pipe  allocated for sid " << sid;
		XCEPT_RAISE(pt::blit::exception::Exception, msg.str());
		return;
	}

}

void pt::blit::Dispatcher::connectionClosedByPeer( int index, int sid, int fd, tcpla::EndPoint & ep_handle) throw (pt::blit::exception::Exception)
{
	toolbox::task::Guard < toolbox::BSem > guard(mutex_);
	//std::cout << "Peer IP address:" << ipstr << " Port:" << port <<  std::endl;int index, int sid, const std::string & peername, size_t port
	if (inputPipe_!= 0 )
	{
			pt::blit::PipeConnectionClosedByPeer  pca(ep_handle, index, sid);
			inputPipe_->fireConnectionClosedByPeer(pca);
	}
}

void pt::blit::Dispatcher::connectionResetByPeer( int index, int sid, int fd, tcpla::EndPoint & ep_handle) throw (pt::blit::exception::Exception)
{
	toolbox::task::Guard < toolbox::BSem > guard(mutex_);
	//std::cout << "Peer IP address:" << ipstr << " Port:" << port <<  std::endl;int index, int sid, const std::string & peername, size_t port
	if (inputPipe_!= 0 )
	{
			pt::blit::PipeConnectionResetByPeer  pca(ep_handle, index, sid);
			inputPipe_->fireConnectionResetByPeer(pca);
	}
}


void pt::blit::Dispatcher::connectionAccepted( int index, int sid, int fd, tcpla::EndPoint & ep_handle) throw (pt::blit::exception::Exception)
{
	toolbox::task::Guard < toolbox::BSem > guard(mutex_);

	LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), "Establishing connection for receiver");

	if (inputPipe_ == 0 )
	{
		std::stringstream msg;
		msg << "Channel assignemnt has failed, no input pipe present";
		XCEPT_RAISE(pt::blit::exception::Exception, msg.str());
	}

	if ( (size_t)sid >  maxClients_ )
	{
		std::stringstream msg;
		msg << "Failed to hash sid " << sid << " (max allowed " << maxClients_ << ")";
		XCEPT_RAISE(pt::blit::exception::Exception, msg.str());
	}

	memPools_[sid] = recvPoolManager_->getPool(sid);

	for ( size_t k = 0; k < maxReceiveBuffers_; k++ )
	{
		toolbox::mem::Reference * reference;


		try
		{
			reference = toolbox::mem::getMemoryPoolFactory()->getFrame(memPools_[sid], maxBlockSize_); // 256KB max size for i2o messages
		}
		catch ( toolbox::mem::exception::Exception & ex )
		{
			std::stringstream msg;
			msg << "Failed to allocate buffer for incoming message";
			XCEPT_RETHROW(pt::blit::exception::Exception, msg.str(), ex);
		}
		tcpla::TCPLA_CONTEXT cookie;
		cookie.as_ptr = reference; // rememeber which reference

		pt::blit::Buffer * buffer = 0;
		try
		{
			buffer = dynamic_cast<pt::blit::Buffer*>(reference->getBuffer());
			if (buffer == 0)
			{
				std::stringstream msg;
				msg << "Failed to cast, invalid memory allocator/pool, expected pt::blit memory pool for sid " << sid << " index " << index;
				XCEPT_RAISE(pt::blit::exception::Exception, msg.str());
			}
		}
		catch (std::bad_cast & e)
		{
			std::stringstream msg;
			msg << "Failed to cast, invalid memory allocator/pool, expected pt::blit memory pool for sid " << sid << " index " << index;
			XCEPT_RAISE(pt::blit::exception::Exception, msg.str());
		}

		buffer->ep_handle_ = ep_handle;
		buffer->sid_ = sid;
		buffer->index_  = index;



		try
		{
			ep_handle.psp_->postRecvBuffer(ep_handle,cookie, (char *) reference->getDataLocation(), reference->getBuffer()->getSize());
		}
		catch ( tcpla::exception::FailedToPostSend & e)
		{
			reference->release();
			std::stringstream msg;
			msg << "Failed to provide buffer for incoming message";
			XCEPT_RETHROW(pt::blit::exception::Exception, msg.str(), e);

		}
		catch (tcpla::exception::InvalidEndPoint & e)
		{
			reference->release();
			std::stringstream msg;
			msg << "Failed to provide buffer for incoming message";
			XCEPT_RETHROW(pt::blit::exception::Exception, msg.str(), e);
		}

	}


	socklen_t len;
	struct sockaddr_storage addr;
	char ipstr[INET6_ADDRSTRLEN];
	int port;

	len = sizeof addr;
	::getpeername(fd, (struct sockaddr*)&addr, &len);

	// deal with both IPv4 and IPv6:
	if (addr.ss_family == AF_INET)
	{
	    struct sockaddr_in *s = (struct sockaddr_in *)&addr;
	    port = ntohs(s->sin_port);
	    inet_ntop(AF_INET, &s->sin_addr, ipstr, sizeof ipstr);
	}
	else
	{ // AF_INET6
	    struct sockaddr_in6 *s = (struct sockaddr_in6 *)&addr;
	    port = ntohs(s->sin6_port);
	    inet_ntop(AF_INET6, &s->sin6_addr, ipstr, sizeof ipstr);
	}

	//std::cout << "Peer IP address:" << ipstr << " Port:" << port <<  std::endl;int index, int sid, const std::string & peername, size_t port
	if (inputPipe_!= 0 )
	{
		pt::blit::PipeConnectionAccepted  pca( ep_handle,index,sid,ipstr,port);
		inputPipe_->fireConnectionAccepted(pca);

	}

	return;
}

void pt::blit::Dispatcher::connectionRequest( tcpla::InterfaceAdapter * ia, tcpla::PublicServicePoint * psp, int cr_handle) throw (pt::blit::exception::Exception)
{

	toolbox::task::Guard < toolbox::BSem > guard(mutex_);


	tcpla::EndPoint ep_handle = ia->createEndPoint();

	try
	{
		tcpla::TCPLA_CONTEXT cookie;
		cookie.as_ptr = 0;

		psp->accept(ep_handle, cr_handle, cookie);
	}
	catch ( pt::blit::exception::Exception & e )
	{
		ia->destroyEndPoint(ep_handle);

		std::stringstream msg;
		msg << "Failed to accept connection";
		XCEPT_RETHROW(pt::blit::exception::Exception, msg.str(), e);
	}

}

pt::blit::InputPipe *  pt::blit::Dispatcher::createInputPipe(pt::blit::PipeAdvertisement & adv, pt::blit::InputPipeListener * listener)
	throw (pt::blit::exception::Exception)
{
	toolbox::task::Guard < toolbox::BSem > guard(mutex_);

	pt::blit::InputPipe * ip = new pt::blit::InputPipe(adv, iqueue_, listener);

	inputPipe_ = ip;

	return ip;


}

void pt::blit::Dispatcher::destroyInputPipe(pt::blit::InputPipe * ip) throw (pt::blit::exception::Exception)
{
	toolbox::task::Guard < toolbox::BSem > guard(mutex_);

	inputPipe_ = 0;

	ip->close();

	delete ip;

}

