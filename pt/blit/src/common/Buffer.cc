// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2015, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, D. Simelevicius                                    *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/
#include "pt/blit/Buffer.h"

#include <stdio.h>
#include <string.h>

pt::blit::Buffer::Buffer ( toolbox::mem::Pool * pool, size_t size, void* address)
	: toolbox::mem::Buffer(pool, size, address)
{
	 sid_ = -1;
	 index_ = -1;
}

