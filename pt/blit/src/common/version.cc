// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2015, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, D.Simelevicius					 *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include "pt/blit/version.h"

#include <string>
#include <set>

GETPACKAGEINFO (ptblit)

void ptblit::checkPackageDependencies () throw ( config::PackageInfo::VersionException )
{
}

std::set<std::string, std::less<std::string> > ptblit::getPackageDependencies ()
{
	std::set<std::string, std::less<std::string> > dependencies;
	return dependencies;
}
