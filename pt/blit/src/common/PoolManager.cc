// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2015, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, D. Simelevicius                                    *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include "pt/blit/PoolManager.h"

#include "pt/blit/Allocator.h"
#include "toolbox/mem/MemoryPoolFactory.h"

pt::blit::PoolManager::PoolManager (size_t psize, const std::string & tag, size_t maxFD)
	: psize_(psize), tag_(tag)
{
	usedPools_.resize(maxFD);
	for (std::vector<toolbox::mem::Pool *>::iterator j = usedPools_.begin(); j != usedPools_.end(); j++)
	{
		(*j) = 0;
	}
}

toolbox::mem::Pool * pt::blit::PoolManager::getPool (int fd)
{
	if (usedPools_[fd] == 0 )
	{
		std::stringstream ss;
		ss << "toolbox-mem-pool-blit-" << tag_ << "-" << fd;
		toolbox::net::URN urn(ss.str(), "blit");

		std::stringstream sm;
		sm << "toolbox-mem-allocator-blit-" << tag_ << "-" << fd;

		pt::blit::Allocator* a = new pt::blit::Allocator(sm.str(), psize_);
		usedPools_[fd] = toolbox::mem::getMemoryPoolFactory()->createPool(urn, a);

		usedPools_[fd]->setHighThreshold((unsigned long) (psize_ * 0.9));
		usedPools_[fd]->setLowThreshold ((unsigned long) (psize_ * 0.5));
	}

	return usedPools_[fd];
}
