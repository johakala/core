// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2015, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, D. Simelevicius                                    *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include <byteswap.h>

#include "pt/blit/btester.h"

#include "xdaq/Application.h"
#include "xdaq/ApplicationContext.h"
#include "xdaq/ApplicationGroup.h"
#include "toolbox/PerformanceMeter.h"
#include "xdaq/NamespaceURI.h"

#include "toolbox/BSem.h"

#include "toolbox/rlist.h"
#include "toolbox/string.h"

#include "toolbox/mem/Reference.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "toolbox/mem/Pool.h"
#include "toolbox/mem/CommittedHeapAllocator.h"

#include "toolbox/task/TimerTask.h"
#include "toolbox/task/Timer.h"
#include "toolbox/task/TimerFactory.h"


#include "xdata/UnsignedLong.h"
#include "xdata/Double.h"
#include "xdata/Boolean.h"
#include "xdata/ActionListener.h"


#include "xcept/Exception.h"
#include "xcept/tools.h"


#include "toolbox/task/WorkLoopFactory.h"

#include "pt/blit/PipeService.h"
#include "pt/PeerTransportAgent.h"

XDAQ_INSTANTIATOR_IMPL(pt::blit::btester)


pt::blit::btester::btester(xdaq::ApplicationStub* c)
	throw(xdaq::exception::Exception)
	: xdaq::Application(c), mutex_(toolbox::BSem::FULL)
{

	xgi::bind(this, &pt::blit::btester::start, "start");
	xgi::bind(this, &pt::blit::btester::stop, "stop");


	this->getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");

	getApplicationInfoSpace()->fireItemAvailable("sampleTime", &sampleTime_);


	this->counter_ = 0;

	process_ = toolbox::task::bind(this, &btester::process, "process");

	pt::blit::PipeService * ps = dynamic_cast<pt::blit::PipeService *>(pt::getPeerTransportAgent()->getPeerTransport("btcp","blit",pt::SenderReceiver));
	ps->addPipeServiceListener(this);

}

void pt::blit::btester::start ( xgi::Input * in, xgi::Output * out ) throw ( xgi::exception::Exception )
{
	mutex_.take();

	pt::blit::PipeService * ps = dynamic_cast<pt::blit::PipeService *>(pt::getPeerTransportAgent()->getPeerTransport("btcp","blit",pt::SenderReceiver));

	std::list<PipeAdvertisement> advs =  ps->getPipeAdvertisements("blit", "btcp");
	for (std::list<PipeAdvertisement>::iterator i =  advs.begin(); i != advs.end(); i++ )
	{
		std::cout << " creating ipipe for PSP index '" << (*i).getIndex() <<  "'" << std::endl;

		ipipes_.push_back(ps->createInputPipe((*i), this));
	}

	mutex_.give();

	try
	{

		toolbox::task::getWorkLoopFactory()->getWorkLoop("blit-worker", "waiting")->activate(); //add option to turn off for testing
		toolbox::task::getWorkLoopFactory()->getWorkLoop("blit-worker", "waiting")->submit(process_);
	}
	catch ( toolbox::task::exception::Exception& e )
	{
		std::stringstream ss;
		ss << "Failed to submit workloop "	;

		std::cerr << ss.str() << std::endl;
	}
	catch ( std::exception& se )
	{
		std::stringstream ss;
		ss << "Failed to submit notification to worker thread, caught standard exception '";
		ss << se.what() << "'";
		std::cerr << ss.str() << std::endl;
	}
	catch ( ... )
	{
		std::cerr << "Failed to submit notification to worker pool, caught unknown exception" << std::endl;
	}
}

void pt::blit::btester::stop  ( xgi::Input * in, xgi::Output * out ) throw ( xgi::exception::Exception )
{

	toolbox::task::getWorkLoopFactory()->getWorkLoop("blit-worker", "waiting")->cancel();

	//::sleep(2);

	mutex_.take();

	pt::blit::PipeService * ps = dynamic_cast<pt::blit::PipeService *>(pt::getPeerTransportAgent()->getPeerTransport("btcp","blit",pt::SenderReceiver));
	for (std::vector<pt::blit::InputPipe *>::iterator i =  ipipes_.begin(); i != ipipes_.end(); i++ )
	{
		ps->destroyInputPipe(*i);
	}

	ipipes_.clear();

	if ( ! ipipes_.empty() )
	{
		std::cerr << "HELP the pipe vector is still not empy!!!!" << std::endl;
	}

	mutex_.give();

}
		
void pt::blit::btester::actionPerformed( xdata::Event& event)
{

	if (event.type() == "urn:xdaq-event:setDefaultValues")
	{
		try
		{

			toolbox::task::Timer * timer = 0;
			if (!toolbox::task::getTimerFactory()->hasTimer("urn:benchmark:btester-timer"))
			{
				timer = toolbox::task::getTimerFactory()->createTimer("urn:benchmark:btester-timer");
			}
			else
			{
				timer = toolbox::task::getTimerFactory()->getTimer("urn:benchmark:btester-timer");
			}

			toolbox::TimeVal startTime;
			startTime = toolbox::TimeVal::gettimeofday();
			toolbox::TimeInterval interval;
			interval.fromString(sampleTime_.toString()); // in seconds
			lastTime_ = toolbox::TimeVal::gettimeofday();

			timer->scheduleAtFixedRate(startTime, this, interval,  0, "ServerCheckRateTimer" );
		}
		catch (toolbox::task::exception::Exception& te)
		{
			std::cout <<  "Cannot run timer" <<  xcept::stdformat_exception_history(te) << std::endl;
		}

		// autostart at load
		this->start(0,0);

	}

}
 	
bool pt::blit::btester::process (toolbox::task::WorkLoop* wl)
{

	// tested with iperf -c 10.176.37.220 -p 30010 -i 10  -f M -t 1000 -l 1M

	/*for(;;)
	{*/
		mutex_.take();

		for (std::vector<pt::blit::InputPipe *>::iterator i =  ipipes_.begin(); i != ipipes_.end(); i++ )
		{
			pt::blit::BulkTransferEvent event;
			//event.ref = 0xffffffff;
			//event.sid = -1;
			if ( (*i)->dequeue(&event) )
			{
				size_t size = event.ref->getDataSize();
				counter_ += size;

				(*i)->grantBuffer(event.ref);
			}
		}
		mutex_.give();
	/*}*/

	return true;
}

void pt::blit::btester::pipeServiceEvent(pt::blit::PipeAdvertisement  & adv)
{
	// used for autostart
	mutex_.take();

	pt::blit::PipeService * ps = dynamic_cast<pt::blit::PipeService *>(pt::getPeerTransportAgent()->getPeerTransport("btcp","blit",pt::SenderReceiver));
	std::cout << " received advertisement for PSP index, create input pipe'" << adv.getIndex() <<  "'" << std::endl;
	ipipes_.push_back(ps->createInputPipe(adv, this));

	mutex_.give();
}

void pt::blit::btester::connectionAcceptedEvent(pt::blit::PipeConnectionAccepted & pca)
{
	mutex_.take();
	std::cout << "connection accepted from peer :" << pca.getPeerName() << " port:" << pca.getPort() << " sid:" << pca.getSID() << " index: " << pca.getPipeIndex() <<  std::endl;
	mutex_.give();
}

void pt::blit::btester::connectionClosedByPeerEvent(pt::blit::PipeConnectionClosedByPeer & pca)
{
	mutex_.take();
	std::cout << "connection closed by  peer :"  << " sid:" << pca.getSID() << " index: " << pca.getPipeIndex() <<  std::endl;
	mutex_.give();
}

void pt::blit::btester::connectionResetByPeerEvent(pt::blit::PipeConnectionResetByPeer & pca)
{
	mutex_.take();
	std::cout << "connection reset by  peer :"  << " sid:" << pca.getSID() << " index: " << pca.getPipeIndex() <<  std::endl;
	mutex_.give();
}


void pt::blit::btester::timeExpired (toolbox::task::TimerEvent& e)
{
	mutex_.take();

	toolbox::TimeVal now = toolbox::TimeVal::gettimeofday();
	double delta = (double)now - (double)lastTime_;

	double bandwidth = counter_ / delta;

	lastTime_ =  now;

	counter_ = 0;
	std::cout << "measured bandwidth:" << (bandwidth / 0x100000 )  << std::endl;

	mutex_.give();
}



