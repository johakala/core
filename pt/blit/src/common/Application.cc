 // $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2015, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, D. Simelevicius                                    *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include "pt/blit/Application.h"

#include "toolbox/string.h"

#include "xgi/framework/Method.h"
#include "xgi/Method.h"
#include "i2o/Method.h"
#include "i2o/utils/AddressMap.h"
#include "pt/PeerTransportAgent.h"
#include "xoap/Method.h"

#include "xdaq/NamespaceURI.h"

#include "xoap/MessageReference.h"
#include "xoap/MessageFactory.h"
#include "xoap/SOAPPart.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPBody.h"
#include "xoap/domutils.h"
#include "xcept/tools.h"

#include "toolbox/task/TimerFactory.h"
#include "toolbox/task/WorkLoopFactory.h"

#include "tcpla/InterfaceAdapter.h"
#include "tcpla/PublicServicePoint.h"
#include "tcpla/WaitingWorkLoop.h"

#include "toolbox/mem/Reference.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "toolbox/mem/Pool.h"
#include "toolbox/mem/HeapAllocator.h"

#include "pt/blit/PeerTransport.h"
#include "pt/blit/Dispatcher.h"
#include "pt/blit/EventHandler.h"
//#include "pt/blit/Proxy.h"

#include "tcpla/InputIndexGenerator.h"
#include <math.h>

XDAQ_INSTANTIATOR_IMPL (pt::blit::Application)

pt::blit::Application::Application (xdaq::ApplicationStub* stub) throw (xdaq::exception::Exception)
	: xdaq::Application(stub), xgi::framework::UIManager(this)
{
	stub->getDescriptor()->setAttribute("icon", "/pt/blit/images/pt-blit-icon.jpg");
	stub->getDescriptor()->setAttribute("icon16x16", "/pt/blit/images/pt-blit-icon.jpg");
	// Bind CGI callbacks
	xgi::framework::deferredbind(this, this, &pt::blit::Application::Default, "Default");


	this->maxClients_ = 1024;
	this->protocol_ = BLIT_PROTOCOL_NAME;
	this->maxReceiveBuffers_ = 32;
	this->maxBlockSize_ = 0x8000; // 32KB

	getApplicationInfoSpace()->fireItemAvailable("maxClients", &maxClients_);
	getApplicationInfoSpace()->fireItemAvailable("maxReceiveBuffers", &maxReceiveBuffers_);
	getApplicationInfoSpace()->fireItemAvailable("maxBlockSize", &maxBlockSize_);

	getApplicationInfoSpace()->fireItemAvailable("protocol", &protocol_);


	// Bind setting of default parameters
	getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");

	tcpla::InputIndexGenerator::getInstance(); // this is to trigger the construction in a single thread of control
}


void pt::blit::Application::actionPerformed ( xdata::Event& e )
{

	if ( e.type() == "urn:xdaq-event:setDefaultValues" )
	{

		pt_ = 0;

		try
		{
			pt_ = new pt::blit::PeerTransport(this, (xdata::UnsignedIntegerT) maxClients_, (xdata::UnsignedIntegerT) maxReceiveBuffers_, (xdata::UnsignedIntegerT) maxBlockSize_, protocol_);

		}
		catch ( pt::exception::Exception & e )
		{
			this->notifyQualified("fatal", e);
			return;
		}


		pt::PeerTransportAgent* pta = pt::getPeerTransportAgent();
		pta->addPeerTransport(pt_);


	}
}

void pt::blit::Application::Default ( xgi::Input * in, xgi::Output * out ) throw ( xgi::exception::Exception )
{
	// Begin of tabs
	*out << "<div class=\"xdaq-tab-wrapper\" id=\"tabPane1\">" << std::endl;

	// Tab 1
	*out << "<div class=\"xdaq-tab\" title=\"Output\">" << std::endl;
	this->SenderTabPage(out);
	*out << "</div>";

	// Tab 2
	*out << "<div class=\"xdaq-tab\" title=\"Input\">" << std::endl;
	this->ReceiverTabPage(out);
	*out << "</div>";

	// Tab 3
	*out << "<div class=\"xdaq-tab\" title=\"Settings\">" << std::endl;
	this->SettingsTabPage(out);
	*out << "</div>";

	// Tab 4
	*out << "<div class=\"xdaq-tab\" title=\"Events\">" << std::endl;
	this->EventsTabPage(out);
	*out << "</div>";


	*out << "</div>"; // end of tab pane
}

void pt::blit::Application::SenderTabPage ( xgi::Output * out )
{
	for ( std::map<std::string, tcpla::InterfaceAdapter *>::iterator i = pt_->iav_.begin(); i != pt_->iav_.end(); i++ )
	{
		*out << (*(*i).second) << std::endl;
	}
}

void pt::blit::Application::ReceiverTabPage ( xgi::Output * out )
{
	for ( size_t i = 0; i < pt_->psp_.size(); i++ )
	{
		*out << (*pt_->psp_[i]);
	}
}

void pt::blit::Application::SettingsTabPage ( xgi::Output * out )
{
	*out << "<style>caption { text-align: left !important; }</style>" << std::endl;
	//psp / ia settings
	*out << cgicc::table().set("class", "xdaq-table") << std::endl;
	*out << cgicc::caption("General Settings")  << std::endl;

	*out << cgicc::thead();
	*out << cgicc::tr();
	*out << cgicc::th("Name");
	*out << cgicc::th("Type");
	*out << cgicc::th("Value");
	*out << cgicc::th("Description");
	*out << cgicc::tr() << std::endl;
	*out << cgicc::thead();

	*out << cgicc::tbody() << std::endl;



	*out << cgicc::tbody() << std::endl;
	*out << cgicc::table() << std::endl;

	//Input / Output Settings
	*out << cgicc::table().set("class", "xdaq-table") << std::endl;
        *out << cgicc::caption("Input / Output Settings")  << std::endl;	

	*out << cgicc::thead();
	*out << cgicc::tr();
	*out << cgicc::th("Name");
	*out << cgicc::th("Type");
	*out << cgicc::th("Value");
	*out << cgicc::th("Description");
	*out << cgicc::tr() << std::endl;
	*out << cgicc::thead();

	*out << cgicc::tbody() << std::endl;
	*out << cgicc::tr();
	*out << cgicc::td("maxReceiveBuffers");
	*out << cgicc::td(maxReceiveBuffers_.type());
	*out << cgicc::td(maxReceiveBuffers_.toString());
	*out << cgicc::td("The number of blocks per receiver socket.");
	*out << cgicc::tr() << std::endl;



	*out << cgicc::tr();
	*out << cgicc::td("maxClients");
	*out << cgicc::td(maxClients_.type() );
	*out << cgicc::td(maxClients_.toString());
	*out << cgicc::td("The maximum number of connections.");
	*out << cgicc::tr() << std::endl;

	*out << cgicc::tr();
	*out << cgicc::td("maxBlockSize_");
	*out << cgicc::td(maxBlockSize_.type() );
	*out << cgicc::td(maxBlockSize_.toString());
	*out << cgicc::td("The maximum receive buffer block size.");
	*out << cgicc::tr() << std::endl;



	*out << cgicc::tbody() << std::endl;
	*out << cgicc::table();


}

void pt::blit::Application::EventsTabPage ( xgi::Output * out )
{
	*out << (*pt_) << std::endl;
}



