// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2015, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, D. Simelevicius                                    *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/


#include "pt/blit/PeerTransport.h"
#include "pt/blit/HeaderInfo.h"

#include "tcpla/PublicServicePoint.h"
#include "tcpla/InterfaceAdapterPoll.h"
#include "tcpla/InterfaceAdapterSelect.h"
#include "tcpla/Address.h"
#include "tcpla/Utils.h"

#include "toolbox/mem/MemoryPoolFactory.h"
#include "pt/exception/ReceiveFailure.h"
#include "i2o/i2o.h"
#include "xcept/tools.h"

#include "toolbox/hexdump.h"


pt::blit::PeerTransport::PeerTransport ( xdaq::Application * owner,  size_t maxClients, size_t maxReceiveBuffers, size_t maxBlockSize, const std::string & protocol )
	: xdaq::Object(owner), mutex_(toolbox::BSem::FULL)
{
	//this->ioQueueSize_  = ioQueueSize;
	//this->eventQueueSize_  = eventQueueSize;
	this->maxClients_ = maxClients;
	this->protocol_ = protocol;
	this->maxReceiveBuffers_ = maxReceiveBuffers;
	this->maxBlockSize_ = maxBlockSize;
	pipeServiceListener_ = 0;

	handler_ = new pt::blit::EventHandler(this->getOwnerApplication(), this->maxClients_ , 4); // max 4 PSPS receivers

}

pt::blit::PeerTransport::~PeerTransport ()
{
	for ( std::map<std::string, tcpla::InterfaceAdapter *>::iterator i = iav_.begin(); i != iav_.end(); i++ )
	{
		delete (*i).second;
		iav_.erase(i);
	}

}


pt::Messenger::Reference pt::blit::PeerTransport::getMessenger ( pt::Address::Reference destination, pt::Address::Reference local ) throw ( pt::exception::UnknownProtocolOrService )
{
	std::stringstream msg;
	msg << "Messenger creation is not supported";
	XCEPT_RAISE(pt::exception::UnknownProtocolOrService, msg.str());

}


tcpla::InterfaceAdapter * pt::blit::PeerTransport::getInterfaceAdapter (pt::Address::Reference address, pt::blit::EventHandler * eh) throw (pt::blit::exception::Exception)
{
	tcpla::Address & a = dynamic_cast<tcpla::Address &>(*address);
	tcpla::InterfaceAdapter * ia = 0;

	std::string id = a.getProperty("hostname");
	std::string service = a.getProperty("service");
	std::string smode = a.getProperty("smode");

	if ( iav_.find(id) != iav_.end() )
	{
		LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), "retrieving cached interface adapter for host " << id);
		ia = iav_[id];
	}
	else
	{
		try
		{
			if ( a.hasProperty("maxbulksize") )
			{

				try
				{
					pt::blit::HeaderInfoFD::fdLength = (size_t) toolbox::toUnsignedLong(a.getProperty("maxbulksize"));

					if (pt::blit::HeaderInfoFD::fdLength > maxBlockSize_)
					{

						XCEPT_RAISE(pt::blit::exception::Exception, "Failed to set bulksize size, receiver block size 'maxBlockSize' too small ");

					}

				}
				catch (toolbox::exception::Exception & e)
				{
					XCEPT_RETHROW(pt::blit::exception::Exception, "Failed to set fixed datagram size, invalid size_t value provided in EndPoint configutation", e);
				}

				// maxClients_ + 1 enable 32 connections see ticket https://svnweb.cern.ch/trac/cmsos/ticket/3541
				if ( smode == "poll")
				{
					ia = new tcpla::InterfaceAdapterPoll(this->getOwnerApplication(), address, eh, maxClients_ + 1, maxReceiveBuffers_, maxReceiveBuffers_ * maxClients_, &pt::blit::HeaderInfoFD::getHeaderSize, &pt::blit::HeaderInfoFD::getLength); //new listening socket on config
				}
				else if( smode == "select")
				{
					ia = new tcpla::InterfaceAdapterSelect(this->getOwnerApplication(), address, eh, maxClients_ + 1, maxReceiveBuffers_, maxReceiveBuffers_ * maxClients_, &pt::blit::HeaderInfoFD::getHeaderSize, &pt::blit::HeaderInfoFD::getLength); //new listening socket on config
				}
				else
				{
					XCEPT_RAISE(pt::blit::exception::Exception, "invalid mode");
				}
			}
			else
			{

					XCEPT_RAISE(pt::blit::exception::Exception, "missing maxbulksize attribute in endpoint ");

			}
		}
		catch ( pt::blit::exception::Exception & e )
		{
			XCEPT_RETHROW(pt::blit::exception::Exception, "Failed to create interface adapter", e);
		}

		iav_[id] = ia;
	}

	return ia;
}

pt::TransportType pt::blit::PeerTransport::getType ()
{
	return pt::SenderReceiver;
}

pt::Address::Reference pt::blit::PeerTransport::createAddress ( const std::string& url, const std::string& service ) throw ( pt::exception::InvalidAddress )
{
	std::map<std::string, std::string, std::less<std::string> > plist;

	toolbox::net::URL urltmp(url);

	plist["protocol"] = urltmp.getProtocol();
	plist["service"] = service;
	plist["hostname"] = urltmp.getHost();
	plist["port"] = toolbox::toString("%d", urltmp.getPort());

	for (std::map<std::string, std::string>::iterator i = plist.begin(); i != plist.end(); i++)
	{
		std::cout << "blit address " << i->first << ": " << i->second << std::endl;
	}

	return this->createAddress(plist);
}

pt::Address::Reference pt::blit::PeerTransport::createAddress ( std::map<std::string, std::string, std::less<std::string> >& address ) throw ( pt::exception::InvalidAddress )
{

	std::string protocol = address["protocol"];

	if ( protocol == this->protocol_ )
	{
		//port scanning here
		return pt::Address::Reference(new tcpla::Address(address));
	}
	else
	{
		std::string msg = "Cannot create address, protocol not supported: ";
		msg += protocol;
		XCEPT_RAISE(pt::exception::InvalidAddress, msg);
	}
}

std::string pt::blit::PeerTransport::getProtocol ()
{
	return this->protocol_;
}

void pt::blit::PeerTransport::config ( pt::Address::Reference address ) throw ( pt::exception::Exception )
{
	tcpla::PublicServicePoint* psp = 0;
	try
	{
		mutex_.take();

		//tcpla::Address & a = dynamic_cast<tcpla::Address &>(*address);
		tcpla::InterfaceAdapter * ia = this->getInterfaceAdapter(address, handler_);

		psp = ia->listen(address);
		psp_.push_back(psp);
		//size_t maxBlockSize = pt::blit::HeaderInfoFD::fdLength; // this one used for the transfer
		handler_->setDispatcher (psp->indexId_, new pt::blit::Dispatcher(this->getOwnerApplication(), psp->indexId_, maxClients_,  maxReceiveBuffers_, maxBlockSize_));

		mutex_.give();

		pt::blit::PipeAdvertisement advertisement (psp);
		if ( pipeServiceListener_ != 0 )
		{
			pipeServiceListener_->pipeServiceEvent(advertisement);
		}
		else
		{
			std::cout << "no listener for Pipe Services" << std::endl;
		}

	}
	catch ( pt::blit::exception::Exception &e )
	{
		mutex_.give();
		XCEPT_RETHROW(pt::exception::Exception, "Failed to configure receiver port in IA", e);

	}
}

std::vector<std::string> pt::blit::PeerTransport::getSupportedServices ()
{

	std::vector < std::string > s;

	s.push_back(BLIT_SERVICE_NAME);

	return s;
}

bool pt::blit::PeerTransport::isServiceSupported ( const std::string& service )
{
	return BLIT_SERVICE_NAME == service;
}

void pt::blit::PeerTransport::addServiceListener ( pt::Listener* listener ) throw ( pt::exception::Exception )
{

	XCEPT_RAISE(pt::exception::Exception, "API not supported");
}

void pt::blit::PeerTransport::removeServiceListener ( pt::Listener* listener ) throw ( pt::exception::Exception )
{

	XCEPT_RAISE(pt::exception::Exception, "API not supported");
}

void pt::blit::PeerTransport::removeAllServiceListeners ()
{
	// ignore

}
// pipe service support
pt::blit::InputPipe * pt::blit::PeerTransport::createInputPipe(pt::blit::PipeAdvertisement & adv, pt::blit::InputPipeListener * listener ) throw (pt::blit::exception::Exception)
{
	return handler_->getDispatcher(adv.getIndex())->createInputPipe(adv, listener);
}

void pt::blit::PeerTransport::destroyInputPipe(pt::blit::InputPipe * ip) throw (pt::blit::exception::Exception)
{
	return  handler_->getDispatcher(ip->getAdvertisement().getIndex())->destroyInputPipe(ip);
}

void pt::blit::PeerTransport::addPipeServiceListener(pt::blit::PipeServiceListener * listener)
{
	pipeServiceListener_ = listener;
}

std::list<pt::blit::PipeAdvertisement> pt::blit::PeerTransport::getPipeAdvertisements(const std::string & service, const std::string & protocol)
{
	std::list<pt::blit::PipeAdvertisement> advs;
	mutex_.take();
	for (std::vector<tcpla::PublicServicePoint*>::iterator i = psp_.begin(); i != psp_.end(); i++ )
	{
		pt::blit::PipeAdvertisement  adv(*i);
		advs.push_back(adv);
	}

	mutex_.give();
	return advs;
}

namespace pt
{
	namespace blit
	{
		std::ostream& operator<< ( std::ostream &sout, pt::blit::PeerTransport & pt )
		{
			pt.mutex_.take();

			sout << (*(pt.handler_));

			pt.mutex_.give();

			return sout;
		}
	}
}
