// $Id: FillFifo.h,v 1.3 2008/07/18 15:27:15 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _FifoFull_h_
#define _FifoFull_h_

#include "xdaq.h"

#include <list>
#include <algorithm>
#include <iostream>
#include <strstream>
#include <string>

// Example:
//
// One object fills the FIFO and causes an
// exception. Another will empty it then and
// the test starts over.
//

#include "i2o.h"
#include "i2oListener.h"
#include "xdaqIOException.h"

//interface code (user defined)
#define I2O_FIFO_FULL_CLASSID  0x5

// callback binding (user defined)
#define I2O_TOKEN_CODE 0x0001

typedef struct _I2O_TOKEN_MESSAGE_FRAME 
{
	I2O_PRIVATE_MESSAGE_FRAME PvtMessageFrame;
} I2O_TOKEN_MESSAGE_FRAME, *PI2O_TOKEN_MESSAGE_FRAME;

class MsgListener: public i2oListener
{
	public:
		MsgListener()
		{
			i2oBindMethod (this, 
                  		 &MsgListener::token, 
                   		I2O_TOKEN_CODE,
                  		 XDAQ_ORGANIZATION_ID, 0);
		}
		
		virtual void token (BufRef * ref ) = 0;
};

class FillFifo: public xdaqApplication, public MsgListener, public Task
{	
	public:

	FillFifo () 
	{ 
		
		   
		counter_ = 0;
		exportParam("counter",counter_);
		destination_ = xdaq::getTid ("FillFifo", 1);		
	}
	
	void Configure() throw (xdaqException)
	{	 	
	}
	
	
	virtual void token (BufRef* ref)
	{	
		if  (instance_ == 1)
		{
			// cout << "Received a frame." << endl;
			xdaq::frameFree(ref);		
		} else 
		{
			cout << "Error. Instance 0 should never receive this message." << endl;
		}
	}
	
	
	
	void Enable() throw (xdaqException)
	{
		this->activate();
	}
	
	int svc()
	{
		cout << "Service routine" << endl;
		this->sleep(2);
		cout << "Starting to send" << endl;
		
		if (instance_ == 0)
		{
		int size = sizeof (I2O_TOKEN_MESSAGE_FRAME);
		BufRef* ref;
		int sent = 0;
		for (;;)
		{
			
			try 
			{
			    // allocate frame 
			    ref = xdaq::frameAlloc(size);

			    // prepare frame and send
			    PI2O_TOKEN_MESSAGE_FRAME frame = (PI2O_TOKEN_MESSAGE_FRAME) ref->buf_;   

			    frame->PvtMessageFrame.StdMessageFrame.MsgFlags         = 0;
			    frame->PvtMessageFrame.StdMessageFrame.VersionOffset    = 0;
			    frame->PvtMessageFrame.StdMessageFrame.TargetAddress    = destination_;
			    frame->PvtMessageFrame.StdMessageFrame.InitiatorAddress = tid_;
			    frame->PvtMessageFrame.StdMessageFrame.MessageSize      = (size >> 2);

			    frame->PvtMessageFrame.StdMessageFrame.Function = I2O_PRIVATE_MESSAGE;
			    frame->PvtMessageFrame.XFunctionCode            = I2O_TOKEN_CODE;
			    frame->PvtMessageFrame.OrganizationID           = XDAQ_ORGANIZATION_ID;
			    xdaq::frameSend (ref);
			    sent++;
			} catch (xdaqException e)
			{
				cout << "Reached FIFO limit after " << sent << " send operations" << endl;
				cout << e.toString() << endl;
				xdaq::frameFree(ref);
				break;
			}
		}
		
		cout << "Return from Enable function." << endl;
		} else {
			cout << "Called Enable not on instance 0" << endl;
		}
		return 0;
	}
	
	
	protected:
	
	unsigned long counter_;
	I2O_TID destination_;
};




//
// Dynamic link wrapper  for HelloWorld
//
// This class allows the user to attach the HelloWorld 
// dynamically to the XDAQ executive.
//

class FillFifoSO: public xdaqSO 
{
        public:
	
	// seen as main program for user application
        void init() 
	{
		allocatePluggable("FillFifo");
        }
	
	// Put interface of this into xdaqSO
	xdaqPluggable * create(string name) 
	{
		if (name == "FillFifo") 
		{
			return new FillFifo();
		} else return (xdaqPluggable*) 0;
	}

        void shutdown() 
	{
        }
	
	// Move to xdaqSO
	void allocatePluggable(string name) 
	{
		U16 localHost = xdaq::getHostId();
		int num = xdaq::getNumInstances(name);

		I2O_TID tid;
		U16 host;

		for (int i = 0; i < num; i++)
		{
			tid = xdaq::getTid(name, i); 
			host = xdaq::getHostId(tid);
			if (host == localHost)
			{
				xdaqPluggable * plugin = create(name);
				xdaq::load(plugin);
			}
		}
	}

        protected:

    
};




#endif
