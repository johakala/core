// $Id: Address.cc,v 1.8 2008/07/18 15:27:15 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "pt/fifo/Address.h"
#include "toolbox/net/URL.h"
#include "toolbox/net/Utils.h"
#include "toolbox/exception/Exception.h"

#include <iostream>
#include <sstream>

pt::fifo::Address::~Address()
{
}

pt::fifo::Address::Address(const std::string & url, const std::string & service): url_(url), service_(service)
{
}

std::string pt::fifo::Address::getService()
{
	return service_;
}

std::string pt::fifo::Address::getProtocol()
{
	return url_.getProtocol();
}

std::string pt::fifo::Address::toString()
{
	return url_.toString();
}

std::string pt::fifo::Address::getURL()
{
	return url_.toString();
}

std::string pt::fifo::Address::getHost()
{
	return url_.getHost();
}

std::string pt::fifo::Address::getPort()
{
    std::ostringstream o;
    if(url_.getPort() > 0)
        o <<  url_.getPort();
    return o.str();
}

std::string pt::fifo::Address::getPath()
{
	std::string path = url_.getPath();
        if ( path.empty()  ) {	
		return "/";
	}
	else
	{
		if ( path[0] == '/' )
		{
			return path;
		}
		else
		{
			path.insert(0,"/");
			return path;
		}
				
	}
}

std::string pt::fifo::Address::getServiceParameters()
{
	return url_.getPath();
}

bool pt::fifo::Address::equals( pt::Address::Reference address )
{
	return (( this->toString() == address->toString()) && ( this->getService() == address->getService()));
}
