// $Id: PeerTransport.cc,v 1.22 2008/07/18 15:27:15 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "toolbox/string.h"
#include "pt/fifo/PeerTransport.h"
#include "pt/fifo/Address.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "toolbox/mem/HeapAllocator.h"
#include "toolbox/task/WorkLoopFactory.h"
#include "toolbox/exception/Processor.h"
#include "pt/fifo/B2INMessenger.h"
#include "xdata/exdr/FixedSizeInputStreamBuffer.h"
#include "xdata/exdr/Serializer.h"

//
// Log4CPLUS
//
#include "log4cplus/logger.h"
#include "log4cplus/consoleappender.h"
#include "log4cplus/socketappender.h"
#include "log4cplus/nullappender.h"
#include "log4cplus/fileappender.h"
#include "log4cplus/helpers/appenderattachableimpl.h"
#include "log4cplus/helpers/loglog.h"
#include "log4cplus/helpers/pointer.h"
#include "log4cplus/spi/loggingevent.h"
#include "log4cplus/layout.h"

pt::fifo::PeerTransport::PeerTransport(Logger& logger) throw(pt::exception::Exception): 
	i2oMessenger_(new pt::fifo::I2OMessenger(this)), 
	b2inMessenger_(new pt::fifo::B2INMessenger(this)), 
	localI2OAddress_(new pt::fifo::Address("loopback://localhost", "i2o")), 
	localB2INAddress_(new pt::fifo::Address("loopback://localhost", "b2in")), 
	logger_(logger)
{
	i2oListener_ = 0;
	b2inListener_ = 0;
	
	fifo_ = toolbox::rlist<fifo::PostDescriptor>::create("fifo-PeerTransport",MaxNoEntries);
	sync_  = new toolbox::BSem(toolbox::BSem::EMPTY);
	mutex_ = new toolbox::BSem(toolbox::BSem::FULL);
	pending_ = 0;
	
	toolbox::net::URN urn("toolbox-mem-pool", "PeerTransportFifo/heap");
	try 
	{
		toolbox::mem::HeapAllocator* a = new toolbox::mem::HeapAllocator();
		toolbox::mem::getMemoryPoolFactory()->createPool(urn, a);			
	}
	catch (toolbox::mem::exception::Exception & e)
	{
		XCEPT_RETHROW (pt::exception::Exception, "Cannot create default memory pool", e);
	}

	pool_ = toolbox::mem::getMemoryPoolFactory()->findPool(urn);

	
	process_ = toolbox::task::bind(this, &pt::fifo::PeerTransport::process, "process");
	
	toolbox::task::getWorkLoopFactory()->getWorkLoop("fifo/PeerTransport", "waiting")->submit(process_);
	toolbox::task::getWorkLoopFactory()->getWorkLoop("fifo/PeerTransport", "waiting")->activate();
	
}


pt::fifo::PeerTransport::~PeerTransport()
{
	delete sync_;
	delete mutex_;
	//delete messenger_;
	
	// STOP THE TASK HERE
}

toolbox::mem::Pool * pt::fifo::PeerTransport::getPool()
{
	return pool_;
}

pt::Messenger::Reference 
pt::fifo::PeerTransport::getMessenger (pt::Address::Reference destination, pt::Address::Reference local) 
throw (pt::exception::UnknownProtocolOrService)
{
	if ( destination->getService()  == "i2o") 	
	{
	 	// Fifo peer transport is an I2O messenger
		return i2oMessenger_;
	}
	else if (destination->getService()  == "b2in") 
	{
			// Fifo peer transport is an I2O messenger
		return b2inMessenger_;
	}
	else
	{
		std::string msg = "Cannot handle protocol service combination, destination protocol was :";
		msg +=  destination->getProtocol();
		msg += " destination service was:";
		msg +=  destination->getService();
		msg += " while local protocol was:";
		msg += local->getProtocol();
		msg += "and local service was:";
		msg += local->getService();
		
		XCEPT_RAISE(pt::exception::UnknownProtocolOrService,msg);
	}

}
	

pt::TransportType pt::fifo::PeerTransport::getType()
{
	return pt::SenderReceiver;
}
	

// The FIFO transport does not support any addresses. It is always locally available
//
pt::Address::Reference pt::fifo::PeerTransport::createAddress( const std::string& url ,const std::string& service)
	throw (pt::exception::InvalidAddress)
{
	if ( service  == "i2o") 	
	{
	 	// Fifo peer transport is an I2O messenger
		return localI2OAddress_;
	}
	else if (service  == "b2in") 
	{
			// Fifo peer transport is an I2O messenger
		return localB2INAddress_;
	}
	else
	{
		std::stringstream msg;
		msg <<  "cannot create adress for url " << url <<  " service " << service;
		XCEPT_RAISE(pt::exception::InvalidAddress,msg.str());
	}
}


pt::Address::Reference 
pt::fifo::PeerTransport::createAddress( std::map<std::string, std::string, std::less<std::string> >& address )
	throw (pt::exception::InvalidAddress)
{
	return this->createAddress("",address["service"]);
}
	

std::string pt::fifo::PeerTransport::getProtocol()
{
	return "loopback";
}
	


void pt::fifo::PeerTransport::post (toolbox::mem::Reference* ref, toolbox::exception::HandlerSignature* handler, void* context, uint32_t signature) 
throw (pt::exception::Exception )
{
	mutex_->take();
	
	int s = fifo_->elements();
	if (s > (MaxNoEntries-10))
	{	 	
		std::string msg = toolbox::toString ("Cannot send, fifo overflow: %d entries, maxEntries-10: %d", s, MaxNoEntries-10);
		mutex_->give();
		XCEPT_RAISE (pt::exception::Exception, msg);
	}
	
	pt::fifo::PostDescriptor desc;
	desc.ref = ref;
	desc.handler = handler;
	desc.context = context;
	desc.signature = signature;
	fifo_->push_back(desc);
	
	if ( pending_ > 0) 
	{
		pending_--;
		sync_->give();
		mutex_->give();
		return;
	}	
	
	mutex_->give();
}

void pt::fifo::PeerTransport::removeServiceListener ( pt::Listener* listener ) throw (pt::exception::Exception)
{
	// This function throws already a pt::exception::Exception
	pt::PeerTransportReceiver::removeServiceListener(listener);

	if (listener->getService() == "i2o" )
	{	
		i2oListener_ =  0;
	} 
	else if (listener->getService() == "b2in" )
	{
		b2inListener_ =  0;
	}
	else
	{
		std::string msg = "Cannot find listener for service ";
		msg += listener->getService();
		XCEPT_RAISE (pt::exception::Exception, msg);
	}
}

void pt::fifo::PeerTransport::removeAllServiceListeners () 
{
	pt::PeerTransportReceiver::removeAllServiceListeners();	
	i2oListener_ =  0;
	b2inListener_ =  0;
}


void pt::fifo::PeerTransport::addServiceListener ( pt::Listener* listener ) throw (pt::exception::Exception)
{
	pt::PeerTransportReceiver::addServiceListener(listener);
	try 
	{	
		if (listener->getService() == "i2o" )
		{	
			i2oListener_ = dynamic_cast<i2o::Listener*>(this->getListener("i2o"));;
		} 
		else if (listener->getService() == "b2in" )
		{
			b2inListener_ =  dynamic_cast<b2in::nub::Listener*>(this->getListener("b2in"));;
		}
		
	} 
	catch (pt::exception::Exception& pte)
	{
		// i2o listener not added
		i2oListener_ = 0;
		std::string msg = "Cannot add a listener for service ";
		msg += listener->getService();
		XCEPT_RETHROW (pt::exception::Exception, msg, pte);
	}
}
//
// The service routine running in a task to receive messages locally
//
bool pt::fifo::PeerTransport::process(toolbox::task::WorkLoop * wl)  
{
	// Put here WHILE(running) to allow exiting the function
	for (;;) 
	{
		mutex_->take();
		if ( ! fifo_->empty() ) 
		{
			pt::fifo::PostDescriptor desc = fifo_->front();
			fifo_->pop_front();
			mutex_->give();

			try
			{
				
				if ( desc.signature == 0x6202696e )
				{
					if (b2inListener_ != 0)
					{
						// close message in case of connection set to close
						// de-serialize properties, within first block
						xdata::exdr::FixedSizeInputStreamBuffer inBuffer((char*) desc.ref->getDataLocation(),desc.ref->getDataSize());
						xdata::Properties plist;
	
						xdata::exdr::Serializer serializer;
						serializer.import(&plist, &inBuffer );

						b2inListener_->processIncomingMessage (desc.ref, plist);			
					} 
					else 
					{
						desc.ref->release();
						pt::exception::Exception e("pt::exception::Exception", "No listener for b2in message handling installed with pt fifo", __FILE__, __LINE__, __FUNCTION__);
						toolbox::exception::getProcessor()->push(e, desc.handler, desc.context);
					}
				}
				else
				{
					if (i2oListener_ != 0)
					{
						i2oListener_->processIncomingMessage (desc.ref);			
					} 
					
					else 
					{
						desc.ref->release();
						pt::exception::Exception e("pt::exception::Exception", "No listener for i2o message handling installed with pt fifo", __FILE__,__LINE__,__FUNCTION__);
						toolbox::exception::getProcessor()->push(e, desc.handler, desc.context);
					}
					
				}	
			} 
			catch (pt::exception::Exception& pte)
			{
				desc.ref->release();
				toolbox::exception::getProcessor()->push(pte, desc.handler, desc.context);
				
			}
			catch(xdata::exception::Exception & e )
			{
				desc.ref->release();
				XCEPT_DECLARE_NESTED(pt::exception::Exception, xbe, "Failed to parse B2IN properties", e);
				toolbox::exception::getProcessor()->push(xbe, desc.handler, desc.context);
			}
			catch (...)
			{
				desc.ref->release();
				XCEPT_DECLARE(pt::exception::Exception, e, "Caught unknown exception in i2o callback");
				toolbox::exception::getProcessor()->push(e, desc.handler, desc.context);
			}
		}
		else 
		{ // go to wait
			pending_++;
			mutex_->give();
			sync_->take(); // wait Not empty condition
		}
	}
	return false;
}


void pt::fifo::PeerTransport::config(pt::Address::Reference address) throw (pt::exception::Exception)
{
	// nothing
}

std::vector<std::string> pt::fifo::PeerTransport::getSupportedServices()
{
	std::vector<std::string> s;
	s.push_back("i2o");
	s.push_back("b2in");
	return s;
}

bool pt::fifo::PeerTransport::isServiceSupported(const std::string& service)
{
	if (service == "i2o") return true;
	if (service == "b2in") return true;
	else return false;
}



