// $Id: I2OMessenger.cc,v 1.7 2008/07/18 15:27:15 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "pt/fifo/I2OMessenger.h"
#include "pt/fifo/PeerTransport.h"

pt::fifo::I2OMessenger::I2OMessenger(pt::fifo::PeerTransport* pt)
{
	pt_ = pt;
}

void pt::fifo::I2OMessenger::send (toolbox::mem::Reference* msg, toolbox::exception::HandlerSignature * handler, void * context) 
	throw (pt::exception::Exception)
{
	// This post function throws already pt::exception::Exception, therefore we don't try/catch around again
	pt_->post(msg, handler, context, 0); // 0 means i2o message signature
}

pt::Address::Reference pt::fifo::I2OMessenger::getLocalAddress()
{
        return pt::Address::Reference(0);
}

pt::Address::Reference pt::fifo::I2OMessenger::getDestinationAddress()
{
        return pt::Address::Reference(0);
}

