// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "pt/fifo/PeerTransportFifo.h"
#include "pt/PeerTransportAgent.h"
#include "hyperdaq/framework/Layout.h"

XDAQ_INSTANTIATOR_IMPL(pt::fifo::PeerTransportFifo)


pt::fifo::PeerTransportFifo::PeerTransportFifo(xdaq::ApplicationStub * s)
 throw(xdaq::exception::Exception)
 : xdaq::Application(s), manager_(this)
{
	s->getDescriptor()->setAttribute("icon","/pt/fifo/images/pt-fifo-icon.png");
	s->getDescriptor()->setAttribute("icons","/pt/images");

	try
	{
		pt_ = new pt::fifo::PeerTransport(getApplicationLogger());
	}
	catch(pt::exception::Exception & e)
	{
		XCEPT_RETHROW(xdaq::exception::Exception,"failed to create peer transport fifo", e);
	}
	
	pt::PeerTransportAgent* pta = pt::getPeerTransportAgent();
	pta->addPeerTransport(pt_);
}

pt::fifo::PeerTransportFifo::~PeerTransportFifo()
{
	pt::PeerTransportAgent* pta = pt::getPeerTransportAgent();	
	pta->removePeerTransport(pt_);
	delete pt_;	
}
