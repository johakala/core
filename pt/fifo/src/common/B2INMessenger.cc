// $Id: B2INMessenger.cc,v 1.3 2008/07/18 15:27:15 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/


#include "pt/fifo/B2INMessenger.h"
#include "pt/fifo/PeerTransport.h"
#include "pt/fifo/Address.h"
#include "toolbox/mem/Reference.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "xdata/exception/Exception.h"
#include "xdata/exdr/Serializer.h"
#include "xdata/exdr/FixedSizeOutputStreamBuffer.h"

pt::fifo::B2INMessenger::B2INMessenger(pt::fifo::PeerTransport * pt ): 
local_(new pt::fifo::Address("loopback://localhost", "b2in"))
{
	pt_ = pt;
}

pt::fifo::B2INMessenger::~B2INMessenger()
{
	//delete channel_;
}

void pt::fifo::B2INMessenger::invalidate()
{
}

void pt::fifo::B2INMessenger::send
(
	toolbox::mem::Reference* msg,
	xdata::Properties & plist,
	toolbox::exception::HandlerSignature* handler,
	void* context
) 
throw (
	b2in::nub::exception::InternalError,
	b2in::nub::exception::QueueFull,
	b2in::nub::exception::OverThreshold
)
{
	xdata::exdr::Serializer serializer;
	
	if (pt_->getPool() == 0)
	{
		std::string msg = "Cannot access memory pool";
		XCEPT_RAISE (b2in::nub::exception::InternalError, msg);
	}
	
	toolbox::mem::Reference* ref = 0;
	try
	{
		ref = toolbox::mem::getMemoryPoolFactory()->getFrame(pt_->getPool(), 2048);
	}
	catch (toolbox::mem::exception::Exception & e) 
	{
		XCEPT_RETHROW (b2in::nub::exception::InternalError, "failed to allocate properties block", e);
	}
	
	xdata::exdr::FixedSizeOutputStreamBuffer outBuffer((char*) ref->getDataLocation(), 2048);
	
	try 
	{
		serializer.exportAll (&plist, &outBuffer);
	}
	catch (xdata::exception::Exception & e )
	{
		ref->release();
		XCEPT_RETHROW (b2in::nub::exception::InternalError, "could not serialize B2IN properites", e);
	}
	
	ref->setDataSize(outBuffer.tellp());
	ref->setNextReference(msg);
	
	/* Ideally PT FIFO implements a dead band algorithm with queue full and overflow situations.
	   This version does only handle the queue full situation.
	*/
	try
	{
		pt_->post (ref, handler, context, 0x6202696e); // signature is b2in
	}
	catch (pt::exception::Exception& e)
	{
		ref->setNextReference(0); // property list buffer need to be free, the rest of the chain is responsibility of the caller
		ref->release();
		XCEPT_RETHROW (b2in::nub::exception::QueueFull, "failed to post packet", e);
	}	
}

pt::Address::Reference pt::fifo::B2INMessenger::getLocalAddress()
{
	return local_;
}

pt::Address::Reference pt::fifo::B2INMessenger::getDestinationAddress()
{
	return local_;
}
