/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2007, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber, L. Orsini, R. Moser                             *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _benchmark_soapping_Server_h_
#define _benchmark_soapping_Server_h_

#include "xdaq/Application.h"
#include "xdata/UnsignedInteger.h"

namespace benchmark
{

namespace soapping
{

class Server:  public xdaq::Application
{	
	public:
		XDAQ_INSTANTIATOR();

		Server(xdaq::ApplicationStub* c) throw(xdaq::exception::Exception);

		// External ping/pong interface  
		xoap::MessageReference start(xoap::MessageReference  message) throw (xoap::exception::Exception);
};

}

}

#endif

