/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2007, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber, L. Orsini, R. Moser                             *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _benchmark_soapping_Client_h_
#define _benchmark_soapping_Client_h_

#include <set>

#include "xdaq/Application.h"
#include "xdata/UnsignedInteger32.h"
#include "toolbox/Task.h"
#include "toolbox/squeue.h"
#include "toolbox/BSem.h"

namespace benchmark
{

namespace soapping
{

class Client:  public xdaq::Application, public toolbox::Task
{	
	public:
		XDAQ_INSTANTIATOR();

		Client(xdaq::ApplicationStub* c) throw(xdaq::exception::Exception);

		// inject a new packet  
		xoap::MessageReference start(xoap::MessageReference message) throw (xoap::exception::Exception);

		int svc();
	protected:

		std::set<xdaq::ApplicationDescriptor*> destinations_;
		toolbox::squeue<xoap::MessageReference> messages_;
		toolbox::BSem mutex_;
		long len_;
		char *buffer_;

		xdata::UnsignedInteger32 bufferSize_;
};

}

}

#endif

