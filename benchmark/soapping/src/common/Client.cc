/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2007, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber, L. Orsini, R. Moser                             *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include "benchmark/soapping/Client.h"

#include "xdaq/NamespaceURI.h"
#include "xcept/Exception.h"
#include "xcept/tools.h"
#include "xoap/MessageFactory.h"
#include "xoap/MessageReference.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPName.h"
#include "xoap/SOAPBody.h"
#include "xoap/Method.h"


benchmark::soapping::Client::Client(xdaq::ApplicationStub* c)
	throw(xdaq::exception::Exception)
	: xdaq::Application(c), toolbox::Task("benchmark::soapping"), mutex_(toolbox::BSem::FULL)
{
	xoap::bind (this, &Client::start, "start", XDAQ_NS_URI);

	try
	{
		destinations_ = getApplicationContext()->getDefaultZone()->getApplicationDescriptors("benchmark::soapping::Server");
	}
	catch (xdaq::exception::Exception& e)
	{
		LOG4CPLUS_FATAL (getApplicationLogger(), xcept::stdformat_exception_history(e));
		XCEPT_RAISE (xcept::Exception, "No Server application instance found. Client cannot be configured.");
	}

	buffer_ = NULL;
	getApplicationInfoSpace()->fireItemAvailable("bufferSize",&bufferSize_);
	
	this->activate();
}
		
 	
xoap::MessageReference benchmark::soapping::Client::start(xoap::MessageReference  message) throw (xoap::exception::Exception)
{
	LOG4CPLUS_INFO (getApplicationLogger(), toolbox::toString("put another message in the queue..."));

	if(buffer_ ==NULL)
	{
    try
    {
      len_ = *dynamic_cast<xdata::UnsignedInteger32*>(getApplicationInfoSpace()->find("bufferSize"));
    }
    catch (xdata::exception::Exception& e)
    {
      len_ = 1000;
    }

		LOG4CPLUS_INFO (getApplicationLogger(), "bufferSize: " << len_);
		buffer_ = (char*)malloc(len_*sizeof(char));
		if(buffer_ == NULL)
		{
			LOG4CPLUS_ERROR (getApplicationLogger(), "buffer not allocated");
			XCEPT_RAISE (xcept::Exception, "buffer could not be allocated");
		}

		// Fill with data
		for(long i=0;i<len_;i++)
			buffer_[i]='0'+(i%10);

		buffer_[len_-1]=0;
	}

	xoap::MessageReference msg = xoap::createMessage();
	xoap::SOAPPart soap = msg->getSOAPPart();
	xoap::SOAPEnvelope envelope = soap.getEnvelope();
	xoap::SOAPBody body = envelope.getBody();
	xoap::SOAPName command = envelope.createName("start","xdaq", "urn:xdaq-soap:3.0");
	body.addBodyElement(command);

	// add an attachment with generated (and in this case invalid) data
	xoap::AttachmentPart * attachment = msg->createAttachmentPart(buffer_, len_, "text/plain");
	attachment->setContentEncoding("exdr");
	toolbox::net::URL address(getApplicationContext()->getContextDescriptor()->getURL());
	attachment->setContentId(toolbox::toString("<%s@%s>","MyTable", address.getHost().c_str()));
	attachment->setContentLocation(toolbox::toString("%s/%s", address.toString().c_str(),getApplicationDescriptor()->getURN().c_str()));
	msg->addAttachmentPart(attachment);

	mutex_.take();
	messages_.push(msg);
	mutex_.give();

	return xoap::createMessage();
}

int benchmark::soapping::Client::svc()
{
	LOG4CPLUS_INFO (getApplicationLogger(), "ping task started");
	for(;;)
	{
		xoap::MessageReference msg = messages_.pop();

		std::set<xdaq::ApplicationDescriptor*>::iterator iter;
		for(iter=destinations_.begin(); iter!=destinations_.end(); iter++)
		{
			try
			{
				LOG4CPLUS_INFO (getApplicationLogger(), "ping");
				xoap::MessageReference reply = getApplicationContext()->postSOAP(msg, *getApplicationDescriptor(), *(*iter));
			} 
			catch (xdaq::exception::Exception& e)
			{
				LOG4CPLUS_INFO (getApplicationLogger(), "Error: " << e.message());
			}
		}

		//TODO push in again - configureable
		mutex_.take();
		messages_.push(msg);
		mutex_.give();
	}
}

