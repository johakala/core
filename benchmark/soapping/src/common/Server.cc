/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2007, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber, L. Orsini, R. Moser                             *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include "benchmark/soapping/Server.h"

#include "xdaq/NamespaceURI.h"
#include "xcept/Exception.h"
#include "xcept/tools.h"
#include "xoap/MessageReference.h"
#include "xoap/Method.h"


benchmark::soapping::Server::Server(xdaq::ApplicationStub* c)
	throw(xdaq::exception::Exception)
	: xdaq::Application(c)
{
	xoap::bind (this, &Server::start, "start", XDAQ_NS_URI);
}
		
 	
xoap::MessageReference benchmark::soapping::Server::start(xoap::MessageReference  message) throw (xoap::exception::Exception)
{
	LOG4CPLUS_INFO (getApplicationLogger(), "pong");

	// just return the same message
	return message;
}

