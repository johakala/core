/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2007, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber, L. Orsini, R. Moser                             *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include "config/version.h"
#include "toolbox/version.h"
#include "xdaq/version.h"
#include "xdata/version.h"
#include "xoap/version.h"
#include "pt/version.h"
#include "xcept/version.h"
#include "benchmark/soapping/version.h"

GETPACKAGEINFO(soapping)

void soapping::checkPackageDependencies() throw (config::PackageInfo::VersionException)
{
	CHECKDEPENDENCY(config)
	CHECKDEPENDENCY(toolbox)
	CHECKDEPENDENCY(xdaq)
	CHECKDEPENDENCY(xdata)
	CHECKDEPENDENCY(xoap)
	CHECKDEPENDENCY(pt)
	CHECKDEPENDENCY(xcept)
}

std::set<std::string, std::less<std::string> > soapping::getPackageDependencies()
{
	std::set<std::string, std::less<std::string> > dependencies;
	ADDDEPENDENCY(dependencies,config);
	ADDDEPENDENCY(dependencies,toolbox);
	ADDDEPENDENCY(dependencies,xdaq);
	ADDDEPENDENCY(dependencies,xdata);
	ADDDEPENDENCY(dependencies,xoap);
	ADDDEPENDENCY(dependencies,pt);
	ADDDEPENDENCY(dependencies,xcept);
	return dependencies;
}

