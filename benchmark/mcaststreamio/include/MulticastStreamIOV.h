// $Id: MulticastStreamIOV.h,v 1.2 2004/09/02 09:59:26 xdaq Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

//
// Version definition for MulticastStreamIO
//
#ifndef MulticastStreamIOV_H
#define MulticastStreamIOV_H

#include "PackageInfo.h"

namespace MulticastStreamIO {
    const string package  =  "MulticastStreamIO";
    const string versions =  "1.3beta";
    const string description = "Multicast (send one to many) example";
    toolbox::PackageInfo getPackageInfo();
    void checkPackageDependencies() throw (toolbox::PackageInfo::VersionException);
    set<string, less<string> > getPackageDependencies();
}

#endif
