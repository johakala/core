// $Id: i2oMulticastStreamIOListener.h,v 1.2 2004/09/02 09:59:26 xdaq Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _i2oMulticastStreamIOListener_h_
#define _i2oMulticastStreamIOListener_h_

#include "i2oListener.h"
#include "i2oMulticastStreamIOMsg.h"

class i2oMulticastStreamIOListener : public i2oListener 
{

 public:
  i2oMulticastStreamIOListener(){
    i2oBindMethod (this, 
                   &i2oMulticastStreamIOListener::token, 
                   I2O_TOKEN_CODE,
                   XDAQ_ORGANIZATION_ID, 0);
  }
 
  virtual void token( BufRef * ref ) = 0;
};

#endif

