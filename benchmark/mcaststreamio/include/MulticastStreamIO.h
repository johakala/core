// $Id: MulticastStreamIO.h,v 1.2 2004/09/02 09:59:26 xdaq Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _MulticastStreamIO_h_
#define _MulticastStreamIO_h_

#include "xdaq.h"

#include "i2oMulticastStreamIOListener.h"

#include <list>
#include <algorithm>
#include "PerformanceMeter.h"

#include "Plot.h"
#include "BSem.h"
#include "rlist.h"

#include "Task.h"

class MulticastStreamIO: public i2oMulticastStreamIOListener, public xdaqApplication  , public PollingObject, public Task
{
	
	public:

	MulticastStreamIO() 
	{
		pta * ptap = executive->peerTransportAgent();
		ptap->registerPollingObject(this);
		
		sync_ = new BSem(BSem::EMPTY);
		
		exportParam("counter",counter_);
		exportParam("samples",samples_);
		exportParam("startSize",startSize_);
		exportParam("endSize",endSize_);
		exportParam("currentSize",currentSize_);
		exportParam("dataBw",dataBw_);
		exportParam("dataRate",dataRate_);
 		exportParam("dataLatency",dataLatency_);
		exportParam("latencyHistogram",latencyPlot_);
		exportParam("rateHistogram",ratePlot_);
		exportParam("bandwidthHistogram",bandwidthPlot_);
		exportParam("numFrames",numFrames_);
		exportParam("step",step_);
		// fill the plot 
		latencyPlot_.addTitle ("RoundTrip Latency");
		ratePlot_.addTitle ("RoundTrip Rate");
		bandwidthPlot_.addTitle ("RoundTrip Bandwidth");
		Histogram1d& hl = latencyPlot_.addHistogram1d ("Latency");
		Histogram1d& hr = ratePlot_.addHistogram1d ("Rate");
		Histogram1d& hb = bandwidthPlot_.addHistogram1d ("Bandwidth");
		hl.showErrorBars ( false);
		hr.showErrorBars ( false);
		hb.showErrorBars ( false);
		hl.setBarColor ("Blue");
		hr.setBarColor ("Red");
		hb.setBarColor ("Green");
		
		XDAQ_NOTE(("created MulticastStreamIO..."));
	}
	
	
	void plugin() 
	{
		XDAQ_NOTE(("plugin MulticastStreamIO..."));
		if ( instance_ == 0 ) streamio_ = this;
	}
	
      
	
	
	void handleFrame(BufRef * ref)
	{
		
	    recycledFrames_.push_back(ref);
		
	}
	
	int svc()
	{
		for (;;) poll();
	}
	
	bool poll() 
	{
	
		if ( ! recycledFrames_.empty() ) 
		{
				XDAQ_DEBUG(("re-send frame"));
				BufRef * ref = recycledFrames_.front();
				recycledFrames_.pop_front();
			
				this->checkMeasurements(ref);

		}
		
		return true;
	}
	
	
	
	void checkMeasurements(BufRef * ref) 
	{
	    XDAQ_DEBUG(("check measurement counter:%d (samples_  - numFrames_): %d", counter_, (samples_ - numFrames_ ) ));
	    
	    if ( counter_ <= (samples_  - numFrames_ )) {
			XDAQ_DEBUG(("recycle frame"));

						
				//recycle frame
				BufRef * copyRef = xdaq::frameRefAlloc();
				*copyRef = *ref;
				
				//override free function arg1
				copyRef->freeFunc_ = &MulticastStreamIO::freeHandler;
				copyRef->arg1_ = ref; //original ref
				
				xdaq::frameSend(copyRef);
			
			
	    }
	    else  //time to flush
	    {  
	    		//XDAQ_NOTE(("flush frame %d", val_--));
			xdaq::frameFree(ref);
			
	    }
	
	    
	    if ( pmeter_.benchmark(currentSize_) ) {
	    	
		
		Histogram1d& hl = latencyPlot_.getHistogram1d ("Latency");
		hl.addValue (pmeter_.latency() );
		Histogram1d& hr = ratePlot_.getHistogram1d ("Rate");
		hr.addValue (pmeter_.rate() );
		Histogram1d& hb = bandwidthPlot_.getHistogram1d ("Bandwidth");
		hb.addValue (pmeter_.bandwidth());
		
		hl.setMaxX (currentSize_);
		hr.setMaxX (currentSize_);
		hb.setMaxX (currentSize_);
		
		XDAQ_NOTE(("measured latency: %f",pmeter_.latency()));
		currentSize_ += step_;

		if ( counter_ != samples_  ) {
		
			XDAQ_ERROR(("out of synchronization counter_:%d samples:%d stop measurement",counter_ ,samples_));
			return;
		}
		
		 if ( currentSize_ > endSize_ ) {
		 	XDAQ_NOTE(("done"));
			return;
		 }	
		XDAQ_NOTE(("start next size: %d",currentSize_)); 
		this->start();
		return;
	    }
	    counter_++;
 
	
	}
	
	static void freeHandler(void * arg1, void *  arg2, void * arg3) 
	{	    
		XDAQ_DEBUG(("free Handler MulticastStreamIO"))
		
		streamio_->handleFrame((BufRef*)arg1);
			
	}
	
	void ParameterSetDefault (list<string> & paramNames) {
		XDAQ_NOTE(("set number of samples to: %d", samples_));
		pmeter_.init(samples_);
	}
	
	void token(  BufRef * ref) 
	{
		// if local instance ignore it
		if ( instance_ != 0 ) {
			counter_++;
				//XDAQ_NOTE(("recived frame and freed"));
			PI2O_TOKEN_MESSAGE_FRAME frame = (PI2O_TOKEN_MESSAGE_FRAME) ref->buf_;   
			unsigned long size = frame->PvtMessageFrame.StdMessageFrame.MessageSize << 2;
			if ( pmeter_.benchmark(size) ) {
	    	
				XDAQ_NOTE(("measured latency: %f",pmeter_.latency()));
		
	    		}
		}
		
		xdaq::frameFree(ref);
	}
	

        //
	// run control send new paramater values. system is started by
	// peer instance 0 upon reception of the paramsSet command from
        // the run control.
        //
	void ParameterSet (list<string> & paramNames){
          
         
	}
	
	//
	// run control requests current paramater values
	//
	void ParameterGet (list<string> & paramNames)   { 
		// update measurements monitors
		dataBw_        =  pmeter_.bandwidth();
    		dataLatency_   =  pmeter_.latency();
		dataRate_      =  pmeter_.rate();
		XDAQ_DEBUG(("bandw: %f",pmeter_.bandwidth()));
	}
	
	void Configure() throw (xdaqException)
	{
		counter_ = 0;
		XDAQ_NOTE (("Configure :Parameter counter: %d", counter_));
		XDAQ_NOTE (("Configure :Parameter samples: %d", samples_));
		XDAQ_NOTE (("Configure :Parameter start size: %d",    startSize_));
		XDAQ_NOTE (("Configure :Parameter end size: %d",    endSize_));
		XDAQ_NOTE (("Configure :Parameter numframes: %d",    numFrames_));
		destination_ = xdaq::getTid("MulticastStreamIO", 1);
		
		
	}
	
	void Enable() throw (xdaqException)
	{
		recycledFrames_.resize(numFrames_);
		freeFrames_.resize(numFrames_);
		
		dataBw_  = 0.0;
		dataRate_ = 0.0;
		dataLatency_ = 0.0;
		pmeter_.init(samples_);

		XDAQ_DEBUG (("Configuring, from state: %d", state()));

		if (instance_ == 0) 
		{
			// increase size to contain header info
			currentSize_ = sizeof(I2O_TOKEN_MESSAGE_FRAME) + startSize_;

			// actual end size
			endSize_ += sizeof(I2O_TOKEN_MESSAGE_FRAME);
			Histogram1d& hl = latencyPlot_.getHistogram1d ("Latency");
			Histogram1d& hr = ratePlot_.getHistogram1d ("Rate");
			Histogram1d& hb = bandwidthPlot_.getHistogram1d ("Bandwidth");


			hl.setMinX (currentSize_);
			hr.setMinX (currentSize_);
			hb.setMinX (currentSize_);
			
			this->activate();		
			this->start();
		}	
	}
	
	void start() {
		counter_ = 0;
		val_ =0;
		flushCounter_ = 0;
		for (int i = 0; i < numFrames_; i++ ){
				// allocate frame 
				BufRef * ref = xdaq::frameAlloc(endSize_);
				val_++;
				// prepare frame and send
				PI2O_TOKEN_MESSAGE_FRAME frame = (PI2O_TOKEN_MESSAGE_FRAME) ref->buf_;   

				frame->PvtMessageFrame.StdMessageFrame.MsgFlags         = XDAQ_MULTICAST;
				frame->PvtMessageFrame.StdMessageFrame.VersionOffset    = 0;
				frame->PvtMessageFrame.StdMessageFrame.TargetAddress    = classId();
				frame->PvtMessageFrame.StdMessageFrame.InitiatorAddress = tid();
				frame->PvtMessageFrame.StdMessageFrame.MessageSize      = currentSize_ >> 2;

				frame->PvtMessageFrame.StdMessageFrame.Function = I2O_PRIVATE_MESSAGE;
				frame->PvtMessageFrame.XFunctionCode            = I2O_TOKEN_CODE;
				frame->PvtMessageFrame.OrganizationID           = XDAQ_ORGANIZATION_ID;

				BufRef * copyRef = xdaq::frameRefAlloc();
				*copyRef = *ref;
				
				//override free function arg1
				copyRef->freeFunc_ = &MulticastStreamIO::freeHandler;
				copyRef->arg1_ = (void*)ref; //original ref
				
				xdaq::frameSend(copyRef);
				
          	}   
	}
	
	void Suspend() throw (xdaqException)
	{
		XDAQ_NOTE (("Configuring, from state: %d", state()));
	}
	
	void Resume() throw (xdaqException)
	{
		XDAQ_NOTE (("Configuring, from state: %d", state()));
	}
	
	void Halt() throw (xdaqException)
	{
		XDAQ_NOTE (("Configuring, from state: %d", state()));
	}
	
	void Disable() throw (xdaqException)
	{
		XDAQ_NOTE (("Configuring, from state: %d", state()));
	}
	
	protected:
	
	// some paramters
       unsigned long counter_;
       unsigned long samples_;
       unsigned long startSize_;
       unsigned long endSize_;
       unsigned long currentSize_;
       unsigned long destination_;
       unsigned long numFrames_;
       unsigned long val_;
       unsigned long step_;
	//
	//! perfromamce Meters
	//
	PerformanceMeter pmeter_;
	//
	//!current measurements
	//
	double dataBw_;
	double dataRate_;
 	double dataLatency_;
	
       Plot latencyPlot_;
       Plot ratePlot_;
       Plot bandwidthPlot_;
	
	vector<BufRef*> usedRefs_;
	
	static MulticastStreamIO * streamio_;
	
	rlist<BufRef*> recycledFrames_;
	rlist<BufRef*> freeFrames_;
	BSem * sync_;
	unsigned long flushCounter_;
};


class MulticastStreamIOSO: public xdaqSO {
        public:


		// seen as main program for user application
        void init() {
				
               allocatePluggable("MulticastStreamIO");

        }

        void shutdown() {
             

        }
	xdaqPluggable * create(string name) 
	{
		if ( name == "MulticastStreamIO" ) return  new MulticastStreamIO(); ;
	}
	
	void allocatePluggable(string name) {
	
		U16 localHost = xdaq::getHostId();
		int num = xdaq::getNumInstances(name);

		I2O_TID tid;
		U16 host;

		for (int i = 0; i < num; i++)
		{
			tid = xdaq::getTid(name, i); 
			host = xdaq::getHostId(tid);
			if (host == localHost)
			{
					xdaqPluggable * plugin = create(name);
					xdaq::load(plugin);
			}
		}
	}

       
    
};




#endif
