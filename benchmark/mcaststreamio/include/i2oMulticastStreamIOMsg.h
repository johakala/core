// $Id: i2oMulticastStreamIOMsg.h,v 1.2 2004/09/02 09:59:26 xdaq Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _i2oMulticastStreamIOMsg_h_
#define _i2oMulticastStreamIOMsg_h_

#include "i2o.h"

// callback binding (user defined)
#define I2O_TOKEN_CODE 0x0001

typedef struct _I2O_TOKEN_MESSAGE_FRAME {
  I2O_PRIVATE_MESSAGE_FRAME PvtMessageFrame;
} I2O_TOKEN_MESSAGE_FRAME, *PI2O_TOKEN_MESSAGE_FRAME;

#endif
