// $Id: Server.h,v 1.2 2008/07/18 15:26:40 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _Server_h_
#define _Server_h_

#include "xdaq/Application.h"
#include "xgi/framework/UIManager.h"

#include <set>
#include "toolbox/mem/Reference.h"
#include "toolbox/mem/Pool.h"
#include "toolbox/fsm/FiniteStateMachine.h"
#include "toolbox/PerformanceMeter.h"
#include "xdata/UnsignedLong.h"
#include "xdata/UnsignedInteger.h"
#include "xdata/String.h"
#include "xdata/Double.h"
#include "xdata/Boolean.h"
#include "xdata/Event.h"
#include "xdata/ActionListener.h"
#include "i2o/Method.h"
#include "toolbox/task/Timer.h"

#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"
#include "xgi/Utils.h"
#include "xgi/Input.h"
#include "xgi/Output.h"


class Server: public xdaq::Application, public xgi::framework::UIManager, xdata::ActionListener, public toolbox::task::TimerListener
{	
	public:
		XDAQ_INSTANTIATOR();

		Server(xdaq::ApplicationStub* c) throw(xdaq::exception::Exception);

		// Interface function invoked for the I2O token message
		// that is received from the Client(s)
		//
		void token(  toolbox::mem::Reference * ref)  throw (i2o::exception::Exception);

		// External interface to start test  
		xoap::MessageReference start(xoap::MessageReference  message) throw (xoap::exception::Exception);
		
		// stop client from sending token frames to server
		xoap::MessageReference stop(xoap::MessageReference  message) throw (xoap::exception::Exception);

		void Default(xgi::Input *in, xgi::Output *out) throw (xgi::exception::Exception);
        void downloadMeasurements(xgi::Input *in, xgi::Output *out) throw (xgi::exception::Exception);
        //void displayMeasurements(xgi::Input *in, xgi::Output *out) throw (xgi::exception::Exception);

	protected:

		//void checkMeasurements(int size);

		// Stop clients from sending I2O frames
		//void stopClients();

		// Tell Clients to update the size of their I2O
		// token message by sending a ParameterSet:
		//  <properties xmlns:c="urn:xdaq-application:PeerTransportTCP" xsi:type="soapenc:Struct">
        	//                        <c:currentSize xsi:type="xsd:unsignedLong">xxx</poolName>
        	//   </properties>
		//void nextSize(xdata::UnsignedLongT size);

		// Start Clients by sending the command "start"
		//
		//void activateClients();
		//
		void timeExpired (toolbox::task::TimerEvent& e);
		void actionPerformed( xdata::Event& event);
		double std_deviation(std::vector<double>& samples, double mean ); 

		void stopped (toolbox::Event::Reference e) throw (toolbox::fsm::exception::Exception);
		void running (toolbox::Event::Reference e) throw (toolbox::fsm::exception::Exception);
	
		xdata::UnsignedLong    counter_;     // counter for all received messages
		//xdata::UnsignedInteger samples_;     // number of messages expected for one size measurement
		//xdata::UnsignedLong    startSize_;   // start size of the messages
		xdata::UnsignedLong    currentSize_; // the size of the currently received token message
		//xdata::UnsignedLong    endSize_;     // end size of the test
		//xdata::UnsignedLong    step_;	     // the increment of the I2O token message
		//xdata::Boolean	       stopped_;     // For checking if the server should count/discard incoming messages
		//xdata::UnsignedLong    completed_;   // For checking if all last messages of a given size are received

		//
		//! perfromamce Meters
		//
		//toolbox::PerformanceMeter * pmeter_;

		//
		//!current measurements
		//
		//xdata::Double dataBw_;
		//xdata::Double dataRate_;
		//xdata::Double dataLatency_;
		//std::set<xdaq::ApplicationDescriptor*> clients_; // remember clients descriptors
		
		toolbox::BSem mutex_;

		/*
		struct Measurement
        {
			Measurement()
			{
					latency = 0.0;
					rate = 0.0;
					bandwidth = 0.0;
			}

			Measurement(double l, double r, double b)
			{
					latency = l;
					rate = r;
					bandwidth = b;
			}

			double latency;
			double bandwidth;
			double rate;
        };
		 */

        //std::map<unsigned long, Measurement, std::less<unsigned long> > measurementHistory_;
		//std::vector<size_t> testSizeList_;
        //size_t currentSizeIndex_;

		toolbox::fsm::FiniteStateMachine fsm_;
		toolbox::TimeVal lastTime_; // used to measure time interval for measuring rate
		std::map<size_t, std::vector<double> > measurementHistory_;
		xdata::UnsignedLong numberOfSamples_;
		xdata::String sampleTime_;
		xdata::UnsignedLong  index_;     // current sample
};

#endif
