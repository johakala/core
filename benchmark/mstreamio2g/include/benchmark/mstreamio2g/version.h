// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

//
// Version definition for MStreamIO
//
#ifndef _mstreamio2g_Version_h_
#define _mstreamio2g_Version_h_

#include "config/PackageInfo.h"
// !!! Edit this line to reflect the latest package version !!!

#define MSTREAMIO2G_VERSION_MAJOR 1
#define MSTREAMIO2G_VERSION_MINOR 2
#define MSTREAMIO2G_VERSION_PATCH 0
// If any previous versions available E.g. #define MSTREAMIO2G_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#undef MSTREAMIO2G_PREVIOUS_VERSIONS


//
// Template macros
//
#define MSTREAMIO2G_VERSION_CODE PACKAGE_VERSION_CODE(MSTREAMIO2G_VERSION_MAJOR,MSTREAMIO2G_VERSION_MINOR,MSTREAMIO2G_VERSION_PATCH)
#ifndef MSTREAMIO2G_PREVIOUS_VERSIONS
#define MSTREAMIO2G_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(MSTREAMIO2G_VERSION_MAJOR,MSTREAMIO2G_VERSION_MINOR,MSTREAMIO2G_VERSION_PATCH)
#else 
#define MSTREAMIO2G_FULL_VERSION_LIST  MSTREAMIO2G_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(MSTREAMIO2G_VERSION_MAJOR,MSTREAMIO2G_VERSION_MINOR,MSTREAMIO2G_VERSION_PATCH)
#endif 

namespace mstreamio2g
{
	const std::string package  =  "mstreamio2g";
	const std::string versions =  MSTREAMIO2G_FULL_VERSION_LIST;
	const std::string summary = "Stream I/O example (N to N point to point communication)";
	const std::string description = "";
	const std::string authors = "Luciano Orsini, Andrea Petrucci";
	const std::string link = "http://xdaqwiki.cern.ch/index.php/Power_Pack";
	config::PackageInfo getPackageInfo();
	void checkPackageDependencies() throw (config::PackageInfo::VersionException);
	std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
