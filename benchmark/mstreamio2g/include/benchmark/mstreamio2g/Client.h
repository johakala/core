// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _Client_h_
#define _Client_h_

#include "xdaq/Application.h"
#include "xgi/framework/UIManager.h"

#include <set>
#include "toolbox/Task.h"
#include "toolbox/mem/Reference.h"
#include "toolbox/mem/Pool.h"
#include "toolbox/fsm/FiniteStateMachine.h"
#include "toolbox/math/random2.h"
#include "xdata/UnsignedLong.h"
#include "xdaq/ApplicationDescriptor.h"
#include "xdata/Boolean.h"
#include "xdata/String.h"
#include "xdata/Event.h"
#include "toolbox/task/Timer.h"
#include "xdata/ActionListener.h"

#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"
#include "xgi/Utils.h"
#include "xgi/Input.h"
#include "xgi/Output.h"

//AP
#include <boost/random.hpp>
#include <boost/random/lognormal_distribution.hpp>
#include <boost/random/variate_generator.hpp>
#include <cmath>

#include "toolbox/task/WorkLoop.h"

class Client: public xdaq::Application, public xgi::framework::UIManager, public xdata::ActionListener, public toolbox::task::TimerListener
{	
	public:
		XDAQ_INSTANTIATOR();

		Client(xdaq::ApplicationStub* c) throw(xdaq::exception::Exception);
		~Client();

		// start sending of token frame to all servers
		xoap::MessageReference start(xoap::MessageReference  message) throw (xoap::exception::Exception);

		// stop client from sending token frames to server
		xoap::MessageReference stop(xoap::MessageReference  message) throw (xoap::exception::Exception);

		void Default(xgi::Input *in, xgi::Output *out) throw (xgi::exception::Exception);
        void downloadMeasurements(xgi::Input *in, xgi::Output *out) throw (xgi::exception::Exception);

	protected:
	
		// Send an I2O token message to all servers, If last is 1, the flag 'last' in the
		// message is set to one. Otherwise it is set to 0
		//
		int sendMessage();

		bool process(toolbox::task::WorkLoop* wl);

		//
		// used to create the memory pool upon parametrization
		//	
		void actionPerformed (xdata::Event& e);
		void timeExpired (toolbox::task::TimerEvent& e);
		double std_deviation(std::vector<double>& samples, double mean ); 

		void stopped (toolbox::Event::Reference e) throw (toolbox::fsm::exception::Exception);
        void running (toolbox::Event::Reference e) throw (toolbox::fsm::exception::Exception);

		toolbox::BSem mutex_;
		
		//AP
		xdata::UnsignedLong stdDev_;
		xdata::UnsignedLong minFragmentSize_;
		xdata::UnsignedLong maxFragmentSize_;
		boost::variate_generator<boost::mt19937, boost::lognormal_distribution<> > * lognorm_;
		xdata::Boolean randomDestination_;  // true then use pMean_ always, false use random generator
		xdata::Boolean fixedSize_;  // true then use pMean_ always, false use random generator

		xdata::UnsignedLong counter_; 			// counts all outgoing I2O messages
		xdata::UnsignedLong currentSize_;		// The size of the current I2O token message
		xdata::UnsignedLong maxFrameSize_;		// The maximum frame size to be allocated by the Client       
		std::set<const xdaq::ApplicationDescriptor*> destination_;	// Vector of all server tids
		xdaq::ApplicationDescriptor **servers_;	// array of all server tids
		xdata::UnsignedLong committedPoolSize_;		// Total memory for messages

		xdata::Boolean createPool_;
		xdata::String poolName_;

		toolbox::math::RandomNumberEngine *random_;
		toolbox::math::UniformIntDistribution<unsigned int> *uniformDist_;

		xdata::UnsignedLong highMemoryWatermark_;	// Application should not use more bytes than specified here
		toolbox::mem::Pool* pool_;			// Memory pool for allocating messages for sending
		toolbox::fsm::FiniteStateMachine fsm_;

		toolbox::TimeVal lastTime_; // used to measure time interval for measuring rate
		std::map<size_t, std::vector<double> > measurementHistory_;
		xdata::UnsignedLong numberOfSamples_;
		xdata::String sampleTime_;
		xdata::UnsignedLong  index_;     // current sample
		xdata::UnsignedLong preAllocateBlocks_;

    	toolbox::task::WorkLoop* workLoop_;
    	toolbox::task::ActionSignature* process_;
};

#endif
