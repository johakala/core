#!/usr/bin/tclsh

set tests {
256
512
768
1024
1280
1536
1792
2048
2304
2560
2816
3072
3328
3584
3840
4096
4352
4608
4864
5120
5376
5632
5888
6144
6400
6656
6912
7168
7424
7680
7936
8192
12288
16384
20480
24064
26112
30720
32768
36864
40960
45056
51200
55296
62720
65536
86016
106496
126976
131072
151552
172032
192512
212992
233472
}



set bootline {
        { "Server" "http://dveb-b1b04-08-02.cms:1972" 11 }
        { "Server" "http://dveb-b1b04-08-04.cms:1972" 11 }
        { "Client" "http://dveb-b1b04-08-01.cms:1972" 10 }
        { "Client" "http://dveb-b1b04-08-03.cms:1972" 10 }
}


puts "start test"

foreach test $tests {
	set size [lindex $test 0]

	puts [format "testing size %d " $size]

	foreach item $bootline {
#       puts $item
        	set name [lindex $item 0]
        	set url  [lindex $item 1]
        	set lid  [lindex $item 2]

		puts [format "configuring Input on %s " $url]

		exec ./parameterset-io2g.sh $url $lid $size $name
	}


	foreach item $bootline {
	 	set name [lindex $item 0]
                set url  [lindex $item 1]
                set lid  [lindex $item 2]

                puts [format "starting Input on %s " $url]
		exec ./start-io2g.sh $url $lid

	}

	#test is running here and data is being collected
	after 60000

	 foreach item $bootline {
                set name [lindex $item 0]
                set url  [lindex $item 1]
                set lid  [lindex $item 2]
        
                puts [format "halting Input on %s " $url]
		exec ./stop-io2g.sh $url $lid

        }

	after 5000


}

foreach item $bootline {
        set name [lindex $item 0]
        set url  [lindex $item 1]
        set lid  [lindex $item 2]

        puts [format "halting Input on %s " $url]
        exec ./stop-io2g.sh $url $lid

}


puts "test finished"

