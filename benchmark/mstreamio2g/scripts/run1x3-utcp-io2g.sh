#!/usr/bin/tclsh

set tests {
256
512
1024
2048
4096
8192
16384
32768
65536
126976
256000
}



set bootline {
        { "Server" "http://srv-c2d06-17.cms:40001" 15 }
        { "Server" "http://srv-c2d06-17.cms:40002" 15 }
        { "Server" "http://srv-c2d06-17.cms:40003" 15 }
        { "Client" "http://srv-c2d06-17.cms:40000" 10 }
        
}

foreach item $bootline {
	puts $item
	set name [lindex $item 0]
	set url  [lindex $item 1]
	set lid  [lindex $item 2]
	 
	if {$name == "Client"} {
		set cmddaq ${url}/urn:xdaq-application:class=pt::utcp::Application,instance=0/init
		puts $cmddaq
	
		exec ./init-io2g.sh $cmddaq
	}	
}
 puts "waiting connections..." 
	
	after 3000

puts "start test"

foreach test $tests {
	set size [lindex $test 0]

	puts [format "testing size %d " $size]

	foreach item $bootline {
#       puts $item
        	set name [lindex $item 0]
        	set url  [lindex $item 1]
        	set lid  [lindex $item 2]

		puts [format "configuring Input on %s " $url]

		exec ./parameterset-io2g.sh $url $lid $size $name
	}


	foreach item $bootline {
	 	set name [lindex $item 0]
                set url  [lindex $item 1]
                set lid  [lindex $item 2]

                puts [format "starting Input on %s " $url]
		exec ./start-io2g.sh $url $lid

	}

	#test is running here and data is being collected
	after 60000

	 foreach item $bootline {
                set name [lindex $item 0]
                set url  [lindex $item 1]
                set lid  [lindex $item 2]
        
                puts [format "halting Input on %s " $url]
		exec ./stop-io2g.sh $url $lid

        }

	after 5000


}

foreach item $bootline {
        set name [lindex $item 0]
        set url  [lindex $item 1]
        set lid  [lindex $item 2]

        puts [format "halting Input on %s " $url]
        exec ./stop-io2g.sh $url $lid

}


puts "test finished"

