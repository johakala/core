// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "benchmark/mstreamio2g/Client.h"

#include "benchmark/mstreamio2g/i2oStreamIOMsg.h"
#include "toolbox/BSem.h"
#include "toolbox/Task.h"

#include "toolbox/rlist.h"
#include "toolbox/fsm/FiniteStateMachine.h"
#include "toolbox/mem/Reference.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "toolbox/mem/Pool.h"
#include "toolbox/mem/Reference.h"
#include "toolbox/mem/Pool.h"
#include "toolbox/mem/CommittedHeapAllocator.h"
#include "toolbox/task/TimerTask.h"
#include "toolbox/task/Timer.h"
#include "toolbox/task/TimerFactory.h"

#include "i2o/Method.h"
#include "i2o/utils/AddressMap.h"

#include "xdaq/Application.h"
#include "xdaq/ApplicationContext.h"
#include "xdaq/ApplicationGroup.h"
#include "xoap/MessageFactory.h"
#include "xdaq/NamespaceURI.h"
#include "xcept/Exception.h"
#include "xcept/tools.h"

#include "xoap/Method.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPName.h"
#include "xoap/SOAPBody.h"

#include "xgi/Method.h"
#include "xgi/framework/Method.h"
#include "xcept/Exception.h"
#include "xcept/tools.h"

#include "toolbox/task/WorkLoopFactory.h"

Client::Client (xdaq::ApplicationStub* c) throw (xdaq::exception::Exception)
	: xdaq::Application(c), xgi::framework::UIManager(this), mutex_(toolbox::BSem::FULL)
{
	preAllocateBlocks_ = 0;

	getApplicationInfoSpace()->fireItemAvailable("counter", &counter_);
	getApplicationInfoSpace()->fireItemAvailable("currentSize", &currentSize_);
	getApplicationInfoSpace()->fireItemAvailable("maxFrameSize", &maxFrameSize_);
	getApplicationInfoSpace()->fireItemAvailable("committedPoolSize", &committedPoolSize_);
	getApplicationInfoSpace()->fireItemAvailable("numberOfSamples", &numberOfSamples_);
	getApplicationInfoSpace()->fireItemAvailable("sampleTime", &sampleTime_);

	//AP
	getApplicationInfoSpace()->fireItemAvailable("StdDev", &stdDev_);
	getApplicationInfoSpace()->fireItemAvailable("MinFragmentSize", &minFragmentSize_);
	getApplicationInfoSpace()->fireItemAvailable("MaxFragmentSize", &maxFragmentSize_);
	getApplicationInfoSpace()->fireItemAvailable("preAllocateBlocks", &preAllocateBlocks_);
	getApplicationInfoSpace()->fireItemAvailable("fixedSize", &fixedSize_);
	getApplicationInfoSpace()->fireItemAvailable("randomDestination", &randomDestination_);

	getApplicationInfoSpace()->fireItemAvailable("createPool", &createPool_);
	getApplicationInfoSpace()->fireItemAvailable("poolName", &poolName_);

	// Add infospace listeners for exporting data values
	getApplicationInfoSpace()->addItemChangedListener("committedPoolSize", this);

	// Define FSM
	fsm_.addState('S', "Stopped");
	fsm_.addState('R', "Running");
	fsm_.addStateTransition('S', 'R', "start", this, &Client::running);
	fsm_.addStateTransition('R', 'S', "stop", this, &Client::stopped);
	fsm_.setInitialState('S');
	fsm_.reset();

	// Bind SOAP callbacks for control messages
	xoap::bind(this, &Client::start, "start", XDAQ_NS_URI);
	xoap::bind(this, &Client::stop, "stop", XDAQ_NS_URI);

	xgi::framework::deferredbind(this, this, &Client::Default, "Default");
	xgi::bind(this, &Client::downloadMeasurements, "downloadMeasurements");

	// initialize 
	counter_ = 0;

	createPool_ = true;
	poolName_ = "clientPool";

	randomDestination_ = false;

	try
	{
		destination_ = getApplicationContext()->getDefaultZone()->getApplicationDescriptors("Server");
		int counter = 0;
		servers_ = new xdaq::ApplicationDescriptor*[destination_.size()];
		std::set<const xdaq::ApplicationDescriptor*>::iterator iter;
		for (iter = destination_.begin(); iter != destination_.end(); iter++)
		{
			servers_[counter] = const_cast<xdaq::ApplicationDescriptor*>(*iter);
			counter++;
		}

		LOG4CPLUS_INFO(getApplicationLogger(), "Found " << destination_.size() << " servers");
	}
	catch (xdaq::exception::Exception& e)
	{
		LOG4CPLUS_FATAL(getApplicationLogger(), xcept::stdformat_exception_history(e));
		XCEPT_RAISE(xcept::Exception, "No Server application instance found. Client cannot be configured.");
	}

	this->getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");
	//Create the random object
	random_ = new toolbox::math::DefaultRandomEngine(this->getApplicationDescriptor()->getInstance());
	uniformDist_ = new toolbox::math::UniformIntDistribution<unsigned int>(0, 1);

	process_ = toolbox::task::bind(this, &Client::process, "process");

	try
	{
		std::stringstream wlss;
		wlss << "mstreamio2g-" << getApplicationDescriptor()->getInstance();
		toolbox::task::getWorkLoopFactory()->getWorkLoop(wlss.str(), "waiting")->activate();
		toolbox::task::getWorkLoopFactory()->getWorkLoop(wlss.str(), "waiting")->submit(process_);
	}
	catch ( toolbox::task::exception::Exception& e )
	{
		std::stringstream ss;
		ss << "Failed to submit workloop ";

		std::cerr << ss.str() << std::endl;
	}
	catch ( std::exception& se )
	{
		std::stringstream ss;
		ss << "Failed to submit notification to worker thread, caught standard exception '";
		ss << se.what() << "'";
		std::cerr << ss.str() << std::endl;
	}
	catch ( ... )
	{
		std::cerr << "Failed to submit notification to worker pool, caught unknown exception" << std::endl;
	}
}

Client::~Client()
{
	delete uniformDist_;
	delete random_;
}

void Client::actionPerformed (xdata::Event& event)
{
	counter_ = 0;
	//currentSize_ = 0;
	index_ = 0;

	if (event.type() == "urn:xdaq-event:setDefaultValues")
	{
		try
		{

			toolbox::task::Timer * timer = 0;
			if (!toolbox::task::getTimerFactory()->hasTimer("urn:benchmark:mstreamio2g-timer"))
			{
				timer = toolbox::task::getTimerFactory()->createTimer("urn:benchmark:mstreamio2g-timer");

			}
			else
			{
				timer = toolbox::task::getTimerFactory()->getTimer("urn:benchmark:mstreamio2g-timer");
			}

			toolbox::TimeVal startTime;
			startTime = toolbox::TimeVal::gettimeofday();
			toolbox::TimeInterval interval;
			interval.fromString(sampleTime_.toString());
			lastTime_ = toolbox::TimeVal::gettimeofday();

			timer->scheduleAtFixedRate(startTime, this, interval, 0, "CheckRateTimer");
		}
		catch (toolbox::task::exception::Exception& te)
		{
			std::cout << "Cannot run timer" << xcept::stdformat_exception_history(te) << std::endl;
		}

		try
		{
			LOG4CPLUS_INFO(getApplicationLogger(), "Committed pool size is " << (unsigned long) committedPoolSize_);
			if (createPool_)
			{
				toolbox::mem::CommittedHeapAllocator* a = new toolbox::mem::CommittedHeapAllocator(committedPoolSize_);
				toolbox::net::URN urn("toolbox-mem-pool", poolName_);
				pool_ = toolbox::mem::getMemoryPoolFactory()->createPool(urn, a);
			}
			else
			{
				toolbox::net::URN urn("toolbox-mem-pool", poolName_);
				pool_ = toolbox::mem::getMemoryPoolFactory()->findPool(urn);
			}


			toolbox::mem::Allocator* ca = dynamic_cast<toolbox::mem::Allocator *>(pool_->getAllocator());
			if (ca->isCommittedSizeSupported())
			{
				size_t committedSize = ca->getCommittedSize();
				pool_->setHighThreshold((unsigned long) (committedSize * 0.9));
				pool_->setLowThreshold((unsigned long) (committedSize * 0.7));
			}
			else
			{
				LOG4CPLUS_FATAL(getApplicationLogger(), "Failed to set threshold on pool. Pool is not a supported pool type");
			}

			std::vector<toolbox::mem::Reference*> samples(preAllocateBlocks_);
			for (size_t k = 0; k < preAllocateBlocks_; k++)
			{
				try
				{
					toolbox::mem::Reference* dref = toolbox::mem::getMemoryPoolFactory()->getFrame(pool_, maxFrameSize_);
					samples[k] = dref;
				}
				catch (toolbox::mem::exception::Exception & e)
				{
					LOG4CPLUS_FATAL(this->getApplicationLogger(), "Failed to allocate mem ref : " << xcept::stdformat_exception_history(e));
				}
			}
			for (unsigned int i = 0; i <  preAllocateBlocks_; ++i)
			{
				samples[i]->release();
			}
			

		}
		catch (toolbox::mem::exception::Exception & e)
		{
			LOG4CPLUS_FATAL(getApplicationLogger(), toolbox::toString("Could not set up memory pool, %s (exiting thread)", e.what()));
			return;
		}
	}
}

void Client::timeExpired (toolbox::task::TimerEvent& e)
{
	mutex_.take();

	toolbox::TimeVal now = toolbox::TimeVal::gettimeofday();
	double delta = (double) now - (double) lastTime_;
	double rate = counter_ / delta;
	counter_ = 0;
	lastTime_ = now;

	if ((fsm_.getCurrentState() == 'R') && ((unsigned long) index_ > 1)) // discard first two
	{
		if (measurementHistory_[currentSize_][index_ % numberOfSamples_] == 0) // fill ring and then discard measurement history
		{
			measurementHistory_[currentSize_][index_ % numberOfSamples_] = rate;
		}
	}
	index_++;

	mutex_.give();
}

bool Client::process ( toolbox::task::WorkLoop* wl )
{
	//LOG4CPLUS_DEBUG(getApplicationLogger(), "starting send loop...");

	for (;;)
	{
		mutex_.take();

		if (fsm_.getCurrentState() == 'R')
		{
			mutex_.give();

			if (!pool_->isHighThresholdExceeded())
			{
				// Stop if there is an error in sending
				if (this->sendMessage() == 1)
				{
					LOG4CPLUS_FATAL(getApplicationLogger(), toolbox::toString("Error in frameSend. Stopping client."));
					return false;
				}
			}
			else
			{
				LOG4CPLUS_DEBUG(getApplicationLogger(), "high threshold is exceeded");
				while (pool_->isLowThresholdExceeded())
				{
					LOG4CPLUS_DEBUG(getApplicationLogger(), "yield till low threshold reached");
					//this->yield(1);
					::sched_yield();
				}
			}
		}
		else
		{
			mutex_.give();
			return true;
		}
	}
	return true;
}

// These methods are called by the server
xoap::MessageReference Client::start (xoap::MessageReference message) throw (xoap::exception::Exception)
{
	LOG4CPLUS_INFO(getApplicationLogger(), toolbox::toString("start client on message size %d + i2o header", (int) currentSize_));

	if (fsm_.getCurrentState() == 'S')
	{
		toolbox::Event::Reference e(new toolbox::Event("start", this));
		fsm_.fireEvent(e);
	}
	return xoap::createMessage();
}

xoap::MessageReference Client::stop (xoap::MessageReference message) throw (xoap::exception::Exception)
{
	LOG4CPLUS_INFO(getApplicationLogger(), toolbox::toString("stop client"));
	if (fsm_.getCurrentState() == 'R')
	{
		// Received Disable from a server. Count Disable
		// messages from all servers and change to state
		// Ready, if all Disable messages have arrived.
		//
		toolbox::Event::Reference e(new toolbox::Event("stop", this));
		fsm_.fireEvent(e);
	}
	return xoap::createMessage();
}

// Send an I2O token message to all servers
// If last is 1, the flag 'last' in the
// message is set to one. Otherwise it
// is set to 0
//
int Client::sendMessage ()
{

	//char c;
	//std::cout << "enter char to send message of size (" << currentSize_ << "):";
	//std::cin >> c;

	std::set<xdaq::ApplicationDescriptor*>::iterator iter;
	LOG4CPLUS_DEBUG(getApplicationLogger(), "send message on  " << destination_.size() << " detinations");
	for (unsigned int i = 0; i < destination_.size(); i++)
	{
		unsigned int dest = i;
		if (randomDestination_)
		{
			//get the random value
			dest = (*uniformDist_)(*random_, 0, destination_.size() - 1);
		}

		toolbox::mem::Reference * ref = 0;
		try
		{

			ref = toolbox::mem::getMemoryPoolFactory()->getFrame(pool_, maxFrameSize_);

			PI2O_TOKEN_MESSAGE_FRAME frame = (PI2O_TOKEN_MESSAGE_FRAME) ref->getDataLocation();

			U32 totalSize = 0;

			if (fixedSize_)
			{
				totalSize = currentSize_;
			}
			else
			{
				totalSize = round((*lognorm_)());
				if (totalSize > maxFragmentSize_)
				{
					totalSize = maxFragmentSize_;
				}
				else
				{
					if (totalSize < minFragmentSize_)
					{
						totalSize = minFragmentSize_;
					}
				}
				//std::cout << "%%%%%%%%%%%%%%%%%%%%%%%%%% created size " << totalSize << std::endl;
			}
			totalSize += 3;
			size_t module = totalSize % 4;
			totalSize -= module;

			frame->PvtMessageFrame.StdMessageFrame.MsgFlags = 0;
			frame->PvtMessageFrame.StdMessageFrame.VersionOffset = 0;
			//frame->PvtMessageFrame.StdMessageFrame.TargetAddress    = i2o::utils::getAddressMap()->getTid(destination_[i]);
			frame->PvtMessageFrame.StdMessageFrame.TargetAddress = i2o::utils::getAddressMap()->getTid(servers_[dest]);
			frame->PvtMessageFrame.StdMessageFrame.InitiatorAddress = i2o::utils::getAddressMap()->getTid(this->getApplicationDescriptor());
			frame->PvtMessageFrame.StdMessageFrame.MessageSize = (totalSize + sizeof(I2O_TOKEN_MESSAGE_FRAME)) >> 2;

			frame->PvtMessageFrame.StdMessageFrame.Function = I2O_PRIVATE_MESSAGE;
			frame->PvtMessageFrame.XFunctionCode = I2O_TOKEN_CODE;
			frame->PvtMessageFrame.OrganizationID = XDAQ_ORGANIZATION_ID;

			ref->setDataSize(frame->PvtMessageFrame.StdMessageFrame.MessageSize << 2);
			LOG4CPLUS_DEBUG(getApplicationLogger(), "sending to tid: " << frame->PvtMessageFrame.StdMessageFrame.TargetAddress);

			//std::cout << "Message size (words)" << frame->PvtMessageFrame.StdMessageFrame.MessageSize  << " Ref size (bytes)" << ref->getDataSize() << std::endl;

			getApplicationContext()->postFrame(ref, this->getApplicationDescriptor(), servers_[dest]);

		}
		catch (toolbox::mem::exception::Exception & me)
		{
			LOG4CPLUS_FATAL(getApplicationLogger(), xcept::stdformat_exception_history(me));
			ref->release();
			return 1; // error
		}
		catch (xdaq::exception::Exception & e)
		{
			LOG4CPLUS_FATAL(getApplicationLogger(), xcept::stdformat_exception_history(e));
			ref->release();
			return 1;

		}
		counter_ = counter_ + 1;
	}
	return 0; // o.k.
}

void Client::stopped (toolbox::Event::Reference e) throw (toolbox::fsm::exception::Exception)
{
	/*
	 mutex_.take();
	 // LO delete lognorm_;
	 mutex_.give();
	 */
}

void Client::running (toolbox::Event::Reference e) throw (toolbox::fsm::exception::Exception)
{
	mutex_.take();

	/*
	 //AP BOOST!!!
	 boost::mt19937 rng;
	 rng.seed((unsigned int) this->getApplicationDescriptor()->getInstance());
	 lognorm_ = new boost::variate_generator<boost::mt19937, boost::lognormal_distribution<> >(rng, boost::lognormal_distribution<>(currentSize_,stdDev_));
	 */

	if (measurementHistory_.find(currentSize_) == measurementHistory_.end())
	{
		std::vector<double> samples;
		samples.resize(numberOfSamples_);
		for (std::vector<double>::iterator i = samples.begin(); i != samples.end(); i++)
		{
			*i = 0.0;
		}
		measurementHistory_[currentSize_] = samples;
	}
	index_ = 0;
	mutex_.give();
}

void Client::Default (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	std::stringstream measurementURL;
	measurementURL << "/" << getApplicationDescriptor()->getURN() << "/downloadMeasurements";
	*out << "<a href=\"" << measurementURL.str() << "\">" << "Download measurements" << "</a>" << std::endl;

	*out << cgicc::br() << cgicc::br() << cgicc::hr() << cgicc::br() << std::endl;

	*out << cgicc::table().set("class", "xdaq-table").set("style", "width: 100%;");

	*out << cgicc::thead();
	*out << cgicc::tr();
	*out << cgicc::th("Size");
	*out << cgicc::th("Rate Samples");
	*out << cgicc::th("Bandwidth(MB/s - IEC Standard)");
	*out << cgicc::th("Mean(Hz)");
	*out << cgicc::th("RMS(Hz)");
	*out << cgicc::tr() << std::endl;
	*out << cgicc::thead();

	*out << cgicc::tbody();

	mutex_.take();

	for (std::map<size_t, std::vector<double> >::iterator i = measurementHistory_.begin(); i != measurementHistory_.end(); i++)
	{
		*out << cgicc::tr();
		*out << cgicc::td() << (*i).first << cgicc::td();
		*out << cgicc::td();

		double avarage = 0.0;
		double throughput = 0.0;
		for (std::vector<double>::iterator j = (*i).second.begin(); j != (*i).second.end(); j++)
		{
			if (j != (*i).second.begin()) *out << ",";
			*out << (size_t) * j;

			avarage += *j;
		}

		*out << cgicc::td();
		avarage = avarage / (*i).second.size();
		throughput = avarage * (*i).first / 1000000.0;
		out->precision(5);
		*out << cgicc::td() << throughput << cgicc::td();
		*out << cgicc::td() << avarage << cgicc::td();
		*out << cgicc::td() << this->std_deviation((*i).second, avarage) << cgicc::td();

		*out << cgicc::tr();
	}

	mutex_.give();

	*out << cgicc::tbody();
	*out << cgicc::table();
}

double Client::std_deviation (std::vector<double> & samples, double mean)
{
	double sum_of_square = 0;

	for (std::vector<double>::iterator j = samples.begin(); j != samples.end(); j++)
	{
		sum_of_square += (*j - mean) * (*j - mean);
	}
	return sqrt(sum_of_square / samples.size());
}

void Client::downloadMeasurements (xgi::Input *in, xgi::Output *out) throw (xgi::exception::Exception)
{
	std::string disposition = toolbox::toString("attachment; filename=%s.ascii;", "measurements");
	out->getHTTPResponseHeader().addHeader("Content-Type", "application/csv");
	out->getHTTPResponseHeader().addHeader("Content-Disposition", disposition);
	out->getHTTPResponseHeader().addHeader("Expires", "0");
	out->getHTTPResponseHeader().addHeader("Cache-Control", "no-store, no-cache, must-revalidate, max-age=0");
	out->getHTTPResponseHeader().addHeader("Cache-Control", "post-check=0, pre-check=0");
	out->getHTTPResponseHeader().addHeader("Pragma", "no-cache");

	mutex_.take();

	*out << "#" << toolbox::toString("%s(%d)", getApplicationDescriptor()->getClassName().c_str(), (unsigned int) getApplicationDescriptor()->getInstance()) << std::endl;
	for (std::map<size_t, std::vector<double> >::iterator i = measurementHistory_.begin(); i != measurementHistory_.end(); i++)
	{
		*out << (*i).first << ",";
		for (std::vector<double>::iterator j = (*i).second.begin(); j != (*i).second.end(); j++)
		{
			if (j != (*i).second.begin()) *out << ",";
			out->precision(5);
			*out << (size_t) * j;
		}
		*out << std::endl;
	}

	mutex_.give();
}
