// $Id: Server.cc,v 1.13 2008/07/18 15:26:40 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "benchmark/mstreamio2g/Server.h"
#include "benchmark/mstreamio2g/i2oStreamIOMsg.h"

#include "xdaq/Application.h"
#include "xdaq/ApplicationContext.h"
#include "xdaq/ApplicationGroup.h"
#include "toolbox/PerformanceMeter.h"
#include "xdaq/NamespaceURI.h"

#include "toolbox/BSem.h"

#include "toolbox/rlist.h"
#include "toolbox/string.h"
#include "toolbox/mem/Reference.h"
#include "toolbox/mem/Pool.h"
#include "toolbox/task/TimerTask.h"
#include "toolbox/task/Timer.h"
#include "toolbox/task/TimerFactory.h"

#include "xdata/UnsignedLong.h"
#include "xdata/Double.h"
#include "xdata/Boolean.h"
#include "xdata/ActionListener.h"

#include "xoap/MessageFactory.h"
#include "xoap/MessageReference.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPName.h"
#include "xoap/SOAPBody.h"
#include "xoap/Method.h"

#include "xcept/Exception.h"
#include "xcept/tools.h"

#include "i2o/Method.h"
#include "xgi/framework/Method.h"
#include "i2o/utils/AddressMap.h"
#include "xgi/Method.h"

Server::Server (xdaq::ApplicationStub* c) throw (xdaq::exception::Exception)
	: xdaq::Application(c), xgi::framework::UIManager(this), mutex_(toolbox::BSem::FULL)
{
	getApplicationInfoSpace()->fireItemAvailable("counter",&counter_);
	getApplicationInfoSpace()->fireItemAvailable("currentSize",&currentSize_);
	getApplicationInfoSpace()->fireItemAvailable("numberOfSamples",&numberOfSamples_);
	getApplicationInfoSpace()->fireItemAvailable("sampleTime",&sampleTime_);


	// Define FSM
	fsm_.addState ('S', "Stopped" );
	fsm_.addState ('R', "Running");
	fsm_.addStateTransition ('S','R', "start", this, &Server::running);
	fsm_.addStateTransition ('R','S', "stop", this, &Server::stopped);
	fsm_.setInitialState('S');
	fsm_.reset();

	// Bind SOAP callbacks for control messages
	xoap::bind (this, &Server::start, "start", XDAQ_NS_URI);
	xoap::bind (this, &Server::stop,  "stop",  XDAQ_NS_URI);

	i2o::bind(this, &Server::token, I2O_TOKEN_CODE, XDAQ_ORGANIZATION_ID);

   	xgi::framework::deferredbind(this, this, &Server::Default, "Default");
   	xgi::bind(this, &Server::downloadMeasurements, "downloadMeasurements");

	this->getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");
}

void Server::actionPerformed( xdata::Event& event)
{
	counter_ = 0;
	currentSize_ = 0;
	index_ = 0;

	if (event.type() == "urn:xdaq-event:setDefaultValues")
	{
		try
		{
			toolbox::task::Timer * timer = 0;
			if (!toolbox::task::getTimerFactory()->hasTimer("urn:benchmark:mstreamio2g-timer"))
			{
				timer = toolbox::task::getTimerFactory()->createTimer("urn:benchmark:mstreamio2g-timer");
			}
			else
			{
				timer = toolbox::task::getTimerFactory()->getTimer("urn:benchmark:mstreamio2g-timer");
			}

			toolbox::TimeVal startTime;
			startTime = toolbox::TimeVal::gettimeofday();
			toolbox::TimeInterval interval;
			interval.fromString(sampleTime_.toString()); // in seconds

			lastTime_ = toolbox::TimeVal::gettimeofday();

			timer->scheduleAtFixedRate(startTime,this, interval,  0, "CheckRateTimer" );
		}
		catch (toolbox::task::exception::Exception& te)
		{
			std::cout <<  "Cannot run timer" <<  xcept::stdformat_exception_history(te) << std::endl;
		}
	}
}

// Callback function invoked for the I2O token message
// that is received from the Clients
//
void Server::token(toolbox::mem::Reference * ref)  throw (i2o::exception::Exception)
{
	mutex_.take();

	if (fsm_.getCurrentState() == 'S' )
	{
		ref->release();
		mutex_.give();
		return;
	}

	counter_ = counter_ + 1;

	LOG4CPLUS_DEBUG (getApplicationLogger(), toolbox::toString("received message number %d", (xdata::UnsignedLongT) counter_));

	//PI2O_TOKEN_MESSAGE_FRAME frame = (PI2O_TOKEN_MESSAGE_FRAME) ref->getDataLocation();   
	//size_t size = frame->PvtMessageFrame.StdMessageFrame.MessageSize << 2;
 	//size -= sizeof(I2O_TOKEN_MESSAGE_FRAME); // want to see infiniband max

	ref->release();

	mutex_.give();
}

void Server::timeExpired (toolbox::task::TimerEvent& e)
{
	mutex_.take();

	toolbox::TimeVal now = toolbox::TimeVal::gettimeofday();
	double delta = (double)now - (double)lastTime_;
	double rate = counter_ / delta;
	counter_ = 0;
	lastTime_ =  now;

	if ((fsm_.getCurrentState() == 'R')  && ((unsigned long)index_ > 1)) // discard first two
	{
		if ( measurementHistory_[currentSize_][index_ % numberOfSamples_] == 0 ) // fill ring and then discard measurement history
		{
			measurementHistory_[currentSize_][index_ % numberOfSamples_] = rate;
		}
	}
	index_++;

	mutex_.give();
}

xoap::MessageReference Server::start(xoap::MessageReference  message) throw (xoap::exception::Exception)
{
	LOG4CPLUS_INFO (getApplicationLogger(), toolbox::toString("start server on message size %d + i2o header", (int)currentSize_));
	if (fsm_.getCurrentState() == 'S' )
    {
		toolbox::Event::Reference e(new toolbox::Event("start", this));
		fsm_.fireEvent(e);
	}
	return  xoap::createMessage();
}

xoap::MessageReference Server::stop(xoap::MessageReference  message) throw (xoap::exception::Exception)
{
	LOG4CPLUS_INFO (getApplicationLogger(), toolbox::toString("stop server"));
	if (fsm_.getCurrentState() == 'R' )
    {
		toolbox::Event::Reference e(new toolbox::Event("stop", this));
		fsm_.fireEvent(e);
	}
	return  xoap::createMessage();
}

void Server::stopped (toolbox::Event::Reference e) throw (toolbox::fsm::exception::Exception)
{
	//std::cout << "Current state is: [" << fsm.getStateName (fsm.getCurrentState()) << "]" << std::endl;

	/*
	mutex_.take();
	mutex_.give();
	*/
}

void Server::running (toolbox::Event::Reference e) throw (toolbox::fsm::exception::Exception)
{
    mutex_.take();

	if ( measurementHistory_.find(currentSize_) == measurementHistory_.end())
	{
		std::vector<double> samples;
		samples.resize(numberOfSamples_);
		for ( std::vector<double>::iterator i = samples.begin(); i != samples.end(); i++ )
		{
			*i = 0.0;
		}
		measurementHistory_[currentSize_] = samples;
	}
	index_ = 0;
	mutex_.give();
}
//
// Web Navigation Pages
//
void Server::Default(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception)
{
	std::stringstream measurementURL;
	measurementURL << "/" << getApplicationDescriptor()->getURN() << "/downloadMeasurements";
	*out << "<a href=\"" << measurementURL.str() << "\">" << "Download measurements" << "</a>" << std::endl;

	*out << cgicc::br() << cgicc::br() << cgicc::hr() << cgicc::br() << std::endl;

	*out << cgicc::table().set("class", "xdaq-table").set("style", "width: 100%;");

    *out << cgicc::thead();
	*out << cgicc::tr();
	*out << cgicc::th("Size");
	*out << cgicc::th("Rate Samples");
	*out << cgicc::th("Bandwidth(MB/s - IEC Standard)");
	*out << cgicc::th("Mean(Hz)");
	*out << cgicc::th("RMS(Hz)");
	*out << cgicc::tr() << std::endl;
    *out << cgicc::thead();

    *out << cgicc::tbody();

	mutex_.take();

	for (std::map<size_t, std::vector<double> >::iterator i = measurementHistory_.begin(); i != measurementHistory_.end(); i++)
	{
		*out << cgicc::tr();
		*out << cgicc::td() << (*i).first << cgicc::td();
		*out << cgicc::td();

		double avarage = 0.0;
		double throughput = 0.0;
		for ( std::vector<double>::iterator j = (*i).second.begin(); j != (*i).second.end(); j++ )
		{
			if ( j != (*i).second.begin() )
					*out << ",";

			*out << (size_t)*j;
			avarage += *j;
		}

		*out << cgicc::td();
		avarage = avarage / (*i).second.size();
		throughput = avarage * (*i).first / 1000000.0;
		out->precision(5);
		*out << cgicc::td() << throughput << cgicc::td();
		*out << cgicc::td() << avarage << cgicc::td();
		*out << cgicc::td() << this->std_deviation((*i).second,avarage) << cgicc::td();

		*out << cgicc::tr();
	}

	mutex_.give();

	*out << cgicc::tbody();
	*out << cgicc::table();
}

double Server::std_deviation( std::vector<double> & samples, double mean)
{
    double sum_of_square = 0;

	for ( std::vector<double>::iterator j = samples.begin(); j != samples.end(); j++ )
    {
		sum_of_square += (*j - mean ) * (*j - mean );
	}
	return sqrt(sum_of_square/samples.size());
}

void Server::downloadMeasurements(xgi::Input *in, xgi::Output *out) throw (xgi::exception::Exception)
{
	std::string disposition = toolbox::toString("attachment; filename=%s.ascii;","measurements");
	out->getHTTPResponseHeader().addHeader("Content-Type", "application/csv");
	out->getHTTPResponseHeader().addHeader("Content-Disposition", disposition);
	out->getHTTPResponseHeader().addHeader("Expires", "0");
	out->getHTTPResponseHeader().addHeader("Cache-Control", "no-store, no-cache, must-revalidate, max-age=0");
	out->getHTTPResponseHeader().addHeader("Cache-Control", "post-check=0, pre-check=0");
	out->getHTTPResponseHeader().addHeader("Pragma", "no-cache");

 	mutex_.take();
 	*out << "#" <<  toolbox::toString("%s(%d)",getApplicationDescriptor()->getClassName().c_str(),(unsigned int)getApplicationDescriptor()->getInstance())  << std::endl;

	for (std::map<size_t, std::vector<double> >::iterator i = measurementHistory_.begin(); i != measurementHistory_.end(); i++)
	{
		*out << (*i).first << ",";
		for ( std::vector<double>::iterator j = (*i).second.begin(); j != (*i).second.end(); j++ )
		{
			if ( j != (*i).second.begin() )
					*out << ",";
			out->precision(5);
			*out << (size_t)*j;

		}

		*out << std::endl;
	}

    mutex_.give();
}
