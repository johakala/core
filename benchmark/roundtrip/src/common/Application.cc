// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "benchmark/roundtrip/Application.h"
#include "xgi/Method.h"
#include "i2o/Method.h"
#include "i2o/utils/AddressMap.h"
#include "pt/PeerTransportAgent.h"
#include "xoap/Method.h"
#include "xdaq/NamespaceURI.h"
#include "xoap/MessageReference.h"
#include "xoap/MessageFactory.h"
#include "xoap/SOAPPart.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPBody.h"
#include "xoap/domutils.h"
#include "xcept/tools.h"


XDAQ_INSTANTIATOR_IMPL(roundtrip::Application)

roundtrip::Application::Application(xdaq::ApplicationStub* stub)
	throw (xdaq::exception::Exception)
	: xdaq::Application(stub), xgi::framework::UIManager(this)
{	
	// export paramater to run control, direct binding run control can read
        // and write all exported variables directly (no need for get and put
        // from the user).

	committedPoolSize_ = 0x500000; // 1Mb

        getApplicationInfoSpace()->fireItemAvailable("counter",&counter_);
        getApplicationInfoSpace()->fireItemAvailable("samples",&samples_);
        getApplicationInfoSpace()->fireItemAvailable("startSize",&startSize_);
        getApplicationInfoSpace()->fireItemAvailable("endSize",&endSize_);
        getApplicationInfoSpace()->fireItemAvailable("currentSize",&currentSize_);
        getApplicationInfoSpace()->fireItemAvailable("dataBw",&dataBw_);
        getApplicationInfoSpace()->fireItemAvailable("dataRate",&dataRate_);
        getApplicationInfoSpace()->fireItemAvailable("dataLatency",&dataLatency_);	       
        getApplicationInfoSpace()->fireItemAvailable("step",&step_);
        getApplicationInfoSpace()->fireItemAvailable("pipeLine",&pipeLine_);
        getApplicationInfoSpace()->fireItemAvailable("committedPoolSize",&committedPoolSize_);

	// Which other roundtrip application will be the data sink
	getApplicationInfoSpace()->fireItemAvailable("destinationInstance", &destinationInstance_);

	i2o::bind (this, &roundtrip::Application::token, I2O_TOKEN_CODE, XDAQ_ORGANIZATION_ID);
	errorHandler_ = toolbox::exception::bind (this, &roundtrip::Application::onError, "onError");

	// Add infospace listeners for exporting data values
	getApplicationInfoSpace()->addItemRetrieveListener ("dataBw", this);
	getApplicationInfoSpace()->addItemRetrieveListener ("dataRate", this);
	getApplicationInfoSpace()->addItemRetrieveListener ("dataLatency", this);
	
	// Define FSM
	fsm_.addState ('H', "Halted");
	fsm_.addState ('R', "Ready");
	fsm_.addState ('E', "Enabled");
	fsm_.addStateTransition ('H','R', "Configure", this, &roundtrip::Application::ConfigureAction);
	fsm_.addStateTransition ('R','E', "Enable", this, &roundtrip::Application::EnableAction);
	fsm_.addStateTransition ('E','H', "Halt", this, &roundtrip::Application::HaltAction);
	fsm_.addStateTransition ('R','H', "Halt", this, &roundtrip::Application::HaltAction);
	fsm_.setInitialState('H');
	fsm_.reset();

	// Bind SOAP callbacks for control messages
	xoap::bind (this, &roundtrip::Application::fireEvent, "Configure", XDAQ_NS_URI);
	xoap::bind (this, &roundtrip::Application::fireEvent, "Enable", XDAQ_NS_URI);
	xoap::bind (this, &roundtrip::Application::fireEvent, "Halt", XDAQ_NS_URI);
	
	// Bind CGI callbacks
	xgi::bind(this, &roundtrip::Application::dispatch, "dispatch");
        xgi::framework::deferredbind(this, this, &roundtrip::Application::Default, "Default");


	// Define Web state machine
	wsm_.addState('H', "Halted",    this, &roundtrip::Application::stateMachinePage);
	wsm_.addState('R', "Ready",     this, &roundtrip::Application::stateMachinePage);
	wsm_.addState('E', "Enabled",   this, &roundtrip::Application::stateMachinePage);
	wsm_.addStateTransition('H','R', "Configure", this, &roundtrip::Application::Configure, &roundtrip::Application::failurePage);
	wsm_.addStateTransition('R','E', "Enable",    this, &roundtrip::Application::Enable,    &roundtrip::Application::failurePage);
	wsm_.addStateTransition('R','H', "Halt",      this, &roundtrip::Application::Halt,      &roundtrip::Application::failurePage);
	wsm_.addStateTransition('E','H', "Halt",      this, &roundtrip::Application::Halt,      &roundtrip::Application::failurePage);
	wsm_.setInitialState('H');
	
	xgi::framework::deferredbind(this, this, &roundtrip::Application::displayMeasurements, "displayMeasurements");
	xgi::bind(this, &roundtrip::Application::downloadMeasurements, "downloadMeasurements");
}	

//
// SOAP Callback trigger state change 
//
xoap::MessageReference roundtrip::Application::fireEvent (xoap::MessageReference msg) throw (xoap::exception::Exception)
{
	xoap::SOAPPart part = msg->getSOAPPart();
	xoap::SOAPEnvelope env = part.getEnvelope();
	xoap::SOAPBody body = env.getBody();
	DOMNode* node = body.getDOMNode();
	DOMNodeList* bodyList = node->getChildNodes();
	for (unsigned int i = 0; i < bodyList->getLength(); i++) 
	{
		DOMNode* command = bodyList->item(i);

		if (command->getNodeType() == DOMNode::ELEMENT_NODE)
		{
			std::string commandName = xoap::XMLCh2String (command->getLocalName());

			try 
			{
				toolbox::Event::Reference e(new toolbox::Event(commandName, this));
				fsm_.fireEvent(e);
				// Synchronize Web state machine
				wsm_.setInitialState(fsm_.getCurrentState());
			}
			catch (toolbox::fsm::exception::Exception & e)
			{
				XCEPT_RETHROW(xcept::Exception, "invalid command", e);
			}

			xoap::MessageReference reply = xoap::createMessage();
			xoap::SOAPEnvelope envelope = reply->getSOAPPart().getEnvelope();
			xoap::SOAPName responseName = envelope.createName( commandName +"Response", "xdaq", XDAQ_NS_URI);
			envelope.getBody().addBodyElement ( responseName );
			return reply;
		}
	}

	XCEPT_RAISE(xoap::exception::Exception,"command not found");		
}
	
bool roundtrip::Application::onError ( xcept::Exception& ex, void * context )
{
	std::cout << "onError: " << ex.what() << std::endl;
	return false;
}
	
//
// This function is invoked whenever a message comes
// in - both on the instance 0 and instance 1
//
void roundtrip::Application::token(  toolbox::mem::Reference * ref)  throw (i2o::exception::Exception) 
{
	LOG4CPLUS_DEBUG (getApplicationLogger(), toolbox::toString("roundtrip::Application::token : instance %d  counter %d  samples %d",getApplicationDescriptor()->getInstance(), (unsigned long) counter_, (unsigned long) samples_));
	counter_ = counter_ + 1;

   	if ( ((this->getApplicationDescriptor()->getInstance() == this->destinationInstance_ )   || (this->currentSize_ <= this->endSize_)) 
		 && (( fsm_.getCurrentState() == 'R' ) || (fsm_.getCurrentState() == 'E' )))		
	{
		// retrieve pointer to frame
		PI2O_TOKEN_MESSAGE_FRAME frame = (PI2O_TOKEN_MESSAGE_FRAME) ref->getDataLocation();
		
		 // std::cout << "Transaction context: " << frame->PvtMessageFrame.TransactionContext << std::endl;

		if ( pmeter_.addSample(currentSize_*2) && (this->getApplicationDescriptor()->getInstance() != this->destinationInstance_) ) 
		{	
			LOG4CPLUS_INFO(getApplicationLogger(), toolbox::toString("latency: %f",this->pmeter_.latency()/2.0));
			LOG4CPLUS_INFO(getApplicationLogger(), toolbox::toString("rate: %f",this->pmeter_.rate()));
			LOG4CPLUS_INFO(getApplicationLogger(), toolbox::toString("bandwidth: %f",this->pmeter_.bandwidth()));

			measurementHistory_[currentSize_] = Measurement(this->pmeter_.latency()/2.0, this->pmeter_.rate(), this->pmeter_.bandwidth());

			this->currentSize_ = this->currentSize_ + this->step_;

			if (this->currentSize_ > this->endSize_)
			{
				LOG4CPLUS_INFO(getApplicationLogger(), toolbox::toString("RoundTrip done: currentSize_:%d endSize_:%d state: %c", (unsigned long) currentSize_, (unsigned long) endSize_, fsm_.getCurrentState()));

				// free frame and stop
				ref->release();
				return;
			}

			LOG4CPLUS_INFO(getApplicationLogger(),toolbox::toString("save measurement and go to next size: %d", (unsigned long) currentSize_));

			//frame->PvtMessageFrame.StdMessageFrame.MessageSize  = currentSize_ >> 2;
		}	

		if  (getApplicationDescriptor()->getInstance() != this->destinationInstance_) 
		{
			frame->PvtMessageFrame.StdMessageFrame.MessageSize  = this->currentSize_ >> 2;
		}

		// Add the counter to the transaction context
		frame->PvtMessageFrame.TransactionContext = counter_;

		// swap source and destination and send
		int destinationTID = frame->PvtMessageFrame.StdMessageFrame.InitiatorAddress;
		int initiatorTID   = frame->PvtMessageFrame.StdMessageFrame.TargetAddress;
		frame->PvtMessageFrame.StdMessageFrame.InitiatorAddress = initiatorTID;
		frame->PvtMessageFrame.StdMessageFrame.TargetAddress    = destinationTID;
		ref->setDataSize(frame->PvtMessageFrame.StdMessageFrame.MessageSize << 2);
	  	try 
		{			
			const xdaq::ApplicationDescriptor* d = i2o::utils::getAddressMap()->getApplicationDescriptor(destinationTID);
			this->getApplicationContext()->postFrame(ref, this->getApplicationDescriptor(), d);
			//this->getApplicationContext()->postFrame(ref, this->getApplicationDescriptor(), destination, errorHandler_, 0);
		} 
		catch (xdaq::exception::Exception& xe)
		{
		  std::cout << xcept::stdformat_exception_history(xe) << std::endl;
			LOG4CPLUS_ERROR (this->getApplicationLogger(), toolbox::toString("Error caught in Application: %s",xcept::stdformat_exception_history(xe).c_str()));
		}

	} 
	else 
	{
		LOG4CPLUS_INFO(getApplicationLogger(), toolbox::toString("RoundTrip done: currentSize_:%d endSize_:%d state: %c", (unsigned long) currentSize_, (unsigned long) endSize_, fsm_.getCurrentState()));

		// free frame and stop
		ref->release();

		/// This code is not yet tested and could break the system!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		// destory pool
		//toolbox::mem::getManager()->removePool("RoundtripSendPool");

		// delete all unused pools
		//toolbox::mem::getManager()->cleanup();
	}
	return;
} // end of token function
	
	
//
// run control requests current paramater values
//
void roundtrip::Application::actionPerformed (xdata::Event& e) 
{ 
	// update measurements monitors		
	if (e.type() == "ItemRetrieveEvent")
	{
		std::string item = dynamic_cast<xdata::ItemRetrieveEvent&>(e).itemName();
		if ( item == "dataBw")
		{
			dataBw_ =  pmeter_.bandwidth();
			LOG4CPLUS_INFO(getApplicationLogger(), toolbox::toString("bandwidth: %f",pmeter_.bandwidth()));
		} else if ( item == "dataLatency")
		{
			dataLatency_ =  pmeter_.latency();
			LOG4CPLUS_INFO(getApplicationLogger(), toolbox::toString("latency: %f",pmeter_.latency()));
		} else if ( item == "dataRate")
		{
			dataRate_ =  pmeter_.rate();
			LOG4CPLUS_INFO(getApplicationLogger(), toolbox::toString("rate: %f",pmeter_.rate()));
		}
	}	
}


	
void roundtrip::Application::ConfigureAction(toolbox::Event::Reference e) throw (toolbox::fsm::exception::Exception)
{
	measurementHistory_.clear();
	
	try
	{
		toolbox::mem::CommittedHeapAllocator* a = new toolbox::mem::CommittedHeapAllocator(committedPoolSize_);
		toolbox::net::URN urn("toolbox-mem-pool", "RoundtripSendPool");
		pool_ = toolbox::mem::getMemoryPoolFactory()->createPool(urn, a);
	} 
	catch (toolbox::mem::exception::Exception& e)
	{
		LOG4CPLUS_DEBUG (getApplicationLogger(), e.what());
	}

	// instance 1 must not be configured or enabled.
	//
	if ( getApplicationDescriptor()->getInstance() == destinationInstance_ ) 
	{
		return;
	}

	counter_ = 0;
	
	LOG4CPLUS_INFO (getApplicationLogger(),toolbox::toString("RoundTrip::Configure :Parameter counter: %d", (unsigned long) counter_));
	LOG4CPLUS_INFO (getApplicationLogger(), toolbox::toString("RoundTrip::Configure :Parameter samples: %d", (unsigned long) samples_));
	LOG4CPLUS_INFO (getApplicationLogger(), toolbox::toString("RoundTrip::Configure :Parameter size: %d",    (unsigned long) startSize_));
	LOG4CPLUS_INFO (getApplicationLogger(),toolbox::toString("RoundTrip::Configure :Parameter size: %d",    (unsigned long) endSize_));

	destination_ = getApplicationContext()->getDefaultZone()->getApplicationDescriptor("roundtrip::Application", (unsigned long) destinationInstance_);
		
}
	
void roundtrip::Application::HaltAction(toolbox::Event::Reference e) throw (toolbox::fsm::exception::Exception )
{	

}

void roundtrip::Application::EnableAction(toolbox::Event::Reference e) throw (toolbox::fsm::exception::Exception )
{	
	// instance 1 must not be configured or enabled.
	//
	if ( getApplicationDescriptor()->getInstance() == destinationInstance_ ) 
	{
		return;
	}

	dataBw_  = 0.0;
	dataRate_ = 0.0;
	dataLatency_ = 0.0;
	pmeter_.init(samples_);

	LOG4CPLUS_INFO (getApplicationLogger(), toolbox::toString("Configuring, from state: %c", fsm_.getCurrentState()));

	// Only do this, if this application is not a
	// data sink instance
	//
	if (getApplicationDescriptor()->getInstance() != destinationInstance_) 
	{
		counter_ = 0;

		// increase size to contain header info
		currentSize_ = sizeof(I2O_TOKEN_MESSAGE_FRAME) + startSize_;

		// actual end size
		endSize_ = endSize_ + sizeof(I2O_TOKEN_MESSAGE_FRAME);

		for (unsigned int i=0; i< pipeLine_; i++ ) 
		{
			LOG4CPLUS_INFO(getApplicationLogger(), toolbox::toString("push token size %s into pipe: %d", currentSize_.toString().c_str(), i));

			toolbox::mem::Reference* ref = 0;
			try {
			// allocate frame 
				 ref = toolbox::mem::getMemoryPoolFactory()->getFrame(pool_, endSize_);
			}
			catch(toolbox::mem::exception::Exception & e)
			{
				XCEPT_RETHROW (toolbox::fsm::exception::Exception, "failed to get frame",  e);
			}
			// prepare frame and send
			PI2O_TOKEN_MESSAGE_FRAME frame = (PI2O_TOKEN_MESSAGE_FRAME) ref->getDataLocation(); 
			
			// Add the counter to the transaction context
			frame->PvtMessageFrame.TransactionContext = counter_;  

			frame->PvtMessageFrame.StdMessageFrame.MsgFlags         = 0;
			frame->PvtMessageFrame.StdMessageFrame.VersionOffset    = 0;
			frame->PvtMessageFrame.StdMessageFrame.TargetAddress    = i2o::utils::getAddressMap()->getTid(destination_);
			frame->PvtMessageFrame.StdMessageFrame.InitiatorAddress = i2o::utils::getAddressMap()->getTid(this->getApplicationDescriptor());
			frame->PvtMessageFrame.StdMessageFrame.MessageSize      = currentSize_ >> 2;

			frame->PvtMessageFrame.StdMessageFrame.Function = I2O_PRIVATE_MESSAGE;
			frame->PvtMessageFrame.XFunctionCode            = I2O_TOKEN_CODE;
			frame->PvtMessageFrame.OrganizationID           = XDAQ_ORGANIZATION_ID;

			ref->setDataSize(currentSize_);

			try 
			{
				//this->getApplicationContext()->postFrame(ref, config()->tid(), destination_, errorHandler_, 0);
				getApplicationContext()->postFrame(ref, this->getApplicationDescriptor(), destination_);
			} 
			catch (xdaq::exception::Exception& xe)
			{
				LOG4CPLUS_ERROR (getApplicationLogger(), "Error caught in rountrip sender" +  xcept::stdformat_exception_history(xe)) ;
			}

		}
	}   
}


// XGI Call back

void roundtrip::Application::Default(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception)
{
	wsm_.displayPage(out);
}

// WSM Dispatcher function
void roundtrip::Application::dispatch (xgi::Input * in, xgi::Output * out)  throw (xgi::exception::Exception)
{
	try
	{
		cgicc::Cgicc cgi(in);
    		//const cgicc::CgiEnvironment& env = cgi.getEnvironment();
		cgicc::const_form_iterator stateInputElement = cgi.getElement("StateInput");
		std::string stateInput = (*stateInputElement).getValue();
		wsm_.fireEvent(stateInput,in,out);
	}
	catch (std::exception& e)
	{
		XCEPT_RAISE (xgi::exception::Exception, e.what());
	}
}


//
// Web Events that trigger state changes (result of wsm::fireEvent)
//
void roundtrip::Application::Configure(xgi::Input * in ) throw (xgi::exception::Exception)
{
	try 
	{
		toolbox::Event::Reference e(new toolbox::Event("Configure", this));
		fsm_.fireEvent(e);
	}
	catch (toolbox::fsm::exception::Exception & e)
	{
		XCEPT_RETHROW(xgi::exception::Exception, "invalid command", e);
	}
}
	
void roundtrip::Application::Enable(xgi::Input * in ) throw (xgi::exception::Exception)
{
	try 
	{
		toolbox::Event::Reference e(new toolbox::Event("Enable", this));
		fsm_.fireEvent(e);
	}
	catch (toolbox::fsm::exception::Exception & e)
	{
		XCEPT_RETHROW(xgi::exception::Exception, "invalid command", e);
	}
}

void roundtrip::Application::Halt(xgi::Input * in ) throw (xgi::exception::Exception)
{
	try 
	{
		toolbox::Event::Reference e(new toolbox::Event("Halt", this));
		fsm_.fireEvent(e);
	}
	catch (toolbox::fsm::exception::Exception & e)
	{
		XCEPT_RETHROW(xgi::exception::Exception, "invalid command", e);
	}
}
	
	
//
// Web Navigation Pages
//
void roundtrip::Application::stateMachinePage( xgi::Output * out ) throw (xgi::exception::Exception)
{
	std::string url = "/";
	url += getApplicationDescriptor()->getURN();
	url += "/dispatch";	

	// display FSM
  	std::set<std::string> possibleInputs = wsm_.getInputs(wsm_.getCurrentState());
        std::set<std::string> allInputs = wsm_.getInputs();


        *out << cgicc::h3("Finite State Machine").set("style", "font-family: arial") << std::endl;
        *out << "<table border cellpadding=10 cellspacing=0>" << std::endl;
        *out << "<tr>" << std::endl;
        *out << "<th>" << wsm_.getStateName(wsm_.getCurrentState()) << "</th>" << std::endl;
        *out << "</tr>" << std::endl;
        *out << "<tr>" << std::endl;
       	std::set<std::string>::iterator i;
        for ( i = allInputs.begin(); i != allInputs.end(); i++)
        {
                *out << "<td>"; 
		*out << cgicc::form().set("method","get").set("action", url).set("enctype","multipart/form-data") << std::endl;

                if ( possibleInputs.find(*i) != possibleInputs.end() )
                {
                        *out << cgicc::input().set("type", "submit").set("name", "StateInput").set("value", (*i) );
                }
                else
                {
                       	*out << cgicc::input() .set("type", "submit").set("name", "StateInput").set("value", (*i) ).set("disabled", "true");
                }

                *out << cgicc::form();
                *out << "</td>" << std::endl;
        }

        *out << "</tr>" << std::endl;
        *out << "</table>" << std::endl;

	// Give some access to the so far performed measurements
	*out << cgicc::hr() << std::endl;
	*out << " Measurements: " << std::endl;
	std::string measurementURL = "/";
	measurementURL += getApplicationDescriptor()->getURN();
	measurementURL += "/displayMeasurements";
	*out << "<a href=\"" << measurementURL << "\">" << "display"
	     << "</a>" << std::endl;
	measurementURL = "/";
	measurementURL += getApplicationDescriptor()->getURN();
	measurementURL += "/downloadMeasurements";
	*out << "<a href=\"" << measurementURL << "\">" << "download"
	     << "</a>" << std::endl;
	// End the page gracefully
}
	
//
// Failure Pages
//
void roundtrip::Application::failurePage(xgi::Output * out, xgi::exception::Exception & e)  throw (xgi::exception::Exception)
{

	*out << cgicc::br() << e.what() << cgicc::br() << std::endl;
	std::string url = "/";
	url += getApplicationDescriptor()->getURN();
	
	*out << cgicc::br() << "<a href=\"" << url << "\">" << "retry" << "</a>" << cgicc::br() << std::endl;

}

//
// Display measurements Pages
//
void roundtrip::Application::displayMeasurements(xgi::Input * in, xgi::Output * out)  throw (xgi::exception::Exception)
{
	if ( currentSize_ <= endSize_ )
	{
		*out << cgicc::h3("Still taking measurements...") << std::endl;
		*out << "Currently processing message size " << currentSize_ << "<br>" << std::endl;
		*out << "Last message size is " << endSize_ << "<p>" << std::endl;
		*out << "Please reload this page later again" << std::endl;
		//xgi::Utils::getPageFooter(*out);
		//return;
	}
	
	*out << cgicc::table()
			.set("cellpadding","5")
			.set("cellspacing","0")
			.set("border","")
			.set("style","text-align: left; width: 100%; background-color: rgb(255,255,255);");

	*out << cgicc::tbody();

	// Write the header line: Class, Instance, Id, Host, Properties
	*out << cgicc::tr();
	*out << cgicc::td("Size").set("style","vertical-align: top; font-weight: bold;");
	*out << cgicc::td("Latency").set("style","vertical-align: top; font-weight: bold;");
	*out << cgicc::td("Rate").set("style","vertical-align: top; font-weight: bold;");
	*out << cgicc::td("Bandwidth").set("style","vertical-align: top; font-weight: bold;");
	*out << cgicc::tr() << std::endl;

	std::map<unsigned long, Measurement, std::less<unsigned long> >::iterator i;
	for (i = measurementHistory_.begin(); i != measurementHistory_.end(); i++)
	{	
		*out << cgicc::tr();
		*out << cgicc::td().set("style","vertical-align: top; font-weight: bold;") << (*i).first << cgicc::td();
		*out << cgicc::td().set("style","vertical-align: top; font-weight: bold;") << (*i).second.latency << cgicc::td();
		*out << cgicc::td().set("style","vertical-align: top; font-weight: bold;") << (*i).second.rate << cgicc::td();
		*out << cgicc::td().set("style","vertical-align: top; font-weight: bold;") << (*i).second.bandwidth << cgicc::td();
		*out << cgicc::tr();
	}
	*out << cgicc::tbody();
	*out << cgicc::table();
	*out << "<p>" << std::endl;
}

void roundtrip::Application::downloadMeasurements(xgi::Input *in, xgi::Output *out) throw (xgi::exception::Exception)
{
  std::string disposition = toolbox::toString("attachment; filename=%s.ascii;","measurements");
  out->getHTTPResponseHeader().addHeader("Content-Type", "application/csv");
  out->getHTTPResponseHeader().addHeader("Content-Disposition", disposition);
  out->getHTTPResponseHeader().addHeader("Expires", "0");
  out->getHTTPResponseHeader().addHeader("Cache-Control", "no-store, no-cache, must-revalidate, max-age=0");
  out->getHTTPResponseHeader().addHeader("Cache-Control", "post-check=0, pre-check=0");
  out->getHTTPResponseHeader().addHeader("Pragma", "no-cache");

  std::map<unsigned long, Measurement, std::less<unsigned long> >::iterator i;
  for (i = measurementHistory_.begin(); i != measurementHistory_.end(); i++)
    {
      std::string l = toolbox::toString("%9d %14.6e %14.6e %14.6e\n",
					(*i).first,(*i).second.latency,(*i).second.rate,(*i).second.bandwidth);
      *out << l;
    }
}
