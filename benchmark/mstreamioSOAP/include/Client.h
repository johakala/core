// $Id: Client.h,v 1.6 2004/09/02 09:59:28 xdaq Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef Client_h_
#define Client_h_

#include "xdaq.h"

//#include "StreamIOListener.h"

#include <list>
#include <algorithm>
#include "PerformanceMeter.h"

#include "Plot.h"
#include "BSem.h"
#include "rlist.h"
#include <vector>

#include "Task.h"

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/dom/DOM.hpp>

#include <string>
//#include <strstream>
#include <iostream>
#include <fstream>

#ifdef linux
using namespace std;
#endif

using namespace toolbox;

#include "SOAPMessage.h"
#include "SOAPPart.h"
#include "SOAPEnvelope.h"
#include "SOAPBody.h"
#include "SOAPBodyElement.h"
#include "SOAPName.h"
#include "AttachmentPart.h"
#include "SOAPAllocator.h"

//
// Test:
// Send a SOAP message with an attachment of n bytes
// to a number of servers. The throughput is measured
// on the server side. The response to the Client is
// an empty SOAP message.
//

class Client: public xdaqApplication, public Task
{	
	public:
	
	vector<SOAPMessage> msg_; // messages to be sent
	
	Client(): Task("Client") 
	{
		exportParam("counter",counter_);
		exportParam("samples",samples_);
		exportParam("startSize",startSize_);
		exportParam("endSize",endSize_);
		exportParam("currentSize",currentSize_);
		exportParam("step",step_);
		
	}
	
	
	void plugin() 
	{
		LOG4CPLUS_INFO(logger_, toString("plugin Client..."));
	}
	
	int svc()
	{
		counter_ = 0;

		createMessage (currentSize_);

		for (unsigned int i = 0; i < samples_; i++)
		{
			
			// Stop if there is an error in sending
			int retVal = this->sendMessage();
				
			if (retVal == 1) return 1; // error

				counter_++;
		} 
		
		// Change parameters to next size
		currentSize_ += step_;
				
		if (currentSize_ > endSize_) 
		{
			// END of test

			return 0;
		}
		createMessage (currentSize_);
		
		// Change state to ready, so the
		// server can enable again.
		
		this->state (Ready);

		cout << "Next size is: " << currentSize_ << endl;

		return 0;
	}
	
	
			
			
	void ParameterSetDefault (list<string> & paramNames) 
	{
	}
	

        //
	// run control send new paramater values. system is started by
	// peer instance 0 upon reception of the paramsSet command from
        // the run control.
        //
	void ParameterSet (list<string> & paramNames)
	{          
       	}
	
	//
	// run control requests current paramater values
	//
	void ParameterGet (list<string> & paramNames)   
	{ 
	}
	
	void Configure() throw (xdaqException)
	{
		counter_ = 0;
		LOG4CPLUS_INFO (logger_, toString("Configure :Parameter counter: %d", counter_));
		LOG4CPLUS_INFO (logger_, toString("Configure :Parameter samples: %d", samples_));
		LOG4CPLUS_INFO (logger_, toString("Configure :Parameter start size: %d",    startSize_));
		LOG4CPLUS_INFO (logger_, toString("Configure :Parameter end size: %d",    endSize_));
		LOG4CPLUS_INFO (logger_, toString("Configure :Parameter numframes: %d",    numFrames_));

		//
		// Put all destination instance TIDs into a vector
		// by querying the class name "Server"
		//
		int num = xdaq::getNumInstances("Server");
				
		cout << "-------------- Find Server tids -------------" << endl;
		for (int i = 0; i < num; i++)
		{			
			int tid = xdaq::getTid ("Server", i); 
			cout << "Configuring Server tid: " << tid << endl;
			destination_.push_back (tid);			
		}
		cout << "---------------------------------------------" << endl; 
		
		currentSize_ = startSize_;
		enableCount_ = 0; // count Enable coming from all servers
				
	}
	
	void Enable() throw (xdaqException)
	{
		LOG4CPLUS_INFO (logger_, toString("Configuring, from state: %d", state()));
		
		enableCount_++;
		
		if ( enableCount_ == destination_.size())
		{
			this->activate();
			enableCount_ = 0;
		}
	}
	
	void createMessage (unsigned long size)
	{
		msg_.erase(msg_.begin(), msg_.end());
		
		for (unsigned int i = 0; i < destination_.size(); i++)
		{
			//
        		// Create SOAP message
			//
			SOAPMessage msg;
		
			SOAPPart soap = msg.getSOAPPart();
			SOAPEnvelope envelope = soap.getEnvelope();
			SOAPBody body = envelope.getBody();
	
        		SOAPName command = envelope.createName("token");
			SOAPBodyElement bodyElement = body.addBodyElement(command);
			bodyElement.addTextNode("SOAP message with an attachment");
		
			SOAPName originator = envelope.createName("originator");
			SOAPName targetAddr = envelope.createName("targetAddr");
		
			//
			// Add originator and target identifiers to the message.
			// These are XDAQ internal attributes that are needed
			// to establish a path between two XDAQ application peers.
			//
			xtuple::Integer localTid  (tid());
			bodyElement.addAttribute (originator, localTid.toString() );
			xtuple::Integer serverTid (destination_[i]);		
			bodyElement.addAttribute (targetAddr, serverTid.toString() );
		
			//
        		// Create attachment
			//
        		AttachmentPart * attachment = msg.createAttachmentPart();

        		char * buf = new char [size];
			
			// Need to initialize buffer, otherwise
			// SOAP leftovers may corrupt the message by
			// '--' mime boundary marks that appear in
			// uninitialized memory areas.
			//
			memset (buf, 'x', size);
			
        		attachment->setContent(buf, size, "content/unknown");

        		msg.addAttachmentPart(attachment);
 
        		//
        		// Write message to screen
			//
        		//string s;
        		//msg.writeTo(s);
        		//cout << s << endl;

        		delete buf;
		
			msg_.push_back(msg);
		}
	}
	
	int sendMessage()
	{
		
		
		//
		// SEND THE MESSAGE HERE
		//
		for (unsigned int i = 0; i < msg_.size(); i++)
		{
			SOAPMessage reply = xdaq::frameSend(msg_[i]);
		
			// Dump the reply to stdout
			//
			//cout << endl;
			//reply.writeTo(cout);
			//cout << endl;
				
			// Check, if there is a fault in the reply
			//
			SOAPBody rb = reply.getSOAPPart().getEnvelope().getBody();
		
			if (rb.hasFault() )
			{
				SOAPFault fault = rb.getFault();
				string msg = "Server: ";
				msg += fault.getFaultString();
				cout << "Error in sending: " << msg << endl;
				return 1;
			} 
		}	
		return 0;
	}
	
	
	
	protected:
	
	// some paramters
       unsigned long counter_;
       unsigned long samples_;
       unsigned long startSize_;
       unsigned long endSize_;
       unsigned long currentSize_;
       vector<int> destination_;
       unsigned long numFrames_;
       unsigned long step_;
       unsigned long enableCount_;
};

#endif
