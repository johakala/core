// $Id: Server.h,v 1.5 2004/09/02 09:59:28 xdaq Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _Server_h_
#define _Server_h_

#include "xdaq.h"

//#include "StreamIOListener.h"

#include <list>
#include <algorithm>
#include "PerformanceMeter.h"

#include "Plot.h"
#include "BSem.h"
#include "rlist.h"

#include "Task.h"

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/dom/DOM.hpp>

#include <string>
//#include <strstream>
#include <iostream>
#include <fstream>

#ifdef linux
using namespace std;
#endif

#include "SOAPMessage.h"
#include "SOAPPart.h"
#include "SOAPEnvelope.h"
#include "SOAPBody.h"
#include "SOAPBodyElement.h"
#include "SOAPName.h"
#include "AttachmentPart.h"
#include "SOAPAllocator.h"


//
// Task to send Enable to all the Clients
//
class EnableTask: public Task
{
	public:
	
	int tid_;
	
	EnableTask(int tid):Task("EnableTask")
	{
		tid_ = tid;
	}
	
	
	int svc()
	{
		// wait for 3 seconds before activating clients
		sleep(3);
		int num = xdaq::getNumInstances("Client");
		
		for (int i = 0; i < num; i++)
		{			
			int tid = xdaq::getTid ("Client", i); 		

	
			//
        		// Create SOAP message
			//
			SOAPMessage msg;
		
			SOAPPart soap = msg.getSOAPPart();
			SOAPEnvelope envelope = soap.getEnvelope();
			SOAPBody body = envelope.getBody();
	
        		SOAPName command = envelope.createName("Enable");
			SOAPBodyElement bodyElement = body.addBodyElement(command);
		
			SOAPName originator = envelope.createName("originator");
			SOAPName targetAddr = envelope.createName("targetAddr");
		
			//
			// Add originator and target identifiers to the message.
			// These are XDAQ internal attributes that are needed
			// to establish a path between two XDAQ application peers.
			//
			xtuple::Integer localTid  ( tid_ );
			bodyElement.addAttribute (originator, localTid.toString() );
			xtuple::Integer serverTid ( tid );		
			bodyElement.addAttribute (targetAddr, serverTid.toString() );
			
			xdaq::frameSend (msg);
		}
		return 0;
	}
};



class Server: public xdaqApplication
{	
	public:
	
	EnableTask* t_;

	Server() 
	{
		soapBindMethod(this, &Server::token, "token");
	
		exportParam("counter",counter_);
		exportParam("startSize",startSize_);
		exportParam("samples",samples_);
		exportParam("dataBw",dataBw_);
		exportParam("dataRate",dataRate_);
 		exportParam("dataLatency",dataLatency_);
		exportParam("latencyHistogram",latencyPlot_);
		exportParam("rateHistogram",ratePlot_);
		exportParam("bandwidthHistogram",bandwidthPlot_);
		
		// fill the plot 
		latencyPlot_.addTitle ("RoundTrip Latency");
		ratePlot_.addTitle ("RoundTrip Rate");
		bandwidthPlot_.addTitle ("RoundTrip Bandwidth");
		Histogram1d& hl = latencyPlot_.addHistogram1d ("Latency");
		Histogram1d& hr = ratePlot_.addHistogram1d ("Rate");
		Histogram1d& hb = bandwidthPlot_.addHistogram1d ("Bandwidth");
		hl.showErrorBars ( false);
		hr.showErrorBars ( false);
		hb.showErrorBars ( false);
		hl.setBarColor ("Blue");
		hr.setBarColor ("Red");
		hb.setBarColor ("Green");		
	}
	
	
	void plugin() 
	{
		LOG4CPLUS_INFO(logger_,toString("plugin server..."));
		pmeter_ = new PerformanceMeter(logger_);
	}
	

 	
	void checkMeasurements(int size) 
	{	    
	    if ( pmeter_->benchmark(size) ) 
	    {
		Histogram1d& hl = latencyPlot_.getHistogram1d ("Latency");
		hl.addValue (pmeter_->latency() );
		Histogram1d& hr = ratePlot_.getHistogram1d ("Rate");
		hr.addValue (pmeter_->rate() );
		Histogram1d& hb = bandwidthPlot_.getHistogram1d ("Bandwidth");
		hb.addValue (pmeter_->bandwidth());
		
		hl.setMaxX (size);
		hr.setMaxX (size);
		hb.setMaxX (size);
		
		
		LOG4CPLUS_INFO(logger_,toString("latency:  %f, rate: %f, bandwidth: %f, size: %d\n", pmeter_->latency(),pmeter_->rate(),pmeter_->bandwidth(),size));
		       
		return;
	    }
	}
	
	void ParameterSetDefault (list<string> & paramNames) 
	{
		LOG4CPLUS_INFO(logger_,toString("set number of samples to: %d", samples_));
		pmeter_->init(samples_);
	}
	
	//
	// Callback function
	//
	SOAPMessage token (SOAPMessage & msg)
	{
		counter_++;
		//cout << "Call received. Counter is: " << counter_ << endl;
		
		//
		// Retrieve attachments from received SOAP message and add to reply message
		//
		list<AttachmentPart*> attachments = msg.getAttachments();
		
		if (attachments.size() != 1)
		{
			cout << "Error: Did not receive 1 attachment, but: " << attachments.size() << endl;
			
			//
			// dump SOAP message
			//
			string s;
        		msg.writeTo(s);
			cout << "--- SOAP dump ---" << endl;
        		cout << s << endl;
			cout << "--- SOAP dump ---" << endl;
			
			SOAPMessage r;
			SOAPEnvelope e = r.getSOAPPart().getEnvelope();
			SOAPBody b = e.getBody();
			SOAPFault f = b.addFault();
			f.setFaultCode ("Server");
			f.setFaultString ("Too many attachments.");
			return r;
			
		} else 
		{
			AttachmentPart * attachment = *(attachments.begin());
			int size = attachment->getSize();
			this->checkMeasurements(size);
			
			//
			// If all samples from one size have arrived, let
			// clients know that they can switch to the next size.
			//
			if (counter_ == (samples_ - 1))
			{
				counter_ = 0;
				
				// Send Enable to all Clients
				//
				t_->activate();				
			}
			
		}
		
		SOAPMessage reply;
		SOAPEnvelope envelope = reply.getSOAPPart().getEnvelope();
		SOAPBody b = envelope.getBody();
		SOAPName responseName = envelope.createName("tokenResponse", "", "");
		//SOAPBodyElement e = b.addBodyElement ( responseName );
		b.addBodyElement ( responseName );

		return reply;
	}
	
	

        //
	// run control send new paramater values. system is started by
	// peer instance 0 upon reception of the paramsSet command from
        // the run control.
        //
	void ParameterSet (list<string> & paramNames)
	{
	}
	
	//
	// run control requests current paramater values
	//
	void ParameterGet (list<string> & paramNames)   
	{ 
		// update measurements monitors
		dataBw_        =  pmeter_->bandwidth();
    		dataLatency_   =  pmeter_->latency();
		dataRate_      =  pmeter_->rate();
		LOG4CPLUS_INFO(logger_,toString("bandw: %f",pmeter_->bandwidth()));
	}
	
	void Configure() throw (xdaqException)
	{
		counter_ = 0;
		
		t_ = new EnableTask ( tid() );
		
		LOG4CPLUS_INFO (logger_,toString("Configure :Parameter counter: %d", counter_));
		LOG4CPLUS_INFO (logger_,toString("Configure :Parameter samples: %d", samples_));
		LOG4CPLUS_INFO (logger_,toString("Configure :Parameter start size: %d",    startSize_));
	}
	
	void Enable() throw (xdaqException)
	{
			dataBw_  = 0.0;
			dataRate_ = 0.0;
			dataLatency_ = 0.0;
			pmeter_->init(samples_);
			Histogram1d& hl = latencyPlot_.getHistogram1d ("Latency");
			Histogram1d& hr = ratePlot_.getHistogram1d ("Rate");
			Histogram1d& hb = bandwidthPlot_.getHistogram1d ("Bandwidth");
			
			hl.setMinX (startSize_);
			hr.setMinX (startSize_);
			hb.setMinX (startSize_);
			
			//
			// Tell clients to send
			//
			t_->activate();
	}
	
	
	
	
	protected:
	
	// some paramters
       unsigned long counter_;
       unsigned long samples_;
       unsigned long startSize_;

	//
	//! perfromamce Meters
	//
	PerformanceMeter * pmeter_;
	//
	//!current measurements
	//
	double dataBw_;
	double dataRate_;
 	double dataLatency_;
	
       Plot latencyPlot_;
       Plot ratePlot_;
       Plot bandwidthPlot_;	
};

#endif
