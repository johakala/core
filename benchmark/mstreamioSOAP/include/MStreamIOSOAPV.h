// $Id: MStreamIOSOAPV.h,v 1.3 2004/09/02 09:59:28 xdaq Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

//
// Version definition for StreamIO
//
#ifndef MStreamIOSOAPV_H
#define MStreamIOSOAPV_H

#include "PackageInfo.h"

namespace MStreamIOSOAP 
{
    const string package  =  "MStreamIOSOAP";
    const string versions =  "2.0";
    const string description = "Unidirectional streaming throughput measurement example for SOAP, single client multiple servers";
    toolbox::PackageInfo getPackageInfo();
    void checkPackageDependencies() throw (toolbox::PackageInfo::VersionException);
    set<string, less<string> > getPackageDependencies();
}

#endif
