// $Id: MStreamIOSOAPSO.h,v 1.4 2004/09/02 09:59:28 xdaq Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef MStreamIOSOAPSO_H
#define MStreamIOSOAPSO_H

#include "xdaq.h"
#include <string>
#include "Client.h"
#include "Server.h"

class MStreamIOSOAPSO: public xdaqSO 
{
        public:

        void init() 
	{	
	
        }

        void shutdown() 
	{      
        }

	xdaqPluggable * createPluggable (string name) 
	{
		if ( name == "Client" ) return new Client();
		if ( name == "Server" ) return new Server();		
		return (xdaqPluggable*)0;
	}

};

#endif
