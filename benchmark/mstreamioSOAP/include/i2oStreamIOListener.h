// $Id: i2oStreamIOListener.h,v 1.2 2004/09/02 09:59:28 xdaq Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _i2oStreamIOListener_h_
#define _i2oStreamIOListener_h_

#include "i2oListener.h"
#include "i2oStreamIOMsg.h"

class i2oStreamIOListener : public i2oListener 
{

 public:
  i2oStreamIOListener(){
    i2oBindMethod (this, 
                   &i2oStreamIOListener::token, 
                   I2O_TOKEN_CODE,
                   XDAQ_ORGANIZATION_ID, 0);
  }
 
  virtual void token( BufRef * ref ) = 0;
};

#endif

