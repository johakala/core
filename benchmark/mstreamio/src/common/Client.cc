// $Id: Client.cc,v 1.13 2008/07/18 15:26:40 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "benchmark/mstreamio/Client.h"

#include "benchmark/mstreamio/i2oStreamIOMsg.h"
#include "toolbox/BSem.h"
#include "toolbox/Task.h"

#include "toolbox/rlist.h"
#include "toolbox/fsm/FiniteStateMachine.h"
#include "toolbox/mem/Reference.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "toolbox/mem/Pool.h"
#include "toolbox/mem/Reference.h"
#include "toolbox/mem/Pool.h"
#include "toolbox/mem/CommittedHeapAllocator.h"

#include "i2o/Method.h"
#include "i2o/utils/AddressMap.h"

#include "xdaq/Application.h"
#include "xdaq/ApplicationContext.h"
#include "xdaq/ApplicationGroup.h"
#include "xoap/MessageFactory.h"
#include "xdaq/NamespaceURI.h"
#include "xcept/Exception.h"
#include "xcept/tools.h"

#include "xoap/Method.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPName.h"
#include "xoap/SOAPBody.h"



Client::Client(xdaq::ApplicationStub* c)
 	throw(xdaq::exception::Exception)
	 : xdaq::Application(c), toolbox::Task("Client")
{
	getApplicationInfoSpace()->fireItemAvailable("counter",&counter_);
	getApplicationInfoSpace()->fireItemAvailable("currentSize",&currentSize_);
	getApplicationInfoSpace()->fireItemAvailable("maxFrameSize",&maxFrameSize_);
	getApplicationInfoSpace()->fireItemAvailable("committedPoolSize",&committedPoolSize_);

	// Add infospace listeners for exporting data values
	getApplicationInfoSpace()->addItemChangedListener ("committedPoolSize", this);

	// Define FSM
	fsm_.addState ('S', "Stopped");
	fsm_.addState ('R', "Running");
	fsm_.addStateTransition ('S','R', "start");
	fsm_.addStateTransition ('R','S', "stop");
	fsm_.setInitialState('S');
	fsm_.reset();

	// Bind SOAP callbacks for control messages
	xoap::bind (this, &Client::start, "start", XDAQ_NS_URI);
	xoap::bind (this, &Client::stop,  "stop",  XDAQ_NS_URI);	

	// initialize 
	counter_ = 0;
        starting_ = 0;

	try 
	{	
		destination_ = getApplicationContext()->getDefaultZone()->getApplicationDescriptors("Server");	
	}
	catch (xdaq::exception::Exception& e)
        {
                LOG4CPLUS_FATAL (getApplicationLogger(), xcept::stdformat_exception_history(e));
                XCEPT_RAISE (xcept::Exception, "No Server application instance found. Client cannot be configured.");
        }	
}
	
	
	
int Client::svc()
{
	LOG4CPLUS_DEBUG (getApplicationLogger(), "starting send loop...");

	while (fsm_.getCurrentState() == 'R' )
	{

		if (!pool_->isHighThresholdExceeded())
		{
			// Stop if there is an error in sending
			if (this->sendMessage(0) == 1) 
			{ 
				LOG4CPLUS_FATAL (getApplicationLogger(), toolbox::toString("Error in frameSend. Stopping client."));
				return 1;
			}
			counter_ = counter_ +1;				
		} else 
		{
			LOG4CPLUS_DEBUG (getApplicationLogger(), "high threshold is exceeded");
			while (pool_->isLowThresholdExceeded())
			{
				LOG4CPLUS_DEBUG (getApplicationLogger(), "yield till low threshold reached");
				this->yield(1);
			}
		}
	}

	// send last message to all destination
	if (this->sendMessage(1) == 1) 
	{
		LOG4CPLUS_FATAL (getApplicationLogger(), toolbox::toString("Error in frameSend. Stopping client."));
		return 1;
	}

	counter_= counter_ +1;
	return 0;
}

//
// used to create the memory pool upon parametrization
//	
void Client::actionPerformed (xdata::Event& e) 
{ 
	// update measurements monitors		
	if (e.type() == "ItemChangedEvent")
	{
		std::string item = dynamic_cast<xdata::ItemChangedEvent&>(e).itemName();
		if ( item == "committedPoolSize")
		{
			try 
			{
				LOG4CPLUS_INFO (getApplicationLogger(), "Committed pool size is " << (unsigned long) committedPoolSize_);
				toolbox::mem::CommittedHeapAllocator* a = new toolbox::mem::CommittedHeapAllocator(committedPoolSize_);
				toolbox::net::URN urn ("toolbox-mem-pool", "StreamIOSendPool");
				pool_ = toolbox::mem::getMemoryPoolFactory()->createPool(urn, a);
				LOG4CPLUS_INFO (getApplicationLogger(), "Set high watermark to 90% and low watermark to 70%");
				pool_->setHighThreshold ( (unsigned long) (committedPoolSize_ * 0.9));
				pool_->setLowThreshold ((unsigned long) (committedPoolSize_ * 0.7));
			}
			catch(toolbox::mem::exception::Exception & e)
			{
				LOG4CPLUS_FATAL (getApplicationLogger(), toolbox::toString("Could not set up memory pool, %s (exiting thread)", e.what()));
				return;
			}
		} 
	}	
}
	
	
// These methods are called by the server
xoap::MessageReference Client::start(xoap::MessageReference  message) throw (xoap::exception::Exception)
{
	LOG4CPLUS_INFO (getApplicationLogger(), toolbox::toString("start client on message size %d", (int)currentSize_));

	starting_ = starting_ + 1;

	if (starting_ == (xdata::UnsignedLongT) destination_.size() ) 
	{
		LOG4CPLUS_INFO (getApplicationLogger(), toolbox::toString("received start from all servers: %d ", (int)starting_));

		toolbox::Event::Reference e(new toolbox::Event("start", this));
		fsm_.fireEvent(e);
		completed_ = 0;
		this->activate();
        }
	return  xoap::createMessage();
}
	
xoap::MessageReference Client::stop(xoap::MessageReference  message) throw (xoap::exception::Exception)
{
	LOG4CPLUS_INFO (getApplicationLogger(), toolbox::toString("stop client"));

	// Received Disable from a server. Count Disable
	// messages from all servers and change to state
	// Ready, if all Disable messages have arrived.
	//
	completed_ = completed_ + 1; 		
	if ( completed_ == (xdata::UnsignedLongT) destination_.size() )
	{
		toolbox::Event::Reference e(new toolbox::Event("stop", this));
		fsm_.fireEvent(e);
		starting_ = 0;
	}

	return  xoap::createMessage();	
}
	
// Send an I2O token message to all servers
// If last is 1, the flag 'last' in the
// message is set to one. Otherwise it
// is set to 0
//
int Client::sendMessage(unsigned int last) 
{
	std::set<xdaq::ApplicationDescriptor*>::iterator iter;

	for(iter=destination_.begin(); iter!=destination_.end(); iter++)
	{
		toolbox::mem::Reference * ref = 0;
		try 
		{
		    	ref = toolbox::mem::getMemoryPoolFactory()->getFrame(pool_, maxFrameSize_);

		    	PI2O_TOKEN_MESSAGE_FRAME frame = (PI2O_TOKEN_MESSAGE_FRAME) ref->getDataLocation();   


			frame->PvtMessageFrame.StdMessageFrame.MsgFlags         = 0;
			frame->PvtMessageFrame.StdMessageFrame.VersionOffset    = 0;
			frame->PvtMessageFrame.StdMessageFrame.TargetAddress    = i2o::utils::getAddressMap()->getTid(*iter);
			frame->PvtMessageFrame.StdMessageFrame.InitiatorAddress = i2o::utils::getAddressMap()->getTid(this->getApplicationDescriptor());
			frame->PvtMessageFrame.StdMessageFrame.MessageSize      = (currentSize_+sizeof(I2O_TOKEN_MESSAGE_FRAME)) >> 2;

			frame->PvtMessageFrame.StdMessageFrame.Function = I2O_PRIVATE_MESSAGE;
			frame->PvtMessageFrame.XFunctionCode            = I2O_TOKEN_CODE;
			frame->PvtMessageFrame.OrganizationID           = XDAQ_ORGANIZATION_ID;

			frame->last = last;
		   	ref->setDataSize(frame->PvtMessageFrame.StdMessageFrame.MessageSize << 2);
			LOG4CPLUS_DEBUG (getApplicationLogger(),"sending to tid: " << frame->PvtMessageFrame.StdMessageFrame.TargetAddress );
			getApplicationContext()->postFrame(ref, this->getApplicationDescriptor(), *iter);
		} 
		catch (toolbox::mem::exception::Exception & me)
		{
		      	LOG4CPLUS_FATAL (getApplicationLogger(), xcept::stdformat_exception_history(me));
		     	 return 1; // error
		}
		catch (xdaq::exception::Exception & e)
		{
			// Retry 3 times
			bool retryOK = false;
			for (unsigned int k = 0; k < 3; k++)
			{
				try
				{
					getApplicationContext()->postFrame(ref,  this->getApplicationDescriptor(), *iter);
					retryOK = true;
					break; // if send was successfull, continue to send other messages
				}
				catch (xdaq::exception::Exception & re)
		    		{
					LOG4CPLUS_WARN (getApplicationLogger(), xcept::stdformat_exception_history(re));
				}
			}

			if (!retryOK)
			{
				LOG4CPLUS_FATAL (getApplicationLogger(), "Frame send failed after 3 times.");
		      		LOG4CPLUS_FATAL (getApplicationLogger(), xcept::stdformat_exception_history(e));
		      		ref->release();
				return 1; // error				
			}
		}
	}
	return 0; // o.k.
}
