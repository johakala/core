// $Id: Server.cc,v 1.13 2008/07/18 15:26:40 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "benchmark/mstreamio/Server.h"
#include "benchmark/mstreamio/i2oStreamIOMsg.h"

#include "xdaq/Application.h"
#include "xdaq/ApplicationContext.h"
#include "xdaq/ApplicationGroup.h"
#include "toolbox/PerformanceMeter.h"
#include "xdaq/NamespaceURI.h"

#include "toolbox/BSem.h"

#include "toolbox/rlist.h"
#include "toolbox/string.h"
#include "toolbox/mem/Reference.h"
#include "toolbox/mem/Pool.h"

#include "xdata/UnsignedLong.h"
#include "xdata/Double.h"
#include "xdata/Boolean.h"
#include "xdata/ActionListener.h"

#include "xoap/MessageFactory.h"
#include "xoap/MessageReference.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPName.h"
#include "xoap/SOAPBody.h"
#include "xoap/Method.h"

#include "xcept/Exception.h"
#include "xcept/tools.h"

#include "i2o/Method.h"
#include "i2o/utils/AddressMap.h"



Server::Server(xdaq::ApplicationStub* c)
	throw(xdaq::exception::Exception)
	: xdaq::Application(c), mutex_(toolbox::BSem::FULL)
{
	getApplicationInfoSpace()->fireItemAvailable("counter",&counter_);
	getApplicationInfoSpace()->fireItemAvailable("startSize",&startSize_);		
	getApplicationInfoSpace()->fireItemAvailable("endSize",&endSize_);
	getApplicationInfoSpace()->fireItemAvailable("currentSize",&currentSize_);
	getApplicationInfoSpace()->fireItemAvailable("step",&step_);
	getApplicationInfoSpace()->fireItemAvailable("samples",&samples_);
	getApplicationInfoSpace()->fireItemAvailable("dataBw",&dataBw_);
	getApplicationInfoSpace()->fireItemAvailable("dataRate",&dataRate_);
 	getApplicationInfoSpace()->fireItemAvailable("dataLatency",&dataLatency_);

	xoap::bind (this, &Server::start, "start", XDAQ_NS_URI);

	pmeter_ = new toolbox::PerformanceMeter();

	i2o::bind(this, &Server::token, I2O_TOKEN_CODE, XDAQ_ORGANIZATION_ID);

	// Add infospace listeners for exporting data values
	getApplicationInfoSpace()->addItemRetrieveListener ("dataBw", this);
	getApplicationInfoSpace()->addItemRetrieveListener ("dataRate", this);
	getApplicationInfoSpace()->addItemRetrieveListener ("dataLatency", this);

	counter_ = 0;
	stopped_ = false;
	currentSize_ = startSize_;

	// Put all destination instance TIDs into a vector by querying the class name "Client"
	try
	{
		clients_ = getApplicationContext()->getDefaultZone()->getApplicationDescriptors("Client");
	}
	catch (xdaq::exception::Exception& e)
	{
		LOG4CPLUS_FATAL (getApplicationLogger(), xcept::stdformat_exception_history(e));
		XCEPT_RAISE (xcept::Exception, "No Client application instance found. Server cannot be configured.");
	}
}
		
 	
void Server::checkMeasurements(int size) 
{	   
    if ( pmeter_->addSample(size) ) 
    {
	LOG4CPLUS_INFO(getApplicationLogger(),toolbox::toString("measured latency: %f for size %d",pmeter_->latency(), size));
	LOG4CPLUS_INFO(getApplicationLogger(),toolbox::toString("latency:  %f, rate: %f,bandwidth %f, size: %d\n", pmeter_->latency(),pmeter_->rate(),pmeter_->bandwidth(),size));

	// stop Clients by sending a SOAP message named <stop>
	stopped_ = true;
	this->stopClients();		 
	return;
    }
}
	
void Server::stopClients()
{
	std::set<xdaq::ApplicationDescriptor*>::iterator iter;
	for(iter=clients_.begin(); iter!=clients_.end(); iter++)
	{			
		xoap::MessageReference msg = xoap::createMessage();
		xoap::SOAPPart soap = msg->getSOAPPart();
		xoap::SOAPEnvelope envelope = soap.getEnvelope();
		xoap::SOAPBody body = envelope.getBody();
        	xoap::SOAPName command = envelope.createName("stop","xdaq", "urn:xdaq-soap:3.0");
		body.addBodyElement(command);

		try
		{	
			xoap::MessageReference reply = getApplicationContext()->postSOAP(msg, *iter);
		} catch (xdaq::exception::Exception& e)
		{
			LOG4CPLUS_FATAL (getApplicationLogger(), xcept::stdformat_exception_history(e));
		}
	}	
}
	
// Tell Clients to update the size of their I2O
// token message by sending a ParameterSet:
//  <properties xmlns:c="urn:xdaq-application:PeerTransportTCP" xsi:type="soapenc:Struct">
//                        <c:currentSize xsi:type="xsd:unsignedLong">xxx</poolName>
//   </properties>
void Server::nextSize(xdata::UnsignedLongT size)
{
        LOG4CPLUS_INFO (getApplicationLogger(), toolbox::toString("set frame size to: %d", (xdata::UnsignedLongT) size));
	std::set<xdaq::ApplicationDescriptor*>::iterator iter;
	for(iter=clients_.begin(); iter!=clients_.end(); iter++)
	{			
		xoap::MessageReference request = xoap::createMessage();

        	try
		{
			xoap::SOAPEnvelope envelope = request->getSOAPPart().getEnvelope(); 
			xoap::SOAPName commandName = envelope.createName (  "ParameterSet", "xdaq", "urn:xdaq-soap:3.0"  );
			xoap::SOAPBody body = envelope.getBody(); 
			xoap::SOAPElement command = body.addBodyElement ( commandName ); 
			command.addNamespaceDeclaration("xsi","http://www.w3.org/2001/XMLSchema-instance");
			command.addNamespaceDeclaration("xsd","http://www.w3.org/2001/XMLSchema");
			command.addNamespaceDeclaration("soapenc","http://schemas.xmlsoap.org/soap/encoding/");

			xoap::SOAPName properties = envelope.createName (  "properties", "c" , "urn:xdaq-application:Client" );
			xoap::SOAPElement propertiesElement = command.addChildElement( properties );
			xoap::SOAPName propertiesType = envelope.createName("type", "xsi", "http://www.w3.org/2001/XMLSchema-instance");
			propertiesElement.addAttribute( propertiesType, "soapenc:Struct");

			xoap::SOAPName currentSizeName = envelope.createName ( "currentSize" ,  "c", "urn:xdaq-application:Client");
			xoap::SOAPElement currentSizeElement = propertiesElement.addChildElement( currentSizeName );      
			xoap::SOAPName currentSizeType = envelope.createName( "type","xsi", "http://www.w3.org/2001/XMLSchema-instance" );
			currentSizeElement.addAttribute( currentSizeType, "xsd:unsignedLong");

			currentSizeElement.addTextNode(toolbox::toString("%d",size));

			LOG4CPLUS_INFO (getApplicationLogger(), toolbox::toString("set size to client: %s ", (*iter)->getURN().c_str()));
			xoap::MessageReference reply = getApplicationContext()->postSOAP(request, *iter);

			// Check, if there is a fault in the reply             
                	xoap::SOAPBody rb = reply->getSOAPPart().getEnvelope().getBody();
                	if (rb.hasFault() )
                	{
				LOG4CPLUS_ERROR (getApplicationLogger(),  rb.getFault().getFaultString() );
               		} 
		} 
		catch (xdaq::exception::Exception& e)
		{
			LOG4CPLUS_FATAL (getApplicationLogger(), xcept::stdformat_exception_history(e));
		}
		catch (xoap::exception::Exception& xe)
		{
			LOG4CPLUS_FATAL (getApplicationLogger(), xcept::stdformat_exception_history(xe));
		}
	}
}
	
	
// Start Clients by sending the SOAP command messsge <start>
//
void Server::activateClients()
{
	LOG4CPLUS_INFO (getApplicationLogger(), toolbox::toString("activates clients"));
	completed_ = 0;
	std::set<xdaq::ApplicationDescriptor*>::iterator iter;
	for(iter=clients_.begin(); iter!=clients_.end(); iter++)
	{			
		xoap::MessageReference msg = xoap::createMessage();
		xoap::SOAPPart soap = msg->getSOAPPart();
		xoap::SOAPEnvelope envelope = soap.getEnvelope();
		xoap::SOAPBody body = envelope.getBody();
        	xoap::SOAPName command = envelope.createName("start", "xdaq", "urn:xdaq-soap:3.0");
		body.addBodyElement(command);

		try
		{
			LOG4CPLUS_INFO (getApplicationLogger(), toolbox::toString("activate client: %s ",(*iter)->getURN().c_str()));
			xoap::MessageReference reply = getApplicationContext()->postSOAP(msg, *iter);
		} catch (xdaq::exception::Exception& e)
		{
			LOG4CPLUS_FATAL (getApplicationLogger(), xcept::stdformat_exception_history(e));
		}
	}	
}

// Callback function invoked for the I2O token message
// that is received from the Clients
//
void Server::token(toolbox::mem::Reference * ref)  throw (i2o::exception::Exception)
{	
	mutex_.take();

	counter_ = counter_ + 1;

	LOG4CPLUS_DEBUG (getApplicationLogger(), toolbox::toString("received message number %d", (xdata::UnsignedLongT) counter_));

	PI2O_TOKEN_MESSAGE_FRAME frame = (PI2O_TOKEN_MESSAGE_FRAME) ref->getDataLocation();   
	size_t size = frame->PvtMessageFrame.StdMessageFrame.MessageSize << 2;
	size -= sizeof(I2O_TOKEN_MESSAGE_FRAME);

	if ( stopped_ ) 
	{		
		if ( frame->last ) 
		{
			// The last message has arrived. Increment the completed counter
			completed_ = completed_ + 1;
		}		 

		// If all last messages have arrived from all clients,
		// increment the size of the token message and restart the clients.
		//
		if (completed_ == (xdata::UnsignedLongT) clients_.size() )
		{
			if ( currentSize_ >= endSize_ ) 
			{
				LOG4CPLUS_INFO (getApplicationLogger(), toolbox::toString("test finished."));
				mutex_.give();
				return;
			} 
			else
			{
				currentSize_ =  currentSize_ + step_;
				this->nextSize(currentSize_);
				this->activateClients();
				stopped_ = false;	
			}				
		}
	}
	else 
	{
		this->checkMeasurements(size);
	}

	ref->release();

	mutex_.give();
}
	

	
void Server::actionPerformed (xdata::Event& e) 
{ 
	// update measurements monitors		
	if (e.type() == "ItemRetrieveEvent")
	{
		std::string item = dynamic_cast<xdata::ItemRetrieveEvent&>(e).itemName();
		if ( item == "dataBw")
		{
			dataBw_ =  pmeter_->bandwidth();
			LOG4CPLUS_INFO(getApplicationLogger(), toolbox::toString("bandwidth: %f",pmeter_->bandwidth()));
		} else if ( item == "dataLatency")
		{
			dataLatency_ =  pmeter_->latency();
			LOG4CPLUS_INFO(getApplicationLogger(), toolbox::toString("latency: %f",pmeter_->latency()));
		} else if ( item == "dataRate")
		{
			dataRate_ =  pmeter_->rate();
			LOG4CPLUS_INFO(getApplicationLogger(), toolbox::toString("rate: %f",pmeter_->rate()));
		}
	}	
}
	
	
xoap::MessageReference Server::start(xoap::MessageReference  message) throw (xoap::exception::Exception)
{
	LOG4CPLUS_INFO (getApplicationLogger(), toolbox::toString("start test..."));
	dataBw_  = 0.0;
	dataRate_ = 0.0;
	dataLatency_ = 0.0;
	pmeter_->init(samples_);

	// Give the initial size to the clients and activate them.
	//
	this->nextSize(startSize_);
	this->activateClients();
	stopped_ = false;
	return  xoap::createMessage();						
}
