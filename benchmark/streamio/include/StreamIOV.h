// $Id: StreamIOV.h,v 1.4 2004/09/02 09:59:32 xdaq Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

//
// Version definition for StreamIO
//
#ifndef StreamIOV_H
#define StreamIOV_H

#include "PackageInfo.h"

namespace StreamIO {
    const string package  =  "StreamIO";
    const string versions =  "2.0";
    const string description = "Unidirectional streaming throughput measurement example";
    toolbox::PackageInfo getPackageInfo();
    void checkPackageDependencies() throw (toolbox::PackageInfo::VersionException);
    set<string, less<string> > getPackageDependencies();
}

#endif
