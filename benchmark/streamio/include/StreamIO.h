// $Id: StreamIO.h,v 1.6 2004/09/02 09:59:32 xdaq Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _StreamIO_h_
#define _StreamIO_h_

#include "xdaq.h"

#include "i2oStreamIOListener.h"

#include <list>
#include <algorithm>
#include "PerformanceMeter.h"

#include "Plot.h"
#include "BSem.h"
#include "rlist.h"
#include "Task.h"

using namespace toolbox;

class StreamIO: public i2oStreamIOListener, public xdaqApplication, public Task
{	
	public:

	StreamIO() : Task("StreamIO")
	{
		exportParam("counter",counter_);
		exportParam("samples",samples_);
		exportParam("startSize",startSize_);
		exportParam("endSize",endSize_);
		exportParam("currentSize",currentSize_);
		exportParam("dataBw",dataBw_);
		exportParam("dataRate",dataRate_);
 		exportParam("dataLatency",dataLatency_);
		exportParam("latencyHistogram",latencyPlot_);
		exportParam("rateHistogram",ratePlot_);
		exportParam("bandwidthHistogram",bandwidthPlot_);
		exportParam("numFrames",numFrames_);
		exportParam("step",step_);
		exportParam("destinationInstance",destinationInstance_);
		
		// fill the plot 
		latencyPlot_.addTitle ("RoundTrip Latency");
		ratePlot_.addTitle ("RoundTrip Rate");
		bandwidthPlot_.addTitle ("RoundTrip Bandwidth");
		Histogram1d& hl = latencyPlot_.addHistogram1d ("Latency");
		Histogram1d& hr = ratePlot_.addHistogram1d ("Rate");
		Histogram1d& hb = bandwidthPlot_.addHistogram1d ("Bandwidth");
		hl.showErrorBars ( false);
		hr.showErrorBars ( false);
		hb.showErrorBars ( false);
		hl.setBarColor ("Blue");
		hr.setBarColor ("Red");
		hb.setBarColor ("Green");		
		
		LOG4CPLUS_INFO(logger_,toString("created StreamIO..."));
	}
	
	
	void plugin() 
	{
		LOG4CPLUS_INFO(logger_, toString("plugin StreamIO..."));
		pmeter_ = new PerformanceMeter(logger_);
	}
	
	int svc()
	{
		SmartBufPool * memPool = executive->getPoolPtr(); 
		memPool->threshold(endSize_, numFrames_);
		
		currentSize_ = startSize_;
		unsigned long samples = 0;
		
		for (;;)
		{
			int availableBlocks = memPool->available(endSize_);
			// not used bool overThreshold = memPool->overThreshold (endSize_);
			
			if (availableBlocks > 0) 
			{
				if (samples < samples_)
				{
					this->sendMessage();
					samples++;
				} else {
					samples = 0;
					currentSize_ += step_;
					
					if (currentSize_ > (endSize_-sizeof(I2O_TOKEN_MESSAGE_FRAME))) return 0;
				}
			} else 
			{
				//cout << "Cannot send anymore!" << endl;
				this->yield(1);
				//sleep(1);
			}
		}
	}
	
 	
	void checkMeasurements(int size) 
	{	    
	    if ( pmeter_->benchmark(size) ) 
	    {
		Histogram1d& hl = latencyPlot_.getHistogram1d ("Latency");
		hl.addValue (pmeter_->latency() );
		Histogram1d& hr = ratePlot_.getHistogram1d ("Rate");
		hr.addValue (pmeter_->rate() );
		Histogram1d& hb = bandwidthPlot_.getHistogram1d ("Bandwidth");
		hb.addValue (pmeter_->bandwidth());
		
		hl.setMaxX (size);
		hr.setMaxX (size);
		hb.setMaxX (size);
		
		LOG4CPLUS_INFO(logger_,toString("measured latency: %f for size %d",pmeter_->latency(), size));
		return;
	    }
	}
	
	void ParameterSetDefault (list<string> & paramNames) 
	{
		LOG4CPLUS_INFO(logger_, toString("set number of samples to: %d", samples_));
		pmeter_->init(samples_);
	}
	
	void token(  BufRef * ref) 
	{
		counter_++;

		PI2O_TOKEN_MESSAGE_FRAME frame = (PI2O_TOKEN_MESSAGE_FRAME) ref->buf_;   
		unsigned long size = frame->PvtMessageFrame.StdMessageFrame.MessageSize << 2;
		size -= sizeof (I2O_TOKEN_MESSAGE_FRAME);
		
		this->checkMeasurements(size);
		
		xdaq::frameFree(ref);
	}
	

        //
	// run control send new paramater values. system is started by
	// peer instance 0 upon reception of the paramsSet command from
        // the run control.
        //
	void ParameterSet (list<string> & paramNames)
	{
          
         
	}
	
	//
	// run control requests current paramater values
	//
	void ParameterGet (list<string> & paramNames)   
	{ 
		// update measurements monitors
		dataBw_        =  pmeter_->bandwidth();
    		dataLatency_   =  pmeter_->latency();
		dataRate_      =  pmeter_->rate();
		LOG4CPLUS_DEBUG(logger_,toString("bandw: %f",pmeter_->bandwidth()));
	}
	
	void Configure() throw (xdaqException)
	{
		counter_ = 0;
		LOG4CPLUS_INFO (logger_,toString("Configure :Parameter counter: %d", counter_));
		LOG4CPLUS_INFO (logger_,toString("Configure :Parameter samples: %d", samples_));
		LOG4CPLUS_INFO (logger_,toString("Configure :Parameter start size: %d",    startSize_));
		LOG4CPLUS_INFO (logger_,toString("Configure :Parameter end size: %d",    endSize_));
		LOG4CPLUS_INFO (logger_,toString("Configure :Parameter numframes: %d",    numFrames_));
		destination_ = xdaq::getTid("StreamIO", destinationInstance_);
		
	}
	
	void Enable() throw (xdaqException)
	{
		LOG4CPLUS_INFO (logger_, toString("Configuring, from state: %d", state()));
		if (instance_ != destinationInstance_) 
		{
			this->activate();
		} else {
			dataBw_  = 0.0;
			dataRate_ = 0.0;
			dataLatency_ = 0.0;
			pmeter_->init(samples_);
			Histogram1d& hl = latencyPlot_.getHistogram1d ("Latency");
			Histogram1d& hr = ratePlot_.getHistogram1d ("Rate");
			Histogram1d& hb = bandwidthPlot_.getHistogram1d ("Bandwidth");

			hl.setMinX (startSize_);
			hr.setMinX (startSize_);
			hb.setMinX (startSize_);

			
		}	
	}
	
	void sendMessage() 
	{
		// allocate frame 
		BufRef * ref = xdaq::frameAlloc(endSize_);
		
		// prepare frame and send
		PI2O_TOKEN_MESSAGE_FRAME frame = (PI2O_TOKEN_MESSAGE_FRAME) ref->buf_;   

		frame->PvtMessageFrame.StdMessageFrame.MsgFlags         = 0;
		frame->PvtMessageFrame.StdMessageFrame.VersionOffset    = 0;
		frame->PvtMessageFrame.StdMessageFrame.TargetAddress    = destination_;
		frame->PvtMessageFrame.StdMessageFrame.InitiatorAddress = tid_;
		frame->PvtMessageFrame.StdMessageFrame.MessageSize      = (currentSize_+sizeof(I2O_TOKEN_MESSAGE_FRAME)) >> 2;

		frame->PvtMessageFrame.StdMessageFrame.Function = I2O_PRIVATE_MESSAGE;
		frame->PvtMessageFrame.XFunctionCode            = I2O_TOKEN_CODE;
		frame->PvtMessageFrame.OrganizationID           = XDAQ_ORGANIZATION_ID;

		try {
			xdaq::frameSend(ref);
		} catch (xdaqException e)
		{
			LOG4CPLUS_FATAL (logger_,toString("Frame send failed: %s", e.what()));
			xdaq::frameFree(ref);
			return;
		}
	}
	
	protected:
	
	// some paramters
       unsigned long counter_;
       unsigned long samples_;
       unsigned long startSize_;
       unsigned long endSize_;
       unsigned long currentSize_;
       unsigned long destination_;
       unsigned long destinationInstance_;
       unsigned long numFrames_;
       unsigned long step_;
	//
	//! perfromamce Meters
	//
	PerformanceMeter * pmeter_;
	//
	//!current measurements
	//
	double dataBw_;
	double dataRate_;
 	double dataLatency_;
	
       Plot latencyPlot_;
       Plot ratePlot_;
       Plot bandwidthPlot_;	
};


class StreamIOSO: public xdaqSO 
{
        public:


        void init() {
				
        }

        void shutdown() {
             

        }

	xdaqPluggable * createPluggable(string name) 
	{
		if ( name == "StreamIO" ) return new StreamIO();
		return (xdaqPluggable*)0;
		
	}

};




#endif
