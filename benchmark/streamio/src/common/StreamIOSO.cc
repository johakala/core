// $Id: StreamIOSO.cc,v 1.2 2004/09/02 09:59:32 xdaq Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "StreamIO.h"

extern "C" void * init_StreamIO() 
{
	return ((void*)new StreamIOSO());
}

