// $Id: version.cc,v 1.2 2008/07/18 15:28:01 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "config/version.h"
#include "toolbox/version.h"
#include "xcept/version.h"

GETPACKAGEINFO(xcept)

void xcept::checkPackageDependencies() throw (config::PackageInfo::VersionException)
{
	CHECKDEPENDENCY(config);   
	CHECKDEPENDENCY(toolbox);
}

std::set<std::string, std::less<std::string> > xcept::getPackageDependencies()
{
    std::set<std::string, std::less<std::string> > dependencies;

    ADDDEPENDENCY(dependencies, config); 
    ADDDEPENDENCY(dependencies, toolbox);
  
    return dependencies;
}	
	
