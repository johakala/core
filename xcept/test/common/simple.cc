// $Id: simple.cc,v 1.3 2008/07/18 15:28:01 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xcept/Exception.h"
#include <iostream>

namespace my
{

class MyException: public xcept::Exception
{
	public:
	
	MyException(std::string name) throw (): xcept::Exception(name)
	{
	}
	
	MyException(std::string name, std::string message) throw(): xcept::Exception(name, message) 
	{
	
	}
	
	MyException( std::string name, std::string message, std::string module, int line, std::string function ): 
		xcept::Exception(name, message, module, line, function)
	{
	}

};

}


int main()
{
	try
	{
		 XCEPT_RAISE( my::MyException, "test" );
		 
	} catch (my::MyException& m)
	{
		std::cout << "Caught: " << m.name() << std::endl;
		std::cout << "to string: " << m.what() << std::endl;
	}
}
