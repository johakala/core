// $Id: Platform.cc,v 1.2 2008/07/18 15:28:40 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/
 
#include "xoap/filter/Platform.h"
#include "xalanc/XPath/XPathEvaluator.hpp"

bool xoap::filter::Platform::initialized_ = false;

void xoap::filter::Platform::initialize()
{
	XALAN_USING_XALAN(XPathEvaluator)

	if (!initialized_)
	{
		initialized_ = true;
		XPathEvaluator::initialize();		
	}
}

void xoap::filter::Platform::terminate()
{
	XALAN_USING_XALAN(XPathEvaluator)

	if (initialized_)
	{
		initialized_ = false;
		XPathEvaluator::terminate();		
	}
}
