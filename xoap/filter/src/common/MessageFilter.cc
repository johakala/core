// $Id: MessageFilter.cc,v 1.6 2008/07/18 15:28:40 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/
#include <sstream>

#include "xoap/filter/Platform.h"
#include "xoap/filter/MessageFilter.h"
#include "xalanc/Include/PlatformDefinitions.hpp"

#if defined(XALAN_CLASSIC_IOSTREAMS)
#include <iostream.h>
#else
#include <iostream>
#endif

#include "xalanc/Include/XalanMemoryManagement.hpp"
#include "xercesc/util/PlatformUtils.hpp"
#include "xercesc/framework/LocalFileInputSource.hpp"
#include <xercesc/parsers/XercesDOMParser.hpp>

#include "xalanc/PlatformSupport/XSLException.hpp"
#include "xalanc/DOMSupport/XalanDocumentPrefixResolver.hpp"
#include "xalanc/XPath/XObject.hpp"
#include "xalanc/XPath/XPathEvaluator.hpp"
#include "xalanc/XalanSourceTree/XalanSourceTreeDOMSupport.hpp"
#include "xalanc/XalanSourceTree/XalanSourceTreeInit.hpp"
#include "xalanc/XalanSourceTree/XalanSourceTreeParserLiaison.hpp"
#include "xalanc/XercesParserLiaison/XercesDocumentWrapper.hpp"

#include "xalanc/XercesParserLiaison/FormatterToXercesDOM.hpp"
#include "xalanc/XercesParserLiaison/XercesDOMFormatterWalker.hpp"
#include "xalanc/XercesParserLiaison/XercesDOMSupport.hpp"
#include "xalanc/XercesParserLiaison/XercesParserLiaison.hpp"
#include "xalanc/XalanTransformer/XercesDOMWrapperParsedSource.hpp"
#include "xalanc/XPath/XObject.hpp"
#include <xalanc/XPath/NodeRefList.hpp>
#include "toolbox/utils.h"
#include "xoap/SOAPElement.h"

XALAN_CPP_NAMESPACE_USE

xoap::filter::MessageFilter::MessageFilter(const std::string& filterExpression)
{
	xoap::filter::Platform::initialize();
	filter_ = filterExpression;
}

xoap::filter::MessageFilter::MessageFilter( xoap::SOAPElement& element) 
{
	xoap::filter::Platform::initialize();
	filter_ = toolbox::trim(element.getValue());
}

std::string xoap::filter::MessageFilter::getFilterExpression()
{
	return filter_;
}

bool xoap::filter::MessageFilter::match(xoap::MessageReference & message) throw (xoap::exception::Exception)
{

	XALAN_USING_XERCES(XMLPlatformUtils)

	XALAN_USING_XALAN(XPathEvaluator)
	XALAN_USING_XALAN(XalanDocumentPrefixResolver)
	XALAN_USING_XERCES(LocalFileInputSource)

	XALAN_USING_XALAN(XalanDocument)
	XALAN_USING_XALAN(XalanDocumentPrefixResolver)
	XALAN_USING_XALAN(XalanDOMString)
	XALAN_USING_XALAN(XalanNode)
	XALAN_USING_XALAN(XalanSourceTreeInit)
	XALAN_USING_XALAN(XalanSourceTreeDOMSupport)
	XALAN_USING_XALAN(XalanSourceTreeParserLiaison)
	XALAN_USING_XALAN(XObjectPtr)
	XALAN_USING_XALAN(NodeRefListBase)
	XALAN_USING_XALAN(XercesDOMWrapperParsedSource)
			
	//
	// Xerces und XPath initialisieren und einen DOM-Parser erstellen.
	//XercesDOMParser parser;
				
    	//parser.setDoValidation(true);
    	//parser.setDoNamespaces(true);

	//parser.parse(XMLString::transcode("/tmp/pippo.xml"));
	//DOMDocument* doc = parser.getDocument( );
	
	xoap::lock();
	
        DOMDocument* doc = message->getDocument(); 

	XercesParserLiaison liaison;
	XercesDOMSupport support(liaison);
	XercesDOMWrapperParsedSource src(doc, liaison, support);
	XalanDocument* xalanDoc = src.getDocument( );

	XPathEvaluator evaluator;
	XalanDocumentPrefixResolver resolver(xalanDoc);
	XalanDOMString xpath(filter_.c_str());
	

	try 
	{
		// Context node is root (first element, not the document node)
		//
                XalanNode* theContextNode =
			evaluator.selectSingleNode(
				support,
				xalanDoc,
				xpath.c_str( ),
				resolver
			);
		//liaison.destroyDocument(xalanDoc);
		//return true;
		
		if ( theContextNode != 0 )
		{
			liaison.destroyDocument(xalanDoc);
			xoap::unlock();
			return true;
		}
		else
		{
			liaison.destroyDocument(xalanDoc);
			xoap::unlock();
			return false;	
		}
	}
	catch (std::exception & e)
	{
			liaison.destroyDocument(xalanDoc);
			xoap::unlock();
			return false;
		XCEPT_RAISE(xoap::exception::Exception, e.what() );
	}
	catch (const XSLException & e)
	{
		// cannot do much with this exception, just return false
			liaison.destroyDocument(xalanDoc);
			xoap::unlock();
		return false;
	}
}

std::string xoap::filter::MessageFilter::evaluate(xoap::MessageReference & message) throw (xoap::exception::Exception)
{
	XALAN_USING_XERCES(XMLPlatformUtils)

	XALAN_USING_XALAN(XPathEvaluator)
	XALAN_USING_XALAN(XalanDocumentPrefixResolver)
	XALAN_USING_XERCES(LocalFileInputSource)

	XALAN_USING_XALAN(XalanDocument)
	XALAN_USING_XALAN(XalanDocumentPrefixResolver)
	XALAN_USING_XALAN(XalanDOMString)
	XALAN_USING_XALAN(XalanNode)
	XALAN_USING_XALAN(XalanSourceTreeInit)
	XALAN_USING_XALAN(XalanSourceTreeDOMSupport)
	XALAN_USING_XALAN(XalanSourceTreeParserLiaison)
	XALAN_USING_XALAN(XObjectPtr)
	XALAN_USING_XALAN(NodeRefListBase)
	XALAN_USING_XALAN(XercesDOMWrapperParsedSource)
			
	//
	// Xerces und XPath initialisieren und einen DOM-Parser erstellen.
	//XercesDOMParser parser;
				
    	//parser.setDoValidation(true);
    	//parser.setDoNamespaces(true);

	//parser.parse(XMLString::transcode("/tmp/pippo.xml"));
	//DOMDocument* doc = parser.getDocument( );
	xoap::lock();
	
	DOMDocument* doc = message->getDocument();

	XercesParserLiaison liaison;
	XercesDOMSupport support(liaison);
	XercesDOMWrapperParsedSource src(doc, liaison, support);
	XalanDocument* xalanDoc = src.getDocument( );

	XPathEvaluator evaluator;
	XalanDocumentPrefixResolver resolver(xalanDoc);
	XalanDOMString xpath(filter_.c_str());
	
	try 
	{
		// Context node is root (first element, not the document node)
		//
                XalanNode* const theContextNode =
			evaluator.selectSingleNode(
				support,
				xalanDoc,
				XalanDOMString("/").c_str(),
				resolver
			);

	
		XObjectPtr result = evaluator.evaluate( support, theContextNode, xpath.c_str( ), resolver);

		const XalanDOMString& str = result->str( );
		std::stringstream s;
		s << str;
		xoap::unlock();
		return s.str();
	}
	catch (std::exception & e)
	{
		xoap::unlock();
		XCEPT_RAISE(xoap::exception::Exception, e.what() );
	}
	catch (const XSLException & e)
	{
		std::ostringstream msg;
	 	msg << e.getMessage() << " " <<  e.getType();
		xoap::unlock();
		XCEPT_RAISE(xoap::exception::Exception, msg.str() );
	}
}

std::list<xoap::SOAPElement> xoap::filter::MessageFilter::extract(xoap::MessageReference & message) throw (xoap::exception::Exception)
{
	XALAN_USING_XERCES(XMLPlatformUtils)

	XALAN_USING_XALAN(XPathEvaluator)
	XALAN_USING_XALAN(XalanDocumentPrefixResolver)
	XALAN_USING_XERCES(LocalFileInputSource)

	XALAN_USING_XALAN(XalanDocument)
	XALAN_USING_XALAN(XalanDocumentPrefixResolver)
	XALAN_USING_XALAN(XalanDOMString)
	XALAN_USING_XALAN(XalanNode)
	XALAN_USING_XALAN(XalanSourceTreeInit)
	XALAN_USING_XALAN(XalanSourceTreeDOMSupport)
	XALAN_USING_XALAN(XalanSourceTreeParserLiaison)
	XALAN_USING_XALAN(XObjectPtr)
	XALAN_USING_XALAN(NodeRefListBase)
	XALAN_USING_XALAN(NodeRefList)
	XALAN_USING_XALAN(XercesDOMWrapperParsedSource)
	
	xoap::lock();			
	
	DOMDocument* doc = message->getDocument();

	XercesParserLiaison liaison;
	XercesDOMSupport support(liaison);
	
	// Needed to provide a mapping between DOM document and Xalan
	// liaison.setBuildWrapperNodes(true);
	// liaison.setBuildMaps(true);
	
	XercesDOMWrapperParsedSource src(doc, liaison, support);
	XalanDocument* xalanDoc = src.getDocument( );

	XPathEvaluator evaluator;
	XalanDocumentPrefixResolver resolver(xalanDoc);
	
	XalanDOMString xpath(filter_.c_str());
	
	// To map xalan nodes back to Xerces nodes
	XercesDocumentWrapper* theWrapper = liaison.mapDocumentToWrapper(src.getDocument());
	
	try 
	{	
	
		// Context node is root (first element, not the document node)
		//
                XalanNode* const theContextNode =
			evaluator.selectSingleNode(
				support,
				xalanDoc,
				XalanDOMString("/").c_str(),
				resolver
			);
				
		XObjectPtr result = evaluator.evaluate( support, theContextNode, xpath.c_str( ), resolver);

		const NodeRefListBase& nodeset = result->nodeset( );

		std::list<SOAPElement> elements;

		for (NodeRefListBase::size_type i = 0; i < nodeset.getLength(); ++i)
		{
			DOMNode * dn = (DOMNode *) theWrapper->mapNode( nodeset.item(i) );
			elements.push_back ( xoap::SOAPElement (dn) );	
		}
		xoap::unlock();
		return elements;		
	}
	catch (std::exception & e)
	{
		xoap::lock();
		XCEPT_RAISE(xoap::exception::Exception, e.what() );
	}
	catch (const XSLException & e)
	{
		std::ostringstream msg;
	 	msg << e.getMessage() << " " <<  e.getType();
		xoap::lock();
		XCEPT_RAISE(xoap::exception::Exception, msg.str() );
	}
}
     
