#include <iostream>
#include <string>
#include "xoap/MessageFactory.h"
#include "xoap/MessageReference.h"
#include "xoap/exception/Exception.h"
#include "xcept/tools.h"
#include "xoap/filter/MessageFilter.h"

int main (int argc, char** argv)
{
	if (argc < 3)
	{
		std::cout << argv[0] << " [file] [filter]" << std::endl;
		return 1;
	}

	XMLPlatformUtils::Initialize();

	try
	{
		xoap::MessageReference msg = xoap::createMessage(argv[1]);
			
		std::cout << "-- Message ---------------------------------------------------" << std::endl;
		msg->writeTo(std::cout);
		std::cout << std::endl << "--------------------------------------------------------------" << std::endl;

		xoap::filter::MessageFilter filter(argv[2]);

		if (filter.match(msg))
		{
			std::cout << "Message matches filter" << std::endl;
		}
		else
		{
			std::cout << "Message does not match filter" << std::endl;
		}

		std::list<xoap::SOAPElement> elements = filter.extract(msg);

		std::cout << "Applying filter '" << argv[2] << "' to message yielded " << elements.size() << " nodes" << std::endl;

		for (std::list<xoap::SOAPElement>::iterator li = elements.begin(); li != elements.end(); ++li)
		{
			xoap::SOAPName name = (*li).getElementName();
			std::cout << "Name: " << name.getLocalName() << ", namespace: " << name.getURI() << std::endl;

			// Check if the element has children and print them up to one layer down
			std::vector<xoap::SOAPElement> children = (*li).getChildElements();

			if (children.size() == 0)
			{
				std::cout << "Node text value: " << (*li).getTextContent() << std::endl;
			}

			std::vector<xoap::SOAPElement>::iterator vi;
			for (vi = children.begin(); vi != children.end(); ++vi)
			{
				xoap::SOAPName childName = (*vi).getElementName();
				std::cout << "  --> Child name: " << childName.getLocalName() << ", value: " << (*vi).getValue() << ", namespace: " << childName.getURI() << std::endl;
			}
		}
	}
	catch (xoap::exception::Exception& e)
	{
		std::cout << xcept::stdformat_exception_history(e) << std::endl;
		return 1;
	}
	return 0;
}
