# $Id: Makefile,v 1.28 2008/07/18 15:28:39 gutleber Exp $

#########################################################################
# XDAQ Components for Distributed Data Acquisition                      #
# Copyright (C) 2000-2009, CERN.			                #
# All rights reserved.                                                  #
# Authors: J. Gutleber and L. Orsini					#
#                                                                       #
# For the licensing terms see LICENSE.		                        #
# For the list of contributors see CREDITS.   			        #
#########################################################################

##
#
# This is the TriDAS/daq/xoap Package Makefile
#
##

BUILD_HOME:=$(shell pwd)/..

ifndef BUILD_SUPPORT
BUILD_SUPPORT=config
endif

ifndef PROJECT_NAME
PROJECT_NAME=daq
endif

include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfAutoconf.rules
include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfDefs.$(XDAQ_OS)

ifndef MFDEFS_SUPPORT
include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfDefs.extern_coretools
include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfDefs.coretools
else
include $(BUILD_HOME)/mfDefs.$(PROJECT_NAME)
endif

#
# Packages to be built
#
Project=$(PROJECT_NAME)
Package=xoap

Sources=\
	domutils.cc \
	Event.cc \
	DOMParserFactory.cc\
	DOMParser.cc\
	SOAPElement.cc\
	SOAPMessage.cc\
	SOAPPart.cc\
	SOAPBody.cc\
	SOAPBodyElement.cc\
	SOAPEnvelope.cc\
	SOAPName.cc\
	SOAPFault.cc\
	SOAPNode.cc\
	SOAPSerializer.cc\
	SOAPAllocator.cc\
	AttachmentPart.cc\
	URLEndpoint.cc\
	Endpoint.cc \
	memSearch.cc \
	HTTPLoader.cc \
	MessageFactory.cc \
	MimeHeaders.cc \
	SOAPHeader.cc \
	SOAPHeaderElement.cc \
	ErrorHandler.cc \
	Lock.cc \
	version.cc \
	ver1_1/SOAPMessageImpl.cc \
	ver1_2/SOAPMessageImpl.cc \
	ver1_1/MessageFactoryImpl.cc \
	ver1_2/MessageFactoryImpl.cc

IncludeDirs = \
	$(XERCES_INCLUDE_PREFIX) \
	$(XCEPT_INCLUDE_PREFIX) \
	$(CONFIG_INCLUDE_PREFIX) \
	$(XDATA_INCLUDE_PREFIX) \
	$(TOOLBOX_INCLUDE_PREFIX)  \
	$(XQILLA_INCLUDE_PREFIX)
	
LibraryDirs = \
	$(LOG4CPLUS_LIB_PREFIX)  \
	$(XERCES_LIB_PREFIX) \
	$(ASYNCRESOLV_LIB_PREFIX) \
	$(XCEPT_LIB_PREFIX) \
	$(XDATA_LIB_PREFIX) \
	$(XQILLA_LIB_PREFIX)

TestLibraryDirs = \
	$(XERCES_LIB_PREFIX) \
	$(CPPUNIT_LIB_PREFIX) \
	$(LOG4CPLUS_LIB_PREFIX)  \
	$(MIMETIC_LIB_PREFIX) \
	$(ASYNCRESOLV_LIB_PREFIX) \
	$(XCEPT_LIB_PREFIX) \
	$(CONFIG_LIB_PREFIX) \
	$(TOOLBOX_LIB_PREFIX)

UserCFlags =
UserCCFlags =
UserDynamicLinkFlags =
UserStaticLinkFlags =
UserExecutableLinkFlags =

# These libraries can be platform specific and
# potentially need conditional processing
#
# FOR Xerces 1.6: Libraries = pthread xerces-c2_3_0
Libraries =

#
# Compile the source files and create a shared library
#
DynamicLibrary=xoap
StaticLibrary=
Executables=

TestExecutables= \
		testMemoryConsumption1.cc \
		SOAPWithAttachment.cc \
		SerializerTest.cc \
		testSOAPName.cc \
		ReferenceCounting.cc \
		sendSOAPMessage.cc \
		PassSOAPElement.cc \
		multiCallSOAPMessage.cc \
		CreateTest01.cc \
		RelocateTest01.cc \
		Attachment1.cc \
		Attachment2.cc \
		Attachment3.cc \
		Attachment4.cc \
		Attachment5.cc \
		Attachment6.cc \
		Attachment7.cc \
		Attachment8.cc \
		Attachment9.cc \
		Attachment10.cc \
		Attachment11.cc \
		Attachment12.cc \
		Attachment13.cc \
		Attachment14.cc \
		Attachment15.cc \
		Attachment16.cc \
		Attachment17.cc \
		Attachment18.cc \
		Attachment19.cc \
		Attachment20.cc \
		Attachment21.cc \
		Attachment22.cc \
		sendWithAttachment01.cc

# for Xerces 1.6: TestLibraries=xoap xerces-c1_6_0 toolbox
TestLibraries= xerces-c uuid config toolbox log4cplus xcept xdata asyncresolv mimetic cgicc

include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/Makefile.rules
include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfRPM.rules
