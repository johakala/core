// $Id: sendSOAPMessage.cc,v 1.6 2008/07/18 15:28:43 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/
#include <iostream>

#include "xoap/SOAPMessage.h"
#include "xoap/SOAPPart.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPBody.h"
#include "xoap/SOAPBodyElement.h"
#include "xoap/SOAPName.h"
#include "xoap/AttachmentPart.h"
#include "xoap/SOAPAllocator.h"
#include "xoap/SOAPConnection.h"
#include "xoap/URLEndpoint.h"
#include "xoap/exception/Exception.h"

using namespace xoap;
 
int main(int argc, char** argv)
{

 if (argc < 6)
 {
 	std::cout << "Usage: " << argv[0] << " hostname portnumber targetAddrStr soapCommand loopcount" << std::endl;
 	return 0;
 }

 // arg1 is hostname
 std::string hostname = argv[1];

 // arg2 is portnumber
 std::string portnumber = argv[2];

 // arg3 is targetAddr
 std::string targetAddrStr = argv[3];

 // arg4 is command
 std::string soapCommand = argv[4];

 // arg5 is loop count
 int count = atoi(argv[5]);
 
 XMLPlatformUtils::Initialize();
 
 for(int i = 0; i< count; i++)
 {
  SOAPMessage request;
  SOAPEnvelope envelope = request.getSOAPPart().getEnvelope();
  SOAPBody body = envelope.getBody();
 
  // set the target method name and add the attributes
  SOAPName name = envelope.createName(soapCommand);
  SOAPElement command = body.addBodyElement(name);
 
  // Add tid of target application
  SOAPName targetAddr = envelope.createName("targetAddr");
  command.addAttribute (targetAddr, targetAddrStr);
 
  // Add tid of this application
  SOAPName originator = envelope.createName("originator");
  command.addAttribute(originator, "45");
 
  std::cout << "send command " << i << " of " << count << std::endl;
 
  SOAPConnection connection;
  std::string urlStr = "http://";
  urlStr += hostname;
  urlStr += ":";
  urlStr += portnumber;

  URLEndpoint url(urlStr);
 
  //cout<<" Request : ------------------------------------ "<<endl;
  //request.writeTo(cout);
  //cout<<" ---------------------------------------------- "<<endl;
 

  //cout<<" Reply : ------------------------------ "<<endl;
  //cout<<" Reply i = "<<dec<<i<<endl;
  
  try {
  	SOAPMessage reply = connection.call(request,url);
  } 
  catch (xoap::exception::Exception soape)
  {
  	std::cout << "SOAP Exception: " << soape.what() << std::endl;
	return 1;
  } 
  catch (...)
  {
  	std::cout << "Caught unspecified exception" << std::endl;
	return 1;
  }
  
  //reply.writeTo(cout);
  //cout<<" -------------------------------------- "<<endl;
 

 }
 
 return 0;
 
}
