// $Id: Attachment21.cc,v 1.7 2008/07/18 15:28:43 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/dom/DOM.hpp>

#include <string>
#include <iostream>
#include <fstream>

#include "xoap/SOAPMessage.h"
#include "xoap/SOAPPart.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPBody.h"
#include "xoap/SOAPBodyElement.h"
#include "xoap/SOAPName.h"
#include "xoap/AttachmentPart.h"
#include "xoap/SOAPAllocator.h"


using namespace xoap;
/*
This example creates a SOAP message with several attachments all containing a text string.
The SOAP message is written to a string, read from the string, and again written to a string.
The two strings are compared.
*/


void t(int count) 
{
	XMLPlatformUtils::Initialize();
	
        //
        // Create SOAP message
	//
	SOAPMessage msg;
	SOAPPart soap = msg.getSOAPPart();
	SOAPEnvelope envelope = soap.getEnvelope();
	SOAPBody body = envelope.getBody();
	
        SOAPName name = envelope.createName ("message");
	SOAPBodyElement bodyElement = body.addBodyElement(name);
	bodyElement.addTextNode ("This is a SOAP message with attachments");

        //
        // Create attachment 1
	//
        char* content1 = "This is attachment 1!";
        int size1 = strlen(content1);
        char * buf1 = new char [size1+1];
        strcpy(buf1, content1);

	AttachmentPart * attachment1 = msg.createAttachmentPart(buf1, size1, "");
	attachment1->addMimeHeader("Content-Type", "text/plain1");
        msg.addAttachmentPart(attachment1);

	delete buf1;
	//
	// Create attachment 2
	//
	char* content2 = "This is attachment 2!";
        int size2 = strlen(content2);
        char * buf2 = new char [size2+1];
        strcpy(buf2, content2);

	AttachmentPart * attachment2 = msg.createAttachmentPart(buf2, size2, "");
	attachment2->addMimeHeader("Content-Type", "text/plain2");
        msg.addAttachmentPart(attachment2);

	delete buf2;
	//
        // Create attachment 3
        //
	char* content3 = "This is attachment 3!";
        int size3 = strlen(content3);
        char * buf3 = new char [size3+1];
        strcpy(buf3, content3);

	AttachmentPart * attachment3 = msg.createAttachmentPart(buf3, size3, "");
	attachment3->addMimeHeader("Content-Type", "text/plain3");
        msg.addAttachmentPart(attachment3);

	delete buf3;
	//
        // Create attachment 4
        //
	char* content4 = "This is attachment 4!";
        int size4 = strlen(content4);
        char * buf4 = new char [size4+1];
        strcpy(buf4, content4);

	AttachmentPart * attachment4 = msg.createAttachmentPart(buf4, size4, "");
	attachment4->addMimeHeader("Content-Type", "text/plain4");
        msg.addAttachmentPart(attachment4);

	delete buf4;
	//
        // Create attachment 5
        //
	char* content5 = "This is attachment 5!";
        int size5 = strlen(content5);
        char * buf5 = new char [size5+1];
        strcpy(buf5, content5);

	AttachmentPart * attachment5 = msg.createAttachmentPart(buf5, size5, "");
	attachment5->addMimeHeader("Content-Type", "text/plain5");
        msg.addAttachmentPart(attachment5);

	delete buf5;

	std::string s1;
	for (int i=0; i < count; i++)
	{
        	msg.writeTo(s1);
		msg.readFrom(s1.c_str(), s1.size());
	}

        std::string s2;
        msg.writeTo(s2);
	std::cout << "s1: " << s1 << std::endl;
	std::cout << "s2: " << s2 << std::endl;

        if ( s1 != s2 )
        {
                std::cout << "\n" << "***Test failed, new and old messages are not identical***" << std::endl;
        } 
	else
        {
                std::cout << "\n" << "***Test passed, messages are identical***" << std::endl;
        }
}

int main (int argc, char** argv)
{
	if (argc < 2)
	{
		std::cout << argv[0] << " count" << std::endl;
		return 1;
	}
	
	t(atoi(argv[1]));
	SOAPAllocator::testMemoryLeakage(); 

	return 0;
}
