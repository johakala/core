// $Id: Attachment10.cc,v 1.7 2008/07/18 15:28:42 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/dom/DOM.hpp>

#include <string>
#include <iostream>
#include <fstream>

#include "xoap/SOAPMessage.h"
#include "xoap/SOAPPart.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPBody.h"
#include "xoap/SOAPBodyElement.h"
#include "xoap/SOAPName.h"
#include "xoap/AttachmentPart.h"
#include "xoap/SOAPAllocator.h"

using namespace xoap;

/*
This example creates a SOAP message with several attachments of different kind.
The SOAP message is written to a string, read from the string, and again written 
to a string. The two strings are compared.
*/


void t() 
{
	XMLPlatformUtils::Initialize();
	
        //
        // Create SOAP message
	//
	SOAPMessage msg;
	SOAPPart soap = msg.getSOAPPart();
	SOAPEnvelope envelope = soap.getEnvelope();
	SOAPBody body = envelope.getBody();
	
        SOAPName name = envelope.createName ("message");
	SOAPBodyElement bodyElement = body.addBodyElement(name);
	bodyElement.addTextNode ("This is a SOAP message with attachments");

        //
        // Create attachment 1
	//
        char* newContent = "This is some string of text which is to be added as an attachment!";
        int sizeA = strlen(newContent);
        char * buf = new char [sizeA+1];
        strcpy(buf, newContent);
	AttachmentPart * attachment1 = msg.createAttachmentPart(buf, sizeA, "content/unknown");
        msg.addAttachmentPart(attachment1);

	//
	// Create attachment 2
	//
	std::string location2 = "../../../test/data/tomcat.gif";
        AttachmentPart * attachment2 = msg.createAttachmentPart(location2);
	attachment2->setContentType("image/gif");
        msg.addAttachmentPart(attachment2);

	//
        // Create attachment 3
        //
	std::string location3 = "../../../test/data/xml-pic.jpg";
        AttachmentPart * attachment3 = msg.createAttachmentPart(location3);
	attachment3->setContentType("image/jpg");
        msg.addAttachmentPart(attachment3);

	//
        // Create attachment 4
        //
	
	std::string location4 = "../../../test/data/test.dat";
	
       	AttachmentPart * attachment4 = msg.createAttachmentPart(location4);
       	attachment4->setContentType("content/unknown");
       	msg.addAttachmentPart(attachment4);

        //
        // Create attachment 5
        //
        AttachmentPart * attachment5 = msg.createAttachmentPart(buf, sizeA, "content/unknown");
        msg.addAttachmentPart(attachment5);

	delete buf;
        //
        // Create attachment 6
        //
        AttachmentPart * attachment6 = msg.createAttachmentPart(location2);
        attachment6->setContentType("image/gif");
        msg.addAttachmentPart(attachment6);

        //
        // Create attachment 7
        //
        AttachmentPart * attachment7 = msg.createAttachmentPart(location3);
        attachment7->setContentType("image/jpg");
        msg.addAttachmentPart(attachment7);

	//
        // Create attachment 8
        //
        AttachmentPart * attachment8 = msg.createAttachmentPart(location4);
        msg.addAttachmentPart(attachment8);

	//
        // Write the message to a string
        //
        std::string s1;
        msg.writeTo(s1);

	//
        // Read message from string and
        // recreate SOAPMessage
        //
        char* read = (char*) s1.c_str();
        SOAPMessage msgNew;
        msgNew.readFrom(read, s1.size());

	//
        // Compare the two strings
        //
        std::string s2;
        msgNew.writeTo(s2);

        if ( s1 != s2 )
        {
                std::cout << "\n" << "***Test failed, new and old messages are not identical***" << std::endl;
        } 
	else
        {
                std::cout << "\n" << "***Test passed, messages are identical***" << std::endl;
        }
}

int main (int argc, char** argv)
{
	t();
	SOAPAllocator::testMemoryLeakage(); 

	return 0;
}
