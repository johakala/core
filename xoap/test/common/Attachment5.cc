// $Id: Attachment5.cc,v 1.7 2008/07/18 15:28:43 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/dom/DOM.hpp>

#include <string>
#include <iostream>
#include <fstream>

#include "xoap/SOAPMessage.h"
#include "xoap/SOAPPart.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPBody.h"
#include "xoap/SOAPBodyElement.h"
#include "xoap/SOAPName.h"
#include "xoap/AttachmentPart.h"
#include "xoap/SOAPAllocator.h"
#include "xoap/domutils.h"

using namespace xoap;

/*
This example creates a SOAP message with an attachment of size 85044 bytes.
The size and the content of the attachment is retrieved.
*/

void t()
{
	XMLPlatformUtils::Initialize();
	
        //
        // Create SOAP message
	//
	SOAPMessage msg;
	SOAPPart soap = msg.getSOAPPart();
	SOAPEnvelope envelope = soap.getEnvelope();
	SOAPBody body = envelope.getBody();
	
        SOAPName name = envelope.createName ("message");
	SOAPBodyElement bodyElement = body.addBodyElement(name);
	bodyElement.addTextNode ("This is a SOAP message with attachments");

        //
        // Create and add attachment
	//
	std::string location = "../../../test/data/test.dat";
	AttachmentPart * attachment = msg.createAttachmentPart(location);
        msg.addAttachmentPart(attachment);

	//
	// Retrieve the size of the attachment
	//
	int attachmentSize = attachment->getSize();
	std::cout << "Size of Attachment: " << attachmentSize << std::endl;

	//
	// Get content and write to file
	//
	char * content = attachment->getContent();

	//
	// Write content to file
	//
	std::ofstream s("attachment5.out");
	s << "The content of the attachment: " << "\n";
	for (int j = 0; j < attachmentSize; j++)
        {
                s.put(content[j]);
        }

	//
	// Read in the file directly
	//
	BinFileInputStream inputSource( (const XMLCh*) XStr(location) );
        int size = inputSource.getSize();
	char * fileContent = new char [size];
        inputSource.readBytes((XMLByte*) fileContent, size );

	//
	// Compare the content of the two
	//
	std::string no1(content, size);
	std::string no2(fileContent, size);
	if (no1 != no2)
	{
		std::cout << "Test failed, 'getContent' did not return the correct content!" << std::endl;
	}
	else
	{
		std::cout << "Test succeeded, 'getContent' did return the correct content!" << std::endl;
	}

	delete fileContent;
}

int main (int argc, char** argv)
{
	t();
	SOAPAllocator::testMemoryLeakage();

	return 0;
}

