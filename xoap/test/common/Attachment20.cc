// $Id: Attachment20.cc,v 1.7 2008/07/18 15:28:42 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/dom/DOM.hpp>

#include <string>
#include <iostream>
#include <fstream>

#include "xoap/SOAPMessage.h"
#include "xoap/SOAPPart.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPBody.h"
#include "xoap/SOAPBodyElement.h"
#include "xoap/SOAPName.h"
#include "xoap/AttachmentPart.h"
#include "xoap/SOAPAllocator.h"

using namespace xoap;
/*
Create a SOAPMessage with an attachment. Set an empty 'Content-Type'
MIME header and try to retrieve it. A SOAPException is thrown.
*/

void t()
{
	try 
	{
		XMLPlatformUtils::Initialize();
	
        	//
        	// Create SOAP message
		//
		xoap::SOAPMessage msg;
		xoap::SOAPPart soap = msg.getSOAPPart();
		xoap::SOAPEnvelope envelope = soap.getEnvelope();
		xoap::SOAPBody body = envelope.getBody();
	
        	xoap::SOAPName name = envelope.createName("message");
		xoap::SOAPBodyElement bodyElement = body.addBodyElement(name);
		bodyElement.addTextNode("This is a SOAP message with an attachment");

        	//
        	// Create attachment
		//
        	xoap::AttachmentPart * attachment = msg.createAttachmentPart();
		std::string location = "../../../test/data/tomcat.gif";
		attachment->setContent(location);
        	msg.addAttachmentPart(attachment);
		attachment->setContentType("");
		std::string contentType = attachment->getContentType();
 
	} 
	catch (xoap::exception::Exception& e)
	{
                std::cout << e.what() << std::endl;

        }
}

int main (int argc, char** argv)
{
	t();
	xoap::SOAPAllocator::testMemoryLeakage();
}

