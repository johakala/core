/*
 * $Id: StandAlone1.java,v 1.1 2002/09/16 08:09:10 lorsini Exp $
 * $Revision: 1.1 $
 * $Date: 2002/09/16 08:09:10 $
 */

/*
 * Copyright 2002 Sun Microsystems, Inc. All rights reserved.
 * SUN PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

import java.io.*;

import javax.xml.soap.*;
import javax.xml.messaging.*;
import java.net.URL;

import javax.mail.internet.*;

import javax.xml.transform.*;
import javax.xml.transform.stream.*;
import javax.activation.*;

import org.dom4j.*;
/*
 * A StandAlone JAXM Client.
 */

public class StandAlone1 {
    
    static final String SIMPLE_SAMPLE_URI = "http://pcepcmd15:40000" ;
    
    public static void main(String args[]) {
        
        try {
            URLEndpoint endpoint = new URLEndpoint(SIMPLE_SAMPLE_URI);
            
	    SOAPConnectionFactory scf = SOAPConnectionFactory.newInstance();
            SOAPConnection connection = scf.createConnection();
            MessageFactory mf = MessageFactory.newInstance();
            
	    //
            // Create a message from the message factory.
            //
            SOAPMessage msg = mf.createMessage();
            
            SOAPPart soapPart=msg.getSOAPPart();
            SOAPEnvelope envelope = soapPart.getEnvelope();
            
	    //
            // Populate SOAP message
	    //
            SOAPBody body = envelope.getBody();
            Name name1 = envelope.createName("soapReply");
            SOAPElement element = body.addChildElement(name1);
            Name name2 = envelope.createName("targetAddr");
            element.addAttribute(name2, "21");
                

	    //
            // Create attachment
	    //
	    String location = "file:/local/helena/xdaq_V1.1/TriDAS/daq/xoap/src/common/examples/files/tomcat.gif";
            URL url = new URL(location);
            AttachmentPart attachment = msg.createAttachmentPart(new DataHandler(url));
            msg.addAttachmentPart(attachment);

            msg.saveChanges();
            
	    //
	    // Send message
	    //
            System.err.println("Sending message to URL: "+ endpoint.getURL());
            SOAPMessage reply = connection.call(msg, endpoint);
            
	    //
	    // Write sent file to file "sent.msg"
	    //
            FileOutputStream sentFile = new FileOutputStream("sent.msg");
            msg.writeTo(sentFile);
            sentFile.close();
            
	    //
	    // Write reply message to file "reply.msg"
	    //
            System.out.println("Received reply from: "+endpoint);
            if (reply != null) {
		//
		// Extract attachments
		//
		java.util.Iterator iterator = reply.getAttachments();
                int k = reply.countAttachments();
                for (int n = 0; n < k; n++)
                {
                	AttachmentPart extractAtt = (AttachmentPart)iterator.next();
                	java.io.InputStream content = (java.io.InputStream)extractAtt.getContent();
                	int b;
                	int l = n + 1;
                        FileOutputStream stream = new FileOutputStream("/local/helena/reply.attachment" + l);
                        while ((b = content.read()) != -1) 
			{
                        	stream.write(b);
			}
                }
		//
		// Write to file
		//
                FileOutputStream replyFile = new FileOutputStream("reply.msg");
                reply.writeTo(replyFile);
                replyFile.close();
            } else {
                System.err.println("No reply");
            }

	    //
            // Display
	    //
            boolean displayResult=true;
            if( displayResult ) {
                // Document source, do a transform.
                System.out.println("Result:");
                TransformerFactory tFact=TransformerFactory.newInstance();
                Transformer transformer = tFact.newTransformer();
                Source src=reply.getSOAPPart().getContent();
                StreamResult result=new StreamResult( System.out );
                transformer.transform(src, result);
                System.out.println();
            }
            
            connection.close();
            
        } catch(Throwable e) {
            e.printStackTrace();
        }
    }
}


