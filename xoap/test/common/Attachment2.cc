// $Id: Attachment2.cc,v 1.7 2008/07/18 15:28:42 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/dom/DOM.hpp>

#include <string>
#include <iostream>
#include <fstream>

#include "xoap/SOAPMessage.h"
#include "xoap/SOAPPart.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPBody.h"
#include "xoap/SOAPBodyElement.h"
#include "xoap/SOAPName.h"
#include "xoap/AttachmentPart.h"
#include "xoap/SOAPAllocator.h"

using namespace xoap;
/*
This example creates a SOAP message with an attachment containing a gif file.
The MIME headers, Content-Type, Content-Id, Content-Location, Content-Encoding,
and Content-Description, are set and retrieved again using the member functions
'getMimeHeader' and 'getAllMimeHeaders'.
The entire SOAP message is written to a file.
*/

void t()
{
	XMLPlatformUtils::Initialize();
	
        //
        // Create SOAP message
	//
	SOAPMessage msg;
	SOAPPart soap = msg.getSOAPPart();
	SOAPEnvelope envelope = soap.getEnvelope();
	SOAPBody body = envelope.getBody();
	
        SOAPName name = envelope.createName("message");
	SOAPBodyElement bodyElement = body.addBodyElement(name);
	bodyElement.addTextNode("This is a SOAP message with an attachment");

        //
        // Create and add attachment
	//
	std::string location = "../../../test/data/tomcat.gif";
        AttachmentPart * attachment = msg.createAttachmentPart();
	attachment->setContent(location);
	msg.addAttachmentPart(attachment);

 	//
	// Set MIME headers
	//
	attachment->setContentType("image/gif");
	attachment->setContentId("giftomcat_soap");
	attachment->setContentLocation(location);
	attachment->setContentEncoding("binary");
	attachment->addMimeHeader("Content-Description", "This is an attachment in a XOAP message");

	//
	// Retrieve all MIME headers using the member function 'getAllMimeHeaders' 
	//
        std::cout << "Retrieve all MIME headers using the member function 'getAllMimeHeaders': " << std::endl;
	std::map<std::string, std::string, std::less<std::string> > mimeHeaders = attachment->getAllMimeHeaders();
        std::map<std::string, std::string, std::less<std::string> >::iterator it;
        for (it = mimeHeaders.begin(); it != mimeHeaders.end(); it++)
        {
                if ((*it).second != "")
                {
                        std::cout << (*it).first << ": " << (*it).second << std::endl;
                }
        }
	std::cout << "\n" << std::endl;

        //
	// Retrieve MIME headers one by one using the member function 'getMimeHeader'
	//
	std::cout << "Retrieve MIME headers using the member function 'getMimeHeader': " << std::endl;
	std::string mimeHeader = attachment->getContentType();
        std::cout << "Content-Type: " << mimeHeader << std::endl;
	mimeHeader = attachment->getContentId();
	std::cout << "Content-Id: " << mimeHeader << std::endl;
	mimeHeader = attachment->getContentLocation();
	std::cout << "Content-Location: " << mimeHeader << std::endl;
	mimeHeader = attachment->getContentEncoding();
	std::cout << "Content-Transfer-Encoding: " << mimeHeader << std::endl;
	mimeHeader = attachment->getMimeHeader("Content-Description");
	std::cout << "Content-Description: " << mimeHeader << "\n" << std::endl;

        //
        // Write message to file
	//
        std::ofstream s("Attachment2.out");
        msg.writeTo(s);
}

int main (int argc, char** argv)
{
	t();
	SOAPAllocator::testMemoryLeakage();

	return 0;
}

