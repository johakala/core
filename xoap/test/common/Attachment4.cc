// $Id: Attachment4.cc,v 1.7 2008/07/18 15:28:43 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/dom/DOM.hpp>

#include <string>
#include <iostream>
#include <fstream>

#include "xoap/SOAPMessage.h"
#include "xoap/SOAPPart.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPBody.h"
#include "xoap/SOAPBodyElement.h"
#include "xoap/SOAPName.h"
#include "xoap/AttachmentPart.h"
#include "xoap/SOAPAllocator.h"

using namespace xoap;
/*
This example creates a SOAP message with three attachments: a text string, a gif file,
and a jpg file. A new MIME boundary is set instead of the default. The sizes of the 
attachments are retrieved. The entire SOAP message is written to a file.
*/

void t()
{
	XMLPlatformUtils::Initialize();
	
        //
        // Create SOAP message
	//
	SOAPMessage msg;
	SOAPPart soap = msg.getSOAPPart();
	SOAPEnvelope envelope = soap.getEnvelope();
	SOAPBody body = envelope.getBody();
	
        SOAPName name = envelope.createName ("message");
	SOAPBodyElement bodyElement = body.addBodyElement(name);
	bodyElement.addTextNode ("This is a SOAP message with attachments");

        //
        // Create attachment1
	//
        char* newContent = "This is some string of text which is to be added as an attachment!";
        int size = strlen(newContent);
        char * buf = new char [size+1];
        strcpy(buf, newContent);
	AttachmentPart * attachment1 = msg.createAttachmentPart(buf, size, "content/unknown");
        msg.addAttachmentPart(attachment1);

	//
	// Create attachment2
	//
	std::string location2 = "../../../test/data/tomcat.gif";
        AttachmentPart * attachment2 = msg.createAttachmentPart(location2);
	attachment2->setContentType("image/gif");
        msg.addAttachmentPart(attachment2);

	//
        // Create attachment3
        //
	std::string location3 = "../../../test/data/xml-pic.jpg";
        AttachmentPart * attachment3 = msg.createAttachmentPart(location3);
	attachment3->setContentType("image/jpg");
        msg.addAttachmentPart(attachment3);

	//
	// Retrieve the size of the attachments
	//
	int size1 = attachment1->getSize();
	int size2 = attachment2->getSize();
	int size3 = attachment3->getSize();

	std::cout << "Size of Attachment 1: " << size1 << std::endl;
	std::cout << "Size of Attachment 2: " << size2 << std::endl;
	std::cout << "Size of Attachment 3: " << size3 << std::endl;

	//
	// Set and retrieve the MIME boundary
	//
	msg.setMimeBoundary("--Attachment4_boundary");
        std::string boundary = msg.getMimeBoundary();
        std::cout << "\n" << "MIME boundary: " << boundary << std::endl;

        //
        // Write message to file
	//
        std::ofstream t("Attachment4.out");
        msg.writeTo(t);

	delete buf;
}

int main (int argc, char** argv)
{
	t();
	SOAPAllocator::testMemoryLeakage();

	return 0;
}

