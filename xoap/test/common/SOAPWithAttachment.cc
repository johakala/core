// $Id: SOAPWithAttachment.cc,v 1.3 2008/07/18 15:28:43 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include <iostream>
#include <string>

#include "xoap/SOAPMessage.h"
#include "xoap/SOAPPart.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPConnection.h"
#include "xoap/SOAPBody.h"
#include "xoap/SOAPBodyElement.h"
#include "xoap/SOAPName.h"
#include "xoap/AttachmentPart.h"
#include "xoap/SOAPAllocator.h"
#include "xoap/URLEndpoint.h"

using namespace xoap;

//
// This test program sends a SOAP message n times to an XDAQ executive
// In inspects the reply value and checks if the attachments are returned.
// The test should not fail and the memory usage should be constant.
//

int main(int argc, char** argv)
{
	if (argc < 4)
	{
		std::cout << "Usage: " << argv[0] << " url SOAPAction file" << std::endl;
		return 0;
	}

	// arg1 is hostname
	std::string urlStr = argv[1];

	// arg2 is portnumber
	std::string action = argv[2];

	// arg3 is filename
	std::string file = argv[3];

	XMLPlatformUtils::Initialize();
 
 	//
 	// Create SOAP message
 	//
	SOAPMessage msg;
	SOAPEnvelope envelope = msg.getSOAPPart().getEnvelope();
	SOAPBody body = envelope.getBody();

	// set the target method name and add the attributes
	SOAPName name = envelope.createName("Data", "xcastor", "http://xdaq.web.cern.ch/xdaq/xsd/2006/xcastor-10");
	SOAPElement command = body.addBodyElement(name);
	msg.getMimeHeaders()->setHeader("SOAPAction", action);

        //
        // Create and add attachment
	//
        AttachmentPart * attachment = msg.createAttachmentPart();
	attachment->setContent(file);
	msg.addAttachmentPart(attachment);
	
	// int attachmentSize = attachment->getSize();

 	//
	// Set MIME headers
	//
	attachment->setContentType("application/octet-stream");
	attachment->setContentId("MyFile");
	attachment->setContentLocation(file);
	attachment->setContentEncoding("binary");
	attachment->addMimeHeader("Content-Description", "This is an attachment in a XOAP message");

 	//
	// Create connection
	//
	SOAPConnection connection;

	URLEndpoint url(urlStr);
 
  //cout<<" Request : ------------------------------------ "<<endl;
  //request.writeTo(cout);
  //cout<<" ---------------------------------------------- "<<endl;
 

  //cout<<" Reply : ------------------------------ "<<endl;
  //cout<<" Reply i = "<<dec<<i<<endl;


  	try 
	{  		  
		std::cout << std::endl;
  		SOAPMessage reply = connection.call(msg,url);
		reply.writeTo(std::cout);
		std::cout << std::endl;
  	} 
	catch (xoap::exception::Exception& soape)
  	{
  		std::cout << "SOAP Exception: " << soape.what() << std::endl;
		return 1;
	} 
	catch (...)
  	{
  		std::cout << "Caught unspecified exception" << std::endl;
		return 1;
  	}
  
  //reply.writeTo(cout);
  //cout<<" -------------------------------------- "<<endl;
 
 return 0;
}
