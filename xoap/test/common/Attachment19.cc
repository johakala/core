// $Id: Attachment19.cc,v 1.7 2008/07/18 15:28:42 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/dom/DOM.hpp>

#include <string>
#include <iostream>
#include <fstream>

#include "xoap/SOAPMessage.h"
#include "xoap/SOAPPart.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPBody.h"
#include "xoap/SOAPBodyElement.h"
#include "xoap/SOAPName.h"
#include "xoap/AttachmentPart.h"
#include "xoap/SOAPAllocator.h"

using namespace xoap;

/*
Create a SOAPMessage with an attachment. Several MIME headers are set.
A SOAPException is thrown when trying to remove a MIME header which
does not exist.
*/

void t()
{
	try {
		XMLPlatformUtils::Initialize();
	
        	//
        	// Create SOAP message
		//
		SOAPMessage msg;
		SOAPPart soap = msg.getSOAPPart();
		SOAPEnvelope envelope = soap.getEnvelope();
		SOAPBody body = envelope.getBody();
	
        	SOAPName name = envelope.createName("message");
		SOAPBodyElement bodyElement = body.addBodyElement(name);
		bodyElement.addTextNode("This is a SOAP message with an attachment");

        	//
        	// Create attachment
		//
        	AttachmentPart * attachment = msg.createAttachmentPart();
		std::string location = "../../../test/data/tomcat.gif";
		attachment->setContent(location);
        	msg.addAttachmentPart(attachment);
		attachment->addMimeHeader("Content-Description", "A XDAQ message");
		attachment->removeMimeHeader("Content-Id");
		std::cout << "Have removed MIME header 'Content-Id'" << std::endl;
		attachment->removeMimeHeader("Content-Description");
		std::cout << "Have removed MIME header 'Content-Description'" << std::endl;
		
		std::cout << "Remove Content-Name should fail, because it does not exist: " << std::endl;
		attachment->removeMimeHeader("Content-Name");
 
	} 
	catch (xoap::exception::Exception& e)
	{
                std::cout << e.what() << std::endl;
        }
}

int main (int argc, char** argv)
{
	std::cout << "Create SOAP with attachment. Create some MIME headers and remove them. One should fail." << std::endl;
	t();
	SOAPAllocator::testMemoryLeakage();
}

