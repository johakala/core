// $Id: Attachment17.cc,v 1.7 2008/07/18 15:28:42 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/dom/DOM.hpp>

#include <string>
#include <iostream>
#include <fstream>

#include "xoap/SOAPMessage.h"
#include "xoap/SOAPPart.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPBody.h"
#include "xoap/SOAPBodyElement.h"
#include "xoap/SOAPName.h"
#include "xoap/AttachmentPart.h"
#include "xoap/SOAPAllocator.h"

using namespace xoap;

/*
Create a SOAPMessage with an attachment. A warning is given when
trying to overwrite MIME headers. A SOAPException is thrown when
trying to add a MIME header which does not start with 'Content-'.
*/

void t()
{
	try {
		XMLPlatformUtils::Initialize();
	
        	//
        	// Create SOAP message
		//
		SOAPMessage msg;
		SOAPPart soap = msg.getSOAPPart();
		SOAPEnvelope envelope = soap.getEnvelope();
		SOAPBody body = envelope.getBody();
	
        	SOAPName name = envelope.createName("message");
		SOAPBodyElement bodyElement = body.addBodyElement(name);
		bodyElement.addTextNode("This is a SOAP message with an attachment");

        	//
        	// Create attachment
		//
        	AttachmentPart * attachment = msg.createAttachmentPart();
		std::string location1 = "../../../test/data/test.dat";
		std::string location2 = "../../../test/data/tomcat.gif";
		
		attachment->setContent(location1);
		
		// Overwrite attachment with different content.
		attachment->setContent(location2);
        	
		msg.addAttachmentPart(attachment);
		attachment->setContentEncoding("8bit");
		attachment->setContentEncoding("binary");
		attachment->setContentLocation("here");
                attachment->setContentLocation(location2);
		attachment->setContentId("file");
                attachment->setContentId("gif_file");
		attachment->addMimeHeader("Content-Type", "content/gif");
		attachment->addMimeHeader("Content-Type", "image/gif");
		
		// Force error "Content-Description" should be o.k.
		//attachment->addMimeHeader("Description", "XDAQ");
		attachment->addMimeHeader("Content-Description", "XDAQ");
 
	} 
	catch (xoap::exception::Exception& e)
	{
		std::cout << e.what() << std::endl;
        }
}

int main (int argc, char** argv)
{
	if (argc < 2)
	{
		std::cout << argv[0] << " times" << std::endl;
		return 1;
	}
	
	std::cout << "Create a SOAP with attachment and overwrite the attachment " << argv[1] << " times." << std::endl;
	
	for (int i = 0; i < atoi(argv[1]); i++)
	{
		t();
	}
	
	SOAPAllocator::testMemoryLeakage();
}

