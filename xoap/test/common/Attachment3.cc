// $Id: Attachment3.cc,v 1.8 2008/07/18 15:28:43 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/dom/DOM.hpp>

#include <string>
#include <iostream>
#include <fstream>

#include "xcept/tools.h"

#include "xoap/SOAPMessage.h"
#include "xoap/SOAPPart.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPBody.h"
#include "xoap/SOAPBodyElement.h"
#include "xoap/SOAPName.h"
#include "xoap/AttachmentPart.h"
#include "xoap/SOAPAllocator.h"

using namespace xoap;
/*
This example creates a SOAP message with an attachment containing a jpg file
and sets various MIME headers. The MIME headers are removed using the member
functions 'removeMimeHeader' and 'removeAllMimeHeaders'. The entire SOAP message
is written to a file.
*/

void t()
{
	XMLPlatformUtils::Initialize();
	
        //
        // Create SOAP message
	//
	SOAPMessage msg;
	SOAPPart soap = msg.getSOAPPart();
	SOAPEnvelope envelope = soap.getEnvelope();
	SOAPBody body = envelope.getBody();
	
        SOAPName name = envelope.createName ("message");
	SOAPBodyElement bodyElement = body.addBodyElement(name);
	bodyElement.addTextNode ("This is a SOAP message with an attachment");

        //
        // Create and add attachment
	//
	std::string location = "../../../test/data/xml-pic.jpg";
	
	// Wrong file link for testing only
	//string location = "../../test/data/xml-pic.jpg";
	
	AttachmentPart * attachment = 0;
	
	try
	{
        	attachment = msg.createAttachmentPart(location);
		msg.addAttachmentPart(attachment);
		//
		// Set MIME Headers
		//
		attachment->setContentType("image/jpg");
		attachment->setContentId("jpgfile");
		attachment->setContentLocation(location);
		attachment->setContentEncoding("binary");
		attachment->addMimeHeader("Content-Description", "This is a message from XDAQ");
	}
	catch (xoap::exception::Exception& e)
	{
		std::cout << "Caught exception while adding attachment: " << std::endl;
		std::cout << xcept::stdformat_exception_history(e) << std::endl;
		return;
	}
 
	//
	// Retrieve All MIME Headers
	//
	std::cout << "All Mime Headers originally set: " << std::endl;
	std::map<std::string, std::string> mimeHeaders1 = attachment->getAllMimeHeaders();
        std::map<std::string, std::string>::iterator it;
        for (it = mimeHeaders1.begin(); it != mimeHeaders1.end(); it++)
        {
                if ((*it).second != "")
                {
                        std::cout << (*it).first << ": " << (*it).second << std::endl;
                }
        }
	std::cout << "\n" << std::endl;

	//
	// Remove the MIME header 'Content-Location' and retrieve the remaining MIME headers
	//
	attachment->removeMimeHeader("Content-Location");
	std::cout << "MIME headers after removal of 'Content-Location': " << std::endl;
	std::map<std::string, std::string> mimeHeaders2 = attachment->getAllMimeHeaders();
        std::map<std::string, std::string>::iterator ite;
        for (ite = mimeHeaders2.begin(); ite != mimeHeaders2.end(); ite++)
        {
                if ((*ite).second != "")
                {
                        std::cout << (*ite).first << ": " << (*ite).second << std::endl;
                }
        }
	std::cout << "\n" << std::endl;

	//
        // Remove the MIME header 'Content-Description' and retrieve the remaining MIME headers
        //
        attachment->removeMimeHeader("Content-Description");
        std::cout << "MIME headers after removal of 'Content-Description': " << std::endl;
        std::map<std::string, std::string> mimeHeaders3 = attachment->getAllMimeHeaders();
        std::map<std::string, std::string>::iterator iter;
        for (iter = mimeHeaders3.begin(); iter != mimeHeaders3.end(); iter++)
        {
                if ((*iter).second != "")
                {
                        std::cout << (*iter).first << ": " << (*iter).second << std::endl;
                }
        }
        std::cout << "\n" << std::endl;

	//
	// Remove all MIME headers
	//
	attachment->removeAllMimeHeaders();
	std::cout << "MIME headers after removal of all: " << std::endl;
	std::map<std::string, std::string> mimeHeaders4 = attachment->getAllMimeHeaders();
        std::map<std::string, std::string>::iterator itera;
        for (itera = mimeHeaders4.begin(); itera != mimeHeaders4.end(); itera++)
        {
                if ((*itera).second != "")
                {
                        std::cout << (*itera).first << ": " << (*itera).second << std::endl;
                }
        }
        std::cout << "\n" << std::endl;

        //
        // Write message to file
	//
        std::ofstream s("Attachment3.out");
        msg.writeTo(s);
}

int main (int argc, char** argv)
{
	t();
	SOAPAllocator::testMemoryLeakage();

	return 0;
}

