// $Id: Attachment15.cc,v 1.7 2008/07/18 15:28:42 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/dom/DOM.hpp>

#include <string>
#include <iostream>
#include <fstream>

#include "xoap/SOAPMessage.h"
#include "xoap/SOAPPart.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPBody.h"
#include "xoap/SOAPBodyElement.h"
#include "xoap/SOAPName.h"
#include "xoap/AttachmentPart.h"
#include "xoap/SOAPAllocator.h"

using namespace xoap;

/*
#if (__GNUC__ == 3)
#include <sstream>
#else
#include <strstream>
#define ostringstream ostrstream;
#endif
*/

/*
Create a SOAPMessage with an attachment. Delete the attachment.
A SOAPException should be thrown when trying to write the message to
a stream.
*/

void t()
{
	try {
		XMLPlatformUtils::Initialize();
	
        	//
        	// Create SOAP message
		//
		SOAPMessage msg;
		SOAPPart soap = msg.getSOAPPart();
		SOAPEnvelope envelope = soap.getEnvelope();
		SOAPBody body = envelope.getBody();
	
        	SOAPName name = envelope.createName("message");
		SOAPBodyElement bodyElement = body.addBodyElement(name);
		bodyElement.addTextNode("This is a SOAP message with an attachment");

        	//
        	// Create attachment
		//
        	AttachmentPart * attachment = msg.createAttachmentPart();
		std::string location = "../../../test/data/tomcat.gif";
		attachment->setContent(location);
        	msg.addAttachmentPart(attachment);
		
		//
		// Clear attachment
		//
		attachment->clearContent();
 
        	//
        	// Write message to screen
		//
        	std::ostringstream s;
        	msg.writeTo(s);
        	std::cout << s.str() << std::endl << std::ends;

	} 
	catch (xoap::exception::Exception& e)
	{
		std::cout << e.what() << std::endl;
        }
}

int main (int argc, char** argv)
{
	std::cout << "Create SOAP with attachment. Then clear the attachment. An exception will be thrown if it works." << std::endl;
	t();
	SOAPAllocator::testMemoryLeakage();

	return 0;
}

