// $Id: Attachment14.cc,v 1.7 2008/07/18 15:28:42 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/dom/DOM.hpp>

#include <string>
#include <iostream>
#include <fstream>

#include "xoap/SOAPMessage.h"
#include "xoap/SOAPPart.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPBody.h"
#include "xoap/SOAPBodyElement.h"
#include "xoap/SOAPName.h"
#include "xoap/AttachmentPart.h"
#include "xoap/SOAPAllocator.h"

using namespace xoap;

/*
Try to add an empty attachment. This is not possible and a SOAPException
should be thrown.
*/

void t()
{
	try 
	{
		XMLPlatformUtils::Initialize();
	
        	//
        	// Create SOAP message
		//
		SOAPMessage msg;
		SOAPPart soap = msg.getSOAPPart();
		SOAPEnvelope envelope = soap.getEnvelope();
		SOAPBody body = envelope.getBody();
	
        	SOAPName name = envelope.createName("message");
		SOAPBodyElement bodyElement = body.addBodyElement(name);
		bodyElement.addTextNode("This is a SOAP message with an attachment");

        	//
        	// Create an empty attachment
		//
        	AttachmentPart * attachment = msg.createAttachmentPart();
        	msg.addAttachmentPart(attachment);

	} 
	catch (xoap::exception::Exception& e)
	{
		std::cout << e.what() << std::endl;
        }
}

int main (int argc, char** argv)
{
	std::cout << "Create SOAP message with an empty attachment and destroy it. Exception should be thrown!" << std::endl;
	t();
	SOAPAllocator::testMemoryLeakage();

	return 0;
}

