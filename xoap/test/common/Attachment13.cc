// $Id: Attachment13.cc,v 1.7 2008/07/18 15:28:42 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/dom/DOM.hpp>

#include <string>
#include <iostream>
#include <fstream>

#include "xoap/SOAPMessage.h"
#include "xoap/SOAPPart.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPBody.h"
#include "xoap/SOAPBodyElement.h"
#include "xoap/SOAPName.h"
#include "xoap/AttachmentPart.h"
#include "xoap/SOAPAllocator.h"

using namespace xoap;

/*
This example creates a SOAP message with an attachment containing a text string.
The SOAP message is written to a string, and then read back from the string
continously.
*/

void t(int n)
{
	XMLPlatformUtils::Initialize();
	
        //
        // Create SOAP message
	//
	SOAPMessage msg;
	SOAPPart soap = msg.getSOAPPart();
	SOAPEnvelope envelope = soap.getEnvelope();
	SOAPBody body = envelope.getBody();
	
        SOAPName name = envelope.createName("message");
	SOAPBodyElement bodyElement = body.addBodyElement(name);
	bodyElement.addTextNode("This is a SOAP message with an attachment");

        //
        // Create attachment
	//
        AttachmentPart * attachment = msg.createAttachmentPart();

        char* newContent = "This is some string of text which is to be added as an attachment!";
        int size = strlen(newContent);
        char * buf = new char [size+1];
        strncpy(buf, newContent, size);
        attachment->setContent(buf, size, "content/unknown");
        msg.addAttachmentPart(attachment);

        delete buf;

	//
	// Make loop: writeTo, readFrom
	//
	for (int i = 0; i < n; i++)
	{
		std::string s1;
		msg.writeTo(s1);
		//cout << std::endl << s1 << std::endl;
		msg.readFrom(s1.c_str(), s1.size());
	}
}

int main (int argc, char** argv)
{
	if (argc < 2)
	{
		std::cout << argv[0] << " number" << std::endl;
		return 1;
	}
	
	int n = atoi (argv[1]);
	t(n);
	SOAPAllocator::testMemoryLeakage();

	return 0;
}

