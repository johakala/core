// $Id: sendWithAttachment01.cc,v 1.5 2008/07/18 15:28:43 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include <iostream>
#include <string>

#include "xoap/SOAPMessage.h"
#include "xoap/SOAPPart.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPConnection.h"
#include "xoap/SOAPBody.h"
#include "xoap/SOAPBodyElement.h"
#include "xoap/SOAPName.h"
#include "xoap/AttachmentPart.h"
#include "xoap/SOAPAllocator.h"
#include "xoap/URLEndpoint.h"

using namespace xoap;

//
// This test program sends a SOAP message n times to an XDAQ executive
// In inspects the reply value and checks if the attachments are returned.
// The test should not fail and the memory usage should be constant.
//

int main(int argc, char** argv)
{
	if (argc < 6)
	{
		std::cout << "Usage: " << argv[0] << " hostname portnumber targetAddrStr soapCommand loopcount" << std::endl;
		return 0;
	}

	// arg1 is hostname
	std::string hostname = argv[1];

	// arg2 is portnumber
	std::string portnumber = argv[2];

	// arg3 is targetAddr
	std::string targetAddrStr = argv[3];

	// arg4 is command
	std::string soapCommand = argv[4];

	// arg5 is loop count
	int count = std::atoi(argv[5]);

	XMLPlatformUtils::Initialize();
 
 	//
 	// Create SOAP message
 	//
	SOAPMessage msg;
	SOAPEnvelope envelope = msg.getSOAPPart().getEnvelope();
	SOAPBody body = envelope.getBody();

	// set the target method name and add the attributes
	SOAPName name = envelope.createName(soapCommand);
	SOAPElement command = body.addBodyElement(name);

	// Add tid of target application
	SOAPName targetAddr = envelope.createName("targetAddr");
	command.addAttribute (targetAddr, targetAddrStr);

	// Add tid of this application
	SOAPName originator = envelope.createName("originator");
	command.addAttribute(originator, "45");
  
        //
        // Create and add attachment
	//
	std::string location = "../../../test/data/tomcat.gif";
        AttachmentPart * attachment = msg.createAttachmentPart();
	attachment->setContent(location);
	msg.addAttachmentPart(attachment);
	
	int attachmentSize = attachment->getSize();

 	//
	// Set MIME headers
	//
	attachment->setContentType("image/gif");
	attachment->setContentId("giftomcat_soap");
	attachment->setContentLocation(location);
	attachment->setContentEncoding("binary");
	attachment->addMimeHeader("Content-Description", "This is an attachment in a XOAP message");

 
 	//
	// Create connection
	//
	SOAPConnection connection;
	std::string urlStr = "http://";
	urlStr += hostname;
	urlStr += ":";
	urlStr += portnumber;

	URLEndpoint url(urlStr);
 
  //cout<<" Request : ------------------------------------ "<<endl;
  //request.writeTo(cout);
  //cout<<" ---------------------------------------------- "<<endl;
 

  //cout<<" Reply : ------------------------------ "<<endl;
  //cout<<" Reply i = "<<dec<<i<<endl;


  	try 
	{  		  
  		//
		// Send message n times
  		//

	 	for (int i = 0; i <  count; i++)
 		{ 
  			SOAPMessage reply = connection.call(msg,url);
			
			 std::list<AttachmentPart*> receivedAttachment = reply.getAttachments();
			 if (receivedAttachment.size() != 1)
			 {
			 	std::cout << "Did not receive exactly one attachment, received " << receivedAttachment.size() << std::endl;
				return 1;
			 }
			 
			//
			// Check size of attachment and compare to original size
			//
			int size = (*(receivedAttachment.begin()))->getSize();
			if (size != attachmentSize)
			{
				std::cout << "Received attachment size " << size << " is different from the one sent originally: " << attachmentSize << std::endl;
				return 1;
			}
		}
  	} 
	catch (xoap::exception::Exception& soape)
  	{
  		std::cout << "SOAP Exception: " << soape.what() << std::endl;
		return 1;
	} catch (...)
  	{
  		std::cout << "Caught unspecified exception" << std::endl;
		return 1;
  	}
  
  //reply.writeTo(cout);
  //cout<<" -------------------------------------- "<<endl;
 

 return 0;
 
}
