// $Id: Attachment11.cc,v 1.7 2008/07/18 15:28:42 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/dom/DOM.hpp>

#include <string>
#include <iostream>
#include <fstream>

#include "xoap/SOAPMessage.h"
#include "xoap/SOAPPart.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPBody.h"
#include "xoap/SOAPBodyElement.h"
#include "xoap/SOAPName.h"
#include "xoap/AttachmentPart.h"
#include "xoap/SOAPAllocator.h"

using namespace xoap;

/*
This example creates a SOAP message with an attachment containing a text string.
A function is called which takes the SOAPMessage as an argument and returns the
SOAPMessage. This involves calling the SOAPMessage objects copy constructor and
assignment operator, and the copy constructor of the AttachmentPart object.
*/

SOAPMessage f(SOAPMessage message)
{
        return message;
}

void t()
{
	XMLPlatformUtils::Initialize();
	
	//
        // Create SOAP message
        //
        SOAPMessage msg;
        SOAPPart soap = msg.getSOAPPart();
        SOAPEnvelope envelope = soap.getEnvelope();
        SOAPBody body = envelope.getBody();
        SOAPName name = envelope.createName("message");
        SOAPBodyElement bodyElement = body.addBodyElement(name);
        bodyElement.addTextNode("This is a SOAP message with an attachment");

        //
        // Create attachment
        //
        char* newContent = "This is some string of text which is to be added as an attachment!";
        int size = strlen(newContent);
        char * buf = new char [size+1];
        strcpy(buf, newContent);

	AttachmentPart * attachment = msg.createAttachmentPart(buf, size, "text/plain");
        msg.addAttachmentPart(attachment);

	delete buf;

	//
	// Call f()
	//
	msg = f(msg);

	//
	// Write SOAPMessage to screen
	//
	std::string s;
	msg.writeTo(s);
	std::cout << "The SOAP message:" << "\n" << s << std::endl;
}

int main (int argc, char** argv)
{
	t();
	SOAPAllocator::testMemoryLeakage();

	return 0;
}

