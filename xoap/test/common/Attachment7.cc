// $Id: Attachment7.cc,v 1.7 2008/07/18 15:28:43 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/dom/DOM.hpp>

#include <string>
#include <iostream>
#include <fstream>

#include "xoap/SOAPMessage.h"
#include "xoap/SOAPPart.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPBody.h"
#include "xoap/SOAPBodyElement.h"
#include "xoap/SOAPName.h"
#include "xoap/AttachmentPart.h"
#include "xoap/SOAPAllocator.h"

using namespace xoap;
/*
This example creates a SOAP message with an attachment containing a text string.
The SOAP message is written to a string, then read from the string back into a
SOAP message. The "new" SOAP message is written back into a string, and the two
strings are compared.
*/

void t()
{
	XMLPlatformUtils::Initialize();
	
        //
        // Create SOAP message
	//
	SOAPMessage msg;
	SOAPPart soap = msg.getSOAPPart();
	SOAPEnvelope envelope = soap.getEnvelope();
	SOAPBody body = envelope.getBody();
	
        SOAPName name = envelope.createName("message");
	SOAPBodyElement bodyElement = body.addBodyElement(name);
	bodyElement.addTextNode("This is a SOAP message with an attachment");

        //
        // Create attachment
	//
        AttachmentPart * attachment = msg.createAttachmentPart();

        char* newContent = "This is some string of text which is to be added as an attachment!";
        int size = strlen(newContent);
        char * buf = new char [size];
        strncpy(buf, newContent, size);
        attachment->setContent(buf, size, "content/unknown");
        msg.addAttachmentPart(attachment);
        delete buf;

	//
	// Write the message to a string
	//
	std::string s1;
	msg.writeTo(s1);

	//
	// Read message from string and
	// recreate the SOAPMessage
	//
	char* read = (char*) s1.c_str();
	SOAPMessage msgNew;
	msgNew.readFrom(read, s1.size());

	//
	// Compare the two strings
	//
	std::string s2;
	msgNew.writeTo(s2);

	if ( s1 != s2 ) 
	{
		std::cout << "\n" << "***Test failed, new and old messages are not identical***" << std::endl;
		std::cout << "First message [" << s1 << "]" << std::endl;

		std::cout << "Second message [" << s2 << "]" <<  std::endl;
	} 
	else
	{
		std::cout << "\n" << "***Test passed, messages are identical***" << std::endl;
	}
}

int main (int argc, char** argv)
{
	t();
	SOAPAllocator::testMemoryLeakage();
}

