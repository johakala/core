// $Id: Attachment8.cc,v 1.7 2008/07/18 15:28:43 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/dom/DOM.hpp>

#include <string>
#include <iostream>
#include <fstream>

#include "xoap/SOAPMessage.h"
#include "xoap/SOAPPart.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPBody.h"
#include "xoap/SOAPBodyElement.h"
#include "xoap/SOAPName.h"
#include "xoap/AttachmentPart.h"
#include "xoap/SOAPAllocator.h"
#include "xoap/domutils.h"

using namespace xoap;
/*
This example creates a SOAP message with an attachment containing a gif file
and several MIME headers. The entire SOAP message is written to a string, read
from the string, transformed back into a SOAP message, and again written to a
string. The two strings are compared.
*/

void t()
{
	XMLPlatformUtils::Initialize();
	
        //
        // Create SOAP message
	//
	SOAPMessage msg;
	SOAPPart soap = msg.getSOAPPart();
	SOAPEnvelope envelope = soap.getEnvelope();
	SOAPBody body = envelope.getBody();
	
        SOAPName name = envelope.createName("message");
	SOAPBodyElement bodyElement = body.addBodyElement(name);
	bodyElement.addTextNode("This is a SOAP message with an attachment");

        //
        // Create attachment
	//
	std::string location = "../../../test/data/tomcat.gif";
        AttachmentPart * attachment = msg.createAttachmentPart(location);
	attachment->setContentType("image/gif");
	attachment->setContentId("tomcatgif_file");
	attachment->setContentLocation(location);
	attachment->setContentEncoding("binary");
	attachment->addMimeHeader("Content-Description","This is an attachment in a XOAP message");
        msg.addAttachmentPart(attachment);

	//
	// Write the message to a string
	//
	std::string s1;
	msg.writeTo(s1);
	
	//
	// Read message from string and
	// recreate SOAPMessage
	//
	char* read = (char*) s1.c_str();
	
	SOAPMessage msgNew;
	msgNew.readFrom(read, s1.size());

	//
	// Compare the two strings
	//
	std::string s2;
	msgNew.writeTo(s2);

	if ( s1 != s2 ) 
	{
		std::cout << "\n" << "***Test failed, new and old messages are not identical***" << std::endl;
	} 
	else
	{
		std::cout << "\n" << "***Test passed, messages are identical***" << std::endl;
	}

	//
	// Write the final message to a file
	//
	std::ofstream s("attachment8.out");
        msgNew.writeTo(s);

}

int main (int argc, char** argv)
{
	t();
	SOAPAllocator::testMemoryLeakage();

	return 0;
}

