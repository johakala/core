// $Id: CreateTest01.cc,v 1.6 2008/07/18 15:28:43 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/dom/DOM.hpp>

#include <string>
#include <iostream>
#include <fstream>

#include "xoap/SOAPMessage.h"
#include "xoap/SOAPPart.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPBody.h"
#include "xoap/SOAPBodyElement.h"
#include "xoap/SOAPName.h"
#include "xoap/AttachmentPart.h"
#include "xoap/SOAPAllocator.h"

using namespace xoap;

void initTest()
{
    XMLPlatformUtils::Initialize();
}

void CTOR1Test ()
{
        //
        // Create empty SOAP message
	//
	SOAPMessage msg;
}

void CTOR2Test()
{
        std::string soap = "<soap-env:Envelope xmlns:soap-env=\"http://schemas.xmlsoap.org/soap/envelope/\"><soap-env:Header/><soap-env:Body><InterfaceQuery targetAddr=\"20\"/></soap-env:Body></soap-env:Envelope>";
        SOAPMessage msg( (char*) soap.c_str(), soap.length() );
}        
  
void CTOR3Test()
{
        SOAPMessage msg1;
        SOAPMessage msg2  = msg1;
}   
  
void CTOR4Test()
{
        std::string soap = "<soap-env:Envelope xmlns:soap-env=\"http://schemas.xmlsoap.org/soap/envelope/\"><soap-env:Header/><soap-env:Body><InterfaceQuery targetAddr=\"20\"/></soap-env:Body></soap-env:Envelope>";
        SOAPMessage msg1 ( (char*) soap.c_str(), soap.length() );
        SOAPMessage msg2 = msg1;
}   



void CTOR5Test()
{
        SOAPMessage msg1;
        SOAPMessage msg2;
        msg1 = msg2;
}   

void CTOR6Test()
{
        std::string soap = "<soap-env:Envelope xmlns:soap-env=\"http://schemas.xmlsoap.org/soap/envelope/\"><soap-env:Header/><soap-env:Body><InterfaceQuery targetAddr=\"20\"/></soap-env:Body></soap-env:Envelope>";
        SOAPMessage msg1 ( (char*) soap.c_str(), soap.length() );
        SOAPMessage msg2;
        msg2 = msg1;
}   

void CTOR7Test()
{
        std::string soap = "<soap-env:Envelope xmlns:soap-env=\"http://schemas.xmlsoap.org/soap/envelope/\"><soap-env:Header/><soap-env:Body><InterfaceQuery targetAddr=\"20\"/></soap-env:Body></soap-env:Envelope>";
        SOAPMessage msg1 ( (char*) soap.c_str(), soap.length() );
        SOAPMessage msg2;
        msg1 = msg2;
}   

void CTOR8Test()
{
        std::string soap = "<soap-env:Envelope xmlns:soap-env=\"http://schemas.xmlsoap.org/soap/envelope/\"><soap-env:Header/><soap-env:Body><InterfaceQuery targetAddr=\"20\"/></soap-env:Body></soap-env:Envelope>";
        SOAPMessage msg1 ( (char*) soap.c_str(), soap.length() );
        SOAPMessage msg2 ( (char*) soap.c_str(), soap.length() );
        
        SOAPMessage msg3 = msg1;
        
        msg3 = msg2;
}  



// helper for Test9
void manual_fill(SOAPMessage & m)
{
        SOAPMessage msg;
	SOAPPart soap = msg.getSOAPPart();
	SOAPEnvelope envelope = soap.getEnvelope();
	SOAPBody body = envelope.getBody();
	
        SOAPName name = envelope.createName("message");
	SOAPBodyElement bodyElement = body.addBodyElement(name);

	SOAPName childName = envelope.createName ("Child");
	SOAPElement e = bodyElement.addChildElement ( childName );
	e.addTextNode("This is a SOAP message");
}

void CTOR9Test()
{
       
        SOAPMessage msg1;
        SOAPMessage msg2;
        SOAPMessage msg3;
        SOAPMessage msg4;
        SOAPMessage msg5;
        SOAPMessage msg6;
        
        manual_fill(msg1);
        msg2 = msg1;
        
        manual_fill(msg3);
        msg4 = msg3;
        
        
        manual_fill(msg5);
        msg6 = msg5;
        
        
}  

// helper functions for tests 100,101,102
SOAPMessage self_return(SOAPMessage  message )
{
        return message;
}

SOAPMessage empty_return(SOAPMessage  message )
{
        SOAPMessage reply;
        return reply;
}

SOAPMessage filled_return(SOAPMessage  message )
{
        std::string soap = "<soap-env:Envelope xmlns:soap-env=\"http://schemas.xmlsoap.org/soap/envelope/\"><soap-env:Header/><soap-env:Body><InterfaceQuery targetAddr=\"20\"/></soap-env:Body></soap-env:Envelope>";
        SOAPMessage reply ( (char*) soap.c_str(), soap.length() );
        return reply;
}

void CTOR100Test()
{
        std::string soap = "<soap-env:Envelope xmlns:soap-env=\"http://schemas.xmlsoap.org/soap/envelope/\"><soap-env:Header/><soap-env:Body><InterfaceQuery targetAddr=\"20\"/></soap-env:Body></soap-env:Envelope>";
        SOAPMessage msg1 ( (char*) soap.c_str(), soap.length() );
        
        SOAPMessage msg2;
        msg2 = self_return(msg1);
}   

void CTOR101Test()
{
        std::string soap = "<soap-env:Envelope xmlns:soap-env=\"http://schemas.xmlsoap.org/soap/envelope/\"><soap-env:Header/><soap-env:Body><InterfaceQuery targetAddr=\"20\"/></soap-env:Body></soap-env:Envelope>";
        SOAPMessage msg1 ( (char*) soap.c_str(), soap.length() );
        
        SOAPMessage msg2;
        msg2 = empty_return(msg1);
        
} 

void CTOR102Test()
{
        std::string soap = "<soap-env:Envelope xmlns:soap-env=\"http://schemas.xmlsoap.org/soap/envelope/\"><soap-env:Header/><soap-env:Body><InterfaceQuery targetAddr=\"20\"/></soap-env:Body></soap-env:Envelope>";
        SOAPMessage msg1 ( (char*) soap.c_str(), soap.length() );
        
        SOAPMessage msg2;
        msg2 = filled_return(msg1);
}
   
void CTOR200Test()
{
        std::string soap = "<soap-env:Envelope xmlns:soap-env=\"http://schemas.xmlsoap.org/soap/envelope/\"><soap-env:Header/><soap-env:Body><InterfaceQuery targetAddr=\"20\"/></soap-env:Body></soap-env:Envelope>";
        SOAPMessage msg1 ( (char*) soap.c_str(), soap.length() );
        SOAPMessage msg2 (msg1);
	
	msg2.readFrom((char*) soap.c_str(), soap.length());
	
	
	//SOAPMessage msg3;
	//msg3.readFrom((char*) soap.c_str(), soap.length());
	
	//msg2.readFrom((char*) soap.c_str(), soap.length());
	//msg3.readFrom((char*) soap.c_str(), soap.length());
	
	
	
}      

void CTOR201Test()
{
        std::string soap = "<soap-env:Envelope xmlns:soap-env=\"http://schemas.xmlsoap.org/soap/envelope/\"><soap-env:Header/><soap-env:Body><InterfaceQuery targetAddr=\"20\"/></soap-env:Body></soap-env:Envelope>";
        SOAPMessage msg1 ( (char*) soap.c_str(), soap.length() );
	
	msg1.readFrom((char*) soap.c_str(), soap.length());
		
	
}  
    
void CTOR202Test()
{
        SOAPMessage msg1;
	  std::string soap = "<soap-env:Envelope xmlns:soap-env=\"http://schemas.xmlsoap.org/soap/envelope/\"><soap-env:Header/><soap-env:Body><InterfaceQuery targetAddr=\"20\"/></soap-env:Body></soap-env:Envelope>";
	msg1.readFrom((char*) soap.c_str(), soap.length());
		
	
} 
  
int main (int argc, char** argv)
{
	initTest();
        
        if (argc < 3)
        {
            std::cout << argv[0] << " num test_case" << std::endl;
            return 1;
        }
        
        int num = atoi(argv[1]);
        int test_case = atoi(argv[2]);
        
        switch (test_case)
        {
            case 1:
                std::cout << "Create an empty SOAP message and delete it " << num << " times." << std::endl;
                for (int i = 0; i < num; i++)
                {
                    CTOR1Test();
                }
                break;
            
            case 2:
                std::cout << "Create a SOAP message from a character buffer and delete it " << num << " times." << std::endl;
                for (int i = 0; i < num; i++)
                {
                    CTOR2Test();
                }
                break;
             case 3:
                std::cout << "Create a SOAP message using copy CTOR from another empty SOAP message " << num << " times." << std::endl;
                for (int i = 0; i < num; i++)
                {
                    CTOR3Test();
                }
                break;
             case 4:
                std::cout << "Create a SOAP message using copy CTOR from another filled (buffer) SOAP message " << num << " times." << std::endl;
                for (int i = 0; i < num; i++)
                {
                    CTOR4Test();
                }
                break;
             case 5:
                std::cout << "Assign empty SOAP message to another empty SOAP message " << num << " times." << std::endl;
                for (int i = 0; i < num; i++)
                {
                    CTOR5Test();
                }
                break;
             case 6:
                std::cout << "Assign a filled SOAP message (buffer) to an empty one " << num << " times." << std::endl;
                for (int i = 0; i < num; i++)
                {
                    CTOR6Test();
                }
                break;
            case 7:
                std::cout << "Assign an empty SOAP message to a filled (buffer) one (overwrite) " << num << " times." << std::endl;
                for (int i = 0; i < num; i++)
                {
                    CTOR7Test();
                }
                break;
            case 8:
                std::cout << "Assign an filled SOAP message to a filled (buffer) one with refcount > 0 (overwrite) " << num << " times." << std::endl;
                for (int i = 0; i < num; i++)
                {
                    CTOR8Test();
                }
                break;
                
              case 9:
                std::cout << "Create n messages and manually fill" << num << " times." << std::endl;
                for (int i = 0; i < num; i++)
                {
                    CTOR9Test();
                }
                break;    
            case 100:
                std::cout << "Function call with (filled) SOAPMessage by value and return of SOAPMessage itself " << num << " times." << std::endl;
                for (int i = 0; i < num; i++)
                {
                    CTOR100Test();
                }
                break;
            case 101:
                std::cout << "Function call with (filled) SOAPMessage by value and return of new empty SOAPMessage " << num << " times." << std::endl;
                for (int i = 0; i < num; i++)
                {
                    CTOR101Test();
                }
                break;
            case 102:
                std::cout << "Function call with (filled) SOAPMessage by value and return of new filled SOAPMessage " << num << " times." << std::endl;
                for (int i = 0; i < num; i++)
                {
                    CTOR101Test();
                }
                break;
	     case 200:
                std::cout << "use of overriding readFrom on filled shared message " << num << " times." << std::endl;
                for (int i = 0; i < num; i++)
                {
                    CTOR200Test();
                }
                break;
	      case 201:
                std::cout << "use of overriding readFrom  on filled parsed message " << num << " times." << std::endl;
                for (int i = 0; i < num; i++)
                {
                    CTOR201Test();
                }
                break;
	      case 202:
                std::cout << "use of overriding readFrom on empty message " << num << " times." << std::endl;
                for (int i = 0; i < num; i++)
                {
                    CTOR202Test();
                }
                break;
        }

        
        
        std::cout << "Tests finished." << std::endl;
	
        //SOAPAllocator::testMemoryLeakage();

	return 0;
}

