// $Id: RelocateTest01.cc,v 1.5 2008/07/18 15:28:43 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/dom/DOM.hpp>

#include <string>
#include <iostream>
#include <fstream>

#include "xoap/SOAPMessage.h"
#include "xoap/SOAPPart.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPBody.h"
#include "xoap/SOAPBodyElement.h"
#include "xoap/SOAPName.h"
#include "xoap/AttachmentPart.h"
#include "xoap/SOAPAllocator.h"

using namespace xoap;
    
SOAPMessage* createAndRelocate()
{
	std::cout << "Create a message" << std::endl;
        SOAPMessage msg1;
	std::string soap = "<soap-env:Envelope xmlns:soap-env=\"http://schemas.xmlsoap.org/soap/envelope/\"><soap-env:Header/><soap-env:Body><InterfaceQuery targetAddr=\"20\"/></soap-env:Body></soap-env:Envelope>";
	msg1.readFrom((char*) soap.c_str(), soap.length());
	
	std::cout << "Create new message using relocate" << std::endl;
	
	SOAPMessage* returnValue = msg1.relocate();
	std::cout << "Return message. Original goes out of scope." << std::endl;
	
	return returnValue;
} 
  
int main (int argc, char** argv)
{
	XMLPlatformUtils::Initialize();
	
	SOAPMessage* msg = createAndRelocate();
	
	std::cout << "Relocated message is: " << std::endl;
	
	//
        // Write message to screen
	//
        std::string s;
        msg->writeTo(s);
        std::cout << s << std::endl;
	
        std::cout << "Delete relocated message." << std::endl;
	
	delete msg;
	
	std::cout << "End of test" << std::endl;
	
        SOAPAllocator::testMemoryLeakage();

	return 0;
}

