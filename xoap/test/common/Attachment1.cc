// $Id: Attachment1.cc,v 1.7 2008/07/18 15:28:42 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/dom/DOM.hpp>

#include <string>
#include <iostream>
#include <fstream>

#include "xoap/SOAPMessage.h"
#include "xoap/SOAPPart.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPBody.h"
#include "xoap/SOAPBodyElement.h"
#include "xoap/SOAPName.h"
#include "xoap/AttachmentPart.h"
#include "xoap/SOAPAllocator.h"

using namespace xoap;

/*
This example creates a SOAP message with an attachment containing a text string.
The entire SOAP message is written to screen.
*/

void t()
{
	XMLPlatformUtils::Initialize();
	
        //
        // Create SOAP message
	//
	SOAPMessage msg;
	SOAPPart soap = msg.getSOAPPart();
	SOAPEnvelope envelope = soap.getEnvelope();
	SOAPBody body = envelope.getBody();
	
        SOAPName name = envelope.createName("message");
	SOAPBodyElement bodyElement = body.addBodyElement(name);
	bodyElement.addTextNode("This is a SOAP message with an attachment");

        //
        // Create attachment
	//
        AttachmentPart * attachment = msg.createAttachmentPart();

        char* newContent = "This is some string of text which is to be added as an attachment!";
        int size = strlen(newContent);
        char * buf = new char [size];
        strncpy(buf, newContent, size);
        attachment->setContent(buf, size, "content/unknown");

        msg.addAttachmentPart(attachment);
 
        //
        // Write message to screen
	//
        std::string s;
        msg.writeTo(s);
        std::cout << s << std::endl;

        delete buf;
}

int main (int argc, char** argv)
{
	t();
	SOAPAllocator::testMemoryLeakage();

	return 0;
}

