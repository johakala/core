// $Id: Attachment6.cc,v 1.7 2008/07/18 15:28:43 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/dom/DOM.hpp>

#include <string>
#include <iostream>
#include <fstream>

#include "xoap/SOAPMessage.h"
#include "xoap/SOAPPart.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPBody.h"
#include "xoap/SOAPBodyElement.h"
#include "xoap/SOAPName.h"
#include "xoap/AttachmentPart.h"
#include "xoap/SOAPAllocator.h"


using namespace xoap;
/*
This example creates a SOAP message with several attachments.
The number of attachments is counted. The attachments are
removed using the member functions 'removeAttachment' and
'removeAllAttachments'.
*/


void t() 
{
	XMLPlatformUtils::Initialize();
	
        //
        // Create SOAP message
	//
	SOAPMessage msg;
	SOAPPart soap = msg.getSOAPPart();
	SOAPEnvelope envelope = soap.getEnvelope();
	SOAPBody body = envelope.getBody();
	
        SOAPName name = envelope.createName ("message");
	SOAPBodyElement bodyElement = body.addBodyElement(name);
	bodyElement.addTextNode ("This is a SOAP message with attachments");

        //
        // Create attachment 1
	//
        char* newContent = "This is some string of text which is to be added as an attachment!";
        int sizeA = strlen(newContent);
        char * buf = new char [sizeA+1];
        strcpy(buf, newContent);
	AttachmentPart * attachment1 = msg.createAttachmentPart(buf, sizeA, "content/unknown");

        msg.addAttachmentPart(attachment1);

	//
	// Create attachment 2
	//
	std::string location2 = "../../../test/data/tomcat.gif";
        AttachmentPart * attachment2 = msg.createAttachmentPart(location2);
	attachment2->setContentType("image/gif");
        msg.addAttachmentPart(attachment2);

	//
        // Create attachment 3
        //
	std::string location3 = "../../../test/data/xml-pic.jpg";
        AttachmentPart * attachment3 = msg.createAttachmentPart(location3);
	attachment3->setContentType("image/jpg");
        msg.addAttachmentPart(attachment3);

	//
        // Create attachment 4
        //
	std::string location4 = "../../../test/data/test.dat";
        AttachmentPart * attachment4 = msg.createAttachmentPart(location4);
        attachment4->setContentType("content/unknown");
        msg.addAttachmentPart(attachment4);

        //
        // Create attachment 5
        //
        AttachmentPart * attachment5 = msg.createAttachmentPart(buf, sizeA, "content/unknown");
        msg.addAttachmentPart(attachment5);

        //
        // Create attachment 6
        //
        AttachmentPart * attachment6 = msg.createAttachmentPart(location2);
        attachment6->setContentType("image/gif");
        msg.addAttachmentPart(attachment6);

        //
        // Create attachment 7
        //
        AttachmentPart * attachment7 = msg.createAttachmentPart(location3);
        attachment7->setContentType("image/jpg");
        msg.addAttachmentPart(attachment7);

	//
        // Create attachment 8
        //
        AttachmentPart * attachment8 = msg.createAttachmentPart(location4);
        msg.addAttachmentPart(attachment8);

	//
	// 
	//
        int numberAtt = msg.countAttachments();
        std::cout << "Number of attachments: " << numberAtt << std::endl;

        //
        // Write message to file
	//
        std::ofstream s("attachment6.out");
        msg.writeTo(s);

	//
	// Remove attachment 2
	//
	msg.removeAttachment(attachment2);
	numberAtt = msg.countAttachments();
	std::cout << "\n" << "Number of attachments after removal of one attachment: " << numberAtt << std::endl;

	//
	// Remove more attachments
	//
	msg.removeAttachment(attachment7);
	msg.removeAttachment(attachment8);
	msg.removeAttachment(attachment6);
	msg.removeAttachment(attachment3);
	msg.removeAttachment(attachment4);

	numberAtt = msg.countAttachments();
        std::cout << "\n" << "Number of attachments after removal of eight attachments: " << numberAtt << std::endl;

	//
        // Write message to screen
        //
	std::ostringstream t;
        msg.writeTo(t);
	std::cout << "\n" << "SOAPMessage with 2 attachments left: " << std::endl;
        std::cout << "\n" << t.str() << std::endl << std::ends;

	//
	// Remove all attachments
	//
	msg.removeAllAttachments();

	//
	// Write message to screen
	//
	std::cout << "SOAPMessage with all attachments removed: " << std::endl;
	std::ostringstream u;
	msg.writeTo(u);
	std::cout << "\n" << u.str() << std::endl << std::ends;

	delete buf;
}

int main (int argc, char** argv)
{
	t();
	SOAPAllocator::testMemoryLeakage(); 

	return 0;
}
