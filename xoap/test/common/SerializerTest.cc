// $Id: SerializerTest.cc,v 1.4 2008/07/18 15:28:43 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/dom/DOM.hpp>

#include <string>
#include <iostream>
#include <fstream>

#include "xoap/SOAPMessage.h"
#include "xoap/SOAPPart.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPBody.h"
#include "xoap/SOAPBodyElement.h"
#include "xoap/SOAPName.h"
#include "xoap/AttachmentPart.h"
#include "xoap/SOAPAllocator.h"
#include "xoap/domutils.h"
#include "xercesc/dom/DOMLSSerializer.hpp"
#include "xercesc/framework/LocalFileFormatTarget.hpp"
#include "xercesc/framework/StdOutFormatTarget.hpp"


using namespace xoap;

void save_to_file(DOMNode *pDoc, const char *strPath)
{
  if (strPath)
  {
  	DOMImplementation *pImplement = NULL;

  	// get a serializer, an instance of DOMWriter (the "LS" stands for load-save).
  	pImplement = DOMImplementationRegistry::getDOMImplementation(xoap::XStr("LS"));
  
    	DOMLSSerializer* writer = ((DOMImplementationLS*)pImplement)->createLSSerializer();

	writer->getDomConfig()->setParameter(XMLUni::fgDOMWRTFormatPrettyPrint,true);

    	writer->writeToURI(pDoc,xoap::XStr(strPath));

	// release the memory
	writer->release();
  }
}

void dump_xml(DOMNode *pDoc)
{
  	DOMImplementation *pImplement = NULL;
  		// these two are needed to display DOM output.
  	DOMLSSerializer *pSerializer = NULL;
  	XMLFormatTarget *pTarget = NULL;
	
  	// get a serializer, an instance of DOMWriter (the "LS" stands for load-save).
  	pImplement = DOMImplementationRegistry::getDOMImplementation(xoap::XStr("LS"));
  	pSerializer = ( (DOMImplementationLS*)pImplement )->createLSSerializer();
  	pTarget = new StdOutFormatTarget();
 	DOMLSOutput* theOutput = ((DOMImplementationLS*)pImplement)->createLSOutput();
    	theOutput->setByteStream(pTarget);
  	// set user specified end of line sequence and output encoding
  	pSerializer->setNewLine( xoap::XStr("\n") );
	
  	// set feature if the serializer supports the feature/mode
  	pSerializer->getDomConfig()->setParameter(XMLUni::fgDOMWRTSplitCdataSections, false);

  	pSerializer->getDomConfig()->setParameter(XMLUni::fgDOMWRTDiscardDefaultContent, false);

  	// turn off serializer "pretty print" option
  	pSerializer->getDomConfig()->setParameter(XMLUni::fgDOMWRTFormatPrettyPrint, true);

  	pSerializer->getDomConfig()->setParameter(XMLUni::fgDOMWRTBOM, false);

  	pSerializer->write(pDoc, theOutput);

	pSerializer->release();
}

void initTest()
{
    XMLPlatformUtils::Initialize();
}

DOMNode* readFile(const char* filename)
{
	std::cout << "Not implemented, use xoap::DOMParser instead" << std::endl;
/*
// DOMImplementationLS contains factory methods for creating objects
// that implement the DOMBuilder and the DOMWriter interfaces
// static const XMLCh gLS[] = { chLatin_L, chLatin_S, chNull };
DOMImplementation *impl = 
     DOMImplementationRegistry::getDOMImplementation(xoap::XStr("LS"));

// construct the DOMBuilder
DOMBuilder* myParser = ((DOMImplementationLS*)impl)->
          createDOMBuilder(DOMImplementationLS::MODE_SYNCHRONOUS, 0);

// parse the XML data, assume it is saved in a local file 
// called "theXMLFile.xml"
// the DOMBuilder will parse the data and return it as a DOM tree
DOMNode* aDOMNode = myParser->parseURI(filename);
return aDOMNode;
*/
}


void CTOR9Test()
{       
	/*
        SOAPMessage msg;
	SOAPPart soap = msg.getSOAPPart();
	SOAPEnvelope envelope = soap.getEnvelope();
	SOAPBody body = envelope.getBody();
	
        SOAPName name = envelope.createName("message");
	SOAPBodyElement bodyElement = body.addBodyElement(name);

	SOAPName childName = envelope.createName ("Child");
	SOAPElement e = bodyElement.addChildElement ( childName );
	e.addTextNode("This is a SOAP message");
	*/
	
	DOMNode* n = readFile("sample.xml");
	
	dump_xml(n);

	save_to_file(n, "/tmp/output.xml");
}  

int main (int argc, char** argv)
{
	initTest();
        
        CTOR9Test();

        
        std::cout << "Tests finished." << std::endl;
	
        //SOAPAllocator::testMemoryLeakage();

	return 0;
}

