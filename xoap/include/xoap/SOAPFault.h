// $Id: SOAPFault.h,v 1.5 2008/07/18 15:28:41 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef xoap_SOAPFault_h
#define xoap_SOAPFault_h

#include <xercesc/dom/DOM.hpp>
#include <xercesc/dom/DOM.hpp>

#include <locale>

#include "xoap/SOAPBodyElement.h"

namespace xoap
{

/*! An element in the SOAPBody object which contains error and/or status information.
    This information may relate to errors in the SOAPMessage object or to problems
    which are not related to the content of the message itself. Problems not related to 
    the message itself are generally errors in processing, such as the inability to
    communicate with an upstream server.
    This class provides member functions to set and retrieve the fault code and fault
    string. 

    A faultcode is intended to identify the fault and may be
    used by the software in further algorithmic processing.
    A faultstring is meant to provide a human readable explanation of the fault.
    For further explanation of faultcode and faultstring see the SOAP specification.

    Depending on the protocol specified while creating the MessageFactory instance, 
    a SOAPFault has sub-elements as defined in the SOAP 1.1/SOAP 1.2 specification.
 */
class SOAPFault : public xoap::SOAPBodyElement 
{
    public:
	SOAPFault (DOMNode* node);
	
        /*! Retrieves the fault code of this SOAPFault object.
           \return a string with the fault code
         */
	std::string getFaultCode();
	
        /*! Retrieves the fault string of this SOAPFault object.
           \return a string with the fault string
         */
	std::string getFaultString();
	
        /*! Sets the fault code of this SOAPFault object.
           \param code - a string with the fault code to be set
         */
	void setFaultCode(const std::string& code)
		throw (xoap::exception::Exception);
	
        /*! Sets the fault string of this SOAPFault object.
           \param str - a string giving an explanation of the fault
         */
	void setFaultString(const std::string& str)
		throw (xoap::exception::Exception);
		
		
        /*!  Creates an optional Detail object and sets it as the Detail object for this SOAPFault object.
	*/
	SOAPElement addDetail()
		throw (xoap::exception::Exception);	
	
        /*!  Returns the optional detail element for this SOAPFault object.
	*/
	SOAPElement getDetail()
		throw (xoap::exception::Exception);	
		
        /*!  Returns true if this SOAPFault has a Detail subelement and false otherwise.
	*/
	bool hasDetail()
		throw (xoap::exception::Exception);
	//
        /*!  Appends or replaces a Reason Text item containing the specified text message and an xml:lang derived from locale.
	*/
 	void	addFaultReasonText(std::string text, std::locale locale) 
		throw (xoap::exception::Exception);

        /*!  Adds a Subcode to the end of the sequence of Subcodes contained by this SOAPFault.
	*/
 	void	appendFaultSubcode(SOAPName subcode) 
		throw (xoap::exception::Exception);

        /*! Gets the fault actor for this SOAPFault object.
	*/
 	std::string	getFaultActor() 
		throw (xoap::exception::Exception);

        /*!  Gets the mandatory SOAP 1.1 fault code for this SOAPFault object as a SAAJ Name object.
	*/
 	SOAPName getFaultCodeAsName() 
		throw (xoap::exception::Exception);

        /*!  Returns the optional Node element value for this SOAPFault object.
	*/
 	std::string	getFaultNode() 
		throw (xoap::exception::Exception);
	
        /*!  Returns a vector of std::locales for which there are associated Reason Text items.
	*/
 	std::vector<std::locale>	getFaultReasonLocales() 
		throw (xoap::exception::Exception);

        /*!  Returns the Reason Text associated with the given std::locale.
	*/
 	std::string	getFaultReasonText(std::locale locale) 
		throw (xoap::exception::Exception);

        /*!  Returns a std::vector of std::string objects containing all of the Reason Text items for this SOAPFault.
	*/
	std::vector<std::string> getFaultReasonTexts() 
		throw (xoap::exception::Exception);

        /*!  Returns the optional Role element value for this SOAPFault object.
	*/
 	std::string getFaultRole() 
		throw (xoap::exception::Exception);

        /*!  Gets the locale of the fault string for this SOAPFault object.
	*/
 	std::locale	getFaultStringLocale() 
		throw (xoap::exception::Exception);

        /*!  Gets the Subcodes for this SOAPFault as a std::vector over QNames.
	*/
 	std::vector<SOAPName> getFaultSubcodes() 
		throw (xoap::exception::Exception);
	
        /*! Removes any Subcodes that may be contained by this SOAPFault.
	*/
 	void	removeAllFaultSubcodes() 
		throw (xoap::exception::Exception);

        /*!  Sets this SOAPFault object with the given fault actor.
	*/
 	void	setFaultActor(std::string faultActor) 
		throw (xoap::exception::Exception);

        /*!  Sets this SOAPFault object with the given fault code.
	*/
 	void	setFaultCode(SOAPName faultCodeQName) 
		throw (xoap::exception::Exception);

        /*!  Creates or replaces any existing Node element value for this SOAPFault object.
	*/
 	void	setFaultNode(std::string uri) 
		throw (xoap::exception::Exception);
	
        /*!  Creates or replaces any existing Role element value for this SOAPFault object.
	*/
 	void	setFaultRole(std::string uri) 
		throw (xoap::exception::Exception);

	/*!  Sets the fault string for this SOAPFault object to the given string and localized to the given locale.
	*/
	void	setFaultString(std::string faultString, std::locale locale) 
		throw (xoap::exception::Exception);
};

}
#endif

