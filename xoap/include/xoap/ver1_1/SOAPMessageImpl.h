// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini and A. Petrucci					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef xoap_ver1_1_SOAPMessageImpl_h
#define xoap_ver1_1_SOAPMessageImpl_h

#include "xoap/SOAPMessage.h"

namespace xoap
{
	class MessageFactory;

	namespace ver1_1
	{

		class SOAPMessageImpl: public SOAPMessage {
    			public:

                        SOAPMessageImpl(xoap::MessageFactory * factImpl) throw (xoap::exception::Exception);

                        SOAPMessageImpl(xoap::MessageFactory * factImpl, char* buf, size_t size) throw (xoap::exception::Exception);

                        SOAPMessageImpl(xoap::MessageFactory * factImpl, DOMNode* node);

                        virtual ~SOAPMessageImpl();

                        SOAPMessage* relocate() throw (xoap::exception::Exception);
		
		};
	}

}

#endif
