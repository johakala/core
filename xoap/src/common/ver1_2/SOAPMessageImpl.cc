// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini and A. Petrucci					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/


#include <xercesc/dom/DOM.hpp>
#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/sax/SAXException.hpp>
#include <xercesc/sax/SAXParseException.hpp>
#include <xercesc/parsers/XercesDOMParser.hpp>
#include <xercesc/dom/DOMException.hpp>
#include <xercesc/dom/DOMNamedNodeMap.hpp>
#include <xercesc/framework/MemBufInputSource.hpp>
#include <xercesc/framework/Wrapper4InputSource.hpp>
#include <xercesc/dom/DOMError.hpp>
#include <xercesc/dom/DOMErrorHandler.hpp>

#include <iomanip>
#include <algorithm>
#include <cstdlib>
#include <map>
#include <string.h>
#include <string>
#include <stdio.h>
#include <memory>
#include "xoap/DOMParserFactory.h"
#include "xoap/DOMParser.h"
#include "xoap/memSearch.h"
#include "xoap/ver1_2/SOAPMessageImpl.h"
#include "xoap/SOAPSerializer.h"
#include "xoap/ErrorHandler.h"
#include "xoap/exception/Exception.h"
#include "xoap/domutils.h"
#include "xoap/MessageFactory.h"


XERCES_CPP_NAMESPACE_USE

xoap::ver1_2::SOAPMessageImpl::SOAPMessageImpl(xoap::MessageFactory * factImpl) throw (xoap::exception::Exception)
        : xoap::SOAPMessage(factImpl)
{
}

xoap::ver1_2::SOAPMessageImpl::~SOAPMessageImpl ()
{
}

xoap::ver1_2::SOAPMessageImpl::SOAPMessageImpl (xoap::MessageFactory * factImpl, char* buf, size_t len) throw (xoap::exception::Exception) 
	: xoap::SOAPMessage(factImpl, buf, len)
{
}

xoap::ver1_2::SOAPMessageImpl::SOAPMessageImpl (xoap::MessageFactory * factImpl, DOMNode* node):  xoap::SOAPMessage(factImpl,node) 
{
}

xoap::SOAPMessage* xoap::ver1_2::SOAPMessageImpl::relocate()  throw (xoap::exception::Exception)
{
	xoap::SOAPMessage* retVal = new xoap::ver1_2::SOAPMessageImpl(*this);
	
	// decrement ref count in new message
	(*referenceCounterPtr_)--;
	
	// Create a new reference counter in the current message and reset it to 0
	referenceCounterPtr_ = new int ();
	*referenceCounterPtr_ = 0;
	
	// Reset the local instance variables
	this->doc_ = 0;
	//this->parser_ = 0;
	this->aps_ = 0;
	this->remember_ = 0;
	
	return retVal;
}


