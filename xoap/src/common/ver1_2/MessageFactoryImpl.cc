// $Id: MessageFactory.cc,v 1.10 2008/07/18 15:28:42 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xoap/MessageFactory.h"
#include "xoap/MessageReference.h"
#include "xoap/ver1_2/SOAPMessageImpl.h"
#include "xoap/ver1_2/MessageFactoryImpl.h"
#include "xoap/SOAPConstants.h"
#include "xoap/Lock.h"

xoap::MessageFactory*  xoap::ver1_2::MessageFactoryImpl::instance = 0;

std::string xoap::ver1_2::MessageFactoryImpl::getProtocolVersion()
{
        return xoap::SOAPConstants::SOAP_1_2_PROTOCOL;
}

std::string xoap::ver1_2::MessageFactoryImpl::getMediaType()
{
        return xoap::SOAPConstants::SOAP_1_2_CONTENT_TYPE;
}

std::string xoap::ver1_2::MessageFactoryImpl::getEnvelopePrefix()
{
        return xoap::SOAPConstants::SOAP_ENV_PREFIX;
}

std::string xoap::ver1_2::MessageFactoryImpl::getURINSEnvelope()
{
        return xoap::SOAPConstants::URI_NS_SOAP_1_2_ENVELOPE;
}

std::string xoap::ver1_2::MessageFactoryImpl::getURINSEncoding()
{
        return xoap::SOAPConstants::URI_NS_SOAP_1_2_ENCODING;
}



xoap::MessageFactory* xoap::ver1_2::MessageFactoryImpl::getInstance()
{
	xoap::lock();
	if ( instance  == 0 )
        {
                instance = new xoap::ver1_2::MessageFactoryImpl();
        }
	xoap::unlock();
        return instance;

}
	
void xoap::ver1_2::MessageFactoryImpl::destroyInstance()
{
	xoap::lock();
	if ( instance != 0 )
        {
                delete instance;
                instance = 0;
        }
	xoap::unlock();

}


xoap::MessageReference xoap::ver1_2::MessageFactoryImpl::createMessage()  throw (xoap::exception::Exception)
{
	xoap::lock();
	xoap::SOAPMessage * msg = 0;
	try
	{
		msg = new xoap::ver1_2::SOAPMessageImpl(this);
	
	}
	catch( xoap::exception::Exception & e )
	{
		xoap::unlock();
                XCEPT_RETHROW (xoap::exception::Exception, "Failed to create SOAP message", e);
	}

	xoap::unlock();
	return xoap::MessageReference( msg );
}

xoap::MessageReference xoap::ver1_2::MessageFactoryImpl::createMessage(char* buf, int size) throw (xoap::exception::Exception)
{
	// The following SOAPMessage CTOR may throw a SOAPException
	xoap::lock();
	xoap::SOAPMessage * msg = 0;
	try
	{

		msg = new xoap::ver1_2::SOAPMessageImpl(this, buf, size);
	}
	catch( xoap::exception::Exception & e )
	{
		xoap::unlock();
		XCEPT_RETHROW (xoap::exception::Exception, "Failed to create SOAP message from character buffer", e);
	}
	xoap::unlock();
	return xoap::MessageReference ( msg );
}

xoap::MessageReference xoap::ver1_2::MessageFactoryImpl::createMessage(DOMNode* node) throw (xoap::exception::Exception)
{
	xoap::lock();	
	xoap::SOAPMessage * msg = 0;
	try
	{
		msg = new xoap::ver1_2::SOAPMessageImpl(this,node);
	}
	catch( xoap::exception::Exception & e )
	{
		xoap::unlock();
		XCEPT_RETHROW (xoap::exception::Exception, "Failed to create SOAP message from DOMNode", e);
	}
	xoap::unlock();	

	return xoap::MessageReference (msg );
}

xoap::MessageReference xoap::ver1_2::MessageFactoryImpl::createMessage(xoap::MessageReference msg) throw (xoap::exception::Exception)
{
	xoap::lock();
	xoap::SOAPMessage* newMsg = 0;
	try
	{
		std::string stringRepresentation;
		msg->writeTo(stringRepresentation);
		newMsg = new xoap::ver1_2::SOAPMessageImpl(this);
		newMsg->readFrom(stringRepresentation.c_str(), stringRepresentation.length());
	}
	catch( xoap::exception::Exception & e )
	{
		xoap::unlock();
		XCEPT_RETHROW (xoap::exception::Exception, "Failed to create SOAP message from xoap::MessageReference", e);
	}
	xoap::unlock();
	return xoap::MessageReference ( newMsg );
}

xoap::MessageReference xoap::ver1_2::MessageFactoryImpl::createMessage(const std::string& filename) throw (xoap::exception::Exception)
{
	xoap::lock();
	
	std::ifstream::pos_type size = 0;
	char * memblock = 0;;
	std::ifstream file (filename.c_str());
  	if (file.is_open())
  	{
		long begin, end;
		begin = file.tellg();
  		file.seekg (0, std::ios::end);
		end = file.tellg();
 		size = (end - begin);
		
		if (size > 0)
		{
    			memblock = new char [size];
		}
		else
		{
			file.close();
			std::stringstream msg;
			msg << "Failed to create SOAP message from empty file '" << filename << "'";
			
			xoap::unlock();

			XCEPT_RAISE (xoap::exception::Exception, msg.str());
		}
		
    		file.seekg (0, std::ios::beg);
    		if (!file.read (memblock, size))
		{
			file.close();
			if (memblock != 0)
			{
				delete[] memblock;
			}
			std::stringstream msg;
			msg << "Failed to create SOAP message from file '" << filename << "', read error.";
			xoap::unlock();
			XCEPT_RAISE (xoap::exception::Exception, msg.str());
		}
    		file.close();
	}
	else
	{
		std::stringstream msg;
		msg << "Failed to create SOAP message from file '" << filename << "', file open error";
		xoap::unlock();
		XCEPT_RAISE (xoap::exception::Exception, msg.str());
	}

	try
	{
		xoap::SOAPMessage * newMsg = new xoap::ver1_2::SOAPMessageImpl(this, memblock, size);
		delete[] memblock;
		memblock = 0;
		xoap::unlock();
		return xoap::MessageReference ( newMsg );
	}
	catch (xoap::exception::Exception& e)
	{
		// Parsed SOAP message text is not needed anymore after message creation
		delete[] memblock;
		memblock = 0;
		std::stringstream msg;
		msg << "Failed to create SOAP message from file '" << filename << "'";
		xoap::unlock();
		XCEPT_RETHROW (xoap::exception::Exception, msg.str(), e);
	}
}
