// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2013, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini and A. Petrucci					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xoap/domutils.h"
#include "xoap/SOAPHeader.h"

#include "xoap/domutils.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/exception/Exception.h"
#include <sstream>

xoap::SOAPEnvelope::SOAPEnvelope ( DOMNode* node ) : xoap::SOAPElement ( node )
{
}

xoap::SOAPName xoap::SOAPEnvelope::createName (const std::string& name)
{
	return xoap::SOAPName (name, "", "");
}

xoap::SOAPName xoap::SOAPEnvelope::createName (const std::string&  name, const std::string& ns, const std::string& uri )
{
	return xoap::SOAPName ( name, ns, uri);
}

bool xoap::SOAPEnvelope::hasHeader() 
{	
	DOMDocument* doc = node_->getOwnerDocument();
	
	// Match <Body> in any namespace. There must exist only one!
	//
	DOMNodeList * l = doc->getElementsByTagNameNS ( node_->getNamespaceURI(), xoap::XStr("Header"));
	
	if (l->getLength() != 1)
	{
		return true;
	} 
	else 
	{
		return false;
	}	
}


xoap::SOAPBody xoap::SOAPEnvelope::getBody() throw (xoap::exception::Exception)
{	
	DOMDocument* doc = node_->getOwnerDocument();
	
	// Match <Body> in any namespace. There must exist only one!
	//
	DOMNodeList * l = doc->getElementsByTagNameNS ( node_->getNamespaceURI(), xoap::XStr("Body"));
	
	if (l->getLength() != 1)
	{
		XCEPT_RAISE(xoap::exception::Exception, "Invalid SOAP message. Not exactly one 'Body' element." );
		
	} else 
	{
		return xoap::SOAPBody ( l->item(0) );
	}	
}

xoap::SOAPBody xoap::SOAPEnvelope::addBody() throw (xoap::exception::Exception)
{	
	xoap::SOAPName bodyName ( "Body", xoap::XMLCh2String(node_->getPrefix()), xoap::XMLCh2String(node_->getNamespaceURI()) );
	xoap::SOAPElement body = this->addChildElement ( bodyName );
	return xoap::SOAPBody ( body.getDOMNode() );
}

xoap::SOAPHeader xoap::SOAPEnvelope::addHeader() throw (xoap::exception::Exception)
{	
	DOMDocument* doc = node_->getOwnerDocument();
	
	// Match <Body> in any namespace. There must exist only one!
	//
	// THIS IS WEIRD!!! The function addHeader tolerates that a header element exists  and returns ir !? GUT
	DOMNodeList * l = doc->getElementsByTagNameNS ( node_->getNamespaceURI(), xoap::XStr("Header"));
	if (l->getLength() == 1)
	{
		return xoap::SOAPHeader ( l->item(0) );
	}
	
	xoap::SOAPName headerName ( "Header", xoap::XMLCh2String(node_->getPrefix()), xoap::XMLCh2String(node_->getNamespaceURI()) );
	xoap::SOAPElement header = this->addChildElement ( headerName );
	return xoap::SOAPHeader ( header.getDOMNode() );
}


xoap::SOAPHeader xoap::SOAPEnvelope::getHeader() throw (xoap::exception::Exception)
{	
	DOMDocument* doc = node_->getOwnerDocument();
	
	// Match <Body> in any namespace. There must exist only one!
	//
	DOMNodeList * l = doc->getElementsByTagNameNS ( node_->getNamespaceURI(), xoap::XStr("Header"));
	
	if (l->getLength() < 1)
	{
		XCEPT_RAISE(xoap::exception::Exception, "Invalid SOAP message. Failed to find '<Header>' element.");
	} 
	else if (l->getLength() > 1)
	{
		std::stringstream msg;
		msg << "Invalid SOAP message, found " << l->getLength() << " header elements: ";
		for (XMLSize_t i = 0; i < l->getLength(); ++i)
		{
			DOMNode* n = l->item(i);
			msg << "(" << xoap::XMLCh2String(n->getPrefix()) << ":" << xoap::XMLCh2String(n->getNamespaceURI()) << ")";
		}
		XCEPT_RAISE(xoap::exception::Exception, msg.str());
	}
	else 
	{
		return xoap::SOAPHeader ( l->item(0) );
	}	
}
