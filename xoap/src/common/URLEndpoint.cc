// $Id: URLEndpoint.cc,v 1.4 2008/07/18 15:28:42 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xoap/URLEndpoint.h"

xoap::URLEndpoint::URLEndpoint( const std::string& id): Endpoint(id)
{
        
}

std::string xoap::URLEndpoint::getURL()
{
    return id_;
}

