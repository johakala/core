// $Id: AttachmentPart.cc,v 1.10 2008/07/18 15:28:42 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/


#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>
#include <xercesc/util/BinFileInputStream.hpp>
#include <xercesc/util/BinMemInputStream.hpp>
#include <xercesc/util/XMLURL.hpp>
#include <xercesc/framework/URLInputSource.hpp>

#include <iostream>
#include <string.h>
#include <string>
#include <fstream>
#include <stdio.h>
#include <memory>

#include "xoap/SOAPAllocator.h"
#include "xoap/AttachmentPart.h"
#include "xoap/domutils.h"

void * xoap::AttachmentPart::operator new (size_t size )
{
	return xoap::SOAPAllocator::allocate(size);
}

void  xoap::AttachmentPart::operator delete (void * ptr )
{
		xoap::SOAPAllocator::dispose(ptr);
}

xoap::AttachmentPart::AttachmentPart() : content_( (char*) 0)
{
	mimeHeaders_["Content-Type"] = "content/unknown";
        mimeHeaders_["Content-Transfer-Encoding"] = "";
	mimeHeaders_["Content-Id"] = "";
	mimeHeaders_["Content-Location"] = "";
	len_ = 0;
	//referenceCounterPtr_ = new int ();
	//*referenceCounterPtr_ = 0;
}

//
// When very last attachment, then free the buffer
//
xoap::AttachmentPart::~AttachmentPart()
{
	if ( content_ != (char*)0 ) {
			  SOAPAllocator::dispose( content_);
	}
	  
	/*if ( *referenceCounterPtr_ == 0 ) {
		if ( content_ != (char*)0 ) {
			  SOAPAllocator::dispose( content_);
		}
		delete referenceCounterPtr_;
	} else {
		(*referenceCounterPtr_)--;
	}
	*/
}

//
// copy CTOR
//
xoap::AttachmentPart::AttachmentPart(const xoap::AttachmentPart & ap)
{
	*this = ap;
	
	//content_ = ap.content_;
       
       
        // len_ = ap.len_;
	//referenceCounterPtr_ = ap.referenceCounterPtr_;
	//(*referenceCounterPtr_)++;
	
}

//
// Assignment operator
//
xoap::AttachmentPart & xoap::AttachmentPart::operator=(const xoap::AttachmentPart & ap)
{

	//if ((char*) content_ != 0)
        //{
        //        cerr << "WARNING: ***The existing content is overwritten***" << endl;
        //}
	this->clearContent();
	content_ = (char*)SOAPAllocator::allocate(ap.len_);
	
	memcpy(content_,ap.content_,ap.len_);
	len_ = ap.len_;

	//char * tmp = ap.getContent(); 
	
	//this->setContent(tmp, ap.getSize(), ap.getContentType());
	
	mimeHeaders_ = ap.mimeHeaders_;

	/*if (this->r
	
	eferenceCounterPtr_ != ap.referenceCounterPtr_)
	{
		// clean up
		if ( content_ != (char*)0 ) {
			  SOAPAllocator::dispose( content_);
		}
		delete referenceCounterPtr_;
		
		referenceCounterPtr_ = ap.referenceCounterPtr_;
		content_ = ap.content_;
        	mimeHeaders_ = ap.mimeHeaders_;
        	len_ = ap.len_;
        	(*referenceCounterPtr_)++;
	}
	*/
	return *this;
}

void xoap::AttachmentPart::clearContent() 
{
	//*referenceCounterPtr_ = 0;
	
	if ( content_ != (char*) 0 ) 
	{
		xoap::SOAPAllocator::dispose( content_);
		len_ = 0;
		content_ = (char*)0;
	}
}

void xoap::AttachmentPart::setContentId(const std::string& id) 
{
	//if (mimeHeaders_["Content-Id"] != "")
	//{
	//	cerr << "WARNING: ***The existing 'Content-Id' is overwritten***" << endl;
	//}
	mimeHeaders_["Content-Id"] = id; 
}

void xoap::AttachmentPart::setContentType(const std::string& type) 
{
	mimeHeaders_["Content-Type"] = type;
}

void xoap::AttachmentPart::setContentLocation(const std::string& location) 
{
	//if (mimeHeaders_["Content-Location"] != "")
        //{
        //        cerr << "WARNING: ***The existing 'Content-Location' is overwritten***" << endl;
        //}
	mimeHeaders_["Content-Location"] = location;
}

void xoap::AttachmentPart::setContentEncoding(const std::string& encoding) 
{
	//if (mimeHeaders_["Content-Transfer-Encoding"] != "")
        //{
        //        cerr << "WARNING: ***The existing 'Content-Transfer-Encoding' is overwritten***" << endl;
        //}
	mimeHeaders_["Content-Transfer-Encoding"] = encoding;
}

void xoap::AttachmentPart::setContent(const char* data, size_t len, const std::string& type) 
{     
	//if ((char*) content_ != 0)
        //{
        //        cerr << "WARNING: ***The existing content is overwritten***" << endl;
        //}
	this->clearContent();
	content_ = (char*)SOAPAllocator::allocate(sizeof(char)*len);
	
	memcpy(content_,data,len);
	len_ = len;
	mimeHeaders_["Content-Type"] = type;
}

void xoap::AttachmentPart::setContent(const std::string& url) throw (xoap::exception::Exception)
{
	//if ((char*) content_ != 0)
        //{
        //        cerr << "WARNING: ***The existing content is overwritten***" << endl;
        //}
	this->clearContent();
	BinFileInputStream  inputSource ( (const XMLCh*)xoap::XStr(url) );
	
	if (inputSource.getIsOpen())
	{	
		unsigned int size = inputSource.getSize();
		if (size == 0)
		{
			std::string msg = "Attached file ";
			msg += url;
			msg += " is empty";
			XCEPT_RAISE(xoap::exception::Exception, msg );
		}

		content_ = (char*) xoap::SOAPAllocator::allocate(sizeof(char)*size);

		inputSource.readBytes ( (XMLByte*)content_, size );
		len_ = size;
	}
	else
	{
		std::string msg = "Cannot add attachment. file open failed: ";
		msg += url;
		XCEPT_RAISE (xoap::exception::Exception, msg);
	}
}



std::string& xoap::AttachmentPart::getContentId()
{
	return mimeHeaders_["Content-Id"];
}

std::string& xoap::AttachmentPart::getContentType() throw (xoap::exception::Exception)
{
	if (mimeHeaders_["Content-Type"] == "")
	{
		XCEPT_RAISE (xoap::exception::Exception, "Content-Type empty");
	}
	return mimeHeaders_["Content-Type"];
}

std::string& xoap::AttachmentPart::getContentLocation() 
{
	return mimeHeaders_["Content-Location"];
}

std::string& xoap::AttachmentPart::getContentEncoding() 
{
	return mimeHeaders_["Content-Transfer-Encoding"];
}

char* xoap::AttachmentPart::getContent()
{
	return content_;
}

size_t xoap::AttachmentPart::getSize() 
{
	return len_;
}

void xoap::AttachmentPart::addMimeHeader(const std::string& name, const std::string & value) throw (xoap::exception::Exception)
{
	size_t pos = name.find("Content-");
	if (pos == std::string::npos) 
	{
		XCEPT_RAISE(xoap::exception::Exception, "The name of a MIME header should start with 'Content-'" );
	} 
	else 
	{	  
        	mimeHeaders_[name] = value;
	}      
}

std::string xoap::AttachmentPart::getMimeHeader(const std::string& name) throw (xoap::exception::Exception)
{
 	//
	// Check if name exists:
	// If not throw a SOAPException
	//
	if ( mimeHeaders_.count(name) == 0) 
	{
		XCEPT_RAISE(xoap::exception::Exception, "The name does not exist, therefore cannot retrieve" );
	} 
	else
	{
		return mimeHeaders_[name];
	}
}

std::map<std::string, std::string, std::less<std::string> > xoap::AttachmentPart::getAllMimeHeaders() 
{
	return mimeHeaders_;   
}

void xoap::AttachmentPart::removeAllMimeHeaders() 
{
	std::map<std::string, std::string, std::less<std::string> >::iterator it;
	for (it = mimeHeaders_.begin(); it != mimeHeaders_.end(); it++) 
	{
	   	 mimeHeaders_.erase((*it).first);
        }   
}	

void xoap::AttachmentPart::removeMimeHeader(const std::string& header) throw (xoap::exception::Exception)
{
	//
	// Check if header exists:
	// If not throw a SOAPException
	//
	if (mimeHeaders_.count(header) == 0)
	{
		XCEPT_RAISE(xoap::exception::Exception, "The MIME header does not exist, therfore cannot delete" );
	} 
	else
	{
    		mimeHeaders_.erase(header);
	}
}
