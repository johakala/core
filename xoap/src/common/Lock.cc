// $Id: Lock.cc,v 1.3 2008/07/18 15:28:42 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "toolbox/BSem.h"
#include "xoap/Lock.h"

static toolbox::BSem * mutex_ = new toolbox::BSem(toolbox::BSem::FULL,true);

void xoap::lock()
{
	mutex_->take();
}

void xoap::unlock()
{
	mutex_->give();
}

