// $Id: Event.cc,v 1.6 2008/07/18 15:28:42 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xoap/Event.h"
#include <string>
#include <sstream>
			
xoap::Event::Event( xoap::MessageReference & msg , const std::string & name):
	  toolbox::Event("urn:xoap-event:Message", 0), msg_(msg), name_(name)
{
//	std::cout << "CTOR of xoap::Event: " << name_ << std::endl;
}

xoap::Event::~Event()
{
	//std::cout << "DTOR of xoap::Event: " << name_ << std::endl;
}


std::string xoap::Event::name()
{
	return name_;
}

xoap::MessageReference  xoap::Event::getMessage()
{
	return msg_;
}
