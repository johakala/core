// $Id: SOAPSerializer.cc,v 1.7 2008/07/18 15:28:42 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/


//XMLFormatter& operator<< (XMLFormatter& strm, const DOMString& s);

#include "xoap/SOAPSerializer.h"
#include "xoap/domutils.h"

static const XMLCh  gEndElement[] = { chOpenAngle, chForwardSlash, chNull };
static const XMLCh  gEndPI[] = { chQuestion, chCloseAngle, chNull};
static const XMLCh  gStartPI[] = { chOpenAngle, chQuestion, chNull };
static const XMLCh  gXMLDecl1[] =
{
        chOpenAngle, chQuestion, chLatin_x, chLatin_m, chLatin_l
    ,   chSpace, chLatin_v, chLatin_e, chLatin_r, chLatin_s, chLatin_i
    ,   chLatin_o, chLatin_n, chEqual, chDoubleQuote, chNull
};
static const XMLCh  gXMLDecl2[] =
{
        chDoubleQuote, chSpace, chLatin_e, chLatin_n, chLatin_c
    ,   chLatin_o, chLatin_d, chLatin_i, chLatin_n, chLatin_g, chEqual
    ,   chDoubleQuote, chNull
};
static const XMLCh  gXMLDecl3[] =
{
        chDoubleQuote, chSpace, chLatin_s, chLatin_t, chLatin_a
    ,   chLatin_n, chLatin_d, chLatin_a, chLatin_l, chLatin_o
    ,   chLatin_n, chLatin_e, chEqual, chDoubleQuote, chNull
};
static const XMLCh  gXMLDecl4[] =
{
        chDoubleQuote, chQuestion, chCloseAngle
    ,   chCR, chLF, chNull
};

static const XMLCh  gStartCDATA[] =
{ 
        chOpenAngle, chBang, chOpenSquare, chLatin_C, chLatin_D,
        chLatin_A, chLatin_T, chLatin_A, chOpenSquare, chNull
};

static const XMLCh  gEndCDATA[] =
{
    chCloseSquare, chCloseSquare, chCloseAngle, chNull
};
static const XMLCh  gStartComment[] =
{ 
    chOpenAngle, chBang, chDash, chDash, chNull
};

static const XMLCh  gEndComment[] =
{
    chDash, chDash, chCloseAngle, chNull
};

static const XMLCh  gStartDoctype[] =
{ 
    chOpenAngle, chBang, chLatin_D, chLatin_O, chLatin_C, chLatin_T,
    chLatin_Y, chLatin_P, chLatin_E, chSpace, chNull
};
static const XMLCh  gPublic[] =
{ 
    chLatin_P, chLatin_U, chLatin_B, chLatin_L, chLatin_I,
    chLatin_C, chSpace, chDoubleQuote, chNull
};
static const XMLCh  gSystem[] =
{ 
    chLatin_S, chLatin_Y, chLatin_S, chLatin_T, chLatin_E,
    chLatin_M, chSpace, chDoubleQuote, chNull
};
static const XMLCh  gStartEntity[] =
{ 
    chOpenAngle, chBang, chLatin_E, chLatin_N, chLatin_T, chLatin_I,
    chLatin_T, chLatin_Y, chSpace, chNull
};
static const XMLCh  gNotation[] =
{ 
    chLatin_N, chLatin_D, chLatin_A, chLatin_T, chLatin_A,
    chSpace, chDoubleQuote, chNull
};




void xoap::SOAPSerializer::serialize(DOMNode* toWrite ) throw (xoap::exception::Exception)
{
	
    if ( toWrite == 0 ) 
    {
    	*gFormatter_ << chNull; 
    	return;
    }
    
    // Get the name and value out for convenience
    const XMLCh*   nodeName = toWrite->getNodeName();
    const XMLCh*   nodeValue = toWrite->getNodeValue();
    unsigned int lent = XMLString::stringLen (nodeValue);

    switch (toWrite->getNodeType())
    {
        case DOMNode::TEXT_NODE:
        {
            gFormatter_->formatBuf(nodeValue, lent, XMLFormatter::CharEscapes);
            break;
        }


        case DOMNode::PROCESSING_INSTRUCTION_NODE :
        {
            *gFormatter_ << XMLFormatter::NoEscapes << gStartPI  << nodeName;
            if (lent > 0)
            {
                *gFormatter_ << chSpace << nodeValue;
            }
            *gFormatter_ << XMLFormatter::NoEscapes << gEndPI;
            break;
        }


        case DOMNode::DOCUMENT_NODE :
        {

            DOMNode* child = toWrite->getFirstChild();
            while( child != 0)
            {
                serialize (child);
                child = child->getNextSibling();
            }
            break;
        }


        case DOMNode::ELEMENT_NODE :
        {
            // The name has to be representable without any escapes
            *gFormatter_  << XMLFormatter::NoEscapes
                         << chOpenAngle << nodeName;

            // Output the element start tag.

            // Output any attributes on this element
            DOMNamedNodeMap* attributes = toWrite->getAttributes();
            int attrCount = attributes->getLength();
            for (int i = 0; i < attrCount; i++)
            {
                DOMNode*  attribute = attributes->item(i);

                //
                //  Again the name has to be completely representable. But the
                //  attribute can have refs and requires the attribute style
                //  escaping.
                //
                *gFormatter_  << XMLFormatter::NoEscapes
                             << chSpace << attribute->getNodeName()
                             << chEqual << chDoubleQuote
                             << XMLFormatter::AttrEscapes
                             << attribute->getNodeValue()
                             << XMLFormatter::NoEscapes
                             << chDoubleQuote;
            }

            //
            //  Test for the presence of children, which includes both
            //  text content and nested elements.
            //
            DOMNode* child = toWrite->getFirstChild();
            if (child != 0)
            {
                // There are children. Close start-tag, and output children.
                // No escapes are legal here
                *gFormatter_ << XMLFormatter::NoEscapes << chCloseAngle;

                while( child != 0)
                {
                    serialize (child);
                    child = child->getNextSibling();
                }

                //
                // Done with children.  Output the end tag.
                // Had to put chNull at the end to be sure
		// that there are no additional data in the stream.
		//
                *gFormatter_ << XMLFormatter::NoEscapes << gEndElement
                            << nodeName << chCloseAngle << chNull;
            }
            else
            {
                //
                //  There were no children. Output the short form close of
                //  the element start tag, making it an empty-element tag.
                //
                *gFormatter_ << XMLFormatter::NoEscapes << chForwardSlash << chCloseAngle;
            }
            break;
        }


        case DOMNode::ENTITY_REFERENCE_NODE:
        {
            DOMNode* child;
            for (child = toWrite->getFirstChild();
                 child != 0;
                 child = child->getNextSibling())
            {
                serialize (child);
            }
            break;
        }

        
        case DOMNode::CDATA_SECTION_NODE:
        {
            *gFormatter_ << XMLFormatter::NoEscapes << gStartCDATA
                        << nodeValue << gEndCDATA;
            break;
        }

        
        case DOMNode::COMMENT_NODE:
        {
            *gFormatter_ << XMLFormatter::NoEscapes << gStartComment
                        << nodeValue << gEndComment;
            break;
        }

        
        case DOMNode::DOCUMENT_TYPE_NODE:
        {
            DOMDocumentType* doctype = (DOMDocumentType *)toWrite;

            *gFormatter_ << XMLFormatter::NoEscapes  << gStartDoctype
                        << nodeName;
            const XMLCh* id = doctype->getPublicId();

            if (id != 0)
	    {
                *gFormatter_ << XMLFormatter::NoEscapes << chSpace << gPublic
                            << id << chDoubleQuote;
	    }

            id = doctype->getSystemId();
            if (id != 0)
            {
                *gFormatter_ << XMLFormatter::NoEscapes << chSpace << gSystem
                            << id << chDoubleQuote;
            }
            
            id = doctype->getInternalSubset(); 
            if (id !=0)
	    {
                *gFormatter_ << XMLFormatter::NoEscapes << chOpenSquare
                            << id << chCloseSquare;
	    }

            *gFormatter_ << XMLFormatter::NoEscapes << chCloseAngle;
            break;
        }
        
        
        case DOMNode::ENTITY_NODE:
        {
            *gFormatter_ << XMLFormatter::NoEscapes << gStartEntity
                        << nodeName;

            const XMLCh* id = ((DOMEntity *)toWrite)->getPublicId();
            if (id != 0)
                *gFormatter_ << XMLFormatter::NoEscapes << gPublic
                            << id << chDoubleQuote;

            id = ((DOMEntity *)toWrite)->getSystemId();
            if (id != 0)
                *gFormatter_ << XMLFormatter::NoEscapes << gSystem
                            << id << chDoubleQuote;
            
            id = ((DOMEntity *)toWrite)->getNotationName();
            if (id != 0)
                *gFormatter_ << XMLFormatter::NoEscapes << gNotation
                            << id << chDoubleQuote;

            *gFormatter_ << XMLFormatter::NoEscapes << chCloseAngle << chCR << chLF;

            break;
        }
        
        
	/*
	  XML_DECL_NODE does not exist in Xerces 2.3
	  
        case DOMNode::XML_DECL_NODE:
        {
            const XMLCh*  str;

            *gFormatter_ << gXMLDecl1 << ((DOMXMLDecl *)toWrite)->getVersion();

            *gFormatter_ << gXMLDecl2 << gEncodingName;
            
            str = ((DOMXMLDecl *)toWrite)->getStandalone();
            if (str != 0)
                *gFormatter_ << gXMLDecl3 << str;
            
            *gFormatter_ << gXMLDecl4;

            break;
        }
	*/
        
        
        default:
	{
		std::string msg = "Cannot serialize unknown DOM node type";
		XCEPT_RAISE (xoap::exception::Exception, msg);
	}
    }
    
    // End of document
    //*gFormatter_ << chNull; 
    
    return;
}

/*
XMLFormatter& operator<< (XMLFormatter& strm, const DOMString& s)
{
	
	
    unsigned int lent = s.length();
    if (lent <= 0) return strm;

    XMLCh*  buf = new XMLCh[lent + 1];
    XMLString::copyNString(buf, s.rawBuffer(), lent);
    buf[lent] = 0;
    strm << buf;
    delete [] buf;
    
    return strm;
}
*/


xoap::SOAPSerializer::SOAPSerializer(std::string& s) : stream_(s) 
{
	    gXmlFile	       	= 0;
	    gDoNamespaces   	= false;
	    gDoExpand	       	= false;
	    gEncodingName	= 0;
	    gUnRepFlags	       	= XMLFormatter::UnRep_CharRef;
	    gFormatter	       	= 0;

	    gFormatter_ = new XMLFormatter(xoap::XStr ("UTF-8"), 
	    		   0,
		           this, 
        	           XMLFormatter::NoEscapes, 
		           gUnRepFlags);
}

xoap::SOAPSerializer::~SOAPSerializer() 
{
    delete gFormatter_;
    delete gEncodingName;
} 


