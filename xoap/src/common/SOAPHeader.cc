// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2013, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini and A.Petrucci					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xoap/SOAPConstants.h"
#include "xoap/SOAPHeader.h"

#include "xoap/SOAPHeader.h"

#include <xercesc/dom/DOM.hpp>
#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/sax/SAXException.hpp>
#include <xercesc/sax/SAXParseException.hpp>
#include <xercesc/parsers/XercesDOMParser.hpp>
#include <xercesc/dom/DOMException.hpp>
#include <xercesc/dom/DOMNamedNodeMap.hpp>
#include <xercesc/framework/MemBufInputSource.hpp>
#include <xercesc/framework/Wrapper4InputSource.hpp>
#include "xoap/domutils.h"
#include "xoap/Lock.h"


xoap::SOAPHeader::SOAPHeader ( DOMNode* node ) : xoap::SOAPElement ( node )
{
}

xoap::SOAPHeaderElement xoap::SOAPHeader::addHeaderElement ( xoap::SOAPName & name )
	throw (xoap::exception::Exception)
{
	// Append a Child to the <Body> that is 
	// the function name
	SOAPElement e = this->addChildElement ( name );	
	return SOAPHeaderElement ( e.getDOMNode() );
}

void xoap::SOAPHeader::removeHeaderElements(const std::string & namespaceURI)
		throw (xoap::exception::Exception)
{

	std::vector<xoap::SOAPHeaderElement> elements = this->examineHeaderElementsByNamespace(namespaceURI);
	for (std::vector<xoap::SOAPHeaderElement>::iterator i = elements.begin(); i != elements.end(); i++ )
	{
		xoap::SOAPName  name = (*i).getElementName();
		this->removeHeaderElement(name);	
	
	}
}
	
void xoap::SOAPHeader::removeHeaderElement(xoap::SOAPName & name)
		throw (xoap::exception::Exception)	
{
	xoap::lock();
	DOMNodeList * headerElements = node_->getChildNodes();
	for (XMLSize_t i = 0; i < headerElements->getLength(); i++ )
	{
		DOMNode * current = headerElements->item(i);
		if ( current->getNodeType() == DOMNode::ELEMENT_NODE ) 
		{			
			
						
			std::string uri  = xoap::XMLCh2String(current->getNamespaceURI());
			std::string localname  = xoap::XMLCh2String(current->getLocalName());
			if ( ( uri == name.getURI() ) &&  (localname == name.getLocalName() )) 
			{
					DOMNode * toBeRemoved = node_->removeChild(current);
					toBeRemoved->release();
					xoap::unlock();
					return;
			}			
		}
	}
	xoap::unlock();
}

	
std::vector<xoap::SOAPHeaderElement> xoap::SOAPHeader::examineHeaderElements()
	throw (xoap::exception::Exception)
{
	std::vector<xoap::SOAPHeaderElement> retVal;
	DOMNodeList * headerElements = node_->getChildNodes();
	for (XMLSize_t i = 0; i < headerElements->getLength(); i++ )
	{
		DOMNode * current = headerElements->item(i);
		if ( current->getNodeType() == DOMNode::ELEMENT_NODE ) 
		{		
			retVal.push_back (xoap::SOAPHeaderElement(headerElements->item(i)));
		}
	}
	return  retVal;
}

std::vector<xoap::SOAPHeaderElement> xoap::SOAPHeader::examineHeaderElementsByNamespace(const std::string & namespaceURI)
	throw (xoap::exception::Exception)
{
	std::vector<xoap::SOAPHeaderElement> retVal;
	DOMNodeList * headerElements = node_->getChildNodes();
	for (XMLSize_t i = 0; i < headerElements->getLength(); i++ )
	{
		DOMNode * current = headerElements->item(i);
		if ( current->getNodeType() == DOMNode::ELEMENT_NODE ) 
		{	
			std::string uri  = xoap::XMLCh2String(current->getNamespaceURI());
			if ( uri == namespaceURI ) 
			{
				retVal.push_back (xoap::SOAPHeaderElement(current));
			}
		}
	}
	return  retVal;
}

std::vector<xoap::SOAPHeaderElement> xoap::SOAPHeader::examineHeaderElements(xoap::SOAPName& name)
	throw (xoap::exception::Exception)
{
	std::vector<xoap::SOAPHeaderElement> retVal;
	DOMNodeList * headerElements = node_->getChildNodes();
	for (XMLSize_t i = 0; i < headerElements->getLength(); i++ )
	{
		DOMNode * current = headerElements->item(i);
		if (( current->getNodeType() == DOMNode::ELEMENT_NODE ) &&
		    ( xoap::XMLCh2String(current->getLocalName()) == name.getLocalName()  ) &&
		    ( xoap::XMLCh2String(current->getNamespaceURI()) == name.getURI()     )
		    )
		{		
			retVal.push_back (xoap::SOAPHeaderElement(headerElements->item(i)));
		}
	}
	return  retVal;
}
	
	
std::vector<xoap::SOAPHeaderElement> xoap::SOAPHeader::extractHeaderElements()
	throw (xoap::exception::Exception)
{
	std::vector<xoap::SOAPHeaderElement> retVal;
	DOMNodeList * headerElements = node_->getChildNodes();
	for (XMLSize_t i = 0; i < headerElements->getLength(); i++ )
	{
		DOMNode * current = headerElements->item(i);
		if ( current->getNodeType() == DOMNode::ELEMENT_NODE ) 
		{			
			retVal.push_back (xoap::SOAPHeaderElement(headerElements->item(i)));
		}
	}
	
	for (std::vector<xoap::SOAPHeaderElement>::iterator i = retVal.begin(); i != retVal.end(); i++ )
	{
		xoap::SOAPName  name = (*i).getElementName();
		this->removeHeaderElement(name);		
	}
	
	return  retVal;
}

std::vector<xoap::SOAPHeaderElement> xoap::SOAPHeader::examineHeaderElements(const std::string & actorURI)
	throw (xoap::exception::Exception)
{
	std::vector<xoap::SOAPHeaderElement> retVal;
	DOMNodeList * headerElements = node_->getChildNodes();
	for (XMLSize_t i = 0; i < headerElements->getLength(); i++ )
	{
		DOMNode * current = headerElements->item(i);
		if ( current->getNodeType() == DOMNode::ELEMENT_NODE ) 
		{		
			DOMNamedNodeMap* attributes = headerElements->item(i)->getAttributes();
			DOMNode * attributeNode = 
				attributes->getNamedItemNS(node_->getNamespaceURI(), xoap::XStr("actor"));
				
			if ( attributeNode != 0 ) 
			{			
				std::string actor  = xoap::XMLCh2String(attributeNode->getNodeValue());
				if ( actor == actorURI ) 
				{
					retVal.push_back (xoap::SOAPHeaderElement(headerElements->item(i)));
				}
			}
			else
			{
				XCEPT_RAISE (xoap::exception::Exception, "Missing actor attribute");
			}			
		}
	}
	return  retVal;
}
	
	
std::vector<xoap::SOAPHeaderElement> xoap::SOAPHeader::extractHeaderElements(const std::string & actorURI)
	throw (xoap::exception::Exception)
{
	std::vector<xoap::SOAPHeaderElement> retVal;
	DOMNodeList * headerElements = node_->getChildNodes();
	for (XMLSize_t i = 0; i < headerElements->getLength(); i++ )
	{
		DOMNode * current = headerElements->item(i);
		if ( current->getNodeType() == DOMNode::ELEMENT_NODE ) 
		{			
			DOMNamedNodeMap* attributes = headerElements->item(i)->getAttributes();
			
			DOMNode * attributeNode = 
				attributes->getNamedItemNS( node_->getNamespaceURI(), xoap::XStr("actor"));
			if ( attributeNode != 0 ) 
			{			
				std::string actor  = xoap::XMLCh2String(attributeNode->getNodeValue());
				if ( actor == actorURI ) 
				{
						retVal.push_back (xoap::SOAPHeaderElement(headerElements->item(i)));
				}
			}
			else
			{
				XCEPT_RAISE (xoap::exception::Exception, "Missing actor attribute");
			}
		}
	}
	
	for (std::vector<xoap::SOAPHeaderElement>::iterator i = retVal.begin(); i != retVal.end(); i++ )
	{
		xoap::SOAPName  name = (*i).getElementName();
		this->removeHeaderElement(name);		
	}
	
	return  retVal;
}
