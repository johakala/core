// $Id: SOAPElement.cc,v 1.19 2008/07/18 15:28:42 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xoap/domutils.h"

#include "xoap/SOAPElement.h"
#include "xoap/SOAPAllocator.h"
#include "xoap/Lock.h"

xoap::SOAPElement::SOAPElement ( DOMNode* element) : xoap::SOAPNode (element)
{
	
}

void xoap::SOAPElement::addAttribute ( xoap::SOAPName& name, const std::string& value )
	throw (xoap::exception::Exception)
{
	xoap::lock();
	
	if (name.getPrefix() != "" ) 
	{
		xoap::XStr nameStr( name.getQualifiedName() );
		xoap::XStr namespaceURI(name.getURI() );
		xoap::XStr valueStr(value);

		try
		{
			((DOMElement*) node_)->setAttributeNS ( namespaceURI, nameStr, valueStr );
		} catch (DOMException& de)
		{
			xoap::unlock();
			// Xerces 2.6: de.getMessage()
			XCEPT_RAISE (xoap::exception::Exception, xoap::XMLCh2String(de.msg));
		}
	}
	else
	{
		xoap::XStr nameStr( name.getLocalName() );
		xoap::XStr valueStr(value);

		try
		{
			((DOMElement*) node_)->setAttribute ( nameStr, valueStr );
		} catch (DOMException& de)
		{
			xoap::unlock();
			// Xerces 2.6: de.getMessage()
			XCEPT_RAISE (xoap::exception::Exception, xoap::XMLCh2String(de.msg));
		}
	}
	xoap::unlock();	
}

xoap::SOAPElement xoap::SOAPElement::addChildElement ( xoap::SOAPName& name ) throw (xoap::exception::Exception)
{
	xoap::lock();
	
	try
	{
        	DOMDocument* doc = node_->getOwnerDocument();
		std::string nameString = name.getQualifiedName();

		DOMElement* newNode;

		if (name.getPrefix() == "") 
		{
			newNode = doc->createElementNS ( 0, xoap::XStr(nameString) );
		} else 
		{
			newNode = doc->createElementNS( xoap::XStr(name.getURI()), xoap::XStr(nameString) );

			// Set namespace definition only, if father does not have it
			if ( xoap::XMLCh2String(node_->getNamespaceURI()) != name.getURI() )
			{
				std::string prefix = "xmlns:";
				prefix += name.getPrefix();
				newNode->setAttribute ( xoap::XStr(prefix), xoap::XStr(name.getURI()) );
			} 
			else
			{
				// namespace is there, but check if prefix is the same
				if (name.getPrefix() != xoap::XMLCh2String(node_->getPrefix()))
				{
					xoap::unlock();
					XCEPT_RAISE(xoap::exception::Exception, "refix mismatch");
				}
			}
		}

   		node_->appendChild ( newNode );
		xoap::unlock();
		return xoap::SOAPElement ( newNode );
	} catch (DOMException& de)
	{
		xoap::unlock();
		// Xerces 2.6: de.getMessage()
		XCEPT_RAISE (xoap::exception::Exception, xoap::XMLCh2String(de.msg));
	}
}

xoap::SOAPElement xoap::SOAPElement::addChildElement (DOMNode* node) throw (xoap::exception::Exception)
{
	xoap::lock();
	try
	{
		DOMDocument* doc = node_->getOwnerDocument();
		DOMNode* newNode = doc->importNode (node, true);
		node_->appendChild ( newNode );
		xoap::unlock();
		return xoap::SOAPElement ( newNode );
	} catch (DOMException& de)
	{
		xoap::unlock();
		// Xerces 2.6: de.getMessage()
		XCEPT_RAISE (xoap::exception::Exception, xoap::XMLCh2String(de.msg));
	}
}

void xoap::SOAPElement::addTextNode ( const std::string& value ) throw (xoap::exception::Exception)
{
	xoap::lock();
	try
	{
		DOMDocument* doc = node_->getOwnerDocument();
		DOMText* textNode = doc->createTextNode ( xoap::XStr(value) );
		node_->appendChild ( textNode );
	} catch (DOMException& de)
	{
		xoap::unlock();
		// Xerces 2.6: de.getMessage()
		XCEPT_RAISE (xoap::exception::Exception, xoap::XMLCh2String(de.msg));
	}
	xoap::unlock();
}

std::string xoap::SOAPElement::getTextContent () throw (xoap::exception::Exception)
{
	return xoap::XMLCh2String( node_->getTextContent() );
}

void xoap::SOAPElement::setTextContent ( const std::string& value ) throw (xoap::exception::Exception)
{
	xoap::lock();
	try
	{
		node_->setTextContent ( xoap::XStr(value) );
	} 
	catch (DOMException& de)
	{
		xoap::unlock();
		// Xerces 2.6: de.getMessage()
		XCEPT_RAISE (xoap::exception::Exception, xoap::XMLCh2String(de.msg));
	}
	xoap::unlock();
}

std::string xoap::SOAPElement::getAttributeValue ( xoap::SOAPName& name ) throw (xoap::exception::Exception)
{
	try
	{
		DOMElement* e = (DOMElement*) node_;
		std::string nameStr = name.getQualifiedName();
		const XMLCh* retVal = e->getAttribute ( xoap::XStr(nameStr) );
		return xoap::XMLCh2String (retVal);
	} catch (DOMException& de)
	{
		// Xerces 2.6: de.getMessage()
		XCEPT_RAISE (xoap::exception::Exception, xoap::XMLCh2String(de.msg));
	}
}

DOMNode* xoap::SOAPElement::getDOM()
{
	return node_;
}
		
std::vector<xoap::SOAPElement> xoap::SOAPElement::getChildElements()
{
	std::vector<xoap::SOAPElement> retVal;
	DOMNodeList* l = node_->getChildNodes();
	for (unsigned int i = 0; i < l->getLength(); i++)
	{
		retVal.push_back (xoap::SOAPElement(l->item(i)));
	}
	return retVal;
}
		
std::vector<xoap::SOAPElement> xoap::SOAPElement::getChildElements ( xoap::SOAPName& name )
{
	std::vector<xoap::SOAPElement> retVal;
	DOMNodeList* l = node_->getChildNodes();
	for (unsigned int i = 0; i < l->getLength(); i++)
	{
		xoap::SOAPElement e(l->item(i));
		if (e.getElementName() == name) 
		{
			retVal.push_back (e);
		}
	}
	return retVal;
}


xoap::SOAPName xoap::SOAPElement::getElementName()
{
	const XMLCh* name = node_->getLocalName();
	const XMLCh* prefix = 0;
	const XMLCh* uri = 0;

	// if no namespace/prefix is available, create with ordinary DOM level 1 node name
	//
	if (name == 0)
	{
		name = node_->getNodeName();
	}
	else
	{
	// otherwise extraxt the prefix and the namespace URI
	//
		prefix = node_->getPrefix();
		uri = node_->getNamespaceURI();
	}
		
	xoap::SOAPName retVal (xoap::XMLCh2String(name), xoap::XMLCh2String(prefix), xoap::XMLCh2String(uri));
	return retVal;
}

void xoap::SOAPElement::addNamespaceDeclaration (const std::string& prefix, const std::string& uri)
	throw (xoap::exception::Exception)
{

	xoap::lock();
	try
	{
		std::string ns = "xmlns:";
		ns += prefix;
		((DOMElement*)node_)->setAttribute( xoap::XStr(ns), xoap::XStr(uri) );
	} catch (DOMException& de)
	{
		xoap::unlock();
		// Xerces 2.6: de.getMessage()
		XCEPT_RAISE (xoap::exception::Exception, xoap::XMLCh2String(de.msg));
	}
	xoap::unlock();
	
}

void xoap::SOAPElement::removeNamespaceDeclaration (const std::string& prefix)
	throw (xoap::exception::Exception)
{
	xoap::lock();
	try
	{
		std::string ns = "xmlns:";
		ns += prefix;
		((DOMElement*)node_)->removeAttribute( xoap::XStr(ns) );
	} catch (DOMException& de)
	{
		xoap::unlock();
		// Xerces 2.6: de.getMessage()
		XCEPT_RAISE (xoap::exception::Exception, xoap::XMLCh2String(de.msg));
	}
	xoap::unlock();
}

// --------------------------
// Protected functions
// --------------------------
void xoap::SOAPElement::appendNodeWithPrefix (const std::string& name, const std::string& value) 
	throw (xoap::exception::Exception)
{
	xoap::lock();
	try 
	{
		DOMDocument* doc = node_->getOwnerDocument();
        	const XMLCh* prefix = doc->getDocumentElement()->getPrefix();
                const XMLCh* nsuri = doc->getDocumentElement()->getNamespaceURI();


		DOMElement* e = 0;

		if (prefix != 0)
		{
			std::string tmp = xoap::XMLCh2String(prefix);
	                tmp += ":";
	                tmp += name;
			 e = doc->createElementNS (nsuri, xoap::XStr(tmp) );
		} else 
		{
			 e = doc->createElementNS ( 0, xoap::XStr(name) );
		}

		node_->appendChild(e);
		xoap::SOAPElement n(e);
		n.addTextNode (value);	
	} catch (DOMException& de)
	{
		std::string msg =  "Failed to append node ";
		// Xerces 2.6: de.getMessage()
		msg += xoap::XMLCh2String (de.msg);
		xoap::unlock();
		XCEPT_RAISE(xoap::exception::Exception, msg);
	}
	xoap::unlock();
}
