// $Id: SOAPPart.cc,v 1.6 2008/07/18 15:28:42 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xoap/SOAPPart.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPAllocator.h"

xoap::SOAPPart::SOAPPart (DOMNode* envelope)
{
	envelope_ = envelope;
}

xoap::SOAPEnvelope xoap::SOAPPart::getEnvelope()
{
	return xoap::SOAPEnvelope(envelope_);
}
