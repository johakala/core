// $Id: DOMParserFactory.cc,v 1.2 2008/07/18 15:28:42 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and S. Murray					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xoap/DOMParserFactory.h"
#include <sstream>

xoap::DOMParserFactory* xoap::DOMParserFactory::instance_ = 0;

xoap::DOMParserFactory* xoap::DOMParserFactory::getInstance()
{
	if ( instance_ == 0 )
	{
		instance_ = new xoap::DOMParserFactory();
	}
	return instance_;
}

		//! Destroy the manager and destroy all pools.
		//
void xoap::DOMParserFactory::destroyInstance()
{
	if ( instance_ != 0 )
	{
		delete instance_;
		instance_ = 0;
	}
	
}
		
xoap::DOMParser* xoap::DOMParserFactory::create (const std::string& name) throw (xoap::exception::Exception)
{
	this->lock();
	if (parsers_.find(name) != parsers_.end())
	{
		std::stringstream msg;
		msg << "Failed to create DOMParser, '" << name << "' already existing";
		this->unlock();
		XCEPT_RAISE (xoap::exception::Exception, msg.str());
	}
	else
	{
		xoap::DOMParser * p = new xoap::DOMParser();
		parsers_[name] = p;
		this->unlock();
		return p;
	}
}

bool  xoap::DOMParserFactory::has (const std::string& name)
{
	this->lock();
	std::map<std::string, xoap::DOMParser*, std::less<std::string> >::iterator i = parsers_.find(name);
	if (i == parsers_.end())
	{
		this->unlock();
		return false;
	}
	else
	{
		this->unlock();
		return true;
	}
}

xoap::DOMParser* xoap::DOMParserFactory::get (const std::string& name) throw (xoap::exception::Exception)
{
	this->lock();
	std::map<std::string, xoap::DOMParser*, std::less<std::string> >::iterator i = parsers_.find(name);
	if (i == parsers_.end())
	{
		xoap::DOMParser * p = new xoap::DOMParser();
		parsers_[name] = p;
		this->unlock();
		return p;
	}
	else
	{
		this->unlock();
		return (*i).second;
	}
}


void xoap::DOMParserFactory::destroy (const std::string & name) throw (xoap::exception::Exception)
{
	this->lock();	
	std::map<std::string, xoap::DOMParser*, std::less<std::string> >::iterator i = parsers_.find(name);
	if (i == parsers_.end())
	{
		std::stringstream msg;
		msg << "Failed to destroy non-existing DOMParser, '" << name << "'";
		this->unlock();
		XCEPT_RAISE (xoap::exception::Exception, msg.str());
	}
	else
	{
		DOMParser* p = (*i).second;
		parsers_.erase(i);
		delete p;		
		this->unlock();
	}
}

xoap::DOMParserFactory::DOMParserFactory(): mutex_(toolbox::BSem::FULL)
{

}

xoap::DOMParserFactory::~DOMParserFactory()
{

}
	
xoap::DOMParserFactory* xoap::getDOMParserFactory()
{
	return xoap::DOMParserFactory::getInstance();
}

void xoap::DOMParserFactory::lock()
{
	mutex_.take();
}

void xoap::DOMParserFactory::unlock()
{
	mutex_.give();
}
