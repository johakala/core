// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini and A. Petrucci					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/


#include <xercesc/dom/DOM.hpp>
#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/sax/SAXException.hpp>
#include <xercesc/sax/SAXParseException.hpp>
#include <xercesc/parsers/XercesDOMParser.hpp>
#include <xercesc/dom/DOMException.hpp>
#include <xercesc/dom/DOMNamedNodeMap.hpp>
#include <xercesc/framework/MemBufInputSource.hpp>
#include <xercesc/framework/Wrapper4InputSource.hpp>
#include <xercesc/dom/DOMError.hpp>
#include <xercesc/dom/DOMErrorHandler.hpp>

#include <iomanip>
#include <algorithm>
#include <cstdlib>
#include <map>
#include <string.h>
#include <string>
#include <stdio.h>
#include <memory>
#include "xoap/DOMParserFactory.h"
#include "xoap/DOMParser.h"
#include "xoap/memSearch.h"
#include "xoap/SOAPMessage.h"
#include "xoap/SOAPSerializer.h"
#include "xoap/ErrorHandler.h"
#include "xoap/exception/Exception.h"
#include "xoap/domutils.h"
#include "xoap/MessageFactory.h"


XERCES_CPP_NAMESPACE_USE

xoap::MessageFactory * xoap::SOAPMessage::getImplementationFactory()
{
	return factImpl_;
}
xoap::SOAPMessage::SOAPMessage(xoap::MessageFactory * factImpl) throw (xoap::exception::Exception): factImpl_(factImpl)
{


	parser_ = 0;
    	boundary_ = "XDAQ_Boundary";
	doc_ = 0;
	
	try
	{
		DOMImplementation* impl = DOMImplementationRegistry::getDOMImplementation ( xoap::XStr (xoap::DOMImplementationFeatures) );
		

		doc_ = impl->createDocument(
		       xoap::XStr(factImpl->getURINSEnvelope()),		// root element namespace URI.
		       xoap::XStr(factImpl->getEnvelopePrefix()+":Envelope"),    					// root element name
		       0);    								// document type object (DTD).

		envelope_ = doc_->getDocumentElement();

		envelope_->setAttribute ( xoap::XStr("xmlns:" + factImpl->getEnvelopePrefix()), xoap::XStr(factImpl->getURINSEnvelope()) );
		//envelope_->setAttributeNS (xoap::XStr(factImpl->getURINSEnvelope()), xoap::XStr(factImpl->getEnvelopePrefix()+ ":encodingStyle"), XStr(factImpl->getURINSEncoding()) );	

		DOMElement * header = doc_->createElementNS ( xoap::XStr(factImpl->getURINSEnvelope()),xoap::XStr(factImpl->getEnvelopePrefix()+":Header") );
		envelope_->appendChild(header);

		DOMElement* body = doc_->createElementNS ( xoap::XStr(factImpl->getURINSEnvelope()),xoap::XStr(factImpl->getEnvelopePrefix()+":Body") );
		envelope_->appendChild(body);		
	}
	catch ( const XMLException& xmle )
	{
		XCEPT_RAISE (xoap::exception::Exception, xoap::XMLCh2String(xmle.getMessage()));
	}
	catch ( const DOMException& de )
	{
		// Xerces 2.6: de.getMessage()
		XCEPT_RAISE (xoap::exception::Exception, xoap::XMLCh2String(de.msg));
	}
  
	referenceCounterPtr_ = new int ();
	aps_ =  new std::list<xoap::AttachmentPart*>();
	remember_ = new std::list<xoap::AttachmentPart*>();
	*referenceCounterPtr_ = 0;
}


xoap::SOAPMessage::SOAPMessage (xoap::MessageFactory * factImpl, char* buf, size_t len) throw (xoap::exception::Exception): factImpl_(factImpl)
{
	doc_ = 0;
        parser_ = 0;
	boundary_ = "XDAQ_Boundary";
        referenceCounterPtr_ = new int ();
	aps_ =  new std::list<xoap::AttachmentPart*>();
	remember_ = new std::list<xoap::AttachmentPart*>();
	*referenceCounterPtr_ = 0;
	
	// The readFrom may throw a xoap::exception::Exception if the parding fails
	
	// std::cout << "---------------------- INCOMING --------------------" << std::endl;
	// std::cout.write(buf,len);
	// std::cout << std::endl;
	// std::cout << "---------------------- -------- --------------------" << std::endl;
	
	this->readFrom(buf, len);
}

xoap::SOAPMessage::~SOAPMessage ()
{
	if ( *referenceCounterPtr_ == 0 ) 
	{
		// delete all attachements in remember
		
		// Added check if aps_ and remember_ != 0
		
		if (remember_ != 0)
		{
			std::list<xoap::AttachmentPart*>::iterator iter;	
			for (iter = remember_->begin(); iter != remember_->end() ; iter++) 
       			{
				delete *iter;
			}
			delete remember_;
		}
		
		if (aps_ != 0)
			delete aps_;
		
		// Delete document:
                // iff created without builder, release document node.
                // Otherwise, release parser.
		// Added doc_ != 0
		//
	
		if ((parser_ == 0) && (doc_ != 0)) doc_->release();
                else if (parser_ != 0 ) {
			delete parser_;
		}

                delete referenceCounterPtr_;
	}
	else 
	{
		(*referenceCounterPtr_)--;
	}		
}



/*
If this is implemented, a clean import must
be performed, creating a DOMBuilder as well!
*/
xoap::SOAPMessage::SOAPMessage (xoap::MessageFactory * factImpl, DOMNode* node): factImpl_(factImpl)
{
        boundary_ = "XDAQ_Boundary";
        parser_ = 0;
	
        doc_ = node->getOwnerDocument();
        envelope_ = doc_->getDocumentElement();
	referenceCounterPtr_ = new int ();
	*referenceCounterPtr_ = 0;
}


xoap::SOAPMessage::SOAPMessage(const xoap::SOAPMessage & msg)
{
	//cout << "SOAPMessage::SOAPMessage(const SOAPMessage & msg)" << endl;

	factImpl_ = msg.factImpl_;
        aps_ = msg.aps_;
        remember_ = msg.remember_;
        boundary_ = msg.boundary_;
        doc_ = msg.doc_;
        parser_ = msg.parser_;
        envelope_ = msg.envelope_;
	referenceCounterPtr_ = msg.referenceCounterPtr_;
	(*referenceCounterPtr_)++;
}


xoap::SOAPMessage::SOAPMessage(xoap::SOAPMessage & msg)
{
	//cout << "SOAPMessage::SOAPMessage(SOAPMessage & msg)" << endl;

	factImpl_ = msg.factImpl_;
        aps_ = msg.aps_;
        remember_ = msg.remember_;
        boundary_ = msg.boundary_;
        doc_ = msg.doc_;
        parser_ = msg.parser_;
        envelope_ = msg.envelope_;
	referenceCounterPtr_ = msg.referenceCounterPtr_;
	(*referenceCounterPtr_)++;
}

xoap::SOAPMessage & xoap::SOAPMessage::operator=(const xoap::SOAPMessage & msg)
{
	//cout << "SOAPMessage & SOAPMessage::operator=(const SOAPMessage & msg)" << endl;

        if (this->referenceCounterPtr_ == msg.referenceCounterPtr_)
        {
            // assignment of the same message. Do nothing.
            return *this;
        }
        
        // others still point to this message,
        // just decrement ref counter and assign new contents
        if ( *referenceCounterPtr_ > 0) 
        {	
                (*referenceCounterPtr_)--;
        
		factImpl_ = msg.factImpl_;
                aps_ = msg.aps_;
                remember_ = msg.remember_;
                boundary_ = msg.boundary_;
             
                doc_ = msg.doc_;
                parser_ = msg.parser_;
                envelope_ = msg.envelope_;
		referenceCounterPtr_ = msg.referenceCounterPtr_;
		(*referenceCounterPtr_)++;
            
        } 
	else 
	{
                // clean up
		delete referenceCounterPtr_;
		
		// delete all attachements in remember
		// Added check for != 0
		
		if (remember_ != 0)
		{
			std::list<xoap::AttachmentPart*>::iterator iter;	
			for (iter = remember_->begin(); iter != remember_->end() ; iter++) 
       			{
				delete *iter;
			}
			delete remember_;
		}
		
		if (aps_ != 0)
			delete aps_;
		
		factImpl_ = msg.factImpl_;
                aps_ = msg.aps_;
                remember_ = msg.remember_;
                boundary_ = msg.boundary_;
		
		                
		// added doc_ != 0
                if ((parser_ == 0) && (doc_ != 0)) doc_->release();
                else if (parser_ != 0) 
		{
			delete parser_;
		}
		
                doc_ = 0;
                parser_ = 0;
                
                doc_ = msg.doc_;
                parser_ = msg.parser_;
                envelope_ = msg.envelope_;
		referenceCounterPtr_ = msg.referenceCounterPtr_;
		(*referenceCounterPtr_)++;
        }
        return *this;
}


void xoap::SOAPMessage::parseFrom(const char* buf, size_t size) throw (xoap::exception::Exception)
{
	// Instantiate the DOM parser.
	/*
    	static const XMLCh gLS[] = { chLatin_L, chLatin_S, chNull };
    	DOMImplementation *impl = DOMImplementationRegistry::getDOMImplementation(gLS);
	if (impl == 0)
	{
		XCEPT_RAISE (xoap::exception::Exception, "Cannot create SOAP message from buffer: Could not retrieve suitable DOM implementation");
	}
	
	try
	{
    		parser_ = (((DOMImplementationLS*)impl)->createDOMBuilder(DOMImplementationLS::MODE_SYNCHRONOUS, 0));	
			
    		parser_->setFeature(XMLUni::fgDOMNamespaces, true);
    		parser_->setFeature(XMLUni::fgXercesSchema, true);
    		parser_->setFeature(XMLUni::fgXercesSchemaFullChecking, true);

 		AbstractDOMParser::ValSchemes valScheme = AbstractDOMParser::Val_Auto;
	
		if (valScheme == AbstractDOMParser::Val_Auto)
		{
			parser_->setFeature(XMLUni::fgDOMValidateIfSchema, true);
		}
		else if (valScheme == AbstractDOMParser::Val_Never)
		{
			parser_->setFeature(XMLUni::fgDOMValidation, false);
		}
		else if (valScheme == AbstractDOMParser::Val_Always)
		{
			parser_->setFeature(XMLUni::fgDOMValidation, true);
		}

		// enable datatype normalization - default is off
		parser_->setFeature(XMLUni::fgDOMDatatypeNormalization, true);

        	xoap::ErrorHandler ser;
        	parser_->setErrorHandler ( &ser );
		// size replaced by strelen
        	MemBufInputSource memBuf ( (const XMLByte*) buf, size, xoap::XStr("parseInfo"), false);
		Wrapper4InputSource w( &memBuf, false );

		doc_ = parser_->parse(w);
		
		//doc_ = dynamic_cast<AbstractDOMParser*>(parser_)->adoptDocument(); // release parser into user's reponsibility
		//delete parser_;
		//parser_ = 0;
		
		//doc_->normalizeDocument(); // removes any spurious characters like tabs
		
		if (ser.hasErrors())
		{
			XCEPT_RAISE(xoap::exception::Exception, ser.getErrorMessage());
		}
		
		envelope_ = doc_->getDocumentElement();

	} 
	catch (const XMLException& xmle) 
	{
		XCEPT_RAISE(xoap::exception::Exception, xoap::XMLCh2String(xmle.getMessage()));		
	} 
	catch(const SAXException& se) 
	{
		XCEPT_RAISE(xoap::exception::Exception, xoap::XMLCh2String(se.getMessage()));
	}
	catch(const DOMException& de) 
	{
		XCEPT_RAISE(xoap::exception::Exception, xoap::XMLCh2String(de.msg));
	}
	*/
	
	parser_ =  new xoap::DOMParser();
	
	try
	{
		doc_ = parser_->parse(buf, size);
		envelope_ = doc_->getDocumentElement();
	}
	catch (xoap::exception::Exception& e)
	{
		XCEPT_RETHROW (xoap::exception::Exception, "Failed to parse SOAP message from character buffer", e);
	}
}

xoap::SOAPPart xoap::SOAPMessage::getSOAPPart()
{
	return xoap::SOAPPart(envelope_);
}


/*
DOMNode * xoap::SOAPMessage::getEnvelope()
{
	#warning "xoap::SOAPMessage::getEnvelope is deprecated, use getDocument() instead"
	return envelope_;
}
*/

DOMDocument * xoap::SOAPMessage::getDocument()
{
	return doc_;
}


void xoap::SOAPMessage::writeTo (std::string& destination) throw (xoap::exception::Exception)
{       
	size_t numberAtt = this->countAttachments();

        if (numberAtt > 0)
        {
        	std::string boundaryStr = this->getMimeBoundary();

		destination = "--";
                destination += boundaryStr;
                destination += "\r\n";
                destination += "Content-Type: ";
		destination += factImpl_->getMediaType();
		destination += "\r\n\r\n";
                destination += "<?xml version='1.0' ?>";

                xoap::SOAPSerializer s(destination);
                s.serialize (envelope_);

                destination += "\r\n--";
                destination += boundaryStr;

                std::list<xoap::AttachmentPart*>::iterator iter;
                for (iter = aps_->begin(); iter != aps_->end(); iter++)
                {
			destination += "\r\n";
                        std::map<std::string, std::string, std::less<std::string> > mimeHeaders = (*iter)->getAllMimeHeaders();

                        std::string mimeBoundary = "--";
			mimeBoundary += boundaryStr + "\r\n";

                        std::map<std::string, std::string, std::less<std::string> >::iterator it;

                        for (it = mimeHeaders.begin(); it != mimeHeaders.end(); it++)
                        {
                        	if ((*it).second != "")
                                {
                                     destination += (*it).first;
                                     destination +=  ": ";
                                     destination += (*it).second;
                                     destination += "\r\n";
                                }
                        }
			destination += "\r\n";

                        char * content= (*iter)->getContent();
    	    		if (content == (char*)0) 
			{
				XCEPT_RAISE(xoap::exception::Exception, "The attachment is empty in writeTo");
   	    		}
			
                        size_t asize = (*iter)->getSize();
                        if ( content != (char*) 0 )
                        {
                        	destination.append ( content, asize );
                        }

                        destination += "\r\n--";
                        destination += boundaryStr;
                }
		destination += "--\r\n";
	} 
	else
        {
        	destination = "";
                xoap::SOAPSerializer s(destination);
                s.serialize (envelope_);
        }
}

void xoap::SOAPMessage::writeTo(std::ostream & stream) throw (xoap::exception::Exception)
{
    std::string soapstr;
    xoap::SOAPSerializer s(soapstr);
    s.serialize (envelope_);
    
    size_t size = this->countAttachments();
    if (size == 0) 
    { // no attachments
	stream << soapstr; // << ends
    } 
    else 
    {
    	std::string mimeBoundary = this->getMimeBoundary();
	std::string contentType = "Content-Type: " + factImpl_->getMediaType();
	
    	stream << "--" << mimeBoundary << "\r\n";
	stream <<  contentType << "\r\n\r\n";
	
	stream << soapstr << "\r\n";
	
	std::list<xoap::AttachmentPart*>::iterator iter;
	std::map<std::string, std::string, std::less<std::string> >::iterator it;
	
	for (iter = aps_->begin(); iter != aps_->end() ; iter++) 
        {
            stream << "--" << mimeBoundary << "\r\n";

	    std::map<std::string, std::string, std::less<std::string> > mimeHeaders = (*iter)->getAllMimeHeaders();
	    for (it = mimeHeaders.begin(); it != mimeHeaders.end(); it++) 
	    {
	        if ((*it).second != "") 
		{
	            stream << (*it).first << ": " << (*it).second << "\r\n";
		}    
	    }	
	    stream << "\r\n";

            char * content= (*iter)->getContent();
    	    if (content == (char*) 0) 
	    {
		 XCEPT_RAISE(xoap::exception::Exception, "The attachment is empty in writeTo");
   	    }
	    
	    size_t asize = (*iter)->getSize();
	    if ( content != (char*) 0 ) 
	    {
	 	for ( size_t j=0; j < asize; j++ ) 
	            stream.put(content[j]);
            }
            stream << "\r\n\r\n";
	}
	stream << "--" << mimeBoundary << "--\r\n";
    }
}


void xoap::SOAPMessage::readFrom(const char* buf, size_t size) throw (xoap::exception::Exception)
{
        if ( *referenceCounterPtr_ > 0) 
        {		
		// if message referred by another SOAPMessage than 
		// re-create a new SOAPMessage
		
                (*referenceCounterPtr_)--;
		
		referenceCounterPtr_ = new int(); 
		aps_ =  new std::list<xoap::AttachmentPart*>();
		remember_ = new std::list<xoap::AttachmentPart*>();
		*referenceCounterPtr_ = 0;
        } 
	else
	{
		//
		// First clear SOAPMessage, in case
		// it is not empty
		//
		std::list<xoap::AttachmentPart*>::iterator iter;	
		for (iter = remember_->begin(); iter != remember_->end() ; iter++) 
       		{
			 delete *iter;
		}
		aps_->clear();
		remember_->clear();
		// Delete document:
                // iff created without builder, release document node.
                // Otherwise, release parser.
		
		if (doc_ != 0)
		{
			doc_->release();
			if (parser_ != 0)
			{
				delete parser_;
			}
		}
	}
	
	doc_ = 0;
	envelope_ = 0;
	parser_ = 0;
	
	//
	// Look if there are attachments. This is the
	// case if the message is wrapped in a MIME
	// boundary (search for --MIME_Boundary).
	// If find a MIME boundary, extract the name
	//

	// skips new-line characters
	char* bufStart = const_cast<char *>(buf);
	while (*bufStart == '\r' || *bufStart == '\n') { ++bufStart; }

	char* boundary = 0;
	if ((*bufStart == '-') && (*(bufStart + 1) == '-'))
        {
                boundary = (char*) xoap::memSearch(bufStart, "--", size - (bufStart - buf), 2);
        }
	if (boundary != 0)
	{
		//
		// Find name of boundary string
		// Boundary string can be ended by "\n" or "\r\n"
		//
		char* boundaryStringEnd = (char*) xoap::memSearch(boundary, "\r\n", size - (boundary - buf), 2);
		if (boundaryStringEnd == 0)
		{
			boundaryStringEnd = (char*) xoap::memSearch(boundary, "\n", size - (boundary - buf), 1);
		}
		int boundaryStringSize = boundaryStringEnd - boundary;
		std::string boundaryString = std::string(boundary, boundaryStringSize);
		this->setMimeBoundary(boundaryString.substr(2));

		//
		// Find start of SOAPPart (search for <)
		// and end (search for next boundaryString)
		//
		char* soapStart = (char*) xoap::memSearch(boundary + boundaryStringSize, "\r\n\r\n", size - (boundary + boundaryStringSize - buf), 4);
		if (soapStart != 0) {
		    soapStart += 4;
		} else {
			soapStart = (char*) xoap::memSearch(boundary, "\n\n", size - (boundary - buf), 2);
			if (soapStart != 0) {
			    soapStart += 2;
			}
		}
 		char* soapEnd = 0;
		if (soapStart == 0) 
		{
			XCEPT_RAISE(xoap::exception::Exception, "Could not find SOAP message start");
		} 
		else
		{
			soapEnd = (char*) xoap::memSearch(soapStart, boundaryString.c_str(), size - (soapStart-buf), boundaryStringSize);
			// go back one character to avoid the boundaryString start
			// back search the SOAP enevelope termination (>)
			while (*(soapEnd -1) != '>' ) soapEnd--;
			
			if (soapEnd == 0)
			{
				XCEPT_RAISE(xoap::exception::Exception, "Could not find SOAP message end");
			} else
			{
				this->parseFrom(soapStart, soapEnd - soapStart);
			}
		}

		//	
		// While not last boundary, then scan attachment
		//
		std::string eofBoundaryStr = boundaryString;
		eofBoundaryStr += "--";
		while ( strncmp(soapEnd, eofBoundaryStr.c_str(), eofBoundaryStr.size()) != 0 )
		{
			//
			// Find MIME header start
			//	
			char* headerStart = (char*) xoap::memSearch(soapEnd, "Content-", size-(soapEnd-buf), 8);
			if (headerStart == 0) 
			{
				XCEPT_RAISE(xoap::exception::Exception, "Could not find any MIME headers. The attachment should at least have the MIME header 'Content-Type'");
			}
			else
			{
				//
				// Find MIME header end and attachment start
				//
				char* attachmentStart;
				char * headerEnd = (char*) xoap::memSearch(headerStart, "\r\n\r\n", size-(headerStart-buf), 4);
				if (headerEnd == 0)
				{
					headerEnd = (char*) xoap::memSearch(headerStart, "\n\n", size-(headerStart-buf), 2);
					attachmentStart = headerEnd + 2;
				}
				else
				{
					attachmentStart = headerEnd + 4;
				}
				
				
				if (headerEnd == 0)
				{
					XCEPT_RAISE(xoap::exception::Exception, "Could not find attachment start");
				}
				else
				{
					//
					// Copy headers to own string
					//
					size_t headerLength = headerEnd - headerStart;
					std::string headers(headerStart, headerLength);
				
					//
					// Find end of attachment
					//
					char* attachmentEnd =
				              (char*) xoap::memSearch(attachmentStart, boundaryString.c_str(), 
                                                                size - (attachmentStart-buf), boundaryStringSize);
					soapEnd = attachmentEnd;

					//
					// Find the size of the attachment
					//
					size_t attachmentSize = 0;
					if (((char*) xoap::memSearch(soapEnd-2, "\r\n", size-((soapEnd-2)-buf), 2)) != 0)
					{
						attachmentSize = soapEnd - attachmentStart - 2;
					} 
					else
					{
						XCEPT_RAISE (xoap::exception::Exception, "Could not find attachment end");
					}

					//
					// Create attachment
					//
					AttachmentPart * attachment = this->createAttachmentPart();
					attachment->setContent(attachmentStart, attachmentSize,"");
					this->addAttachmentPart(attachment);

					//
					// Set MIME headers
					//
					size_t pos3 = 0;
					while (pos3 < headers.size())
					{
						size_t pos1 = headers.find("Content-");
						size_t pos2 = headers.find(":");
						
						if (headers[pos2+1] != ' ')
						{
							XCEPT_RAISE(xoap::exception::Exception, "Missing blank after ':' in MIME header");
						}
						
						pos3 = headers.find ("\r\n");
						size_t skip = 0;
						if (pos3 == std::string::npos)
						{
							// Try to find end of line coded by '\n'. No check required, cause end of MIME
							// headers with \r\n or \n has already been checked above.
							pos3 = headers.find("\n");
							skip = 1;
						} 
						else 
						{
							skip = 2;
						}
						
						std::string name = headers.substr(pos1, pos2);
                                        	std::string value = headers.substr(pos2+2, pos3-(pos2+2));
                                        	attachment->addMimeHeader(name, value);						
						headers = headers.substr(pos3+skip);
					}
				}
			}
			
		}
	} else
	{ 
		this->parseFrom(buf, size);
	}
}

xoap::AttachmentPart * xoap::SOAPMessage::createAttachmentPart() 
{
    remember_->push_back( new xoap::AttachmentPart());
    return remember_->back();
}

xoap::AttachmentPart * xoap::SOAPMessage::createAttachmentPart(const std::string& file) 
	throw (xoap::exception::Exception)
{
	xoap::AttachmentPart* part = new xoap::AttachmentPart();
	try
	{
		part->setContent(file);
	} 
	catch (xoap::exception::Exception& e)
	{
		delete part;
		XCEPT_RETHROW (xoap::exception::Exception, "Cannot add attachment", e);
	}
	
    	remember_->push_back(part);
    	return remember_->back();
}

xoap::AttachmentPart * xoap::SOAPMessage::createAttachmentPart(const char* content, size_t len, const std::string& type)
	throw (xoap::exception::Exception)
{
    remember_->push_back(new xoap::AttachmentPart());
    remember_->back()->setContent(content, len, type);
    
    return remember_->back();
}

void xoap::SOAPMessage::addAttachmentPart(xoap::AttachmentPart * ap) 
	throw (xoap::exception::Exception) 
{ 
    // check if attachment belong to this SOAPMessage
    std::list<xoap::AttachmentPart*>::iterator iter;
    for (iter = remember_->begin(); iter != remember_->end(); iter++)
    {
    	if ((*iter) == ap ) break; // found attachment ok
    }
    if ( iter == remember_->end() )
    {
    	XCEPT_RAISE(xoap::exception::Exception,"wrong attachment");
    }
    std::string error;
    char* content = ap->getContent();
    if (content == (char*) 0) 
    {
	    XCEPT_RAISE(xoap::exception::Exception,"Trying to add an empty attachment");
    }
    aps_->push_back(ap);
}

size_t xoap::SOAPMessage::countAttachments() 
{
    return aps_->size();
}

std::list<xoap::AttachmentPart*> xoap::SOAPMessage::getAttachments() 
{
    return *aps_;
}

void xoap::SOAPMessage::setMimeBoundary(const std::string& boundary) 
	throw (xoap::exception::Exception)
{
    boundary_ = boundary;
}

std::string xoap::SOAPMessage::getMimeBoundary() 
{
    return boundary_;
}

void xoap::SOAPMessage::removeAllAttachments() 
{
    aps_->erase(aps_->begin(), aps_->end());
}

void xoap::SOAPMessage::removeAttachment(xoap::AttachmentPart * ap) 
	throw (xoap::exception::Exception)
{	
    std::list<xoap::AttachmentPart*>::iterator iter;
    iter = std::find(aps_->begin(), aps_->end(), ap);
    if (iter != aps_->end()) 
    {
    	aps_->erase(iter);
    }		
    else
    {
	XCEPT_RAISE (xoap::exception::Exception, "Could not erase attachment: not found");    	
    }
}


xoap::MimeHeaders* xoap::SOAPMessage::getMimeHeaders()
{
	return &headers_;
}

// DUMPING TREE

/*
void pippo (DOMNode* node, string& str) 
{	
	if (node->getNodeType() == DOMNode::ELEMENT_NODE) 
	{
		str += "<";
		string tmp = XMLCh2String(node->getNodeName());
		str += tmp;

		DOMNamedNodeMap* attributes = node->getAttributes(); 
		for (unsigned int i = 0; i < attributes->getLength(); i++) 
		{
			str += " ";
			string aName = XMLCh2String ( (attributes->item(i))->getNodeName() );
			string aValue =XMLCh2String ( (attributes->item(i))->getNodeValue() );

			str += aName;
			str += "=\"";
			str += aValue;
			str += "\"";
		}

		if (node->hasChildNodes() )
		{
			str += ">";

			DOMNodeList* l = node->getChildNodes();
			for (unsigned int j = 0; j < l->getLength(); j++)
			{	
				if (l->item(j)->getNodeType() == DOMNode::TEXT_NODE) 
				{
					string aValue = XMLCh2String ( l->item(j)->getNodeValue() ); 
					str += aValue;
				} else {
					str += "\n";
					pippo(l->item(j), str);
				}
			}
			str += "</";
			str += tmp;
			str += ">";
		}
		else {
			str += "/>";
			return;
		}
	}
}	
*/
// DUMPING TREE

