// $Id: SOAPNode.cc,v 1.7 2008/07/18 15:28:42 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include <string>
#include "xoap/domutils.h"
#include "xoap/SOAPNode.h"
#include "xoap/SOAPElement.h"

xoap::SOAPNode::SOAPNode (DOMNode* node)
{
	node_ = node;
}
		
std::string xoap::SOAPNode::getValue()
{
	if (node_->hasChildNodes())
	{
		DOMNode* c = node_->getFirstChild();
		if (c->getNodeType() == DOMNode::TEXT_NODE)
		{
			return xoap::XMLCh2String (c->getNodeValue());
		} 	
	}
	
	return "";
}

/*
void SOAPNode::setParentElement (SOAPElement& parent)
{
	// NOT YET IMPLEMENTED
}
*/

xoap::SOAPElement xoap::SOAPNode::getParentElement()
{
	return xoap::SOAPElement (node_->getParentNode());
}

DOMNode* xoap::SOAPNode::getDOMNode()
{
	return node_;
}

void xoap::SOAPNode::detachNode()
{
	DOMNode* father = node_->getParentNode();
	father->removeChild (node_);
}
