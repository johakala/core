// $Id: domutils.cc,v 1.5 2008/07/18 15:28:42 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xoap/domutils.h"
#include <memory>

/*
DOMElement* xoap::appendNode ( DOMNode* target, const std::string& tag, const std::string& value)
	throw (xoap::exception::Exception)
{
	try
	{
		DOMDocument* doc = target->getOwnerDocument();
		DOMElement* nameTag = doc->createElement ( xoap::XStr(tag) );
		target->appendChild ( nameTag );
		if ( value != "" )
		{
			DOMText* text = doc->createTextNode ( xoap::XStr(value) );
			nameTag->appendChild (text);
		}
		return nameTag;
	} catch (DOMException& de)
	{
		// de.getMessage in Xerces 2.6
		XCEPT_RAISE(xoap::exception::Exception, de.msg);
	}
}
*/

std::string xoap::getNodeAttribute(DOMNode* node, const std::string& name)
{
	DOMNamedNodeMap* attributes = node->getAttributes();
	for (unsigned int i = 0; i < attributes->getLength(); i++ ) 
	{
		std::string valueStr  = xoap::XMLCh2String( attributes->item(i)->getNodeValue() );
		std::string nameStr  = xoap::XMLCh2String( attributes->item(i)->getNodeName() );
		
		if ( nameStr == name ) 
		{
			return valueStr;
		}	
	}
	return "";
}



void xoap::dumpTree(DOMNode* node, std::string& str) 
{	
	if (node->getNodeType() == DOMNode::ELEMENT_NODE) 
	{
		str += "<";
		std::string tmp = xoap::XMLCh2String(node->getNodeName());
		str += tmp;

		DOMNamedNodeMap* attributes = node->getAttributes(); 
		for (unsigned int i = 0; i < attributes->getLength(); i++) 
		{
			str += " ";
			std::string aName  = xoap::XMLCh2String ( (attributes->item(i))->getNodeName() );
			std::string aValue = xoap::XMLCh2String ( (attributes->item(i))->getNodeValue() );

			str += aName;
			str += "=\"";
			str += aValue;
			str += "\"";
		}

		if (node->hasChildNodes() )
		{
			str += ">";

			DOMNodeList* l = node->getChildNodes();
			for (unsigned int j = 0; j < l->getLength(); j++)
			{	
				if (l->item(j)->getNodeType() == DOMNode::TEXT_NODE) 
				{
					std::string aValue = xoap::XMLCh2String ( l->item(j)->getNodeValue() ); 
					str += aValue;
				} else 
				{
					str += "\n";
					dumpTree(l->item(j), str);
				}
			}
			str += "</";
			str += tmp;
			str += ">";
		}
		else 
		{
			str += "/>";
			return;
		}
	}
}	


std::string xoap::XMLCh2String (const XMLCh* ch)
{
	if (ch == 0) return "";	
	std::auto_ptr<char> v(XMLString::transcode (ch));
	return std::string(v.get());
}


xoap::XStr::XStr(const char* const toTranscode)
{
	// Call the private transcoding method
	fUnicodeForm = XMLString::transcode(toTranscode);
}

xoap::XStr::XStr(std::string toTranscode)
{
	// Call the private transcoding method
	fUnicodeForm = XMLString::transcode(toTranscode.c_str());
}

xoap::XStr::~XStr()
{
	XMLString::release(&fUnicodeForm);
}

const XMLCh* xoap::XStr::unicodeForm() const
{
	return fUnicodeForm;
}

xoap::XStr::operator const XMLCh*()
{
	return this->unicodeForm();
}

