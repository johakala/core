// $Id: HTTPLoader.cc,v 1.11 2008/07/18 15:28:42 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xoap/HTTPLoader.h"
#include "xoap/domutils.h"
#include "xcept/tools.h"

xoap::HTTPLoader::HTTPLoader (bool xmlInitialize) throw (xoap::exception::Exception)
{
	// Initialize with a buffer size of 16 KB
	this->init (xmlInitialize, 16384);
}

xoap::HTTPLoader::HTTPLoader (bool xmlInitialize, int size) throw (xoap::exception::Exception)
{
	init (xmlInitialize, size);
}

void xoap::HTTPLoader::init (bool xmlInitialize, int size) throw (xoap::exception::Exception)
{
        bufferSize = size;
        buffer = new XMLByte[bufferSize];

	try
	{
        	if (xmlInitialize)
                	XMLPlatformUtils::Initialize();
	} 
	catch (...)
	{
		delete buffer;
		XCEPT_RAISE (xoap::exception::Exception, "Could not initialize Xerces XML system");
	}

        //accessor = new SocketNetAccessor();
}

xoap::HTTPLoader::~HTTPLoader()
{
        delete buffer;
        //delete accessor;
}


int xoap::HTTPLoader::get (const std::string& url, std::string& fileBuf)
	throw (xoap::exception::Exception)
{

        try 
	{
                XMLURL xmlUrl (url.c_str());
                //BinInputStream* s = accessor->makeNew (xmlUrl); 
		BinInputStream* s = xmlUrl.makeNewStream();
		
		if (s == 0)
		{
			std::string msg = "Failed to access ";
			msg += url;
			XCEPT_RAISE (xoap::exception::Exception, msg);
		}
		
                fileBuf = "";

                int r = 0;
                int curPos = 0;

                while ( (r = s->readBytes (buffer, bufferSize)) > 0)
                {
                        int read = s->curPos() - curPos;
                        curPos = s->curPos();
                        fileBuf.append((const char*) buffer,read);
                }

                delete s;
                return curPos;
        } 
	catch (NetAccessorException& nae)
        {
		XCEPT_RAISE (xoap::exception::Exception, xoap::XMLCh2String (nae.getMessage()));
        } 
	catch (MalformedURLException& mue)
	{
		XCEPT_RAISE (xoap::exception::Exception, xoap::XMLCh2String (mue.getMessage()));
	} 
	catch (std::exception& se)
	{
		XCEPT_RAISE (xoap::exception::Exception, se.what());
	} 
	catch (...)
	{
		std::string msg = "Caught unknown exception during 'get' operation on url ";
		msg += url;
		XCEPT_RAISE (xoap::exception::Exception, msg);
	}
}


int xoap::HTTPLoader::store (const std::string& url, const std::string& path)
	throw (xoap::exception::Exception)
{
        try 
	{
                XMLURL xmlUrl (url.c_str());
                //BinInputStream* s = accessor->makeNew (xmlUrl);
                //UnixHTTPURLInputStream * s = new UnixHTTPURLInputStream(xmlUrl);
		BinInputStream* s = xmlUrl.makeNewStream();
		
		if (s == 0)
		{
			std::string msg = "Failed to access ";
			msg += url;
			XCEPT_RAISE (xoap::exception::Exception, msg);
		}
		
                int r = 0;
                int curPos = 0;
                int slash = url.rfind ("/");

		std::string saveFile = path;
                saveFile += url.substr (slash, url.length() - slash);
                std::ofstream to (saveFile.c_str());
                if (!to) 
		{
			std::string msg = "Cannot open file ";
			msg += path;
			msg += " to store URL content ";
			msg += url;
			XCEPT_RAISE (xoap::exception::Exception, msg);
		}

                while ( (r = s->readBytes (buffer, bufferSize)) > 0)
                {
                        int read = s->curPos() - curPos;
                        curPos = s->curPos();

                        to.write ( (const char*) buffer, read );
                        if (to.bad())
                        {
                                to.close();
                                std::string msg = "Error writing to file ";
				msg += path;
				msg += " while storing URL content ";
				msg += url;
				XCEPT_RAISE (xoap::exception::Exception, msg);
                        }
                }

                to.close();
                delete s;
                return curPos;
        } 
	catch (NetAccessorException nae)
        {
		XCEPT_RAISE (xoap::exception::Exception, XMLCh2String (nae.getMessage()));
        }
	catch (MalformedURLException mue)
	{
		XCEPT_RAISE (xoap::exception::Exception, XMLCh2String (mue.getMessage()));
	}
	catch (std::exception& se)
	{
		XCEPT_RAISE (xoap::exception::Exception, se.what());
	} 
	catch (...)
	{
		std::string msg = "Caught unknown exception during 'store' operation of url ";
		msg += url;
		msg += " to path ";
		msg += path;
		XCEPT_RAISE (xoap::exception::Exception, msg);
	}
}

