#include "xoap/ErrorHandler.h"

#include <xercesc/dom/DOM.hpp>
#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/sax/SAXException.hpp>
#include <xercesc/sax/SAXParseException.hpp>
#include <xercesc/parsers/XercesDOMParser.hpp>
#include <xercesc/dom/DOMException.hpp>
#include <xercesc/dom/DOMNamedNodeMap.hpp>
#include <xercesc/framework/MemBufInputSource.hpp>
#include <xercesc/framework/Wrapper4InputSource.hpp>
#include <xercesc/dom/DOMError.hpp>
#include <xercesc/dom/DOMErrorHandler.hpp>

#include <iomanip>
#include <algorithm>
#include <cstdlib>
#include <map>
#include <string>
#include <stdio.h>
#include <memory>

#include "xoap/memSearch.h"
#include "xoap/SOAPMessage.h"
#include "xoap/exception/Exception.h"
#include "xoap/domutils.h"


XERCES_CPP_NAMESPACE_USE

xoap::ErrorHandler::ErrorHandler()
{
	this->reset();
}


bool xoap::ErrorHandler::handleError (const DOMError &domError)
{
	std::stringstream errorMessage;

	if (domError.getSeverity() == DOMError::DOM_SEVERITY_WARNING)
	{
		errorMessage << "Warning at file ";
	}
	else if (domError.getSeverity() == DOMError::DOM_SEVERITY_ERROR)
	{
		errorMessage << "Error at file ";
	}
	else
	{
		errorMessage << "Error at file ";
	}

	errorMessage << xoap::XMLCh2String(domError.getLocation()->getURI())
		<< ", line " << domError.getLocation()->getLineNumber()
		<< ", char " << domError.getLocation()->getColumnNumber()
		<< "\nMessage: " << xoap::XMLCh2String(domError.getMessage()) << std::endl;

	msg_ += errorMessage.str();
	hasErrors_ = true;
	return true;
}

bool xoap::ErrorHandler::hasErrors()
{
	return hasErrors_;
}

void xoap::ErrorHandler::reset()
{
	msg_ = "";
	hasErrors_ = false;
}

std::string xoap::ErrorHandler::getErrorMessage()
{
	return msg_;
}
