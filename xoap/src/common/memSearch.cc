// $Id: memSearch.cc,v 1.4 2008/07/18 15:28:42 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xoap/memSearch.h"

void * xoap::memSearch(const void *buf, const void *pattern, size_t buflen, size_t len)
{
              size_t i, j;
               char *bf = (char *)buf, *pt = (char *)pattern;

               if (len > buflen)
                     return (void *)NULL;

               for (i = 0; i <= (buflen - len); ++i)
               {
                     for (j = 0; j < len; ++j)
                     {
                           if (pt[j] != bf[i + j])
                                 break;
                     }
                     if (j == len)
                           return (bf + i);
               }
               return NULL;
}
