// $Id: MessageFactory.cc,v 1.10 2008/07/18 15:28:42 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xoap/MessageFactory.h"
#include "xoap/MessageReference.h"
#include "xoap/ver1_1/MessageFactoryImpl.h"
#include "xoap/ver1_2/MessageFactoryImpl.h"
#include "xoap/SOAPConstants.h"
#include "xoap/Lock.h"

xoap::MessageReference xoap::createMessage() throw (xoap::exception::Exception)
{
	return xoap::MessageReference(xoap::MessageFactory::getInstance(xoap::SOAPConstants::DEFAULT_SOAP_PROTOCOL)->createMessage());
}

xoap::MessageReference xoap::createMessage(char* buf, int size) throw (xoap::exception::Exception)
{
	return xoap::MessageReference(xoap::MessageFactory::getInstance(xoap::SOAPConstants::DEFAULT_SOAP_PROTOCOL)->createMessage(buf, size));
}

xoap::MessageReference xoap::createMessage(DOMNode* node) throw (xoap::exception::Exception)
{
	return xoap::MessageReference(xoap::MessageFactory::getInstance(xoap::SOAPConstants::DEFAULT_SOAP_PROTOCOL)->createMessage(node));
}

xoap::MessageReference xoap::createMessage(xoap::MessageReference msg) throw (xoap::exception::Exception)
{

        return xoap::MessageReference(xoap::MessageFactory::getInstance(msg->getImplementationFactory()->getProtocolVersion())->createMessage(msg));
}

xoap::MessageReference xoap::createMessage(const std::string& filename, const std::string& protocol) throw (xoap::exception::Exception)
{
        return xoap::MessageReference(xoap::MessageFactory::getInstance(protocol)->createMessage(filename));

}

	
xoap::MessageFactory* xoap::MessageFactory::getInstance(const std::string & protocol)
{
	if ( protocol ==  xoap::SOAPConstants::SOAP_1_1_PROTOCOL )
	{
         	return xoap::ver1_1::MessageFactoryImpl::getInstance();
	}
	else if ( protocol ==  xoap::SOAPConstants::SOAP_1_2_PROTOCOL )
	{
         	return xoap::ver1_2::MessageFactoryImpl::getInstance();
	}
	else
	{
		XCEPT_RAISE (xoap::exception::Exception, "invalid SOAP protocol " + protocol);
	}

}


void xoap::MessageFactory::destroyInstance(const std::string & protocol)
{
	if ( protocol ==  xoap::SOAPConstants::SOAP_1_1_PROTOCOL )
	{
         	return xoap::ver1_1::MessageFactoryImpl::destroyInstance();
	}
	else if ( protocol ==  xoap::SOAPConstants::SOAP_1_2_PROTOCOL )
	{
         	return xoap::ver1_2::MessageFactoryImpl::destroyInstance();
	}
	else
	{
		XCEPT_RAISE (xoap::exception::Exception, "invalid SOAP protocol " + protocol);
	}
}

