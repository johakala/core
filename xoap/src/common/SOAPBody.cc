// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2013, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini and A. Petrucci					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xoap/domutils.h"
#include "xoap/SOAPBody.h"

xoap::SOAPBody::SOAPBody ( DOMNode* node ) : xoap::SOAPElement ( node )
{
}

xoap::SOAPBodyElement xoap::SOAPBody::addBodyElement ( xoap::SOAPName & name )
	throw (xoap::exception::Exception)
{
	// Append a Child to the <Body> that is 
	// the function name
	xoap::SOAPElement e = this->addChildElement ( name );	
	return xoap::SOAPBodyElement ( e.getDOMNode() );
}

bool xoap::SOAPBody::hasFault()
{
	DOMDocument* doc = node_->getOwnerDocument();	
	DOMNodeList* l = doc->getElementsByTagNameNS ( xoap::XStr("*"), xoap::XStr("Fault"));
	
	if (l->getLength() == 1)
	{
		return true;
	} else {
		return false;
	}
}

xoap::SOAPFault xoap::SOAPBody::getFault() throw (xoap::exception::Exception)
{
        DOMDocument* doc = node_->getOwnerDocument();
        DOMNodeList* l = doc->getElementsByTagNameNS ( XStr("*"), XStr("Fault"));

        if (l->getLength() == 1)
        {
                return xoap::SOAPFault ( l->item(0) );
        }

        XCEPT_RAISE(xoap::exception::Exception, "SOAPBody object does not contain exactly one SOAPFault object");

}

xoap::SOAPFault xoap::SOAPBody::addFault () throw (xoap::exception::Exception)
{
	DOMDocument* doc = node_->getOwnerDocument();
	const XMLCh* prefix = doc->getDocumentElement()->getPrefix();
	const XMLCh* uri = doc->getDocumentElement()->getNamespaceURI();

	if (prefix == 0)
	{
		//cout << "Adding <Fault> without namespace prefix!" << endl;
		
		xoap::SOAPName faultName ( "Fault", "", "");
	
		xoap::SOAPElement e = this->addChildElement ( faultName );
		return xoap::SOAPFault ( e.getDOMNode() );
	} else 
	{
		xoap::SOAPName faultName ( "Fault", xoap::XMLCh2String(prefix), xoap::XMLCh2String(uri));
	
		xoap::SOAPElement e = this->addChildElement ( faultName );
		return xoap::SOAPFault ( e.getDOMNode() );
	}
	
	
}
