// $Id: SOAPFault.cc,v 1.12 2008/07/18 15:28:42 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xoap/SOAPFault.h"
#include "xoap/SOAPConstants.h"
#include "xoap/domutils.h"
#include "toolbox/string.h"
#include <memory>

xoap::SOAPFault::SOAPFault ( DOMNode* node ) : xoap::SOAPBodyElement ( node )
{

}

std::string xoap::SOAPFault::getFaultCode()
{
	DOMNodeList* l = node_->getChildNodes();
	for (unsigned int i = 0; i < l->getLength(); i++)
	{
		DOMNode* c = l->item(i);
		std::string faultName = xoap::XMLCh2String(c->getPrefix());
		
		if (faultName != "" )
		{
			faultName += ":faultcode";
		} else {
			faultName = "faultcode";
		}
		
		if (xoap::XMLCh2String(c->getNodeName()) == faultName)
		{
			xoap::SOAPNode n(c);
			return n.getValue();
		} 
	}
	
	return "";
}

std::string xoap::SOAPFault::getFaultString()
{
	DOMNodeList* l = node_->getChildNodes();
	for (unsigned int i = 0; i < l->getLength(); i++)
	{
		DOMNode* c = l->item(i);
		std::string faultName = xoap::XMLCh2String(c->getPrefix());
		
		if (faultName != "" )
		{
			faultName += ":faultstring";
		} else {
			faultName = "faultstring";
		}
		
		if (xoap::XMLCh2String(c->getNodeName()) == faultName)
		{
			xoap::SOAPNode n(c);
			return n.getValue();
		} 
	}
	
	return "";
}

void xoap::SOAPFault::setFaultCode(const std::string& code) throw (xoap::exception::Exception)
{
	DOMDocument* doc = node_->getOwnerDocument();
        const XMLCh* nsuri = doc->getDocumentElement()->getNamespaceURI();
        if ( xoap::XMLCh2String(nsuri) == xoap::SOAPConstants::URI_NS_SOAP_1_1_ENVELOPE)
        {
               xoap::SOAPName faultcodeName ( "faultcode", "", "");
               xoap::SOAPElement fc = this->addChildElement ( faultcodeName );
	       fc.setTextContent (code);
	}
	else
	{
		 XCEPT_RAISE(xoap::exception::Exception, "Not supported in SOAP 1.2");
	}

}

void xoap::SOAPFault::setFaultString(const std::string& str) throw (xoap::exception::Exception)
{
	DOMDocument* doc = node_->getOwnerDocument();
        const XMLCh* nsuri = doc->getDocumentElement()->getNamespaceURI();
        if ( xoap::XMLCh2String(nsuri) == xoap::SOAPConstants::URI_NS_SOAP_1_1_ENVELOPE)
        {
               xoap::SOAPName faultstringName ( "faultstring", "", "");
               xoap::SOAPElement fs = this->addChildElement ( faultstringName );
               fs.setTextContent (str);
        }
        else
        {
                 XCEPT_RAISE(xoap::exception::Exception, "Not supported in SOAP 1.2");
        }
}

xoap::SOAPElement xoap::SOAPFault::addDetail() throw (xoap::exception::Exception)
{

	if ( !this->hasDetail() )
	{
		DOMDocument* doc = node_->getOwnerDocument();
		const XMLCh* prefix = doc->getDocumentElement()->getPrefix();
        	const XMLCh* nsuri = doc->getDocumentElement()->getNamespaceURI();
		if ( xoap::XMLCh2String(nsuri) == xoap::SOAPConstants::URI_NS_SOAP_1_1_ENVELOPE)
		{
		
			xoap::SOAPName detailName ( "detail", "", "");
			return this->addChildElement ( detailName );

			/*
			if (prefix == 0)
			{		
				xoap::SOAPName detailName ( "detail", "", "");
			
				return this->addChildElement ( detailName );
			} else 
			{
				xoap::SOAPName detailName ( "detail", xoap::XMLCh2String(prefix), xoap::XMLCh2String(nsuri) );
			
				return this->addChildElement ( detailName );
			}
			*/
		}
		else
		{
		 	xoap::SOAPName detailName ( "Detail", xoap::XMLCh2String(prefix), xoap::XMLCh2String(nsuri));
	
                 	return this->addChildElement ( detailName );
	
		}
	}
	else
	{
		XCEPT_RAISE(xoap::exception::Exception,"'detail' element already present");	
	}
	
}
xoap::SOAPElement xoap::SOAPFault::getDetail()
	throw (xoap::exception::Exception)
{
	DOMNodeList* l = node_->getChildNodes();
	for (unsigned int i = 0; i < l->getLength(); i++)
	{
		DOMNode* c = l->item(i);
		std::string name = toolbox::tolower(xoap::XMLCh2String(c->getLocalName()));
		
		if (( name == "detail") || ( name == "Detail"))
		{
			return xoap::SOAPElement (c);
		} 
	}
	
	XCEPT_RAISE(xoap::exception::Exception,"missing 'detail' element");

}		

bool xoap::SOAPFault::hasDetail()
	throw (xoap::exception::Exception)
{
	DOMNodeList* l = node_->getChildNodes();
	for (unsigned int i = 0; i < l->getLength(); i++)
	{
		DOMNode* c = l->item(i);
		std::string name = toolbox::tolower(xoap::XMLCh2String(c->getLocalName()));
		
		if (( name == "detail"  ) || ( name == "Detail"))
		{
			return true;
		} 
	}
	
	return false;
}		

void xoap::SOAPFault::addFaultReasonText(std::string text, std::locale locale) throw (xoap::exception::Exception)
{
	DOMDocument* doc = node_->getOwnerDocument();
        const XMLCh* prefix = doc->getDocumentElement()->getPrefix();
        const XMLCh* nsuri = doc->getDocumentElement()->getNamespaceURI();
        if ( xoap::XMLCh2String(nsuri) == xoap::SOAPConstants::URI_NS_SOAP_1_1_ENVELOPE)
        {
                XCEPT_RAISE(xoap::exception::Exception, "Not supported in SOAP 1.1");
        }

        xoap::SOAPName codeName ( "Reason", xoap::XMLCh2String(prefix), xoap::XMLCh2String(nsuri));
        xoap::SOAPElement code = addChildElement (codeName );
        xoap::SOAPName txtName ( "Text", xoap::XMLCh2String(prefix), xoap::XMLCh2String(nsuri) );
        xoap::SOAPElement txt = code.addChildElement (txtName );
	xoap::SOAPName lang ( "lang", "xml", "http://www.w3.org/XML/1998/namespace" );
	txt.addAttribute ( lang, locale.name().c_str() );
        txt.setTextContent (text);

}

void xoap::SOAPFault::appendFaultSubcode(SOAPName subcode) throw (xoap::exception::Exception)
{
	DOMDocument* doc = node_->getOwnerDocument();
        const XMLCh* nsuri = doc->getDocumentElement()->getNamespaceURI();
        if ( xoap::XMLCh2String(nsuri) == xoap::SOAPConstants::URI_NS_SOAP_1_1_ENVELOPE)
        {
                XCEPT_RAISE(xoap::exception::Exception, "Not supported in SOAP 1.1");
        }
        XCEPT_RAISE(xoap::exception::Exception, "Not implemented");

}

std::string xoap::SOAPFault::getFaultActor() throw (xoap::exception::Exception)
{
	// getFaultRoleName(); as saaj
        XCEPT_RAISE(xoap::exception::Exception, "Not implemented");
	return "";
}

xoap::SOAPName xoap::SOAPFault::getFaultCodeAsName() throw (xoap::exception::Exception)
{
        XCEPT_RAISE(xoap::exception::Exception, "Not implemented");
	xoap::SOAPName detailName ( "dummy", "", "");
	return detailName;
}

std::string xoap::SOAPFault::getFaultNode() throw (xoap::exception::Exception)
{
	
	DOMDocument* doc = node_->getOwnerDocument();
        const XMLCh* nsuri = doc->getDocumentElement()->getNamespaceURI();
        if ( xoap::XMLCh2String(nsuri) == xoap::SOAPConstants::URI_NS_SOAP_1_1_ENVELOPE)
        {
                XCEPT_RAISE(xoap::exception::Exception, "Not supported in SOAP 1.1");
        }
        XCEPT_RAISE(xoap::exception::Exception, "Not implemented");
	return "";
}
	
std::vector<std::locale> xoap::SOAPFault::getFaultReasonLocales() throw (xoap::exception::Exception)
{
	DOMDocument* doc = node_->getOwnerDocument();
        const XMLCh* nsuri = doc->getDocumentElement()->getNamespaceURI();
        if ( xoap::XMLCh2String(nsuri) == xoap::SOAPConstants::URI_NS_SOAP_1_1_ENVELOPE)
        {
                XCEPT_RAISE(xoap::exception::Exception, "Not supported in SOAP 1.1");
        }

	std::vector<std::locale> v;
	std::locale locale;
	v.push_back(locale);
        XCEPT_RAISE(xoap::exception::Exception, "Not implemented");
	return v;
}

std::string xoap::SOAPFault::getFaultReasonText(std::locale locale) throw (xoap::exception::Exception)
{
	DOMDocument* doc = node_->getOwnerDocument();
        const XMLCh* nsuri = doc->getDocumentElement()->getNamespaceURI();
        if ( xoap::XMLCh2String(nsuri) == xoap::SOAPConstants::URI_NS_SOAP_1_1_ENVELOPE)
        {
                XCEPT_RAISE(xoap::exception::Exception, "Not supported in SOAP 1.1");
        }
        XCEPT_RAISE(xoap::exception::Exception, "Not implemented");
	return "";
}

std::vector<std::string> xoap::SOAPFault::getFaultReasonTexts() throw (xoap::exception::Exception)
{
	DOMDocument* doc = node_->getOwnerDocument();
        const XMLCh* nsuri = doc->getDocumentElement()->getNamespaceURI();
        if ( xoap::XMLCh2String(nsuri) == xoap::SOAPConstants::URI_NS_SOAP_1_1_ENVELOPE)
        {
                XCEPT_RAISE(xoap::exception::Exception, "Not supported in SOAP 1.1");
        }
	std::vector<std::string> v;
        XCEPT_RAISE(xoap::exception::Exception, "Not implemented");
	return v;
}

std::string xoap::SOAPFault::getFaultRole() throw (xoap::exception::Exception)
{
	DOMDocument* doc = node_->getOwnerDocument();
        const XMLCh* nsuri = doc->getDocumentElement()->getNamespaceURI();
	if ( xoap::XMLCh2String(nsuri) == xoap::SOAPConstants::URI_NS_SOAP_1_1_ENVELOPE)
        {
                XCEPT_RAISE(xoap::exception::Exception, "Not supported in SOAP 1.1");
        }
	return "";
}

std::locale	xoap::SOAPFault::getFaultStringLocale() throw (xoap::exception::Exception)
{
        XCEPT_RAISE(xoap::exception::Exception, "Not implemented");
	std::locale l;
	return l; 
}

std::vector<xoap::SOAPName> xoap::SOAPFault::getFaultSubcodes() throw (xoap::exception::Exception)
{
	DOMDocument* doc = node_->getOwnerDocument();
        const XMLCh* nsuri = doc->getDocumentElement()->getNamespaceURI();
        if ( xoap::XMLCh2String(nsuri) == xoap::SOAPConstants::URI_NS_SOAP_1_1_ENVELOPE)
        {
                XCEPT_RAISE(xoap::exception::Exception, "Not supported in SOAP 1.1");
        }

        XCEPT_RAISE(xoap::exception::Exception, "Not implemented");
	std::vector<xoap::SOAPName> v;
        return v;
}
	
void	xoap::SOAPFault::removeAllFaultSubcodes() throw (xoap::exception::Exception)
{
	DOMDocument* doc = node_->getOwnerDocument();
        const XMLCh* nsuri = doc->getDocumentElement()->getNamespaceURI();
        if ( xoap::XMLCh2String(nsuri) == xoap::SOAPConstants::URI_NS_SOAP_1_1_ENVELOPE)
        {
                XCEPT_RAISE(xoap::exception::Exception, "Not supported in SOAP 1.1");
        }
        XCEPT_RAISE(xoap::exception::Exception, "Not implemented");

}

void	xoap::SOAPFault::setFaultActor(std::string faultActor) throw (xoap::exception::Exception)
{
	DOMDocument* doc = node_->getOwnerDocument();
        const XMLCh* nsuri = doc->getDocumentElement()->getNamespaceURI();
        if ( xoap::XMLCh2String(nsuri) == xoap::SOAPConstants::URI_NS_SOAP_1_1_ENVELOPE)
        {
		 this->appendNodeWithPrefix ("faultactor", faultActor);
        }
        else
        {
		// If this SOAPFault supports SOAP 1.2 then this call is equivalent to setFaultRole(String)
                this->setFaultRole(faultActor);
        }
}

void	xoap::SOAPFault::setFaultCode(SOAPName faultCodeQName) throw (xoap::exception::Exception)
{
        DOMDocument* doc = node_->getOwnerDocument();
        const XMLCh* prefix = doc->getDocumentElement()->getPrefix();
	const XMLCh* nsuri = doc->getDocumentElement()->getNamespaceURI();
	if ( xoap::XMLCh2String(nsuri) == xoap::SOAPConstants::URI_NS_SOAP_1_1_ENVELOPE)
        {
		this->setFaultCode(faultCodeQName.getQualifiedName());
        }
	else
	{
        	xoap::SOAPName codeName ( "Code", xoap::XMLCh2String(prefix), xoap::XMLCh2String(nsuri));
        	xoap::SOAPElement code = addChildElement (codeName );
		xoap::SOAPName valueName ( "Value", xoap::XMLCh2String(prefix), xoap::XMLCh2String(nsuri) );
        	xoap::SOAPElement value = code.addChildElement (valueName );
		value.setTextContent (faultCodeQName.getQualifiedName());
	}


}

void	xoap::SOAPFault::setFaultNode(std::string uri) throw (xoap::exception::Exception)
{

	DOMDocument* doc = node_->getOwnerDocument();
        const XMLCh* prefix = doc->getDocumentElement()->getPrefix();
        const XMLCh* nsuri = doc->getDocumentElement()->getNamespaceURI();
 	if ( xoap::XMLCh2String(nsuri) == xoap::SOAPConstants::URI_NS_SOAP_1_1_ENVELOPE)	
	{
		XCEPT_RAISE(xoap::exception::Exception, "Not supported in SOAP 1.1");
	}

        xoap::SOAPName nodeName ( "Node", xoap::XMLCh2String(prefix), xoap::XMLCh2String(nsuri));
        xoap::SOAPElement node = this->addChildElement (nodeName );
        node.setTextContent (uri);

}
	
void	xoap::SOAPFault::setFaultRole(std::string uri) throw (xoap::exception::Exception)
{
	DOMDocument* doc = node_->getOwnerDocument();
        const XMLCh* prefix = doc->getDocumentElement()->getPrefix();
        const XMLCh* nsuri = doc->getDocumentElement()->getNamespaceURI();
	if ( xoap::XMLCh2String(nsuri) == xoap::SOAPConstants::URI_NS_SOAP_1_1_ENVELOPE)
        {
                XCEPT_RAISE(xoap::exception::Exception, "Not supported in SOAP 1.1");
        }

        xoap::SOAPName roleName ( "Node", xoap::XMLCh2String(prefix), xoap::XMLCh2String(nsuri));
        xoap::SOAPElement role = this->addChildElement (roleName );
        role.setTextContent (uri);

}

void	xoap::SOAPFault::setFaultString(std::string faultString, std::locale locale) throw (xoap::exception::Exception)
{
	 DOMDocument* doc = node_->getOwnerDocument();
        const XMLCh* nsuri = doc->getDocumentElement()->getNamespaceURI();
        if ( xoap::XMLCh2String(nsuri) == xoap::SOAPConstants::URI_NS_SOAP_1_1_ENVELOPE)
        {
		this->setFaultString(faultString);

		 DOMNodeList* l = node_->getChildNodes();
        	for (unsigned int i = 0; i < l->getLength(); i++)
        	{
                	DOMElement* c = (DOMElement*)l->item(i);
                	std::string name = toolbox::tolower(xoap::XMLCh2String(c->getLocalName()));
	
                	if (( name == "faultstring") )
                	{
				xoap::SOAPElement f(c);
		   		xoap::SOAPName lang ( "lang", "xml", "http://www.w3.org/XML/1998/namespace" );
        	   		f.addAttribute ( lang, locale.name().c_str() );
                	}
        	}

        }
	else
	{
		// if SOAP 1.2 same as addFaultReasonText(faultString, locale);
		this->addFaultReasonText(faultString,locale);

	}
}

