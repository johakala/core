// $Id: MimeHeaders.cc,v 1.5 2008/07/18 15:28:42 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xoap/MimeHeaders.h"
#include "toolbox/string.h"
   
xoap::MimeHeaders::MimeHeaders() 
{
}


std::vector<std::string> xoap::MimeHeaders::getHeader(const std::string& name) 
{
	std::string pattern = toolbox::tolower(name);
    std::vector<std::string> values;

    std::multimap<std::string, std::string, std::less<std::string> >::iterator i = headers_.begin();

    while (i != headers_.end())
    {
    	    if ( (*i).first == pattern)
    	    {
    		    values.push_back( (*i).second);
    	    }
    	    i++;
    }
    
    return values;
}


void xoap::MimeHeaders::setHeader(const std::string& name, const std::string& value) throw (xoap::exception::Exception)
{
    if (name == "")
    {
	XCEPT_RAISE(xoap::exception::Exception, "Illegal MimeHeader name");
    }
    
    // first, remove all existing header
    headers_.erase(toolbox::tolower(name));

    this->addHeader(toolbox::tolower(name), value);
}


void xoap::MimeHeaders::addHeader(const std::string& name, const std::string& value) throw (xoap::exception::Exception)
{
    if (name == "")
    {
	XCEPT_RAISE(xoap::exception::Exception, "Illegal MimeHeader name");
    }

    headers_.insert (std::pair<std::string, std::string>(toolbox::tolower(name),value));       
}


void xoap::MimeHeaders::removeHeader(const std::string& name) 
{
    headers_.erase(toolbox::tolower(name));
}


void xoap::MimeHeaders::removeAllHeaders() 
{
    headers_.clear();
}


std::multimap<std::string, std::string, std::less<std::string> >& xoap::MimeHeaders::getAllHeaders() 
{
    return headers_;
}
