// $Id: SOAPName.cc,v 1.7 2008/07/18 15:28:42 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xoap/SOAPName.h"
#include "xoap/SOAPAllocator.h"

xoap::SOAPName::SOAPName(  const std::string& name, const std::string& prefix, const std::string& uri)
{
	prefix_ = prefix;
	name_ = name;
	uri_ = uri;
}

std::string xoap::SOAPName::getQualifiedName()
{	
	if (prefix_ == "" ) return name_; 
	else {
		std::string tmp = prefix_;
		tmp += ":";
		tmp += name_;
		return tmp;
	}
}

std::string xoap::SOAPName::getLocalName()
{
	return name_;
}

std::string xoap::SOAPName::getPrefix()
{
	return prefix_;
}

std::string xoap::SOAPName::getURI()
{
	return uri_;
}

//
// Preliminary comparison operator, which
// compares local names only.
//
bool xoap::SOAPName::operator==(xoap::SOAPName& name)
{
	if (name_ == name.getLocalName()) return true;
	else return false;
}
