#include "xoap/DOMParser.h"
#include "xoap/domutils.h"
#include <iostream>
#include <stdlib.h>
#include <string.h>

#include <xercesc/framework/URLInputSource.hpp>
#include <xercesc/sax/SAXException.hpp>
#include <xercesc/sax/SAXParseException.hpp>
#include <xercesc/util/OutOfMemoryException.hpp>

xoap::DOMParserErrorHandler::DOMParserErrorHandler ()
{
	this->resetErrors();
}

bool xoap::DOMParserErrorHandler::handleError (const DOMError& domError)
{
	bool continueParsing = true;
	if (errors_ == true)
	{
		msg_ << std::endl;
	}

	if (domError.getSeverity() == DOMError::DOM_SEVERITY_WARNING)
	{
		msg_ << "warning, ";
	}
	else if (domError.getSeverity() == DOMError::DOM_SEVERITY_ERROR)
	{
		msg_ << "error, ";
	}
	else
	{
		msg_ << "fatal, ";
	}

	msg_ << XMLCh2String(domError.getLocation()->getURI()) << ", line " << domError.getLocation()->getLineNumber() << ", char " << domError.getLocation()->getColumnNumber() << "\n  Message: " << XMLCh2String(domError.getMessage()) << XERCES_STD_QUALIFIER
	endl;

	errors_ = true;

	return continueParsing;
}

bool xoap::DOMParserErrorHandler::hasErrors ()
{
	return errors_;
}

std::string xoap::DOMParserErrorHandler::getError ()
{
	return msg_.str();
}

void xoap::DOMParserErrorHandler::resetErrors ()
{
	errors_ = false;
	msg_.clear();
}

xoap::DOMParser::~DOMParser ()
{
	parser->release();
}

xoap::DOMParser::DOMParser () throw (xoap::exception::Exception)
	: mutex_(toolbox::BSem::FULL)
{
	try
	{

		//see  xoap::DOMImplementationFeatures for xerces as compared to xqilla e.g. "Core" etc.
		DOMImplementation* xqillaImplementation = DOMImplementationRegistry::getDOMImplementation(XStr(xoap::DOMImplementationFeatures));
		parser = xqillaImplementation->createLSParser(DOMImplementationLS::MODE_SYNCHRONOUS, 0);

		parser->getDomConfig()->setParameter(XMLUni::fgDOMNamespaces, true);
		parser->getDomConfig()->setParameter(XMLUni::fgXercesSchema, false);
		parser->getDomConfig()->setParameter(XMLUni::fgDOMValidateIfSchema, false);
		parser->getDomConfig()->setParameter(XMLUni::fgXercesHandleMultipleImports, true);

	}
	catch (const DOMException& e)
	{
		XCEPT_RAISE(xoap::exception::Exception, xoap::XMLCh2String(e.msg));

	}
	catch (...)
	{
		XCEPT_RAISE(xoap::exception::Exception, "error detected during parser creation");
	}
}

DOMDocument* xoap::DOMParser::parse (const XMLURL& url) throw (xoap::exception::Exception)
{
	this->lock();
	try
	{
		//parser->resetDocumentPool();
		xoap::DOMParserErrorHandler errorHandler;
		parser->getDomConfig()->setParameter(XMLUni::fgDOMErrorHandler, &errorHandler);
		DOMDocument *doc = parser->parseURI(url.getURLText());

		if (errorHandler.hasErrors())
		{
			std::string msg = errorHandler.getError();
			if (doc != NULL)
			{
				doc->release();
			}

			this->unlock();
			XCEPT_RAISE(xoap::exception::Exception, msg);

		}
		else
		{
			this->unlock();
			if (doc == NULL)
			{
				std::string msg = "Failed to parse DOM document from url ";
				msg += xoap::XMLCh2String(url.getURLText());
				XCEPT_RAISE(xoap::exception::Exception, msg);
			}
			DOMDocumentType * dtype = doc->getDoctype();
			if ( dtype != NULL )
			{
				std::string tnames = xoap::XMLCh2String(dtype->getName());	
				if ( tnames != "xml" )
				{
					std::string msg = "Invalid document type ";
                                	msg += tnames + ", expected XML document";
					doc->release();	
                                	XCEPT_RAISE(xoap::exception::Exception, msg);
				}
			}
			return doc;
		}

	}
	catch (const DOMLSException& e)
	{
		std::string msg = "XML parse error: ";
		msg += xoap::XMLCh2String(e.getMessage());
		this->unlock();
		XCEPT_RAISE(xoap::exception::Exception, msg);
	}
	catch (const DOMException& e)
	{
		std::string msg = "XML parse error: ";
		msg += xoap::XMLCh2String(e.getMessage());
		this->unlock();
		XCEPT_RAISE(xoap::exception::Exception, msg);
	}
	catch (const XMLException& xmle)
	{
		std::string msg = "DOM parse error: ";
		msg += xoap::XMLCh2String(xmle.getMessage());
		this->unlock();
		XCEPT_RAISE(xoap::exception::Exception, msg);
	}
}

DOMDocument* xoap::DOMParser::parse (const std::string& data) throw (xoap::exception::Exception)
{
	//std::cout << "DOMDocument* xoap::DOMParser::parse( const std::string& data ) throw(xoap::exception::Exception)" << std::endl;
	this->lock();

	XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument
	*doc = 0;

	try
	{
		//parser->resetDocumentPool();

		const char* c_data = data.c_str();
		MemBufInputSource source((const XMLByte*) c_data, data.length(), XStr("inMemoryXML"), false);
		Wrapper4InputSource wrapper(&source, false); // do not adopt the source
		xoap::DOMParserErrorHandler errorHandler;
		parser->getDomConfig()->setParameter(XMLUni::fgDOMErrorHandler, &errorHandler);
		doc = parser->parse(&wrapper);

		if (errorHandler.hasErrors())
		{
			std::string msg = errorHandler.getError();
			doc->release();
			this->unlock();
			XCEPT_RAISE(xoap::exception::Exception, msg);
		}
		else
		{
			this->unlock();
			return doc;
		}

	}
	catch (const DOMLSException& xmle)
	{
		std::string msg = "XML parse error: ";
		msg += xoap::XMLCh2String(xmle.getMessage());
		this->unlock();
		XCEPT_RAISE(xoap::exception::Exception, msg);
	}
	catch (const DOMException& de)
	{
		std::string msg = "DOM parse error: ";
		msg += xoap::XMLCh2String(de.msg);
		this->unlock();
		XCEPT_RAISE(xoap::exception::Exception, msg);
	}
	catch (const XMLException& xmle)
	{
		std::string msg = "DOM parse error: ";
		msg += xoap::XMLCh2String(xmle.getMessage());
		this->unlock();
		XCEPT_RAISE(xoap::exception::Exception, msg);
	}
}

DOMDocument* xoap::DOMParser::parse (const char* c_data, size_t size) throw (xoap::exception::Exception)
{
	this->lock();
	XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument
	*doc = 0;

	try
	{
		parser->resetDocumentPool();
		MemBufInputSource source((const XMLByte*) c_data, size, XStr("inMemoryXML"), false);
		Wrapper4InputSource wrapper(&source, false);
		xoap::DOMParserErrorHandler errorHandler;
		parser->getDomConfig()->setParameter(XMLUni::fgDOMErrorHandler, &errorHandler);

		doc = parser->parse(&wrapper);

		if (errorHandler.hasErrors())
		{
			std::string msg = errorHandler.getError();
			doc->release();
			this->unlock();
			XCEPT_RAISE(xoap::exception::Exception, msg);
		}
		else
		{
			this->unlock();
			return doc;
		}

	}
	catch (const DOMLSException& xmle)
	{
		std::string msg = "XML parse error: ";
		msg += xoap::XMLCh2String(xmle.getMessage());
		this->unlock();
		XCEPT_RAISE(xoap::exception::Exception, msg);
	}
	catch (const DOMException& de)
	{
		std::string msg = "DOM parse error: ";
		msg += xoap::XMLCh2String(de.msg);
		this->unlock();
		XCEPT_RAISE(xoap::exception::Exception, msg);
	}
	catch (xoap::exception::Exception& xe)
	{
		std::string msg = "Parse error";
		this->unlock();
		XCEPT_RETHROW(xoap::exception::Exception, msg, xe);
	}
	catch (...)
	{
		std::string soapText(c_data, size);

		std::string msg = "Parse error for message: ";
		msg += soapText;
		this->unlock();
		XCEPT_RAISE(xoap::exception::Exception, msg);
	}
}

DOMDocument* xoap::DOMParser::loadXML (const std::string& pathname) throw (xoap::exception::Exception)
{
	std::string filename;
	if ((pathname.find("file:/") == std::string::npos) && (pathname.find("http://") == std::string::npos) && (pathname.find("https://") == std::string::npos))
	{
		filename = "file:";
		filename += pathname;
	}
	else
	{
		filename = pathname;
	}

	XMLURL* source = 0;
	try
	{
		source = new XMLURL(filename.c_str());
	}
	catch (...)
	{
		XCEPT_RAISE(xoap::exception::Exception, "Failed to read XML data from " + filename);
	}

	try
	{
		DOMDocument* doc = this->parse(*source);
		delete source;
		return doc;
	}
	catch (xoap::exception::Exception& xe)
	{
		delete source;
		XCEPT_RETHROW(xoap::exception::Exception, "Failed to parse XML loaded from " + filename, xe);
	}
}

void xoap::DOMParser::lock ()
{
	this->mutex_.take();
}

void xoap::DOMParser::unlock ()
{
	this->mutex_.give();
}
