// $Id: SOAPAllocator.cc,v 1.5 2008/07/18 15:28:42 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include <iostream>
#include "xoap/SOAPAllocator.h"

static  int bufferAllocated = 0;

void * xoap::SOAPAllocator::allocate(size_t size) 
{
        //
        // Call old new
        //
        void *ptr = (void*) malloc(size);
        bufferAllocated++;
        return(ptr);
}

void xoap::SOAPAllocator::dispose(void * data) 
{
        free(data);
        bufferAllocated--;
}

void xoap::SOAPAllocator::testMemoryLeakage()
{
        if (bufferAllocated == 0)
        {
                std::cout << "Test passed: No memory leaks!" << std::endl;
        } else
        {
                std::cout << "Test failed: There are " << bufferAllocated << " memory leaks" << std::endl;
        }
}

