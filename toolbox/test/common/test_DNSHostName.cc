// $Id: test_regex.cc,v 1.3 2008/07/18 15:27:42 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include "toolbox/net/Utils.h"
#include "toolbox/net/exception/Exception.h"

int main(int argc, char** argv)
{


   if (argc != 2) {
        std::cout << "Not enough arguments" << std::endl;
        std::cout << "Usage:" << std::endl;
        std::cout << "	./test_DNSHostName.exe HOSTNAME" << std::endl;
        std::cout << "Example:" << std::endl;
        std::cout << "	./test_DNSHostName.exe cmstcdslab.cern.ch" << std::endl;
        return 1;
    }
 
   std::string hostname;
   std::string dnsHostname;
   hostname = hostname+argv[1];
   std::cout << "Hostname=" << hostname << std::endl;
   

   try {   
   	dnsHostname = toolbox::net::getDNSHostName(hostname);
   } catch (toolbox::net::exception::Exception& ne) {
   	std::cout << ne.what() << std::endl;
   	std::cout << std::endl;
	return 1;
   }

   std::cout << "DNSHostname=" << dnsHostname << std::endl;
   std::cout << std::endl;

   return 0;
}
