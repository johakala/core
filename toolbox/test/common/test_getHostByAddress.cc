// $Id: test_getHostByAddress.cc,v 1.1 2008/12/10 14:16:31 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include <iostream>
#include "xcept/tools.h"
#include "toolbox/net/Utils.h"

int main(int argc, char** argv)
{
	// argv[1] is hostname, argv[2] is portnumber
	if (argc < 3)
	{
		std::cout << argv[0] << ": [hostname] [port]" << std::endl;
		return 1;
	}

	try
	{
		struct sockaddr_in sa;
		sa = toolbox::net::getSocketAddressByName(argv[1], atoi(argv[2]));
		char* dq;
		dq = (char*) &(sa.sin_addr.s_addr);
		std::cout << "Address: (big endian): " << std::hex << sa.sin_addr.s_addr << ", (little endian): " << ntohl(sa.sin_addr.s_addr) << std::dec << ", (dotted quad): " << ((short) dq[0] & 0x00ff) << "." << ((short) dq[1] & 0x00ff) << "." << ((short) dq[2] & 0x00ff) << "." << ((short) dq[3] & 0x00ff) << std::dec << std::endl;
		std::cout << "Port (big endian): " << sa.sin_port << ", (little endian): " << ntohs(sa.sin_port) << std::endl;
	}
	catch (toolbox::net::exception::UnresolvedAddress & e)
	{
		std::cout << xcept::stdformat_exception_history(e) << std::endl;
	}


	return 0;
}
