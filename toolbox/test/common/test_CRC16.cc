// $Id: test_CRC16.cc,v 1.2 2008/07/18 15:27:42 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: V. Boyer and J. Gutleber and L. Orsini			 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "toolbox/net/Utils.h"

//---------------------------------Constants----------------------------------//
#define FILE_NAME_SIZE (256)
#define FRAME_SIZE (17000000)
//----------------------------------------------------------------------------//

int main(int argc, char** argv)
{

//------------------------------Declarations----------------------------------//
//A table to put the data
    unsigned char *frame;
//An unsigned short to put the new Crc
    unsigned short Crc;
//The input file with the frame
    FILE* infile;
//The name of the input file
    char fileName[FILE_NAME_SIZE];
//A byte to put each data
    unsigned char inbyte;
//An integer to put the number of bytes of the frame
    int lengthFrame = 0;
// to get the data
    int scan = 0;
    int wordCount = 0;
    int k =0;

    int endOfFrame = 0;
    int lastWord = 0;


//----------------------------------------------------------------------------//

//--------Initialize tables to contain the frame
    frame = (unsigned char*) malloc (sizeof (unsigned char) * FRAME_SIZE);

//--------Get the filemame
    if (argc == 2 && (sscanf(argv[1], "%s", fileName) == 1))
        strncpy(fileName, argv[1], FILE_NAME_SIZE);
    else
        strncpy(fileName, "sim_data_cut.txt", FILE_NAME_SIZE);

    printf("-----------------------------------------\n");
    printf("-----------------Calc Crc----------------\n");
    printf("Filename : %s \n", fileName);

//--------Open the file
    infile = fopen(fileName, "r");

//--------Put the data in a table for each frame
    while((fscanf(infile, "%02x", &inbyte) == 1))
    {
       scan++;

       if(wordCount > 8)
           wordCount = 0;

       if(wordCount != 0)
       {
           frame[lengthFrame] = inbyte;
           lengthFrame++;
       }

       wordCount++;
    }


//--------Display the number of data
           printf("--> %d bytes in the frame\n", lengthFrame);

//--------Display the frame in the order
           printf("The frame is :\n");
           for(k = 1; k < lengthFrame + 1; k++)
           {
               if((k % 8) == 0)
                   printf ("%02x \n", frame[k - 1]);
	       else
	           printf ("%02x ", frame[k - 1]);
           }

//--------Calcul of the new CRC

          Crc = toolbox::net::calculateCRC16(frame, lengthFrame);

          printf ("The CRC = %04x\n", Crc);


//--------Close the file
    fclose (infile);


    return 0;

}

