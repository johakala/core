// $Id: test_toString.cc,v 1.1 2008/09/19 09:07:46 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include <iostream>
#include "toolbox/string.h"
#include "xcept/tools.h"

int main(int argc, char** argv)
{
	int i = 100;
	float f = 3.141;
	
	std::cout << toolbox::toString ("Integer %d, float %f\n", i, f);

	return 0;
}
