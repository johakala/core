// $Id: test_hostname.cc,v 1.3 2008/07/18 15:27:42 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include <iostream>
#include "toolbox/net/Utils.h"

int main()
{
	std::cout << "Hostname is: " << toolbox::net::getHostName() << std::endl;
	std::cout << "Domainname is: " << toolbox::net::getDomainName() << std::endl;

	return 0;
}
