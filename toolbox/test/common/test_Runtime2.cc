// $Id: test_Runtime2.cc,v 1.2 2008/07/18 15:27:42 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include <iostream>
#include "toolbox/Runtime.h"

class ShutdownListener: public toolbox::ActionListener
{
	public:

	void actionPerformed(toolbox::Event& e)
	{
		std::cout << "Shutdown callback, event type: " << e.type() << std::endl;
	}
};

int main()
{
	std::cout << "Add a shutdown listener and exit with exit(2)" << std::endl;
	toolbox::Runtime* r = toolbox::getRuntime();
	r->addShutdownListener(new ShutdownListener());

	exit(2);

	return 0;
}
