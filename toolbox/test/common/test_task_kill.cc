// $Id: test_task_kill.cc,v 1.6 2008/07/18 15:27:42 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include <iostream>
#include <unistd.h>
#include "toolbox/Task.h"
#include "toolbox/TaskAttributes.h"

// Declare a task
//
class Counter: public toolbox::Task
{
	public:

	Counter(): toolbox::Task("Counter")
	{
		i = 0;
	}

	~Counter() 
	{
		std::cout << "DTOR of counter task" << std::endl;
	}

	int svc() 
	{
		for (;;) 
		{
			std::cout << "Counter task service routine loop, counter: " << i++ << std::endl;
			this->sleep(1);
		}
		return 0;
	}

	private:
	int i;
};

int main(int argc, char* argv[])
{
	Counter counter;
	counter.activate(argc, argv);

	::sleep(2);

	//
	// Print the task attributes
	//
	toolbox::TaskAttributes attr;
	counter.get(&attr);

	std::cout << "Stacksize: " << attr.stacksize() << std::endl;
	std::cout << "Priority:  " << attr.priority() << std::endl;
	std::cout << "Name:      " << attr.name() << std::endl;

	counter.kill();

	std::cout << "End of test." << std::endl;

	return 0;
}
