// $Id: test_bsem_timeout.cc,v 1.1 2009/03/06 09:45:37 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include <iostream>
#include "toolbox/Task.h"
#include "toolbox/TaskAttributes.h"
#include "toolbox/TaskGroup.h"
#include "toolbox/BSem.h"

#define PRODUCER 0
#define CONSUMER 1

class ProducerConsumer: public toolbox::Task
{
	public:
	
	ProducerConsumer (int which, char* type): toolbox::Task(type)
	{
		which_ = which;

		if (which_ == PRODUCER) 
		{
			mutex_ = new toolbox::BSem(toolbox::BSem::FULL);
		}
	}
	
	~ProducerConsumer()
	{
   		if (which_ == PRODUCER) 
		{
			std::cout << "In Producer DTOR" << std::endl;
                }
		else
		{
			std::cout << "In Consumer DTOR" << std::endl;
			delete mutex_;
		}
	}

	int svc()
	{
		       if (which_ == PRODUCER) 
		       {
				// This producer is only releasing the bsem after 30 seconds.
				// The client will try to get it and always timeout until then.
			       mutex_->take();
				std::cout << "Producer has successfully taken the BSem" << std::endl;

			       //
			       // Pause for 30 seconds
			       //
			       this->sleep(20);

			       //
			       // Signal the consumer to read
			       //
			       mutex_->give();
				std::cout << "Producer has released the BSem" << std::endl;
		       }
		       else  // consumer part
		       {
				// Loop until the bsem can be acuired with a timeout of 5 seconds
				struct timeval timeout;
				timeout.tv_sec = 5;
				timeout.tv_usec = 0;

				for (;;)
				{
			       		int val = mutex_->take(&timeout);
					if (val != 0)
					{
			       			std::cout << "Unable to acquire BSem, timeout" << std::endl;
					}
					else
					{
						std::cout << "Successfully acquired Bsem, exiting" << std::endl;
						break;
					}
				}
		       }
		return 0;
	}	
	
	private:
		
	int 		which_;
	static toolbox::BSem* 	mutex_;
};

toolbox::BSem* ProducerConsumer::mutex_  = 0;

int main(int argc, char** argv)
{
	toolbox::TaskGroup group;
	
	std::cout << "Create my task" << std::endl;
	
	ProducerConsumer p(PRODUCER, "Producer");
	ProducerConsumer c(CONSUMER, "Consumer");	
	
	std::cout << "Activate my task" << std::endl;
	
	p.initTaskGroup(&group);
  	// Wait a second to give the producer the opportunity to grab the BSem	
	::sleep(1);
	c.initTaskGroup(&group);
		
	p.activate();
	c.activate();
	
	group.join();
	
	std::cout << "Kill task and exit" << std::endl;
	
	std::cout << "End of main." << std::endl;
	return 0;
}
