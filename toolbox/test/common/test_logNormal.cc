// $Id: test_regex.cc,v 1.3 2008/07/18 15:27:42 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include "toolbox/math/random.h"

int main(int argc, char** argv)
{

unsigned long seed = 0;
unsigned long min = 0;
unsigned long max = 0;
unsigned long samples = 0;
double mean = 0;
double stdDev = 0;

   if (argc != 7) {
        std::cout << "Not enough arguments" << std::endl;
        std::cout << "Usage:" << std::endl;
        std::cout << "	./test_logNormal.exe SEED MIN MAX SAMPLES MEAN STDDEV" << std::endl;
        std::cout << "Example:" << std::endl;
        std::cout << "	./test_logNormal.exe 1212 24 65000 100 2048 4096" << std::endl;
        return 1;
    }

	seed = atol(argv[1]);
	min = atol(argv[2]);
	max = atol(argv[3]);
	samples = atol(argv[4]);
	mean = atof(argv[5]);
	stdDev = atof(argv[6]);

	std::cout << "Seed=" << seed << std::endl;
	std::cout << "Min=" << min << std::endl;
	std::cout << "Max=" << max << std::endl;
	std::cout << "Samples=" << samples << std::endl;
	std::cout << "Mean=" << mean << std::endl;
	std::cout << "StdDev=" << stdDev << std::endl;

	toolbox::math::LogNormalGen * lognorm;
	lognorm = new toolbox::math::LogNormalGen(seed, mean, stdDev, min, max);
	for (unsigned long i=0; i< samples; i++)
	{
		if (i!=0) {
	 	std::cout << ",";
		
		}
		unsigned long size =  lognorm->getRandomSize();
	 	std::cout << size;
	}
	std::cout << std::endl;

	return 0;
}
