// $Id: test_FileSystemInfo.cc,v 1.2 2008/07/18 15:27:42 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include <iostream>
#include "toolbox/Runtime.h"
#include "xcept/tools.h"

int main()
{
	try
	{
		toolbox::FileSystemInfo::Reference fsi = toolbox::getRuntime()->getFileSystemInfo();

		std::set<std::string> fsList = fsi->getFileSystems();
		
		for (unsigned int i = 0; i < 5; ++i)
		{ 
			std::cout << "Last sampled " << fsi->sampleDuration() << " seconds ago" << std::endl;
			std::cout << "File System\tfree \%\tused \%\tfree MB\tused MB\ttotal MB" << std::endl;
			for (std::set<std::string>::iterator i = fsList.begin(); i != fsList.end(); ++i)
			{
				std::cout << (*i) << "\t\t";
				std::cout << fsi->available( (*i), "%" ) << "\t";
				std::cout << fsi->used( (*i), "%" ) << "\t";
				std::cout << fsi->available( (*i), "MB" ) << "\t";
				std::cout << fsi->used( (*i), "MB" ) << "\t";
				std::cout << fsi->total( (*i), "MB" ) << std::endl;
			}
			
			::sleep(2);
		}
	}
	catch  (toolbox::exception::Exception & e)
	{
		std::cout << xcept::stdformat_exception_history(e) << std::endl;
	}
	
	return 0;
}
