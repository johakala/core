// $Id: test_task.cc,v 1.8 2008/07/18 15:27:42 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "toolbox/Task.h"
#include "toolbox/TaskAttributes.h"
#include <iostream>
#include <unistd.h>

class MyTask: public toolbox::Task
{
	public:

	MyTask() : toolbox::Task("MyTask")
	{
	
	}
	
	int svc()
	{
		
		std::cout << "Hello world!" << std::endl;
		return(0);
	}	

};

int main (int argc, char* argv[])
{
	std::cout << "Create my task" << std::endl;
	
	MyTask* aTask = new MyTask();
	
	std::cout << "Activate my task" << std::endl;
	//aTask.activate(0, "Ciao", 0);
	aTask->activate();
	
	::sleep(5);
	
	std::cout << "Kill task and exit" << std::endl;
	aTask->kill();
	
	
	delete aTask;
	std::cout << "End of main." << std::endl;
	
	return 0;
}
