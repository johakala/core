// $Id: test_task_sleep.cc,v 1.7 2008/07/18 15:27:42 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include <iostream>
#include "toolbox/Task.h"
#include "toolbox/TaskAttributes.h"
#include "toolbox/TaskGroup.h"
#include <unistd.h>

//
// A simple counting task
//
class Sleeper: public toolbox::Task
{
	int s_;

	public:

	Sleeper(int s): toolbox::Task ("Sleeper") 
	{
		s_ = s;
	}

	~Sleeper() 
	{
		std::cout << "DTOR of Sleeper task" << std::endl;
	}

	int svc() 
	{
		std::cout << "Going to sleep for " << s_ << " seconds." << std::endl;
		sleep(s_);
		std::cout << "Returning from sleep after " << s_ << " seconds." << std::endl;
		return 0;
	}
};

int main(char** argv, int argc)
{
	Sleeper s1(10);
	Sleeper	s2(15);
	
	s2.activate();
	s1.activate();
	
	std::cout << "Main thread sleep now 2 seconds" << std::endl;
	sleep (2);
	std::cout << "Wakeup thread 1" << std::endl;
	s1.wakeup();
	std::cout << "Finishing program." << std::endl;
	pause();
  	return 0;
}
