// $Id: test_bsem.cc,v 1.5 2008/07/18 15:27:42 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include <iostream>
#include "toolbox/Task.h"
#include "toolbox/TaskAttributes.h"
#include "toolbox/TaskGroup.h"
#include "toolbox/BSem.h"

#define PRODUCER 0
#define CONSUMER 1

class ProducerConsumer: public toolbox::Task
{
	public:
	
	ProducerConsumer (int which, char* type): toolbox::Task(type)
	{
		which_ = which;

		if (which_ == PRODUCER) 
		{
			//
			// The full_ semaphore indicates that
			// data is in the shared region, ready to be read.
			//
			full_  = new toolbox::BSem(toolbox::BSem::EMPTY);

			//
			// The empty_ semaphore indicates to the producer
			// that it can write again to the shared region, as
			// the consumer has read a pending data item.
			//
			empty_ = new toolbox::BSem(toolbox::BSem::FULL);
		}
	}
	
	~ProducerConsumer()
	{
   		if (which_ == PRODUCER) 
		{
			std::cout << "In Producer DTOR" << std::endl;
			delete full_;
			delete empty_;
                }
		else
		{
			std::cout << "In Consumer DTOR" << std::endl;
		}
	}

	int svc()
	{
		for (int i = 0; i < 5; i++) 
		{
		       if (which_ == PRODUCER) 
		       {
			       //
			       // Acquire the empty semaphore that
			       // is set by the consumer after having
			       // read the value.
			       //
			       empty_->take();

			       //
			       // Pause for a second
			       //
			       this->sleep(1);

			       //
			       // Write to the shared region
			       //
			       shared_++;

			       //
			       // Signal the consumer to read
			       //
			       full_->give();
		       }
		       else  // consumer part
		       {
			       full_->take();

			       std::cout << "Read: " << shared_ << std::endl;

			       empty_->give();
		       }
		}
		return 0;
	}	
	
	private:
		
	int 		which_;
	static toolbox::BSem* 	full_;
	static toolbox::BSem* 	empty_;
	static int 	shared_;

};

toolbox::BSem* ProducerConsumer::full_   = 0;
toolbox::BSem* ProducerConsumer::empty_  = 0;
int ProducerConsumer::shared_ = 0;

int main(int argc, char** argv)
{
	toolbox::TaskGroup group;
	
	std::cout << "Create my task" << std::endl;
	
	ProducerConsumer p(PRODUCER, "Producer");
	ProducerConsumer c(CONSUMER, "Consumer");	
	
	std::cout << "Activate my task" << std::endl;
	
	p.initTaskGroup(&group);
	c.initTaskGroup(&group);
		
	p.activate();
	c.activate();
	
	group.join();
	
	std::cout << "Kill task and exit" << std::endl;
	
	std::cout << "End of main." << std::endl;
	return 0;
}
