// $Id: test_string_conversion.cc,v 1.2 2008/07/18 15:27:42 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include <iostream>
#include "toolbox/string.h"
#include "xcept/tools.h"

int main(int argc, char** argv)
{
	try
	{
		std::string l = "1234";
		std::string d = "1234.1234";
		long ln = toolbox::toLong(l);
		double dn = toolbox::toDouble(d);

		std::cout << "Converted long " << ln << ", double " << dn << std::endl;
	}
	catch (toolbox::exception::Exception& e)
	{
		xcept::stdformat_exception_history(e);
	}

	try
	{
		std::string le = "54294967296";
		std::cout << "Converted long " << toolbox::toLong(le) << std::endl;
	}
	catch (toolbox::exception::Exception& e)
	{
		std::cout << xcept::stdformat_exception_history(e) << std::endl;
	}

	try
	{
		std::string de = "2.8e+20";
		std::cout << "Converted double " << toolbox::toDouble(de) << std::endl;
	}
	catch (toolbox::exception::Exception& e)
	{
		std::cout << xcept::stdformat_exception_history(e) << std::endl;
	}
	return 0;
}
