// $Id: test_gauge.cc,v 1.3 2008/07/18 15:27:42 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and S. Murray					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include <iostream>
#include "toolbox/ActionListener.h"
#include "toolbox/ThresholdCrossedEvent.h"
#include "toolbox/Gauge.h"
#include "toolbox/GaugeEvent.h"
#include "toolbox/GaugeFactory.h"

class TestListener: public toolbox::ActionListener
{
	public:
	
	TestListener()
	{}
	
	virtual ~TestListener()
	{}
	
	void actionPerformed(toolbox::Event& e) 
	{
		std::cout << "Received event ";
		std::cout << e.type() << std::endl;
		
		if(e.type() == "ThresholdCrossedEvent")
		{
			try
			{
				toolbox::ThresholdCrossedEvent& te = dynamic_cast<toolbox::ThresholdCrossedEvent&>(e);
		
				std::cout << "Name: " << te.getName() << ", threshold: " << te.getThreshold();
				std::cout << ", from value: " << te.getFromValue() << ", to value: " << te.getToValue() << std::endl;
			} 
			catch (...)
			{
				std::cout << "Failed to cast to toolbox::ThresholdCrossedEvent&" << std::endl;
			}		
		}
		else if(e.type() == "GaugeAvailableEvent")
		{
			try
                        {
                                toolbox::GaugeEvent& ge = dynamic_cast<toolbox::GaugeEvent&>(e);

				std::cout << "Gauge:" << ge.getName() << " became available" << std::endl;
                        }
                        catch (...)
                        {
                                std::cout << "Failed to cast to toolbox::GaugeEvent&" << std::endl;
                        }
		}
	}
};

int main ()
{
	TestListener listener;
	
	toolbox::GaugeFactory *factory = toolbox::getGaugeFactory();
	factory->addActionListener(&listener);
	toolbox::Gauge *g = factory->createGauge("testGauge", 0.0, 10.0, 5.0);
	g->addActionListener (&listener);
	
	g->addThreshold ( "A", 1.1 );
	g->addThreshold ( "B", 2.2 );
	g->addThreshold ( "C", 3.3 );
	
	
	g->setValue (0.5);
	g->setValue (1.5);
	g->setValue (5.0);
	
	return 0;
}
