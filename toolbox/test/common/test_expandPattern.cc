// $Id: test_expandPattern.cc,v 1.2 2008/07/18 15:27:42 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include <iostream>
#include "toolbox/Runtime.h"

int main(int argc, char** argv)
{
	try
	{
		std::vector<std::string> list = toolbox::getRuntime()->expandPathPattern(argv[1]);

		for (unsigned int i = 0; i < list.size(); i++)
		{
			std::cout << i << " is " << list[i] << std::endl;
		}	
	} catch (toolbox::exception::Exception& e)
	{
		std::cout << "Error: " << e.what() << std::endl;
	}

	return 0;
}
