// $Id: test_task_create.cc,v 1.7 2008/07/18 15:27:42 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include <iostream>
#include <vector>
#include <unistd.h>
#include "toolbox/Task.h"
#include "toolbox/TaskAttributes.h"

class MyTask: public toolbox::Task
{
	bool running_;
	bool active_;
	int  s_;
	
	public:
	
	MyTask (int s): toolbox::Task("MyTask")
	{
		active_ = false;
		s_ = s;
	}
	
	void start()
	{
		running_ = true;
		this->activate();
	}
	
	void stop()
	{
		running_ = false;
	}
	
	bool running()
	{
		return active_;
	}

	//int svc(int arg1)
	int svc()
	{
		active_ = true;
		while (running_)
		{
			if (s_ > 0) this->sleep(s_);
		}
		active_ = false;
		return(0);
	}	
};

int main (int argc, char* argv[])
{
	if (argc < 3)
	{
		std::cout << "usage: " << argv[0] << " iterations[1..n] sleep[0..n] " << std::endl;
		return 0;
	}
	unsigned long n = ::atoi(argv[1]);
	int s = ::atoi(argv[2]);
	
	std::cout << "Creating/deleting tasks " << n << " times. Sleeping in between: " << s << " seconds." << std::endl;
	
	//MyTask t(s);
	std::vector<MyTask *> t;
	
	for (unsigned long i = 0; i < n; i++)
	{
		MyTask* my = new MyTask(s);
		my->start();
		t.push_back(my);
	}
	
	sleep(1);
	
	for (std::vector<MyTask*>::size_type i = 0; i < t.size(); i++)
	{
		t[i]->stop();
		while (t[i]->running()) { t[i]->yield(1); }
		delete t[i];
		std::cout << "Task " << i << " done." << std::endl;	
	}
	
	std::cout << "Test done." << std::endl;
	return 0;
}
