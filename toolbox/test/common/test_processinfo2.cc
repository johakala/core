// $Id: test_processinfo2.cc,v 1.1 2008/09/01 07:14:32 rmoser Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include <iostream>
#include <sstream>
#include "toolbox/Runtime.h"
#include "toolbox/ProcessInfo.h"
#include "toolbox/task/Timer.h"
#include "toolbox/task/TimerEvent.h"
#include "toolbox/task/TimerFactory.h"
#include "toolbox/task/TimerListener.h"

#include "toolbox/Task.h"

#include <sys/sysinfo.h>

class Info :
	public toolbox::task::TimerListener
{
	private:
		toolbox::ProcessInfo::Reference processInfo_;
		int num_;

	public:
		Info(int num)
		{
			num_ = num;

			// Create a process object for monitoring the local resource usage
			this->processInfo_ = toolbox::getRuntime()->getProcessInfo(toolbox::getRuntime()->getPid());
			this->processInfo_->sample();

			std::stringstream ss;
			ss << "TestTimer" << num;

			// trigger timer every 6 seconds
			toolbox::task::Timer * timer = toolbox::task::getTimerFactory()->createTimer(ss.str());
			toolbox::TimeInterval interval(6,0);
			toolbox::TimeVal start;
			start = toolbox::TimeVal::gettimeofday();
			timer->scheduleAtFixedRate( start, this, interval,  0, "" );
		}

		virtual ~Info()
		{
		}

		void timeExpired (toolbox::task::TimerEvent& e)
		{
			processInfo_->sample();

			std::cout << "CPUUsage [" << num_ << "] [" << processInfo_->cpuUsage() << " %]" << std::endl;
			std::cout << "rsize()  [" << num_ << "] [" << (processInfo_->rsize() >>10) << " kB]" << std::endl;
			std::cout << "vsize()  [" << num_ << "] [" << (processInfo_->vsize() >>10) << " kB]" << std::endl;
		}
};

class Endless :
	public toolbox::Task
{
	public:
		Endless() :
			Task("task")
		{
		}

		int svc()
		{
  		while(1)
			{
  			long a=0;
				for(long i=0 ; i<1000000000; i++)
  			{
  			  a+=i;
  			}
	  		std::cout << "xxx" << std::endl;
			}

			return 0;
		}
};


int main(int argc, char *argv[])
{
	// create two objects to see if they interfere
	new Info(0);
	new Info(1);

	int ncpu = get_nprocs();
	for(int i=0;i<ncpu;i++)
	{
		Endless *task = new Endless();
		task->activate();
	}

  while(1)
	{
		sleep(10);
	}

	return 0;
}

