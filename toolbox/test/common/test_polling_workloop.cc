// $Id: test_polling_workloop.cc,v 1.4 2008/07/18 15:27:42 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "toolbox/task/WorkLoopFactory.h"
#include "toolbox/task/PollingWorkLoop.h"
#include "toolbox/task/Action.h"
#include "toolbox/lang/Class.h"

#include <iostream>
#include <unistd.h>

class Test: public toolbox::lang::Class
{
	public:
	
	Test()
	{
		// Get a work loop
		toolbox::task::WorkLoop* wl = toolbox::task::getWorkLoopFactory()->getWorkLoop("PollingWorkLoop", "polling");

		j1_ = toolbox::task::bind (this, &Test::job1, "job1");
		j2_ = toolbox::task::bind (this, &Test::job2, "job2");
		
		wl->submit(j1_);
		
		
		wl->activate();
		
		std::cout << "Work Loop active: " << wl->isActive() << std::endl;
		
		sleep(1);
		
		wl->cancel();
		
		std::cout << "Work Loop active: " << wl->isActive() << std::endl;	
		
		sleep(10);
		
		std::cout << "Done." << std::endl;		
	}
	
	bool job1(toolbox::task::WorkLoop* wl)
	{
		std::cout << "Job 1" << std::endl;
		wl->submit(j2_);
		return true; // go on
	}
	
	bool job2(toolbox::task::WorkLoop* wl)
	{
		std::cout << "Job 2" << std::endl;
		return false; // do once
	}
	
	private:
	
		toolbox::task::ActionSignature* j1_;
		toolbox::task::ActionSignature* j2_;		
	
};

int main ()
{
	Test t;
	return 0;
}
