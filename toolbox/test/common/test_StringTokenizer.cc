// $Id: test_StringTokenizer.cc,v 1.2 2008/07/18 15:27:42 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini                                    *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <string>
#include "toolbox/string.h"

int main()
{

   //string tempStr = "|x|x|x~|x|x|x~aa~ |x ~b~b |x ~c~c~ |x|x|x|x ~d~d~|xw|xs|xd|x3|x4|xd|xf|x1|x222|xwwww|xgg|xjj|xkk|xvv|x|x22|x#3";
   //StringTokenizer strtok = StringTokenizer(tempStr,"|x");

   std::string tempStr = "01|02|03|04|05|06|07|08|09|10|11|12";

   toolbox::StringTokenizer strtok = toolbox::StringTokenizer(tempStr,"|");


   std::cout << "Number Of Tokens: " << strtok.countTokens()     << std::endl;
   std::cout << "String:           " << strtok.remainingString() << std::endl;

   int cnt = strtok.countTokens();
   std::string finalString ="";

   for(int i=0; i < cnt; i++)
   {
      std::string tempStr ="";
      std::cout << "Token[" << i << "] ------> [" << (tempStr=strtok./*filterN*/nextToken(/*" "*/)) << "]       ";
      std::cout << "Token Count" << strtok.countTokens() << std::endl;
      finalString += tempStr;
   }

   std::cout << std::endl << "Final String: " << finalString <<  std::endl;

   return 0;
}

