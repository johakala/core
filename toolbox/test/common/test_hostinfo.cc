// $Id: test_hostinfo.cc,v 1.1 2009/05/08 13:01:32 rmoser Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include <iostream>
#include "toolbox/HostInfoImpl.h"
#include "toolbox/task/Timer.h"
#include "toolbox/task/TimerEvent.h"
#include "toolbox/task/TimerFactory.h"
#include "toolbox/task/TimerListener.h"

#include "toolbox/Task.h"

#include <sys/sysinfo.h>

class Info :
	public toolbox::task::TimerListener
{
	private:
		toolbox::HostInfoImpl *hostInfo_;

	public:
		Info()
		{
			// Create a process object for monitoring the local resource usage
			//this->hostInfo_ = toolbox::getRuntime()->getHostInfo(toolbox::getRuntime()->getPid());
			this->hostInfo_ = new toolbox::HostInfoImpl();
			this->hostInfo_->sample();

			// trigger timer every 6 seconds
			toolbox::task::Timer * timer = toolbox::task::getTimerFactory()->createTimer("TestTimer");
			toolbox::TimeInterval interval(6,0);
			toolbox::TimeVal start;
			start = toolbox::TimeVal::gettimeofday();
			timer->scheduleAtFixedRate( start, this, interval,  0, "" );
		}

		virtual ~Info()
		{
		}

		void timeExpired (toolbox::task::TimerEvent& e)
		{
			hostInfo_->sample();

			std::cout << "CPUUsage [" << hostInfo_->getCpuUsage() << " %]" << std::endl;
			hostInfo_->show();
		}
};

class Endless :
	public toolbox::Task
{
	public:
		Endless() :
			Task("task")
		{
		}

		int svc()
		{
  		while(1)
			{
  			long a=0;
				for(long i=0 ; i<1000000000; i++)
  			{
  			  a+=i;
  			}
	  		std::cout << "xxx" << std::endl;
			}

			return 0;
		}
};


int main(int argc, char *argv[])
{
	new Info();

	int ncpu = get_nprocs();
	for(int i=0;i<ncpu;i++)
	{
		Endless *task = new Endless();
		task->activate();
	}

  while(1)
	{
		sleep(10);
	}

	return 0;
}

