// $Id: test_task_timer_insert.cc,v 1.4 2008/07/18 15:27:42 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "toolbox/task/Timer.h"
#include <iostream>
#include <unistd.h>

class Test: public toolbox::task::TimerListener
{
	public:

	void timeExpired(toolbox::task::TimerEvent& e)
	{
		std::cout << "Name: "  << e.getTimerTask()->name << std::endl;
		std::cout << "Actual time: " << e.getTimerTask()->lastExecutionTime.toString(toolbox::TimeVal::loc) << std::endl;
		std::cout << "Schedule time: "  << e.getTimerTask()->schedule.toString(toolbox::TimeVal::loc) << std::endl;
		
		toolbox::TimeVal delta = e.getTimerTask()->lastExecutionTime-e.getTimerTask()->schedule;
		
		// std::cout << "Delta: " << delta.fmt_ss_mls() << std::endl;
		std::cout << "Delta: " << delta.usec() << " usec" << std::endl;
		std::cout << "-------------------------------------------------------" << std::endl;
		std::cout << std::endl;
	}	
	
};

int main (int argc, char** argv)
{
	Test t;
	
	toolbox::task::Timer timer("myTimer");

	timer.start();
	
	std::cout << "Now schedule a task every 20 seconds" << std::endl;
	
	toolbox::TimeInterval period1(20,0);
	toolbox::TimeVal now = toolbox::TimeVal::gettimeofday();
	timer.scheduleAtFixedRate (now, &t, period1, (void*) 0, "pippo");
	
	getchar();

	std::cout << "Now schedule a task every 1 seconds" << std::endl;

	toolbox::TimeInterval period2(1,0);
	now = toolbox::TimeVal::gettimeofday();
	timer.scheduleAtFixedRate (now, &t, period2, (void*) 0, "pluto");

	getchar();

	std::cout << "Now remove the 1 second task again" << std::endl;

	timer.remove("pluto");

	getchar();
	
	timer.stop();
	
	return 0;
}
