// $Id: test_CRC16_compare.cc,v 1.2 2008/07/18 15:27:42 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include <iostream>
#include "toolbox/net/Utils.h"
#include "toolbox/hexdump.h"
#include "netinet/in.h"

int main(int argc, char** argv)
{
	unsigned char buffer[1024]; // first buffer
	
	// fill first buffer;
	
	unsigned long i;
	for (i = 0; i < 1022; i++)
	{
		// Fill the two buffers with ABCDEF
		//
		buffer[i] = (unsigned char) ((i % 6) + 65);
	}

	// fill last two bytes with 0
	buffer[1022] = 0x0;
	buffer[1023] = 0x0;

	// print out the first buffer	
	toolbox::hexdump(buffer, 1024);
	
	// Calculate the CRCs 
	unsigned short crc = toolbox::net::calculateCRC16(buffer, 1022);
	printf ("CRC of buffer: %04x\n", crc);

	// Add the checksum at the end of the packet
	// mind that unsigned short may be little endian on Intel
	// and we need to convert it into network byte order to
	// put it into the stream
	//
	unsigned short swapped = htons(crc);
	memcpy (&buffer[1022], &swapped, sizeof(unsigned short));

	toolbox::hexdump(buffer, 1024);

	// Recalculate the CRC now, should be 0
	unsigned short receiveCrc = toolbox::net::calculateCRC16(buffer, 1024);
	printf ("CRC of correctly receive buffer: %04x\n", receiveCrc);
	
	std::cout << "Now introduce an error in byte 6 of buffer: ABCDEF -> ABCDEA" << std::endl;
	
	buffer[5] = 'A';
	
	// Recalculate CRC of buffer 1
	receiveCrc = toolbox::net::calculateCRC16(buffer, 1024);
	
	printf ("CRC of buffer after error: %04x\n", receiveCrc);

    return 0;

}

