// $Id: test_smartbufpool.cc,v 1.1 2008/09/19 08:50:59 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include <iostream>
#include <vector>
#include "toolbox/mem/Reference.h"
#include "toolbox/mem/SmartBufPool.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "toolbox/mem/CommittedHeapAllocator.h"
#include "toolbox/mem/exception/Exception.h"
#include "toolbox/mem/exception/FailedAllocation.h"
#include "xcept/tools.h"


int main ()
{
		size_t blockSize = 0x500; // minus 1 KB in order to avoid allocating blocks of the next higher size
		size_t poolSize = 0x5000; // 50 MB
		toolbox::net::URN urn("toolbox-mem-pool", "TestPool");
		std::string poolName = urn.toString();
		toolbox::mem::Pool* pool = 0;
		//std::vector<toolbox::mem::Reference*> references;
		
		//toolbox::mem::Reference* references[100];
		toolbox::mem::Buffer* references[100];
		for (int x = 0; x < 100; ++x)
		{
			references[x] = 0;
		}

		try 
		{
			toolbox::mem::CommittedHeapAllocator* a = new toolbox::mem::CommittedHeapAllocator(poolSize);
			pool = toolbox::mem::getMemoryPoolFactory()->createPool(urn, a);	
		}
		catch (toolbox::mem::exception::Exception & e)
		{
			std::cout << xcept::stdformat_exception_history(e) << std::endl;
		}

 		toolbox::mem::MemoryPoolFactory* factory = toolbox::mem::getMemoryPoolFactory();

		for (;;)
	        {
		size_t total = 0;
		for (int n = 0; n < 10; ++n)
		{
			try
                	{
                		// references[n] = factory->getFrame(pool, blockSize);
                		references[n] = pool->alloc(blockSize);
				total = total + blockSize;
                	}
                	catch (	toolbox::mem::exception::FailedAllocation & e )
                	{
				references[n] = 0;
				std::cout << "*** Failed to allocate: " << xcept::stdformat_exception_history(e) << std::endl;
				std::cout << std::endl;
                	}
		}

		std::cout << "Allocated in total: " << total << " out of " << poolSize << ", diff: " << poolSize-total << std::endl;

		((toolbox::mem::SmartBufPool*)pool)->show();

		std::cout << "RELEASE BEGIN" << std::endl;

		size_t v = 0;
		while(references[v] != 0)
		{
			std::cout << "Entry in: " << v << std::endl;
			++v;
		}

		v = 0;
		while(references[v] != 0)
		{
			try
			{
				//references[v]->release();
				pool->free(references[v]);
			} catch ( toolbox::mem::exception::Exception & e )
                        {
                                std::cout << "Failed to free: " << xcept::stdformat_exception_history(e) << std::endl;
                                std::cout << std::endl;
                                break;
                        }

			references[v] = 0;
			++v;
		}

		std::cout << "RELEASE END" << std::endl;

		((toolbox::mem::SmartBufPool*)pool)->show();

		std::getchar();
	}


	return 0;
}
