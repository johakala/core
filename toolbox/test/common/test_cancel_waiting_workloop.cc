// $Id: test_cancel_waiting_workloop.cc,v 1.4 2008/07/18 15:27:42 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "toolbox/task/WorkLoopFactory.h"
#include "toolbox/task/WaitingWorkLoop.h"
#include "toolbox/task/Action.h"
#include "toolbox/lang/Class.h"
#include "toolbox/BSem.h"

#include <iostream>
#include <unistd.h>

class Test: public toolbox::lang::Class
{
	public:
	
	toolbox::BSem mutex_;
	
	Test(): mutex_(toolbox::BSem::EMPTY)
	{
		// Get a work loop
		toolbox::task::WorkLoop* wl = toolbox::task::getWorkLoopFactory()->getWorkLoop("WaitingWorkLoop", "polling");

		j_ = toolbox::task::bind (this, &Test::job1, "job");
		
		wl->submit(j_);
		
		wl->activate();
		
		std::cout << "Work Loop active: " << wl->isActive() << std::endl;
		
		sleep(10);
		
		wl->cancel();
		
		wl->remove(j_);
		
		std::cout << "Work Loop active: " << wl->isActive() << std::endl;	
		
		sleep(5);
		
		std::cout << "Re-activate..." << std::endl;
		
		//wl->schedule(j_);
		
		wl->activate();
		
		// wl->submit(j_);		
		
		std::cout << "Work Loop active: " << wl->isActive() << std::endl;	
		
		sleep(5);
		
		std::cout << "Stopping again..." << std::endl;
		
		wl->cancel();
		
		std::cout << "Done: " << wl->isActive() << std::endl;		
	}
	
	bool job1(toolbox::task::WorkLoop* wl)
	{
		std::cout << "job run" << std::endl;
		sleep(1);
		return true; // go on
	}
	
	
	private:
	
		toolbox::task::ActionSignature* j_;
};

int main ()
{
	Test t;
	return 0;
}
