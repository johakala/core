// $Id: test_condition.cc,v 1.5 2008/07/18 15:27:42 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "toolbox/Task.h"
#include "toolbox/TaskAttributes.h"
#include "toolbox/TaskGroup.h"
#include "toolbox/SyncQueue.h"

#define PRODUCER 0
#define CONSUMER 1

class ProducerConsumer: public toolbox::Task
{
	public:
	
	ProducerConsumer (int which, char* type, toolbox::SyncQueue<int>* q): toolbox::Task(type)
	{
		queue_ = q;
	  	which_ = which;
	}
	
	~ProducerConsumer()
	{
	}

	int svc()
	{
		if (which_ == PRODUCER) 
		{
			int i = 0;
			queue_->push (i++);
			queue_->push (i++);
			queue_->push (i++);
			queue_->push (i++);
			queue_->push (i++);
			
			while (i < 10)
			{
			       queue_->push (i++);
			       this->sleep(1);
			}
		}
		else  // consumer part
		{
			for (int i = 0; i < 10; i++) 
			{
				int x = queue_->pop();
				int size = queue_->size();
				std::cout << "Read: " << x << ", queue size: " << size << std::endl;
			}
		}
		return 0;
	}	
	
	private:
		
	int 		which_;
	toolbox::SyncQueue<int>* queue_;
	

};

int main (int argc, char* argv[])
{
	toolbox::SyncQueue<int> queue;
	toolbox::TaskGroup group;
	
	std::cout << "Create two tasks, producer and consumer" << std::endl;
	
	ProducerConsumer p(PRODUCER, "Producer", &queue);
	ProducerConsumer c(CONSUMER, "Consumer", &queue);	
	
	std::cout << "Activate tasks" << std::endl;
	//aTask.activate(0, "Ciao", 0);
	
	p.initTaskGroup(&group);
	c.initTaskGroup(&group);
		
	p.activate();
	c.activate();
	
	group.join();
	
	std::cout << "Shutdown tasks" << std::endl;
	
	p.kill();
	c.kill();	
	
	std::cout << "End of test." << std::endl;
	return 0;
}
