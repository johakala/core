// $Id: test_task_suspend.cc,v 1.6 2008/07/18 15:27:42 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include <iostream>
#include "toolbox/Task.h"
#include "toolbox/TaskAttributes.h"
#include "toolbox/TaskGroup.h"
#include <unistd.h>

//
// A simple counting task
//
class Counter: public toolbox::Task
{
	public:

	Counter(): toolbox::Task("Counter")
	{
	}

	~Counter() 
	{
		std::cout << "DTOR of Counter" << std::endl;
	}

	int svc() 
	{
		for (i = 0; i < 10; i++) 
		{
			std::cout <<  "Task " 
			   << argv_[0] 
			   << " of " 
			   << argv_[1] 
			   << ": " 
			   << i++ << std::endl;

			if (i == 5) 
			{
				std::cout << "Wait for a wakeup signal..." << std::endl;
				this->pause();
				std::cout << "Continue" << std::endl;
			}
		}

		std::cout << "Wait for 5 seconds" << std::endl;
		this->sleep(5);
		std::cout << "Exit from thread service routine (svc)" << std::endl;
		return 0;
	}

	private:
	int i;
};

int main(int argc, char* argv[])
{
	//
	// Declare 2 tasks
	//
	Counter counter[3];

	//
	// Create a thread group
	//
	toolbox::TaskGroup group;

	counter[0].initTaskGroup(&group);
	counter[1].initTaskGroup(&group);

	//
	// Spawn the threads
	//
	counter[0].activate(2, "0", "3");
	counter[1].activate(2, "1", "3");
	counter[2].activate(2, "2", "3");

	//
	// Let them run for two seconds
	//
	sleep(5);

	//
	// Wake up the pausing tasks
	//
	counter[0].wakeup();

	//sleep(1);

	counter[1].wakeup();

	//
	// Wait for service routines to finish
	//
	group.join();

	std::cout << "Kill third task and exit" << std::endl;
	counter[2].kill();

	std::cout << "End of test" << std::endl;

	return 1;
}
