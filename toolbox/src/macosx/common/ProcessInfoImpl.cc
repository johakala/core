// $Id: ProcessInfoImpl.cc,v 1.8 2008/09/09 08:30:35 rmoser Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/
 
#include "toolbox/ProcessInfoImpl.h"



//#include <unistd.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <ios>
#include <iostream>
#include <fstream>
#include <sstream>

#include "toolbox/TimeVal.h"
#include "config/PackageInfo.h"

#ifndef CLK_TCK
#define CLK_TCK 100
#endif

toolbox::ProcessInfoImpl::ProcessInfoImpl(pid_t pid) throw (toolbox::exception::Exception)
{
	// Initialize the sample timestamp
	struct timeval now;
	gettimeofday (&now, 0);
	
	last_sample_time_ = now.tv_sec + (now.tv_usec/1000000.0); // initialize sample time
	
	last_process_time_ = 0; // initialize last process CPU usage time
	cpu_percent_ = 0.0; // initialize CPU usage;

    	thread_pid = pid;
	
	// Set system pagesize
	pageSize_ = getpagesize(); 

	// Set the number of processors;
	ncpu_ = 1;
}

toolbox::ProcessInfoImpl::~ProcessInfoImpl() 
{
}
  
double toolbox::ProcessInfoImpl::cpuUsage()
{
	return cpu_percent_;
}

void toolbox::ProcessInfoImpl::calcCPUUsage (double sample_duration)
{	 
	// assume utime, stime already sampled
	// calculate process usage time in JIFFIE

	size_t process_time;
    // utime and stime take only the root thread into account. This one is sleeping, however and not consuming time
	process_time =  (utime_ + stime_);

	// calculate time in seconds the process was working
	// since the last sampling operation
	size_t process_active_time = process_time - last_process_time_;

	// Only calculate CPU usage if sample time is not 0
	// in practice this means that a cpu usage sample can be
	// done at most at a granularity of 1 second, since the
	// sample time is measured in whole seconds
	//

	// Factor /100.0 is hard. Should be the Hertz!!!!
	cpu_percent_ = ((( (double)process_active_time / 100.0) / sample_duration)*100.0)/(double)ncpu_;

	// remember process_time
	last_process_time_ = process_time;		
}

double toolbox::ProcessInfoImpl::sampleDuration ()
{
	return last_sample_duration_;
}
	
// ---------------------------------------------------------------
// API function implementations
// ---------------------------------------------------------------

bool toolbox::ProcessInfoImpl::isThreadExisting() 
{
	return false;
}

  
void toolbox::ProcessInfoImpl::sample()
{
	struct timeval sample_time;
	gettimeofday (&sample_time, 0);

	// compare to last sample time. Only sample if new sample
	// time is at least 1 microsend after the last sample time.
	double now = sample_time.tv_sec + (sample_time.tv_usec / 1000000.0);

    // If the current sample time - last sample time is smaller than 5 seconds
    // do not re-calculate. The values will not be accurate otherwise
    //
	if ((now - last_sample_time_) <= (double) minSampleTime)
	{
		// don't sample and don't update sample time, return
		return;
	}
	
	last_sample_duration_ = now-last_sample_time_;


	last_sample_time_ = now;
}
  
void toolbox::ProcessInfoImpl::show()
{
}


size_t toolbox::ProcessInfoImpl::vsize()
{
	return vsize_;
}

size_t toolbox::ProcessInfoImpl::rsize()
{
	return (rss_ * pageSize_);
}

std::string toolbox::ProcessInfoImpl::command()
{
	return command_;
}

pid_t toolbox::ProcessInfoImpl::pid()
{
	return thread_pid;
}


