// $Id: TaskGroup.cc,v 1.3 2008/07/18 15:27:41 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <iostream>
#include <unistd.h>

#include "toolbox/ArchTask.h"
#include "toolbox/Task.h"
#include "toolbox/TaskGroup.h"


toolbox::TaskGroup::TaskGroup()
{
  pthread_mutex_init (&join_condvar_lock, 0);
  pthread_mutex_init (&join_release_lock, 0);
  refCount = 0;
}


toolbox::TaskGroup::~TaskGroup()
{
  pthread_mutex_destroy(&join_condvar_lock);
  pthread_mutex_destroy(&join_release_lock);
}




void toolbox::TaskGroup::incRefCount()
{
 pthread_mutex_lock   (&join_condvar_lock);
 if (refCount == 0) {
   pthread_mutex_lock   (&join_release_lock);
 }
 refCount++;
 //cout << "incRefCount now: " << refCount << endl;
 pthread_mutex_unlock (&join_condvar_lock);
}


void toolbox::TaskGroup::decRefCount()
{
 pthread_mutex_lock   (&join_condvar_lock);
 refCount--;
 //cout << "decRefCount now: " << refCount << endl;
 if (refCount == 0) {
   // wakeup the join
   pthread_mutex_unlock (&join_release_lock);
 }
 pthread_mutex_unlock (&join_condvar_lock);
}


int toolbox::TaskGroup::join()
{ 
 //
 // now hang on the mutex until all threads are gone
 //
 //cout << "Hang on release lock" << endl;
 pthread_mutex_lock   (&join_release_lock);
 //cout << "Out of join" << endl;

 return 0;
}
