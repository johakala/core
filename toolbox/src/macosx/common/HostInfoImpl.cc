// $Id: HostInfoImpl.cc,v 1.3 2009/05/12 13:33:51 rmoser Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/
 
#include "toolbox/HostInfoImpl.h"



#include <sys/types.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <ios>
#include <iostream>
#include <fstream>
#include <sstream>

#include "toolbox/TimeVal.h"
#include "config/PackageInfo.h"

#ifndef CLK_TCK
#define CLK_TCK 100
#endif

toolbox::HostInfoImpl::HostInfoImpl() throw (toolbox::exception::Exception)
{

	// Initialize the sample timestamp
	struct timeval now;
	gettimeofday (&now, 0);
	
	last_sample_time_ = now.tv_sec + (now.tv_usec/1000000.0); // initialize sample time
	
	last_process_time_ = 0; // initialize last process CPU usage time

	// Get the number of processors;
	ncpu_ = 1;

	new_.resize(ncpu_+1);
	old_.resize(ncpu_+1);
	cpuUsage_.resize(ncpu_+1);
	for(size_t i=0 ; i<(ncpu_+1) ; i++)
	{
		new_[i].nice_        = 0;
		new_[i].user_        = 0;
		new_[i].system_      = 0;
		new_[i].idle_        = 0;
		new_[i].iowait_      = 0;
		new_[i].irqwait_     = 0;
		new_[i].softirqwait_ = 0;

		old_[i].nice_        = 0;
		old_[i].user_        = 0;
		old_[i].system_      = 0;
		old_[i].idle_        = 0;
		old_[i].iowait_      = 0;
		old_[i].irqwait_     = 0;
		old_[i].softirqwait_ = 0;

		cpuUsage_[i]         = 0.0; // initialize CPU usage;
	}

}

toolbox::HostInfoImpl::~HostInfoImpl() 
{
}

double toolbox::HostInfoImpl::getCpuUsage()
{
	return cpuUsage_[0];
}

double toolbox::HostInfoImpl::sampleDuration ()
{
	return last_sample_duration_;
}
	
// ---------------------------------------------------------------
// API function implementations
// ---------------------------------------------------------------

void toolbox::HostInfoImpl::sample()
{
}
  
void toolbox::HostInfoImpl::show()
{
	std::cout << "Host Statistics" << std::endl;
	for( size_t i = 0 ; i<old_.size() ; i++)
	{
		std::cout << "CPU usage " << cpuUsage_[i] <<std::endl;
		std::cout << "cpu(old): "
			<< new_[i].nice_ << " "
			<< new_[i].user_ << " "
			<< new_[i].system_ << " "
			<< new_[i].idle_ << " "
			<< new_[i].iowait_ << " "
			<< new_[i].irqwait_ << " "
			<< new_[i].softirqwait_ << std::endl;
		std::cout << "cpu(new): "
			<< old_[i].nice_ << " "
			<< old_[i].user_ << " "
			<< old_[i].system_ << " "
			<< old_[i].idle_ << " "
			<< old_[i].iowait_ << " "
			<< old_[i].irqwait_ << " "
			<< old_[i].softirqwait_ << std::endl;
	}
}

