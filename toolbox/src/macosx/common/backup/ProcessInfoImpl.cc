// $Id: ProcessInfoImpl.cc,v 1.8 2008/09/09 08:30:35 rmoser Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/
 
#include "toolbox/ProcessInfoImpl.h"


#include <linux/version.h>

//#include <unistd.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <sys/sysinfo.h>
#include <ios>
#include <iostream>
#include <fstream>
#include <sstream>

#include "toolbox/TimeVal.h"
#include "config/PackageInfo.h"

#ifndef CLK_TCK
#define CLK_TCK 100
#endif

toolbox::ProcessInfoImpl::ProcessInfoImpl(pid_t pid) throw (toolbox::exception::Exception)
{
	// Initialize the sample timestamp
	struct timeval now;
	gettimeofday (&now, 0);
	
	last_sample_time_ = now.tv_sec + (now.tv_usec/1000000.0); // initialize sample time
	
	last_process_time_ = 0; // initialize last process CPU usage time
	cpu_percent_ = 0.0; // initialize CPU usage;

    	thread_pid = pid;
	
	// Open the process stat file
    	
	// For Linux kerne l< 2.4.19
	// sprintf(proc_cpu_file,"/proc/%i/cpu",pid);
    	
	// For Linux kernel > 2.4.19
	std::sprintf(proc_cpu_file,"/proc/%i/stat",pid);
	cpu_info = new std::ifstream(proc_cpu_file);
    	if (! *cpu_info) 
	{ 
		std::string msg = "Cannot open pid file for process ";
		msg += proc_cpu_file;
		XCEPT_RAISE (toolbox::exception::Exception, msg);
	}
	
	// Set system pagesize
	pageSize_ = getpagesize(); 

	// Set the number of processors;
	ncpu_ = get_nprocs();
}

toolbox::ProcessInfoImpl::~ProcessInfoImpl() 
{
	// close process stat file
	cpu_info->close();
	delete cpu_info;
}
  
double toolbox::ProcessInfoImpl::cpuUsage()
{
	return cpu_percent_;
}

void toolbox::ProcessInfoImpl::calcCPUUsage (double sample_duration)
{	 
	// assume utime, stime already sampled
	// calculate process usage time in JIFFIE

	size_t process_time;
#if OS_VERSION_CODE < OS_VERSION(2,6,0)
    // In Linux 2.x, utime and stime give the consumed time per thread. To obtain the
    // process time, all child thread times of the main thread need to be traversed and
    // be summed up. As of Linux 2.4.21, group_utime and group_stime report the cumulative
    // consumed time of the thread group, i.e. the whole process.
    //
	process_time =  (group_utime_ + group_stime_);
#else
    // utime and stime take only the root thread into account. This one is sleeping, however and not consuming time
	process_time =  (utime_ + stime_);
#endif

	// calculate time in seconds the process was working
	// since the last sampling operation
	size_t process_active_time = process_time - last_process_time_;

	// Only calculate CPU usage if sample time is not 0
	// in practice this means that a cpu usage sample can be
	// done at most at a granularity of 1 second, since the
	// sample time is measured in whole seconds
	//

	// Factor /100.0 is hard. Should be the Hertz!!!!
	cpu_percent_ = ((( (double)process_active_time / 100.0) / sample_duration)*100.0)/(double)ncpu_;

	// remember process_time
	last_process_time_ = process_time;		
}

double toolbox::ProcessInfoImpl::sampleDuration ()
{
	return last_sample_duration_;
}
	
// ---------------------------------------------------------------
// API function implementations
// ---------------------------------------------------------------

bool toolbox::ProcessInfoImpl::isThreadExisting() 
{
	// check if the thread exists
	std::stringstream name;
	name << "/proc/" << thread_pid;
	if (access(name.str().c_str(), R_OK) != 0)
		return false;

	return true;
}

  
void toolbox::ProcessInfoImpl::sample()
{
	struct timeval sample_time;
	gettimeofday (&sample_time, 0);

	// compare to last sample time. Only sample if new sample
	// time is at least 1 microsend after the last sample time.
	double now = sample_time.tv_sec + (sample_time.tv_usec / 1000000.0);

    // If the current sample time - last sample time is smaller than 5 seconds
    // do not re-calculate. The values will not be accurate otherwise
    //
	if ((now - last_sample_time_) <= (double) minSampleTime)
	{
		// don't sample and don't update sample time, return
		return;
	}
	
	last_sample_duration_ = now-last_sample_time_;

	// Rewind to beginning of /proc/pid/stat file
	cpu_info->seekg(0,std::ios::beg);
	cpu_info->sync();

	// Values that can be retrieved from /proc/pid/stat:
	
	// pid %d
	pid_t pid;
	*cpu_info >> pid;

	// com %s
	*cpu_info >> command_;

	// state %c
	*cpu_info >> state_;

	// ppid %d
	*cpu_info >> ppid_;

	// pgrp %d
	*cpu_info >> pgrp_;

	// session %d
	*cpu_info >> session_;

	// tty %d
	*cpu_info >> tty_;

	// tpgid %d
	*cpu_info >> tpgid_;

	// flags %u
	*cpu_info >> flags_;

	// minflt %u
	*cpu_info >> minflt_;

	// cminflt %u
	*cpu_info >> cminflt_;

	// majflt %u
	*cpu_info >> majflt_;

	// cmajflt %u
	*cpu_info >> cmajflt_;

	// utime %d
	*cpu_info >> utime_;

	// stime %d
	*cpu_info >> stime_;

	// cutime %d
	*cpu_info >> cutime_;

	// cstime %d
	*cpu_info >> cstime_;

#if OS_VERSION_CODE < OS_VERSION(2,6,0)
	// counter %d
	*cpu_info >> counter_;
#endif

	// priority %d
	*cpu_info >> priority_;

#if OS_VERSION_CODE >= OS_VERSION(2,6,0)
	long nice;
	// nice %ld
	*cpu_info >> nice;
#endif

	// timeout %u
	*cpu_info >> timeout_;

	// itrealvalue %u
	*cpu_info >> itrealvalue_;

	// starttime %d
	*cpu_info >> starttime_;

	// vsize %u
	*cpu_info >> vsize_;

	// rss %u
	*cpu_info >> rss_;

	// rlim %u
	*cpu_info >> rlim_;

	// startcode %u
	*cpu_info >> startcode_;

	// endcode %u
	*cpu_info >> endcode_;

	// startstack %u
	*cpu_info >> startstack_;

	// kstkesp %u
	*cpu_info >> kstkesp_;

	// kstkeip %u
	*cpu_info >> kstkeip_;

	// signal %d
	*cpu_info >> signal_;

	// blocked %f
	*cpu_info >> blocked_;

	// sigignore %d
	*cpu_info >> sigignore_;

	// sigcatch %d
	*cpu_info >> sigcatch_;

	// wchan %u
	*cpu_info >> wchan_;

	// nsswap %lu
	*cpu_info >> nswap_;

	// cnsswap %lu
        *cpu_info >> cnswap_;

	// exit_signal %d
        *cpu_info >> exit_signal_;

#if OS_VERSION_CODE < OS_VERSION(2,6,0)
        *cpu_info >> task_;
        *cpu_info >> rt_priority_;
        *cpu_info >> policy_;
        *cpu_info >> group_utime_;
        *cpu_info >> group_stime_;
        *cpu_info >> group_cutime_;
        *cpu_info >> group_cstime_;
#else
	// processor %d
	size_t processor;
	*cpu_info >> processor;
#endif

	// calculate the processes CPU usage
	calcCPUUsage(last_sample_duration_);

	last_sample_time_ = now;
}
  
void toolbox::ProcessInfoImpl::show()
{
	std::cout << "Statistics for process " << thread_pid << ", command: " << command_ << std::endl;
	std::cout << "State: " << state_ << std::endl;  
	std::cout << "Parent pid: " << ppid_ << ", process group ID: " << pgrp_ << ", session ID: " << session_;
	std::cout << ", tty: " << tty_ << ", process group ID owning tty: " << tpgid_ << std::endl;

	std::cout << "flags: " << flags_ << std::endl;
	std::cout << "minor faults of process: " << minflt_ << ", process and children: " << cminflt_ << std::endl;
	std::cout << "major faults of process: " << majflt_ << ", process and children: " << cmajflt_ << std::endl;
	std::cout << "time of process in user mode: " << utime_ << ", in kernel mode: " << stime_ << std::endl;
	std::cout << "time of process and children in user mode: " << cutime_ << ", in kernel mode: " << cstime_ << std::endl;
	std::cout << "Remaining time in timeslice: " << counter_ << std::endl;
	std::cout << "Nice value plus fifteen: " << priority_ << std::endl;
	std::cout << "Time to next timeout: " << timeout_ << std::endl;
	std::cout << "Time before next SIGALRM: " << itrealvalue_ << std::endl;
	std::cout << "Process start time: " << starttime_ << std::endl;
	std::cout << "Virtual memory size: " << vsize_ << ", resident set size in pages: " << rss_ << std::endl;
	std::cout << "Resident size limit in pages: " << rlim_ << std::endl;
	std::cout << "Address for program text. start: " << startcode_ << ", end: " << endcode_ << std::endl;
	std::cout << "Stack start address: " << startstack_ << std::endl;
	std::cout << "Current stack address: " << kstkesp_ << std::endl;
	std::cout << "Current instruction pointer: " << kstkeip_ << std::endl;
	std::cout << "Signal bitmap. Pending: " << signal_;
	std::cout << " Blocked: " << blocked_;
	std::cout << " Ignored: " << sigignore_;
	std::cout << " Caught: " << sigcatch_ << std::endl;
	std::cout << "Waiting is system call: " << wchan_ << std::endl;
}


size_t toolbox::ProcessInfoImpl::vsize()
{
	return vsize_;
}

size_t toolbox::ProcessInfoImpl::rsize()
{
	return (rss_ * pageSize_);
}

std::string toolbox::ProcessInfoImpl::command()
{
	return command_;
}

pid_t toolbox::ProcessInfoImpl::pid()
{
	return thread_pid;
}


