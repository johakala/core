// $Id: TaskAttributes.cc,v 1.4 2008/07/18 15:27:42 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <iostream.h>
#include <unistd.h>

#include "TaskAttributes.h"


TaskAttributes::TaskAttributes()
{

}


TaskAttributes::~TaskAttributes()
{
 
}


int
TaskAttributes::stacksize(int size)
{
  return pthread_attr_setstacksize(&attr_,size);
}

int
TaskAttributes::stacksize()
{ 
  size_t stacksize;

#ifdef lynx
  return pthread_attr_getstacksize(&attr_);
#else
  return pthread_attr_getstacksize(&attr_, &stacksize);
#endif
  return stacksize;
}


int
TaskAttributes::priority(int level)
{
#ifdef lynx
  if (pthread_attr_setsched(&attr_, level) != 0) {
     // Never enter there => SIGSEGV or a SIGBUS signal 
#else
  struct sched_param priority;
  priority.sched_priority = level;
  
  if (pthread_attr_setschedparam(&attr_, &priority)!=0) {
#endif

    cout << "ERROR setting priority: " << strerror(errno) << endl;
    return 1;
  }
  return 0;
}



int
TaskAttributes::priority()
{
#ifdef lynx
  int level ;

  level = pthread_attr_getsched (&attr_) ;

  return (level) ;
#else
  struct sched_param priority;
  pthread_attr_getschedparam(&attr_, &priority);

  return priority.sched_priority;
#endif
}



int
TaskAttributes::name(char* name)
{
  name_ = strdup(name);
  return (int) name_;
}


char*
TaskAttributes::name()
{
  return name_;
}



