// $Id: ArchTaskAttributes.cc,v 1.3 2008/07/18 15:27:42 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <iostream.h>
#include <unistd.h>

#include "ArchTaskAttributes.h"


ArchTaskAttributes::ArchTaskAttributes()
{
#ifdef lynx
  pthread_attr_create(&attr_) ; /* initialize attr with default attributes */ 
#else
  pthread_attr_init(&attr_); /* initialize attr with default attributes */
#endif
}


ArchTaskAttributes::~ArchTaskAttributes()
{
// Both function pthread_attr_delete on lynx and 
// pthread_attr_destroy on linux don't do anything 
#ifdef lynx        /* Both function pthread_attr_delete and pthread_attr_destroy on linux don't do anything */ 
  pthread_attr_delete(&attr_);
#else
  pthread_attr_destroy(&attr_);
#endif
}




