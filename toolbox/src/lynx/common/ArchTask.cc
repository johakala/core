// $Id: ArchTask.cc,v 1.3 2008/07/18 15:27:42 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <iostream.h>
#include <unistd.h>

#include "Task.h"
#include "TaskAttributes.h"
#include "TaskGroup.h"


//
// Forward declaration
//
void* threadFunc(void* task);

//
// A wrapper to the POSIX pthread_create function.
// This function is a friend of TaskAttributes and Task
// so that it can access all necessary variables for
// creating a thread.
//



int task_spawn(TaskAttributes *attrib, Task* task)
{
#ifdef lynx
  if (pthread_create(&(task->tid_), 
		     (attrib->attr_), 
		     threadFunc, 
		     (void*) task) != 0) {
#else
  pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, 0);

  if (pthread_create(&(task->tid_), 
		     &(attrib->attr_), 
		     threadFunc, 
		     (void*) task) != 0) {
#endif

    cout << "PTHREAD_CREATE ERROR" << endl;
    perror(strerror(errno));
    return 1;
  }
  return 0;
}


ArchTask::ArchTask()
{
  pthread_mutex_init (&task_condvar_lock, 0);
  pthread_cond_init  (&task_condvar, 0);
  group_ = 0;
}

ArchTask::~ArchTask()
{
  pthread_mutex_destroy(&task_condvar_lock);
  pthread_cond_destroy (&task_condvar);
}

void ArchTask::initTaskGroup(TaskGroup* group)
{
  group_ = group;
}

void ArchTask::incRefCount()
{
 if (group_ != 0)
    group_->incRefCount();
}

void ArchTask::decRefCount()
{
  if (group_ != 0)
    group_->decRefCount();
}


