// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini                                    *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include <stdlib.h>
#include "toolbox/regex.h"

bool toolbox::regx_match_nocase( const std::string &stl_string, const std::string &pattern )
     throw( std::bad_alloc, std::invalid_argument )
{
   	return toolbox::regx_match( stl_string, pattern , REG_EXTENDED|REG_NOSUB|REG_ICASE);
}

bool toolbox::regx_match( const std::string &stl_string, const std::string &pattern )
     throw( std::bad_alloc, std::invalid_argument )
{
	return toolbox::regx_match( stl_string, pattern , REG_EXTENDED|REG_NOSUB);	
}
     
bool toolbox::regx_match( const std::string &stl_string, const std::string &pattern , int cflags)
     throw( std::bad_alloc, std::invalid_argument )
{
    regex_t regexp;
    int status = 0;
    char *err_msg = NULL;

    // Compile the regular expression pattern
    if( ( status = regcomp( &regexp, pattern.c_str(), cflags )) != 0 )
    {
        if( status == REG_ESPACE )
            throw std::bad_alloc();
        else
        {
            // Get the size of the message.
            int err_msg_sz = regerror( status, &regexp, NULL, (size_t) 0 );
            
            // Allocate the memory, print the message,
            // and then free the memory.
            if( ( err_msg = (char *) malloc( err_msg_sz ) ) != NULL )
            {
                regerror( status, &regexp, err_msg, err_msg_sz );
                std::string error = err_msg;
                free( err_msg );
                err_msg = NULL;
                error += "\nRegular expression = \"";
                error += pattern;
                error += "\"";
                throw std::invalid_argument( error.c_str() );
            }
            else
            {
                std::string invalid = "Invalid regular expression:  ";
                invalid += pattern;
                throw std::invalid_argument( invalid.c_str() );
            }
        }
    }

    // Search for the regular expression in the string
    status = regexec( &regexp, stl_string.c_str(), (size_t) 0, NULL, 0 );

    // Free the memory allocated by the regex.h functions.
    regfree( &regexp );
    if( status == REG_NOMATCH )
        return false;
    else if( status != 0 )
    {
        if( status == REG_ESPACE )
            throw std::bad_alloc();
        else
        {
            // Get the size of the message.
            int err_msg_sz = regerror( status, &regexp, NULL, (size_t) 0 );
            
            // Allocate the memory, print the message,
            // and then free the memory.
            if( ( err_msg = (char *) malloc( err_msg_sz ) ) != NULL )
            {
                regerror( status, &regexp, err_msg, err_msg_sz );
                std::string error = err_msg;
                free( err_msg );
                err_msg = NULL;
                error += "\nRegular expression = \"";
                error += pattern;
                error += "\"";
                throw std::invalid_argument( error.c_str() );
            }
            else
            {
                std::string invalid = "Invalid regular expression:  ";
                invalid += pattern;
                throw std::invalid_argument( invalid.c_str() );
            }
        }
    }

    return true;
}

bool toolbox::regx_match( const std::string &stl_string, const std::string &pattern,
                 std::vector<std::string> &sub_strings )
     throw( std::bad_alloc, std::invalid_argument )
{
    regex_t regexp;
    char *err_msg = NULL;
    int status = 0;
    int nsubs  = pattern.size() + 1;

    // Compile the regular expression and check for errors
    if( ( status = regcomp( &regexp, pattern.c_str(), REG_EXTENDED )) != 0 )
    {
        if( status == REG_ESPACE )
            throw std::bad_alloc();
        else
        {
            // Get the size of the message.
            int err_msg_sz = regerror( status, &regexp, NULL, (size_t) 0 );
            
            // Allocate the memory, print the message,
            // and then free the memory.
            if( ( err_msg = (char *) malloc( err_msg_sz ) ) != NULL )
            {
                regerror( status, &regexp, err_msg, err_msg_sz );
                std::string error = err_msg;
                free( err_msg );
                err_msg = NULL;
                error += "\nRegular expression = \"";
                error += pattern;
                error += "\"";
                throw std::invalid_argument( error.c_str() );
            }
            else
            {
                std::string invalid = "Invalid regular expression:  ";
                invalid += pattern;
                throw std::invalid_argument( invalid.c_str() );
            }
        }
    }

    if( nsubs > 0 )
    {
        // Allocate space to save the matching substring indices
        regmatch_t *subidx = new regmatch_t[nsubs * sizeof(regmatch_t)];
        if( subidx == NULL )
            nsubs = 0;
        else
        {
            // Allocate space to save the characters in the
            // matching substrings.
            char *substring = new char[stl_string.size() + 1];
            if( substring == NULL )
                nsubs = 0;
            else
            {
                // Perform the search and save the matching substring indices
                status = regexec( &regexp, stl_string.c_str(), (size_t) nsubs, subidx, 0 );
                sub_strings.clear();

                // If matched, save the substrings
                if( status == 0 )
                {
                    int i = 0;
                    for( i = 0; i < nsubs; i++ )
                    {
                        // Stop the substring saves when there are no more to save
                        if( subidx[i].rm_so == -1 || subidx[i].rm_eo == -1 )
                            break;

                        int j = 0;
                        int k = 0;
                        // Save the matching substrings to the allocated array
                        for( j = subidx[i].rm_so, k = 0; j < subidx[i].rm_eo; j++, k++ )
                            substring[k] = stl_string.at( j );
                        substring[k] = '\0';

                        // Save the matching substrings in the vector of strings
                        std::string str = substring;
                        sub_strings.push_back( str );
                    }
                }
                delete[] substring;
            }
            delete[] subidx;
        }
    }
    // If the memory allocation failed, perform the search
    // without saving the substrings.
    if( nsubs <= 0 )
        status = regexec( &regexp, stl_string.c_str(), (size_t) 0, NULL, 0 );

    // Free the memory allocated by the regex.h functions.
    regfree( &regexp );

    if( status == REG_NOMATCH )
        return false;

    if( status != 0 )
    {
        if( status == REG_ESPACE )
            throw std::bad_alloc();
        else
        {
            // Get the size of the message.
            int err_msg_sz = regerror( status, &regexp, NULL, (size_t) 0 );
            
            // Allocate the memory, print the message,
            // and then free the memory.
            if( ( err_msg = (char *) malloc( err_msg_sz ) ) != NULL )
            {
                regerror( status, &regexp, err_msg, err_msg_sz );
                std::string error = err_msg;
                free( err_msg );
                err_msg = NULL;
                error += "\nRegular expression = \"";
                error += pattern;
                error += "\"";
                throw std::invalid_argument( error.c_str() );
            }
            else
            {
                std::string invalid = "Invalid regular expression:  ";
                invalid += pattern;
                throw std::invalid_argument( invalid.c_str() );
            }
        }
    }

    return true;
}


