// $Id: TimeVal.cc,v 1.11 2008/07/18 15:27:38 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include <time.h>		
#include <stdio.h>		
#include <string.h>		

#include "toolbox/TimeVal.h"
#include<strings.h>
#include<sstream>
#include<iostream>
#include<iomanip>

//------------------------------------------------------------------------------
// Static definitions
//------------------------------------------------------------------------------

toolbox::TimeVal toolbox::TimeVal::m_zero;       // zero time
static const suseconds_t ONE_SECOND = 1000000;

//------------------------------------------------------------------------------
// Membe functions
//------------------------------------------------------------------------------

toolbox::TimeVal& toolbox::TimeVal::operator+=(const toolbox::TimeVal& rhs)
{
	tv_sec += rhs.tv_sec;
	tv_usec += rhs.tv_usec;

	if (tv_usec >= ONE_SECOND) {
		tv_usec -= ONE_SECOND;
		tv_sec++;
	} 
	else if (tv_sec >= 1 && tv_usec < 0) {
		tv_usec += ONE_SECOND;
		tv_sec--;
	}
	normalize ();
	return *this;
}

toolbox::TimeVal& toolbox::TimeVal::operator+=(const toolbox::TimeInterval& rhs)
{
	tv_sec += rhs.tv_sec;
	tv_usec += rhs.tv_usec;

	if (tv_usec >= ONE_SECOND) {
		tv_usec -= ONE_SECOND;
		tv_sec++;
	} 
	else if (tv_sec >= 1 && tv_usec < 0) {
		tv_usec += ONE_SECOND;
		tv_sec--;
	}
	normalize ();
	return *this;
}

toolbox::TimeVal& toolbox::TimeVal:: operator-=(const toolbox::TimeVal& rhs)
{
	tv_sec -= rhs.tv_sec;
	tv_usec -= rhs.tv_usec;

	if (tv_usec < 0) {
		tv_usec += ONE_SECOND;
		tv_sec--;
	} 
	else if (tv_usec >= ONE_SECOND) {
		tv_usec -= ONE_SECOND;
		tv_sec++;
	}
	normalize ();
	return *this;
}

toolbox::TimeVal& toolbox::TimeVal:: operator-=(const toolbox::TimeInterval& rhs)
{
	tv_sec -= rhs.tv_sec;
	tv_usec -= rhs.tv_usec;

	if (tv_usec < 0) {
		tv_usec += ONE_SECOND;
		tv_sec--;
	} 
	else if (tv_usec >= ONE_SECOND) {
		tv_usec -= ONE_SECOND;
		tv_sec++;
	}
	normalize ();
	return *this;
}

void toolbox::TimeVal::normalize ()
{
	if (tv_usec >= ONE_SECOND) {
		do {
			tv_sec++;
			tv_usec -= ONE_SECOND;
		}
		while (tv_usec >= ONE_SECOND);
	}
	else if (tv_usec <= -ONE_SECOND) {
		do {
			tv_sec--;
			tv_usec += ONE_SECOND;
		}
		while (tv_usec <= -ONE_SECOND);
	}

	if (tv_sec >= 1 && tv_usec < 0) {
		tv_sec--;
		tv_usec += ONE_SECOND;
	}
	else if (tv_sec < 0 && tv_usec > 0) {
		tv_sec++;
		tv_usec -= ONE_SECOND;
	}
}


std::string toolbox::TimeVal::toString (toolbox::TimeVal::TimeZone tz) const
{
	return this->toString("",tz);
}

std::string toolbox::TimeVal::toString (const std::string& fmt, toolbox::TimeVal::TimeZone tz) const
{
	struct tm ct;
	char buf[80];
	memset (buf, 0, 80);

	if (tz == loc)
		localtime_r ((const time_t*) &tv_sec, &ct );
	else
		gmtime_r ((const time_t*) &tv_sec,&ct );

	if (fmt == "") 
	{
		// Calculate time zon string in format [+|-]HH:MM
		//
		std::stringstream tzd;
		
		tzd << std::setw(4) << (1900 + ct.tm_year) << "-";
		tzd << std::setfill ('0') << std::setw(2) << (ct.tm_mon + 1) << "-";
		tzd << std::setfill ('0') << std::setw(2) << ct.tm_mday;
		tzd << "T";
		tzd << std::setfill ('0') << std::setw(2) << ct.tm_hour << ":";
		tzd << std::setfill ('0') << std::setw(2) << ct.tm_min << ":";
		tzd << std::setfill ('0') << std::setw(2) << ct.tm_sec;
		
		// Add factional usec
		tzd << "." << std::setfill ('0') << std::setw(6) << tv_usec;
		
		if (tz != gmt)
		{
			// timezone is an external variable set by tzset()
			//
			int timezonediff = timezone;
			if (ct.tm_isdst > 0)
			{
				timezonediff -= 3600; // another hour for summer time
			}
			
			/*
				timezone is set to the difference between UTC/GMT and local time,
				i.e. how much one needs to add or subtract to/from local time to
				get to UTC. The standard format expresses, however, the local time
				in HH:MM:SS and the timezone that is the offset TO UTC, e.g.:
				local: 10:00:00 and timezone -3600 means that one needs to minus 1 hour to
				come to UTC 09:00:00.
				The standard time format must be 10:00:00+01:00, telling that the current
				time is one hour added to UTC
			*/
			if (timezone >= 0)
			{
				tzd << '-';
				tzd << std::setfill ('0') << std::setw(2) << (timezonediff/3600) << ':';
				tzd << std::setfill ('0') << std::setw(2) << ((timezonediff%3600)/60);
			}
			else
			{
				tzd << '+';
				tzd << std::setfill ('0') << std::setw(2) << (-timezonediff/3600) << ':';
				tzd << std::setfill ('0') << std::setw(2) << ((-timezonediff%3600)/60);
			}
		}
		else tzd << "Z";
		return tzd.str();
		// strftime (buf, 80, "%Z %Y/%m/%d %H:%M:%S", &ct);
		
		// OLD
		//sprintf (buf + strlen(buf),
		//	 ".%03ld", (tv_usec %1000000)/1000);
	}
	else 
	{
		strftime(buf, 80, fmt.c_str(), &ct);
	}
	return std::string (buf);
}

void toolbox::TimeVal::fromString
(
	const std::string& s,
	const std::string& fmt,
	toolbox::TimeVal::TimeZone tz
)
	throw (toolbox::exception::Exception)
{
	struct tm ct;
	suseconds_t usec = 0;
	bzero(&ct, sizeof(struct tm));

	// the epoch!!!
	ct.tm_year = 1970-1900;
	ct.tm_mon = 0;
	ct.tm_mday = 1;
		
	if (fmt == "") 
	{
		// strptime (s.c_str(), "%Y/%m/%d %H:%M:%S", &ct);
		std::istringstream is;
		char blank;
        	is.str(s);
		is >> std::setw(4) >> ct.tm_year;
		
		if ((ct.tm_year < 1900) || (ct.tm_year > 2100))
		{
			std::string msg = "Invalid year in time value '";
			msg += s;
			msg += "'";
			XCEPT_RAISE (toolbox::exception::Exception, msg);
		}
		
		ct.tm_year -= 1900; // correct to relative since 1900
		is >> blank >> ct.tm_mon;
		
		if ((blank != '-') || (ct.tm_mon < 1 ) || (ct.tm_mon > 12))
		{
			std::string msg = "Invalid month in time value '";
			msg += s;
			msg += "'";
			XCEPT_RAISE (toolbox::exception::Exception, msg);
		}
		
		ct.tm_mon -= 1;
		
		is >> blank >> ct.tm_mday;
		
		if ((blank != '-') || (ct.tm_mday < 1 ) || (ct.tm_mday > 31))
		{
			std::string msg = "Invalid day in time value '";
			msg += s;
			msg += "'";
			XCEPT_RAISE (toolbox::exception::Exception, msg);
		}
		
		is >> blank;
		
		if (blank != 'T')
		{
			std::string msg = "Invalid 'T' separator in time value '";
			msg += s;
			msg += "'";
			XCEPT_RAISE (toolbox::exception::Exception, msg);
		}
		
		is >> ct.tm_hour;
		
		if ((ct.tm_hour < 0 ) || (ct.tm_hour > 24))
		{
			std::string msg = "Invalid hour in time value '";
			msg += s;
			msg += "'";
			XCEPT_RAISE (toolbox::exception::Exception, msg);
		}
		
		is >> blank;
		
		if (blank != ':')
		{
			std::string msg = "Invalid ':' separator in time value '";
			msg += s;
			msg += "'";
			XCEPT_RAISE (toolbox::exception::Exception, msg);
		}
		
		is >> ct.tm_min;
		
		if ((ct.tm_hour < 0 ) || (ct.tm_hour > 59))
		{
			std::string msg = "Invalid minute in time value '";
			msg += s;
			msg += "'";
			XCEPT_RAISE (toolbox::exception::Exception, msg);
		}
		
		is >> blank;
		
		if (blank != ':')
		{
			std::string msg = "Invalid ':' separator in time value '";
			msg += s;
			msg += "'";
			XCEPT_RAISE (toolbox::exception::Exception, msg);
		}
		
		is >> ct.tm_sec;
		
		if ((ct.tm_sec < 0 ) || (ct.tm_sec > 59))
		{
			std::string msg = "Invalid second in time value '";
			msg += s;
			msg += "'";
			XCEPT_RAISE (toolbox::exception::Exception, msg);
		}
		
		// Add factional usec
		is >> blank; // plus or minus symbol or "." if fractional second
		if (blank == '.')
		{
			is >> usec >> blank;
		}
		
		// correction to gmt always applied manually
		// internally always convert to UTC
		//
		if (blank == 'Z')
		{
			// nothing
		}
		else if (blank == '-')
		{
			// tells how many hours have been subtracted from UTC
			int diffHour, diffMinute;
			is >> diffHour;
			
			if ((diffHour < 0 ) || (diffHour > 59))
			{
				std::string msg = "Invalid time zone hour in time value '";
				msg += s;
				msg += "'";
				XCEPT_RAISE (toolbox::exception::Exception, msg);
			}
			
			is >> blank;
			
			if (blank != ':')
			{
				std::string msg = "Invalid ':' separator in time value '";
				msg += s;
				msg += "'";
				XCEPT_RAISE (toolbox::exception::Exception, msg);
			}
			
			is >> diffMinute;
			
			if ((diffMinute < 0 ) || (diffMinute > 59))
			{
				std::string msg = "Invalid time zone minute in time value '";
				msg += s;
				msg += "'";
				XCEPT_RAISE (toolbox::exception::Exception, msg);
			}
			
			ct.tm_hour += diffHour;
			ct.tm_min += diffMinute;
		}
		else if (blank == '+')
		{
			// tells how many hours have been added to UTC
			int diffHour, diffMinute;
			is >> diffHour;
			
			if ((diffHour < 0 ) || (diffHour > 59))
			{
				std::string msg = "Invalid time zone hour in time value '";
				msg += s;
				msg += "'";
				XCEPT_RAISE (toolbox::exception::Exception, msg);
			}
			
			is >> blank;
			
			if (blank != ':')
			{
				std::string msg = "Invalid ':' separator in time value '";
				msg += s;
				msg += "'";
				XCEPT_RAISE (toolbox::exception::Exception, msg);
			}
			
			is >> diffMinute;
			
			if ((diffMinute < 0 ) || (diffMinute > 59))
			{
				std::string msg = "Invalid time zone minute in time value '";
				msg += s;
				msg += "'";
				XCEPT_RAISE (toolbox::exception::Exception, msg);
			}
			
			ct.tm_hour -= diffHour;
			ct.tm_min -= diffMinute;			
		}
	}
	else 
	{
		// DO NOT USE %Z for time zone format, it won't work!!!!!
		//
		strptime(s.c_str(), fmt.c_str(), &ct);
	}

	time_t t;
	if (tz == loc)
	{
		ct.tm_isdst = -1; // let the computer find out if DST is applied or not
		
		// Convert into internal GMT format
		t = mktime(&ct); 
	}
	else
	{
		ct.tm_isdst = -1;
		t = timegm(&ct);
	}
	
	init (t,usec,gmt);
}



