// $Id: Chrono.cc,v 1.4 2008/07/18 15:27:38 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "toolbox/Chrono.h"

double toolbox::Chrono::tstamp()
{
  return((double)start_ts_.tv_sec + (double)start_ts_.tv_usec/
         (double)1000000.0);
}
suseconds_t toolbox::Chrono::dusecs() 
{
	return((suseconds_t)((dsecs_*1000000) + (suseconds_t)(dfsecs_)));
}
double toolbox::Chrono::dsecs() 
{
		return ((double)dsecs_ + (double)dfsecs_/(double)1000000.0);
}
void toolbox::Chrono::start(size_t stamp) 
{  
	gettimeofday (&start_ts_, 0);
	
		  //printf("Start: %d, %d Stamped:%d\n", start_ts_.tv_sec, start_ts_.tv_nsec, stamp); 
}

void toolbox::Chrono::stop(size_t stamp) 
{ 
		  //printf("Start: %d, %d\n", start_ts_.tv_sec, start_ts_.tv_nsec); 
		  //printf("Stop: %d, %d\n", stop_ts_.tv_sec, stop_ts_.tv_nsec); 
	gettimeofday (&stop_ts_, 0);
	
	
	
	dsecs_ = stop_ts_.tv_sec - start_ts_.tv_sec;
	dfsecs_ = stop_ts_.tv_usec - start_ts_.tv_usec;;
        if ( dfsecs_< 0 ) { dsecs_--; dfsecs_ = 1000000 +  dfsecs_;}
		   //printf("Delta: %d secs + %d nsecs Stamped:%d\n", dsecs_ , dnsecs_ ,stamp);
}
    
