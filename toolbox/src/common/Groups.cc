// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2018, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L. Orsini and D. Simelevicius                                *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include <stddef.h>
#include <cstdlib>
#include <errno.h>
#include <grp.h>
#include <sstream>
#include <string.h>

#include "toolbox/Groups.h"
#include "toolbox/exception/Exception.h"

void toolbox::setSupplementaryGroups(const struct passwd* pwd)
{
	if ( pwd == NULL )
		return;
	std::ostringstream oss;

	// Find out how many groups this user is member of by calling it with 0 max number of groups
	errno = 0;
	gid_t *groups = NULL;
	int ngroups = 0;
	getgrouplist( pwd->pw_name, pwd->pw_gid, groups, &ngroups );
	if ( ngroups == 0 ) return;
	// Call it again, this time with the correct ngroups
	groups = new gid_t[ngroups];
	errno = 0;
	getgrouplist( pwd->pw_name, pwd->pw_gid, groups, &ngroups );
	if ( errno != 0 )
	{
		delete[] groups;
		oss.str("");
		oss << "Failed to get the supplementary groups of user " << pwd->pw_name << " due to error (" << errno << ") in getgrouplist(): " << strerror(errno);
		XCEPT_RAISE ( toolbox::exception::Exception, oss.str() );
	}

	// List supplemenrtary groupss
	/*
	oss.str("");
	oss << "User " << pwd->pw_name << " is in groups { ";
	for ( int i=0; i<ngroups; ++i )
	{
		errno = 0;
		struct group *gr = getgrgid(groups[i]);
		if ( gr != NULL )
		{
			oss << gr->gr_name  << "(" << groups[i] << ") ";
		}
		else
		{
			oss << "UNKNOWN_GROUP_ID(" << groups[i] << ") ";
		}
	}
	oss << "}";
	std::cout << oss.str() << std::endl;
	*/

	// Set supplementary groups for the process
	errno = 0;
	if ( setgroups( ngroups, groups ) != 0 )
	{
		delete[] groups;
		oss.str("");
		oss << "Failed to set the supplementary groups of the process due to error (" << errno << ") in setgroups(): " << strerror(errno);
		XCEPT_RAISE ( toolbox::exception::Exception, oss.str() );
	}
	delete[] groups;
}
