// $Id: GaugeEvent.cc,v 1.2 2008/07/18 15:27:38 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and S. Murray					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "toolbox/GaugeEvent.h"

toolbox::GaugeEvent::GaugeEvent(const std::string& type, const std::string& name, void* originator):
	toolbox::Event(type, originator)
{
	name_ = name;
}


std::string toolbox::GaugeEvent::getName()
{
	return name_;
}


toolbox::GaugeAvailableEvent::GaugeAvailableEvent(const std::string& name, void* originator):
	toolbox::GaugeEvent("GaugeAvailableEvent", name, originator)
{
}

toolbox::GaugeRevokedEvent::GaugeRevokedEvent(const std::string& name, void* originator):
	toolbox::GaugeEvent("GaugeRevokedEvent", name, originator)
{
}

