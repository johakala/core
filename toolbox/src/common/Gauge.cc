// $Id: Gauge.cc,v 1.3 2008/07/18 15:27:38 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and S. Murray					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include <sstream>
#include "toolbox/Gauge.h"
#include "toolbox/ThresholdCrossedEvent.h"


toolbox::Gauge::Gauge(): mutex_(BSem::FULL)
{
	min_   = 0;
	max_   = 0;
	value_ = 0;
}


toolbox::Gauge::Gauge(const double min, const double max, const double value)
throw (toolbox::exception::Exception): mutex_(BSem::FULL)
{
	try
	{
		init(min, max, value);
	}
	catch(toolbox::exception::Exception &e)
	{
		XCEPT_RETHROW(toolbox::exception::Exception, "Failed to initialise", e);
	}
}


toolbox::Gauge::~Gauge()
{
}


void toolbox::Gauge::lock ()
{
	mutex_.take();
}


void toolbox::Gauge::unlock ()
{
	mutex_.give();
}


void toolbox::Gauge::addThreshold ( const std::string& name, const double v )
	throw (toolbox::exception::Exception)
{	
	std::map <double, std::string, std::less<double> >::iterator i;
	
	for (i = thresholds_.begin(); i != thresholds_.end(); i++)
	{
		if ( ((*i).second == name) && ((*i).first == v) )
		{
			std::stringstream msg;
               		msg << "Threshold " << name << " with value " << v << " already existing";
                	XCEPT_RAISE(toolbox::exception::Exception, msg.str());	
		}
	}

	if ((v > min_) && (v < max_))
	{
		thresholds_[v] = name;
	}
	else
	{
		std::stringstream msg;
               	msg << "Threshold " << name << " out of range (" << min_ << ", " << max_ << ")";
               	XCEPT_RAISE(toolbox::exception::Exception, msg.str());	
	}
}
			
void toolbox::Gauge::removeThreshold ( const std::string& name )
	throw (toolbox::exception::Exception)
{
	std::map <double, std::string, std::less<double> >::iterator i;
	
	for (i = thresholds_.begin(); i != thresholds_.end(); i++)
	{
		if ( (*i).second == name )
		{
			thresholds_.erase(i);
			return;
		}
	}

	std::stringstream msg;
        msg << "Threshold " << name << " not found";
        XCEPT_RAISE(toolbox::exception::Exception, msg.str());	
}
			
double toolbox::Gauge::getThreshold ( const std::string& name )
	throw (toolbox::exception::Exception)
{
	std::map <double, std::string, std::less<double> >::iterator i;
	
	for (i = thresholds_.begin(); i != thresholds_.end(); i++)
	{
		if ( (*i).second == name )
		{
			return (*i).first;
		}
	}

	std::stringstream msg;
        msg << "Threshold " << name << " not found";
        XCEPT_RAISE(toolbox::exception::Exception, msg.str());
}





bool toolbox::Gauge::isThresholdExceeded (const std::string& name)
throw (toolbox::exception::Exception)
{
	double t = this->getThreshold(name);
	
	if (value_ > t) 
	{
		return true;
	}
	else
	{
		return false;
	}
}


void toolbox::Gauge::init(const double min, const double max, const double value)
throw (toolbox::exception::Exception)
{
	if(min > max)
	{
		std::stringstream msg;
		msg << "Min:" << min << " is greater than max:" << max;
		XCEPT_RAISE(toolbox::exception::Exception, msg.str());
	}

	if((value < min) || (value > max))
	{
		std::stringstream msg;
		msg << "Initial value:" << value << " out of range (" << min << ", " << max << ")";
		XCEPT_RAISE(toolbox::exception::Exception, msg.str());
	}

	min_   = min;
	max_   = max;
	value_ = value;
}


double toolbox::Gauge::getMinValue()
{
	return min_;
}


double toolbox::Gauge::getMaxValue()
{
	return max_;
}


void toolbox::Gauge::incValue(const double inc)
throw (toolbox::exception::Exception)
{
	try
	{
		setValue(value_ + inc);
	}
	catch(toolbox::exception::Exception &e)
	{
		XCEPT_RETHROW(toolbox::exception::Exception, "Failed to set value", e);
	}
}


void toolbox::Gauge::decValue(const double dec)
throw (toolbox::exception::Exception)
{
	try
	{
        	setValue(value_ - dec);
	}
	catch(toolbox::exception::Exception &e)
	{
		XCEPT_RETHROW(toolbox::exception::Exception, "Failed to set value", e);
	}
}


void toolbox::Gauge::setValue(const double v)
throw (toolbox::exception::Exception)
{
	if (v == value_) return;

	if ((v >= min_) && (v <= max_))
	{
		double oldValue = value_;
		value_ = v;				
		bool up = (v > oldValue);
	
		if (up)
		{
			std::map <double, std::string, std::less<double> >::iterator i;
			for (i = thresholds_.begin(); i != thresholds_.end(); i++)
			{	
				// upwards: trigger if greater than old value and smaller than new one		
				if ( ((*i).first > oldValue) && ((*i).first < v ) )
				{
					toolbox::ThresholdCrossedUpEvent e((*i).second, oldValue, v);
					dispatcher_.fireEvent(e);	
				}
			}
		}
		else
		{
			std::map <double, std::string, std::less<double> >::reverse_iterator i;
			for (i = thresholds_.rbegin(); i != thresholds_.rend(); i++)
			{
				// downwards: trigger if smaller than old value and greater than new one
				if ( ((*i).first < oldValue) && ((*i).first > v ) )
				{
					toolbox::ThresholdCrossedDownEvent e((*i).second, oldValue, v);
					dispatcher_.fireEvent(e);	
				}
			}
		}
	}
	else
	{
		std::stringstream msg;
                msg << "New value outside minimum " << min_;
		msg << " and maximum " << max_ << " range";
                XCEPT_RAISE(toolbox::exception::Exception, msg.str());
	}
}


double toolbox::Gauge::getValue()
{
	return value_;
}


void toolbox::Gauge::addActionListener (toolbox::ActionListener* listener)
{
	dispatcher_.addActionListener(listener);
}

void toolbox::Gauge::removeActionListener (toolbox::ActionListener* listener)
throw (toolbox::exception::Exception)
{
	dispatcher_.removeActionListener(listener);
}
