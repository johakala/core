
#include "toolbox/fsm/FiniteStateMachine.h"
#include "toolbox/fsm/FailedEvent.h"
#include <map>
#include <string>
#include "toolbox/fsm/exception/Exception.h"


toolbox::fsm::FiniteStateMachine::FiniteStateMachine()
{
	this->addState('F', "failed");
	actionTable_['*']["fail"] = 0;
	currentState_ = '\0';
	initialState_ = '\0';
}


toolbox::fsm::FiniteStateMachine::~FiniteStateMachine()
{
	// delete all functors
	for (std::map< State, std::map<std::string, ActionSignature*, std::less<std::string> >, std::less<State> >::iterator i =  actionTable_.begin(); i != actionTable_.end(); i++)
	{
		for ( std::map<std::string, ActionSignature*, std::less<std::string> >::iterator j =  (*i).second.begin(); j != (*i).second.end(); j++)
		if ( (*j).second != 0 )
			delete (*j).second;
	}

	for (std::map< State, StateChangedSignature*, std::less<State> >::iterator k =  stateChangedTable_.begin(); k != stateChangedTable_.end(); k++ )
	{
		 if ( (*k).second != 0 )
                        delete (*k).second;

	}
	
}

void toolbox::fsm::FiniteStateMachine::addState(toolbox::fsm::State s, const std::string & name ) 
	throw (toolbox::fsm::exception::Exception)
{
	if ( states_.find(s) == states_.end() )
	{
		states_[s] = name;
		stateChangedTable_[s] =  0;
	}
	else
	{
		std::string msg = "State: ";
		msg += s;
		msg += "named: ";
		msg += states_[s];
		msg += " already existing";
		XCEPT_RAISE (toolbox::fsm::exception::Exception, msg); 

	}
}

toolbox::fsm::State toolbox::fsm::FiniteStateMachine::getInitialState() throw (toolbox::fsm::exception::Exception)
{
	return initialState_;
}

void toolbox::fsm::FiniteStateMachine::setInitialState(toolbox::fsm::State s) 
	throw (toolbox::fsm::exception::Exception)
{
	if ( states_.find(s) != states_.end() )
	{
		initialState_ = s;

	}
	else
	{
		std::string msg = "State: ";
		msg += s;
		msg += "undeclared ";
		XCEPT_RAISE (toolbox::fsm::exception::Exception, msg); 

	}
}

void toolbox::fsm::FiniteStateMachine::reset() 
	throw (toolbox::fsm::exception::Exception)
{
	if ( initialState_ != '\0' )
	{
		currentState_ = initialState_;
	}
	else
	{
		XCEPT_RAISE (toolbox::fsm::exception::Exception, "Initial state not declared"); 
	}
}

std::string toolbox::fsm::FiniteStateMachine::getStateName(toolbox::fsm::State s) throw (toolbox::fsm::exception::Exception)
{
	if ( states_.find(s) != states_.end() )
	{
		return states_[s];
	}
	else
	{
		std::string msg = "Cannot find name for state: ";
		msg += s;
		msg += ", unknwon";

		XCEPT_RAISE (toolbox::fsm::exception::Exception, msg); 
	}


}

void toolbox::fsm::FiniteStateMachine::setStateName(toolbox::fsm::State s, const std::string & name) throw (toolbox::fsm::exception::Exception)
{
	if ( states_.find(s) != states_.end() )
	{
		states_[s] = name;
	}
	else
	{
		std::string msg = "Cannot find name for state: ";
		msg += s;
		msg += ", unknwon";

		XCEPT_RAISE (toolbox::fsm::exception::Exception, msg); 
	}


}

std::vector<toolbox::fsm::State> toolbox::fsm::FiniteStateMachine::getStates()
{
	std::vector<toolbox::fsm::State> states;
	std::map< toolbox::fsm::State, std::string, std::less<toolbox::fsm::State> >::iterator i;

	for (i = states_.begin(); i != states_.end(); i++ )
	{
		states.push_back((*i).first);
	}

	return states;
}


std::set<std::string> toolbox::fsm::FiniteStateMachine::getInputs(toolbox::fsm::State s)
{
	std::set<std::string> inputs;
	if ( stateTransitionTable_.find(s) != stateTransitionTable_.end() )
	{
		std::map<std::string, toolbox::fsm::State, std::less<std::string> >::iterator i;
		for ( i = stateTransitionTable_[s].begin(); i != stateTransitionTable_[s].end(); i++ )
		{
			inputs.insert((*i).first);
		}

	}
	return inputs;
}


std::set<std::string> toolbox::fsm::FiniteStateMachine::getInputs()
{
	return inputs_;
}

toolbox::fsm::State toolbox::fsm::FiniteStateMachine::getCurrentState()
{
	return currentState_;

}

void toolbox::fsm::FiniteStateMachine::addStateTransition(toolbox::fsm::State from, 
								toolbox::fsm::State to, 
								const std::string& input) 
								throw (toolbox::fsm::exception::Exception)
{
	if (states_.find(from) == states_.end()) 
	{
		std::string msg = "'From' state: ";
		msg += from;
		msg += " undeclared ";
		XCEPT_RAISE (toolbox::fsm::exception::Exception, msg); 
	}	

	if (states_.find(to) == states_.end()) 
	{
		std::string msg = "'To' state: ";
		msg += to;
		msg += " undeclared ";
		XCEPT_RAISE (toolbox::fsm::exception::Exception, msg); 
	}	

	inputs_.insert(input);
	stateTransitionTable_[from][input] = to;
	actionTable_[from][input] = 0;
}

std::map<std::string, toolbox::fsm::State, std::less<std::string> > 
	toolbox::fsm::FiniteStateMachine::getTransitions(toolbox::fsm::State s)
throw (toolbox::fsm::exception::Exception)
{

	std::map<std::string, toolbox::fsm::State, std::less<std::string> > transitions;
	
	if (states_.find(s) == states_.end()) 
	{
		std::string msg = "State: ";
		msg += s;
		msg += " undeclared ";
		XCEPT_RAISE (toolbox::fsm::exception::Exception, msg); 
	}
	
	// check if any transition for from this state
	if (stateTransitionTable_.find(s) != stateTransitionTable_.end()) 
	{
		transitions = stateTransitionTable_[s];
 
	}
		
	return transitions;
	
	
}

void toolbox::fsm::FiniteStateMachine::fireEvent(toolbox::Event::Reference e) 
	throw (toolbox::fsm::exception::Exception)
{
	if (currentState_ == '\0')
	{
		XCEPT_RAISE (toolbox::fsm::exception::Exception, "State machine not initialized"); 
	}

	toolbox::fsm::State newState = stateTransitionTable_[currentState_][e->type()];
	if ( newState == 0 )
	{
		std::string msg = "Invalid input event: ";
		msg += e->type();
		msg += ", current state: ";
		msg += currentState_;
		XCEPT_RAISE (toolbox::fsm::exception::Exception, msg); 
	}
	
	if ( actionTable_[currentState_][e->type()] != 0 ) 
	{
		try
		{
			actionTable_[currentState_][e->type()]->invoke(e);
		} 
		catch (toolbox::fsm::exception::Exception& fsme)
		{
			if (actionTable_['*']["fail"] != 0)
			{
				// Eventually pass a FailedEvent that contains the exception and the original event
				toolbox::Event::Reference failedEvent(
					new toolbox::fsm::FailedEvent(fsme, currentState_, newState, "fail", this)
				);
				
				currentState_ = 'F';
				actionTable_['*']["fail"]->invoke(failedEvent);
				if ( stateChangedTable_['F'] != 0 )
					stateChangedTable_['F']->invoke(*this);
			}
			currentState_ = 'F';
			return;
		}
		catch (...)
		{
			if (actionTable_['*']["fail"] != 0)
			{
				// Eventually pass a FailedEvent that contains the exception and the original event
				xcept::Exception xe("xcept::Exception", "Caught unknown exception raised in user code", __FILE__, __LINE__, __FUNCTION__);
				toolbox::Event::Reference failedEvent(
					new toolbox::fsm::FailedEvent(xe, currentState_, newState, "fail", this)
				);
				
				currentState_ = 'F';
				actionTable_['*']["fail"]->invoke(failedEvent);
				if ( stateChangedTable_['F'] != 0 )
					stateChangedTable_['F']->invoke(*this);
			}
			currentState_ = 'F';
			return;	
		}
		/*
		catch (std::exception& ex)
		{
			if (actionTable_['*']["fail"] != 0)
			{
				// Eventually pass a FailedEvent that contains the exception and the original event
				xcept::Exception xe("xcept::Exception", ex.what(), __FILE__, __LINE__, __FUNCTION__);
				toolbox::Event::Reference failedEvent(
					new toolbox::fsm::FailedEvent(xe, currentState_, newState, "fail", this)
				);
				
				currentState_ = 'F';
				actionTable_['*']["fail"]->invoke(failedEvent);
				if ( stateChangedTable_['F'] != 0 )
					stateChangedTable_['F']->invoke(*this);
			}
			currentState_ = 'F';
			return;
		} 
		*/
	}	
	
	currentState_ = newState;
	
	if ( stateChangedTable_[newState] != 0 )
		stateChangedTable_[newState]->invoke(*this);
}

