// $Id: FailedEvent.cc,v 1.2 2008/07/18 15:27:39 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "toolbox/fsm/FailedEvent.h"

toolbox::fsm::FailedEvent::FailedEvent(xcept::Exception& ex, 
			toolbox::fsm::State fromState, 
			toolbox::fsm::State toState,
			const std::string & type, void* originator): toolbox::Event(type, originator)
{
	ex_ = ex;
	fromState_ = fromState;
	toState_ = toState;
}

xcept::Exception& toolbox::fsm::FailedEvent::getException()
{
	return ex_;
}

toolbox::fsm::State toolbox::fsm::FailedEvent::getFromState()
{
	return fromState_;
}

toolbox::fsm::State toolbox::fsm::FailedEvent::getToState()
{
	return toState_;
}

