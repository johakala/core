// $Id: AsynchronousFiniteStateMachine.cc,v 1.3 2008/07/18 15:27:39 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/
 
#include "toolbox/fsm/AsynchronousFiniteStateMachine.h"
#include "toolbox/fsm/FailedEvent.h"
#include "toolbox/fsm/InvalidInputEvent.h"
#include <map>
#include <string>
#include "toolbox/fsm/exception/Exception.h"
#include "toolbox/task/WorkLoopFactory.h"
#include "toolbox/task/WaitingWorkLoop.h"
#include "toolbox/task/Action.h"
#include "toolbox/BSem.h"

toolbox::fsm::AsynchronousFiniteStateMachine::AsynchronousFiniteStateMachine(const std::string& workLoopName)
	:eventQueueLock_(toolbox::BSem::FULL), stateMachineLock_(toolbox::BSem::FULL, true)
{

        eventQueue_ = toolbox::rlist<toolbox::Event::Reference*>::create(workLoopName + "-afsm-rlist");

	invalidInputAction_ = 0;

	processTransitionJob_ = toolbox::task::bind(this, &toolbox::fsm::AsynchronousFiniteStateMachine::processTransition, "processTransition");
		
	// Get a work loop
	workLoop_ = toolbox::task::getWorkLoopFactory()->getWorkLoop(workLoopName, "waiting");
	workLoop_->activate();
}


toolbox::fsm::AsynchronousFiniteStateMachine::~AsynchronousFiniteStateMachine()
{
	// delete all functors
	toolbox::rlist<toolbox::Event::Reference*>::destroy(eventQueue_);	
}



bool toolbox::fsm::AsynchronousFiniteStateMachine::processTransition(toolbox::task::WorkLoop* wl)
{
	stateMachineLock_.take();

	// Spurious jobs may still be in the queue, because the state machine has been reset
	// and all events have been cleared.
	if (eventQueue_->empty()) 
	{
		stateMachineLock_.give();
		return false;		
	}

	// Retrieve a queued event
	toolbox::Event::Reference* eventReference = eventQueue_->front();
	toolbox::Event::Reference e(*eventReference);
	delete eventReference;
	eventQueue_->pop_front();
	
	toolbox::fsm::State newState = stateTransitionTable_[currentState_][e->type()];
	if ( newState == 0 )
	{
		//std::string msg = "Invalid input event: ";
		//msg += e->type();
		//msg += ", current state: ";
		//msg += currentState_;
		//std::cout << msg << std::endl;

		try 
		{
			if ( invalidInputAction_ != 0 )
			{
 				toolbox::Event::Reference invalidInputEvent( new toolbox::fsm::InvalidInputEvent(currentState_, e->type(), this));
				invalidInputAction_->invoke(invalidInputEvent);
			}
			stateMachineLock_.give();
			return false;
		}
		catch (toolbox::fsm::exception::Exception& fsme)
		{
			if (actionTable_['*']["fail"] != 0)
			{
				// Eventually pass a FailedEvent that contains the exception and the original event
				toolbox::Event::Reference failedEvent(
					new toolbox::fsm::FailedEvent(fsme, currentState_, newState, "fail", this)
				);
				
				currentState_ = 'F';
				actionTable_['*']["fail"]->invoke(failedEvent);
				if ( stateChangedTable_['F'] != 0 )
					stateChangedTable_['F']->invoke(*this);
			}
			currentState_ = 'F';
			stateMachineLock_.give();
			return false; // go out of loop
		}
		catch (...)
		{
			if (actionTable_['*']["fail"] != 0)
			{
				// Eventually pass a FailedEvent that contains the exception and the original event
				xcept::Exception xe("xcept::Exception", "Caught unknown exception raised in user code", __FILE__, __LINE__, __FUNCTION__);
				toolbox::Event::Reference failedEvent(
					new toolbox::fsm::FailedEvent(xe, currentState_, newState, "fail", this)
				);
				
				currentState_ = 'F';
				actionTable_['*']["fail"]->invoke(failedEvent);
				if ( stateChangedTable_['F'] != 0 )
					stateChangedTable_['F']->invoke(*this);
			}
			currentState_ = 'F';
			stateMachineLock_.give();
			return false;	
		}
		
	}
	
	if ( actionTable_[currentState_][e->type()] != 0 ) 
	{
		try
		{
			actionTable_[currentState_][e->type()]->invoke(e);
		} 
		catch (toolbox::fsm::exception::Exception& fsme)
		{
			if (actionTable_['*']["fail"] != 0)
			{
				// Eventually pass a FailedEvent that contains the exception and the original event
				toolbox::Event::Reference failedEvent(
					new toolbox::fsm::FailedEvent(fsme, currentState_, newState, "fail", this)
				);
				
				currentState_ = 'F';
				actionTable_['*']["fail"]->invoke(failedEvent);
				if ( stateChangedTable_['F'] != 0 )
					stateChangedTable_['F']->invoke(*this);
			}
			currentState_ = 'F';
			stateMachineLock_.give();
			return false; // go out of loop
		}
		catch (...)
		{
			if (actionTable_['*']["fail"] != 0)
			{
				// Eventually pass a FailedEvent that contains the exception and the original event
				xcept::Exception xe("xcept::Exception", "Caught unknown exception raised in user code", __FILE__, __LINE__, __FUNCTION__);
				toolbox::Event::Reference failedEvent(
					new toolbox::fsm::FailedEvent(xe, currentState_, newState, "fail", this)
				);
				
				currentState_ = 'F';
				actionTable_['*']["fail"]->invoke(failedEvent);
				if ( stateChangedTable_['F'] != 0 )
					stateChangedTable_['F']->invoke(*this);
			}
			currentState_ = 'F';
			stateMachineLock_.give();
			return false;	
		}
	}	
	
	currentState_ = newState;
	
	if ( stateChangedTable_[newState] != 0 )
	{
		try
                {
			stateChangedTable_[newState]->invoke(*this);
		}
		catch (toolbox::fsm::exception::Exception& fsme)
                {
                        if (actionTable_['*']["fail"] != 0)
                        {
                                // Eventually pass a FailedEvent that contains the exception and the original event
                                toolbox::Event::Reference failedEvent(
                                        new toolbox::fsm::FailedEvent(fsme, currentState_, newState, "fail", this)
                                );

                                currentState_ = 'F';
                                actionTable_['*']["fail"]->invoke(failedEvent);
                                if ( stateChangedTable_['F'] != 0 )
                                        stateChangedTable_['F']->invoke(*this);
                        }
                        currentState_ = 'F';
                        stateMachineLock_.give();
                        return false; // go out of loop
                }
                catch (...)
                {
                        if (actionTable_['*']["fail"] != 0)
                        {
                                // Eventually pass a FailedEvent that contains the exception and the original event
                                xcept::Exception xe("xcept::Exception", "Caught unknown exception raised in user code", __FILE__, __LINE__, __FUNCTION__);
                                toolbox::Event::Reference failedEvent(
                                        new toolbox::fsm::FailedEvent(xe, currentState_, newState, "fail", this)
                                );

                                currentState_ = 'F';
                                actionTable_['*']["fail"]->invoke(failedEvent);
                                if ( stateChangedTable_['F'] != 0 )
                                        stateChangedTable_['F']->invoke(*this);
                        }
                        currentState_ = 'F';
                        stateMachineLock_.give();
                        return false;
                }

	}

	stateMachineLock_.give();
	return false;
}


void toolbox::fsm::AsynchronousFiniteStateMachine::reset() 
	throw (toolbox::fsm::exception::Exception)
{
	stateMachineLock_.take();
	if ( initialState_ != '\0' )
	{
		currentState_ = initialState_;
	}
	else
	{
		stateMachineLock_.give();
		XCEPT_RAISE (toolbox::fsm::exception::Exception, "Initial state not declared"); 
	}
	
	// clear event queue
	while (!eventQueue_->empty())
	{
		toolbox::Event::Reference* eventReference = eventQueue_->front();
		delete eventReference;
		eventQueue_->pop_front();
	}
	stateMachineLock_.give();
}

void toolbox::fsm::AsynchronousFiniteStateMachine::fireEvent(toolbox::Event::Reference e) 
	throw (toolbox::fsm::exception::Exception)
{
	eventQueueLock_.take();
	if (currentState_ == '\0')
	{
		eventQueueLock_.give();
		XCEPT_RAISE (toolbox::fsm::exception::Exception, "State machine not initialized"); 
	}

	try
	{	
		// enqueue the event
		eventQueue_->push_back(new toolbox::Event::Reference(e));
	}
	catch(toolbox::exception::RingListFull & e)
	{
		eventQueueLock_.give();
		XCEPT_RETHROW (toolbox::fsm::exception::Exception, "State machine not initialized", e); 
	}

	// submit a processing job for this event
	workLoop_->submit(processTransitionJob_);
	eventQueueLock_.give();
}
