// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2016, CERN.			                             *
 * All rights reserved.                                                  *
 * Authors: L. Orsini and D. Simelevicius				                 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                             *
 * For the list of contributors see CREDITS.   			                 *
 *************************************************************************/


#include "toolbox/TimeIntervalSA.h"
#include "toolbox/string.h"
#include <iostream>
#include <string>


toolbox::TimeIntervalSA::TimeIntervalSA(const std::string & in): lexicalAnalyzer_(in), seconds_(0)
{
}

toolbox::TimeIntervalSA::~TimeIntervalSA()
{
}

void toolbox::TimeIntervalSA::syntax()  throw (toolbox::exception::Exception)
{
	nextToken_ =  toolbox::TimeIntervalLA::EMPTY;
	nextToken_ = lexicalAnalyzer_.getNextToken();
	if (nextToken_ == toolbox::TimeIntervalLA::PSYMBOL )
	{
		duration();
	}
	else
	{
		XCEPT_RAISE(toolbox::exception::Exception,"syntax error, expecting P symbol");

	}

	if (nextToken_ != toolbox::TimeIntervalLA::EMPTY )
	{
		XCEPT_RAISE(toolbox::exception::Exception,"syntax error, spurious characters");
	}
}
size_t toolbox::TimeIntervalSA::getSeconds()
{
	return seconds_;
}

void  toolbox::TimeIntervalSA::duration()
{
	nextToken_ = lexicalAnalyzer_.getNextToken();

	if ( nextToken_ == toolbox::TimeIntervalLA::TSYMBOL)
	{
		this->timeexpr();
	}
	else if (nextToken_ == toolbox::TimeIntervalLA::INTVAL )
	{
		size_t value = toolbox::toLong(lexicalAnalyzer_.getLexeme());

		nextToken_ = lexicalAnalyzer_.getNextToken();
		if (nextToken_ == toolbox::TimeIntervalLA::YEARS )
		{
			seconds_ += value * 365 * 24 * 3600;
			years();
		}
		else if (nextToken_ == toolbox::TimeIntervalLA::MSYMBOL)
		{
			seconds_ += value * 30 * 24 * 3600;
			months();
		}
		else if (nextToken_ == toolbox::TimeIntervalLA::DAYS)
		{
			seconds_ += value * 24 * 3600;
			days();
		}
		else if (nextToken_ == toolbox::TimeIntervalLA::WEEKS)
		{
			seconds_ += value * 7 * 24 * 3600;
			weeks();
		}
		else
		{
			XCEPT_RAISE(toolbox::exception::Exception,"syntax error, expected either Y, M, W or D specifier");
		}

	}
	else
	{
		XCEPT_RAISE(toolbox::exception::Exception,"syntax error, expecting T or INT value ");
	}

}
// https://www.ietf.org/rfc/rfc3339.txt

//  tv_sec = seconds + (minutes*60) + (hours*3600) + (days*86400) + (months * 2592000) + (years*31536000);


void toolbox::TimeIntervalSA::seconds()
{
	nextToken_ = lexicalAnalyzer_.getNextToken();
}

void toolbox::TimeIntervalSA::weeks()
{
	nextToken_ = lexicalAnalyzer_.getNextToken();
}

void toolbox::TimeIntervalSA::timeexpr()
{
	nextToken_ = lexicalAnalyzer_.getNextToken();
	if (nextToken_ == toolbox::TimeIntervalLA::INTVAL )
	{
		size_t value = toolbox::toLong(lexicalAnalyzer_.getLexeme());

		nextToken_ = lexicalAnalyzer_.getNextToken();
		if (nextToken_ == toolbox::TimeIntervalLA::HOURS )
		{
			seconds_ += value * 3600;
			hours();
		}
		else if (nextToken_ == toolbox::TimeIntervalLA::MSYMBOL )
		{
			seconds_ += value * 60;
			minutes();
		}
		else if (nextToken_ == toolbox::TimeIntervalLA::SECONDS )
		{
			seconds_ += value;
			seconds();
		}
		else
		{
			XCEPT_RAISE(toolbox::exception::Exception,"syntax error, expected H or M or S specifier");

		}
	}
	else
	{
		XCEPT_RAISE(toolbox::exception::Exception,"syntax error, expected INT value  after T specifier");
	}
}
void toolbox::TimeIntervalSA::minutes()
{
	nextToken_ = lexicalAnalyzer_.getNextToken();
	if (nextToken_ == toolbox::TimeIntervalLA::INTVAL )
	{
		size_t value = toolbox::toLong(lexicalAnalyzer_.getLexeme());

		nextToken_ = lexicalAnalyzer_.getNextToken();
		if (nextToken_ == toolbox::TimeIntervalLA::SECONDS )
		{
			seconds_ += value;
			seconds();
		}
		else
		{
			XCEPT_RAISE(toolbox::exception::Exception,"syntax error, expected S specifier");
		}
	}
}

void toolbox::TimeIntervalSA::hours()
{
	 nextToken_ = lexicalAnalyzer_.getNextToken();
	if (nextToken_ == toolbox::TimeIntervalLA::INTVAL )
	{
		size_t value = toolbox::toLong(lexicalAnalyzer_.getLexeme());
		nextToken_ = lexicalAnalyzer_.getNextToken();
		if (nextToken_ == toolbox::TimeIntervalLA::MSYMBOL )
		{
			seconds_ += value * 60;
			minutes();
		}
		else
		{
			XCEPT_RAISE(toolbox::exception::Exception,"syntax error, expected minutes specifier");
		}
	}
}

void toolbox::TimeIntervalSA::days()
{
    nextToken_ = lexicalAnalyzer_.getNextToken();
	if (nextToken_ == toolbox::TimeIntervalLA::TSYMBOL )
	{
		timeexpr();
	}

}

void toolbox::TimeIntervalSA::months()
{
    nextToken_ = lexicalAnalyzer_.getNextToken();
    if (nextToken_ == toolbox::TimeIntervalLA::INTVAL )
    {
    	size_t value = toolbox::toLong(lexicalAnalyzer_.getLexeme());
    	nextToken_ = lexicalAnalyzer_.getNextToken();
    	if (nextToken_ == toolbox::TimeIntervalLA::DAYS )
    	{
    		seconds_ += value * 24 * 3600;
    		days();
    	}
    	else
    	{
    		XCEPT_RAISE(toolbox::exception::Exception,"syntax error, expected D (days) specifier");
    	}
    }
    else if (nextToken_ == toolbox::TimeIntervalLA::TSYMBOL )
	{
		timeexpr();
	}
}
void toolbox::TimeIntervalSA::years()
{
	nextToken_ = lexicalAnalyzer_.getNextToken();
	if (nextToken_ == toolbox::TimeIntervalLA::INTVAL )
	{
			size_t value = toolbox::toLong(lexicalAnalyzer_.getLexeme());
			nextToken_ = lexicalAnalyzer_.getNextToken();
			if (nextToken_ == toolbox::TimeIntervalLA::MSYMBOL )
			{
					seconds_ += value * 30 * 24 * 3600;
					months();
			}
			else
			{
				XCEPT_RAISE(toolbox::exception::Exception,"syntax error, expected M (month) specifier");
			}

	}
	else if ((nextToken_ == toolbox::TimeIntervalLA::TSYMBOL ))
	{
			timeexpr();
	}

}

