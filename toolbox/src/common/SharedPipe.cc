// $Id: shared_rlist.h,v 1.10 2008/07/18 15:27:32 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 		 *
 * All rights reserved.                                                  *
 * Authors: A. Petrucci, L. Orsini, C. Wakefield			 	 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			             *
 *************************************************************************/

#include "toolbox/SharedPipe.h"
#include <math.h>
#include "toolbox/hexdump.h"
#include "i2o/i2o.h"
#include "xcept/tools.h"


toolbox::SharedPipe::SharedPipe( const std::string & name )
{
	sqrtItemSize = (size_t) log2(PipeItemSize);

	try
	{
		queue_ = toolbox::sharedlist<ITEM, PipeQueueSize>::create(name);
	}
	catch ( toolbox::mem::exception::FailedCreation & e )
	{
		std::cerr << "Failed to create queue" << std::endl;
		std::cerr << xcept::stdformat_exception_history(e) << std::endl; 

	}
}

void toolbox::SharedPipe::read ( void * ptr, size_t size, size_t & len) throw (xdaq::exception::Exception)
{
	while ( queue_->empty() )
	{
			//std::cout << "-";
			::sched_yield();
	}
	toolbox::SharedPipe::ITEM* pipeItem = static_cast<toolbox::SharedPipe::ITEM *>( ptr );
	*pipeItem = queue_->front();

	//
	size_t blocks;
	PI2O_MESSAGE_FRAME frame = (PI2O_MESSAGE_FRAME)(ptr);

	size_t msgLen = frame->MessageSize << 2;

	if ( size < msgLen )
	{
		std::stringstream ss;
		ss << "Total size of blocks: " << msgLen << " exceeds size provided by buffer: " << size;
		XCEPT_RAISE(xdaq::exception::Exception, ss.str());
	}

	if ( msgLen % PipeItemSize == 0 )
	{
			blocks = msgLen >> sqrtItemSize;
	}
	else
	{
			blocks = (msgLen + PipeItemSize)  >> sqrtItemSize;
	}

	queue_->pop_front();
	pipeItem = pipeItem + 1;

	for ( size_t j = 1; j < blocks; j++ )
	{
			while ( queue_->empty() )
			{
				//std::cout << ".";
				::sched_yield();
			}
			//std::cout << "f1" << std::endl;

			*pipeItem = queue_->front();
			queue_->pop_front();

			pipeItem = pipeItem + 1;
	}

	len = msgLen;
}

void toolbox::SharedPipe::write ( void * ptr, size_t size ) throw (xdaq::exception::Exception)
{
	size_t blocks;

	//std::cout << "prompt size: " << size <<  std::endl;
	//char o;

	//std::cin >> o;

	if ( size % PipeItemSize == 0 )
	{
		blocks = size >> sqrtItemSize;
	}
	else
	{
		blocks = (size + PipeItemSize)  >> sqrtItemSize;
	}

	for ( size_t i = 0; i < blocks; i++ )
	{
		toolbox::SharedPipe::ITEM * item = static_cast<toolbox::SharedPipe::ITEM *>( ptr );


		while ( 1 )
		{
			try
			{
				//std::cout << "----------------" << std::endl;
				queue_->push_back(*item);
				break;
			}
			catch ( toolbox::exception::RingListFull & e )
			{
				::sched_yield();
				//std::cout << ".";
				continue;
			}
		}

		ptr = (char*) ptr + PipeItemSize;
	}
}

void toolbox::SharedPipe::reset()
{
	queue_->reset();
}
