// $Id: EventDispatcher.cc,v 1.4 2008/07/18 15:27:38 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "toolbox/EventDispatcher.h"
#include <algorithm>

toolbox::EventDispatcher::EventDispatcher()
{

}

void toolbox::EventDispatcher::addActionListener(  toolbox::ActionListener * l )
{
	actionListenerList_.push_back(l);
}


void toolbox::EventDispatcher::removeActionListener( toolbox::ActionListener * l ) throw (toolbox::exception::Exception)
{
	std::list<toolbox::ActionListener*>::iterator i;
	i = std::find( actionListenerList_.begin(), actionListenerList_.end(), l);
	if (i != actionListenerList_.end())
	{
		actionListenerList_.erase(i);
	} else
	{
		XCEPT_RAISE( toolbox::exception::Exception, "Could not remove listener. Object not found." );
	}
}


void toolbox::EventDispatcher::fireEvent( toolbox::Event & e )
{
	std::list<toolbox::ActionListener*>::iterator i;
	for ( i = actionListenerList_.begin(); i != actionListenerList_.end(); i++)
	{
		(*i)->actionPerformed(e);
	}
}

std::list<toolbox::ActionListener*> toolbox::EventDispatcher::getActionListeners()
{
	return actionListenerList_;
}
