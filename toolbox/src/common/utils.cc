// $Id: utils.cc,v 1.9 2008/07/18 15:27:38 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "toolbox/utils.h"
#include <time.h>
#include <string.h>
#include <string>
#include <sstream>
#include <iomanip>

#ifdef macosx
#include <unistd.h>
#endif

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>

//! visible only here in the toolbox module
static const char* DaysOfWeek [] = {"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"};				
static const char* MonthsOfYear [] = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};


std::string toolbox::chop_path(const std::string & pathname)
{
	const char* __pstr = pathname.c_str();
	const char* __pstrend = &__pstr[strlen(__pstr)];
	while ((*__pstrend != '/') && (__pstrend != __pstr)) __pstrend--;
	if (*__pstrend == '/') ++__pstrend;
	return __pstrend;
}


int toolbox::u_sleep (useconds_t useconds)
{
	usleep(useconds);
	return 0;
}


/*
int toolbox::u_sleep (useconds_t useconds)
{
	struct timespec tv, tr;
	tv.tv_sec = useconds / (time_t) 1000000;
	tv.tv_nsec = (useconds % 1000000) * 1000;

	tr.tv_sec = 0;
	tr.tv_nsec = 0;

	while (nanosleep ( &tv, &tr ) != 0) {
		if ((tr.tv_sec != 0) || (tr.tv_nsec != 0)) {
			tv = tr;
			tr.tv_sec = 0;
			tr.tv_nsec = 0;
		} else {
			return -1;
		}
	}
	return 0;
}
*/


std::string toolbox::getDateTime()
{
	time_t nTime = time( 0 );
	struct tm oTm ;
	gmtime_r (&nTime, &oTm);
	
	std::stringstream t;
	t.fill('0');
	t << std::right;
	t << DaysOfWeek[ oTm.tm_wday ] << ", ";
	t << std::setw(2) << oTm.tm_mday << " ";
	t << MonthsOfYear [ oTm.tm_mon ] << " ";
	t << std::setw(4) << oTm.tm_year + 1900 << " ";
	t << std::setw(2) <<  oTm.tm_hour << ":" << oTm.tm_min << ":" << oTm.tm_sec << " GMT";
	
	
	return t.str();
}

int toolbox::createFile (const std::string& filename, bool truncate)
	throw (toolbox::exception::Exception)
{
	struct stat lstatinfo;
        int fildes;

        /*
         * lstat() the file. If it doesn't exist, create it with O_EXCL.
         * If it does exist, open it for writing and perform the fstat()
         * check.
         */
        if (::lstat(filename.c_str(), &lstatinfo) < 0) 
	{
                /*
                 * If lstat() failed for any reason other than "file not
                 * existing", exit.
                 */
                if (errno != ENOENT) 
		{
			std::string msg = "Error checking file ";
			msg += filename;
			msg += ", ";
			msg += strerror(errno);
			
			XCEPT_RAISE (toolbox::exception::Exception, msg);
                }

                /*
                 * The file doesn't exist, so create it with O_EXCL to make
                 * sure an attacker can't slip in a file between the lstat()
                 * and open()
                 */
                if ((fildes =
                     ::open(filename.c_str(), O_RDWR | O_CREAT | O_EXCL, 0644)) < 0) 
		{
                        std::string msg = "Could not create file ";
			msg += filename;
			msg += ", ";
			msg += strerror(errno);
			
			XCEPT_RAISE (toolbox::exception::Exception, msg);
                }
        } 
	else 
	{
                struct stat fstatinfo;
                int flags;

                flags = O_RDWR;
                if (!truncate)
                        flags |= O_APPEND;

                /*
                 * Open an existing file.
                 */
                if ((fildes = ::open(filename.c_str(), flags)) < 0) 
		{
                        std::string msg = "Could not open file ";
			msg += filename;
			msg += ", ";
			msg += strerror(errno);
			
			XCEPT_RAISE (toolbox::exception::Exception, msg);
                }
	
		/*
                 * fstat() the opened file and check that the file mode bits,
                 * inode, and device match.
                 */
                if (::fstat(fildes, &fstatinfo) < 0
                    || lstatinfo.st_mode != fstatinfo.st_mode
                    || lstatinfo.st_ino != fstatinfo.st_ino
                    || lstatinfo.st_dev != fstatinfo.st_dev) 
		{
		
		 	std::string msg = "File ";
			msg += filename;
			msg += "has been changed before it could be opened, ";
			msg += strerror(errno);
			
			close(fildes);
			
			XCEPT_RAISE (toolbox::exception::Exception, msg);
                }

                /*
                 * If the above check was passed, we know that the lstat()
                 * and fstat() were done on the same file. Now we check that
                 * there's only one link, and that it's a normal file (this
                 * isn't strictly necessary because the fstat() vs lstat()
                 * st_mode check would also find this)
                 */
                if (fstatinfo.st_nlink > 1 || !S_ISREG(lstatinfo.st_mode)) 
		{
			std::string msg = "File ";
			msg += filename;
			msg += "has too many links, or is not a regular file, ";
			msg += strerror(errno);
			
			close(fildes);
			
			XCEPT_RAISE (toolbox::exception::Exception, msg);
                }

                /*
                 * Just return the file descriptor if we _don't_ want the file
                 * truncated.
                 */
                if (!truncate)
                        return fildes;

                /*
                 * On systems which don't support ftruncate() the best we can
                 * do is to close the file and reopen it in create mode, which
                 * unfortunately leads to a race condition, however "systems
                 * which don't support ftruncate()" is pretty much SCO only,
                 * and if you're using that you deserve what you get.
                 * ("Little sympathy has been extended")
                 */
#ifdef HAVE_FTRUNCATE
                ::ftruncate(fildes, 0);
#else
                ::close(fildes);
                if ((fildes =
                     ::open(filename.c_str(), O_RDWR | O_CREAT | O_TRUNC, 0644)) < 0) 
		{
			std::string msg = "Could not open file ";
			msg += filename;
			msg += ", ";
			msg += strerror(errno);
		
                        XCEPT_RAISE (toolbox::exception::Exception, msg);
                }
#endif                          /* HAVE_FTRUNCATE */
        }

        return fildes;
}
