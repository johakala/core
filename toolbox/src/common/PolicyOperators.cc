// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, D. Simelevicius					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "toolbox/PolicyOperators.h"
#include "toolbox/mem/exception/FailedCreation.h"

void* operator new  ( std::size_t count, toolbox::AllocPolicy * policy)
{
	try
	{
		size_t size = count + sizeof(size_t); 
		size_t *  buffer = (size_t*)policy->alloc(size);
		buffer[0] = size;
		return &(buffer[1]);
	}
	catch (toolbox::exception::Exception & e)
	{
		XCEPT_RETHROW (toolbox::mem::exception::FailedCreation, "Failed to allocate memory for rlist instance", e);
	}
}
	
void operator delete(void* ptr, toolbox::AllocPolicy * policy) throw()
{
	size_t * buffer = (size_t*)ptr;
	size_t size = buffer[-1];
	policy->free((void*)(&buffer[-1]), size);
}

void* operator new[]   ( size_t count, toolbox::AllocPolicy * policy ) 
{ 
         try
         {
		size_t size = count + sizeof(size_t);
                size_t *  buffer = (size_t*)policy->alloc(size);
                buffer[0] = size;
                return &(buffer[1]);
         }
         catch (toolbox::exception::Exception & e)
         {
                XCEPT_RETHROW (toolbox::mem::exception::FailedCreation, "Failed to allocate memory for rlist instance", e);
         }

}

void  operator delete[]( void* ptr , toolbox::AllocPolicy * policy)  throw() 
{ 
     	size_t * buffer = (size_t*)ptr;
      	size_t size = buffer[-1];
        policy->free((void*)(&buffer[-1]), size);

}
