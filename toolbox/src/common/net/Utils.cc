// $Id: Utils.cc,v 1.16 2008/12/10 14:16:30 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include <iostream>
#include <sstream>
#include <netdb.h>
#include <stdint.h>
#include "toolbox/net/Utils.h"
#include "toolbox/exception/Exception.h"
#include "toolbox/string.h"

#include "asyncresolv/resolver.h"

#include <sys/ioctl.h>
#include <net/if.h>

std::string toolbox::net::getHostName()
{
	char host_name[255];

	if (gethostname(host_name, 255) == 0)
	{
		// Extended resolution
		struct hostent *phe; // Host information entry
		phe = gethostbyname (host_name);
		if (phe)
		{
			return phe->h_name;
		}
		else
		{
			return host_name;
		}		
	} 
	else
	{
		return ("localhost");
	}
}

std::string toolbox::net::getDomainName()
{
	char domain_name[255];
	if (getdomainname(domain_name, 255) == 0)
	{
		return domain_name;
	} else
	{
		return ("");
	}
}

std::string toolbox::net::getDNSHostName(const std::string& name) 
	throw (toolbox::net::exception::Exception)
{
	std::string dnsname = "";
	
	AsyncDNSResolver * resolver = new AsyncDNSResolver();

	/* initialize resolver */
	if(resolver->initialize() == -1)
	{
		delete resolver;
		XCEPT_RAISE (toolbox::net::exception::Exception, "Failed to initialize DNS resolver");
	}
    
	/* set query type (recursive, status/normal/inverse query) */
	if(resolver->prepareQuery(RFC1035_OPCODE_QUERY, RFC1035_RESOLVE_RECURSIVE) == -1)
	{
		delete resolver;
		std::string msg = "Failed prepare query for DNS resolver for hostname ";
		msg += name;
		XCEPT_RAISE (toolbox::net::exception::Exception, msg);
	} 

	/* set query ( query name, resource record type, class) */
	if(resolver->addQuery(name.c_str(), RFC1035_TYPE_A, RFC1035_CLASS_IN) == -1)
	{
		delete resolver;
		std::string msg = "Failed to add query to DNS resolver for hostname ";
		msg += name;
		XCEPT_RAISE (toolbox::net::exception::Exception, msg);
	} 
    
	while(resolver->resolveQueries() == EAGAIN)
	{
		if(resolver->wait(10000) != 0)
		{
		    if(resolver->timedout() == -1)
		    {
        		break;
		    }
		}
	}

	const AsyncDNSReply * reply = resolver->getReply();

	if(reply)
	{        		
        	const RFC1035_RR * answer = 0;
        	int i = 0;
        	while((answer = reply->getAnswer(i++)) != 0)
		{
        	    if(answer->type == RFC1035_TYPE_A)
		    {
		    	dnsname = answer->name;
    		    }
		    else
		    {
           		delete resolver;
		    	std::string msg = "Failed to interpret answer from DNS resolver for hostname ";
			msg += name;
			XCEPT_RAISE (toolbox::net::exception::Exception, msg);
		    }

        	}
    	}
    	else 
	{
       		delete resolver;
		std::string msg = "Failed to query DNS resolver for hostname ";
		msg += name;
		XCEPT_RAISE (toolbox::net::exception::Exception, msg);
    	}

    	resolver->cleanup(); //must be called after each call to resolveQueries

	delete resolver;
	return dnsname;
}


/*!
 *   Returns the current the host address as a 4-byte value in
 *   network order.  Returns 0x7f000001 (loopback) if sockets are not
 *   supported or there was an error getting the current host IP address.
 *   If there are several IP addresses on the system, returns one arbitrary
 *   address.
 */
/*
unsigned long toolbox::net::getHostAddr (const std::string & hostname)
{
#if (defined (HAS_SOCKETS))
    struct hostent *phe; // Host information entry

    phe = gethostbyname (hostname.c_str());
    if (phe)
        return (*(unsigned long *) (phe-> h_addr_list [0]));
    else
        return (htonl (SOCKET_LOOPBACK));
#else
    return (htonl (SOCKET_LOOPBACK));
#endif
}
*/

/*!
 *   Returns the current the host address as a 4-byte value in
 *   network order.  Returns 0x7f000001 (loopback) if sockets are not 
 *   supported or there was an error getting the current host IP address.
 *   If there are several IP addresses on the system, returns one arbitrary 
 *   address.
 */
/*
unsigned long toolbox::net::getHostAddr ()
{
#if (defined (HAS_SOCKETS))
    struct hostent *phe; // Host information entry

    phe = gethostbyname (toolbox::net::getHostName().c_str());
    if (phe)
        return (*(unsigned long *) (phe-> h_addr_list [0]));
    else
        return (htonl (SOCKET_LOOPBACK));
#else
    return (htonl (SOCKET_LOOPBACK));
#endif
}
*/

struct sockaddr_in toolbox::net::getSocketAddressByName(const std::string & hostname, uint16_t port) 
	throw (toolbox::net::exception::UnresolvedAddress )
{
	struct hostent *hp = 0;
	struct sockaddr_in addr;
	int retries = 3; // hard set retry 3 times
	
	while (retries > 0)
	{
		h_errno = 0;
		hp = ::gethostbyname(hostname.c_str());
	        if ( hp == 0 )
		{	
			if (h_errno == TRY_AGAIN)
			{
				--retries; // retry query after a second
				::sleep(1);
			}
			else 
			{
				std::stringstream msg;
				msg << "Failed to resolve host '" << hostname << "', " << hstrerror(h_errno);
				XCEPT_RAISE(toolbox::net::exception::UnresolvedAddress, msg.str());	
			}		
		}
		else
		{
			struct in_addr **pptr = (struct in_addr **) hp->h_addr_list;
		
			// Take the first address in the list and return it
			for ( ; *pptr != 0; pptr++)
			{
				int len = sizeof(addr);
				bzero ((char*) &addr, len);
				memcpy ( &addr.sin_addr, *pptr, sizeof (struct in_addr) );
				addr.sin_family = AF_INET;
				
				// port must be in network byte order!
				addr.sin_port = htons(port);
				
				return addr;			
			}
		}
	}
	
	XCEPT_RAISE(toolbox::net::exception::UnresolvedAddress,toolbox::toString("Cannot determine socket address for (retry failed): %s",hostname.c_str()));
	return addr; // makes the compiler happy

}

/***************************************************************/
/* CRC16 calculation code				       */
/***************************************************************/
/******************************************************************************/
/* Declaration of a CRC Lookup Table. The polynom 1 + X2 + X15 +X16 determines*/
/* the table.                                                                 */
/******************************************************************************/
const uint16_t CRC16_table[256] =
{
 0x0000, 0x8005, 0x800F, 0x000A, 0x801B, 0x001E, 0x0014, 0x8011,
 0x8033, 0x0036, 0x003C, 0x8039, 0x0028, 0x802D, 0x8027, 0x0022,
 0x8063, 0x0066, 0x006C, 0x8069, 0x0078, 0x807D, 0x8077, 0x0072,
 0x0050, 0x8055, 0x805F, 0x005A, 0x804B, 0x004E, 0x0044, 0x8041,
 0x80C3, 0x00C6, 0x00CC, 0x80C9, 0x00D8, 0x80DD, 0x80D7, 0x00D2,
 0x00F0, 0x80F5, 0x80FF, 0x00FA, 0x80EB, 0x00EE, 0x00E4, 0x80E1,
 0x00A0, 0x80A5, 0x80AF, 0x00AA, 0x80BB, 0x00BE, 0x00B4, 0x80B1,
 0x8093, 0x0096, 0x009C, 0x8099, 0x0088, 0x808D, 0x8087, 0x0082,
 0x8183, 0x0186, 0x018C, 0x8189, 0x0198, 0x819D, 0x8197, 0x0192,
 0x01B0, 0x81B5, 0x81BF, 0x01BA, 0x81AB, 0x01AE, 0x01A4, 0x81A1,
 0x01E0, 0x81E5, 0x81EF, 0x01EA, 0x81FB, 0x01FE, 0x01F4, 0x81F1,
 0x81D3, 0x01D6, 0x01DC, 0x81D9, 0x01C8, 0x81CD, 0x81C7, 0x01C2,
 0x0140, 0x8145, 0x814F, 0x014A, 0x815B, 0x015E, 0x0154, 0x8151,
 0x8173, 0x0176, 0x017C, 0x8179, 0x0168, 0x816D, 0x8167, 0x0162,
 0x8123, 0x0126, 0x012C, 0x8129, 0x0138, 0x813D, 0x8137, 0x0132,
 0x0110, 0x8115, 0x811F, 0x011A, 0x810B, 0x010E, 0x0104, 0x8101,
 0x8303, 0x0306, 0x030C, 0x8309, 0x0318, 0x831D, 0x8317, 0x0312,
 0x0330, 0x8335, 0x833F, 0x033A, 0x832B, 0x032E, 0x0324, 0x8321,
 0x0360, 0x8365, 0x836F, 0x036A, 0x837B, 0x037E, 0x0374, 0x8371,
 0x8353, 0x0356, 0x035C, 0x8359, 0x0348, 0x834D, 0x8347, 0x0342,
 0x03C0, 0x83C5, 0x83CF, 0x03CA, 0x83DB, 0x03DE, 0x03D4, 0x83D1,
 0x83F3, 0x03F6, 0x03FC, 0x83F9, 0x03E8, 0x83ED, 0x83E7, 0x03E2,
 0x83A3, 0x03A6, 0x03AC, 0x83A9, 0x03B8, 0x83BD, 0x83B7, 0x03B2,
 0x0390, 0x8395, 0x839F, 0x039A, 0x838B, 0x038E, 0x0384, 0x8381,
 0x0280, 0x8285, 0x828F, 0x028A, 0x829B, 0x029E, 0x0294, 0x8291,
 0x82B3, 0x02B6, 0x02BC, 0x82B9, 0x02A8, 0x82AD, 0x82A7, 0x02A2,
 0x82E3, 0x02E6, 0x02EC, 0x82E9, 0x02F8, 0x82FD, 0x82F7, 0x02F2,
 0x02D0, 0x82D5, 0x82DF, 0x02DA, 0x82CB, 0x02CE, 0x02C4, 0x82C1,
 0x8243, 0x0246, 0x024C, 0x8249, 0x0258, 0x825D, 0x8257, 0x0252,
 0x0270, 0x8275, 0x827F, 0x027A, 0x826B, 0x026E, 0x0264, 0x8261,
 0x0220, 0x8225, 0x822F, 0x022A, 0x823B, 0x023E, 0x0234, 0x8231,
 0x8213, 0x0216, 0x021C, 0x8219, 0x0208, 0x820D, 0x8207, 0x0202
};

uint16_t toolbox::net::recalculateCRC16 (uint16_t crc, unsigned char byte)
{
	return (CRC16_table[((crc >> 8) ^ byte) & 0xFF] ^ (crc << 8));
}

uint16_t toolbox::net::calculateCRC16 (unsigned char *buffer, size_t length)
{
	//Local declarations
	uint16_t  crc = 0xFFFF;
  	uint16_t  newCrc = 0;
	size_t i;

	for(i = 0; i < length; i++) 
	{
    		newCrc = recalculateCRC16(crc, buffer[i]);
    		crc = newCrc;
  	}

	return crc;
}

/*
 * Find the name of the interface that the hostname uses to communicate. If no interface can be found, return "Unknown"
 */
std::string toolbox::net::getInterface(const std::string & hostname) throw (toolbox::net::exception::UnresolvedAddress, toolbox::net::exception::Exception)
{
	struct sockaddr_in hostaddr;
	bzero(&hostaddr,sizeof(hostaddr));
	hostaddr = toolbox::net::getSocketAddressByName(hostname, 0);

	struct ifaddrs *addrs, *iap;
	struct sockaddr_in *sa;
	char buf[32];

	int err = getifaddrs(&addrs);

	if (err != 0)
	{
		XCEPT_RAISE(toolbox::net::exception::Exception, toolbox::toString("Failed to find network interfaces on local machine: ERRNO = ", strerror(errno)));
	}

	std::string interfaceName = "Unknown";

	for (iap = addrs; iap != NULL; iap = iap->ifa_next)
	{
		if (iap->ifa_addr == NULL)
		{
			continue;
		}

		if (iap->ifa_addr && (iap->ifa_flags & IFF_UP) && iap->ifa_addr->sa_family == AF_INET)
		{
			sa = (struct sockaddr_in *)(iap->ifa_addr);
			inet_ntop(iap->ifa_addr->sa_family, (void *)&(sa->sin_addr), buf, sizeof(buf));

			if (!strcmp(inet_ntoa(hostaddr.sin_addr), buf))
			{
				interfaceName = iap->ifa_name;
				break;
			}
		}
	}
	freeifaddrs(addrs);

	return interfaceName;
}

/*
 * Return the netmask used by the given interface.
 * To do a bitwise &, use the s_addr field within the in_addr struct
 */
struct in_addr toolbox::net::getNetMaskByInterface(const std::string & interfaceName) throw (toolbox::net::exception::Exception)
{
	int fd;
	struct ifreq ifr;

	fd = socket(AF_INET, SOCK_DGRAM, 0);

	ifr.ifr_addr.sa_family = AF_INET;

	strncpy(ifr.ifr_name, interfaceName.c_str(), IFNAMSIZ-1);

	int err = ioctl(fd, SIOCGIFNETMASK, &ifr);
	if (err != 0)
	{
		std::stringstream ss;
		ss << "Failed to find subnet mask for interface " << interfaceName << " : ERRNO = " << strerror(errno);
		XCEPT_RAISE(toolbox::net::exception::Exception, ss.str());
	}

	close(fd);

	return ((struct sockaddr_in *)&ifr.ifr_addr)->sin_addr;
}
