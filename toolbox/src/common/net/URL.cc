// $Id: URL.cc,v 1.19 2008/07/18 15:27:40 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include <iostream>
#include <sstream>
#include <netdb.h>
#include <arpa/inet.h>

#include "toolbox/net/URL.h"
#include "toolbox/net/Utils.h"
#include "toolbox/string.h"
#include "toolbox/net/exception/MalformedURL.h"
#include "toolbox/net/exception/UnresolvedAddress.h"


toolbox::net::URL::URL(const std::string& protocol,
	 const std::string& hostname,
	 unsigned int portno, const std::string& spec) throw (toolbox::net::exception::MalformedURL)
    : d_protocol(protocol),
      d_portno(portno), d_spec(spec) 
{
    d_hostname = toolbox::tolower(hostname);
}    

toolbox::net::URL::URL(const std::string& str) throw (toolbox::net::exception::MalformedURL)
{
    // Find :// if that doesn't work, try to find :/, otherwise give up
    std::string::size_type s=str.find(":/");
    if(s == std::string::npos)
    {
		std::string msg = "Malformed URL, could not find ':/' in ";
		msg += str;
		XCEPT_RAISE( toolbox::net::exception::MalformedURL, msg);
    }
    
    // See, if there is another '/' after the first one
    // if so, let the hostname part start after that.
    std::string rest = str.substr(s+3);
    
    if (str[s+2] != '/')
    {
    	// file :/ spec
	d_protocol=str.substr(0, s);
	
	// For now treat file URL with absoulte pathnames, e.g.:
	// file:/tmp/pippo will give for the filename /tmp/pippo
	d_spec = str.substr(s+1);
	return;
    } 
    
    d_protocol = str.substr(0, s);

    s  =rest.find(":");
    
    if(s == std::string::npos)
    {
	s = rest.find("/");
	if(s == std::string::npos)
	{
	    d_hostname=toolbox::tolower(rest);
	    d_portno=0;
	    d_spec="";
	} 
	else 
	{
	    d_hostname=toolbox::tolower(rest.substr(0, s));
	    d_spec=rest.substr(s+1);
	}
    } 
    else 
    {
	d_hostname = toolbox::tolower(rest.substr(0, s));
	rest = rest.substr(s+1);
	std::istringstream i(rest);
	i >> d_portno;
	if(!i)
	{
		std::string msg = "Malformed URL, could not parse port number in ";
		msg += str;
		XCEPT_RAISE( toolbox::net::exception::MalformedURL, msg);
	}
	s = rest.find("/");
	if(s == std::string::npos)
	{
		s = rest.find("#");
		if(s == std::string::npos)
		{
		 	d_spec="";
		}
		else
		{
			d_spec=rest.substr(s);
		}
		
	} 
	else 
	{
	    d_spec=rest.substr(s+1);
	}
    }
}

toolbox::net::URL::~URL()
{
}

std::string toolbox::net::URL::toString() const
{
    std::ostringstream o;
    
    if (d_protocol != "file")
    {
    	o << d_protocol << "://" << d_hostname;
    } 
    else 
    {
	// file URL start with '/', so no need to add the slash
    	o << d_protocol << ":" << d_spec;
	return o.str();
    }
    
    if(d_portno > 0)
	o << ":" << d_portno;
    if(d_spec.length() > 0 && d_spec[0] != '/')
	o << '/';
    o << d_spec;
    return o.str();
}

std::string toolbox::net::URL::getProtocol() const
{
    return d_protocol;
}

std::string toolbox::net::URL::getScheme() const
{
    return d_protocol;
}

std::string toolbox::net::URL::getHost() const
{
    return d_hostname;
}

std::string toolbox::net::URL::getAuthority() const
{
	std::stringstream authority;
	authority << d_hostname;

	if(d_portno > 0)
		authority << ":" << d_portno;
	
	return authority.str();
}

unsigned int toolbox::net::URL::getPort() const
{
    return d_portno;
}

std::string toolbox::net::URL::getPath() const
{
 	std::string::size_type s = d_spec.find("?");
	std::string fragment;
	if(s == std::string::npos)
	{
		s = d_spec.find("#");
		if(s == std::string::npos)
		{
		 	 return d_spec;
		}
		else
		{
			return d_spec.substr(0,s);
		}
	}
	else
	{
		return d_spec.substr(0,s);
	}
}

std::string toolbox::net::URL::getFragment() const
{
	std::string::size_type s = d_spec.find("#");
	if(s == std::string::npos)
	{
		return "";
	}
	return d_spec.substr(s+1);	
}

std::string toolbox::net::URL::getQuery() const
{
	std::string::size_type q = d_spec.find("?");
	if(q == std::string::npos)
	{
		return "";
	}
	else
	{
		std::string::size_type h = d_spec.find("#");
		if(h == std::string::npos)
		{
		 	 return d_spec.substr(q+1);
		}
		else
		{
			return d_spec.substr(q+1,h-q-1);
		}
	}	
}

/*
long URL::getIP() throw (toolbox::exception::Exception)
{
  struct hostent *he;
  if((he=gethostbyname(d_hostname.c_str())) == NULL)
  {
  	string msg = "invalid hostname: ";
	msg += d_hostname;
    throw toolbox::exception::Exception (msg);
  }
  return *((long*)he->h_addr);
}
*/
toolbox::net::URL::URL(const URL& ucopy)
  : d_protocol(ucopy.d_protocol),
    d_hostname(ucopy.d_hostname),
    d_portno(ucopy.d_portno),
    d_spec(ucopy.d_spec)
{
}


std::string toolbox::net::URL::getNormalizedURL() 
	throw (toolbox::net::exception::BadURL)
{
	try
	{
		std::string url = d_protocol;
 		url += "://";
		struct sockaddr_in sa = toolbox::net::getSocketAddressByName(d_hostname, d_portno);
 		url += inet_ntoa(sa.sin_addr);
		url += ":";
		url += toolbox::toString("%d", d_portno);
 		return url;
	}
	catch (toolbox::net::exception::UnresolvedAddress& e)
	{
		XCEPT_RETHROW (toolbox::net::exception::BadURL, "Cannot resolve host given in url", e);
	}
}
