// $Id: URN.cc,v 1.5 2008/07/18 15:27:40 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "toolbox/net/URN.h"

toolbox::net::URN::URN (std::string spec) throw (toolbox::net::exception::MalformedURN)
{
	urn_ = spec;
	
	// first grab the "urn:" part
	if ( urn_.find ("urn:", 0) == std::string::npos)
	{
		XCEPT_RAISE (toolbox::net::exception::MalformedURN, "URN does not start with \"urn:\"");
	}
	else
	{
		// Now the namespace
		std::string::size_type nid_pos = urn_.find(':', 4);
		if (nid_pos == std::string::npos)
		{
			XCEPT_RAISE (toolbox::net::exception::MalformedURN, "Missing namespace in URN");
		}

		namespace_  = urn_.substr(4, nid_pos -4 );
		if (namespace_ == "")
		{
			XCEPT_RAISE (toolbox::net::exception::MalformedURN, "Missing namespace");
		}

		reference_ = urn_.substr(nid_pos  + 1, urn_.size() - nid_pos);
		if (reference_ == "")
		{
			XCEPT_RAISE (toolbox::net::exception::MalformedURN, "Missing reference");
		}
	}
}

  
toolbox::net::URN::URN(std::string nid, std::string nss) throw (toolbox::net::exception::MalformedURN)
{
	if((nid == "") || (nss == ""))
	{
		XCEPT_RAISE (toolbox::net::exception::MalformedURN, "Null portion of URN supplied");
	}

	namespace_ = nid;
	reference_ = nss;

	urn_ = "urn:" + nid;
	urn_ +=':';
	urn_ += nss;
}


std::string toolbox::net::URN::getNamespace()
{
	return namespace_;
}


std::string toolbox::net::URN::getNSS()
{
	return reference_;
}


toolbox::net::URL toolbox::net::URN::getURL() throw (toolbox::exception::UnsupportedService)
{
	/*
	URNResolverService service =
	RDSManager.getFirstResolver(namespace, URIResolverService.I2L);

	if(service == null)
	throw new UnsupportedServiceException("No URL resolution is available");

	return (URL)service.decode(this, URIResolverService.I2L);
	*/
	XCEPT_RAISE (toolbox::exception::UnsupportedService, "URL retrieval from URN not implemented.");
}


std::vector<toolbox::net::URL> toolbox::net::URN::getURLList() throw (toolbox::exception::UnsupportedService)
{
	/*
	Enumeration services =
	RDSManager.getAllResolvers(namespace, URIResolverService.I2Ls);

	int i;
	boolean list_supported = (services != null);
	URNResolverService resolver;
	Object[] urls;
	LinkedList found_resources = new LinkedList();
	HashMap res_map = new HashMap();

	// Loop through all of the returned services and see if they can
	// resolve this particular URN. If they can then this is added to the
	// list of items. If not, then it is ignored and the next call made.
	while(services.hasMoreElements())
	{
	resolver = (URNResolverService)services.nextElement();

	try
	{
	urls = resolver.decodeList(this, URIResolverService.I2Ls);
	if(urls != null)
	{
	  // Add all of the found resources. We don't want to duplicate
	  // the outputs of two resolvers giving the same resource, so
	  // we check them before adding it to the list.
	  //
	  // This works nicely for URIs, but ResourceConnections and URCs
	  // are a bit of an unknown quantity at the moment.
	  for(int j = 0; j < urls.length; j++)
	  {
	    if(!res_map.containsKey(urls[j].toString()))
	    {
	      found_resources.add(urls[j]);
	      res_map.put(urls[j].toString(), urls[j]);
	    }
	  }
	}
	}
	catch(UnsupportedServiceException use)
	{
	// ignore and move on.
	}

	}

	// Now check to see if we can add extra using the single resource.
	services = RDSManager.getAllResolvers(namespace, URIResolverService.I2L);

	if((services == null) && !list_supported)
	throw new UnsupportedServiceException("URL lists are not available");

	Object a_url;

	// This time we're looking through the list of single resolvers available
	while(services.hasMoreElements())
	{
	resolver = (URNResolverService)services.nextElement();

	try
	{
	a_url = resolver.decode(this, URIResolverService.I2L);
	if((a_url != null) && !res_map.containsKey(a_url.toString()))
	{
	  found_resources.add(a_url);
	  res_map.put(a_url.toString(), a_url);
	}
	}
	catch(UnsupportedServiceException use)
	{
	// ignore and move on.
	}
	}

	// Now this is all done, just turn the linked list into an array and
	// return the items.
	URL[] ret_val = new URL[found_resources.size()];
	found_resources.toArray(ret_val);

	return ret_val;
	*/

	XCEPT_RAISE (toolbox::exception::UnsupportedService, "URL retrieval from URN not implemented.");
}


toolbox::net::URC toolbox::net::URN::getURC() throw (toolbox::exception::UnsupportedService)
{
	/*
	URNResolverService service =
	RDSManager.getFirstResolver(namespace, URIResolverService.I2C);

	if(service == null)
	throw new UnsupportedServiceException("No URL resolution is available");

	return (URC)service.decode(this, URIResolverService.I2C);
	*/

	XCEPT_RAISE (toolbox::exception::UnsupportedService, "URC retrieval from URN not implemented.");
}


std::vector<toolbox::net::URC> toolbox::net::URN::getURCList() throw (toolbox::exception::UnsupportedService)
{
	/*
	Enumeration services =
	RDSManager.getAllResolvers(namespace, URIResolverService.I2Cs);

	int i;
	boolean list_supported = (services != null);
	URNResolverService resolver;
	Object[] urcs;
	LinkedList found_resources = new LinkedList();

	// Loop through all of the returned services and see if they can
	// resolve this particular URN. If they can then this is added to the
	// list of items. If not, then it is ignored and the next call made.
	while(services.hasMoreElements())
	{
	resolver = (URNResolverService)services.nextElement();

	try
	{
	urcs = resolver.decodeList(this, URIResolverService.I2Cs);

	if(urcs != null)
	{
	  // GRRRR. Need to write an ArrayCollection to make this easy.
	  for(int j = 0; j < urcs.length; j++)
	    found_resources.add(urcs[j]);
	}
	}
	catch(UnsupportedServiceException use)
	{
	// ignore and move on.
	}
	}

	// Now check to see if we can add extra using the single resource.
	services = RDSManager.getAllResolvers(namespace, URIResolverService.I2C);

	if((services == null) && !list_supported)
	throw new UnsupportedServiceException("URC lists are not available");

	Object a_urc;

	// This time we're looking through the list of single resolvers available
	while(services.hasMoreElements())
	{
	resolver = (URNResolverService)services.nextElement();

	try
	{
	a_urc = resolver.decode(this, URIResolverService.I2C);
	if((a_urc != null) && !found_resources.contains(a_urc))
	  found_resources.add(a_urc);
	}
	catch(UnsupportedServiceException use)
	{
	// ignore and move on.
	}
	}

	// Now this is all done, just turn the linked list into an array and
	// return the items.
	System.out.println("found total " + found_resources.size());

	URC[] ret_val = new URC[found_resources.size()];
	found_resources.toArray(ret_val);

	return ret_val;
	*/

	XCEPT_RAISE (toolbox::exception::UnsupportedService, "URC retrieval from URN not implemented.");
}




std::vector< toolbox::net::URN> toolbox::net::URN::getURNList() throw (toolbox::exception::UnsupportedService)
{
	/*
	Enumeration services =
	RDSManager.getAllResolvers(namespace, URIResolverService.I2Ns);

	int i;
	boolean list_supported = (services != null);
	URNResolverService resolver;
	Object[] urns;
	LinkedList found_resources = new LinkedList();

	// Loop through all of the returned services and see if they can
	// resolve this particular URN. If they can then this is added to the
	// list of items. If not, then it is ignored and the next call made.
	while(services.hasMoreElements())
	{
	resolver = (URNResolverService)services.nextElement();

	try
	{
	urns = resolver.decodeList(this, URIResolverService.I2Ns);
	if(urns != null)
	{
	  // GRRRR. Need to write an ArrayCollection to make this easy.
	  for(int j = 0; j < urns.length; j++)
	    found_resources.add(urns[j]);
	}
	}
	catch(UnsupportedServiceException use)
	{
	// ignore and move on.
	}
	}

	// Now check to see if we can add extra using the single resource.
	services = RDSManager.getAllResolvers(namespace, URIResolverService.I2N);

	if((services == null) && !list_supported)
	throw new UnsupportedServiceException("URN lists are not available");

	Object a_urn;

	// This time we're looking through the list of single resolvers available
	while(services.hasMoreElements())
	{
	resolver = (URNResolverService)services.nextElement();

	try
	{
	a_urn = resolver.decode(this, URIResolverService.I2N);
	if(a_urn != null)
	  found_resources.add(a_urn);
	}
	catch(UnsupportedServiceException use)
	{
	// ignore and move on.
	}
	}

	// Now this is all done, just turn the linked list into an array and
	// return the items.
	URN[] ret_val = new URN[found_resources.size()];
	found_resources.toArray(ret_val);

	return ret_val;
	*/

	XCEPT_RAISE (toolbox::exception::UnsupportedService, "URC retrieval from URN not implemented.");
}


/*
public ResourceConnection getResource()
throws UnsupportedServiceException, IOException
{
URNResolverService service =
RDSManager.getFirstResolver(namespace, URIResolverService.I2R);

if(service == null)
throw new UnsupportedServiceException("No URL resolution is available");

return (ResourceConnection)service.decode(this, URIResolverService.I2R);
}
*/


/*
public ResourceConnection[] getResourceList()
throws UnsupportedServiceException, IOException
{
Enumeration services =
RDSManager.getAllResolvers(namespace, URIResolverService.I2Rs);

int i;
boolean list_supported = (services != null);
URNResolverService resolver;
Object[] res_list;
LinkedList found_resources = new LinkedList();

// Loop through all of the returned services and see if they can
// resolve this particular URN. If they can then this is added to the
// list of items. If not, then it is ignored and the next call made.
while(services.hasMoreElements())
{
resolver = (URNResolverService)services.nextElement();

try
{
res_list = resolver.decodeList(this, URIResolverService.I2Rs);
if(res_list != null)
{
  // GRRRR. Need to write an ArrayCollection to make this easy.
  for(int j = 0; j < res_list.length; j++)
    found_resources.add(res_list[j]);
}
}
catch(UnsupportedServiceException use)
{
// ignore and move on.
}
}

// Now check to see if we can add extra using the single resource.
services = RDSManager.getAllResolvers(namespace, URIResolverService.I2R);

if((services == null) && !list_supported)
throw new UnsupportedServiceException("Resource lists are not available");

Object a_res;

// This time we're looking through the list of single resolvers available
while(services.hasMoreElements())
{
resolver = (URNResolverService)services.nextElement();

try
{
a_res = resolver.decode(this, URIResolverService.I2R);
if(a_res != null)
  found_resources.add(a_res);
}
catch(UnsupportedServiceException use)
{
// ignore and move on.
}
}

// Now this is all done, just turn the linked list into an array and
// return the items.
ResourceConnection[] ret_val =
new ResourceConnection[found_resources.size()];

found_resources.toArray(ret_val);

return ret_val;
}
*/


std::string toolbox::net::URN::toString() const
{
	return urn_;
}


bool toolbox::net::URN::equals(const URN& urn)
{
	return (urn_ == urn.toString());
}


bool toolbox::net::URN::equals(const toolbox::net::URI& uri) throw (toolbox::exception::UnsupportedService)
{
	// resolve URN of both and compare them
	//
	XCEPT_RAISE (toolbox::exception::UnsupportedService, "URN/URI comparison not implemented.");
}
