// $Id: UUID.cc,v 1.8 2008/07/18 15:27:41 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include <string.h>
#include <stdarg.h>
#include <sstream>

#include "toolbox/net/UUID.h"

/*  standard constructor */
toolbox::net::UUID::UUID() throw (toolbox::net::exception::Exception)
{
   uuid_generate(id_);
}

/*  copy constructor */
toolbox::net::UUID::UUID(const UUID &obj) throw (toolbox::net::exception::Exception)
{
    uuid_copy(id_,obj.id_);
}


/*  extra constructor via string representation */
toolbox::net::UUID::UUID(const std::string & value) throw (toolbox::net::exception::Exception)
{
	int status = uuid_parse(value.c_str(),id_);
	if (status < 0 )
	{
		std::stringstream msg;
		msg << "failed to parse uuid '" << value << "'";
		XCEPT_RAISE(toolbox::net::exception::Exception, msg.str());
	}
}

toolbox::net::UUID::UUID(uuid_t & id)
{
	uuid_copy(id_, id);
}

/*  standard destructor */
toolbox::net::UUID::~UUID()
{
    uuid_clear(id_);
    return;
}

/*  assignment operator: import of other C++ API object */
toolbox::net::UUID & toolbox::net::UUID::operator=(const UUID &obj) throw (toolbox::net::exception::Exception)
{
    uuid_copy(id_,obj.id_);
    return *this;
}

/*  method: comparison for Nil UUID */
bool toolbox::net::UUID::isnil(void) const throw (toolbox::net::exception::Exception) 
{
   return (uuid_is_null(id_) > 0);

}

/*  method: comparison against other object */
int toolbox::net::UUID::compare(const UUID &obj) const throw (toolbox::net::exception::Exception) 
{
    return uuid_compare(id_,obj.id_);
}

/*  method: comparison for equality */
int toolbox::net::UUID::operator==(const UUID &obj) const
{
    return (compare(obj) == 0);
}

/*  method: comparison for inequality */
int toolbox::net::UUID::operator!=(const UUID &obj) const
{
    return (compare(obj) != 0);
}

/*  method: comparison for lower-than */
int toolbox::net::UUID::operator<(const UUID &obj) const
{
    return (compare(obj) < 0);
}

/*  method: comparison for lower-than-or-equal */
int toolbox::net::UUID::operator<=(const UUID &obj) const
{
    return (compare(obj) <= 0);
}

/*  method: comparison for greater-than */
int toolbox::net::UUID::operator>(const UUID &obj) const
{
    return (compare(obj) > 0);
}

/*  method: comparison for greater-than-or-equal */
int toolbox::net::UUID::operator>=(const UUID &obj) const
{
    return (compare(obj) >= 0);
}

std::string toolbox::net::UUID::toString(void) const throw (toolbox::net::exception::Exception)
{
        char uuidstr[37]; // 36 + \0
	
	uuid_unparse(id_,uuidstr);
	return uuidstr;
}

std::ostream& toolbox::net::operator <<(std::ostream& s, const toolbox::net::UUID& uuid)
{
	char uuidstr[37]; // 36 + \0
	uuid_unparse(uuid.id_,uuidstr);
	return s << uuidstr;
}

