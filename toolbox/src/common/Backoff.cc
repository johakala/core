// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2016, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, D. Simelevicius                                   *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <limits.h>
#include <sys/time.h>
#include <unistd.h>
#include <iostream>

#include "toolbox/Backoff.h"

toolbox::Backoff::Backoff(unsigned int seed, BackoffType backoffType): random_(seed), randomDist_(0, 1), backoffType_(backoffType)
{
}

void toolbox::Backoff::backoff(unsigned int n, unsigned int slotTime)
{
	//sleeptime in microseconds
	unsigned int sleepTime;

	//pick a random sleep time
	if ( backoffType_ == Fibonacci )
		sleepTime = randomDist_(random_, 0, this->fibonacci(n) * slotTime * 1000);
	else
		sleepTime = randomDist_(random_, 0, this->power(2, n) * slotTime * 1000);

	::usleep(sleepTime);
}

unsigned int toolbox::Backoff::power(unsigned int base, unsigned int exp)
{
	unsigned int p;
	if ( exp == 0 )
		return 1;

	for(p = base; exp > 1; exp--)
		p *= base;

	return p;
}

unsigned int toolbox::Backoff::fibonacci(unsigned int n)
{
	if ( n == 0 )
		return 0;
	else if ( n <= 2 ) {
		return 1;
	}

	unsigned int a = 1;
	unsigned int b = 1;
	unsigned int c;
	for (unsigned int i = 0; i < n - 2; ++i) {
		c = a + b;
		b = a;
		a = c;
	}
	return a;
}

