// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2013, CERN.			                 		 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, A. Petrucci, C.Wakefield					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         	 *
 *************************************************************************/

#include <numa.h>
#include "toolbox/string.h"
#include "toolbox/utils.h"
#include "toolbox/POSIXAllocPolicy.h"

toolbox::POSIXAllocPolicy::POSIXAllocPolicy()
{
}

toolbox::POSIXAllocPolicy::~POSIXAllocPolicy()
{
}

std::string toolbox::POSIXAllocPolicy::getPackage()
{
	return "posix";
}

void* toolbox::POSIXAllocPolicy::alloc (size_t size) throw (toolbox::exception::Exception)
{
	void * memAlloc = ::calloc(1,size);

	if ( memAlloc == NULL )
       	{
                std::stringstream ss;
                ss << "POSIX allocation failed due to insufficient memory";
                XCEPT_RAISE(toolbox::exception::Exception, ss.str());
       	}

	return memAlloc;
}

void toolbox::POSIXAllocPolicy::free (void* buffer, size_t size) throw (toolbox::exception::Exception)
{
	::free(buffer);
}

