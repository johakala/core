// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini and D. Simelevicius					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "toolbox/conversion.h"
#include "toolbox/string.h"

#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <iostream>
#include <iomanip>
#include <string>
#include <stdio.h>
#include <exception>
#include <typeinfo>
#include <errno.h>
#include <algorithm>
#include <limits.h>

long toolbox::toLongConversion (const std::string& n) throw (toolbox::exception::Exception)
{
	std::string str = toolbox::trim(n);
	std::stringstream ss(str);
	long value;
	if ( !(ss >> value) || (ss.peek() != EOF) )
	{
		std::string msg = "Error converting string '"	+ n + "' to long";
		XCEPT_RAISE(toolbox::exception::Exception, msg);
	}
	return value;
}

unsigned long toolbox::toUnsignedLongConversion (const std::string& n) throw (toolbox::exception::Exception)
{
	std::string str = toolbox::trim(n);
	std::stringstream ss(str);
	char first = ss.peek();
	unsigned long value;
	if ( (first == '-') || !(ss >> value) || (ss.peek() != EOF) )
	{
		std::string msg = "Error converting string '"	+ n + "' to unsigned long";
		XCEPT_RAISE(toolbox::exception::Exception, msg);
	}
	return value;
}

double toolbox::toDoubleConversion (const std::string& n) throw (toolbox::exception::Exception)
{
	std::string str = toolbox::trim(n);
	std::stringstream ss(str);
	double value;
	if ( !(ss >> value) || (ss.peek() != EOF) )
	{
		std::string msg = "Error converting string '"	+ n + "' to double";
		XCEPT_RAISE(toolbox::exception::Exception, msg);
	}
	return value;
}
