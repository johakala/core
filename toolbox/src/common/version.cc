// $Id: version.cc,v 1.2 2008/07/18 15:27:38 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/
#include "xcept/version.h"
#include "config/version.h"
#include "toolbox/version.h"

GETPACKAGEINFO(toolbox)

void toolbox::checkPackageDependencies()  throw (config::PackageInfo::VersionException)
{
    CHECKDEPENDENCY(config)
    CHECKDEPENDENCY(xcept)     
}

std::set<std::string, std::less<std::string> > toolbox::getPackageDependencies()
{
    std::set<std::string, std::less<std::string> > dependencies;
    ADDDEPENDENCY(dependencies,config);
    ADDDEPENDENCY(dependencies,xcept);
    return dependencies;
}	
	
