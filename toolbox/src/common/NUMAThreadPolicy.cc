// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2013, CERN.			                 		 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, A. Petrucci, C.Wakefield					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         	 *
 *************************************************************************/

#include <numa.h>
#include <numaif.h>
#include <errno.h>

#include "toolbox/NUMAThreadPolicy.h"
#include "toolbox/utils.h"
#include "toolbox/string.h"

toolbox::NUMAThreadPolicy::NUMAThreadPolicy ()
{
	this->setProperty("cpunodes", "*");
	this->setProperty("mempolicy", "local");
	this->setProperty("affinity", "*");
}

toolbox::NUMAThreadPolicy::~NUMAThreadPolicy()
{

}

std::string toolbox::NUMAThreadPolicy::getPackage()
{
	return "numa";
}

void toolbox::NUMAThreadPolicy::setDefaultPolicy () throw (toolbox::exception::Exception)
{
	//XCEPT_RAISE(toolbox::exception::Exception, "Not implemented");
	if ( this->getProperty("mempolicy") == "local")
	{
		numa_set_localalloc();
	}
	else if ( this->getProperty("mempolicy") == "interleaved")
	{
		if ( this->hasProperty("memnodes") )
		{
			struct bitmask *mask = NULL;

			mask = numa_parse_nodestring((char*)(this->getProperty("memnodes").c_str()));

			numa_set_interleave_mask(mask);

			numa_bitmask_free(mask);
		}
		else
		{
			XCEPT_RAISE(toolbox::exception::Exception, "Invalid allocation policy, no nodes provided");
		}
	}
	else if ( this->getProperty("mempolicy") == "onnode")
	{
		if ( this->hasProperty("memnode") )
		{
			numa_set_preferred(toolbox::toLong(this->getProperty("memnode")));
		}
		else
		{
			XCEPT_RAISE(toolbox::exception::Exception, "Missing node attribute for memory policy allocation");
		}
	}
	else
	{
		XCEPT_RAISE(toolbox::exception::Exception, "Invalid allocation policy");
	}
	
	//if both options are set warn the user
	if ( this->getProperty("cpunodes") != "*" && this->getProperty("affinity") != "*" )
	{
		std::cerr << "WARNING: both cpunodes (" << this->getProperty("cpunodes") << ") and affinity (" << this->getProperty("affinity") << ") are set" << std::endl;		
		std::cerr << "The affinity setting will be ignored" << std::endl;
	}

	//cpu affinity
	if ( this->getProperty("cpunodes") != "*" )
	{
/*
		struct bitmask *mask = NULL;

		mask = numa_parse_cpustring((char*)(this->getProperty("cpunodes").c_str()));

		numa_sched_setaffinity(pthread_self(), mask);

		numa_bitmask_free(mask);
*/
		int nodes = numa_num_configured_nodes();
		// std::cout << "Found " << nodes << " nodes" <<  std::endl;
		struct bitmask *cpunodemask = NULL;
		cpunodemask = numa_parse_nodestring((char*)(this->getProperty("cpunodes").c_str()));
		if ( cpunodemask == 0 )
		{
			std::stringstream ss;
                        ss << "Invalid thread policy (cpunodes) provided = '" << this->getProperty("cpunodes") << "', value possibly out of range";
                        XCEPT_RAISE(toolbox::exception::Exception, ss.str());
		}
		cpu_set_t mask; // posix
               	CPU_ZERO(&mask); // posix
		for (int node = 0; node < nodes; node++) 
		{
                	if (!numa_bitmask_isbitset(cpunodemask, node))
			{
                        	continue;
			}
                	//std::cout << "Node " << node << " ( #" << numa_num_configured_cpus() << "):" <<  std::endl;
                	unsigned int  i;
	
        		struct bitmask *cpus = 0;

        		cpus = numa_allocate_cpumask();
			//std::cout << "cpus " << cpus << std::endl;
			errno = 0;
			
        		int err = numa_node_to_cpus(node, cpus);
        		if (err >= 0) 
			{
				//std::cout << "inspecting bitmask: ";
                		for (i = 0; i < cpus->size; i++)
				{
                        		if (numa_bitmask_isbitset(cpus, i))
					{
                				CPU_SET(i ,&mask); // posix
						//std::cout << i << ",";
					}
				}
			}
			else
			{
				 std::stringstream ss;
                        	ss << "Failed to get cpus for node number='" << node << "' (" <<  strerror(errno) << ")";
                        	XCEPT_RAISE(toolbox::exception::Exception, ss.str());
			}
			numa_free_cpumask(cpus);
		} // for nodes

		numa_free_nodemask(cpunodemask);

		//std::cout << std::endl;

		if (pthread_setaffinity_np(pthread_self(), sizeof(unsigned long), &mask) <0)
		{
			std::stringstream ss;
			ss << "Failed to set affinity for cpu nodes  " << this->getProperty("cpunodes");
			XCEPT_RAISE(toolbox::exception::Exception, ss.str());
		}

		return;
	}
	
	if ( this->getProperty("affinity") != "*" )
	{
		cpu_set_t mask;
		CPU_ZERO(&mask);
		std::set<std::string> affinities = toolbox::parseTokenSet(this->getProperty("affinity"),",");

		try
		{
			for ( std::set<std::string>::iterator it = affinities.begin(); it != affinities.end(); ++it )
			{
				size_t affinity = toolbox::toLong(*it);
				CPU_SET(affinity ,&mask);
			}
		}
		catch ( toolbox::exception::Exception & e )
		{
			XCEPT_RETHROW(toolbox::exception::Exception, "Failed to set NUMA Allocation node, invalid integer value provided in Policy configutation", e);
		}

		if (pthread_setaffinity_np(pthread_self(), sizeof(unsigned long), &mask) <0)
		{
			std::stringstream ss;
			ss << "Failed to set affinity " << this->getProperty("affinity");
			XCEPT_RAISE(toolbox::exception::Exception, ss.str());
		}
	}
}


