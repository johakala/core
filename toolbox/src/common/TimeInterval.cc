// $Id: TimeInterval.cc,v 1.7 2008/07/18 15:27:38 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include <time.h>		
#include <stdio.h>		
#include <errno.h>

#include "toolbox/TimeInterval.h"
#include "toolbox/TimeIntervalSA.h"
#include "toolbox/string.h"
#include "toolbox/regex.h"

#include<strings.h>
#include<iostream>
#include<sstream>

//------------------------------------------------------------------------------
// Static definitions
//------------------------------------------------------------------------------

static const suseconds_t ONE_SECOND = 1000000;

//------------------------------------------------------------------------------
// Membe functions
//------------------------------------------------------------------------------

toolbox::TimeInterval& toolbox::TimeInterval::operator+=(const toolbox::TimeInterval& rhs)
{
	tv_sec += rhs.tv_sec;
	tv_usec += rhs.tv_usec;

	if (tv_usec >= ONE_SECOND) {
		tv_usec -= ONE_SECOND;
		tv_sec++;
	} 
	else if (tv_sec >= 1 && tv_usec < 0) {
		tv_usec += ONE_SECOND;
		tv_sec--;
	}
	normalize ();
	return *this;
}

toolbox::TimeInterval& toolbox::TimeInterval:: operator-=(const toolbox::TimeInterval& rhs)
{
	tv_sec -= rhs.tv_sec;
	tv_usec -= rhs.tv_usec;

	if (tv_usec < 0) {
		tv_usec += ONE_SECOND;
		tv_sec--;
	} 
	else if (tv_usec >= ONE_SECOND) {
		tv_usec -= ONE_SECOND;
		tv_sec++;
	}
	normalize ();
	return *this;
}

void toolbox::TimeInterval::normalize ()
{
	if (tv_usec >= ONE_SECOND) {
		do {
			tv_sec++;
			tv_usec -= ONE_SECOND;
		}
		while (tv_usec >= ONE_SECOND);
	}
	else if (tv_usec <= -ONE_SECOND) {
		do {
			tv_sec--;
			tv_usec += ONE_SECOND;
		}
		while (tv_usec <= -ONE_SECOND);
	}

	if (tv_sec >= 1 && tv_usec < 0) {
		tv_sec--;
		tv_usec += ONE_SECOND;
	}
	else if (tv_sec < 0 && tv_usec > 0) {
		tv_sec++;
		tv_usec -= ONE_SECOND;
	}
}


//------------------------------------------------------------------------------
// All possible variation of HH:MM:SS.MMM I could think of:
//------------------------------------------------------------------------------

std::string toolbox::TimeInterval::toString () const
{
	time_t seconds = tv_sec;
	size_t minutes = 0;
	size_t hours = 0;
	size_t days = 0;
	size_t years = 0;
	
	if (seconds > 59)
	{
		minutes = seconds/60;
		seconds = seconds % 60;
	}
		
	if (minutes > 59)
	{
		hours = minutes/60;
		minutes = minutes % 60;
	}
	
	if (hours > 23)
	{
		days = hours/24;
		hours = hours % 24;
	}
	
	if (days > 364)
	{
		years = days/365;
		days = days % 365;
	}
	
	return toolbox::toString ("%d:%d:%0d:%0d:%0d", years,days,hours,minutes,seconds);
}

std::string toolbox::TimeInterval::toString (const std::string& fmt) const
{
	time_t seconds = tv_sec;
	size_t minutes = 0;
	size_t hours = 0;
	size_t days = 0;
	size_t years = 0;
	size_t months = 0;

	size_t total_hours = 0;
	size_t total_minutes = 0;
	size_t total_days = 0;

	//seconds
	total_minutes = seconds / 60;
	seconds = seconds % 60;

	//minutes
	total_hours = total_minutes / 60;
	minutes = total_minutes % 60;

	//hours
	total_days = total_hours / 24;
	hours = total_hours % 24;

	//years
	years = total_days / 365;
	days =  total_days % 365;

	//months
	months = days / 30;

	//days
	days = days % 30;

	std::stringstream buf;
	
	// PnYnMnDTnHnMnS
	
	if ((fmt == "") || (fmt == "xs:duration"))
	{
		buf << "P";
		if (years > 0)
		{
			buf << years << "Y";
		}
		
		if ((months > 0) || ((years >0) && (days > 0)))
		{
			buf << months << "M";
		}

		if (days > 0)
		{
			buf << days << "D";	
		}
		
		if ((hours > 0) || (minutes > 0) || (seconds > 0))
		{
			buf << "T";
			if (hours > 0)
			{
				buf << hours << "H";
			}
			
			if ( (minutes > 0) || ((hours > 0) && (seconds > 0)))
			{
				buf << minutes << "M";				
			}
			
			if (seconds > 0)
			{
				buf << seconds << "S";
			}
		}
	}

	return buf.str();
}

void toolbox::TimeInterval::fromString(const std::string& s) throw (toolbox::exception::Exception)
{
	toolbox::TimeIntervalSA sa(s);
	sa.syntax();

    tv_sec = sa.getSeconds();
	tv_usec = 0;
	normalize();
}

