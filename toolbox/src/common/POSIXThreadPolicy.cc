// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2013, CERN.			                 		 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, A. Petrucci, C.Wakefield					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         	 *
 *************************************************************************/
#include <sstream>

#include "toolbox/POSIXThreadPolicy.h"
#include "toolbox/string.h"

toolbox::POSIXThreadPolicy::POSIXThreadPolicy ()
{
	this->setProperty("affinity", "*");
}

toolbox::POSIXThreadPolicy::~POSIXThreadPolicy ()
{
}

std::string toolbox::POSIXThreadPolicy::getPackage()
{
	return "posix";
}

void toolbox::POSIXThreadPolicy::setDefaultPolicy () throw (toolbox::exception::Exception)
{
	if ( this->getProperty("affinity") != "*" )
	{
		cpu_set_t mask;
		CPU_ZERO(&mask);
		std::set<std::string> affinities = toolbox::parseTokenSet(this->getProperty("affinity"),",");

		try
		{
			for ( std::set<std::string>::iterator it = affinities.begin(); it != affinities.end(); ++it )
			{
				size_t affinity = toolbox::toLong(*it);
				CPU_SET(affinity ,&mask);
			}
		}
		catch ( toolbox::exception::Exception & e )
		{
			XCEPT_RETHROW(toolbox::exception::Exception, "Failed to set NUMA Allocation node, invalid integer value provided in Policy configutation", e);
		}

		if (pthread_setaffinity_np(pthread_self(), sizeof(unsigned long), &mask) <0)
		{
			std::stringstream ss;
			ss << "Failed to set affinity " << this->getProperty("affinity");
			XCEPT_RAISE(toolbox::exception::Exception, ss.str());
		}
	}
}


