// $Id: Runtime.cc,v 1.16 2008/07/18 15:27:38 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include <stdlib.h>
#include <string.h>
#include "toolbox/Runtime.h"

#include "toolbox/stacktrace.h"
#include "toolbox/ShutdownEvent.h"
#include "toolbox/AbortEvent.h"

// Currently only supported process implementation
#include "toolbox/ProcessInfoImpl.h"
#include "toolbox/FileSystemInfoImpl.h"

#include "toolbox/utils.h"
#include "toolbox/net/Utils.h"
#include "wordexp.h"
#include "glob.h"

#include <errno.h>

// LOG4CPLUS INCLUDES
#include "log4cplus/logger.h"
#include "log4cplus/consoleappender.h"
#include "log4cplus/helpers/appenderattachableimpl.h"
#include "log4cplus/helpers/loglog.h"
#include "log4cplus/helpers/pointer.h"
#include "log4cplus/spi/loggingevent.h"
#include "log4cplus/loggingmacros.h"

using namespace log4cplus;
using namespace log4cplus::helpers;
using namespace log4cplus::spi;

// Linux specific, have solaris code as well
/*
#include <execinfo.h>
void print_stack_trace(void)
{
	void* array[20];
	size_t size;
	char** strings;
	size_t i;
	
	size = ::backtrace(array,20);
	strings = backtrace_symbols (array, size);
	
	std::cout << "Obtained " << size << " stack frames" << std::endl;
	for (i = 0; i < size; i++)
	{
		std::cout << "Frame " << i << ": " << strings[i] << std::endl;
	}
	
	free(strings);
}
*/
// End of Linux specific code



// Definition of the atExit handler
//void onExitCallback(int exitcode, void* tmp)
void onExitCallback(void)
{
	// destroy the runtime
	toolbox::ShutdownEvent e(0);
	toolbox::getRuntime()->fireEvent(e);
	toolbox::getRuntime()->destroyInstance();
	
	// HARD EXIT OF THE PROCESS
	// for the future: register properly callback routines
	// to shutdown all running threads to avoid multi-thread
	// conflict at exiting time (LOG4CPLUS logger already deleted)
	//

	//std::cout << "Exit " << exitcode << std::endl;
	//_Exit(exitcode);
}

void toolbox::signalCallback(int sig, siginfo_t* info, void* c)
{
	if (toolbox::getRuntime()->handleSignals_ == true)
	{
		//ucontext_t* context = (ucontext_t*) c;
		//std::cout << "Process [" << info->si_pid << "] terminates. Caught signal " << sig << std::endl;
	
		std::cout << "Terminating..." << std::endl;
		exit(toolbox::getRuntime()->exitStatus_); // will cause exit handler to be called
	} else
	{
		std::cout << "XDAQ process has been scheduled for termination. Please wait..." << std::endl;
		toolbox::getRuntime()->pendingSignal_ = sig;
		toolbox::getRuntime()->pendingSigInfo_ = info;
		toolbox::getRuntime()->pendingContext_ = c;
	}
}

void toolbox::signalSEGVCallback(int sig, siginfo_t* info, void* c)
{
	//ucontext_t* context = (ucontext_t*) c;
	if (toolbox::getRuntime()->handleSignals_ == true)
	{
		// print stack frame
		std::cout << "Process [" << info->si_pid << "] on node ";
		std::cout << toolbox::net::getHostName() << " terminates abnormally at ";
		std::cout << toolbox::TimeVal::gettimeofday().toString(toolbox::TimeVal::loc) << ".";
		std::cout << " Caught signal " << sig;
		std::cout << " at address [" << std::hex << info->si_addr << std::dec << "]" << std::endl;
		std::cout << "Stacktrace follows:" << std::endl;
		toolbox::stacktrace(20,std::cout);

		// destroy the runtime
		toolbox::AbortEvent e(0);
		toolbox::getRuntime()->fireEvent(e);
		toolbox::getRuntime()->destroyInstance();
		// reraise the signal and exit process...
		::kill ( (-1)*::getpid(), sig);
	}
	else
	{
		std::cout << "XDAQ process will abort. Please wait..." << std::endl;
		toolbox::getRuntime()->pendingSignal_ = sig;
		toolbox::getRuntime()->pendingSigInfo_ = info;
		toolbox::getRuntime()->pendingContext_ = c;
	}
}

static void terminate_mainhandler()
{
        Logger rootLogger = Logger::getRoot();
	LOG4CPLUS_FATAL(rootLogger, "Uncaught exception in main thread");
#ifdef __GNUC__
	__gnu_cxx::__verbose_terminate_handler();
#else
	abort()
#endif /* __GNUC__ */
}

static void unexpected_mainhandler()
{
        Logger rootLogger = Logger::getRoot();
	LOG4CPLUS_FATAL(rootLogger, "Unexpected exception in main thread");
#ifdef __GNUC__
	__gnu_cxx::__verbose_terminate_handler();
#else
	abort()
#endif /* __GNUC__ */
}

toolbox::Runtime* toolbox::Runtime::instance_ = 0;

toolbox::Runtime* toolbox::getRuntime()
{
        std::set_terminate(terminate_mainhandler);
        std::set_unexpected(unexpected_mainhandler);

	return toolbox::Runtime::getInstance();
}

toolbox::Runtime* toolbox::Runtime::getInstance()
{
	if (instance_ == 0)
	{
		instance_ = new toolbox::Runtime();
	}
	return instance_;
}

void toolbox::Runtime::fireEvent (toolbox::Event& e)
{
	// Fire all the shutdown handlers only for now
	dispatcher_.fireEvent(e);
}

void toolbox::Runtime::destroyInstance()
{
	if (instance_ != 0)
	{
		delete instance_;
		instance_ = 0;
	}
}



toolbox::Runtime::Runtime (): mutex_(BSem::FULL)
{	
	// Register a callback that is called when the process
	// exits with the exit() call
	exitStatus_  = 0;
	if (atexit(onExitCallback) != 0)
	{
		// should never happen
		std::cout << "Cannot register atexit handler." << std::endl;
	}
	
	// Register abort() handler
	struct sigaction sa;
	sigset_t mask;
	
	sigemptyset(&mask); // don't block any signals
	//sigaddset (&mask, SIGPIPE); // block SIGPIPE
	
	sa.sa_handler = 0;
	sa.sa_mask = mask;
	sa.sa_flags = SA_SIGINFO;
	sa.sa_sigaction = toolbox::signalCallback;
	
	// don't care about previous installed handlers
	sigaction(SIGINT, &sa, 0); // handle keyboard interrupt with CTRL-C
	sigaction(SIGQUIT, &sa, 0); // handle keyboard QUIT
	sigaction(SIGTERM, &sa, 0); // handle ordinary termination
	
	
	struct sigaction sa1;
	sa1.sa_handler = 0;
	sa1.sa_mask = mask;
	sa1.sa_flags = SA_SIGINFO | SA_RESETHAND;
	// SA_RESETHAND replaces SA_ONESHOT;
	sa1.sa_sigaction = signalSEGVCallback;
	
	sigaction(SIGABRT, &sa1, 0);
	sigaction(SIGSEGV, &sa1, 0); // handle segmentation fault
	
	
	pendingSignal_ = 0;
	pendingSigInfo_ = 0;
	pendingContext_ = 0;
	handleSignals_ = true;
}


toolbox::Runtime::~Runtime ()
{
	
}


void toolbox::Runtime::addShutdownListener (toolbox::ActionListener* listener)
{
	dispatcher_.addActionListener(listener);
}
			
void toolbox::Runtime::removeShutdownListener (toolbox::ActionListener* listener) throw (toolbox::exception::Exception)
{
	dispatcher_.removeActionListener(listener);
}

void toolbox::Runtime::disableSignals ()
{
	mutex_.take();
	handleSignals_ = false;
	mutex_.give();
}

void toolbox::Runtime::halt(int code)
{
	exitStatus_ = code;
	::kill( (-1)*::getpid(),SIGTERM);
}

void toolbox::Runtime::createPidFile(const std::string& filename) throw (toolbox::exception::Exception)
{
	int fildes;
        FILE *fd;

	try
	{
        	fildes = toolbox::createFile(filename, true);
	}
	catch (toolbox::exception::Exception& e)
	{
		std::string msg = "Failed to create pid file ";
		msg += filename;
		XCEPT_RETHROW (toolbox::exception::Exception, msg, e);
	}
	
	// Open a stdio file over the low-level one.
        if ((fd = ::fdopen(fildes, "w")) == NULL) 
	{
		::close(fildes);
                ::unlink(filename.c_str());

		std::string msg = "Failed to open pid file ";
		msg += filename;
		msg += ", ";
		msg += strerror(errno);
		XCEPT_RAISE (toolbox::exception::Exception, msg);
        }

        if (::fprintf(fd, "%ld\n", (long) ::getpid()) < 0)
	{
		::close(fildes);
                ::unlink(filename.c_str());

		std::string msg = "Failed to write to pid file ";
		msg += filename;
		msg += ", ";
		msg += strerror(errno);
		XCEPT_RAISE (toolbox::exception::Exception, msg);
	}
	
        ::fclose(fd);
}

void toolbox::Runtime::enableSignals ()
{
	// process pending signal
	mutex_.take();
	handleSignals_ = true;
	if ((pendingSignal_ == SIGABRT) || (pendingSignal_ == SIGSEGV))
	{
		//std::cout << "A signal " << pendingSignal_ << " is to be handled." << std::endl;
		signalSEGVCallback(toolbox::getRuntime()->pendingSignal_, 
				toolbox::getRuntime()->pendingSigInfo_, 
				toolbox::getRuntime()->pendingContext_);
	}
	else if (pendingSignal_ != 0)
	{
		// there has been a signal
		//std::cout << "A signal " << pendingSignal_ << " is to be handled." << std::endl;
		signalCallback(toolbox::getRuntime()->pendingSignal_, 
				toolbox::getRuntime()->pendingSigInfo_, 
				toolbox::getRuntime()->pendingContext_);
	}
	mutex_.give();
}

std::vector<std::string> toolbox::Runtime::expandPathName(const std::string & pathname)  throw (toolbox::exception::Exception)
{
	std::vector<std::string> expandedPaths;

	wordexp_t result;
	/* Expand the string for the program to run. Do not expand undefined environment variables to empty: WRDE_UNDEF */
	int ret = wordexp (pathname.c_str(), &result, WRDE_UNDEF);
	switch (ret)
	{
	  case 0:			/* Successful.  */
		break;
	  case WRDE_NOSPACE:
		   /* If the error was WRDE_NOSPACE,
		   then perhaps part of the result was allocated.  */
		   wordfree (&result);
	  default:                    /* Some other error.  */
		XCEPT_RAISE(toolbox::exception::Exception, "cannot expand path name: " +  pathname);
	}
		
	for ( size_t i = 0; i < result.we_wordc; i++ )
	{
		expandedPaths.push_back(result.we_wordv[i]);
	}

	wordfree (&result);
	return expandedPaths;

}

std::vector<std::string> toolbox::Runtime::expandPathPattern(const std::string & pattern)  
	throw (toolbox::exception::Exception)
{
	std::vector<std::string> expandedPatterns;

	glob_t result;
	/* Expand the string for the program to run.  */
	//int flags = GLOB_NOSORT | GLOB_NOCHECK | GLOB_BRACE;
	int flags = GLOB_BRACE|GLOB_NOMAGIC;
	switch (glob (pattern.c_str(), flags, 0, &result))
	{
	  case 0:			/* Successful.  */
		break;
	  case GLOB_NOSPACE:
		   /* If the error was GLOB_NOSPACE,
		   then perhaps part of the result was allocated.  */
		   globfree (&result);
	  default:                    /* Some other error.  */
		XCEPT_RAISE(toolbox::exception::Exception, "cannot expand path pattern: " +  pattern);
	}
	for ( size_t i = 0; i < result.gl_pathc; i++ )
	{
		expandedPatterns.push_back(result.gl_pathv[i]);
	}

	globfree (&result);
	return expandedPatterns;

}

pid_t toolbox::Runtime::getPid()
{
	return getpid();
}

toolbox::ProcessInfo::Reference toolbox::Runtime::getProcessInfo(pid_t pid) throw (toolbox::exception::Exception)
{
	// if Linux, return a Linux process (this is the only current one supported)
	// The CTOR of the class will throw also a toolbox::exception::Exception that we let fall through here
	
	toolbox::ProcessInfoImpl* p = new toolbox::ProcessInfoImpl(pid);
	return toolbox::ProcessInfo::Reference(p);
}

toolbox::FileSystemInfo::Reference toolbox::Runtime::getFileSystemInfo() throw (toolbox::exception::Exception)
{
	// if Linux, return a Linux info object (this is the only current one supported)
	// The CTOR of the class will throw also a toolbox::exception::Exception that we let fall through here
	
	toolbox::FileSystemInfoImpl* p = new toolbox::FileSystemInfoImpl();
	return toolbox::FileSystemInfo::Reference(p);
}
