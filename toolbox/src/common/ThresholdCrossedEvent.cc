// $Id: ThresholdCrossedEvent.cc,v 1.2 2008/07/18 15:27:38 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and S. Murray					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "toolbox/ThresholdCrossedEvent.h"

toolbox::ThresholdCrossedEvent::ThresholdCrossedEvent(	
	const std::string& name, 
	const std::string& threshold,
	double from,
	double to,
	void* originator):
		toolbox::Event(name, originator)
{
	threshold_ = threshold;
	from_ = from;
	to_ = to;
}


std::string toolbox::ThresholdCrossedEvent::getName()
{
	return name_;
}

std::string toolbox::ThresholdCrossedEvent::getThreshold()
{
	return threshold_;
}

double toolbox::ThresholdCrossedEvent::getFromValue()
{
	return from_;
}

double toolbox::ThresholdCrossedEvent::getToValue()
{
	return to_;
}


toolbox::ThresholdCrossedUpEvent::ThresholdCrossedUpEvent(
	const std::string& threshold, 
	double from,
	double to,
	void* originator):
		toolbox::ThresholdCrossedEvent("ThresholdCrossedUpEvent", threshold, from, to, originator)
{
}

toolbox::ThresholdCrossedDownEvent::ThresholdCrossedDownEvent(
	const std::string& threshold, 
	double from,
	double to,
	void* originator):
		toolbox::ThresholdCrossedEvent("ThresholdCrossedDownEvent", threshold, from, to, originator)
{
}
