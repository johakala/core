// $Id: WorkLoopFactory.cc,v 1.10 2008/07/18 15:27:41 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include<string>
#include<list>
#include<map>

#include "toolbox/task/WorkLoopFactory.h"
#include "toolbox/task/PollingWorkLoop.h"
#include "toolbox/task/WaitingWorkLoop.h"
#include "toolbox/BSem.h"
#include "toolbox/task/Guard.h"

#include <iostream>

toolbox::task::WorkLoopFactory* toolbox::task::WorkLoopFactory::instance_ = 0;
static toolbox::BSem mutex_(toolbox::BSem::FULL,false);
		
//
// urn:toolbox-task-workloop:http/PeerTransportReceiver@101.102.103.104:40000/waiting
//
		
toolbox::task::WorkLoop* toolbox::task::WorkLoopFactory::getWorkLoop(const std::string & name, const std::string & type) throw (toolbox::task::exception::Exception)
{
	std::string reference = name + "/";
	reference += type;
	toolbox::net::URN urn("toolbox-task-workloop", reference);
	return this->getWorkLoop(urn, type);	
}

toolbox::task::WorkLoop* toolbox::task::WorkLoopFactory::getWorkLoop(toolbox::net::URN & urn, const std::string & type) throw (toolbox::task::exception::Exception)
{
        toolbox::task::Guard <toolbox::BSem> guard(mutex_);

	std::string index = urn.toString();
	toolbox::task::WorkLoop* l = workLoops_[index];
	if (l == 0)
	{
		std::string name = urn.getNSS();
		
		if (type == "waiting")
		{
			workLoops_[index] = new toolbox::task::WaitingWorkLoop(name);
		}
		else if (type == "polling")
		{
			workLoops_[index] = new toolbox::task::PollingWorkLoop(name);
		}
		else
		{
			std::string msg = "Requested work loop type not existing: ";
			msg += index;
			XCEPT_RAISE (toolbox::task::exception::Exception, msg);
		}
		return workLoops_[index];
	}
	else 
	{
		return l;
	}
}

void toolbox::task::WorkLoopFactory::removeWorkLoop(const std::string & name, const std::string & type) throw (toolbox::task::exception::Exception)
{
	std::string reference = name + "/";
	reference += type;
	toolbox::net::URN urn("toolbox-task-workloop", reference);
	this->removeWorkLoop(urn);	
}
			
void toolbox::task::WorkLoopFactory::removeWorkLoop(toolbox::net::URN & urn) throw (toolbox::task::exception::Exception)
{
        toolbox::task::Guard <toolbox::BSem> guard(mutex_);

	std::string index = urn.toString();
	std::map<std::string, toolbox::task::WorkLoop*, std::less<std::string> >::iterator i = workLoops_.find(index);
	if (i != workLoops_.end())
	{
		workLoops_.erase(i);
	}
	else
	{
		std::string msg = "Requested work loop for removal not found: ";
		msg += index;
		XCEPT_RAISE (toolbox::task::exception::Exception, msg);
	}
}

std::list<toolbox::task::WorkLoop*> toolbox::task::WorkLoopFactory::getWorkLoops()
{
	std::list<toolbox::task::WorkLoop*> l;
	std::map<std::string, toolbox::task::WorkLoop*, std::less<std::string> >::iterator i;
	for (i = workLoops_.begin(); i != workLoops_.end(); i++)
	{
		l.push_back((*i).second);
	}
	return l;
}

//! Retrieve a pointer to the toolbox::task::WorkLoopFactory singleton
//
toolbox::task::WorkLoopFactory* toolbox::task::WorkLoopFactory::getInstance()
{
        toolbox::task::Guard <toolbox::BSem> guard(mutex_);
	if (instance_ == 0)
	{
		instance_ = new toolbox::task::WorkLoopFactory();
	}
	return instance_;
}

//! Destoy the factory and all associated work loops
//
void toolbox::task::WorkLoopFactory::destroyInstance()
{
        toolbox::task::Guard <toolbox::BSem> guard(mutex_);
	if (instance_ != 0)
	{
		delete instance_;
		instance_ = 0;
	}
}

		
toolbox::task::WorkLoopFactory* toolbox::task::getWorkLoopFactory()
{
	return toolbox::task::WorkLoopFactory::getInstance();
}
