// $Id: WaitingWorkLoop.cc,v 1.11 2008/07/18 15:27:41 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "toolbox/task/WaitingWorkLoop.h"

toolbox::task::WaitingWorkLoop::WaitingWorkLoop(const std::string & name): toolbox::task::WorkLoop(name, "waiting")
{

}

void toolbox::task::WaitingWorkLoop::resize (size_t n) throw (toolbox::task::exception::Exception)
{
        try
        {
                jobs_.resize(n);
                size_ = n;
        }
        catch (toolbox::exception::RingListNotEmpty& rlne)
        {
                XCEPT_RETHROW(toolbox::task::exception::Exception, "Failed to resize WaitingWorkLoop", rlne);
        }
}



void toolbox::task::WaitingWorkLoop::process()
{
	
	toolbox::task::ActionSignature* a = jobs_.pop();
	
	// null elemnt is artificially put by the cance command
	if ( a == 0 ) return;
	
	if (a->invoke(this))
	{
			// if the action requires to be re-executed,
			// re-schedule it.
			this->submit(a);
	}
}

void toolbox::task::WaitingWorkLoop::submit (toolbox::task::ActionSignature* method) throw (toolbox::task::exception::Exception)
{
	if ( method == 0 ) 
	{
		std::string msg = toolbox::toString("Cannot submit null action on WorkLoop: %s",this->getName().c_str());
		XCEPT_RAISE(toolbox::task::exception::Exception, msg);
	}
	try
	{		
		jobs_.push(method);
	} 
	catch (toolbox::exception::QueueFull& qf)
	{
		XCEPT_RETHROW (toolbox::task::exception::Exception, "Failed to submit, waitingWorkLoop queue full", qf);
	}
}

void toolbox::task::WaitingWorkLoop::remove (toolbox::task::ActionSignature* method) throw (toolbox::task::exception::Exception)
{
	if ( ! this->isActive() )
	{	
		if ( jobs_.empty() ) {
				
			std::string msg = toolbox::toString("Cannot find job: %s, WorkLoop: %s",method->name().c_str(),this->getName().c_str());
			XCEPT_RAISE(toolbox::task::exception::Exception, msg);
		}
		// remove element from squeue	
		toolbox::task::ActionSignature* first = jobs_.pop();
		jobs_.push(first);
		toolbox::task::ActionSignature* current = 0;
		do {
			current = jobs_.pop();
			if ( current == method )
				return;
			else
				jobs_.push(current);
		
		} while (first != current );
		
		
		std::string msg = toolbox::toString("Cannot find job: %s, WorkLoop: %s",method->name().c_str(),this->getName().c_str());
		XCEPT_RAISE(toolbox::task::exception::Exception, msg);
		
		
	}
	else 
	{
		std::string msg = toolbox::toString("Cannot remove job: %s, WorkLoop: %s is still active",method->name().c_str(),this->getName().c_str());
		XCEPT_RAISE(toolbox::task::exception::Exception,msg );
	}	
}	

void toolbox::task::WaitingWorkLoop::cancel()  throw (toolbox::task::exception::Exception)
{
	if ( this->isActive() )
	{	
		stopped_ = true;
		toolbox::task::ActionSignature* tmp = 0;
		jobs_.push(tmp);
		toolbox::task::WorkLoop::cancel();
	}
	else 
	{
		std::string msg = toolbox::toString("Cannot cancel non active WorkLoop: %s",this->getName().c_str());
		XCEPT_RAISE(toolbox::task::exception::Exception, msg);
	}
} 


