// $Id: PollingWorkLoop.cc,v 1.8 2008/07/18 15:27:41 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/


#include "toolbox/task/PollingWorkLoop.h"



			
toolbox::task::PollingWorkLoop::PollingWorkLoop(const std::string & name): toolbox::task::WorkLoop(name, "polling"), mutex_(BSem::FULL)
{
	schedule_ = toolbox::rlist<toolbox::task::ActionSignature*>::create(name + "-schedule-queue");
	jobs_ =  toolbox::rlist<toolbox::task::ActionSignature*>::create(name + "-jobs-queue");

}

void toolbox::task::PollingWorkLoop::resize (size_t n) throw (toolbox::task::exception::Exception)
{
	try
	{
		// actually it would be necessary to try/catch on each one
		// and to try to recover - but that's too detailed
		schedule_->resize(n);
		jobs_->resize(n);
		size_ = n;
	}
	catch (toolbox::exception::RingListNotEmpty& rlne)
	{
		XCEPT_RETHROW(toolbox::task::exception::Exception, "Failed to resize PollingWorkLoop", rlne);
	}
}

void toolbox::task::PollingWorkLoop::submit (toolbox::task::ActionSignature* action) throw (toolbox::task::exception::Exception)
{
	mutex_.take();
	try
	{
		schedule_->push_back(action);
	}
	catch (toolbox::exception::RingListFull& rlf)
	{
		mutex_.give();
		XCEPT_RETHROW (toolbox::task::exception::Exception, "Failed to submit, pollingWorkLoop queue full", rlf);
	}
	mutex_.give();
}


void toolbox::task::PollingWorkLoop::remove (toolbox::task::ActionSignature* method) throw (toolbox::task::exception::Exception)
{
	if ( ! this->isActive() )
	{	
		
		if ( jobs_->empty() ) {
				
			std::string msg = toolbox::toString("Cannot find job: %s, WorkLoop: %s",method->name().c_str(),this->getName().c_str());
			XCEPT_RAISE(toolbox::task::exception::Exception, msg);
		}
		// remove element from squeue	
		toolbox::task::ActionSignature* first = jobs_->front();
		jobs_->pop_front();
		
		jobs_->push_back(first);
		toolbox::task::ActionSignature* current = 0;
		do {
			current = jobs_->front();
			jobs_->pop_front();
			
			if ( current == method )
				return;
			else
				jobs_->push_back(current);
		
		} while (first != current );
		
		
		std::string msg = toolbox::toString("Cannot find job: %s, WorkLoop: %s",method->name().c_str(),this->getName().c_str());
		XCEPT_RAISE(toolbox::task::exception::Exception, msg);
		
		
		
	}
	else 
	{
		std::string msg = toolbox::toString("Cannot remove job: %s, WorkLoop: %s is still active",method->name().c_str(),this->getName().c_str());
		XCEPT_RAISE(toolbox::task::exception::Exception,msg );
	}	
}	
	

void toolbox::task::PollingWorkLoop::process()
{

	// check any jobs to be removed, check any job to be scheduled
	while (! schedule_->empty() ) 
	{
			toolbox::task::ActionSignature* action = schedule_->front();
			schedule_->pop_front();
			jobs_->push_back(action);

	}

	if ( !jobs_->empty() ) {
		toolbox::task::ActionSignature* a = jobs_->front();
		jobs_->pop_front();
		if (a->invoke(this))
		{
			// if the action requires to be re-executed,
			// re-schedule it.
			jobs_->push_back(a);
		}
		
	}


}
