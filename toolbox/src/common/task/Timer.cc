// $Id: Timer.cc,v 1.8 2008/07/18 15:27:41 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "toolbox/task/Timer.h"
#include "toolbox/string.h"
#include "toolbox/utils.h"
#include "toolbox/exception/FailedToActivateTask.h"
#include <pthread.h>

toolbox::task::Timer::Timer(const std::string & name) throw (toolbox::task::exception::Exception) : toolbox::Task((char*)name.c_str())
{		
	if (pthread_mutexattr_init (&mattr_) != 0) 
	{
		XCEPT_RAISE(toolbox::task::exception::Exception, "Unable to init mattr");	
	}
	
	if (pthread_mutexattr_setpshared (&mattr_, PTHREAD_PROCESS_PRIVATE) != 0) 
	{
		XCEPT_RAISE(toolbox::task::exception::Exception, "Error setting pshared to PTHREAD_PROCESS_PRIVATE");	
	}
	
	if (pthread_mutex_init(&conditionMutex_, &mattr_) != 0) 
	{
		XCEPT_RAISE(toolbox::task::exception::Exception, "Error initing the mutex");	
	}
	
	if (pthread_condattr_init (&cvattr_) != 0) 
	{
		XCEPT_RAISE(toolbox::task::exception::Exception, "Error initing cvattr");	
	}
	
	if (pthread_condattr_setpshared (&cvattr_, PTHREAD_PROCESS_PRIVATE) != 0) 
	{
		XCEPT_RAISE(toolbox::task::exception::Exception, "Unable to set cvattr to PTHREAD_PROCESS_PRIVATE");	
	}
	
	if (pthread_cond_init (&conditionVar_, &cvattr_) != 0) 
	{
		XCEPT_RAISE(toolbox::task::exception::Exception, "Failed to initialize condition variable");	
	}
	
	active_  = false;
	done_ = false;
}

toolbox::task::Timer::~Timer()
{
	// cleanup
	pthread_cond_destroy(&conditionVar_);
	pthread_mutex_destroy(&conditionMutex_);
	pthread_condattr_destroy(&cvattr_);
	pthread_mutexattr_destroy(&mattr_);	
}

void toolbox::task::Timer::start()  throw (toolbox::task::exception::Exception)
{
	pthread_mutex_lock ( &conditionMutex_ );
	if ( this->isActive()  && (! done_ ) )
	{
		pthread_mutex_unlock( &conditionMutex_ );
		std::string msg = toolbox::toString("Cannot start an active timer");
		XCEPT_RAISE(toolbox::task::exception::Exception, msg);
	
	}
	pthread_mutex_unlock( &conditionMutex_ );
		
	try
	{
		this->activate();
	}
	catch(toolbox::exception::FailedToActivateTask & e)
	{
		XCEPT_RETHROW(toolbox::task::exception::Exception, "failed to activate thread for timer task",e);
	}
	
	while (! active_ ) { sched_yield();}
}

int toolbox::task::Timer::svc()
{	
	active_  = true;
	while ( active_ )
	{
		pthread_mutex_lock ( &conditionMutex_ );


		if (jobs_.empty())
		{

			(void) pthread_cond_wait ( &conditionVar_, &conditionMutex_ );

			pthread_mutex_unlock( &conditionMutex_ );
		}
		else
		{
			toolbox::task::TimerTask t = jobs_.top();
			toolbox::TimeVal waitingTime = t.schedule - toolbox::TimeVal::gettimeofday();

			if (waitingTime > toolbox::TimeVal::zero())
			{	

				struct timespec timeout;
				timeout.tv_sec = t.schedule.sec();
				timeout.tv_nsec = t.schedule.usec() * 1000;

				
				pthread_cond_timedwait ( &conditionVar_, &conditionMutex_, &timeout );
				pthread_mutex_unlock( &conditionMutex_ );
			
			}
			else // execute immediately
			{
			
				try
				{
					jobs_.pop();

					// Bookeeping	
					schedules_.erase(t.name);

					pthread_mutex_unlock( &conditionMutex_ );
					t.lastExecutionTime =  toolbox::TimeVal::gettimeofday();
					toolbox::task::TimerEvent e(this, &t);

					t.listener->timeExpired(e);

					if (t.fixedRate)
					{	
						try
						{				
							this->scheduleAtFixedRate(t.lastExecutionTime, t.listener, t.period, t.context, t.name);
						}
						catch (toolbox::task::exception::NotActive& e)
						{
							// timer has been stopped by somebody , therefore ignore this re-schedule
						}
					}
				}
				catch (toolbox::task::exception::Exception& te)
				{
					// We may redefine the fire that it returns a true/false
					// boolean return value, indicating if the exception was
					// properly handled or not
					//
					std::stringstream oss;
					oss << te.what();
					xcept::Exception e( "xcept::Exception", "Caught task exception in timer task.\n" + oss.str(), __FILE__, __LINE__, __FUNCTION__, te);
					dispatcher_.fire(e);
				}
				catch (xcept::Exception& xe)
				{
					// We may redefine the fire that it returns a true/false
					// boolean return value, indicating if the exception was
					// properly handled or not
					//
					std::stringstream oss;
					oss << xe.what();
					xcept::Exception e( "xcept::Exception", "Caught xcept exception in timer task.\n" + oss.str(), __FILE__, __LINE__, __FUNCTION__, xe);
					dispatcher_.fire(e);
				}
				catch (std::exception& se)
				{
					// We may redefine the fire that it returns a true/false
					// boolean return value, indicating if the exception was
					// properly handled or not
					//
					std::stringstream oss;
					oss << se.what();
					xcept::Exception e( "xcept::Exception", "Caught std exception in timer task.\n" + oss.str(), __FILE__, __LINE__, __FUNCTION__);
					dispatcher_.fire(e);
				}
				catch (...)
				{
					// We may redefine the fire that it returns a true/false
					// boolean return value, indicating if the exception was
					// properly handled or not
					//
					xcept::Exception e( "xcept::Exception", "Caught Unknown exception in timer task", __FILE__, __LINE__, __FUNCTION__);
					dispatcher_.fire(e);
				}
			}
		}
	} // end of while active_
	
	
	// empty the fifo
	while (!jobs_.empty())
		jobs_.pop();

	//Bookeeeping
	schedules_.clear();
	//
	active_ = false;
	done_ = true;	
	return 0;
}



void toolbox::task::Timer::scheduleAtFixedRate 
(
	toolbox::TimeVal& start,
	toolbox::task::TimerListener* listener,
	toolbox::TimeInterval& period,
	void* context,
	const std::string& name
) 
	throw (toolbox::task::exception::InvalidListener, toolbox::task::exception::InvalidSubmission, toolbox::task::exception::NotActive)
{
	pthread_mutex_lock ( &conditionMutex_ );
	
	if ( ! this->isActive() )
	{
		pthread_mutex_unlock( &conditionMutex_ );
		std::string msg = toolbox::toString("Cannot submit to non-active Timer");
		XCEPT_RAISE(toolbox::task::exception::NotActive, msg);	
	}	

	if ( listener == 0 ) 
	{
		pthread_mutex_unlock( &conditionMutex_ );
		std::string msg = toolbox::toString("Cannot submit null listener to Timer");
		XCEPT_RAISE(toolbox::task::exception::InvalidListener, msg);
	}

	toolbox::task::TimerTask t;
	t.listener = listener;
	t.context = context;
	t.name = name;
	//t.schedule = toolbox::TimeVal::gettimeofday() + period;
	
	t.schedule = start + period;
	t.period = period;
	t.fixedRate = true;
	
	// Bookeeping
	if ( schedules_.find(name) != schedules_.end() )
	{
		pthread_mutex_unlock( &conditionMutex_ );
		std::string msg = toolbox::toString("Cannot submit %s, already existing",name.c_str());
		XCEPT_RAISE(toolbox::task::exception::InvalidSubmission, msg);
	}
	
	schedules_[name] = t;
	//
	
	jobs_.push(t);
	pthread_cond_signal ( &conditionVar_ );
	pthread_mutex_unlock( &conditionMutex_ );			
}

void toolbox::task::Timer::schedule (
						toolbox::task::TimerListener* listener,
						toolbox::TimeVal& time,
						void* context,
						const std::string& name) 
	throw (toolbox::task::exception::InvalidListener, toolbox::task::exception::InvalidSubmission, toolbox::task::exception::NotActive)
{
	pthread_mutex_lock ( &conditionMutex_ );

	if ( ! this->isActive() )
	{
		pthread_mutex_unlock( &conditionMutex_ );
		std::string msg = toolbox::toString("Cannot submit to  not active Timer");
		XCEPT_RAISE(toolbox::task::exception::NotActive, msg);
	
	}

	if ( listener == 0 ) 
	{
		pthread_mutex_unlock( &conditionMutex_ );
		std::string msg = toolbox::toString("Cannot submit null listener to Timer");
		XCEPT_RAISE(toolbox::task::exception::InvalidListener, msg);
	}

	toolbox::task::TimerTask t;
	t.listener = listener;
	t.context = context;
	t.name = name;
	t.schedule = time;
	t.period = toolbox::TimeVal::zero();
	t.fixedRate = false;
	
	// Bookeeping
	if ( schedules_.find(name) != schedules_.end() )
	{
		pthread_mutex_unlock( &conditionMutex_ );
		std::string msg = toolbox::toString("Cannot submit %s, already existing",name.c_str());
		XCEPT_RAISE(toolbox::task::exception::InvalidSubmission, msg);
	}
	
	schedules_[name] = t;
	//
	jobs_.push(t);	
	pthread_cond_signal ( &conditionVar_ );
	pthread_mutex_unlock( &conditionMutex_ );
	
		
}

void toolbox::task::Timer::remove (const std::string& name) 
	throw (toolbox::task::exception::NotActive, toolbox::task::exception::NoJobs, toolbox::task::exception::JobNotFound )
{	
	
	
	pthread_mutex_lock( &conditionMutex_ );

	if ( ! this->isActive() )
	{
		pthread_mutex_unlock( &conditionMutex_ );
		std::string msg = toolbox::toString("Cannot remove from  not active Timer");
		XCEPT_RAISE(toolbox::task::exception::NotActive, msg);
	
	}

	if ( jobs_.empty() ) 
	{				
		std::string msg = toolbox::toString("Cannot find job: %s", name.c_str());
		pthread_mutex_unlock( &conditionMutex_ );
		XCEPT_RAISE(toolbox::task::exception::NoJobs, msg);
	}

	std::vector<toolbox::task::TimerTask> temp;
	bool found = false;
	while (!jobs_.empty())
	{
		toolbox::task::TimerTask t = jobs_.top();
		jobs_.pop();
		if (t.name == name)
		{
			found = true;
			//Bookeeeping
			schedules_.erase(name);
			//
			continue;
		}
		else
		{
			temp.push_back(t); // remember
		}
	}

	for (std::vector<toolbox::task::TimerTask>::size_type i = 0; i < temp.size(); i++)
	{
		jobs_.push(temp[i]);
	}
	
	pthread_mutex_unlock( &conditionMutex_ );
	
	if (!found)
	{
		std::string msg = toolbox::toString("Cannot find job: %s", name.c_str());
		XCEPT_RAISE(toolbox::task::exception::JobNotFound, msg);
	}
}	

void toolbox::task::Timer::stop()  
	throw (toolbox::task::exception::NotActive)
{

	pthread_mutex_lock ( &conditionMutex_ );
	
	if ( ! this->isActive() )
	{	
		pthread_mutex_unlock( &conditionMutex_ );
		std::string msg = toolbox::toString("Cannot stop non active timer");
		XCEPT_RAISE(toolbox::task::exception::NotActive, msg);
	}	
		
	done_ = false;
	active_ = false;

	// force wake up
	pthread_cond_signal ( &conditionVar_ );
	pthread_mutex_unlock( &conditionMutex_ );

	// wait untile service routine exits

	while (!done_)
	{
		toolbox::u_sleep(1000);
	}

	
		
} 

bool toolbox::task::Timer::isActive()
{
	return active_;
}


std::vector<toolbox::task::TimerTask>   toolbox::task::Timer::getScheduledTasks()
	throw (toolbox::task::exception::NotActive)
{
	std::vector<toolbox::task::TimerTask> schedules;

	pthread_mutex_lock ( &conditionMutex_ );

	if ( ! this->isActive() )
	{	
		pthread_mutex_unlock( &conditionMutex_ );
		std::string msg = toolbox::toString("Cannot cancel non active timer");
		XCEPT_RAISE(toolbox::task::exception::NotActive, msg);
	}	


	// fill vector
	std::map<std::string, toolbox::task::TimerTask, std::less<std::string> >::iterator i;
	for (i = schedules_.begin(); i != schedules_.end(); i++ )
	{
		schedules.push_back((*i).second);
	}

	pthread_mutex_unlock( &conditionMutex_ );
	
	return schedules;
}

void toolbox::task::Timer::addExceptionListener
(
	toolbox::exception::Listener * listener
)
{
	dispatcher_.addListener(listener);
}

void toolbox::task::Timer::removeExceptionListener
(
	toolbox::exception::Listener * listener
) 
	throw (toolbox::exception::Exception)
{
	// throws same exception, should be forwarded properly
	//
	dispatcher_.removeListener(listener);
}
