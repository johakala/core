// $Id: TimerFactory.cc,v 1.4 2008/07/18 15:27:41 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include<string>
#include<list>
#include<map>
#include <iostream>

#include "toolbox/task/TimerFactory.h"
#include "toolbox/task/Timer.h"
#include "toolbox/BSem.h"
#include "toolbox/task/Guard.h"
#include "toolbox/BSem.h"
#include "toolbox/task/Guard.h"


toolbox::task::TimerFactory* toolbox::task::TimerFactory::instance_ = 0;
static toolbox::BSem mutex_(toolbox::BSem::FULL,true);

bool toolbox::task::TimerFactory::hasTimer(const std::string & name)
{
	toolbox::task::Guard <toolbox::BSem> guard(mutex_);
	toolbox::net::URN urn("toolbox-task-timer", name);
	return this->hasTimer(urn);	
}

bool toolbox::task::TimerFactory::hasTimer(toolbox::net::URN & urn)
{
	toolbox::task::Guard <toolbox::BSem> guard(mutex_);
	std::string index = urn.toString();
	std::map<std::string, toolbox::task::Timer*, std::less<std::string> >::iterator i = timers_.find(index);
	if (i != timers_.end())
	{
		return true;
	} 
	else
	{
		return false;
	}
}

toolbox::task::Timer* toolbox::task::TimerFactory::getTimer(const std::string & name) throw (toolbox::task::exception::Exception)
{
	toolbox::task::Guard <toolbox::BSem> guard(mutex_);
	toolbox::net::URN urn("toolbox-task-timer", name);
	return this->getTimer(urn);	
}

toolbox::task::Timer* toolbox::task::TimerFactory::getTimer(toolbox::net::URN & urn) throw (toolbox::task::exception::Exception)
{
	toolbox::task::Guard <toolbox::BSem> guard(mutex_);
	std::string index = urn.toString();
	
	std::map<std::string, toolbox::task::Timer*, std::less<std::string> >::iterator i = timers_.find(index);
	if (i != timers_.end())
	{
		return (*i).second;
	} 
	else
	{
		std::string msg = "Timer not found: ";
		msg += index;
		XCEPT_RAISE (toolbox::task::exception::Exception, msg);
	}
}
		
toolbox::task::Timer* toolbox::task::TimerFactory::createTimer(const std::string & name) throw (toolbox::task::exception::Exception)
{
	toolbox::task::Guard <toolbox::BSem> guard(mutex_);
	toolbox::net::URN urn("toolbox-task-timer", name);
	return this->createTimer(urn);	
}

toolbox::task::Timer* toolbox::task::TimerFactory::createTimer(toolbox::net::URN & urn) throw (toolbox::task::exception::Exception)
{
	toolbox::task::Guard <toolbox::BSem> guard(mutex_);
	std::string index = urn.toString();
	
	std::map<std::string, toolbox::task::Timer*, std::less<std::string> >::iterator i = timers_.find(index);
	if (i != timers_.end())
	{
		std::string msg = "Cannot create timer, already existing: ";
		msg += index;
		XCEPT_RAISE (toolbox::task::exception::Exception, msg);
	}
	else
	{
		std::string name = urn.getNSS();
		
		toolbox::task::Timer* t = new toolbox::task::Timer(name);
		timers_[index] = t;
		
		t->start(); // activate timer thread
		
		return t;
	}
}

void toolbox::task::TimerFactory::removeTimer(const std::string & name) throw (toolbox::task::exception::Exception)
{
	toolbox::task::Guard <toolbox::BSem> guard(mutex_);
	toolbox::net::URN urn("toolbox-task-timer", name);
	this->removeTimer(urn);	
}
			
void toolbox::task::TimerFactory::removeTimer(toolbox::net::URN & urn) throw (toolbox::task::exception::Exception)
{	
	toolbox::task::Guard <toolbox::BSem> guard(mutex_);
	std::string index = urn.toString();
	std::map<std::string, toolbox::task::Timer*, std::less<std::string> >::iterator i = timers_.find(index);
	if (i != timers_.end())
	{
		toolbox::task::Timer* t = (*i).second;
		
		if (t->isActive())
			t->stop();
			
		delete t;
			
		timers_.erase(i);
	}
	else
	{
		std::string msg = "Requested timer for removal not found: ";
		msg += index;
		XCEPT_RAISE (toolbox::task::exception::Exception, msg);
	}
}

std::list<toolbox::task::Timer*> toolbox::task::TimerFactory::getTimers()
{
	toolbox::task::Guard <toolbox::BSem> guard(mutex_);
	std::list<toolbox::task::Timer*> l;
	std::map<std::string, toolbox::task::Timer*, std::less<std::string> >::iterator i;
	for (i = timers_.begin(); i != timers_.end(); i++)
	{
		l.push_back((*i).second);
	}
	return l;
}

//! Retrieve a pointer to the toolbox::task::TimerFactory singleton
//
toolbox::task::TimerFactory* toolbox::task::TimerFactory::getInstance()
{
	toolbox::task::Guard <toolbox::BSem> guard(mutex_);
	if (instance_ == 0)
	{
		instance_ = new toolbox::task::TimerFactory();
	}
	return instance_;
}

//! Destoy the factory and all associated work loops
//
void toolbox::task::TimerFactory::destroyInstance()
{
	toolbox::task::Guard <toolbox::BSem> guard(mutex_);
	if (instance_ != 0)
	{
		delete instance_;
		instance_ = 0;
	}
}

		
toolbox::task::TimerFactory* toolbox::task::getTimerFactory()
{
	return toolbox::task::TimerFactory::getInstance();
}
