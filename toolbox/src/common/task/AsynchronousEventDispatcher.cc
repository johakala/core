// $Id: AsynchronousEventDispatcher.cc,v 1.8 2008/07/18 15:27:41 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "toolbox/task/AsynchronousEventDispatcher.h"
#include "toolbox/task/WorkLoopFactory.h"
#include "toolbox/task/exception/Exception.h"
#include "xcept/tools.h"
#include "toolbox/utils.h"

size_t toolbox_task_AsynchronousEventDispatcher_queueSize;

toolbox::task::AsynchronousEventDispatcher::AsynchronousEventDispatcher(const std::string & workloop,const std::string & type, double threshold, size_t qsize ):
	mutex_ (BSem::FULL,true)
{
	processNotificationJob_ = toolbox::task::bind (this, &AsynchronousEventDispatcher::processNotification, "processNotification");
	workLoop_ = toolbox::task::getWorkLoopFactory()->getWorkLoop(workloop, type);
	std::string name = "notificationq-" + workloop + "-" + type;
	notificationQueue_ = toolbox::rlist<toolbox::task::EventReference*>::create(name,qsize);
	threshold_ = (size_t)(qsize * threshold); // e.g 0.1 means 10%
 	deadBand_ = false;

 	// this queue should always big enough to accept any buffer that have been pushed into notificationQueue_. So that the squeue associated with this workloop will never
 	// produce an internal error. If internal error happen is FATAL
 	workLoop_->resize(qsize);

}  

toolbox::task::AsynchronousEventDispatcher::~AsynchronousEventDispatcher()
{
	 toolbox::rlist<toolbox::task::EventReference*>::destroy(notificationQueue_);
}

void toolbox::task::AsynchronousEventDispatcher::addActionListener( toolbox::ActionListener * l ) throw (toolbox::exception::Exception)
{
	dispatcher_.addActionListener(l);
}

void toolbox::task::AsynchronousEventDispatcher::removeActionListener( toolbox::ActionListener * l ) throw (toolbox::exception::Exception)
{
	dispatcher_.removeActionListener(l);
}


bool toolbox::task::AsynchronousEventDispatcher::processNotification(toolbox::task::WorkLoop* wl)
{
	// pop message from queue
	// does not need to be locked, since rlist is thread safe between
	// one producer and once consumer. Since producers all lock each other
	// they fall back to being seen as a single producer
	//
	EventReference* event = notificationQueue_->front();
	notificationQueue_->pop_front();

	try
	{
		dispatcher_.fireEvent( **event );
	}
	catch (toolbox::exception::Exception & e)
	{
#warning "Asynchronous error dispatching to be filled in"
		std::cout << xcept::stdformat_exception_history(e) << std::endl;
	}	

	delete event;

	return false;
}

void toolbox::task::AsynchronousEventDispatcher::fireEvent ( toolbox::task::EventReference & e) 
	throw (toolbox::task::exception::OverThreshold, 
	toolbox::task::exception::Overflow,
	toolbox::task::exception::InternalError )
{
	mutex_.take();

	if ( notificationQueue_->elements() < threshold_ ) 
	{
		if ( deadBand_ ) 
		{ // I am crossing the threshold and I was in the dead zone, now I can push data again
			
			deadBand_ = false;
		}	
	}
	else 
	{
		if ( deadBand_ )  // I am in dead zone so I discard the data
		{
			
			mutex_.give();
			XCEPT_RAISE (toolbox::task::exception::OverThreshold, "event discarded (deadband)");			
			
		}	

	}	

	toolbox::task::EventReference * event = new toolbox::task::EventReference(e);
	try
        {
                notificationQueue_->push_back(event);
        }
        catch (toolbox::exception::RingListFull& rlf)
        {
		delete event;
		// I went over the ceiling therefore I activate the dead zone
		deadBand_ = true;
		
		mutex_.give();
		
		XCEPT_RETHROW (toolbox::task::exception::Overflow,"event discarded", rlf );	
        }

	mutex_.give();

        // submit a job                
        try
        {
                workLoop_->submit(processNotificationJob_);                    
        }
        catch (toolbox::task::exception::Exception& e)
        {        
                XCEPT_RETHROW(toolbox::task::exception::InternalError, "Failed to submit processing job, a reference pointer has been lost", e);
		 
        }
        catch (std::exception& se)
        {
                std::stringstream msg;
		msg << "Failed to submit notification to worker pool, caught standard exception '";
		msg << se.what() << "'";
                XCEPT_RAISE(toolbox::task::exception::InternalError, msg.str());
		
        }       
        catch (...)
        {
		
                XCEPT_RAISE(toolbox::task::exception::InternalError, "Failed to submit notification to worker pool, caught unknown exception");
        }
}		
/*
void toolbox::task::AsynchronousEventDispatcher::fireEvent ( toolbox::task::EventReference & e , bool throwex) throw (toolbox::task::exception::Exception)
{
	mutex_.take();

	size_t retry = 0;               
        while (retry < 3)
        {
                try
                {
			toolbox::task::EventReference * event = new toolbox::task::EventReference(e);
                        notificationQueue_.push_back(event);
                        break; // break out of loop, go on with processing
                }
                catch (toolbox::exception::RingListFull& rlf)
                {
                        ++retry;
                        toolbox::u_sleep(100);
                }
        }

        if (retry == 3)
        {
                std::stringstream msg;
                msg << "Failed to enqueue notification into worker pool after " << retry << " attempts";
		mutex_.give();
		if ( throwex ) 
		{
                	XCEPT_RAISE (toolbox::task::exception::Exception, msg.str() );
		}
		else
		{
			// silently discard
			return;
		}	
        }

	mutex_.give();

        // submit a job                
        try
        {
                workLoop_->submit(processNotificationJob_);                    
        }
        catch (toolbox::task::exception::Exception& e)
        {        
		if ( throwex ) 
		{
                	XCEPT_RETHROW(toolbox::task::exception::Exception, "Failed to submit processing job", e);
		}
		else
		{
			// silently discard
			return;
		}	               
        }
        catch (std::exception& se)
        {
		
		if ( throwex ) 
		{
                	std::stringstream msg;
			msg << "Failed to submit notification to worker pool, caught standard exception '";
			msg << se.what() << "'";
                	XCEPT_RAISE(toolbox::task::exception::Exception, msg.str());
		}
		else
		{
			// silently discard
			return;
		}	
        }       
        catch (...)
        {
		if ( throwex ) 
		{
                	XCEPT_RAISE(toolbox::task::exception::Exception, "Failed to submit notification to worker pool, caught unknown exception");

		}
		else
		{
			// silently discard
			return;
		}
        }
}
*/

	
