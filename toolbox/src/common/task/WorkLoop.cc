// $Id: WorkLoop.cc,v 1.13 2008/07/18 15:27:41 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "toolbox/task/WorkLoop.h"
#include <signal.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include "toolbox/string.h"
#include "toolbox/PolicyFactory.h"
#include "toolbox/Policy.h"
#include "toolbox/exception/Exception.h"
#include <iostream>
#include "xcept/tools.h"
#include <unistd.h>

#include <sys/syscall.h>    /* For SYS_xxx definitions */

static void terminate_workloophandler()
{
	std::cout << "Uncaught exception in workloop thread" << std::endl;
#ifdef __GNUC__
	__gnu_cxx::__verbose_terminate_handler();
#else
	abort()
#endif /* __GNUC__ */
}

static void unexpected_workloophandler()
{
	std::cout << "Unexpected exception in workloop thread" << std::endl;
#ifdef __GNUC__
	__gnu_cxx::__verbose_terminate_handler();
#else
	abort()
#endif /* __GNUC__ */
}

void * toolbox::task::thread_func (void* workLoop)
{
	// block all signals, only main thread shall handle signals
	sigset_t mask;
	sigemptyset(&mask);
	sigaddset(&mask, SIGINT);
	sigaddset(&mask, SIGQUIT);
	sigaddset(&mask, SIGTERM);
	sigaddset(&mask, SIGPIPE);
	sigaddset(&mask, SIGABRT);
	sigaddset(&mask, SIGSEGV);		
	sigprocmask(SIG_BLOCK,&mask,0);

        std::set_terminate(terminate_workloophandler);
        std::set_unexpected(unexpected_workloophandler);

	toolbox::task::WorkLoop* wl = (toolbox::task::WorkLoop*) workLoop;
	wl->run();
	pthread_exit(0);
	return 0;
}


toolbox::task::WorkLoop::WorkLoop(std::string name, std::string type)
{
	name_ = name;
	type_ = type;
	active_ = false;
	size_ = 1024;
}

toolbox::task::WorkLoop::~WorkLoop()
{
	if ( this->isActive() )
		this->cancel();
}

	
bool toolbox::task::WorkLoop::isActive()
{
	return active_;
}
	
void toolbox::task::WorkLoop::activate() throw (toolbox::task::exception::Exception)
{
	if (this->isActive())
	{
		std::string msg = "Work loop cannot be activated, is already active: ";
		msg += name_;
		XCEPT_RAISE (toolbox::task::exception::Exception, msg);
	}
	else
	{
		// Initialize thread attributes with default values
		pthread_attr_init(&thread_attributes_);

		// set detached so that all resources are cleaned up automatically
		//pthread_attr_setdetachstate(&thread_attributes_, PTHREAD_CREATE_DETACHED);
		pthread_attr_setdetachstate(&thread_attributes_, PTHREAD_CREATE_JOINABLE);

		//int cancelState;

		//pthread_setcanceltype(PTHREAD_CANCEL_ENABLE, &cancelState);
		//pthread_setcanceltype(PTHREAD_CANCEL_DEFERRED, &cancelState);
		
		active_ = false;
		errno = 0;
		if (pthread_create ( &thread_id_, &thread_attributes_, thread_func, (void*) this) != 0)
		{
			XCEPT_RAISE (toolbox::task::exception::Exception, strerror(errno));
		}
		// the workloop  will become active within the run() method(in the new created thread)
		 while (! active_ ) { sched_yield();}
		//active_ = true;
	}

}


void toolbox::task::WorkLoop::cancel() throw (toolbox::task::exception::Exception)
{
	if (!this->isActive())
	{
		std::string msg = toolbox::toString("Cannot cancel non active WorkLoop: %s",this->getName().c_str());
		XCEPT_RAISE (toolbox::task::exception::Exception, msg);
	}
	else
	{
		//trigger the exiting of the thread
		stopped_ = true;

		// make sure the thread is over
		pthread_join(thread_id_, NULL);
		pthread_attr_destroy(&thread_attributes_);
		// now it is possible to active the thread again
		active_ = false;
	}
}



std::string toolbox::task::WorkLoop::getName()
{
	return name_;
}

std::string toolbox::task::WorkLoop::getType()
{
	return type_;
}

void toolbox::task::WorkLoop::run()
{
	stopped_ = false;
	// now I can really say that all conditions are satisifed and the thread is active
	// the cancel() will always work

	toolbox::PolicyFactory * factory = toolbox::getPolicyFactory();
	toolbox::Policy * policy = 0;
	
	try
	{
		toolbox::net::URN urn("toolbox-task-workloop", name_);
		policy = factory->getPolicy(urn, "thread");
		policy->setDefaultPolicy();
#ifdef linux                
		// add SYS_gettid
		pid_t const thread_id = pid_t(::syscall(SYS_gettid));
		toolbox::net::URN tidurn("toolbox-task-workloop-SYS_gettid",name_);
		policy->setProperty(tidurn.toString(),toolbox::toString("%d",thread_id));
#else
		// not implemented yet
#endif
	}
	catch (toolbox::exception::Exception & e)
	{
		std::cout << xcept::stdformat_exception_history(e) << std::endl;
		XCEPT_RETHROW (toolbox::exception::Exception, "Panic - failed setting Policy for WorkLoop", e);
	}

	active_ = true;
	while ( ! stopped_ )
	{
		this->process();
	}
}

void toolbox::task::WorkLoop::resize(size_t n) throw (toolbox::task::exception::Exception)
{
}


/*
void toolbox::task::WorkLoop::cancel() throw (toolbox::task::exception::Exception)
{
	if (! this->isActive())
	{
		std::string msg = "Work loop cannot be cancelled, is not active: ";
		msg += name_;
		XCEPT_RAISE (toolbox::task::exception::Exception, msg);	
	}
	else
	{
		active_ = false;
		pthread_cancel(thread_id_);
		pthread_join(thread_id_, NULL);
		pthread_attr_destroy(&thread_attributes_);
	}
}
*/
