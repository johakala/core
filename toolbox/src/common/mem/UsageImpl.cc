// $Id: UsageImpl.cc,v 1.5 2008/07/18 15:27:40 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "toolbox/mem/UsageImpl.h"
#include "toolbox/mem/SmartBufPool.h"
#include "toolbox/mem/Allocator.h"
#include "toolbox/mem/BufPoolEntry.h"
#include <sstream>

toolbox::mem::UsageImpl::UsageImpl(toolbox::mem::SmartBufPool * pool)
{
	pool_ = pool;
}


toolbox::mem::UsageImpl::~UsageImpl()
{
	
}

		
size_t toolbox::mem::UsageImpl::getCommitted()
{
	if (pool_->allocator_->isCommittedSizeSupported())
	{
		return pool_->allocator_->getCommittedSize();
	}
	else
	{
		return 0;
	}
}
		
size_t toolbox::mem::UsageImpl::getUsed()
{
	return pool_->used_;
}
		
size_t toolbox::mem::UsageImpl::getMax()
{
#warning "Not implemented. Should come from allocator"
	return 0;
}
		
size_t toolbox::mem::UsageImpl::getAllocatedBlocks(size_t size)
{
	size_t index = BUF_SIZE_TO_BUF_INDEX(size);
	if (size > BUF_SIZE_MAX)
	{
		//std::stringstream msg;
		//msg << "Specified size " << size << " too big.";
		//XCEPT_RAISE (toolbox::mem::exception::SizeExceeded, msg.str());
		return 0;
	}
	
	return pool_->bufPoolTable_[BUF_LOG2_TO_BUF_INDEX(index)]->getNumberOfAllocatedBlocks();
}

size_t toolbox::mem::UsageImpl::getCachedBlocks(size_t size)
{
	size_t index = BUF_SIZE_TO_BUF_INDEX(size);
	if (size > BUF_SIZE_MAX)
	{
		//std::stringstream msg;
		//msg << "Specified size " << size << " too big.";
		//XCEPT_RAISE (toolbox::mem::exception::SizeExceeded, msg.str());
		return 0;
	}
	return pool_->bufPoolTable_[BUF_LOG2_TO_BUF_INDEX(index)]->getNumberOfCachedBlocks();
}

std::vector<size_t> toolbox::mem::UsageImpl::getBlockSizes()
{
	std::vector<size_t> v;
	// Loop over all BufPoolEntry elements in the bufPoolTable_ array
	// Start with entry BUF_LOG2_MIN that is caching 64 Bytes blocks. All smaller ones are 0
	//
	for (size_t i = BUF_LOG2_MIN; i <= BUF_LOG2_MAX; i++)
	{
		if (pool_->bufPoolTable_[BUF_LOG2_TO_BUF_INDEX(i)]->getNumberOfCreatedBlocks() > 0)
		{
			v.push_back(BUF_LOG2_TO_BUF_SIZE(i));
		}
	}
	return v;
}


std::map<size_t, size_t, std::less<size_t> >& 
toolbox::mem::UsageImpl::getAllocatedBlocks()
{
	// update map
	for (size_t i = BUF_LOG2_MIN; i <= BUF_LOG2_MAX; i++)
	{
		//std::cout << "getting number of blocks from index:" << i << " in size " << BUF_LOG2_TO_BUF_SIZE(i) << ": " << pool_->bufPoolTable_[i]->getNumberOfAllocatedBlocks() << endl;
		
		pool_->bufPoolTable_[BUF_LOG2_TO_BUF_INDEX(i)]->show();
		allocatedBlocksMap_[BUF_LOG2_TO_BUF_SIZE(i)] = pool_->bufPoolTable_[BUF_LOG2_TO_BUF_INDEX(i)]->getNumberOfAllocatedBlocks();		
	}
	// return map
	pool_->show();
	return allocatedBlocksMap_;
}


std::map<size_t, size_t, std::less<size_t> >&
toolbox::mem::UsageImpl::getCachedBlocks()
{
	// update map
	for (size_t i = BUF_LOG2_MIN; i <= BUF_LOG2_MAX; i++)
	{
		cachedBlocksMap_[BUF_LOG2_TO_BUF_SIZE(i)] = pool_->bufPoolTable_[BUF_LOG2_TO_BUF_INDEX(i)]->getNumberOfCachedBlocks();		
	}
	// return map
	return cachedBlocksMap_;
}

std::map<size_t, size_t, std::less<size_t> >&
toolbox::mem::UsageImpl::getCreatedBlocks()
{
	// update map
	for (size_t i = BUF_LOG2_MIN; i <= BUF_LOG2_MAX; i++)
	{
		createdBlocksMap_[BUF_LOG2_TO_BUF_SIZE(i)] = pool_->bufPoolTable_[BUF_LOG2_TO_BUF_INDEX(i)]->getNumberOfCreatedBlocks();		
	}
	// return map
	return createdBlocksMap_;
}

