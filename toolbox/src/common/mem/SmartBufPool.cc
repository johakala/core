// $Id: SmartBufPool.cc,v 1.11 2008/07/18 15:27:40 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "toolbox/mem/SmartBufPool.h"
#include "toolbox/mem/BufPoolEntry.h"

#include "toolbox/string.h"
#include "toolbox/mem/exception/OutOfMemory.h"
#include "toolbox/mem/exception/SizeExceeded.h"
#include "toolbox/mem/Event.h"


#include "toolbox/BSem.h"

const unsigned char log2_roundup_table[257] = {
  0,
  0, 1, 2, 2, 3, 3, 3, 3, 4, 4, 4, 4, 4, 4, 4, 4,
  5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5,
  6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6,
  6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6,
  7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7,
  7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7,
  7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7,
  7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7,
  8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8,
  8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8,
  8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8,
  8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8,
  8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8,
  8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8,
  8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8,
  8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8,
};

size_t log2_roundup (size_t n)
{
  size_t ret;

  if (n <= 0x100)
    ret = log2_roundup_table[n] + 0;
  else
    {
      n = (n+0xff)>>8 & 0xffffff;
      if (n <= 0x100)
        ret = log2_roundup_table[n] + 8;
      else
        {
          n = (n+0xff)>>8;
          if (n <= 0x100)
            ret = log2_roundup_table[n] + 16;
          else
            {
              n = (n+0xff)>>8;
              ret = log2_roundup_table[n] + 24;
            }
        }
    }
  return ret;
}



toolbox::mem::SmartBufPool::SmartBufPool(toolbox::mem::Allocator * allocator)
	throw (toolbox::mem::exception::FailedCreation)
	: usage_(this)
{
	allocator_ = allocator;
	lowThreshold_ = 0;
	highThreshold_ = 0;
	used_ = 0;
	location_ = -1; // in low section of thresholds, no buffers are used
	
	// create pool entries (x log2)
	for (size_t i=0; i < BUF_TBL_SIZE; i++ )
	{
		//std::cout << "inserting pool entry on:" << i << std::endl;
		bufPoolTable_[i] = new BufPoolEntry(BUF_LOG2_TO_BUF_SIZE(BUF_INDEX_TO_LOG2(i)),allocator);
	}

	mutex_ = new BSem(BSem::FULL);
}
	
	
toolbox::mem::SmartBufPool::~SmartBufPool() 
{
	for (size_t i=0; i < BUF_TBL_SIZE; i++ )
	{
		delete bufPoolTable_[i];
	}
	delete mutex_;
}	
	
	
void toolbox::mem::SmartBufPool::invalidate (size_t size)
	throw (toolbox::mem::exception::FailedDispose)
{
	this->lock();
	try
	{
		bufPoolTable_[BUF_SIZE_TO_BUF_INDEX(size)]->invalidate();
		this->unlock();
	} 
	catch (toolbox::mem::exception::FailedDispose& fd)
	{
		this->unlock();
		std::string msg = toolbox::toString ("Failed to invalidate pool entry size %d", size);
		XCEPT_RETHROW (toolbox::mem::exception::FailedDispose, msg, fd);
	}
}

void toolbox::mem::SmartBufPool::invalidate ()
	throw (toolbox::mem::exception::FailedDispose)
{
	this->lock();
	try
	{
		for (size_t i = 0; i < BUF_TBL_SIZE; i++)
		{
			bufPoolTable_[i]->invalidate();
		}
		this->unlock();
	} 
	catch (toolbox::mem::exception::FailedDispose& fd)
	{
		this->unlock();
		XCEPT_RETHROW (toolbox::mem::exception::FailedDispose, "Failed to invalidate pool", fd);
	}
}


toolbox::mem::Buffer * toolbox::mem::SmartBufPool::alloc(size_t size)
	throw (toolbox::mem::exception::FailedAllocation)
{
	if ( size > BUF_SIZE_MAX  ) 
	{ 		
		std::string msg = toolbox::toString("Exceeded maximum buffer size %d. Requested size: %d", BUF_SIZE_MAX, size);
		XCEPT_RAISE (toolbox::mem::exception::FailedAllocation, msg);
	}
	
	try
	{	
		toolbox::mem::Buffer * b = bufPoolTable_[BUF_SIZE_TO_BUF_INDEX(size)]->alloc(this);
		used_ += b->getSize();
		
		// check if threshold over or under and fire appropriate event
		if (location_ == 0)
		{
			// in normal
			if (this->isHighThresholdExceeded())
			{
				// crossing from normal to upper
				location_ = 1;
				toolbox::mem::PoolUpperThresholdCrossed e(toolbox::mem::up, this);
				dispatcher_.fireEvent(e);
			}
		} 
		else if (location_ == 1)
		{
			// in upper section
			// an allocation cannot cause another crossing, already in upper section
		} 
		else if (location_ == -1)
		{
			// in lower section
			// an allocation can only cause a crossing to normal;
			if (this->isLowThresholdExceeded())
			{
				// crossing from lower to normal
				location_ = 0;
				toolbox::mem::PoolLowerThresholdCrossed e(toolbox::mem::up, this);
				dispatcher_.fireEvent(e);
			}
		}
		
		return b;		
	}
	catch (toolbox::mem::exception::FailedAllocation& fa)
	{
		std::string msg = toolbox::toString ("Out of memory in allocating buffer with %d bytes from pool", size);
		XCEPT_RETHROW (toolbox::mem::exception::FailedAllocation, msg, fa);
	}
}
	
	
void toolbox::mem::SmartBufPool::free ( toolbox::mem::Buffer* buffer )
	throw (toolbox::mem::exception::FailedDispose)
{
	try
	{
		size_t size = buffer->getSize();
		bufPoolTable_[BUF_SIZE_TO_BUF_INDEX(size)]->free(buffer);
		used_ -= size;
		
		// check if threshold over or under and fire appropriate event
		if (location_ == 0)
		{
			// in normal
			if (!this->isLowThresholdExceeded())
			{
				// crossing from normal to lower
				location_ = -1;
				toolbox::mem::PoolLowerThresholdCrossed e(toolbox::mem::down, this);
				dispatcher_.fireEvent(e);
			}
		} 
		else if (location_ == 1)
		{
			// in upper section
			// an free can cause a crossing to the lower section
			if (!this->isHighThresholdExceeded())
			{
				// crossing from lower to normal
				location_ = 0;
				toolbox::mem::PoolUpperThresholdCrossed e(toolbox::mem::down, this);
				dispatcher_.fireEvent(e);
			}
		} 
		else if (location_ == -1)
		{
			// in lower section
			// a free in the lower cannot cause another threshold crossing
		}
		
	} 
	catch (toolbox::mem::exception::FailedDispose& fd)
	{
		std::string msg = toolbox::toString ("Failed to free buffer");
		XCEPT_RETHROW (toolbox::mem::exception::FailedAllocation, msg, fd);
	}
}
	
	
void toolbox::mem::SmartBufPool::lock() 
{
	mutex_->take();
}
	
	
void toolbox::mem::SmartBufPool::unlock() 
{
	mutex_->give();	
}
	
	
void toolbox::mem::SmartBufPool::show() 
{
	std::cout << "SmartPoolBuf entries" << std::endl;
	for (size_t i=0; i < BUF_TBL_SIZE; i++ ) 
	{
		bufPoolTable_[i]->show();
	}
}
	
	
size_t toolbox::mem::SmartBufPool::getHighThreshold () 
{
	return highThreshold_;
}

size_t toolbox::mem::SmartBufPool::getLowThreshold () 
{
	return lowThreshold_;
}


void toolbox::mem::SmartBufPool::setHighThreshold ( size_t nbytes) 
	throw (toolbox::mem::exception::InvalidThreshold)
{
	if (allocator_->isCommittedSizeSupported())
	{
		if (nbytes < allocator_->getCommittedSize())
		{
			highThreshold_ = nbytes;
		}
		else
		{
			std::string msg = toolbox::toString ("Threshold %d higher/equal than commited size %d", nbytes, allocator_->getCommittedSize());
			XCEPT_RAISE (toolbox::mem::exception::InvalidThreshold, msg);
		}
	}
	else
	{
		highThreshold_ = nbytes;
	}
}

void toolbox::mem::SmartBufPool::setLowThreshold ( size_t nbytes) 
	throw (toolbox::mem::exception::InvalidThreshold)
{
	if (allocator_->isCommittedSizeSupported())
	{
		if (nbytes < allocator_->getCommittedSize())
		{
			lowThreshold_ = nbytes;
		}
		else
		{
			std::string msg = toolbox::toString ("Threshold %d higher/equal than commited size %d", nbytes, allocator_->getCommittedSize());
			XCEPT_RAISE (toolbox::mem::exception::InvalidThreshold, msg);
		}
	}
	else
	{
		lowThreshold_ = nbytes;
	}
}

//size_t toolbox::mem::SmartBufPool::getUsed() 
//{
//	return used_;
//}

bool toolbox::mem::SmartBufPool::isHighThresholdExceeded ()
{
	if (highThreshold_ && (used_ > highThreshold_)) 
	{
		return true;
	}
	else
	{
		return false;
	}	
	
}

bool toolbox::mem::SmartBufPool::isLowThresholdExceeded ()
{
	if (lowThreshold_ && (used_ > lowThreshold_)) 
	{
		return true;
	}
	else
	{
		return false;
	}	
	
}

void toolbox::mem::SmartBufPool::addActionListener (toolbox::ActionListener* listener)
{
	dispatcher_.addActionListener(listener);
}
			
void toolbox::mem::SmartBufPool::removeActionListener (toolbox::ActionListener* listener) throw (toolbox::exception::Exception)
{
	dispatcher_.removeActionListener(listener);
}

toolbox::mem::Allocator * toolbox::mem::SmartBufPool::getAllocator()
{
	return allocator_;
}
	
toolbox::mem::Usage & toolbox::mem::SmartBufPool::getMemoryUsage()
{
	return usage_;
}
