// $Id: MemoryPartition.cc,v 1.11 2008/07/18 15:27:40 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "toolbox/mem/MemoryPartition.h"
#include "toolbox/string.h"

#include <iostream>
#include <sstream>
#include <stdio.h>
#include <ctype.h>

#include <assert.h>
#include <memory.h>


// Declare the interface, including the requested buffer size type,
// bufsize. 

#define MemSize     size_t               // Type for size arguments to memxxx()
                                      //   functions such as memcmp(). 
		
#define BH(p)	((struct bhead *) (p))
#define BDH(p)	((struct bdhead *) (p))
#define BFH(p)	((struct bfhead *) (p))

//  Minimum allocation quantum: 
		
#define QLSize	(sizeof(struct qlinks))
#define SizeQ	((SizeQuant > QLSize) ? SizeQuant : QLSize)
#define V   (void)		      // To denote unwanted returned values
		
// End sentinel: value placed in bsize field of dummy block delimiting
// end of pool block.  The most negative number which will  fit  in  a
// BufferSizeType, defined in a way that the compiler will accept.
		
#define ESent	((BufferSizeType) (-(((1L << (sizeof(BufferSizeType) * 8 - 2)) - 1) * 2) - 2))


toolbox::mem::MemoryPartition::MemoryPartition ()
{
	
	freelist_.bh.prevfree = 0;
	freelist_.bh.bsize    = 0;
	freelist_.ql.flink = &freelist_;
	freelist_.ql.blink = &freelist_;
	
#ifdef BufStats
	
	totalloc_ = 0; // Total space currently allocated 
	numget_ = 0;
	numrel_ = 0;   // Number of bget() and brel() calls
	
#ifdef BECtl
	
	numpblk_ = 0;  // Number of pool blocks 
	numpget_ = 0;
	numprel_ = 0;  // Number of block gets and rels
	numdget_ = 0;
	numdrel_ = 0;  //Number of direct gets and rels 
	
#endif // BECtl 
#endif // BufStats 
	
#ifdef BECtl
	
	// Automatic expansion block management functions 
	
	compfcn_ = NULL;
	acqfcn_ = NULL;
	relfcn_ = NULL;
	
	exp_incr_ = 0;	      /// Expansion block size 
	pool_len_ = 0;	      // 0: no bpool calls have been made
						  //	 -1: not all pool blocks are
						  //	     the same size
						  //	 >0: (common) block size for all
						  //	     bpool calls made so far
	
#endif	
}

void * toolbox::mem::MemoryPartition::virtualToPhysical (void * userAddr)
	throw (toolbox::mem::exception::InvalidAddress)
{
	std::map<void *, struct MemoryChunk, std::less<void*> >::iterator i = memoryPoolMap_.begin();
	
	for ( i = memoryPoolMap_.begin(); i != memoryPoolMap_.end(); i++ )
	{
		size_t memory_chunk_start = (size_t)(*i).second.userAddress;
		size_t memory_chunk_end = memory_chunk_start +  (*i).second.size;
		
		// if address contained in range, then calculate physical address
		if (( (size_t)userAddr <= memory_chunk_end ) && ((size_t)userAddr >= memory_chunk_start) ) 
		{
			return (void*)((size_t)((*i).first) + ((size_t)userAddr - (memory_chunk_start)));		
		}	
	}
	std::string msg = toolbox::toString("Cannot find physical mapping for virtual address %x", userAddr);
	XCEPT_RAISE (toolbox::mem::exception::InvalidAddress, msg);
}

void * toolbox::mem::MemoryPartition::virtualToKernel (void * userAddr)
	throw (toolbox::mem::exception::InvalidAddress)
{
	std::map<void *, struct MemoryChunk, std::less<void*> >::iterator i = memoryPoolMap_.begin();
	
	for ( i = memoryPoolMap_.begin(); i != memoryPoolMap_.end(); i++ )
	{
		size_t memory_chunk_start = (size_t)(*i).second.userAddress;
		size_t memory_chunk_end = memory_chunk_start +  (*i).second.size;
		
		//if address contained in range, then calculate physical address
		if (( (size_t)userAddr <= memory_chunk_end ) && ((size_t)userAddr >= memory_chunk_start) ) 
		{
			size_t offset = (size_t)((size_t)userAddr) - memory_chunk_start;
			return (void*)((size_t)((*i).second.kernelAddress) + offset);		
		}	
	}
	std::string msg = toolbox::toString("Cannot find kernel space mapping for virtual address %x", userAddr);
	XCEPT_RAISE (toolbox::mem::exception::InvalidAddress, msg);
}


void * toolbox::mem::MemoryPartition::physicalToVirtual (void * physAddr)
	throw (toolbox::mem::exception::InvalidAddress)
{
	std::map<void *, struct MemoryChunk, std::less<void*> >::iterator i = memoryPoolMap_.begin();

	for ( i = memoryPoolMap_.begin(); i != memoryPoolMap_.end(); i++ )
	{
		size_t phys_memory_chunk_start = (size_t)((*i).first);
		size_t phys_memory_chunk_end = phys_memory_chunk_start +  (size_t)(*i).second.size;
		
		//if address contained in range, then calculate physical address
		if (( (size_t)physAddr <= phys_memory_chunk_end ) && ((size_t)physAddr >= phys_memory_chunk_start) ) 
		{
			return (void*)((size_t)((*i).second.userAddress) + ((size_t)physAddr - (phys_memory_chunk_start)));		
		}	
	}
	std::string msg = toolbox::toString("Cannot find virtual address mapping for physical address %x", physAddr);
	XCEPT_RAISE (toolbox::mem::exception::InvalidAddress, msg);
}


void toolbox::mem::MemoryPartition::addToPool (void * physAddr, void * kernelAddr, void * userAddr, BufferSizeType len)
	throw (toolbox::mem::exception::Corruption, toolbox::mem::exception::InvalidAddress, toolbox::mem::exception::MemoryOverflow)
{
	struct bfhead *b = BFH(userAddr);
	struct bhead *bn;
	
#ifdef SizeQuant
	len &= ~(SizeQuant - 1);
#endif
	
#ifdef BECtl
	if (pool_len_ == 0) 
	{
		pool_len_ = len;
	} 
	else if (len != pool_len_) 
	{
		pool_len_ = -1;
	}
	
	
	
	
#ifdef BufStats
	
	numpget_++;			      // Number of block acquisitions 
	numpblk_++;			      // Number of blocks total 
	
	// assert(numpblk_ == numpget_ - numprel_);	
	// Assure that the total number of blocks in the pool is equal to
	// the number of blocks retrieved minus the blocks created
	if !(numpblk_ == numpget_ - numprel_)
	{
		std::string msg = std::toString("Total number of blocks (%d) not equal to blocks retrieved (%d) minus blocks created (%d)", numpblk_,numpget_,numprel_);
		XCEPT_RAISE (toolbox::mem::exception::Corruption, msg);
	}
	
#endif // BufStats 
#endif // BECtl 
	
	// record mapping of virtual adddress
	MemoryChunk mchunk ;
	mchunk.size = len;
	mchunk.userAddress = userAddr;
	mchunk.kernelAddress = kernelAddr;
	
	if (memoryPoolMap_.find(physAddr) != memoryPoolMap_.end())
	{
		std::string msg = toolbox::toString("Physical address %x already added", physAddr);
		XCEPT_RAISE (toolbox::mem::exception::InvalidAddress, msg);	
	}
	
	// Since the block is initially occupied by a single free  buffer,
	// it  had better not  be  (much) larger than the largest buffer
	// whose size we can store in bhead.bsize. 
	
	// assert(len - sizeof(struct bhead) <= -((BufferSizeType) ESent + 1));
	
	if (!(len - sizeof(struct bhead) <= -((BufferSizeType) ESent + 1)))
	{
		std::string msg = toolbox::toString("Memory overflow, added %d bytes at physical address %x", len, physAddr);
		XCEPT_RAISE (toolbox::mem::exception::MemoryOverflow, msg);	
	}
	
	memoryPoolMap_[physAddr] = mchunk;
	
	// Clear  the  backpointer at  the start of the block to indicate that
	// there  is  no  free  block  prior  to  this   one.    That   blocks
	// recombination when the first block in memory is released. */
		
	b->bh.prevfree = 0;
		
	// Chain the new block to the free list. 
		
	// assert(freelist_.ql.blink->ql.flink == &freelist_);
	// assert(freelist_.ql.flink->ql.blink == &freelist_);
	
	if (!(freelist_.ql.blink->ql.flink == &freelist_))
	{
		XCEPT_RAISE (toolbox::mem::exception::Corruption, "Corrupted block chain");
	}
	
	if (!(freelist_.ql.flink->ql.blink == &freelist_))
	{
		XCEPT_RAISE (toolbox::mem::exception::Corruption, "Corrupted block chain");
	}
	
		
	b->ql.flink = &freelist_;
	b->ql.blink = freelist_.ql.blink;
	freelist_.ql.blink = b;
	b->ql.blink->ql.flink = b;
		
	// Create a dummy allocated buffer at the end of the pool.	This dummy
	// buffer is seen when a buffer at the end of the pool is released and
	// blocks  recombination  of  the last buffer with the dummy buffer at
	// the end.  The length in the dummy buffer  is  set  to  the  largest
	// negative  number  to  denote  the  end  of  the pool for diagnostic
	// routines (this specific value is  not  counted  on  by  the  actual
	// allocation and release functions).
	
	len -= sizeof(struct bhead);
	b->bh.bsize = (BufferSizeType) len;
	
#ifdef FreeWipe
	V memset(((char *) b) + sizeof(struct bfhead), 0x55, (MemSize) (len - sizeof(struct bfhead)));
#endif
	bn = BH(((char *) b) + len);
	bn->prevfree = (BufferSizeType) len;
	
	// Definition of ESent assumes two's complement! 
	// assert((~0) == -1);
	
	if (~0 != -1)
	{
		XCEPT_RAISE (toolbox::mem::exception::Corruption, "~0 is not -1. Proper operation requires architecture with two's complement");
	}
	
	bn->bsize = ESent;
}


void * toolbox::mem::MemoryPartition::alloc (BufferSizeType requested_size)
	throw (toolbox::mem::exception::FailedAllocation)
{
	BufferSizeType size = requested_size;
	struct bfhead *b;
#ifdef BestFit
	struct bfhead *best;
#endif
	void *buf;
#ifdef BECtl
	BufferSizeType compactseq = 0;
#endif
	
	// assert(size > 0);
	
	if (size <= (BufferSizeType)0)
	{
		XCEPT_RAISE (toolbox::mem::exception::FailedAllocation, "Cannot allocate 0 bytes");
	}
	
	if (size < (BufferSizeType)SizeQ)
	{ 	      	// Need at least room for the 
		size = SizeQ;		//    queue links. 
	}
	
#ifdef SizeQuant
#if SizeQuant > 1
	size = (size + (SizeQuant - 1)) & (~(SizeQuant - 1));
#endif
#endif
	
	size += sizeof(struct bhead);     // Add overhead in allocated buffer to size required. 
	
#ifdef BECtl
	// If a compact function was provided in the call to bectl(), wrap
	// a loop around the allocation process  to  allow	compaction  to
	// intervene in case we don't find a suitable buffer in the chain. 
	
	while (1)
	{
#endif
		b = freelist_.ql.flink;
#ifdef BestFit
		best = &freelist_;
#endif
		// Scan the free list searching for the first buffer big enough
		//   to hold the requested size buffer.
		
#ifdef BestFit
		while (b != &freelist_) 
		{
			if (b->bh.bsize >= size) 
			{
				if ((best == &freelist_) || (b->bh.bsize < best->bh.bsize)) 
				{
					best = b;
				}
			}
			b = b->ql.flink;		  // Link to next buffer
		}
		b = best;
#endif // BestFit 
		
		while (b != &freelist_) 
		{
			if ((BufferSizeType) b->bh.bsize >= size)
			{
				
				// Buffer  is big enough to satisfy  the request.  Allocate it
				//   to the caller.  We must decide whether the buffer is  large
				//   enough  to  split  into  the part given to the caller and a
				//   free buffer that remains on the free list, or  whether  the
				//   entire  buffer  should  be  removed	from the free list and
				//   given to the caller in its entirety.   We  only  split  the
				//   buffer if enough room remains for a header plus the minimum
				//   quantum of allocation.
				
				if (((BufferSizeType) b->bh.bsize - size) > (BufferSizeType)(SizeQ + (sizeof(struct bhead)))) 
				{
					struct bhead *ba, *bn;
					
					ba = BH(((char *) b) + (b->bh.bsize - size));
					bn = BH(((char *) ba) + size);
					assert(bn->prevfree == b->bh.bsize);
					// Subtract size from length of free block. 
					b->bh.bsize -= size;
					// Link allocated buffer to the previous free buffer. 
					ba->prevfree = b->bh.bsize;
					// Plug negative size into user buffer. 
					ba->bsize = -(BufferSizeType) size;
					// Mark buffer after this one not preceded by free block. 
					bn->prevfree = 0;
					
#ifdef BufStats
					totalloc_ += size;
					numget_++;		  // Increment number of bget() calls 
#endif
					buf = (void *) ((((char *) ba) + sizeof(struct bhead)));
					return buf;
				} 
				else
				{
					struct bhead *ba;
					
					ba = BH(((char *) b) + b->bh.bsize);
					assert(ba->prevfree == b->bh.bsize);
					
					// The buffer isn't big enough to split.  Give  the  whole
					//   shebang to the caller and remove it from the free list. 
					
					assert(b->ql.blink->ql.flink == b);
					assert(b->ql.flink->ql.blink == b);
					b->ql.blink->ql.flink = b->ql.flink;
					b->ql.flink->ql.blink = b->ql.blink;
					
#ifdef BufStats
					totalloc_ += b->bh.bsize;
					numget_++;		  // Increment number of bget() calls 
#endif
					// Negate size to mark buffer allocated. 
					b->bh.bsize = -(b->bh.bsize);
					
					// Zero the back pointer in the next buffer in memory
					//   to indicate that this buffer is allocated. 
					ba->prevfree = 0;
					
					// Give user buffer starting at queue links.
					buf =  (void *) &(b->ql);
					return buf;
				}
			}
			b = b->ql.flink;		  // Link to next buffer 
		}
		
#ifdef BECtl
		
		// We failed to find a buffer.  If there's a compact  function
		//   defined,  notify  it  of the size requested.  If it returns
		//   TRUE, try the allocation again.
		
		if ((compfcn == NULL) || (!(*compfcn)(size, ++compactseq))) 
		{
			break;
		}
		
	}
	
	// No buffer available with requested size free.
	
	// Don't give up yet -- look in the reserve supply.
	
	if (acqfcn != NULL) 
	{
		if (size > exp_incr - sizeof(struct bhead)) 
		{
			
			// Request	is  too  large	to  fit in a single expansion
			//   block.  Try to satisy it by a direct buffer acquisition. 
			
			struct bdhead *bdh;
			
			size += sizeof(struct bdhead) - sizeof(struct bhead);
			if ((bdh = BDH((*acqfcn)((BufferSizeType) size))) != NULL) 
			{
				
				// Mark the buffer special by setting the size field
				//    of its header to zero. 
				bdh->bh.bsize = 0;
				bdh->bh.prevfree = 0;
				bdh->tsize = size;
#ifdef BufStats
				totalloc += size;
				numget++;	      // Increment number of bget() calls 
				numdget++;	      // Direct bget() call count 
#endif
				buf =  (void *) (bdh + 1);
				return buf;
			}
			
		} 
		else
		{
			
			//	Try to obtain a new expansion block 
			
			void *newpool;
			
			if ((newpool = (*acqfcn)((BufferSizeType) exp_incr)) != NULL) 
			{
				bpool(newpool, exp_incr);
				buf =  bget(requested_size);  // This can't, I say, can't
											  //  get into a loop. 
				return buf;
			}
		}
	}
	
	//	Still no buffer available
	
#endif // BECtl
	
	XCEPT_RAISE (toolbox::mem::exception::FailedAllocation, toolbox::toString("Could not allocate %d bytes", requested_size));
	
	return 0;
}

void * toolbox::mem::MemoryPartition::allocz(BufferSizeType size )
	throw (toolbox::mem::exception::FailedAllocation)
{
	char * buf = 0;
	try
	{
		buf = (char *) this->alloc(size);
	} 
	catch (toolbox::mem::exception::FailedAllocation& af)
	{
		XCEPT_RETHROW (toolbox::mem::exception::FailedAllocation, "Cannot allocate zero-initialized memory", af);
	}
	
	struct bhead *b;
	BufferSizeType rsize;

	b = BH(buf - sizeof(struct bhead));
	rsize = -(b->bsize);
	if (rsize == 0) 
	{
		struct bdhead *bd;

		bd = BDH(buf - sizeof(struct bdhead));
		rsize = bd->tsize - sizeof(struct bdhead);
	} 
	else 
	{
		rsize -= sizeof(struct bhead);
	}

	//assert(rsize >= size);

	if (!(rsize >= (BufferSizeType)size))
	{
		XCEPT_RAISE(toolbox::mem::exception::FailedAllocation, toolbox::toString("Cannot allocate %d bytes, have only %d", size, rsize));
	}

	V memset(buf, 0, (MemSize) rsize);
		
	return ((void *) buf);
}

void toolbox::mem::MemoryPartition::free (char *buf)
	throw (toolbox::mem::exception::InvalidAddress, toolbox::mem::exception::Corruption)
{
	struct bfhead *b, *bn;
	
	b = BFH(((char *) buf) - sizeof(struct bhead));
#ifdef BufStats
	numrel_++;			      // Increment number of brel() calls 
#endif
	// assert(buf != NULL);
	
	if (buf == 0)
	{
		XCEPT_RAISE (toolbox::mem::exception::InvalidAddress, "Attempt to free address 0x0");
	}
	
#ifdef BECtl
	if (b->bh.bsize == 0) {	      // Directly-acquired buffer? 
		struct bdhead *bdh;
		
		bdh = BDH(((char *) buf) - sizeof(struct bdhead));
		
		// assert(b->bh.prevfree == 0);
		
		if !(b->bh.prevfree == 0)
		{
			std::string msg = toolbox::toString("Failed to free address %x, directly acquired (single) buffer seems to be in a chain", buf);
			XCEPT_RAISE (toolbox::mem::exception::Corruption, msg);
		}
		
#ifdef BufStats
		totalloc_ -= bdh->tsize;
		
		// assert(totalloc_ >= 0);
		
		if !(totalloc_ >= 0)
		{
			std::string msg = toolbox::toString("Failed to free address %x, no buffers were allocated", buf);
			XCEPT_RAISE (toolbox::mem::exception::Corruption, msg);
		}
		
		numdrel_++;		      // Number of direct releases 
#endif /* BufStats */
#ifdef FreeWipe
		V memset((char *) buf, 0x55,
				 (MemSize) (bdh->tsize - sizeof(struct bdhead)));
#endif // FreeWipe 
		// assert(relfcn != NULL);
		
		if !(relfcn != 0)
		{
			std::string msg = toolbox::toString("Failed to free address %x, free function points to 0x0", buf);
			XCEPT_RAISE (toolbox::mem::exception::Corruption, msg);
		}
		
		(*relfcn)((void *) bdh);      // Release it directly. 
		return;
	}
#endif // BECtl
	
	// Buffer size must be negative, indicating that the buffer is
	// allocated. 
	
	if (b->bh.bsize >= 0) {
		bn = NULL;
	}
	
	//assert(b->bh.bsize < 0);
		
	if (!(b->bh.bsize < 0))
	{
		std::string msg = toolbox::toString("Failed to free address %x, buffer size should be negative to indicate that it has been allocated. Size is %d", buf, b->bh.bsize);
		XCEPT_RAISE (toolbox::mem::exception::Corruption, msg);
	}
	
	// Back pointer in next buffer must be zero, indicating the same thing:	
	// assert(BH((char *) b - b->bh.bsize)->prevfree == 0);
	
	if (BH((char *) b - b->bh.bsize)->prevfree != 0)
	{
		std::string msg = toolbox::toString("Failed to free address %x, back-pointer not zero but %x", buf, BH((char *) b - b->bh.bsize)->prevfree);
		XCEPT_RAISE (toolbox::mem::exception::Corruption, msg);
	}
	
#ifdef BufStats
	totalloc_ += b->bh.bsize;
	
	// assert(totalloc_ >= 0);
	
	if (totalloc_ < 0)
	{
		std::string msg = toolbox::toString("Failed to free address %x, total allocated blocks smaller zero (%d)", buf, totalloc_);
		XCEPT_RAISE (toolbox::mem::exception::Corruption, msg);
	}
#endif
	
	// If the back link is nonzero, the previous buffer is free.  
	
	if (b->bh.prevfree != 0) 
	{
		
		// The previous buffer is free.  Consolidate this buffer  with	it
		//   by  adding  the  length  of	this  buffer  to the previous free
		//   buffer.  Note that we subtract the size  in	the  buffer  being
		//   released,  since  it's  negative to indicate that the buffer is
		//   allocated. 
		
		register BufferSizeType size = b->bh.bsize;
		
		// Make the previous buffer the one we're working on. 
		// assert(BH((char *) b - b->bh.prevfree)->bsize == b->bh.prevfree);
		
		if (BH((char *) b - b->bh.prevfree)->bsize != b->bh.prevfree)
		{
			std::string msg = toolbox::toString("Failed to free address %x, previous buffer not set to current", buf);
			XCEPT_RAISE (toolbox::mem::exception::Corruption, msg);
		}
		
		b = BFH(((char *) b) - b->bh.prevfree);
		b->bh.bsize -= size;
	} 
	else
	{
		
		// The previous buffer isn't allocated.  Insert this buffer
		// on the free list as an isolated free block.		
		// assert(freelist_.ql.blink->ql.flink == &freelist_);
		// assert(freelist_.ql.flink->ql.blink == &freelist_);
		
		if (freelist_.ql.blink->ql.flink != &freelist_)
		{
			std::string msg = toolbox::toString("Failed to free address %x, address of freellist corrupted", buf);
			XCEPT_RAISE (toolbox::mem::exception::Corruption, msg);
		}
		if (freelist_.ql.flink->ql.blink != &freelist_)
		{
			std::string msg = toolbox::toString("Failed to free address %x, address of freellist corrupted", buf);
			XCEPT_RAISE (toolbox::mem::exception::Corruption, msg);
		}
		
		b->ql.flink = &freelist_;
		b->ql.blink = freelist_.ql.blink;
		freelist_.ql.blink = b;
		b->ql.blink->ql.flink = b;
		b->bh.bsize = -b->bh.bsize;
	}
	
	// Now we look at the next buffer in memory, located by advancing from
	// the  start  of  this  buffer  by its size, to see if that buffer is
	// free.  If it is, we combine  this  buffer  with	the  next  one	in
	// memory, dechaining the second buffer from the free list. 
	
	bn =  BFH(((char *) b) + b->bh.bsize);
	if (bn->bh.bsize > 0) 
	{
		
		// The buffer is free.	Remove it from the free list and add
		// its size to that of our buffer.
		// assert(BH((char *) bn + bn->bh.bsize)->prevfree == bn->bh.bsize);
		// assert(bn->ql.blink->ql.flink == bn);
		// assert(bn->ql.flink->ql.blink == bn);
		
		XCEPT_ASSERT( (BH((char *) bn + bn->bh.bsize)->prevfree == bn->bh.bsize), toolbox::mem::exception::Corruption, "Failed to free");
		XCEPT_ASSERT( (bn->ql.blink->ql.flink == bn), toolbox::mem::exception::Corruption, "Failed to free");
		XCEPT_ASSERT( (bn->ql.flink->ql.blink == bn), toolbox::mem::exception::Corruption, "Failed to free");
		
		bn->ql.blink->ql.flink = bn->ql.flink;
		bn->ql.flink->ql.blink = bn->ql.blink;
		b->bh.bsize += bn->bh.bsize;
		
		// Finally,  advance  to   the	buffer	that   follows	the  newly
		//   consolidated free block.  We must set its  backpointer  to  the
		//   head  of  the  consolidated free block.  We know the next block
		//   must be an allocated block because the process of recombination
		//   guarantees  that  two  free	blocks will never be contiguous in
		//   memory.  
		
		bn = BFH(((char *) b) + b->bh.bsize);
	}
	
#ifdef FreeWipe
	V memset(((char *) b) + sizeof(struct bfhead), 0x55,
			 (MemSize) (b->bh.bsize - sizeof(struct bfhead)));
#endif
	// assert(bn->bh.bsize < 0);
	XCEPT_ASSERT( (bn->bh.bsize < 0), toolbox::mem::exception::Corruption, "Failed to free");
	
	// The next buffer is allocated.  Set the backpointer in it  to  point
	// to this buffer; the previous free buffer in memory. 
	
	bn->bh.prevfree = b->bh.bsize;
	
#ifdef BECtl
	
	// If  a  block-release function is defined, and this free buffer
	// constitutes the entire block, release it.  Note that  pool_len
	// is  defined  in  such a way that the test will fail unless all
	// pool blocks are the same size.
	
	if (relfcn != NULL && ((BufferSizeType) b->bh.bsize) == (pool_len - sizeof(struct bhead)))
	{
		// assert(b->bh.prevfree == 0);
		// assert(BH((char *) b + b->bh.bsize)->bsize == ESent);
		// assert(BH((char *) b + b->bh.bsize)->prevfree == b->bh.bsize);
		
		XCEPT_ASSERT( (b->bh.prevfree == 0), toolbox::mem::exception::Corruption, "Failed to free");
		XCEPT_ASSERT( (BH((char *) b + b->bh.bsize)->bsize == ESent), toolbox::mem::exception::Corruption, "Failed to free");
		XCEPT_ASSERT( (BH((char *) b + b->bh.bsize)->prevfree == b->bh.bsize), toolbox::mem::exception::Corruption, "Failed to free");
		
		// Unlink the buffer from the free list 
		
		b->ql.blink->ql.flink = b->ql.flink;
		b->ql.flink->ql.blink = b->ql.blink;
		
		(*relfcn)(b);
#ifdef BufStats
		numprel++;		      // Nr of expansion block releases 
		numpblk--;		      // Total number of blocks 
		assert(numpblk == numpget - numprel);
		
		if !(numpblk == numpget - numprel)
		{
			std::string msg = toolbox::toString("Failed to free %x, total number of blocks (%d) not equal to blocks retrieved (%d) minus blocks allocated (%d)", buf, numpblk, numpget, numprel);
			XCEPT_RAISE (toolbox::mem::exception::Corruption, msg);
		}
		
#endif // BufStats 
	}
#endif // BECtl
}

void * toolbox::mem::MemoryPartition::realloc (char *buf, BufferSizeType size)
	throw (toolbox::mem::exception::FailedAllocation, toolbox::mem::exception::Corruption)
{
	void *nbuf = 0;
	BufferSizeType osize;		      // Old size of buffer 
	struct bhead *b;
	
	try
	{
		nbuf = this->alloc(size);
	} 
	catch (toolbox::mem::exception::FailedAllocation& af)
	{
		std::string msg = toolbox::toString("Failed to re-allocate buffer %x with %d bytes", buf, size);
		XCEPT_RETHROW (toolbox::mem::exception::FailedAllocation, msg, af);
	}
	
	b = BH(((char *) buf) - sizeof(struct bhead));
	osize = -b->bsize;

#ifdef BECtl
	if (osize == 0) 
	{
		//  Buffer acquired directly through acqfcn. 
		struct bdhead *bd;
		
		bd = BDH(((char *) buf) - sizeof(struct bdhead));
		osize = bd->tsize - sizeof(struct bdhead);
	} else
#endif
	osize -= sizeof(struct bhead);
	
	assert(osize > 0);
	
	XCEPT_ASSERT ( (osize>0), toolbox::mem::exception::Corruption, "Failed to realloc");
	
	V memcpy((char *) nbuf, (char *) buf, // Copy the data 
			 (MemSize) ((size < osize) ? size : osize));
			 
	try
	{
		this->free(buf);
	} 
	catch (toolbox::mem::exception::InvalidAddress& ia)
	{
		std::string msg = toolbox::toString("Failed to re-allocate buffer %x with %d bytes", buf, size);
		XCEPT_RETHROW (toolbox::mem::exception::FailedAllocation, msg, ia);
	} 
	catch (toolbox::mem::exception::Corruption& c)
	{
		std::string msg = toolbox::toString("Failed to re-allocate buffer %x with %d bytes", buf, size);
		XCEPT_RETHROW (toolbox::mem::exception::Corruption, msg, c);
	}
	
	return nbuf;
}


toolbox::mem::MemoryPartition::MemoryPartition (const toolbox::mem::MemoryPartition & aMemPart)
{
}

toolbox::mem::MemoryPartition & toolbox::mem::MemoryPartition::operator = (const toolbox::mem::MemoryPartition & aMemPart)
{
	return *this;
}


#ifdef BufStats

// BSTATS  --	Return buffer allocation free space statistics.  

BufferSizeType toolbox::mem::MemoryPartition::getUsed()
{
	return totalloc_;
} 

void toolbox::mem::MemoryPartition::stats( BufferSizeType *curalloc, BufferSizeType *totfree,BufferSizeType *maxfree, BufferSizeType *nget,BufferSizeType *nrel)
	throw (toolbox::mem::exception::Corruption)
{
	struct bfhead *b = freelist_.ql.flink;
	
	*nget = numget_;
	*nrel = numrel_;
	*curalloc = totalloc_;
	*totfree = 0;
	*maxfree = (BufferSizeType)-1;
	while (b != &freelist_) 
	{
		// assert(b->bh.bsize > 0);
		
		XCEPT_ASSERT ( (b->bh.bsize > 0), toolbox::mem::exception::Corruption, "Failed to create statistics");
		
		*totfree += b->bh.bsize;
		if (b->bh.bsize > *maxfree) 
		{
			*maxfree = b->bh.bsize;
		}
		b = b->ql.flink;	      /// Link to next buffer 
	}
}



#ifdef BECtl



void toolbox::mem::MemoryPartition::estats( BufferSizeType *pool_incr, BufferSizeType *npool,BufferSizeType *npget, BufferSizeType*nprel, BufferSizeType*ndget,BufferSizeType *ndrel)
{
	*pool_incr = (pool_len_ < 0) ? -exp_incr_ : exp_incr_;
	*npool = numpblk_;
	*npget = numpget_;
	*nprel = numprel_;
	*ndget = numdget_;
	*ndrel = numdrel_;
}
#endif /* BECtl */
#endif /* BufStats */

void toolbox::mem::MemoryPartition::dumpBuffer(void *buf)
	throw (toolbox::mem::exception::Corruption)
{
	struct bfhead *b;
	unsigned char *bdump;
	BufferSizeType bdlen;
	
	b = BFH(((char *) buf) - sizeof(struct bhead));
	
	// assert(b->bh.bsize != 0);
	
	XCEPT_ASSERT ((b->bh.bsize != 0), toolbox::mem::exception::Corruption, "Cannot dump buffer");
	
	if (b->bh.bsize < 0) 
	{
		bdump = (unsigned char *) buf;
		bdlen = (-b->bh.bsize) - sizeof(struct bhead);
	} 
	else
	{
		bdump = (unsigned char *) (((char *) b) + sizeof(struct bfhead));
		bdlen = b->bh.bsize - sizeof(struct bfhead);
	}
	
	while (bdlen > 0)
	{
		BufferSizeType i, dupes = 0;
		BufferSizeType l = bdlen;
		char bhex[50], bascii[20];
		
		if (l > 16) 
		{
			l = 16;
		}
		
		for (i = 0; i < l; i++) 
		{
			V sprintf(bhex + i * 3, "%02X ", bdump[i]);
			bascii[i] = isprint(bdump[i]) ? bdump[i] : ' ';
		}
		
		bascii[i] = 0;
		V printf("%-48s   %s\n", bhex, bascii);
		bdump += l;
		bdlen -= l;
		while ((bdlen > 16) && (memcmp((char *) (bdump - 16),(char *) bdump, 16) == 0)) 
		{
			dupes++;
			bdump += 16;
			bdlen -= 16;
		}
		
		if (dupes > 1) 
		{
			std::cout << "(" << dupes << " lines [" << dupes*16 << " bytes] identical to above line skipped)" << std::endl;
		} 
		else if (dupes == 1) 
		{
			bdump -= 16;
			bdlen += 16;
		}
	}
}


void toolbox::mem::MemoryPartition::dumpPool(void * buf, int dumpalloc, int dumpfree)
	throw (toolbox::mem::exception::Corruption)
{
	struct bfhead *b = BFH(buf);
	
	while (b->bh.bsize != ESent) 
	{
		BufferSizeType bs = b->bh.bsize;
		
		if (bs < 0) 
		{
			bs = -bs;
			
			std::cout << "Allocated buffer: " << (size_t) bs << " bytes" << std::endl;
			
			if (dumpalloc) 
			{
				// May throw toolbox::mem::Corruption, will be chained up
				this->dumpBuffer((void *) (((char *) b) + sizeof(struct bhead)));
			}
		} 
		else
		{
			std::string lerrs = "";
			
			// assert(bs > 0);			
			XCEPT_ASSERT ((bs > 0), toolbox::mem::exception::Corruption, "Cannot dump pool");
			
			if ((b->ql.blink->ql.flink != b) || (b->ql.flink->ql.blink != b)) 
			{
				lerrs = "  (Bad free list links)";
			}			
			std::cout << "Free block: " << (size_t) bs << " bytes, " << lerrs << std::endl;

#ifdef FreeWipe
			char * lerr = ((char *) b) + sizeof(struct bfhead);
			if ((bs > sizeof(struct bfhead)) && 
			    ((*lerr != 0x55) || (memcmp(lerr, lerr + 1, (MemSize) (bs - (sizeof(struct bfhead) + 1))) != 0))) 
			{
				std::cout << "(Contents of above free block have been overstored.)" << std::endl;
						 
				// May throw toolbox::mem::Corruption, will be chained up		
				this->dumpBuffer((void *) (((char *) b) + sizeof(struct bhead)));
			} 
			else
#endif
			if (dumpfree) 
			{
					// May throw toolbox::mem::Corruption, will be chained up
					this->dumpBuffer((void *) (((char *) b) + sizeof(struct bhead)));
			}
		}
		b = BFH(((char *) b) + bs);
	}
}

#ifdef BufValid


int toolbox::mem::MemoryPartition::validate(void * buf) 
	throw (toolbox::mem::exception::Corruption)
{
	struct bfhead *b = BFH(buf);
	
	while (b->bh.bsize != ESent) 
	{
		BufferSizeType bs = b->bh.bsize;
		
		if (bs < 0) 
		{
			bs = -bs;
		}
		else
		{
			// LO  unused when -Wall -Werror  char *lerr = "";
			
			// assert(bs > 0);
			XCEPT_ASSERT ((bs > 0), toolbox::mem::exception::Corruption, "Cannot validate pool");
			
			if (bs <= 0) 
			{
				return 0;
			}
			
			if ((b->ql.blink->ql.flink != b) || (b->ql.flink->ql.blink != b)) 
			{
				std::cout << "Free block, size " << (size_t) bs << " bytes" << std::endl;
				// assert(0);
				// return 0;
				
				std::stringstream msg;
				msg << "Validate failed, free block " << (size_t) bs << " bytes";
				
				XCEPT_RAISE (toolbox::mem::exception::Corruption, msg.str());				
			}
			
#ifdef FreeWipe
			lerr = ((char *) b) + sizeof(struct bfhead);
			
			if ((bs > sizeof(struct bfhead)) && ((*lerr != 0x55) ||
				(memcmp(lerr, lerr + 1, (MemSize) (bs - (sizeof(struct bfhead) + 1))) != 0))) 
			{
				std::cout << "(Contents of above free block have been overstored.)" << std::endl;
				bufdump((void *) (((char *) b) + sizeof(struct bhead)));
				//assert(0);
				//return 0;
				XCEPT_RAISE (toolbox::mem::exception::Corruption, "Validate failed. Contents of above free block have been overstored.");
			}
#endif
		}
		b = BFH(((char *) b) + bs);
	}
	return 1;
}
#endif // BufValid 


#ifdef BECtl


void toolbox::mem::MemoryPartition::autoExpansion( int (*compact) (BufferSizeType sizereq, int sequence),  void *(*acquire) (BufferSizeType size),
		   void (*release) (void *buf), BufferSizeType pool_incr)
{
	compfcn_ = compact;
	acqfcn_ = acquire;
	relfcn_ = release;
	exp_incr_ = pool_incr;
}
#endif


