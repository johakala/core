// $Id: CommittedHeapAllocator.cc,v 1.10 2008/09/19 08:48:02 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "toolbox/mem/CommittedHeapAllocator.h"
#include "toolbox/mem/CommittedHeapBuffer.h"
#include "toolbox/PolicyFactory.h"
#include "toolbox/AllocPolicy.h"
#include <string>
#include <limits>
#include "toolbox/string.h"

toolbox::mem::CommittedHeapAllocator::CommittedHeapAllocator(size_t committedSize)
throw (toolbox::mem::exception::FailedCreation)
{
	this->init("toolbox-mem-allocator", committedSize);
}
toolbox::mem::CommittedHeapAllocator::CommittedHeapAllocator(const std::string & name, size_t committedSize)
	throw (toolbox::mem::exception::FailedCreation)
{
	this->init(name, committedSize);
}

void toolbox::mem::CommittedHeapAllocator::init(const std::string & name, size_t committedSize)
	throw (toolbox::mem::exception::FailedCreation)
{
	committedSize_ = committedSize;
	
	if ( committedSize > static_cast<size_t>(std::numeric_limits<ssize_t>::max()) )
	{
		std::stringstream msg;
		msg << "Failed to create committed heap allocator, requested size " << committedSize << ", allowed size " << std::numeric_limits<ssize_t>::max();
		XCEPT_RAISE (toolbox::mem::exception::FailedCreation, msg.str());
	}

        toolbox::PolicyFactory * factory = toolbox::getPolicyFactory();
        toolbox::AllocPolicy * policy = 0;

        try
        {
                toolbox::net::URN urn(name, this->type());
                policy = dynamic_cast<toolbox::AllocPolicy *>(factory->getPolicy(urn, "alloc"));
		buffer_ = (char*)(policy->alloc(committedSize));
        }
        catch (toolbox::exception::Exception & e)
        {
                XCEPT_RETHROW (toolbox::mem::exception::FailedCreation, "Failed to allocate memory", e);
        }

	
	try
	{
		//buffer_ = new char[committedSize];
		
		// physical, user and kernel address are all the same in this case
		//
		memPartition_.addToPool(buffer_, buffer_, buffer_, committedSize);
	} 
	catch (toolbox::mem::exception::Corruption& ce)
	{
		std::stringstream msg;
		msg << "Failed to create committed heap allocator with memory region of " << committedSize << " bytes";
		XCEPT_RETHROW (toolbox::mem::exception::FailedCreation, msg.str(), ce);
	}
	catch (toolbox::mem::exception::InvalidAddress& ia)
	{
		std::stringstream msg;
		msg << "Failed to create committed heap allocator with memory region of " << committedSize << " bytes";
		XCEPT_RETHROW (toolbox::mem::exception::FailedCreation, msg.str(), ia);
	}
	catch (toolbox::mem::exception::MemoryOverflow& mo)
	{
		std::stringstream msg;
		msg << "Failed to create committed heap allocator with memory region of " << committedSize << " bytes";
		XCEPT_RETHROW (toolbox::mem::exception::FailedCreation, msg.str(), mo);
	}
	catch (std::bad_alloc& e)
	{
		std::stringstream msg;
		msg << "Failed to create committed heap allocator with memory region of " << committedSize << " bytes, reason: " << e.what();
		XCEPT_RAISE (toolbox::mem::exception::FailedCreation, msg.str());
	}
}


toolbox::mem::CommittedHeapAllocator::~CommittedHeapAllocator()
{
	toolbox::PolicyFactory * factory = toolbox::getPolicyFactory();
        toolbox::AllocPolicy * policy = 0;

        try
        {
                toolbox::net::URN urn("toolbox-mem-allocator", this->type());
                policy = dynamic_cast<toolbox::AllocPolicy *>(factory->getPolicy(urn, "alloc"));
                policy->free(buffer_, committedSize_);

        }
        catch (toolbox::exception::Exception & e)
        {
                XCEPT_RETHROW (toolbox::mem::exception::FailedDispose, "Failed to free memory", e);
        }

	//delete buffer_;
}

toolbox::mem::Buffer * toolbox::mem::CommittedHeapAllocator::alloc(size_t size, toolbox::mem::Pool * pool) 
	throw (toolbox::mem::exception::FailedAllocation)
{
	if ( size > static_cast<size_t>(std::numeric_limits<ssize_t>::max()) )
	{
		std::stringstream msg;
		msg << "Failed to allocate " << size << " bytes from committed reagion " << committedSize_ << ", maximum allowed is " << std::numeric_limits<ssize_t>::max();
		XCEPT_RAISE (toolbox::mem::exception::FailedAllocation, msg.str());
	}
	
	try
	{
		char* b = (char*) memPartition_.alloc(size);
		toolbox::mem::CommittedHeapBuffer* bufPtr = new toolbox::mem::CommittedHeapBuffer(pool, size, b);			
		return bufPtr;
	}
	catch (toolbox::mem::exception::FailedAllocation& fa)
	{
		BufferSizeType curalloc, totfree, maxfree;
		BufferSizeType nget, nrel;
		memPartition_.stats (&curalloc, &totfree, &maxfree, &nget, &nrel);
		std::string msg = toolbox::toString ("Out of memory in CommittedHeapAllocator while allocating %d bytes. Bytes used %d, bytes free %d, max free %d, number allocated %d, number released %d", size, curalloc, totfree, maxfree, nget, nrel);
		XCEPT_RETHROW (toolbox::mem::exception::FailedAllocation, msg, fa);
	}
}

	
void toolbox::mem::CommittedHeapAllocator::free (toolbox::mem::Buffer * buffer) 
	throw (toolbox::mem::exception::FailedDispose)
{
	try
	{
		memPartition_.free( (char*) buffer->getAddress());
		delete buffer;
	} 
	catch (toolbox::mem::exception::InvalidAddress& ia)
	{
		// Retry possible, maybe pointer was only wrong, don't delete buffer
		std::string msg = toolbox::toString ("Cannot free buffer %x, invalid pointer", buffer);
		XCEPT_RETHROW (toolbox::mem::exception::FailedDispose, msg, ia);
	}
	catch (toolbox::mem::exception::Corruption& c)
	{
		delete buffer; // delete in any case, there's a corruption, continuatio is actually not possible anymore
		XCEPT_RETHROW (toolbox::mem::exception::FailedDispose, "Cannot free buffer, memory corruption", c);	
	}
}

std::string toolbox::mem::CommittedHeapAllocator::type () 
{
	return "CommittedHeapAllocator";
}


bool toolbox::mem::CommittedHeapAllocator::isCommittedSizeSupported()
{
	return true;
}
	
size_t toolbox::mem::CommittedHeapAllocator::getCommittedSize()
{
	return committedSize_;
}

size_t  toolbox::mem::CommittedHeapAllocator::getUsed()
{
	return memPartition_.getUsed();
}
