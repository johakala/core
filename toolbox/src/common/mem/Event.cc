// $Id: Event.cc,v 1.4 2008/07/18 15:27:40 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "toolbox/mem/Event.h"

toolbox::mem::Event::Event(const std::string& type, const std::string& name, void* originator):
	toolbox::Event(type, originator)
{
	name_ = name;
}


std::string toolbox::mem::Event::getName()
{
	return name_;
}


toolbox::mem::PoolAvailableEvent::PoolAvailableEvent(const std::string& name, void* originator):
	toolbox::mem::Event("PoolAvailableEvent", name, originator)
{
}

toolbox::mem::PoolRevokedEvent::PoolRevokedEvent(const std::string& name, void* originator):
	toolbox::mem::Event("PoolRevokedEvent", name, originator)
{
}

toolbox::mem::PoolUpperThresholdCrossed::PoolUpperThresholdCrossed(int direction, void* originator):
	toolbox::mem::Event("PoolUpperThresholdCrossed", "", originator)
{
	direction_ = direction;
}

int toolbox::mem::PoolUpperThresholdCrossed::direction()
{
	return direction_;
}

toolbox::mem::PoolLowerThresholdCrossed::PoolLowerThresholdCrossed(int direction, void* originator):
	toolbox::mem::Event("PoolLowerThresholdCrossed", "", originator)
{
	direction_ = direction;
}

int toolbox::mem::PoolLowerThresholdCrossed::direction()
{
	return direction_;
}

