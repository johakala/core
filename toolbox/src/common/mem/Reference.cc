// $Id: Reference.cc,v 1.8 2008/07/18 15:27:40 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "toolbox/mem/Reference.h"
#include <cassert>
  
toolbox::mem::Reference::Reference()
{
	refPool_ = 0;
	offset_ = 0;
	buffer_ = 0;
	nextReference_ = 0;
	dataSize_ = 0;
}

void toolbox::mem::Reference::setReferencePool(toolbox::mem::ReferencePool * refPool)
{
	refPool_ = refPool;
}
  
                  
void* toolbox::mem::Reference::getDataLocation() 
{
	if (buffer_ != 0)
	{
		return (void*) (((size_t) buffer_->getAddress()) + offset_);
	}
	
	return 0;
}

toolbox::mem::Buffer* toolbox::mem::Reference::getBuffer() 
{
	return buffer_;
}


size_t toolbox::mem::Reference::getDataOffset() 
{
	return offset_;

}
          
size_t toolbox::mem::Reference::getDataSize() 
{
	return dataSize_;
}      
  
  
void toolbox::mem::Reference::setDataSize(size_t dataSize) 
{
	dataSize_ = dataSize;
}  

void toolbox::mem::Reference::setDataOffset(size_t offset) 
{
	offset_ = offset;
}            
	      
void toolbox::mem::Reference::setBuffer (toolbox::mem::Buffer * buffer)
{
	buffer_ = buffer;
}


void toolbox::mem::Reference::setNextReference(toolbox::mem::Reference * ref) 
{
	nextReference_ = ref;
}


toolbox::mem::Reference * toolbox::mem::Reference::getNextReference()
{
	return nextReference_;
}

toolbox::mem::Reference * toolbox::mem::Reference::duplicate()
{
	toolbox::mem::Reference* tmp = this;
	toolbox::mem::Reference* previous = 0;
	
	refPool_->lock();
	toolbox::mem::Reference* newChain = 0;
	
	try
	{
		newChain = refPool_->alloc();
	}
	catch(toolbox::mem::exception::FailedAllocation & e )
	{
		refPool_->unlock();
		return 0;
	}
	
	newChain->setNextReference( 0 );
	
	refPool_->unlock();
	 
	toolbox::mem::Reference* ref = newChain;
	 
	while (tmp != 0)
	{
		
		if (previous != 0)  // re-allocate a new reference
		{
			refPool_->lock();
			try
			{
				ref = refPool_->alloc();
			}
			catch(toolbox::mem::exception::FailedAllocation & e)
			{
				refPool_->unlock();
				newChain->release();
				return 0;
			}
			
			refPool_->unlock();
			ref->setNextReference( 0 );
			previous->setNextReference( ref );
		}
		
		
		previous = ref;
		
		// locking
		tmp->getBuffer()->getPool()->lock();
		
		ref->setBuffer(tmp->getBuffer());
		ref->setDataOffset(tmp->getDataOffset());
		ref->setDataSize(tmp->getDataSize());
		ref->getBuffer()->incrementRefCounter();
		
		// unlocking
		tmp->getBuffer()->getPool()->unlock();
		
		tmp = tmp->getNextReference();	
	} 
	
	return newChain;
}

void toolbox::mem::Reference::release()
{
	// release the whole chain
	toolbox::mem::Reference* tmp = this;
	while (tmp != 0)
	{
		toolbox::mem::Buffer* buffer = tmp->getBuffer();
		toolbox::mem::Pool* pool = buffer->getPool();
		
		pool->lock();
		
		buffer->decrementRefCounter();
		
		if (buffer->getRefCounter() == 0)
		{
			pool->free(buffer);
		}
				
		pool->unlock();
		
		Reference * next = tmp->getNextReference();
		
		refPool_->lock();
		// clear it
		tmp->setBuffer(0);
 		tmp->setDataOffset(0);
		tmp->setDataSize(0);
		tmp->setNextReference( 0 );
		refPool_->free(tmp);
		
		refPool_->unlock();
		
		// debug only assert (next == 0);
		
		tmp = next;
	}
}
