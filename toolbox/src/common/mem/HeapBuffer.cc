// $Id: HeapBuffer.cc,v 1.6 2008/07/18 15:27:40 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "toolbox/mem/HeapBuffer.h"
#include <new>

toolbox::mem::HeapBuffer::HeapBuffer(toolbox::mem::Pool * pool, size_t size, void * address):
	toolbox::mem::Buffer(pool, size, address)
{	
	//address_ = new char[size];
	//size_ = size;
}

toolbox::mem::HeapBuffer::~HeapBuffer() 
{
	//delete [] (char*) address_;
}
	
