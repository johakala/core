// $Id: HeapAllocator.cc,v 1.7 2008/07/18 15:27:40 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "toolbox/mem/HeapAllocator.h"
#include "toolbox/mem/HeapBuffer.h"
#include "toolbox/PolicyFactory.h"
#include "toolbox/AllocPolicy.h"
#include <string>

toolbox::mem::HeapAllocator::HeapAllocator()
{
	used_ = 0;
}

toolbox::mem::Buffer * toolbox::mem::HeapAllocator::alloc(size_t size, toolbox::mem::Pool * pool) 
	throw (toolbox::mem::exception::FailedAllocation)
{
	toolbox::mem::HeapBuffer * bufPtr = 0;

	toolbox::PolicyFactory * factory = toolbox::getPolicyFactory();
        toolbox::AllocPolicy * policy = 0;

        try
        {
                toolbox::net::URN urn("toolbox-mem-allocator", this->type());
                policy = dynamic_cast<toolbox::AllocPolicy *>(factory->getPolicy(urn, "alloc"));
        }
        catch (toolbox::exception::Exception & e)
        {
                XCEPT_RETHROW (toolbox::mem::exception::FailedAllocation, "Failed to allocate memory", e);
        }


	bufPtr = new toolbox::mem::HeapBuffer( pool, size,policy->alloc(size) );
	
	used_ += size;
	return bufPtr;
}

	
void toolbox::mem::HeapAllocator::free (toolbox::mem::Buffer * buffer) 
	throw (toolbox::mem::exception::FailedDispose)
{
	used_ -= buffer->getSize();
	

	toolbox::PolicyFactory * factory = toolbox::getPolicyFactory();
        toolbox::AllocPolicy * policy = 0;

        try
        {
                toolbox::net::URN urn("toolbox-mem-allocator", this->type());
		policy = dynamic_cast<toolbox::AllocPolicy *>(factory->getPolicy(urn, "alloc"));
		policy->free(buffer->getAddress(), buffer->getSize());

        }
        catch (toolbox::exception::Exception & e)
        {
                XCEPT_RETHROW (toolbox::mem::exception::FailedDispose, "Failed to free memory", e);
        }

//delete buffer->getAddress();
	delete  buffer;
}

std::string toolbox::mem::HeapAllocator::type () 
{
	return "HeapAllocator";
}


bool toolbox::mem::HeapAllocator::isCommittedSizeSupported()
{
	return false;
}
	
size_t toolbox::mem::HeapAllocator::getCommittedSize()
{
	return 0;
}


size_t toolbox::mem::HeapAllocator::getUsed()
{
	return  used_;
}
