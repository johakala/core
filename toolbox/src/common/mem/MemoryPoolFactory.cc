// $Id: MemoryPoolFactory.cc,v 1.12 2008/07/18 15:27:40 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "toolbox/mem/MemoryPoolFactory.h"
#include "toolbox/mem/SmartBufPool.h"
#include "toolbox/mem/SmartGCBufPool.h"
#include "toolbox/mem/Reference.h"
#include "toolbox/mem/Event.h"
#include "toolbox/mem/exception/Exception.h"
#include "toolbox/mem/exception/DuplicateAllocator.h"
#include "toolbox/mem/exception/DuplicateMemoryPool.h"
#include "toolbox/mem/exception/AllocatorNotFound.h"
#include "toolbox/mem/exception/FailedCreation.h"
#include "toolbox/mem/exception/OutOfMemory.h"
#include "toolbox/mem/exception/SizeExceeded.h"
#include "toolbox/BSem.h"
#include "toolbox/task/Guard.h"


#include <map>

const size_t MaximumReferencePoolSize = 10000000;

static toolbox::BSem mutex_(toolbox::BSem::FULL,false);

toolbox::mem::MemoryPoolFactory* toolbox::mem::MemoryPoolFactory::instance_ = 0;

toolbox::mem::MemoryPoolFactory* toolbox::mem::getMemoryPoolFactory()
{
	return toolbox::mem::MemoryPoolFactory::getInstance();
}

toolbox::mem::MemoryPoolFactory* toolbox::mem::MemoryPoolFactory::getInstance()
{
	toolbox::task::Guard <toolbox::BSem> guard(mutex_);

	if (instance_ == 0)
	{
		instance_ = new toolbox::mem::MemoryPoolFactory();
	}
	return instance_;
}

// Set Reference Pool size in CTOR
//
toolbox::mem::MemoryPoolFactory::MemoryPoolFactory (): referencePool_( MaximumReferencePoolSize  )
{
}


void toolbox::mem::MemoryPoolFactory::addActionListener (toolbox::ActionListener* listener)
{
	toolbox::task::Guard < toolbox::BSem > guard(mutex_);
	dispatcher_.addActionListener(listener);
}
			
void toolbox::mem::MemoryPoolFactory::removeActionListener (toolbox::ActionListener* listener) throw (toolbox::exception::Exception)
{
	toolbox::task::Guard < toolbox::BSem > guard(mutex_);
	dispatcher_.removeActionListener(listener);
}


toolbox::mem::Reference* toolbox::mem::MemoryPoolFactory::getFrame (toolbox::mem::Pool* pool, size_t size)
	throw (toolbox::mem::exception::Exception)
{
	toolbox::mem::Reference* ref = 0;

	referencePool_.lock();
	try
	{
		ref = referencePool_.alloc();
	} 
	catch (toolbox::mem::exception::FailedAllocation& e)
	{
		referencePool_.unlock();
		std::string msg = toolbox::toString ("Failed to allocate %d bytes from reference pool", size);
		XCEPT_RETHROW(toolbox::mem::exception::Exception, msg, e);
	}
	
	referencePool_.unlock();
	
	pool->lock();
	toolbox::mem::Buffer* buffer = 0;
	
	try
	{
		buffer = pool->alloc(size);
	}	
	catch (toolbox::mem::exception::FailedAllocation& e)
	{
		pool->unlock();
		referencePool_.lock();
		try
		{
			referencePool_.free(ref);
		} 
		catch (toolbox::mem::exception::FailedDispose& e1)
		{
			referencePool_.unlock();
		}
		
		referencePool_.unlock();
		std::string msg = toolbox::toString ("Failed to allocate %d bytes from memory pool", size);
		XCEPT_RETHROW(toolbox::mem::exception::Exception, msg, e);
	}

	ref->setBuffer(buffer);
	buffer->incrementRefCounter();
	ref->setNextReference(0);
	ref->setDataOffset(0);
	ref->setDataSize(buffer->getSize());
	
	pool->unlock();

	
	
	return ref;
}


std::vector<std::string> toolbox::mem::MemoryPoolFactory::getMemoryPoolNames()
{
	toolbox::task::Guard < toolbox::BSem > guard(mutex_);
	std::map<std::string, toolbox::mem::Pool *, std::less<std::string> >::iterator i;
	std::vector<std::string> v;
	for (i = pools_.begin(); i != pools_.end(); i++)
	{
		v.push_back ((*i).first);
	}
	return v;
}

toolbox::mem::Pool*  toolbox::mem::MemoryPoolFactory::createPool(toolbox::net::URN & urn, toolbox::mem::Allocator * allocator)
				throw (toolbox::mem::exception::DuplicateMemoryPool,
					toolbox::mem::exception::AllocatorNotFound,
					toolbox::mem::exception::FailedCreation)
{
	toolbox::task::Guard < toolbox::BSem > guard(mutex_);
	std::string index = urn.toString();

	std::map<std::string, toolbox::mem::Pool *, std::less<std::string> >::iterator i = pools_.find(index);
	if (i == pools_.end())
	{
		toolbox::mem::Pool* p = 0;
		if(urn.getNamespace() == "toolbox-mem=pool-gc")
		{
			p = new toolbox::mem::SmartGCBufPool(allocator);
		}
		else
		{
			p = new toolbox::mem::SmartBufPool(allocator);
		}

		pools_[index] = p;
		
		// announce new pool
		toolbox::mem::PoolAvailableEvent e(index);
		dispatcher_.fireEvent(e);
		return p;
	} 
	else 
	{
		std::string msg = "Memory pool cannot be created. Exists already: ";
		msg += index;
		XCEPT_RAISE (toolbox::mem::exception::DuplicateMemoryPool, msg);
	}
}

toolbox::mem::Pool*  toolbox::mem::MemoryPoolFactory::createPool(const std::string& poolName, toolbox::mem::Allocator * allocator)
				throw (toolbox::mem::exception::DuplicateMemoryPool,
					toolbox::mem::exception::AllocatorNotFound,
					toolbox::mem::exception::FailedCreation)
{
	toolbox::net::URN urn("toolbox-mem-pool", poolName);
	return this->createPool(urn, allocator);
}

toolbox::mem::Pool* toolbox::mem::MemoryPoolFactory::findPool(toolbox::net::URN & urn) 
	throw (toolbox::mem::exception::MemoryPoolNotFound)
{
	toolbox::task::Guard < toolbox::BSem > guard(mutex_);
	std::map<std::string, toolbox::mem::Pool *, std::less<std::string> >::iterator i;
	i = pools_.find( urn.toString() );
	if (i != pools_.end())
	{
		return ((*i).second);
	} 
	else 
	{
		std::string msg = "Memory pool not found: ";
		msg += urn.toString();
		XCEPT_RAISE (toolbox::mem::exception::MemoryPoolNotFound, msg);
	}
}


void toolbox::mem::MemoryPoolFactory::removePool(toolbox::net::URN & urn) 
	throw (toolbox::mem::exception::MemoryPoolNotFound)
{
	toolbox::task::Guard < toolbox::BSem > guard(mutex_);
	std::string name = urn.toString();

	std::map<std::string, toolbox::mem::Pool *, std::less<std::string> >::iterator i;
	i = pools_.find(name);
	if (i != pools_.end())
	{
		// Remember pool and remove from map
		unusedPools_.push_back( (*i).second );
		pools_.erase(i);
		
		// announce pool revoked
		PoolRevokedEvent e(name);
		dispatcher_.fireEvent(e);
	} else 
	{
		std::string msg = "Memory pool not found: ";
		msg += name;
		XCEPT_RAISE (toolbox::mem::exception::MemoryPoolNotFound, msg);
	}
}

void toolbox::mem::MemoryPoolFactory::destroyPool(toolbox::net::URN & urn) 
	throw (toolbox::mem::exception::MemoryPoolNotFound)
{
	toolbox::task::Guard < toolbox::BSem > guard(mutex_);
	std::string name = urn.toString();

	std::map<std::string, toolbox::mem::Pool *, std::less<std::string> >::iterator i;
	i = pools_.find(name);
	if (i != pools_.end())
	{
		// announce pool revoked
		PoolRevokedEvent e(name);
		dispatcher_.fireEvent(e);

		// destroy pool		
		delete (*i).second;
		
		// remove from pool map
		pools_.erase(i);
		
	} else 
	{
		std::string msg = "Memory pool not found: ";
		msg += name;
		XCEPT_RAISE (toolbox::mem::exception::MemoryPoolNotFound, msg);
	}
}


void toolbox::mem::MemoryPoolFactory::destroyInstance()
{
	toolbox::task::Guard < toolbox::BSem > guard(mutex_);
	if (instance_ != 0)
	{
		delete instance_;
		instance_ = 0;
	}
}

void toolbox::mem::MemoryPoolFactory::cleanup()
{
	toolbox::task::Guard < toolbox::BSem > guard(mutex_);
	std::list<toolbox::mem::Pool *>::iterator j;
	for (j = unusedPools_.begin(); j != unusedPools_.end(); j++)
	{
		delete (*j);
	}
}

toolbox::mem::MemoryPoolFactory::~MemoryPoolFactory()
{
	
	std::map<std::string, toolbox::mem::Pool *, std::less<std::string> >::iterator i;
	for (i = pools_.begin(); i != pools_.end(); i++)
	{
		PoolRevokedEvent e( (*i).first );
		dispatcher_.fireEvent(e);
		
		delete (*i).second;
	}
	
	cleanup();	
}

