// $Id: ReferencePool.cc,v 1.6 2008/07/18 15:27:40 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "toolbox/mem/ReferencePool.h"
#include "toolbox/mem/Reference.h"

#include <iostream>

toolbox::mem::ReferencePool::ReferencePool(size_t num)
	throw (toolbox::mem::exception::FailedCreation)
	: references_(num), mutex_(BSem::FULL)
{
	
}
	
	
toolbox::mem::ReferencePool::~ReferencePool() 
{
}
	

void toolbox::mem::ReferencePool::lock() 
{
	mutex_.take();
}
	
void  toolbox::mem::ReferencePool::unlock() 
{
	mutex_.give();
}
	

	
void toolbox::mem::ReferencePool::free(toolbox::mem::Reference * ref) 
	throw (toolbox::mem::exception::FailedDispose)
{	
	try
	{
		references_.free(ref);
	} catch (toolbox::mem::exception::FailedDispose& fd)
	{
		XCEPT_RETHROW (toolbox::mem::exception::FailedDispose, "Failed to free buffer reference", fd);
	}
}
	
toolbox::mem::Reference * toolbox::mem::ReferencePool::alloc()
	throw (toolbox::mem::exception::FailedAllocation)
{
	try
	{
		toolbox::mem::Reference* r = references_.alloc();
		if (r == 0)
		{
			// failed to allocate, out of references
			XCEPT_RAISE (toolbox::mem::exception::FailedAllocation, "Failed to allocate buffer reference, pool exhausted");
		}
		r->setReferencePool(this);	
		return r;
	} catch (toolbox::mem::exception::FailedAllocation& fa)
	{
		XCEPT_RETHROW (toolbox::mem::exception::FailedAllocation, "Failed to allocate buffer reference", fa);
	}
}
	

