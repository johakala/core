// $Id: Buffer.cc,v 1.5 2008/07/18 15:27:40 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "toolbox/mem/Buffer.h"

toolbox::mem::Buffer::Buffer(toolbox::mem::Pool* pool, size_t size, void * address)
{
	refCounter_ = 0;
	pool_ = pool;
	size_ = size;
	address_ = address;
}

void* toolbox::mem::Buffer::getAddress()
{
	return address_;
}

void toolbox::mem::Buffer::incrementRefCounter()
{
	refCounter_++;
}

void toolbox::mem::Buffer::decrementRefCounter()
{
	refCounter_--;
}

unsigned int toolbox::mem::Buffer::getRefCounter()
{
	return refCounter_;
}

size_t toolbox::mem::Buffer::getSize()
{
	return size_;
}

toolbox::mem::Pool* toolbox::mem::Buffer::getPool()
{
	return pool_;
}

