// $Id: BufPoolEntry.cc,v 1.9 2008/07/18 15:27:40 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "toolbox/mem/BufPoolEntry.h"
#include "toolbox/string.h"
#include <stdio.h>
#include <sstream>

toolbox::mem::BufPoolEntry::BufPoolEntry(size_t blockSize, toolbox::mem::Allocator * allocator) 
	throw (toolbox::mem::exception::FailedCreation)
{
	try
	{
		allocator_ = allocator;
		free_ = new toolbox::mem::Buffer * [MAX_ENTRIES_SIZE+1];
		// remember_ = new toolbox::mem::Buffer * [MAX_ENTRIES_SIZE+1];
		last_ = 0;
		first_ = 0;
		num_ = MAX_ENTRIES_SIZE+1;
		created_ = 0;
		blockSize_ = blockSize;
		log2size_ = BUF_SIZE_TO_LOG2(blockSize_);
	} 
	catch (std::bad_alloc& ba)
	{
		std::string msg = toolbox::toString ("Failed to create buffer pool entry with block size %d, caused by %s", blockSize, ba.what());
		XCEPT_RAISE (toolbox::mem::exception::FailedCreation, msg);
	}
}
	
toolbox::mem::BufPoolEntry::~BufPoolEntry() 
{	 	
        //printf ("delete Buf Poo entry for log2 of %d\n",log2size_);
	//for (int i=0; i< created_; i++ ) 
	//{
		//printf("call allocator free on entry  %d on pointer %x\n",i,remember_[i]);
		// allocator_->free(remember_[i]);
	try
	{
		std::set<Buffer*>::iterator i ;
		for (i = remember_.begin(); i != remember_.end(); i++)
		{		
			allocator_->free( (*i) );
		}
	//}
		delete free_;
	// delete remember_;
		remember_.clear();
	} catch (toolbox::mem::exception::FailedDispose& fd)
	{
		// don't yet know if to throw FailedDispose or not.
		XCEPT_RETHROW (toolbox::mem::exception::Exception, "Failed to delete buffer pool entry", fd);
	}
	
}
	

	
toolbox::mem::Buffer * toolbox::mem::BufPoolEntry::alloc(toolbox::mem::Pool* pool)
	throw (toolbox::mem::exception::FailedAllocation)
{
	if ( first_ != last_ ) 
	{
		toolbox::mem::Buffer * tmp = free_[first_];
		first_ = (first_ + 1) % num_;
		return tmp;			
	}
	else 
	{
		if ( created_ == MAX_ENTRIES_SIZE) 
		{
			std::stringstream msg;
			msg << "Failed to allocate buffer from pool, maximum reached: " << created_;
			XCEPT_RAISE (toolbox::mem::exception::FailedAllocation, msg.str());
			// return (toolbox::mem::Buffer*)0;			
		}

		try
		{
			toolbox::mem::Buffer * tmp = allocator_->alloc(blockSize_, pool);
			remember_.insert(tmp);
			created_++;
			return tmp;
		} 
		catch (toolbox::mem::exception::FailedAllocation& fa)
		{		
			XCEPT_RETHROW (toolbox::mem::exception::FailedAllocation, "Failed to allocate buffer from pool", fa);
		}		
	}
}


void toolbox::mem::BufPoolEntry::invalidate()
	throw (toolbox::mem::exception::FailedDispose)
{	
	try
	{
		while ( first_ != last_ ) 
		{
			toolbox::mem::Buffer * tmp = free_[first_];
			first_ = (first_ + 1) % num_;		
			allocator_->free(tmp);
			// remove from remember list
			remember_.erase(tmp);
			created_--;
		}
	} 
	catch (toolbox::mem::exception::Exception& e)
	{
		XCEPT_RETHROW (toolbox::mem::exception::FailedDispose, "Failed to invalidate pool", e);
	}	
}
	
void toolbox::mem::BufPoolEntry::free( toolbox::mem::Buffer * block ) 
	throw (toolbox::mem::exception::FailedDispose)
{
	if ((( last_ + 1) % num_ ) != first_ ) 
	{
		free_[last_] = block;
		last_ = (last_ + 1) % num_;
	}
	else 
	{
		std::stringstream msg;
		msg << "Buffer pool overflow in free of block size " << blockSize_;
		XCEPT_RAISE (toolbox::mem::exception::FailedDispose, msg.str());
	}
}

	
	
size_t toolbox::mem::BufPoolEntry::blockLen()  
{
	return blockSize_;
}
	
void toolbox::mem::BufPoolEntry::show() 
{
	std::cout << "blockSize_:" << blockSize_ << " log2size_:" << log2size_ << " Indexed as:"<<
	BUF_SIZE_TO_BUF_INDEX(blockSize_) <<" created_:" << created_  
	<<  " first_:" << first_<< " last_:" << last_ << std::endl;
}

size_t toolbox::mem::BufPoolEntry::getNumberOfAllocatedBlocks()
{
	//std::cout << "Created: " << created_ << ", cached: " << this->getNumberOfCachedBlocks() << std::endl;
	return created_ - this->getNumberOfCachedBlocks();

}

size_t toolbox::mem::BufPoolEntry::getNumberOfCachedBlocks()
{
	if ( first_ <= last_ ) 
	{		
		return last_ - first_;
	}
	else
	{
		return (num_ - first_) + last_;
	}
}

size_t toolbox::mem::BufPoolEntry::getNumberOfCreatedBlocks()
{
	return created_;
}	
