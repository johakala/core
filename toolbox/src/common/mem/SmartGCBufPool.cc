// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "toolbox/mem/SmartGCBufPool.h"

#include "toolbox/string.h"
#include "toolbox/mem/exception/OutOfMemory.h"
#include "toolbox/mem/exception/SizeExceeded.h"
#include "toolbox/mem/Event.h"

toolbox::mem::SmartGCBufPool::SmartGCBufPool(toolbox::mem::Allocator * allocator)
	throw (toolbox::mem::exception::FailedCreation)
	: SmartBufPool(allocator)
{
	// replace with recursive since invalidate is locked and the alloc is locked outside
	delete mutex_;
	mutex_ = new toolbox::BSem(BSem::FULL, true);
}
	
	
toolbox::mem::SmartGCBufPool::~SmartGCBufPool() 
{
}
	
	
toolbox::mem::Buffer * toolbox::mem::SmartGCBufPool::alloc(size_t size)
	throw (toolbox::mem::exception::FailedAllocation)
{
	toolbox::mem::Buffer * buffer = 0;
	try
	{
		buffer =  toolbox::mem::SmartBufPool::alloc(size);
	}
	catch(toolbox::mem::exception::FailedAllocation & fae)
	{
		try
		{
			this->invalidate(); // dispose blocks to unformatted memory
		}
		catch(toolbox::mem::exception::FailedDispose & fde)
		{
			std::stringstream msg;
			msg << "Failed garbage collection when allocating for size " << size;
			XCEPT_RETHROW(toolbox::mem::exception::FailedAllocation, msg.str(), fde);
		}
		buffer = toolbox::mem::SmartBufPool::alloc(size);
	}
	return buffer;
}

