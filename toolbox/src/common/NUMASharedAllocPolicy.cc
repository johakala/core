// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2013, CERN.			                 		 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, A. Petrucci, C.Wakefield					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         	 *
 *************************************************************************/

#include <numa.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <fcntl.h>

#include "toolbox/string.h"
#include "toolbox/utils.h"
#include "toolbox/NUMASharedAllocPolicy.h"

toolbox::NUMASharedAllocPolicy::NUMASharedAllocPolicy( const std::string & name ) : name_(name)
{
	this->setProperty("mempolicy", "none");

	this->maxNode_ = numa_max_node();
}

toolbox::NUMASharedAllocPolicy::~NUMASharedAllocPolicy()
{
}

std::string toolbox::NUMASharedAllocPolicy::getPackage()
{
	return "shared";
}

void* toolbox::NUMASharedAllocPolicy::alloc (size_t size) throw (toolbox::exception::Exception)
{
	int fh;

	errno = 0;

	std::string xdaq_shared;
	std::string filePath = name_;
	char* envptr;
	if ( (envptr = (char*)getenv("XDAQ_SHARED")) != (char*)0 )
	{
		xdaq_shared = envptr;
		filePath = xdaq_shared + "/" + name_;
	}

	if (-1 == (fh = open(filePath.c_str(), O_CREAT|O_TRUNC|O_RDWR, S_IREAD|S_IWRITE))) {
		std::stringstream ss;
		ss << "Failed to create file for ftok in NUMA Shared Policy: " << strerror(errno);

		XCEPT_RAISE(toolbox::exception::Exception, ss.str());
	}

	errno = 0;

	if (-1 == close(fh)) {
		std::stringstream ss;
		ss << "Failed to close file for ftok in NUMA Shared Policy: " << strerror(errno);

		XCEPT_RAISE(toolbox::exception::Exception, ss.str());
	}


	key_t key = ftok(filePath.c_str(), 1);

	errno = 0;

	if ( key < 0 )
	{
		std::stringstream ss;
		ss << "Failed to convert Policy key: " << strerror(errno);

		XCEPT_RAISE(toolbox::exception::Exception, ss.str());
	}

	char * sharedval;
	int shmid = shmget(key, size, S_IRUSR | S_IWUSR | IPC_CREAT);

	errno = 0;

	if ( shmid < 0 )
	{
		std::stringstream ss;
		ss << "Failed to convert Policy key: " << strerror(errno);

		XCEPT_RAISE(toolbox::exception::Exception, ss.str());
	}

	sharedval = (char *) shmat(shmid, NULL, 0);

	errno = 0;

	if ( sharedval < 0 )
	{
		std::stringstream ss;
		ss << "Failed to convert Policy key: " << strerror(errno);

		XCEPT_RAISE(toolbox::exception::Exception, ss.str());
	}

	if ( this->getProperty("mempolicy") == "interleaved")
	{
		if ( this->hasProperty("memnodes") )
		{
			struct bitmask *mask = NULL;

			mask = numa_parse_nodestring((char*)(this->getProperty("memnodes").c_str()));

			numa_interleave_memory(sharedval, size, mask);

			numa_bitmask_free(mask);
		}
		else
		{
			XCEPT_RAISE(toolbox::exception::Exception, "Invalid allocation policy, no nodes provided");
		}

	}
	else if ( this->getProperty("mempolicy") == "onnode")
	{
		if ( this->hasProperty("node") )
		{
			size_t node = 0;

			try
			{
				node = toolbox::toLong(this->getProperty("node"));
			}
			catch ( toolbox::exception::Exception & e )
			{
				XCEPT_RETHROW(toolbox::exception::Exception, "Failed to set NUMA Allocation node, invalid integer value provided in Policy configutation", e);
			}

			if ( node > maxNode_ )
			{
				std::stringstream ss;
				ss << "Invalid NUMA Allocation node (" << node << ") max node (" << this->maxNode_ << ")";

				XCEPT_RAISE(toolbox::exception::Exception, ss.str());
			}

			numa_tonode_memory(sharedval, size, node);
		}
		else
		{
			XCEPT_RAISE(toolbox::exception::Exception, "Missing node attribute for memory policy allocation");
		}
	}
	else
	{
		XCEPT_RAISE(toolbox::exception::Exception, "Invalid allocation policy");
	}

	return sharedval;
}

void toolbox::NUMASharedAllocPolicy::free (void* buffer, size_t size) throw (toolbox::exception::Exception)
{
	shmdt(buffer);
}

