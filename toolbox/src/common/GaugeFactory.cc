// $Id: GaugeFactory.cc,v 1.3 2008/07/18 15:27:38 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and S. Murray					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "toolbox/GaugeFactory.h"
#include "toolbox/GaugeEvent.h"
#include "toolbox/exception/Exception.h"
#include <map>

toolbox::GaugeFactory* toolbox::GaugeFactory::instance_ = 0;

toolbox::GaugeFactory* toolbox::getGaugeFactory()
{
	return toolbox::GaugeFactory::getInstance();
}

toolbox::GaugeFactory* toolbox::GaugeFactory::getInstance()
{
	if (instance_ == 0)
	{
		instance_ = new toolbox::GaugeFactory();
	}
	return instance_;
}


toolbox::GaugeFactory::GaugeFactory ()
{
}


void toolbox::GaugeFactory::addActionListener (toolbox::ActionListener* listener)
{
	dispatcher_.addActionListener(listener);
}
			
void toolbox::GaugeFactory::removeActionListener (toolbox::ActionListener* listener) 
	throw (toolbox::exception::Exception)
{
	dispatcher_.removeActionListener(listener);
}


std::vector<std::string> toolbox::GaugeFactory::getGaugeNames()
{
	std::map<std::string, toolbox::Gauge*, std::less<std::string> >::iterator i;
	std::vector<std::string> v;
	for (i = gauges_.begin(); i != gauges_.end(); i++)
	{
		v.push_back ((*i).first);
	}
	return v;
}

toolbox::Gauge* toolbox::GaugeFactory::createGauge(toolbox::net::URN & urn)
				throw (toolbox::exception::Exception)
{
	std::string index = urn.toString();

	std::map<std::string, toolbox::Gauge*, std::less<std::string> >::iterator i = gauges_.find(index);
	if (i == gauges_.end())
	{		
		toolbox::Gauge* g = new toolbox::Gauge();		
		gauges_[index] = g;
		
		// announce new gauge
		toolbox::GaugeAvailableEvent e(index);
		dispatcher_.fireEvent(e);
		return g;
	} 
	else 
	{
		std::string msg = "Gauge cannot be created. Exists already: ";
		msg += index;
		XCEPT_RAISE (toolbox::exception::Exception, msg);
	}
}

toolbox::Gauge* toolbox::GaugeFactory::createGauge(toolbox::net::URN & urn, double min, double max, double value)
				throw (toolbox::exception::Exception)
{
	std::string index = urn.toString();

	std::map<std::string, toolbox::Gauge*, std::less<std::string> >::iterator i = gauges_.find(index);
	if (i == gauges_.end())
	{		
		toolbox::Gauge* g = new toolbox::Gauge(min, max, value);		
		gauges_[index] = g;
		
		// announce new gauge
		toolbox::GaugeAvailableEvent e(index);
		dispatcher_.fireEvent(e);
		return g;
	} 
	else 
	{
		std::string msg = "Gauge cannot be created. Exists already: ";
		msg += index;
		XCEPT_RAISE (toolbox::exception::Exception, msg);
	}	
}

toolbox::Gauge* toolbox::GaugeFactory::createGauge(const std::string& name, double min, double max, double value)
				throw (toolbox::exception::Exception)		
{
	toolbox::net::URN urn("toolbox-gauge", name);
	return this->createGauge(urn, min, max, value);
}

toolbox::Gauge* toolbox::GaugeFactory::findGauge(toolbox::net::URN& urn) 
	throw (toolbox::exception::Exception)
{
	std::map<std::string, toolbox::Gauge*, std::less<std::string> >::iterator i;
	i = gauges_.find( urn.toString() );
	if (i != gauges_.end())
	{
		return ((*i).second);
	} 
	else 
	{
		std::string msg = "Gauge not found: ";
		msg += urn.toString();
		XCEPT_RAISE (toolbox::exception::Exception, msg);
	}
}


void toolbox::GaugeFactory::removeGauge(toolbox::net::URN & urn) 
	throw (toolbox::exception::Exception)
{
	std::string name = urn.toString();

	std::map<std::string, toolbox::Gauge*, std::less<std::string> >::iterator i;
	i = gauges_.find(name);
	if (i != gauges_.end())
	{
		// Remember gauge and remove from map
		unusedGauges_.push_back( (*i).second );
		gauges_.erase(i);
		
		// announce gauge revoked
		GaugeRevokedEvent e(name);
		dispatcher_.fireEvent(e);
	} else 
	{
		std::string msg = "Gauge not found: ";
		msg += name;
		XCEPT_RAISE (toolbox::exception::Exception, msg);
	}
}

void toolbox::GaugeFactory::destroyGauge(toolbox::net::URN& urn) 
	throw (toolbox::exception::Exception)
{
	std::string name = urn.toString();

	std::map<std::string, toolbox::Gauge*, std::less<std::string> >::iterator i;
	i = gauges_.find(name);
	if (i != gauges_.end())
	{
		// announce gauge revoked
		GaugeRevokedEvent e(name);
		dispatcher_.fireEvent(e);

		// destroy gauge		
		delete (*i).second;
		
		// remove from gauge map
		gauges_.erase(i);
		
	} else 
	{
		std::string msg = "Gauge not found: ";
		msg += name;
		XCEPT_RAISE (toolbox::exception::Exception, msg);
	}
}


void toolbox::GaugeFactory::destroyInstance()
{
	if (instance_ != 0)
	{
		delete instance_;
		instance_ = 0;
	}
}

void toolbox::GaugeFactory::cleanup()
{
	std::list<toolbox::Gauge*>::iterator j;
	for (j = unusedGauges_.begin(); j != unusedGauges_.end(); j++)
	{
		delete (*j);
	}
}

toolbox::GaugeFactory::~GaugeFactory()
{	
	std::map<std::string, toolbox::Gauge*, std::less<std::string> >::iterator i;
	for (i = gauges_.begin(); i != gauges_.end(); i++)
	{
		GaugeRevokedEvent e( (*i).first );
		dispatcher_.fireEvent(e);		
		delete (*i).second;
	}
	
	cleanup();	
}

