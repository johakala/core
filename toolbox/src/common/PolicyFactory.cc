// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2013, CERN.			                 		 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, A. Petrucci, C.Wakefield					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         	 *
 *************************************************************************/

#include <numa.h>
#include <iostream>
#include <algorithm> 

#include "toolbox/regex.h"
#include "toolbox/PolicyFactory.h"

#include "toolbox/ThreadPolicy.h"
#include "toolbox/POSIXThreadPolicy.h"
#include "toolbox/NUMAThreadPolicy.h"

#include "toolbox/AllocPolicy.h"
#include "toolbox/POSIXAllocPolicy.h"
#include "toolbox/NUMAAllocPolicy.h"
#include "toolbox/NUMASharedAllocPolicy.h"
#include "toolbox/BSem.h"
#include "toolbox/task/Guard.h"

toolbox::PolicyFactory* toolbox::PolicyFactory::instance_ = 0;

static toolbox::BSem mutex_(toolbox::BSem::FULL,true);

toolbox::Policy* toolbox::PolicyFactory::getPolicy(toolbox::net::URN & urn, const std::string & type) throw (toolbox::exception::Exception)
{
	toolbox::task::Guard <toolbox::BSem> guard(mutex_);

	std::string fullURN = urn.toString();

	for ( std::map<std::string, toolbox::Policy*, std::less<std::string> >::iterator i = policies_.begin(); i != policies_.end(); i++ )
	{
		std::string regex = (*i).first;

		if ( toolbox::regx_match_nocase(fullURN, regex) && (*i).second->getType() == type )
		{
			(*i).second->counter_++;

			if ( (*i).second->getProperty("matches") == "on" )
			{
				bool found = (std::find((*i).second->users_.begin(), (*i).second->users_.end(), fullURN) != (*i).second->users_.end());
				if ( !found )
				{
					(*i).second->users_.push_back(fullURN);
				}
			}
			return (*i).second;
		}	
	}

	toolbox::Policy *  policy = this->createPolicy(fullURN, type, "" );
	policy->counter_++;
	if (policy->getProperty("matches") == "on" )
	{
		policy->users_.push_back(fullURN);
	}
	return policy; 
}

toolbox::Policy* toolbox::PolicyFactory::createPolicy(const std::string & regex, const std::string & type, const std::string & package) throw (toolbox::exception::Exception)
{
	toolbox::task::Guard <toolbox::BSem> guard(mutex_);

	if ( package  == "numa" && numa_available() < 0 )
	{
		XCEPT_RAISE (toolbox::exception::Exception, "NUMA is not supported by this systems hardware");
	}

	std::string index = regex;
	toolbox::Policy* l = policies_[index];


	if ( l != 0 && l->getType() == type )
	{
		return l;
	} 	
	else
	{
		
		if (type == "thread")
		{
			if ( package  == "numa" )
			{
				policies_[index] = new toolbox::NUMAThreadPolicy();
			}
			else if ( package  == "posix" || package == "" )
			{
				policies_[index] = new toolbox::POSIXThreadPolicy();
			}
			else
			{
				XCEPT_RAISE (toolbox::exception::Exception, "Invalid policy kind");
			}
		}
		else if (type == "alloc")
		{
			if ( package == "numa" )
			{
				policies_[index] = new toolbox::NUMAAllocPolicy();
			}
			else if ( package == "shared" )
			{
				policies_[index] = new toolbox::NUMASharedAllocPolicy(regex);
			}
			else if ( package  == "posix"  || package == "" )
			{
				policies_[index] = new toolbox::POSIXAllocPolicy();
			}
			else
			{
				XCEPT_RAISE (toolbox::exception::Exception, "Invalid policy kind");
			}
		}
		else
		{
			std::string msg = "Requested policy type does not exist: ";
			msg += index;
			XCEPT_RAISE (toolbox::exception::Exception, msg);
		}
		policies_[index]->setProperty("pattern",index);
		return policies_[index];
	}
}
			
void toolbox::PolicyFactory::removePolicy (toolbox::Policy * policy) throw (toolbox::exception::Exception)
{
	toolbox::task::Guard <toolbox::BSem> guard(mutex_);

	for ( std::map<std::string, toolbox::Policy*, std::less<std::string> >::iterator i = policies_.begin(); i != policies_.end(); i++ )
	{	
		if ( policy == (*i).second )
		{
			delete (*i).second;
			policies_.erase(i);
			return;
		}
	}

	std::string msg = "Requested policy for removal not found: ";
	msg += policy->toString();
	XCEPT_RAISE (toolbox::exception::Exception, msg);
}	

std::list<toolbox::Policy*> toolbox::PolicyFactory::getPolicies()
{
	toolbox::task::Guard <toolbox::BSem> guard(mutex_);

	std::list<toolbox::Policy*> l;
	std::map<std::string, toolbox::Policy*, std::less<std::string> >::iterator i;
	for (i = policies_.begin(); i != policies_.end(); i++)
	{
		l.push_back((*i).second);
	}

	return l;
}

std::list<toolbox::Policy*> toolbox::PolicyFactory::getPolicies(const std::string & type)
{
	toolbox::task::Guard <toolbox::BSem> guard(mutex_);

	std::list<toolbox::Policy*> l;
        
	std::map<std::string, toolbox::Policy*, std::less<std::string> >::iterator i;
        for (i = policies_.begin(); i != policies_.end(); i++)
        {
        	if (((*i).second)->getType() == type)
		{
	        	l.push_back((*i).second);
        	}
	}

        return l;
}

//! Retrieve a pointer to the toolbox::PolicyFactory singleton
//
toolbox::PolicyFactory* toolbox::PolicyFactory::getInstance()
{
	toolbox::task::Guard <toolbox::BSem> guard(mutex_);

	if (instance_ == 0)
	{
		instance_ = new toolbox::PolicyFactory();
	}

	return instance_;
}

//! Destoy the factory and all associated policies
//
void toolbox::PolicyFactory::destroyInstance()
{
	toolbox::task::Guard <toolbox::BSem> guard(mutex_);

	if (instance_ != 0)
	{
		delete instance_;
		instance_ = 0;
	}
}

toolbox::PolicyFactory* toolbox::getPolicyFactory()
{

	return toolbox::PolicyFactory::getInstance();
}
