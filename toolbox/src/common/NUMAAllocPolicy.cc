// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2013, CERN.			                 		 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, A. Petrucci, C.Wakefield					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         	 *
 *************************************************************************/

#include <numa.h>
#include "toolbox/string.h"
#include "toolbox/utils.h"
#include "toolbox/NUMAAllocPolicy.h"

toolbox::NUMAAllocPolicy::NUMAAllocPolicy()
{
	this->setProperty("mempolicy", "none");

	this->maxNode_ = numa_max_node();
}

toolbox::NUMAAllocPolicy::~NUMAAllocPolicy()
{
}

std::string toolbox::NUMAAllocPolicy::getPackage()
{
	return "numa";
}

void* toolbox::NUMAAllocPolicy::alloc (size_t size) throw (toolbox::exception::Exception)
{
	if ( this->getProperty("mempolicy") == "interleaved")
	{
		void * memAlloc = numa_alloc_interleaved(size);

		if ( memAlloc == NULL )
		{
			std::stringstream ss;
			ss << "NUMA Interleaved allocation failed due to insufficient memory";

			XCEPT_RAISE(toolbox::exception::Exception, ss.str());
		}

		return memAlloc;
	}
	else if ( this->getProperty("mempolicy") == "onnode")
	{
		if ( this->hasProperty("node") )
		{
			size_t node = 0;

			try
			{
				node = toolbox::toLong(this->getProperty("node"));
			}
			catch ( toolbox::exception::Exception & e )
			{
				XCEPT_RETHROW(toolbox::exception::Exception, "Failed to set NUMA Allocation node, invalid integer value provided in Policy configutation", e);
			}

			if ( node > maxNode_ )
			{
				std::stringstream ss;
				ss << "Invalid NUMA Allocation node (" << node << ") max node (" << this->maxNode_ << ")";

				XCEPT_RAISE(toolbox::exception::Exception, ss.str());
			}

			void * memAlloc = numa_alloc_onnode(size, node);

			if ( memAlloc == NULL )
			{
				std::stringstream ss;
				ss << "NUMA allocation on node " << node << " failed due to insufficient memory";

				XCEPT_RAISE(toolbox::exception::Exception, ss.str());
			}

			return memAlloc;
		}
		else
		{
			XCEPT_RAISE(toolbox::exception::Exception, "Missing node attribute for memory policy allocation");
		}
	}
	else
	{
		XCEPT_RAISE(toolbox::exception::Exception, "Invalid allocation policy");
	}
}

void toolbox::NUMAAllocPolicy::free (void* buffer, size_t size) throw (toolbox::exception::Exception)
{
	numa_free(buffer, size);
}

