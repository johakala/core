// $Id: Processor.cc,v 1.7 2008/07/18 15:27:39 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "toolbox/exception/Processor.h"
#include "xcept/tools.h"

// LOG4CPLUS INCLUDES
#include "log4cplus/logger.h"
#include "log4cplus/consoleappender.h"
#include "log4cplus/helpers/appenderattachableimpl.h"
#include "log4cplus/helpers/loglog.h"
#include "log4cplus/helpers/pointer.h"
#include "log4cplus/spi/loggingevent.h"
#include "log4cplus/loggingmacros.h"

using namespace log4cplus;
using namespace log4cplus::helpers;
using namespace log4cplus::spi;

const unsigned long MaximumExceptionQueueSize = 4096;

toolbox::exception::Processor* toolbox::exception::Processor::instance_ = 0;

toolbox::exception::Processor* toolbox::exception::getProcessor()
{
	return toolbox::exception::Processor::getInstance();
}

toolbox::exception::Processor* toolbox::exception::Processor::getInstance()
{
	if (instance_ == 0)
	{
		instance_ = new toolbox::exception::Processor();
	}
	return instance_;
}

void toolbox::exception::Processor::destroyInstance()
{
	if (instance_ != 0)
	{
		delete instance_;
		instance_ = 0;
	}
}

toolbox::exception::Processor::Processor()
	:toolbox::Task ("toolbox/exception/Processor"), exceptions_(MaximumExceptionQueueSize)
{
	defaultHandler_ = 0;
	
	//
	// The first one who gets a processor instance will cause the task to be created
	//
	this->activate();
}

void toolbox::exception::Processor::setDefaultHandler(toolbox::exception::HandlerSignature * handler)
{
	defaultHandler_ = handler;
}



toolbox::exception::Processor::~Processor()
{

}

void toolbox::exception::Processor::push (xcept::Exception& exception, toolbox::exception::HandlerSignature* handler, void * context)
{
	toolbox::exception::Processor::HandlerInfo info;
	info.exception = exception.clone(); // Copy the exception
	info.handler = handler;
	if ( handler == 0 )
	{
		info.handler = defaultHandler_;
	}
	
	info.context = context;
	
	try
	{
		exceptions_.push(info);
	} 
	catch (toolbox::exception::QueueFull& qf)
	{
		LOG4CPLUS_FATAL ( Logger::getRoot(), "toolbox::exception::Processor queue full, exception notification lost.");
	}
}

/*
void toolbox::exception::Processor::notify (xcept::Exception & ex)
{
	toolbox::exception::getProcessor()->push(ex, 0, 0);
}
*/

int toolbox::exception::Processor::svc()
{
	for(;;)
	{
		toolbox::exception::Processor::HandlerInfo info;
		info = exceptions_.pop();
		
		if (info.handler == 0) 
		{
			// No handler provided then last chance is to log the problem
			std::string msg = "No handler for asynchronous exception. ";
			msg += xcept::stdformat_exception_history(*info.exception);
			LOG4CPLUS_ERROR ( Logger::getRoot(), msg);			
		}
		else if  ( info.handler->invoke(*(info.exception), info.context) )
		{
			// returned true: problem solved
			// --> Maybe put log statement here
		}
		else
		{
			// returned false: problem persistent
			// --> Maybe put log statement here
		}
		
		delete info.exception;
			
	}
	// the only way this method returns in practise is through a major error, so returning -1 to satisfy compiler
	return -1;
}
