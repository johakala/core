// $Id: Dispatcher.cc,v 1.2 2008/07/18 15:27:39 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "toolbox/exception/Dispatcher.h"
#include <algorithm>

toolbox::exception::Dispatcher::Dispatcher()
{

}

void toolbox::exception::Dispatcher::addListener( toolbox::exception::Listener * listener)
{
	listenerList_.push_back(listener);
}


void toolbox::exception::Dispatcher::removeListener( toolbox::exception::Listener * listener) 
throw (toolbox::exception::Exception)
{
	std::list<toolbox::exception::Listener*>::iterator i;
	i = std::find( listenerList_.begin(), listenerList_.end(), listener);
	if (i != listenerList_.end())
	{
		listenerList_.erase(i);
	} else
	{
		XCEPT_RAISE( toolbox::exception::Exception, "Could not remove listener. Object not found." );
	}
}


void toolbox::exception::Dispatcher::fire( xcept::Exception & e )
{
	std::list<toolbox::exception::Listener*>::iterator i;
	for ( i = listenerList_.begin(); i != listenerList_.end(); ++i)
	{
		// for the time being, the context is alway NULL
		// also: ignore any bool true/false return value in this version
		//
		(*i)->onException(e);
	}
}
