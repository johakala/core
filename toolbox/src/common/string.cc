// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber, L. Orsini, D. Simelevicius					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "toolbox/string.h"

#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <iostream>
#include <iomanip>
#include <string>
#include <stdio.h>
#include <exception>
#include <typeinfo>
#include <errno.h>
#include <algorithm>
#include <limits.h>

std::string toolbox::escape(const std::string& s)
{
        std::stringstream ss;
        size_t len = s.length();
        for(size_t i=0 ; i<len ; i++)
        {
                char c = s[i];
                switch(c)
                {
                        case 'a' ... 'z':
                        case 'A' ... 'Z':
                        case '0' ... '9':
                        case ';':
                        case '/':
                        case '?':
                        case ':':
                        case '@':
                        case '&':
                        case '=':
                        case '+':
                        case '$':
                        case ',':
                        case '-':
                        case '_':
                        case '.':
                        case '!':
                        case '~':
                        case '*':
                        case '\'':
                        case '(':
                        case ')':
                                ss << c;
                                break;
                        default:
                                ss << '%' << std::setbase(16) << std::setfill('0') << std::setw(2) << (int)(c);
                                break;
                }
        }
        return ss.str();
}

std::string toolbox::quote(const std::string& s)
{
        std::stringstream ss;
        size_t len = s.length();
        for(size_t i=0 ; i<len ; i++)
        {
                char c = s[i];
                switch(c)
                {
                        case 'a' ... 'z':
                        case 'A' ... 'Z':
                        case '0' ... '9':
                        case ';':
                        case '/':
                        case '?':
                        case ':':
                        case '@':
                        case '&':
                        case '=':
                        case '+':
                        case '$':
                        case ',':
                        case '-':
                        case '_':
                        case '.':
                        case '!':
                        case '~':
                        case '*':
                        case '\'':
                        case '(':
                        case ')':
                        case ' ':
                                ss << c;
                                break;
                        default:
                                ss << '\\' << c;
                                break;
                }
        }
        return ss.str();
}

std::string toolbox::jsonquote(const std::string& s)
{
	std::string result;
        size_t len = s.length();
        for(size_t i=0 ; i<len ; i++)
        {
                char c = s[i];
		switch(c)       
		{
          	case '\"':
             	result += "\\\"";
             	break;
          	case '\\':
             	result += "\\\\";
             	break;
          	case '\b':
             	result += "\\b";
             	break;
          	case '\f':
             	result += "\\f";
             	break;
          	case '\n':
             	result += "\\n";
             	break;
          	case '\r':
             	result += "\\r";
             	break;
          	case '\t':
             	result += "\\t";
             	break;
          	//case '/':
             	// Even though \/ is considered a legal escape in JSON, a bare
             	// slash is also legal, so I see no reason to escape it.
             	// (I hope I am not misunderstanding something.
             	// blep notes: actually escaping \/ may be useful in javascript to avoid </ 
             	// sequence.
             	// Should add a flag to allow this compatibility mode and prevent this 
             	// sequence from occurring.
          	default:
		if ( c > 0 && c <= 0x1F) // is a control character 
             	{
                	std::ostringstream oss;
                	oss << "\\u" << std::hex << std::uppercase << std::setfill('0') << std::setw(4) << static_cast<int>(c);
                	result += oss.str();
             	}
             	else
             	{
                	result += c;
             	}
             break;
       }
	}
        return result;

}

std::string toolbox::unquote(const std::string& s)
{
        std::stringstream ss;
        size_t len = s.length();
        for(size_t i=0 ; i<len ; i++)
        {
                char c = s[i];
		if ( ((i == 0) || (i == (len-1))) && (c == '"')) continue;

                switch(c)
                {
			case '\\':
				break;
                        default:
                                ss << c;
                                break;
                }
        }
        return ss.str();
}



std::string toolbox::toString(char* format, ...)
{
	const int max_size = 1024;
	va_list varlist;
	va_start (varlist, format);
	char tmp[max_size];
	int expected = vsnprintf(tmp, max_size, format, varlist);

	std::string str = tmp;
	if ( expected >= max_size )
	{
		str = str + " ...string is too long";
	}
	return str;
}

std::string toolbox::toString(const char* format, ...)
{
	const int max_size = 1024;
	va_list varlist;
	va_start(varlist, format);
	char tmp[max_size];
	int expected = vsnprintf(tmp, max_size, format, varlist);

	std::string str = tmp;
	if ( expected >= max_size )
	{
		str = str + " ...string is too long";
	}
	return str;
}

std::string toolbox::tolower (const std::string& str)
{
	std::string result = str;
	for (size_t i = 0; i < result.size(); i++)
	{
		result[i] = std::tolower(result[i]);
	}
	return result;
}

std::string toolbox::toupper (const std::string& str)
{
	std::string result = str;
	std::transform(str.begin(), str.end(), result.begin(), (int(*)(int))std::toupper);
	return result;
}

/*
std::string toolbox::trim (const std::string& str)
{
	std::string::size_type start = str.find_first_not_of(' ');
	
	return str.substr(start, str.find_last_not_of(' ') - start +1);

}
*/

/* 
 * Fills a 256-byte bytemask with input. You can specify a range like 'a..z',
 * it needs to be incrementing.  
 */
void toolbox::charmask( const char *input, int len, char *mask) throw (toolbox::exception::Exception) 
{
	 const char *end;
	 char c;

	memset(mask, 0, 256);
	for (end = input+len; input < end; input++) 
	{
		c=*input; 
		if ((input+3 < end) && input[1] == '.' && input[2] == '.' && input[3] >= c) 
		{
			memset(mask+c, 1, input[3] - c + 1);
			input+=3;
		} 
		else if ((input+1 < end) && input[0] == '.' && input[1] == '.') 
		{
			/* Error, try to be as helpful as possible:
			   (a range ending/starting with '.' won't be captured here) */
			if (end-len >= input) 
			{ /* there was no 'left' char */
				XCEPT_RAISE (toolbox::exception::Exception, "Invalid '..'-range, no character to the left of '..'.");
			}
			if (input+2 >= end) 
			{ /* there is no 'right' char */
				XCEPT_RAISE (toolbox::exception::Exception, "Invalid '..'-range, no character to the right of '..'.");
			}
			if (input[-1] > input[2]) 
			{ /* wrong order */
				XCEPT_RAISE (toolbox::exception::Exception, "Invalid '..'-range, '..'-range needs to be incrementing.");
			} 
			/* FIXME: better error (a..b..c is the only left possibility?) */
			XCEPT_RAISE (toolbox::exception::Exception,  "Invalid '..'-range.");
		
		} 
		else 
		{
			mask[(unsigned char)c]=1;
		}
	}
}

/* 
 * mode 1 : trim left
 * mode 2 : trim right
 * mode 3 : trim left and right
 * what indicates which chars are to be trimmed
 */
std::string toolbox::trim(const char *c, int len, const char *what, int what_len,  int mode ) throw (toolbox::exception::Exception) 
{
	register int i;
	int trimmed = 0;
	char mask[256];

	
	toolbox::charmask(what, what_len, mask);
	

	if (mode & 1) {
		for (i = 0; i < len; i++) {
			if (mask[(unsigned char)c[i]]) {
				trimmed++;
			} else {
				break;
			}
		}
		len -= trimmed;
		c += trimmed;
	}
	if (mode & 2) {
		for (i = len - 1; i >= 0; i--) {
			if (mask[(unsigned char)c[i]]) {
				len--;
			} else {
				break;
			}
		}
	}

	return std::string(c,len);
	//return estrndup(c, len);
	
	//return "";
}

std::string toolbox::trim (const std::string& str) throw (toolbox::exception::Exception) 
{
	return toolbox::trim(str.c_str(), str.size()," \t\n\r\v\0",6, 3); 

}

std::string toolbox::trim (const std::string& str, std::string what) throw (toolbox::exception::Exception) 
{
	return toolbox::trim(str.c_str(), str.size(), what.c_str(), what.size(), 3); 

}


bool toolbox::startsWith(const std::string& str, const std::string& start)
{
    std::string::size_type pos = str.find(start);
    return (pos == 0);
}

bool toolbox::endsWith(const std::string& str, const std::string& end)
{
    std::string::size_type pos = str.rfind(end);
    if ((pos != std::string::npos) && (pos == str.size() - end.size())) {
    	return true;
    } else {
    	return false;
    }
}

long toolbox::toLong (const std::string& n) throw (toolbox::exception::Exception)
{
	errno = 0;
	long value = std::strtol(n.c_str(), (char **)NULL, 0);

	if (errno != 0)
	{

		std::string msg = "Error when converting ";
		msg += n;
		msg += ", ";
		msg += strerror(errno);

		if(value >= LONG_MAX)
		{
			msg += ", overflow";
		}
		if(value <= LONG_MIN)
		{
			msg += ", underflow";
		}
	        XCEPT_RAISE (toolbox::exception::Exception, msg);
	}

	return value;
}

unsigned long toolbox::toUnsignedLong (const std::string& n) throw (toolbox::exception::Exception)
{
	errno = 0;
	unsigned long value = std::strtoul(n.c_str(), (char **)NULL, 0);

	if (errno != 0)
	{

		std::string msg = "Error when converting ";
		msg += n;
		msg += ", ";
		msg += strerror(errno);

		if(value >= ULONG_MAX)
		{
			msg += ", overflow";
		}
		if(value <= 0)
		{
			msg += ", invalid";
		}
	        XCEPT_RAISE (toolbox::exception::Exception, msg);
	}

	return value;
}

double toolbox::toDouble (const std::string& n) throw (toolbox::exception::Exception)
{
	errno = 0;
	double value = std::strtod(n.c_str(), (char **)NULL);

	if (errno != 0)
	{
		std::string msg = "Error converting ";
		msg += n;
		msg += "to double, ";
		msg += strerror(errno);
		XCEPT_RAISE (toolbox::exception::Exception, msg);
	}

	return value;
}

// -----------------------------------------------------
// StringTokenizer implementation
// -----------------------------------------------------

toolbox::StringTokenizer::StringTokenizer(const std::string& str, const std::string& delim)
{
   if ((str.length() == 0) || (delim.length() == 0)) return;

   tokenStr_    = str;
   delim_ = delim;

  /*
     Remove sequential delimiter
  */
   size_t currentPos = 0;

   while(true)
   {
      if ((currentPos = tokenStr_.find(delim_,currentPos)) != std::string::npos)
      {
         currentPos +=delim_.length();
         while(tokenStr_.find(delim_,currentPos) == currentPos)
         {
            tokenStr_.erase(currentPos,delim_.length());
         }
      }
      else
       break;
   }

   // Trim leading delimiter
   if (tokenStr_.find(delim_,0) == 0)
   {
      tokenStr_.erase(0,delim_.length());
   }

   // Trim ending delimiter
   currentPos = 0;
   if ((currentPos = tokenStr_.rfind(delim_)) != std::string::npos)
   {
      if (currentPos != (tokenStr_.length()-delim_.length())) return;
      tokenStr_.erase(tokenStr_.length()-delim_.length(),delim_.length());
   }
}

size_t toolbox::StringTokenizer::countTokens()
{
   std::string::size_type prevPos = 0;
   size_t numTokens        = 0;

   if (tokenStr_.length() > 0)
   {
      numTokens = 0;
      size_t currentPos = 0;
      while(true)
      {
         if ((currentPos = tokenStr_.find(delim_,currentPos)) != std::string::npos)
         {
            numTokens++;
            prevPos     = currentPos;
            currentPos += delim_.length();
         }
         else
          break;
      }

      return ++numTokens;
   }
   else
   {
      return 0;
   }
}


bool toolbox::StringTokenizer::hasMoreTokens()
{
   return (tokenStr_.length() > 0);
}


std::string toolbox::StringTokenizer::nextToken()
{
   if (tokenStr_.length() == 0) return "";

   std::string       tStr ="";
   std::string::size_type pos  = tokenStr_.find(delim_,0);

   if (pos != std::string::npos)
   {
      tStr     = tokenStr_.substr(0,pos);
      tokenStr_ = tokenStr_.substr(pos+delim_.length(),tokenStr_.length()-pos);
   }
   else
   {
      tStr = tokenStr_.substr(0,tokenStr_.length());
      tokenStr_ = "";
   }

   return tStr;
}


std::string toolbox::StringTokenizer::nextToken(const std::string& delimiter)
{
   if (tokenStr_.length() == 0) return "";

   std::string       tStr ="";
   std::string::size_type pos  = tokenStr_.find(delimiter,0);

   if (pos != std::string::npos)
   {
      tStr     = tokenStr_.substr(0,pos);
      tokenStr_ = tokenStr_.substr(pos+delimiter.length(),tokenStr_.length()-pos);
   }
   else
   {
      tStr = tokenStr_.substr(0,tokenStr_.length());
      tokenStr_ = "";
   }

   return tStr;
}


std::string toolbox::StringTokenizer::remainingString()
{
   return  tokenStr_;
}


std::string toolbox::StringTokenizer::filterNextToken(const std::string& filterStr)
{
   std::string       str        = nextToken();
   size_t currentPos = 0;

   while((currentPos = str.find(filterStr,currentPos)) != std::string::npos)
   {
      str.erase(currentPos,filterStr.length());
   }

   return str;
}

std::list<std::string> toolbox::parseTokenList(const std::string& str, const std::string& end)
{
	std::list<std::string> l;
	toolbox::StringTokenizer tokens(str,end);
	while (tokens.hasMoreTokens())
	{
		l.push_back(toolbox::trim(tokens.nextToken()));
	} 
	return l;
}

std::set<std::string> toolbox::parseTokenSet(const std::string& str, const std::string& end)
{
	std::set<std::string> s;
	toolbox::StringTokenizer tokens(str,end);
	while (tokens.hasMoreTokens())
	{
			s.insert(toolbox::trim(tokens.nextToken()));
	} 
	return s;

}

std::string toolbox::printTokenSet(const std::set<std::string> & tokenList, const std::string& delimiter)
{
	std::stringstream s;
	std::set<std::string>::const_iterator i;
	i = tokenList.begin();

	while (i != tokenList.end())
	{
		s << (*i);
		++i;
		if (i != tokenList.end())
		{
			s << delimiter;
		}
	}
	return s.str();
}

std::string toolbox::printTokenList(const std::list<std::string> & tokenList, const std::string& delimiter)
{
	std::stringstream s;
	std::list<std::string>::const_iterator i;
	i = tokenList.begin();

	while (i != tokenList.end())
	{
		s << (*i);
		++i;
		if (i != tokenList.end())
		{
			s << delimiter;
		}
	}
	return s.str();
}
