// $Id: random.cc,v 1.3 2008/07/18 15:27:40 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "toolbox/math/random.h"
#include <assert.h>

//----------------------------------------------------------------------------//
// Ran002: combined congruential with shuffle
//----------------------------------------------------------------------------//
//
// Returns an integer random number uniformly distributed within [1,2147483562]  
//
// Reference:
//  (1) This is the long period ( > 2.3 * 10^18 ) random number generator of 
//      P. L'Ecuyer, Commun. ACM 31, 743 (1988), but with Bays-Durham shuffle 
//      and "added safeguards" as proposed by
//  (2) William H. Press and Saul A. Teukolsky,
//      "Portable Random Number Generators",
//      Computers in Phyics, Vol. 6, No. 5, Sep/Oct 1992, Algorithm "ran2"
//  (3) This generator has been validated also by 
//      G. Marsaglia and A. Zaman, 
//      "Some portable very-long-period random number generators",
//      Computers in Physics, Vol. 8, No. 1, Jan/Feb 1994.  
//
// Notes:
//   -  William Press and Saul Teukolsky think that this is a "perfect" 
//      random generator and will pay $1000 for the first one who convinces 
//      them otherwise.
//
//   -  This generator is very slow
//
//----------------------------------------------------------------------------//

static const int NTAB = 32;

static const long IM1  = 2147483563L,
             	  IM2  = 2147483399L,
             	  IA1  = 40014L,
             	  IA2  = 40692L,
             	  IQ1  = 53668L,
             	  IQ2  = 52774L,
             	  IR1  = 12211L,
             	  IR2  = 3791L,
             	  IMM1 = IM1-1L;

//----------------------------------------------------------------------------//

toolbox::math::Ran002::~Ran002 ()
{
}

toolbox::math::Ran002::Ran002 (unsigned long the_seed) : RandomGenerator(the_seed)
{ 
    // requires at least 32 bit arithmetics
    assert( sizeof(unsigned long) >= 4 );

    // initialize the maximum value: max_val
    max_val = IMM1;
    
    // initialize shuffle table using the seed: iv and idum,idum2,iy
    idum  = (seed > 1L) ? seed : 1L;    // Be sure to prevent idum = 0 
    idum2 = idum;			// was previously: idum2 = 123456789L;

    for (int j = NTAB+7; j >= 0; j--) { // Load shuffle table (after 8 warm-ups)
	long k = idum/IQ1;
	idum = IA1 * (idum - k * IQ1) - IR1 * k;
	if (idum < 0) idum += IM1;
	if (j < NTAB) iv[j] = idum;
    }
    iy = iv[0];
}

//----------------------------------------------------------------------------//

unsigned long toolbox::math::Ran002::Long (void)
{
    long j, k;

    k = idum/IQ1;			// Start here when not initializing
    idum = IA1*(idum-k*IQ1)-IR1*k;	// Compute idum=mod(IA1*idum,IM1) without
					// overflows by Schrage's method 
    if (idum < 0) idum += IM1;
    
    k = idum2/IQ2;			// Compute idum2=mod(IA2*idum2,IM2) likewise
    idum2 = IA2*(idum2-k*IQ2)-IR2*k;	// Compute idum=mod(IA1*idum,IM1) without
					// overflows by Schrage's method
    if (idum2 < 0) idum2 += IM2;
    
    j = iy/(1L+IMM1/NTAB);		// Will be in the range 0..NTAB-1
    iy = iv[j]-idum2;			// Output previously stored value and 
					// refill the shuffle table
    iv[j] = idum;
    if (iy < 1) iy += IMM1;
    
    return iy;
}

//----------------------------------------------------------------------------//

//----------------------------------------------------------------------------//
// class NormalDistribution						      //
//----------------------------------------------------------------------------//
//								       	      //
// Return normal (Gaussian) distributed random deviates			      //
// with mean "m" and standard deviation	"s" according to the density:	      //
//								       	      //
//                                           2			       	      //
//			1               (x-m)    			      //
//  p   (x) dx =  ------------  exp( - ------- ) dx   			      //
//   m,s          sqrt(2 pi) s          2 s*s				      //
//								       	      //
//----------------------------------------------------------------------------//

toolbox::math::NormalDistribution::~NormalDistribution ()
{
}

double toolbox::math::NormalDistribution::operator () (void)
{
    // We don't have an extra deviate
    if  (cached == 0) {

	// Pick two uniform numbers in the square extending from -1 tp +1
	// in each direction and check if they are in the unit circle
	double v1, v2, r;
	do {
	    v1 = scale * gen->Long() - 1; // scale maps the random long to [0,2]
	    v2 = scale * gen->Long() - 1;
	    r = v1 * v1 + v2 * v2;
	} while (r >= 1.0);

	double f = sqrt( (-2 * log(r)) / r);

	// Make Box-Muller transformation to get two normal deviates.
	// Return one and save the other for the next time.
	cacheval = v1 * f;
	cached = 1;
	return (v2 * f * s + m);

    // We have an extra deviate, so unset the flag and return it
    } else {
	cached = 0;
	return (cacheval * s + m);
    }
}

//----------------------------------------------------------------------------//

//----------------------------------------------------------------------------//
// class LogNormalDistribution						      //
//----------------------------------------------------------------------------//
//								       	      //
// Return log-normal distributed random deviates			      //
// with given mean and standard deviation stdev				      //
// according to the density function:					      //
//                                                2			      //
//                     1                (ln x - m)			      //
// p   (x) dx =  -------------- exp( - ------------ ) dx  for x > 0	      //
//  m,s          sqrt(2 pi x) s               2				      //
//                                         2 s				      //
//			 						      //
//            = 0   otherwise						      //
//									      //
// where  m  and  s  are related to the arguments mean and stdev by:	      //
// 								       	      //
//                        2						      //
//                    mean						      //
// m = ln ( --------------------- )					      //
//                     2      2						      //
//          sqrt( stdev + mean  )					      //
// 									      //
//                     2      2						      //
//                stdev + mean						      //
// s = sqrt( ln( -------------- ) )					      //
//                        2						      //
//                    mean 						      // 
//								       	      //
//----------------------------------------------------------------------------//

toolbox::math::LogNormalDistribution::~LogNormalDistribution()
{
}

void toolbox::math::LogNormalDistribution::Initialize (double mean, double stddev)
{
    // set mean and standard deviation of the log-normal distribution
    m_log = mean; 
    s_log = stddev; 
    
    // mean "m" and standard deviation "s" of the corresponding 
    // normal distribution wich is a base class of this class
    double m2 = m_log * m_log, 
	   s2 = s_log * s_log,
           sm2 = s2+m2;
    m = log( m2 / sqrt(sm2) );
    s = sqrt( log(sm2/m2) );
}

//----------------------------------------------------------------------------//

