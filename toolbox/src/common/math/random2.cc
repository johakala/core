// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2016, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: D. Simelevicius and L. Orsini                                *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include "toolbox/math/random2.h"

toolbox::math::Ran2::Ran2(unsigned int seed): RandomNumberEngine(seed)
{ 
	//capping seed by the value of maximal 32bit signed integer
	seed_ = seed_ & 0x7fffffff;

	//initialize shuffle table using the seed: iv and idum, idum2, iy
	idum  = (seed >= 1) ? seed : 1; //be sure to prevent idum = 0 
	idum2 = idum;

	for (int j = NTAB + 7; j >= 0; j--) //load shuffle table (after 8 warm-ups)
	{
		int k = idum / IQ1;
		idum = IA1 * (idum - k * IQ1) - k * IR1;
		if (idum < 0)
			idum += IM1;
		if (j < NTAB)
			iv[j] = idum;
	}
	iy = iv[0];
}

int toolbox::math::Ran2::operator()()
{
	int j, k;

	k = idum / IQ1;                            // Start here when not initializing
	idum = IA1 * (idum - k * IQ1) - k * IR1;   // Compute idum=mod(IA1*idum,IM1) without
	                                           // overflows by Schrage's method 
	if (idum < 0)
		idum += IM1;

	k = idum2 / IQ2;                           // Compute idum2=mod(IA2*idum2,IM2) likewise
	idum2 = IA2 * (idum2 - k * IQ2) - k * IR2; // Compute idum=mod(IA1*idum,IM1) without
	                                           // overflows by Schrage's method
	if (idum2 < 0)
		idum2 += IM2;

	j = iy / NDIV;                             // Will be in the range 0..NTAB-1
	iy = iv[j] - idum2;                        // Output previously stored value and
	                                           // refill the shuffle table
	iv[j] = idum;
	if (iy < 1)
		iy += IMM1;

	return iy;
}

int toolbox::math::Ran2::min()
{
	return 1;
}

int toolbox::math::Ran2::max()
{
	return IMM1;
}

