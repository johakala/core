// $Id: PerformanceMeter.cc,v 1.3 2008/07/18 15:27:38 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "toolbox/PerformanceMeter.h"

void toolbox::PerformanceMeter::init(size_t samples) 
{
	samples_ = samples;
	loopCounter_ = 0;
	latency_ = 0.0;
	throughput_= 0.0;
	rate_ = 0.0;
}
	
toolbox::PerformanceMeter::PerformanceMeter()
{
	latency_ = 0.0;
	throughput_= 0.0;
	rate_ = 0.0;
}
			
bool toolbox::PerformanceMeter::addSample(size_t size) 
{	
	if ( loopCounter_ == 0 )
	{	
		chrono_.start(0);
		totalMB_ = 0;
	}
	else if ( loopCounter_ == (samples_ - 1  ) )
	{
		chrono_.stop(0);
		double usecs = (double) chrono_.dusecs();
		loopCounter_ = 0;

		throughput_ = totalMB_ / (usecs/ (double) 1000000.0);
		rate_ = ((double) samples_) / (usecs/ (double) 1000000.0);
		latency_ = ((double) 1000000.0)/rate_;
		return true;			
	}	

	totalMB_ += ( (double) size / (double) 0x100000 );

	loopCounter_++;
	return false;
}	

double toolbox::PerformanceMeter::bandwidth() 
{
	return throughput_;
}

double toolbox::PerformanceMeter::rate() 
{
	return rate_;
}

double toolbox::PerformanceMeter::latency() 
{
	return latency_;
}

