// $Id: Class.cc,v 1.8 2008/07/18 15:27:39 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "toolbox/lang/Class.h"
#include <iostream>

void toolbox::lang::Class::addMethod (toolbox::lang::Method * m , std::string name)
{
	namedMethods_[name] = m;
}


void toolbox::lang::Class::removeMethod (std::string name)
{
	std::map<std::string, toolbox::lang::Method*, std::less<std::string> >::iterator i;
	i = namedMethods_.find(name);
	if (i != namedMethods_.end())
	{
		delete (*i).second;
		namedMethods_.erase(i);
		return;
	}

	// Functor not found, must throw here		
}


void toolbox::lang::Class::addMethod (toolbox::lang::Method * m, size_t fid )
{
	while (fid >= indexedMethods_.size())
	{
		indexedMethods_.push_back (0);
	}

	indexedMethods_[fid] = m;
}


void toolbox::lang::Class::removeMethod(size_t fid)
{
	if (fid < indexedMethods_.size())
	{
		toolbox::lang::Method * f = indexedMethods_[fid];
		if (f != 0)
		{
			delete f;
			indexedMethods_[fid] = 0;
			return;
		}
	}

	// Functor not found, must throw here		
}


toolbox::lang::Method * toolbox::lang::Class::getMethod(const std::string & name)
{
	if ( namedMethods_.find(name) != namedMethods_.end() )
	{
		return namedMethods_[name];
	}
	else
	{
		return 0;
	}
}

toolbox::lang::Method * toolbox::lang::Class::getMethod(size_t fid)
{
	if (indexedMethods_.size() > fid)
	{
		return indexedMethods_[fid];
	}
	else
	{
		return 0;
	}
}

std::vector<toolbox::lang::Method*> toolbox::lang::Class::getMethods() 
{
	std::vector <toolbox::lang::Method*> methods;
	
	// Loop over the SOAP methods (soap and asoap)
	std::map<std::string, toolbox::lang::Method*, std::less<std::string> >::iterator i;
	
	for ( i = namedMethods_.begin(); i != namedMethods_.end(); i++)
	{
		methods.push_back( (*i).second );
	}
	
	for ( std::vector<toolbox::lang::Method*>::size_type i = 0; i < indexedMethods_.size(); i++ )
	{
		if (indexedMethods_[i] != 0)
		{
			methods.push_back(indexedMethods_[i]);
		}
	}

	return methods;

}

