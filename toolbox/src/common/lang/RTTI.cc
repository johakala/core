// $Id: RTTI.cc,v 1.4 2008/07/18 15:27:39 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <cxxabi.h>

#include "toolbox/lang/RTTI.h"
#include "toolbox/string.h"

std::string toolbox::lang::demangle (const std::type_info &ti)
{
                int status;
                char * realname =  abi::__cxa_demangle (ti.name(), 0,0,&status);
                std::string realnameStr = realname;
                free(realname);
                return realnameStr;
}

std::string toolbox::lang::scopemangle (const std::string & ns)
{
	StringTokenizer	t(ns,"::");
	std::string mangled = "";
	while (t.hasMoreTokens() )
	{
		std::string name = t.nextToken();
		mangled += toolbox::toString("%d%s",name.size(), name.c_str());	
	}
	return mangled;
}


