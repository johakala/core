// $Id: stacktrace.cc,v 1.4 2008/07/18 15:27:41 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/
 
#include <toolbox/stacktrace.h>
#include <iostream>
#include <execinfo.h> // Linux header file
//
void toolbox::stacktrace(int depth, std::ostream& stream)
{
	void** dump = 0;

	try
	{
		dump = new void* [depth];

		if(!dump)
		{
			stream << "No memory for stack trace" << std::endl;
			return;
		}

		int size = ::backtrace(dump, depth);
		char** list = ::backtrace_symbols(dump, size);

		if(list)
		{
			for (int i = 0; i < size; ++i)
			{ 
				stream << list[i] << std::endl;
			}
		}
		else
		{
			stream << "No symbols available" << std::endl;
		}
		delete [] dump;
	}
	catch(...)
	{
		delete [] dump;
		stream << "Stacktrace failed" << std::endl;
	}
}
