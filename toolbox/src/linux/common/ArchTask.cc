// $Id: ArchTask.cc,v 1.3 2008/07/18 15:27:41 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <iostream>
#include <unistd.h>

#include "toolbox/Task.h"
#include "toolbox/TaskAttributes.h"
#include "toolbox/TaskGroup.h"


//
// Forward declaration
//
void* threadFunc(void* task);

//
// A wrapper to the POSIX pthread_create function.
// This function is a friend of TaskAttributes and Task
// so that it can access all necessary variables for
// creating a thread.
//



int toolbox::task_spawn(toolbox::TaskAttributes *attrib, toolbox::Task* task)
	throw (toolbox::exception::FailedToActivateTask)
{
	int oldState;

	pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, &oldState);

	int s = pthread_create(&(task->threadid_), &(attrib->attr_), threadFunc, (void*) task); 
	if (s != 0)
	{
		//std::cerr << "PTHREAD_CREATE ERROR" << std::endl;
		//perror(strerror(errno));
		XCEPT_RAISE(toolbox::exception::FailedToActivateTask,strerror(s));
	}
	return 0;
}


toolbox::ArchTask::ArchTask()
{
	pthread_mutex_init (&task_condvar_lock, 0);
	pthread_cond_init  (&task_condvar, 0);
	group_ = 0;
}

toolbox::ArchTask::~ArchTask()
{
	pthread_mutex_destroy(&task_condvar_lock);
	pthread_cond_destroy (&task_condvar); 
}

void toolbox::ArchTask::initTaskGroup(TaskGroup* group)
{
	group_ = group;
}

void toolbox::ArchTask::incRefCount()
{
	if (group_ != 0)
		group_->incRefCount();
}

void toolbox::ArchTask::decRefCount()
{
	if (group_ != 0)
		group_->decRefCount();
}


