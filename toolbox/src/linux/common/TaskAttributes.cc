// $Id: TaskAttributes.cc,v 1.3 2008/07/18 15:27:41 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include <string.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <iostream>
#include <unistd.h>

#include "toolbox/TaskAttributes.h"

toolbox::TaskAttributes::TaskAttributes()
{

}


toolbox::TaskAttributes::~TaskAttributes()
{
	//GUT memory leak?
	delete name_;
}


int
toolbox::TaskAttributes::stacksize(int size)
{
  return pthread_attr_setstacksize(&attr_,size);
}

int
toolbox::TaskAttributes::stacksize()
{ 
  size_t stacksize;
  return pthread_attr_getstacksize(&attr_, &stacksize);
  return stacksize;
}


int
toolbox::TaskAttributes::priority(int level)
{
  struct sched_param priority;
  priority.sched_priority = level;
  
  if (pthread_attr_setschedparam(&attr_, &priority)!=0) 
  {
    std::cout << "Error setting task priority: " << strerror(errno) << std::endl;
    return 1;
  }
  return 0;
}



int
toolbox::TaskAttributes::priority()
{
  struct sched_param priority;
  pthread_attr_getschedparam(&attr_, &priority);
  return priority.sched_priority;
}



int
toolbox::TaskAttributes::name(const char* name)
{
  name_ = strdup(name);
  if (name_ == 0) 
  {
  	return 0;
  }
  else 
  {
  	return 1;
  }
  // return (int) name_;
}


char*
toolbox::TaskAttributes::name()
{
  return name_;
}

int
toolbox::TaskAttributes::detached(bool detach)
{
	if (detach)
	{
		return pthread_attr_setdetachstate(&attr_,PTHREAD_CREATE_DETACHED);
	} 
	else 
	{
		return pthread_attr_setdetachstate(&attr_,PTHREAD_CREATE_JOINABLE);
	} 
}

bool
toolbox::TaskAttributes::detached()
{ 
	int detach;
	pthread_attr_getdetachstate(&attr_, &detach);
	if (detach == PTHREAD_CREATE_DETACHED) return true;
	else return false;
}



