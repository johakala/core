// $Id: Task.cc,v 1.3 2008/07/18 15:27:41 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/
#include <stdlib.h>
#include <string>
#include <stdarg.h>
#include <signal.h>
#include <unistd.h>

#include "toolbox/Task.h"
#include "toolbox/TaskAttributes.h"
#include "toolbox/TaskGroup.h"


// LOG4CPLUS INCLUDES
#include "log4cplus/logger.h"
#include "log4cplus/consoleappender.h"
#include "log4cplus/helpers/appenderattachableimpl.h"
#include "log4cplus/helpers/loglog.h"
#include "log4cplus/helpers/pointer.h"
#include "log4cplus/spi/loggingevent.h"
#include <log4cplus/ndc.h>

using namespace log4cplus;
using namespace log4cplus::helpers;
using namespace log4cplus::spi;


toolbox::Task::Task(const char * name)
{
  attrib_ = new toolbox::TaskAttributes();
  attrib_->name(name);
  
  // make sure that the task is detached so that the
  // cleanup after the svc is left is ensured
  attrib_->detached(true);
  
  yield_counter_ = 0;
}



toolbox::Task::~Task()
{
  delete attrib_;
}

static void terminate_taskhandler()
{
	std::cout << "Uncaught exception in toolbox::task thread" << std::endl;
#ifdef __GNUC__
	__gnu_cxx::__verbose_terminate_handler();
#else
	abort()
#endif /* __GNUC__ */
}

static void unexpected_taskhandler()
{
	std::cout << "Unexpected exception in toolbox::task thread" << std::endl;
#ifdef __GNUC__
	__gnu_cxx::__verbose_terminate_handler();
#else
	abort()
#endif /* __GNUC__ */
}

//
// The hook function for the service routine
//

void* threadFunc(void* task)
{
	 // block all signals, only main thread shall handle signals
	sigset_t mask;
	sigemptyset(&mask);
	sigaddset(&mask, SIGINT);
	sigaddset(&mask, SIGQUIT);
	sigaddset(&mask, SIGTERM);
	sigaddset(&mask, SIGPIPE);
	sigaddset(&mask, SIGABRT);
	sigaddset(&mask, SIGSEGV);		
	sigprocmask(SIG_BLOCK,&mask,0);
 
  //Task* t = (Task*) task;

  // use of logger NDC to set thread of execution context
  toolbox::TaskAttributes attributes;
  ((toolbox::Task*) task)->get(&attributes); 
  getNDC().push(attributes.name());

	std::set_terminate(terminate_taskhandler);
	std::set_unexpected(unexpected_taskhandler);

  // Do the actual work
  ((toolbox::Task*) task)->svc(); 

  getNDC().pop();
  
  
  ((toolbox::Task*) task)->decRefCount();

  return (void*) 0;
}



int toolbox::Task::get(toolbox::TaskAttributes* attrib)
{
  attrib->stacksize(attrib_->stacksize());
  attrib->priority(attrib_->priority());
  attrib->name(attrib_->name());
  return 0;
}

int toolbox::Task::set(toolbox::TaskAttributes* attrib)
{
  //
  // Copy constructor may be better later on
  //
  attrib_->stacksize(attrib->stacksize());
  attrib_->priority(attrib->priority());
  attrib_->name(attrib->name());

  return 0;
}






int toolbox::Task::activate(int argc, char* argv[]) throw(toolbox::exception::FailedToActivateTask)
{
  this->argc_ = argc;
  this->argv_ = argv;

  this->incRefCount();

  return task_spawn(attrib_, this);
}


// this function is called when activate is invoked without parameters
int toolbox::Task::activate(int argc, ...) throw(toolbox::exception::FailedToActivateTask)
{
  if (argc != 0) 
  {
    argc_ = argc;
    argv_ = new char*[argc];

    va_list pvar;
  
    //va_start(pvar, int);
    va_start(pvar, argc);
    int i = 0;

    while (i < argc_) 
    {
      argv_[i++] = va_arg(pvar, char*);
    }

    va_end(pvar);
  }
  
  this->incRefCount();

  //
  // Perform the actual spawn
  //
  return task_spawn(attrib_, this);
}


int toolbox::Task::kill()
{
  return pthread_cancel(threadid_);
}

void toolbox::Task::yield(int every_n_times)
{
	yield_counter_++;
	if (yield_counter_ == every_n_times)
	{
		sched_yield();
		yield_counter_ = 0;
	}
}



int toolbox::Task::wakeup()
{
  pthread_mutex_lock   (&task_condvar_lock);
  pthread_cond_signal  (&task_condvar);
  pthread_mutex_unlock (&task_condvar_lock);

  return 0;
}


int toolbox::Task::pause()
{
	int err = 0;

	do {
 		pthread_mutex_lock   (&task_condvar_lock);
 		err = pthread_cond_wait    (&task_condvar, &task_condvar_lock);
 		pthread_mutex_unlock (&task_condvar_lock);
	} while (err != 0);

	return err;
}


int toolbox::Task::sleep(int seconds)
{
	::sleep(seconds);
	return 0;
}



