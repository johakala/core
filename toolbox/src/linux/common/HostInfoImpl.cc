// $Id: HostInfoImpl.cc,v 1.3 2009/05/12 13:33:51 rmoser Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/
 
#include "toolbox/HostInfoImpl.h"


#include <linux/version.h>

//#include <unistd.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <sys/sysinfo.h>
#include <ios>
#include <iostream>
#include <fstream>
#include <sstream>

#include "toolbox/TimeVal.h"
#include "config/PackageInfo.h"

#ifndef CLK_TCK
#define CLK_TCK 100
#endif

toolbox::HostInfoImpl::HostInfoImpl() throw (toolbox::exception::Exception)
{
	// Information in /proc/stat are counted in jiffies. The number of jiffies per second is:
	jiffies_ = sysconf(_SC_CLK_TCK);

	// Initialize the sample timestamp
	struct timeval now;
	gettimeofday (&now, 0);
	
	last_sample_time_ = now.tv_sec + (now.tv_usec/1000000.0); // initialize sample time
	
	last_process_time_ = 0; // initialize last process CPU usage time

	// Get the number of processors;
	ncpu_ = get_nprocs();

	new_.resize(ncpu_+1);
	old_.resize(ncpu_+1);
	cpuUsage_.resize(ncpu_+1);
	for(size_t i=0 ; i<(ncpu_+1) ; i++)
	{
		new_[i].nice_        = 0;
		new_[i].user_        = 0;
		new_[i].system_      = 0;
		new_[i].idle_        = 0;
		new_[i].iowait_      = 0;
		new_[i].irqwait_     = 0;
		new_[i].softirqwait_ = 0;

		old_[i].nice_        = 0;
		old_[i].user_        = 0;
		old_[i].system_      = 0;
		old_[i].idle_        = 0;
		old_[i].iowait_      = 0;
		old_[i].irqwait_     = 0;
		old_[i].softirqwait_ = 0;

		cpuUsage_[i]         = 0.0; // initialize CPU usage;
	}

	// Open the stat file
	host_info = new std::ifstream("/proc/stat");
    	if (! *host_info) 
	{ 
		XCEPT_RAISE (toolbox::exception::Exception, "Cannot open file /proc/stat");
	}
	
}

toolbox::HostInfoImpl::~HostInfoImpl() 
{
	// close process stat file
	host_info->close();
	delete host_info;
}

/*
double toolbox::HostInfoImpl::getUserUsage(unsigned int cpunum)
{
	return ((double)(new_[0].user_-old_[0].user_))/jiffies_;
}
*/

double toolbox::HostInfoImpl::getCpuUsage()
{
	return cpuUsage_[0];
}

double toolbox::HostInfoImpl::sampleDuration ()
{
//	return ((double)(new_[0].all_ - old_[0].all_))/jiffies_;
	return last_sample_duration_;
}
	
// ---------------------------------------------------------------
// API function implementations
// ---------------------------------------------------------------

void toolbox::HostInfoImpl::sample()
{
	old_ = new_;

	struct timeval sample_time;
	gettimeofday (&sample_time, 0);

	// compare to last sample time. Only sample if new sample
	// time is at least 1 microsend after the last sample time.
	double now = sample_time.tv_sec + (sample_time.tv_usec / 1000000.0);

	// If the current sample time - last sample time is smaller than 5 seconds
	// do not re-calculate. The values will not be accurate otherwise
	//
	if ((now - last_sample_time_) <= (double) minSampleTime)
	{
		// don't sample and don't update sample time, return
		return;
	}
	
	last_sample_duration_ = now-last_sample_time_;

	// Rewind to beginning of /proc/stat file
	host_info->clear();
	host_info->seekg(0,std::ios::beg);
	host_info->sync();

	// Values that can be retrieved from /proc/stat:

	std::string line;
	while ( std::getline(*host_info,line) )
	{
		std::istringstream liness(line);

		// identifier %s
		std::string identifier;
		liness >> identifier; //identifier;
		if(identifier.find("cpu") == 0)
		{
//			std::cout << line << std::endl;

			unsigned int cpunum = 0;
			if(identifier == "cpu")
			{
				cpunum = 0;
			}
			else
			{
				// determine cpu number starting with 1
				identifier.erase(0,3);
				std::istringstream iss(identifier);
				iss >> cpunum;
				cpunum++;
			}
			if(cpunum > old_.size())
			{
				old_.resize(cpunum+1);
				new_.resize(cpunum+1);
				cpuUsage_.resize(cpunum+1);
				old_[cpunum].nice_ = 0;
				old_[cpunum].user_ = 0;
				old_[cpunum].system_ = 0;
				old_[cpunum].idle_ = 0;
				old_[cpunum].iowait_ = 0;
				old_[cpunum].irqwait_ = 0;
				old_[cpunum].softirqwait_ = 0;
				cpuUsage_[cpunum] = 0;
			}

			// summarized counters for all CPUs
			// nice %lld
			liness >> new_[cpunum].nice_;

			// user %lld
			liness >> new_[cpunum].user_;

			// system %lld
			liness >> new_[cpunum].system_;

			// idle %lld
			liness >> new_[cpunum].idle_;

			// iowait %lld
			liness >> new_[cpunum].iowait_;

			// irqwait %lld
			liness >> new_[cpunum].irqwait_;

			// softirqwait %lld
			liness >> new_[cpunum].softirqwait_;

			new_[cpunum].all_ = new_[cpunum].nice_ + new_[cpunum].user_ + new_[cpunum].system_ + new_[cpunum].idle_ + new_[cpunum].iowait_ + new_[cpunum].irqwait_ + new_[cpunum].softirqwait_;

			cpuUsage_[cpunum] = 100.0-100.0*((double)(new_[cpunum].idle_-old_[cpunum].idle_))/((double)(new_[cpunum].all_-old_[cpunum].all_));

			// cpuUsage must be between 0 and 100
			if(cpuUsage_[cpunum] > 100.0)
			{
				cpuUsage_[cpunum] = 100.0;
			}
			else if(cpuUsage_[cpunum] < 0.0)
			{
				cpuUsage_[cpunum] = 0.0;
			}
		}
		else
		{
//			std::cout << "ignore line starting with '" << identifier << "'" << std::endl;
			// ignore
		}
	}

	last_sample_time_ = now;
}
  
void toolbox::HostInfoImpl::show()
{
	std::cout << "Host Statistics" << std::endl;
	for( size_t i = 0 ; i<old_.size() ; i++)
	{
		std::cout << "CPU usage " << cpuUsage_[i] <<std::endl;
		std::cout << "cpu(old): "
			<< new_[i].nice_ << " "
			<< new_[i].user_ << " "
			<< new_[i].system_ << " "
			<< new_[i].idle_ << " "
			<< new_[i].iowait_ << " "
			<< new_[i].irqwait_ << " "
			<< new_[i].softirqwait_ << std::endl;
		std::cout << "cpu(new): "
			<< old_[i].nice_ << " "
			<< old_[i].user_ << " "
			<< old_[i].system_ << " "
			<< old_[i].idle_ << " "
			<< old_[i].iowait_ << " "
			<< old_[i].irqwait_ << " "
			<< old_[i].softirqwait_ << std::endl;
	}
}

