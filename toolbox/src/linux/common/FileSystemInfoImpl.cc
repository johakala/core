// $Id: FileSystemInfoImpl.cc,v 1.4 2008/07/18 15:27:41 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "toolbox/FileSystemInfoImpl.h"

#include <string.h>
#include <sys/statvfs.h>
#include <fstab.h>
#include <iostream>
#include <error.h>
#include <errno.h>
#include <sstream>

toolbox::FileSystemInfoImpl::FileSystemInfoImpl()
{

}

toolbox::FileSystemInfoImpl::~FileSystemInfoImpl()
{

}

std::set<std::string> toolbox::FileSystemInfoImpl::getFileSystems()
	throw (toolbox::exception::ReadError)
{
	if (fs_.empty())
	{
		try
		{		
			this->sample();
		}
		catch (toolbox::exception::ReadError& e)
		{
			std::stringstream msg;
			msg << "Failed to obtain information about file systems";
			XCEPT_RETHROW (toolbox::exception::ReadError, msg.str(), e);
		}
	}
	
	std::set<std::string> fsNames;

	std::map<std::string, Record>::iterator i;
	for (i = fs_.begin(); i != fs_.end(); ++i)
	{
		fsNames.insert( (*i).first );
	}
	return fsNames;
}

std::string toolbox::FileSystemInfoImpl::state(const std::string& fs)
	throw (toolbox::exception::NotFound, toolbox::exception::Unsupported)
{
	std::map<std::string, Record>::iterator i = fs_.find(fs);
	
	if (i == fs_.end())
	{
		std::stringstream msg;
		msg << "File system '" << fs << "' not found";
		XCEPT_RAISE (toolbox::exception::NotFound, msg.str());
	}
	
	return (*i).second.state;
}

double toolbox::FileSystemInfoImpl::available(const std::string& fs, const std::string& unit)
	throw (toolbox::exception::NotFound, toolbox::exception::Unsupported)
{
	std::map<std::string, Record>::iterator i = fs_.find(fs);
	
	if (i == fs_.end())
	{
		std::stringstream msg;
		msg << "File system '" << fs << "' not found";
		XCEPT_RAISE (toolbox::exception::NotFound, msg.str());
	}
	
	
	if ((*i).second.total == 0)
	{
		return 0.0;
	}
	
	try
	{
		double v = (*i).second.available;
		if (unit == "%")
		{
			return ( (100 * v) / (*i).second.total);
		}
		else
		{
			return this->convertTo (v, (*i).second.blocksize, unit);
		}
	}
	catch (toolbox::exception::Unsupported &e)
	{
		std::stringstream msg;
		msg << "Failed to retrieve total space for file system '" << fs;
		msg << "'";
		XCEPT_RETHROW (toolbox::exception::Unsupported, msg.str(), e);
	}
}

double toolbox::FileSystemInfoImpl::used(const std::string& fs, const std::string& unit)
	throw (toolbox::exception::NotFound, toolbox::exception::Unsupported)
{
	std::map<std::string, Record>::iterator i = fs_.find(fs);
	
	if (i == fs_.end())
	{
		std::stringstream msg;
		msg << "File system '" << fs << "' not found";
		XCEPT_RAISE (toolbox::exception::NotFound, msg.str());
	}
	
	if ((*i).second.total == 0)
	{
		return 0.0;
	}
	
	try
	{
		double v = (*i).second.total-(*i).second.available;
		if (unit == "%")
		{
			return ( (100 * v) / (*i).second.total);
		}
		else
		{
			return this->convertTo (v, (*i).second.blocksize, unit);
		}
	}
	catch (toolbox::exception::Unsupported &e)
	{
		std::stringstream msg;
		msg << "Failed to retrieve total space for file system '" << fs;
		msg << "'";
		XCEPT_RETHROW (toolbox::exception::Unsupported, msg.str(), e);
	}
}

double toolbox::FileSystemInfoImpl::total(const std::string& fs, const std::string& unit)
	throw (toolbox::exception::NotFound, toolbox::exception::Unsupported)
{
	std::map<std::string, Record>::iterator i = fs_.find(fs);
	
	if (i == fs_.end())
	{
		std::stringstream msg;
		msg << "File system '" << fs << "' not found";
		XCEPT_RAISE (toolbox::exception::NotFound, msg.str());
	}
	
	try
	{
		if (unit == "%") 
		{
			return 100.0;
		}
		else
		{	
			return this->convertTo ((*i).second.total, (*i).second.blocksize, unit);
		}
	}
	catch (toolbox::exception::Unsupported &e)
	{
		std::stringstream msg;
		msg << "Failed to retrieve total space for file system '" << fs;
		msg << "'";
		XCEPT_RETHROW (toolbox::exception::Unsupported, msg.str(), e);
	}
}

toolbox::TimeVal toolbox::FileSystemInfoImpl::sample ()
	throw (toolbox::exception::ReadError)
{
	if (setfsent() == 1)
	{
	        struct fstab* f;
		for (f = getfsent(); f != 0; f = getfsent())	
		{		
			std::string device =  f->fs_spec;
			// std::cout << "Mount point: " << f->fs_file << std::endl;

			// Only attempt to get information about real file systems
			//
			if (device != "none")
			{
				//device.erase (0, 6);
				struct statvfs fs;
				errno = 0;
				int r = statvfs (f->fs_file, &fs);
				if (r == 0)
				{
					fs_[f->fs_file].available = fs.f_bfree;
					fs_[f->fs_file].total = fs.f_blocks;
					fs_[f->fs_file].blocksize = fs.f_bsize;
					fs_[f->fs_file].state = "OK";
				}
				else if ((r == -1) && (errno == ENOENT))
				{
					fs_[f->fs_file].available = 0;
					fs_[f->fs_file].total = 0;
					fs_[f->fs_file].blocksize = 0;
					fs_[f->fs_file].state = "size unknown";
				}
				else if ((r == -1) && (errno == EOVERFLOW))
				{
					fs_[f->fs_file].available = 0;
					fs_[f->fs_file].total = 0;
					fs_[f->fs_file].blocksize = 0;
					fs_[f->fs_file].state = "unlimited size";
				}
				else
				{
					std::stringstream msg;
					msg << "Failed to read information (errno = " << errno << ") for file system '" << f->fs_file << "', ";
					msg << strerror(errno);
					XCEPT_RAISE (toolbox::exception::ReadError, msg.str());
				}
			}
		} 

		endfsent();
	}
	else
	{
		std::stringstream msg;
		msg << "Failed to access file system information, ";		
		msg << strerror(errno);
		XCEPT_RAISE (toolbox::exception::ReadError, msg.str());
	}

	sampleTime_ = toolbox::TimeVal::gettimeofday();
	return sampleTime_;
}

double toolbox::FileSystemInfoImpl::sampleDuration ()
{
	return (toolbox::TimeVal::gettimeofday()-sampleTime_);
}

toolbox::TimeVal toolbox::FileSystemInfoImpl::sampleTime ()
{
	return sampleTime_;
}

void toolbox::FileSystemInfoImpl::show ()
{

}
	
double toolbox::FileSystemInfoImpl::convertTo (double value, size_t blocksize, const std::string& unit)
	throw (toolbox::exception::Unsupported)
{
	if (unit == "B")
	{
		return (value * blocksize);
	}
	else if (unit == "kB")
	{
		return ((value/1024)*blocksize);
	}
	else if (unit == "MB")
	{
		return ((value/(1024 * 1024))*blocksize);
	}
	else if (unit == "GB")
	{
		return ((value/(1024 * 1024 * 1024))*blocksize);
	}
	else
	{
		std::stringstream msg;
		msg << "Unit '" << unit << "' not recognized";
		XCEPT_RAISE (toolbox::exception::Unsupported, msg.str());
	}
}
