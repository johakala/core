// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2016, CERN.			                             *
 * All rights reserved.                                                  *
 * Authors: L. Orsini and D. Simelevicius				                 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                             *
 * For the list of contributors see CREDITS.   			                 *
 *************************************************************************/

#ifndef _toolbox_TimeIntervalLA_h_
#define _toolbox_TimeIntervalLA_h_

#include <iostream>
#include <string>
#include <sstream>

namespace toolbox {


class TimeIntervalLA
{
public:

	typedef enum {PSYMBOL,
				 TSYMBOL,
				 YEARS,
				 DAYS,
				 WEEKS,
				 HOURS,
				 MSYMBOL,
				 SECONDS,
				 INTVAL,
				 EMPTY } Token;

	TimeIntervalLA(const std::string & in);
	virtual ~TimeIntervalLA();

	Token getNextToken();
	std::string getLexeme();


private:

	std::istringstream instream_;
	std::string lexeme_;

};
}

#endif
