// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2013, CERN.			                 		 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, A. Petrucci, C.Wakefield					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         	 *
 *************************************************************************/

#ifndef _toolbox_ThreadPolicy_h_
#define _toolbox_ThreadPolicy_h_

#include <string>

#include "toolbox/Policy.h"

#include "toolbox/exception/Exception.h"

namespace toolbox
{
	class ThreadPolicy: public Policy
	{
		public:

		virtual ~ThreadPolicy();
		std::string getName();
		std::string getType();
		virtual void setDefaultPolicy () throw (toolbox::exception::Exception) = 0;
	};
}

#endif
