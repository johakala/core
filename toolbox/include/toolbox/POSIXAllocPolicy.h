// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2013, CERN.			                 		 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, A. Petrucci, C.Wakefield					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         	 *
 *************************************************************************/

#ifndef _toolbox_POSIXAllocPolicy_h_
#define _toolbox_POSIXAllocPolicy_h_

#include <string>

#include "toolbox/AllocPolicy.h"

#include "toolbox/exception/Exception.h"

namespace toolbox
{
	class POSIXAllocPolicy: public AllocPolicy
	{
		public:

		POSIXAllocPolicy();
		virtual ~POSIXAllocPolicy();	
		std::string getPackage();
		void* alloc (size_t size) throw (toolbox::exception::Exception);
		void free (void* buffer, size_t size) throw (toolbox::exception::Exception);

	};
}

#endif
