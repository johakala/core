// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2016, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: D. Simelevicius and L. Orsini                                *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _toolbox_math_random2_h_
#define _toolbox_math_random2_h_

#include <iostream>
#include <limits>
#include <sys/types.h>
#include <algorithm>
#include <math.h>
#include <cmath>
#include <assert.h>
#include <stdint.h>

namespace toolbox {
namespace math {

//----------------------------------------------------------------------------//
// class RandomNumberEngine: base class for random number engines             //
//----------------------------------------------------------------------------//

class RandomNumberEngine
{
public:
	RandomNumberEngine(unsigned int seed): seed_(seed) {}
	virtual ~RandomNumberEngine() {}
	//returns integer uniform deviate in the range [min..max]
	virtual int operator()() = 0;
	//returns the lowest possible random number
	virtual int min() = 0;
	//returns the highest possible random number
	virtual int max() = 0;
protected:
	unsigned int seed_; // the seed of the random engine
};

//----------------------------------------------------------------------------------//
// class Ran2: random number engine ran2                                          //
//                                                                                  //
// William H. Press, Saul A. Teukolsky, William T. Vetterling, Brian P. Flannery    //
// Numerical Recipes in C++: The Art of Scientific Computing, Second Edition, 2002. //
// ran2 algorithm page 286                                                          //
//----------------------------------------------------------------------------------//

class Ran2: public RandomNumberEngine
{
public:
	Ran2(unsigned int seed = 0);
	//returns integer uniform deviate in the range [1..2147483562]
	virtual int operator()();
	//returns the lowest possible random number
	virtual int min();
	//returns the highest possible random number
	virtual int max();
private:
	static const int IM1 = 2147483563;
	static const int IM2 = 2147483399;
	static const int IA1 = 40014;
	static const int IA2 = 40692;
	static const int IQ1 = 53668;
	static const int IQ2 = 52774;
	static const int IR1 = 12211;
	static const int IR2 = 3791;
	static const int NTAB = 32;
	static const int IMM1 = IM1 - 1;
	static const int NDIV = 1 + IMM1 / NTAB;
	int idum, idum2, iy, iv[NTAB];
};

typedef Ran2 DefaultRandomEngine;

//--------------------------------------------------------------------------------//
// class CanonicalRandomEngine: generates real random numbers in the range [0..1) //
//--------------------------------------------------------------------------------//

template <class Real_type>
class CanonicalRandomEngine
{
public:
	CanonicalRandomEngine(RandomNumberEngine &ng): ng_(ng)
	{
		//Algorithm from GCC source code
		const size_t b = std::min(static_cast<size_t>(std::numeric_limits<Real_type>::digits), static_cast<size_t>(std::numeric_limits<int>::digits));
		r_ = static_cast<long double>(ng_.max()) - static_cast<long double>(ng_.min()) + 1.0L;
		const size_t log2r = log(r_) / log(2.0L);
		m_ = std::max<size_t>(1UL, (b + log2r - 1UL) / log2r);
	}
	Real_type operator()()
	{
		//Algorithm from GCC source code
		Real_type ret;
		do
		{
			Real_type sum = Real_type(0);
			Real_type tmp = Real_type(1);
			for (size_t k = m_; k != 0; --k)
			{
				sum += Real_type(ng_() - ng_.min()) * tmp;
				tmp *= r_;
			}
			ret = sum / tmp;
		}
		while (__builtin_expect(ret >= Real_type(1), 0));
		return ret;
	}
private:
	RandomNumberEngine &ng_;
	size_t m_;
	long double r_;
};

//--------------------------------------------------------------------------------------//
// class UniformIntDistribution: class generates uniformly distributed integer numbers //
//--------------------------------------------------------------------------------------//

template <class Integer_type>
class UniformIntDistribution
{
public:
	UniformIntDistribution(Integer_type min, Integer_type max): min_(min), max_(max)
	{
		//Integer_type should be bigger or equal to the type provided by RandomNumberEngine
		assert(sizeof(Integer_type) >= sizeof(int));
	}
	//generates random number uniformely distributed in the range [min..max]
	Integer_type operator()(RandomNumberEngine &ng)
	{
		return this->operator()(ng, min_, max_);
	}
	Integer_type operator()(RandomNumberEngine &ng, Integer_type min, Integer_type max)
	{
		//Algorithm from GCC source code
		const int ngmin = ng.min();
		const int ngmax = ng.max();
		int ngrange = ngmax - ngmin;
		Integer_type range = max - min;
		Integer_type ret;

		if (ngrange > range)
		{
			// downscaling
			const Integer_type uerange = range + 1; // range can be zero
			const Integer_type scaling = ngrange / uerange;
			const Integer_type past = uerange * scaling;
			do
				ret = ng() - ngmin;
			while (ret >= past);
			ret /= scaling;
		}
		else if (ngrange < range)
		{
			// upscaling
			/*
			              Note that every value in [0, range]
			              can be written uniquely as
			              (ngrange + 1) * high + low
			              where
			              high in [0, range / (ngrange + 1)]
			              and
			              low in [0, ngrange].
			 */
			Integer_type tmp; // wraparound control
			do
			{
				const Integer_type uerngrange = ngrange + 1;
				tmp = uerngrange * operator()(ng, 0, range / uerngrange);
				ret = tmp + ng() - ngmin;
			}
			while (ret > range || ret < tmp);
		}
		else
			ret = ng() - ngmin;

		return ret + min;
	}
	Integer_type min()
	{
		return min_;
	}
	Integer_type max()
	{
		return max_;
	}
private:
	Integer_type min_; //minimal value
	Integer_type max_; //maximal value
};

//--------------------------------------------------------------------------------------//
// class UniformRealDistribution: class generates uniformly distributed real numbers   //
//--------------------------------------------------------------------------------------//

template <class Real_type>
class UniformRealDistribution
{
public:
	UniformRealDistribution(Real_type min, Real_type max): min_(min), max_(max) {};
	//generates random number uniformely distributed in the range [min..max)
	Real_type operator()(CanonicalRandomEngine<Real_type> &cng)
	{
		return this->operator()(cng, min_, max_);
	}
	Real_type operator()(CanonicalRandomEngine<Real_type> &cng, Real_type min, Real_type max)
	{
		return cng() * (max - min) + min;
	}
	Real_type min()
	{
		return min_;
	}
	Real_type max()
	{
		return max_;
	}
private:
	Real_type min_; //minimal value
	Real_type max_; //maximal value
};

//----------------------------------------------------------------------------------//
// class NormalRealDistribution: normal (Gaussian) distributed random numbers       //
//                                                                                  //
// William H. Press, Saul A. Teukolsky, William T. Vetterling, Brian P. Flannery    //
// Numerical Recipes in C++: The Art of Scientific Computing, Second Edition, 2002. //
// Page 293                                                                         //
//----------------------------------------------------------------------------------//

template <class Real_type>
class NormalRealDistribution
{
public:
	NormalRealDistribution(Real_type mean, Real_type stddev): mean_(mean), stddev_(stddev), iset_(false), gset_(0)
	{
	}
	//generates random normally distributed number
	Real_type operator()(CanonicalRandomEngine<Real_type> &cng)
	{
		return this->operator()(cng, mean_, stddev_);
	}
	Real_type operator()(CanonicalRandomEngine<Real_type> &cng, Real_type mean, Real_type stddev)
	{
		Real_type fac;
		Real_type rsq;
		Real_type v1;
		Real_type v2;

		// We don't have an extra deviate
		if ( !iset_ )
		{
			// Pick two uniform numbers in the square extending from -1 to +1
			// in each direction and check if they are in the unit circle
			do
			{
				v1 = 2 * cng() - 1;
				v2 = 2 * cng() - 1;
				rsq = v1 * v1 + v2 * v2;
			} while ( (rsq >= 1.0) || (rsq == 0.0) );

			fac = sqrt((-2 * log(rsq)) / rsq);

			// Make Box-Muller transformation to get two normal deviates.
			// Return one and save the other for the next time.
			gset_ = v1 * fac;
			iset_ = true;
			return v2 * fac * stddev + mean;
		}
		else // We have an extra deviate, so unset the flag and return it
		{
			iset_ = false;
			return gset_ * stddev + mean;
		}
	}
	Real_type mean()
	{
		return mean_;
	}
	Real_type stddev()
	{
		return stddev_;
	}
private:
	Real_type mean_; //mean value
	Real_type stddev_; //standard deviation
	bool iset_;
	Real_type gset_;
};

//-----------------------------------------------------------------------------//
// class LogNormalRealDistribution: log-normal distributed random numbers      //
//                                                                             //
//WARNING (mean, stddev) are not equal to (m, s) please find description below //
//                                                                             //
//mean - Mean of log-normal distribution.                                      //
//stddev - Standard deviation of log-normal distribution.                      //
//m - Mean of the underlying normal distribution formed by the logarithm       //
//transformations of the possible values in this distribution.                 //
//s - Standard deviation of the underlying normal distribution formed by the   //
//logarithm transformations of the possible values in this distribution.       //
//-----------------------------------------------------------------------------//

template <class Real_type>
class LogNormalRealDistribution
{
public:
	LogNormalRealDistribution(Real_type mean, Real_type stddev): mean_(mean), stddev_(stddev), normDist_(0, 1)
	{
		this->transformMeanStddev(mean, stddev, m_, s_);
	}
	//generates random log normally distributed number
	Real_type operator()(CanonicalRandomEngine<Real_type> &cng)
	{
		return this->transformRandom(cng, m_, s_);
	}
	Real_type operator()(CanonicalRandomEngine<Real_type> &cng, Real_type mean, Real_type stddev)
	{
		Real_type m;
		Real_type s;
		this->transformMeanStddev(mean, stddev, m, s);
		return this->transformRandom(cng, m, s);
	}
	Real_type mean()
	{
		return mean_;
	}
	Real_type stddev()
	{
		return stddev_;
	}
	Real_type m()
	{
		return m_;
	}
	Real_type s()
	{
		return s_;
	}
private:
	Real_type transformRandom(CanonicalRandomEngine<Real_type> &cng, Real_type m, Real_type s)
	{
		return std::exp(normDist_(cng, m, s));
	}
	void transformMeanStddev(Real_type mean, Real_type stddev, Real_type &m, Real_type &s)
	{
		//mean "m" and standard deviation "s" of the corresponding normal distribution
		Real_type m2 = mean * mean;
		Real_type s2 = stddev * stddev;
		Real_type sm2 = s2 + m2;
		m = log(m2 / sqrt(sm2));
		s = sqrt(log(sm2 / m2));
	}
	Real_type mean_;
	Real_type stddev_;
	Real_type m_;
	Real_type s_;
	NormalRealDistribution<Real_type> normDist_;
};

}
}

#endif

/* ------------------------------------- Example -----------------------------------------
#include <iostream>
#include <climits>
#include <map>
#include <unistd.h>
#include <limits>
#include <math.h>
#include "toolbox/math/random2.h"

int main()
{
	toolbox::math::DefaultRandomEngine ng(::getpid());
	toolbox::math::UniformIntDistribution<int> dist1(1, 6);

	toolbox::math::CanonicalRandomEngine<double> cng(ng);
	toolbox::math::UniformRealDistribution<double> dist2(1, 10);
	toolbox::math::NormalRealDistribution<double> dist3(5, 2);
	toolbox::math::LogNormalRealDistribution<double> dist4(3, 1);

	std::cout << "Uniform integer distribution in range [" << dist1.min() << ".." << dist1.max() << "]" << std::endl;

	std::map<int, int> hist1;
	for (int n = 0; n < 10000; ++n)
	{
		++hist1[dist1(ng)];
	}

	for (std::map<int, int>::iterator it = hist1.begin(); it != hist1.end(); ++it)
	{
		std::cout << it->first << ' ' << std::string(it->second / 200, '*') << std::endl;
	}

	//---------------------------------------------------------------------------------------------------------

	std::cout << "Uniform real distribution in range [" << dist2.min() << ".." << dist2.max() << ")" << std::endl;

	std::map<int, int> hist2;
	for (int n = 0; n < 10000; ++n)
	{
		++hist2[floor(dist2(cng))];
	}

	for (std::map<int, int>::iterator it = hist2.begin(); it != hist2.end(); ++it)
	{
		std::cout << std::fixed << it->first << ' ' << std::string(it->second / 200, '*') << std::endl;
	}

	//---------------------------------------------------------------------------------------------------------

	std::cout << "Normal distribution mean = " << dist3.mean() << ", stddev = " << dist3.stddev() << std::endl;

	std::map<int, int> hist3;
	for (int n = 0; n < 10000; ++n)
	{
		++hist3[floor(dist3(cng))];
	}

	for (std::map<int, int>::iterator it = hist3.begin(); it != hist3.end(); ++it)
	{
		std::cout << std::fixed << it->first << ' ' << std::string(it->second / 200, '*') << std::endl;
	}

	//---------------------------------------------------------------------------------------------------------

	std::cout << "Log normal distribution mean = " << dist4.mean() << ", stddev = " << dist4.stddev() << std::endl;

	std::map<int, int> hist4;
	for (int n = 0; n < 10000; ++n)
	{
		++hist4[floor(dist4(cng))];
	}

	for (std::map<int, int>::iterator it = hist4.begin(); it != hist4.end(); ++it)
	{
		std::cout << std::fixed << it->first << ' ' << std::string(it->second / 200, '*') << std::endl;
	}
}
*/
