// $Id: random.h,v 1.4 2008/07/18 15:27:34 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _toolbox_math_random_h_
#define _toolbox_math_random_h_

#include <math.h>
#include <stdint.h>

namespace toolbox {
namespace math {

//----------------------------------------------------------------------------//
// class RandomGenerator: base class for all random generators		      //
//----------------------------------------------------------------------------//

class RandomGenerator {
  protected:
    unsigned long seed;		// the seed of the random generator
    unsigned long max_val;		// uniform unsigned long values within [0...max_val]
    RandomGenerator *gen;	// pointer to generator    
  public:
    RandomGenerator (unsigned long new_seed = 0)
	{ seed = (new_seed & 0x7fffffff); }
    RandomGenerator (RandomGenerator* ran)
	{ gen = ran; max_val = ran->max_val; seed = ran->seed; }  
    virtual ~RandomGenerator() { }
    unsigned long Seed (void) const { return seed; }
    unsigned long Maximum (void) const { return max_val; }
    RandomGenerator* Generator (void) const { return gen; }
    virtual unsigned long Long (void) = 0;
};

class Ran002: public RandomGenerator {	// Press's combined MLCG "ran2"
  private:
    long idum, idum2, iy, iv[32];
  public:
    virtual ~Ran002();
    Ran002 (unsigned long seed = 0);
    virtual unsigned long Long (void);
};

//----------------------------------------------------------------------------//
// class NormalDistribution: normal (Gaussian) distributed random numbers     //
//----------------------------------------------------------------------------//

class NormalDistribution : public RandomGenerator 
{
  protected:
    double m, s, scale, cacheval;
    int cached;
  public:
    NormalDistribution (void) {}
    NormalDistribution (double mean, double stdev, 
			RandomGenerator *ran) : RandomGenerator(ran) 
        { cached = 0; m = mean; s = stdev; scale  = 2.0 / max_val; }
    virtual ~NormalDistribution ();
    double operator () (void);
    unsigned long Long (void) { return gen->Long(); };
    double Mean (void) const { return m; }
    double Stdev (void) const { return s; }
};

//----------------------------------------------------------------------------//
// class LogNormalDistribution: log-normal distributed random numbers         //
//----------------------------------------------------------------------------//

class LogNormalDistribution 
    : public NormalDistribution {
  protected:
    double m_log, s_log;
    void Initialize (double mean, double stdev);
  public:
    LogNormalDistribution (void) {}
    LogNormalDistribution (double mean, double stdev, 
			   RandomGenerator* ran) 
	: NormalDistribution(mean,stdev,ran) { Initialize(mean,stdev); }
    virtual ~LogNormalDistribution();
    double operator () (void) 
        { return exp(this->NormalDistribution::operator()()); }
    double Mean (void) const { return m_log; }
    double Stdev (void) const { return s_log; }
};

class LogNormalGen {
	
	public:
	
	LogNormalGen (unsigned long seed, double mean, double stdev, unsigned long min, unsigned long max ) {
		
		r002_ = new Ran002(seed);		
		lognorm_ = new LogNormalDistribution (mean, stdev, r002_);
		min_ = min;
		max_ = max;
	}
	
	LogNormalGen (unsigned long seed, double mean, double stdev) 
	{
		r002_ = new Ran002(seed);		
		lognorm_ = new LogNormalDistribution (mean, stdev, r002_);
		min_ = 0;
		max_ = 0xffffffff;
	}	
	
	~LogNormalGen()
	{
		delete lognorm_;
		delete r002_;
	}
	
	unsigned long getRandomSize() {
		while(1) {
			//unsigned long esize = ((unsigned long)(*lognorm_)() & 0xfffffffffffffffc ); // multiple of 4
			unsigned long esize = ((unsigned long)(*lognorm_)()); 
			if (esize >= max_) return max_;
			if (esize >= min_) return esize;
			return min_;
		}
	}

	unsigned long getRawRandomSize() 
	{
		  unsigned long esize = (unsigned long)(*lognorm_)(); 
		  return esize;
	}

	protected:
	
	Ran002 * r002_;		
	LogNormalDistribution * lognorm_;
	unsigned long min_;
	unsigned long max_;
	
};

}
}

#endif
