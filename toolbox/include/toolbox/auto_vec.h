// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini							 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _toolbox_auto_vec_h_
#define _toolbox_auto_vec_h_

#include <stddef.h>

namespace toolbox 
{
	namespace stl
	{
 
 	template <class T> class auto_vec 
	{

     		T* d_;

   		public:

     		typedef T element_type;
 	
     		explicit auto_vec(T*d = 0) throw(): d_(d) {}
 	
     		auto_vec(auto_vec &av) throw(): d_(av.release()) {}
 	
     		~auto_vec() throw() 
		{ 	delete[] d_; 
		}
 	
     		auto_vec &operator = (auto_vec &av) throw() 
		{
       			reset(av.release());
       			return *this;
     		}
 		
     		T* get() const throw() 
		{ 
			return d_; 
		}
 		
     		T &operator[](size_t i) throw() 
		{ 
			return d_[i]; 
		}
 		
     		T* release() throw() 
		{
       			T *r = d_;
       			d_ = 0;
       			return r;
     		}
 	
     		void reset(T*d=0) throw() 
		{
       			if (d != d_) 
			{
       		    		delete[] d_;
       		    		d_ = d;
       			}
     		}
       
 	};
 
 	}
}

#endif
