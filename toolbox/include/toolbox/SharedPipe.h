 // $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2013, CERN.			                 		 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, A. Petrucci, C.Wakefield					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         	 *
 *************************************************************************/

#ifndef _toolbox_shared_pipe_h_
#define _toolbox_shared_pipe_h_

#include <string>
#include <list>
#include "toolbox/sharedlist.h"
#include "xdaq/exception/Exception.h"

const size_t PipeItemSize = 1024;
const size_t PipeQueueSize = 16384;
 //sqrt of pipeitemsize

namespace toolbox
{
class SharedPipe
{
	public:
	SharedPipe(const std::string & name  );
	~SharedPipe(){}

	void write ( void * ptr, size_t size ) throw(xdaq::exception::Exception);
	void read ( void * ptr, size_t size , size_t & len) throw(xdaq::exception::Exception);
	void reset();

	typedef struct item_
	{
		char data[PipeItemSize];
	} ITEM;

	typedef struct info_
	{
		size_t size;
		size_t blocks;
	} INFO;

	sharedlist<ITEM, PipeQueueSize>* queue_;
	sharedlist<INFO, PipeQueueSize>* info_;

	size_t sqrtItemSize;
};
}



#endif
