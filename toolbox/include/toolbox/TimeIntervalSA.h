// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2016, CERN.			                             *
 * All rights reserved.                                                  *
 * Authors: L. Orsini and D. Simelevicius				                 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                             *
 * For the list of contributors see CREDITS.   			                 *
 *************************************************************************/

#ifndef _toolbox_time_TimeIntervalLA_h_
#define _toolbox_time_TimeIntervalLA_h_

#include <iostream>
#include <string>
#include "toolbox/TimeIntervalLA.h"
#include "toolbox/TimeInterval.h"


/*
Durations:
   expr              = duration
   duration          = "P" (dur-date | dur-time | dur-week)
   dur-second        = 1*DIGIT "S"
   dur-minute        = 1*DIGIT "M" [dur-second]
   dur-hour          = 1*DIGIT "H" [dur-minute]
   dur-time          = "T" (dur-hour | dur-minute | dur-second)
   dur-day           = 1*DIGIT "D"
   dur-week          = 1*DIGIT "W"
   dur-month         = 1*DIGIT "M" [dur-day]
   dur-year          = 1*DIGIT "Y" [dur-month]
   dur-date          = (dur-day | dur-month | dur-year) [dur-time]

   For example, "P3Y6M4DT12H30M5S"

 */
namespace toolbox {


class TimeIntervalSA
{
public:
	void syntax()  throw (toolbox::exception::Exception);
	TimeIntervalSA(const std::string & in);
	virtual ~TimeIntervalSA();
	size_t getSeconds();

private:
	void duration();
	void seconds();
	void minutes();
	void hours();
	void timeexpr();
	void days();
	void weeks();
	void months();
	void years();


private:
	toolbox::TimeIntervalLA lexicalAnalyzer_;
	toolbox::TimeIntervalLA::Token nextToken_;
	size_t seconds_; // secs
};

}
#endif
