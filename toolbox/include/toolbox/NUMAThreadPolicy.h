// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2013, CERN.			                 		 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, A. Petrucci, C.Wakefield					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         	 *
 *************************************************************************/

#ifndef _toolbox_NUMAThreadPolicy_h_
#define _toolbox_NUMAThreadPolicy_h_

#include <string>
#include <vector>

#include "toolbox/ThreadPolicy.h"

#include "toolbox/exception/Exception.h"

namespace toolbox
{
	class NUMAThreadPolicy: public ThreadPolicy
	{
		public:

		NUMAThreadPolicy();
		virtual ~NUMAThreadPolicy();
		std::string getPackage();
		void setDefaultPolicy () throw (toolbox::exception::Exception);
	};
}

#endif
