// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2016, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, D. Simelevicius                                   *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _toolbox_Backoff_h
#define _toolbox_Backoff_h

#include "toolbox/math/random2.h"

namespace toolbox
{
	class Backoff
	{
	public:
		enum BackoffType {Fibonacci, Exponential};

		Backoff(unsigned int seed, BackoffType backoffType);

		//Function waits according to a chosen backoff algorithm
		//n - the iteration number
		//slotTime - the biggest backoff time in milliseconds when a backoff algorithm returns 1
		void backoff(unsigned int n, unsigned int slotTime);
	private:
		//Integer power function
		unsigned int power(unsigned int base, unsigned int exp);

		//A function to compute the fibonacci number
		unsigned int fibonacci(unsigned int n);

		//Randomization object
		toolbox::math::DefaultRandomEngine random_;
		toolbox::math::UniformIntDistribution<unsigned int> randomDist_;

		//Backoff algorithm type
		BackoffType backoffType_;
	};
}
#endif
