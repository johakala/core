// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition
                      *
 * Copyright (C) 2000-2015, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini and D. Simelevicius 					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _toolbox_conversion_h_
#define _toolbox_conversion_h_

#include <string>
#include <set>
#include <list>
#include <sstream>
#include "toolbox/exception/Exception.h"

namespace toolbox
{
	
	long toLongConversion (const std::string& n) throw (toolbox::exception::Exception);
	
	unsigned long toUnsignedLongConversion (const std::string& n) throw (toolbox::exception::Exception);

	double toDoubleConversion (const std::string& n) throw (toolbox::exception::Exception);

} // end of namespace toolbox

#endif
