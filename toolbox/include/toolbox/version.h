// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2018, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber, L. Orsini and D. Simelevicius                   *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/


#ifndef _toolbox_version_h_
#define _toolbox_version_h_

#include "config/PackageInfo.h"
// !!! Edit this line to reflect the latest package version !!!

#define TOOLBOX_VERSION_MAJOR 9
#define TOOLBOX_VERSION_MINOR 9
#define TOOLBOX_VERSION_PATCH 1
// If any previous versions available E.g. #define TOOLBOX_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#define TOOLBOX_PREVIOUS_VERSIONS "9.6.0,9.7.0,9.8.0,9.0.0"

// List of required RPMs ( they are all XDAQ packages prefixed with daq- as a convention, RPM spec format cam be used)
#define TOOLBOX_REQUIRED_PACKAGE_LIST config,xcept,log4cplus,xdaq,asyncresolv

//
// Template macros
//
#define TOOLBOX_VERSION_CODE PACKAGE_VERSION_CODE(TOOLBOX_VERSION_MAJOR,TOOLBOX_VERSION_MINOR,TOOLBOX_VERSION_PATCH)
#ifndef TOOLBOX_PREVIOUS_VERSIONS
#define TOOLBOX_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(TOOLBOX_VERSION_MAJOR,TOOLBOX_VERSION_MINOR,TOOLBOX_VERSION_PATCH)
#else 
#define TOOLBOX_FULL_VERSION_LIST  TOOLBOX_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(TOOLBOX_VERSION_MAJOR,TOOLBOX_VERSION_MINOR,TOOLBOX_VERSION_PATCH)
#endif 

namespace toolbox 
{
	const std::string package  =  "toolbox";
	const std::string versions = TOOLBOX_FULL_VERSION_LIST;
	const std::string summary = "System and basic programming support classes";
	const std::string description = "";
	const std::string authors = "Johannes Gutleber, Luciano Orsini, Dainius Simelevicius";
	const std::string link = "http://xdaqwiki.cern.ch/index.php/Core_Tools";

	config::PackageInfo getPackageInfo();
	void checkPackageDependencies() throw (config::PackageInfo::VersionException);
	std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
