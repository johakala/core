// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2013, CERN.			                 		 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, A. Petrucci, C.Wakefield					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         	 *
 *************************************************************************/

#ifndef _toolbox_AllocPolicy_h_
#define _toolbox_AllocPolicy_h_


#include <string>

#include "toolbox/Policy.h"

#include "toolbox/exception/Exception.h"

namespace toolbox
{

	class AllocPolicy: public Policy
	{
		public:
		virtual ~AllocPolicy();
		
		void setDefaultPolicy();	
		std::string getType();
		virtual void* alloc (size_t size) throw (toolbox::exception::Exception) = 0;
		virtual void free (void* buffer, size_t size) throw (toolbox::exception::Exception) = 0;
	};
}

#endif

