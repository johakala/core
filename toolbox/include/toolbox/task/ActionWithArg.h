// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2016, CERN.			                             *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, D. Simelev 					                     *
 *                                                                       *
 * For the licensing terms see LICENSE.		                             *
 * For the list of contributors see CREDITS.   			                 *
 *************************************************************************/

#ifndef _toolbox_task_ActionWithArg_h_
#define _toolbox_task_ActionWithArg_h_


#include "toolbox/task/Action.h"

namespace toolbox
{
namespace task
{
class WorkLoop;
template <class LISTENER,typename T>
class ActionWithArg: public toolbox::task::ActionSignature
{
public:

	ActionWithArg(T arg): arg_(arg)
	{

	}
	virtual ~ActionWithArg()
	{
	}

	std::string type()
	{
		return "actionwitharg";
	}

	bool invoke (toolbox::task::WorkLoop * wl)
	{
		return (obj_->*func_)(wl, arg_);
	}

	std::string name()
	{
		return name_;
	}

	void name(const std::string & name)
	{
		name_ = name;
	}

	LISTENER * obj_;
	bool (LISTENER::*func_)(toolbox::task::WorkLoop *, T arg);
	std::string name_;
	T arg_;

};

template <class LISTENER,typename T>
toolbox::task::ActionWithArg<LISTENER,T> * bindwitharg(LISTENER * obj, bool (LISTENER::*func)(toolbox::task::WorkLoop *, T arg), const std::string & name, T arg)
{
	toolbox::task::ActionWithArg<LISTENER,T> * f = new toolbox::task::ActionWithArg<LISTENER,T>(arg);
	f->obj_ = obj;
	f->func_ = func;
	f->name_ = name;
	obj->addMethod(f, name);
	return f;
}
}}

#endif
