// $Id: sharedlist.h,v 1.10 2008/07/18 15:27:32 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _toolbox_sharedlist_h__
#define _toolbox_sharedlist_h__

#include <string.h>
#include <string>
#include <sstream>

#include "toolbox/exception/RingListFull.h"
#include "toolbox/exception/RingListEmpty.h"
#include "toolbox/exception/RingListNotEmpty.h"
#include "toolbox/exception/Exception.h"
#include "toolbox/mem/exception/FailedCreation.h"
#include "toolbox/string.h"
#include "toolbox/Policy.h"
#include "toolbox/AllocPolicy.h"
#include "toolbox/PolicyFactory.h"

#include "toolbox/hexdump.h"

namespace toolbox
{
template <class Type, int Size>
class sharedlist{
	
	public:
	
	sharedlist() throw (toolbox::mem::exception::FailedCreation)
	{
		size_ = Size;
		first_ = 0;
		last_ = 0;
	}

	~sharedlist()
	{
	}

	static sharedlist<Type, Size>* create(const std::string & name) throw (toolbox::mem::exception::FailedCreation)
	{
		toolbox::PolicyFactory * factory = toolbox::getPolicyFactory();
		toolbox::AllocPolicy * policy = 0;

		try
		{
			toolbox::net::URN urnObject(name + "-object", "alloc");
			policy = dynamic_cast<toolbox::AllocPolicy *>(factory->getPolicy(urnObject, "alloc"));
			void * ptr = policy->alloc(sizeof(sharedlist<Type, Size>));

			return new (ptr) sharedlist<Type, Size>();
		}
		catch (toolbox::exception::Exception & e)
		{
			XCEPT_RETHROW (toolbox::mem::exception::FailedCreation, "Failed to allocate shared sharedlist", e);
		}
	}
	
	//! gives the number of element contained in the sharedlist
	inline size_t elements() 
	{
		/*
		ssize_t first = first;
		ssize_t last = last_;
		ssize_t size = last-first;
		if (size < 0) size =  ( size_ - first ) + last;
		*/
		
		if ( first_ <= last_ )			
			return (last_ - first_);
		else
			return 	(( size_ - first_ ) + last_);
	}
	
	//! Reinitializes the sharedlist
	void clear()
	{
		while (!empty()) pop_front();
	}
	
	//! set the max number of element the sharedlist can contain
	
	inline bool empty() 
	{
		if ( first_ == last_ ) 
		{
			return(true);
		}
		return(false);		
	}
	
	inline void pop_front() throw (toolbox::exception::RingListEmpty) 
	{
		if ( first_ != last_ ) {
			first_ = (first_ +1 ) % size_;
			return;
		}
		else
		{
			std::string msg = toolbox::toString("empty sharedlist %s", name_.c_str());
			XCEPT_RAISE(toolbox::exception::RingListEmpty, "no element available in sharedlist");
		}
	}
	
	inline Type front() 
	{
		return(v_[first_]);
	}
	
	inline void push_back(Type i) throw (toolbox::exception::RingListFull) 
	{
		if ((( last_ + 1) % size_ ) != first_ ) 
		{
			v_[last_] = i;

			last_ = (last_ + 1) % size_;
			return;
		}
		else
		{			
			std::string msg = "sharedlist overflow: ";
			//msg += ", duplicates: ";
			//msg += this->checkDuplicates();
			XCEPT_RAISE(toolbox::exception::RingListFull, msg);
		}
	
	}
	
/*	std::string checkDuplicates()
	{
		std::stringstream duplicates;
		
		for (int i = 0; i < size_; i++)
		{
			Type r = v_[i];
			for (int j = (i+1); j < size_; j++)
			{
				if (r == v_[j])
				{					
					duplicates << "<" << i << "," << j << ">=<" << std::hex << (int) r << std::dec << "; ";
				}
			}
		}
		
		return duplicates.str();
	}
*/	
	//protected:
	
	inline void reset()
	{
		size_ = Size;
		first_ = 0;
		last_ = 0;
	}

	sharedlist(const sharedlist<Type, Size> & l)
	{
		XCEPT_RAISE (toolbox::exception::Exception, "sharedlist Copy CTOR not implemented");
	}
	
	size_t size_;
	size_t first_;
	size_t last_;
	Type v_[Size];
	std::string  name_;
};
}


#endif
