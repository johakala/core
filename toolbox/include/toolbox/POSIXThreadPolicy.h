// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2013, CERN.			                 		 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, A. Petrucci, C.Wakefield					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         	 *
 *************************************************************************/

#ifndef _toolbox_POSIXThreadPolicy_h_
#define _toolbox_POSIXThreadPolicy_h_

#include <string>

#include "toolbox/ThreadPolicy.h"

#include "toolbox/exception/Exception.h"

namespace toolbox
{

	class POSIXThreadPolicy: public ThreadPolicy
	{
		public:

		POSIXThreadPolicy();
		virtual ~POSIXThreadPolicy();
		std::string getPackage();
		void setDefaultPolicy () throw (toolbox::exception::Exception);
	};
}

#endif
