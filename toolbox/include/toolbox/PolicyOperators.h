// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini and D. Simelevicius				 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef __PolicyOperators_h__
#define __PolicyOperators_h__

#include "toolbox/AllocPolicy.h"

void* operator new  ( std::size_t count, toolbox::AllocPolicy * policy);
void operator delete(void* ptr, toolbox::AllocPolicy * policy) throw();

void* operator new[]   ( size_t count, toolbox::AllocPolicy * policy );
void  operator delete[]( void* ptr , toolbox::AllocPolicy * policy) throw(); 


#endif
