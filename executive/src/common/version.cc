// $Id: version.cc,v 1.3 2008/07/18 15:26:47 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "executive/version.h"
#include "config/version.h"
#include "xcept/version.h"
#include "xdata/version.h"
#include "xdaq/version.h"
#include "xgi/version.h"
#include "pt/version.h"
#include "i2o/utils/version.h"

GETPACKAGEINFO(executive)

void executive::checkPackageDependencies() throw (config::PackageInfo::VersionException)
{
	CHECKDEPENDENCY(config);
	CHECKDEPENDENCY(xcept);
	CHECKDEPENDENCY(xdaq);   
	CHECKDEPENDENCY(xgi);  
	CHECKDEPENDENCY(pt);     
	CHECKDEPENDENCY(i2outils);     
}

std::set<std::string, std::less<std::string> > executive::getPackageDependencies()
{
	std::set<std::string, std::less<std::string> > dependencies;

	ADDDEPENDENCY(dependencies,config); 
	ADDDEPENDENCY(dependencies,xcept); 
	ADDDEPENDENCY(dependencies,xdaq); 
	ADDDEPENDENCY(dependencies,xgi);
	ADDDEPENDENCY(dependencies,pt);
	ADDDEPENDENCY(dependencies,i2outils);
	  
	return dependencies;
}	
	
