// $Id: XgiDispatcher.cc,v 1.2 2008/07/18 15:26:47 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "executive/XgiDispatcher.h"
#include "toolbox/string.h"
#include "xdaq/exception/ApplicationNotFound.h"
#include "xgi/Method.h"
#include "xcept/tools.h"
#include "executive/Application.h"
#include "xdata/UnsignedInteger.h"
	
executive::XgiDispatcher::XgiDispatcher(xdaq::ApplicationContext* context, log4cplus::Logger & logger): 
	logger_( log4cplus::Logger::getInstance( logger.getName() + ".xdaq.XgiDispatcher"))
{
	registry_ = context->getApplicationRegistry();
	context_ = context;
}

executive::XgiDispatcher::~XgiDispatcher()
{
}

void executive::XgiDispatcher::processIncomingMessage (xgi::Input * in, xgi::Output * out) throw (pt::exception::Exception)
{
	// find tid from URL path, see examples below
	//
	// http://<host>:<port>/urn:xdaq-application:id=<number>
	// http://<host>:<port>/urn:xdaq-application:class=<name>,instance=<number>/<additional path>
	//
	// Example: PATH_TRANSLATED = http://lxcmd101:40000/urn:xdaq-application:class=Executive,instance=0/Configure?file=filename.xml
	// The urn is extracted into SCRIPT_NAME
	//
	std::string urn = in->getenv("SCRIPT_NAME");
	
	//std::cout << "URN: " << urn << std::endl;	
	std::string commandName = in->getenv("PATH_INFO");
	//std::cout << "Path-info:" << commandName << std::endl;
	//std::cout << "Script-name: " << in->getenv("SCRIPT_NAME") << std::endl;
	//std::cout << "Query-string: " << in->getenv("QUERY_STRING") << std::endl;
	
	xdata::UnsignedInteger id;
	xdaq::Application* application = 0; // application to call back
		
	std::string::size_type pos = urn.find("lid=");
	if (pos != std::string::npos)
	{
		// extract id: +3 needs to change if it is not anymore "id="
		std::string idNumber = urn.substr(pos+4,  urn.size()-(pos+4) );

		//std::cout << "Pos=" << pos << ", size: " << urn.size() << ", urn: " << urn << std::endl;
		//std::cout << "idNumber: [" << idNumber << "]" << std::endl;

		if (idNumber == "")
		{
			std::string msg = "Missing local id number in URN ";
			msg += urn;
			LOG4CPLUS_ERROR (logger_, msg);
			XCEPT_RAISE (pt::exception::Exception, msg);
		}

		try
		{
			id.fromString(idNumber);
			application = this->registry_->getApplication(id);
		}
		catch (xdaq::exception::ApplicationNotFound& anf)
		{
			XCEPT_RETHROW(pt::exception::Exception, "Could not find application specified in URL for CGI request", anf);
		}
		catch (xdata::exception::Exception& e)
		{
			XCEPT_RETHROW (pt::exception::Exception,"Failed to parse local id", e);
		}
	}
	else if ( (pos = urn.find("id=")) != std::string::npos )
	{
		// extract id: +3 needs to change if it is not anymore "id="
		std::string idNumber = urn.substr(pos+3,  urn.size()-(pos+3) );

		//std::cout << "Pos=" << pos << ", size: " << urn.size() << ", urn: " << urn << std::endl;
		//std::cout << "idNumber: [" << idNumber << "]" << std::endl;

		if (idNumber == "")
		{
			std::string msg = "Missing local id number in URN ";
			msg += urn;
			LOG4CPLUS_ERROR (logger_, msg);
			XCEPT_RAISE (pt::exception::Exception, msg);
		}

		try
		{
			id.fromString(idNumber);
			application = this->registry_->getApplication(id);
		}
		catch (xdaq::exception::ApplicationNotFound& anf)
		{
			XCEPT_RETHROW(pt::exception::Exception, "Could not find application specified in URL for CGI request", anf);
		}
		catch (xdata::exception::Exception& e)
		{
			XCEPT_RETHROW (pt::exception::Exception,"Failed to parse local id", e);
		}
			
	}
	else if ( (pos = urn.find("class=")) != std::string::npos )
	{
		// find instance number
		std::string::size_type instancePos = urn.find(",instance=");
		if (instancePos != std::string::npos)
		{
			// Extract class name
			std::string className = urn.substr(pos+6, instancePos-(pos+6));
			// std::cout << "ClassName: " << className << std::endl;

			// instance number must be terminated either by "/" or end of URN
			std::string::size_type endOfInstance = urn.find("/",instancePos+10);
			if (endOfInstance == std::string::npos)
			{
				endOfInstance = urn.size();
			}
			std::string instanceNumber = urn.substr(instancePos+10, endOfInstance -(instancePos+10) );
			
			if (className == "")
			{
				std::string msg = "Missing class name in urn ";
				msg += urn;
				LOG4CPLUS_ERROR (logger_, msg);
				XCEPT_RAISE (pt::exception::Exception, msg);
			}

			if (instanceNumber == "")
			{
				std::string msg = "Missing instance number in urn ";
				msg += urn;
				LOG4CPLUS_ERROR (logger_, msg);
				XCEPT_RAISE (pt::exception::Exception, msg);
			}

			try
			{
				xdata::UnsignedInteger instance(instanceNumber);
				application = registry_->getApplication( className, static_cast<xdata::UnsignedIntegerT>(instance) );
			}
			catch (xdaq::exception::ApplicationNotFound& anf)
			{
				XCEPT_RETHROW(pt::exception::Exception, "Application not found by lid", anf);
			}
			catch (xdata::exception::Exception& e)
			{
				XCEPT_RETHROW (pt::exception::Exception,"Failed to parse instance number", e);
			}
			catch (xdaq::exception::ApplicationDescriptorNotFound& adnf)
			{
				std::string msg = "Could not dispatch incoming CGI message to class ";
				msg += className;
				msg += " instance number ";
				msg += instanceNumber;
				XCEPT_RETHROW (pt::exception::Exception, msg, adnf);
			}
		}
		else
		{
			std::string msg = "Missing instance number in urn ";
			msg += urn;
			LOG4CPLUS_ERROR (logger_, msg);
			XCEPT_RAISE(pt::exception::Exception, msg);
		}
	}
	else if ( (pos = urn.find("service=")) != std::string::npos )
	{
		// service name must be terminated either by "/" or end of URN
                std::string::size_type endOfService = urn.find("/",pos+8);
                if (endOfService == std::string::npos)
                {
                        endOfService = urn.size();
                }

		std::string service = urn.substr(pos+8,  endOfService-(pos+8) );
		std::vector<xdaq::Application*> apps = this->registry_->getApplications("service", service);
		if (apps.size() != 1)
		{
			std::string msg = toolbox::toString("Cannot retrieve %d applications from ambiguous urn %s", apps.size(), urn.c_str());
			LOG4CPLUS_ERROR (logger_, msg);
			XCEPT_RAISE(pt::exception::Exception, msg);
		}
		else
		{
			application = apps[0];
		}
	}
	else
	{	
		std::string msg = "Missing id or class specification in urn ";
		msg += urn;
		LOG4CPLUS_ERROR (logger_, msg);
		XCEPT_RAISE(pt::exception::Exception, msg);
	}
	
	// Put try/catch around and free frame in catch!
	xgi::MethodSignature* method = 0;

	if (commandName == "") commandName = "Default";
	method = static_cast<xgi::MethodSignature*>(application->getMethod(commandName));

	if (method == 0)
	{
		std::stringstream msg;
		msg << "No callback method found for incoming request [" << commandName << "] in " << application->getApplicationDescriptor()->getClassName();
		LOG4CPLUS_WARN (logger_, msg.str());

		method = static_cast<xgi::MethodSignature*>(application->getMethod("noCallbackFound"));
		if (method == 0)
		{
			LOG4CPLUS_WARN (logger_, "No framework callback method found");
			XCEPT_RAISE(pt::exception::Exception, msg.str());
		}
	}

	try
	{
		method->invoke(in,out);
		return;
	}
	catch (xgi::exception::Exception& xgie)
	{
		std::string msg = "XGI exception occured while calling CGI method ";
		msg += commandName;
		LOG4CPLUS_ERROR (logger_, xcept::stdformat_exception_history(xgie));
		XCEPT_RETHROW(pt::exception::Exception, msg, xgie);
	}
	catch (...)
	{
		std::string msg = "Caught unknown exception raised in CGI method ";
		msg += commandName;
		msg += " on application with urn ";
		msg += urn;
		LOG4CPLUS_ERROR (logger_, msg);
		XCEPT_RAISE (xdaq::exception::Exception, msg);
	}
}
	
