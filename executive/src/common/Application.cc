// $Id: Application.cc,v 1.12 2009/05/11 16:26:36 rmoser Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/
 
#include "executive/Application.h"

#include <iostream>
#include <sstream>
#include <memory>

//
// XOAP includes
//
#include "xoap/domutils.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/exception/Exception.h"
#include "xoap/MessageFactory.h"
#include "xoap/MessageReference.h"
#include "xoap/DOMParser.h"
#include "xoap/HTTPLoader.h"
#include "xoap/Method.h"

#include "pt/PeerTransportReceiver.h"
#include "pt/PeerTransportAgent.h"
#include "pt/exception/InvalidAddress.h"

#include "xercesc/util/XMLURL.hpp"

#include "toolbox/Event.h"
#include "toolbox/net/URL.h"
#include "toolbox/exception/Handler.h"
#include "toolbox/exception/Processor.h"
#include "toolbox/HostInfoImpl.h"

#include "xdaq/InstantiateApplicationEvent.h"
#include "xdaq/EndpointAvailableEvent.h"
#include "xdaq/ApplicationContextImpl.h"
#include "xdaq/ApplicationGroup.h"
#include "xdaq/ApplicationDescriptor.h"
#include "xdaq/ApplicationStub.h"
#include "xdaq/ApplicationRegistry.h"
#include "xdaq/ContextDescriptor.h"
#include "xdaq/Application.h"
#include "xdaq/NamespaceURI.h"
#include "xdaq/exception/Exception.h"
#include "xdaq/exception/DuplicateRoute.h"
#include "xdaq/exception/NoRoute.h"
#include "xdaq/exception/BadCommand.h"
#include "xdaq/exception/ConfigurationError.h"
#include "xdaq/exception/AddressMismatch.h"
#include "xdaq/exception/ApplicationDescriptorNotFound.h"
#include "xdaq/exception/ClassNotFound.h"
#include "xdaq/exception/DuplicateEndpoint.h"
#include "xdaq/exception/DuplicateTid.h"
#include "xdaq/exception/LoadFailed.h"
#include "xdaq/exception/NoEndpoint.h"
#include "xdaq/exception/NoNetwork.h"
#include "xdaq/exception/ParameterSetFailed.h"
#include "xdaq/exception/SymbolLookupFailed.h"
#include "xdaq/Zone.h"
#include "xdaq/ApplicationDescriptorFactory.h"

#include "xdata/ItemGroupEvent.h"
#include "xdata/TimeVal.h"
#include "xdata/InfoSpaceFactory.h"


// dispatchers for I2O and SOAP
//
#include "executive/SOAPDispatcher.h"
#include "executive/XgiDispatcher.h"

//#ifdef B2IN
#include "b2in/nub/Dispatcher.h"
//#endif

#include "i2o/utils/Dispatcher.h"
#include "i2o/utils/AddressMap.h"

#include "xcept/tools.h"

//
// Log4CPLUS
//
#include "log4cplus/logger.h"
#include "log4cplus/consoleappender.h"
#include "log4cplus/socketappender.h"
#include "log4cplus/nullappender.h"
#include "log4cplus/fileappender.h"
#include "log4cplus/helpers/appenderattachableimpl.h"
#include "log4cplus/helpers/loglog.h"
#include "log4cplus/helpers/pointer.h"
#include "log4cplus/spi/loggingevent.h"
#include "log4cplus/layout.h"

// 
// xdata includes
//
#include "xdata/XMLDOM.h"
#include "xdata/soap/Serializer.h"

using namespace log4cplus;
using namespace log4cplus::helpers;
using namespace log4cplus::spi;

XDAQ_INSTANTIATOR_IMPL(executive::Application)

executive::Application::Application 
(
	xdaq::ApplicationStub * stub
)
	throw (xdaq::exception::Exception)
	: xdaq::Application(stub), ConfigurationNamespaceURI(XDAQ_CONFIGURATION_NS_URI), uimanager_(this)
{	
	stub->getDescriptor()->setAttribute("icon","/executive/images/executive-icon.png");
	stub->getDescriptor()->setAttribute("service","executive");

	// Export PID in attribytes
	std::stringstream pidstream;
	pidstream << toolbox::getRuntime()->getPid();
	stub->getDescriptor()->setAttribute("pid", pidstream.str());
	
	// install a default error handler
	toolbox::exception::HandlerSignature* defaultExceptionHandler = toolbox::exception::bind (this, &executive::Application::handleException, "executive::Application::handleException");
	toolbox::exception::getProcessor()->setDefaultHandler(defaultExceptionHandler);

	// register dispatchers for SOAP and I2O
	pt::getPeerTransportAgent()->addListener( new executive::SOAPDispatcher( this->getApplicationContext(), this->getApplicationLogger() ) );
	pt::getPeerTransportAgent()->addListener( new i2o::utils::Dispatcher( this->getApplicationContext(), this->getApplicationLogger() ) );
	pt::getPeerTransportAgent()->addListener( new executive::XgiDispatcher( this->getApplicationContext(), this->getApplicationLogger() ) );

// #ifdef B2IN
	pt::getPeerTransportAgent()->addListener( new b2in::nub::Dispatcher( this->getApplicationContext(), this->getApplicationLogger() ) );
// #endif

	tempDir_ = "/tmp";
	
	this->getApplicationInfoSpace()->fireItemAvailable ("tempDir", &tempDir_);
	
	checkPackageDependencies_ = true;
	
	this->getApplicationInfoSpace()->fireItemAvailable ("checkPackageDependencies", &checkPackageDependencies_);
	
	// Move the creation of these two variables to context. Share between context and executive
	//
	this->logUrl_ = "console";	
	this->lastLogUrl_ = logUrl_;
	this->getApplicationInfoSpace()->fireItemAvailable ("logUrl", &logUrl_);	
	this->logLevel_ = dynamic_cast<xdaq::ApplicationContextImpl*>(this->getApplicationContext())->getLogLevel();
	this->lastLogLevel_ = logLevel_;
	this->getApplicationInfoSpace()->fireItemAvailable ("logLevel", &logLevel_);
	
	// Create a process object for monitoring the local resource usage
	this->processInfo_ = toolbox::getRuntime()->getProcessInfo(toolbox::getRuntime()->getPid());

	// Create a host object for monitoring the host-specific resource usage
	toolbox::HostInfoImpl *p = new toolbox::HostInfoImpl();
	this->hostInfo_ = toolbox::HostInfo::Reference(p);
	
	// Perform an initial sample of the process information so that subsequent requests yield non-zero averages
	this->processInfo_->sample();
	this->hostInfo_->sample();
	
	// Add infospace listeners for reacting to modified parameters
	//
	getApplicationInfoSpace()->addItemChangedListener ("logUrl", this);
	getApplicationInfoSpace()->addItemChangedListener ("logLevel", this);	

	// SOAP callback bindings
	
	// Receive and apply an XML configuration for this process
	xoap::bind(this, &executive::Application::Configure, "Configure", XDAQ_NS_URI );
	
	// Send back a list of all loaded application modules and their versions
	xoap::bind(this, &executive::Application::ListModules, "ListModules", XDAQ_NS_URI );
		
	// Monitoring data
	//xdata::InfoSpace * is;
	m_context_ = getApplicationDescriptor()->getContextDescriptor()->getURL();
	//is = xdata::InfoSpace::get("urn:xdaq-monitorable:executive");
	
	toolbox::net::URN monitorable = this->createQualifiedInfoSpace("executive-sysinfo");
	xdata::InfoSpace * is = xdata::getInfoSpaceFactory()->get(monitorable.toString());
	
	is->fireItemAvailable("pid", &m_pid_);
	is->fireItemAvailable("sampleDuration", &m_sampleDuration_);
	is->fireItemAvailable("cpuUsage", &m_cpuUsage_);
	is->fireItemAvailable("totalCpuUsage", &m_totalCpuUsage_);
	is->fireItemAvailable("virtualSize", &m_virtualSize_);
	is->fireItemAvailable("residentSize", &m_residentSize_);
	is->fireItemAvailable("context", &m_context_);
	
	
	m_diskUsage_.addColumn ("fileSystem", "string");
	m_diskUsage_.addColumn ("totalMB", "double");
	m_diskUsage_.addColumn ("usePercent", "double");
	m_diskUsage_.addColumn ("sampleTime", "time");
	m_diskUsage_.addColumn ("state", "string");
	
	is->fireItemAvailable("diskUsage", &m_diskUsage_);

	is->addGroupRetrieveListener (this);

}


	
executive::Application::~Application ()
{
}



bool executive::Application::handleException(xcept::Exception& ex, void * context)
{
	std::string msg = "Unhandled exception occured\n";
	msg += xcept::stdformat_exception_history(ex);
	LOG4CPLUS_ERROR (this->getApplicationLogger(), msg);
	return true;
}


void executive::Application::Configure (DOMNode* partitionNode) throw (xdaq::exception::Exception)
{
	// Reset the initially set context id to the one given in the configuration file
	//
	//this->getApplicationContext()->getHostTable()->removeHost(this->getApplicationContext()->getContextId());
	//dynamic_cast<xdaq::ApplicationContextImpl*>(this->getApplicationContext())->setContextId(hostId);
		
	try
	{
		// set Policies before creating workloops and memory allocators
		this->setContextPolicies (partitionNode);

		// create application configuration 
		this->addApplicationConfigs(partitionNode);

		// create protocol specific map
		this->configureProtocols(partitionNode);

		// download all specified libraries
		this->downloadModules(partitionNode);

		// instantiate applications
		this->instantiateApplications(partitionNode);

		// configuring logical networks and tid routing
		this->configureNetworks(partitionNode);

		// Configure local peer transport receivers with addresses from configuration
		this->configureTransportReceivers();

		// configuring network aliases
		this->configureAliasNetworks(partitionNode);

		// configuring default routing
		this->configureDefaultRouting(partitionNode);

		// overriding of default routing
		this->overwriteDefaultRouting(partitionNode);  
	} 
	catch (xdaq::exception::BadCommand& bc)
	{
		LOG4CPLUS_ERROR (this->getApplicationLogger(), xcept::stdformat_exception_history(bc));
		XCEPT_RETHROW (xdaq::exception::Exception, "Received malformed '<Configure>' command", bc);
	}
	catch (xdaq::exception::ConfigurationError& ce)
	{
		LOG4CPLUS_ERROR (this->getApplicationLogger(), xcept::stdformat_exception_history(ce));
		XCEPT_RETHROW (xdaq::exception::Exception, "Failed to configure executive", ce);
	}

	xdaq::Event e("urn:xdaq-event:configuration-loaded", this);
	dynamic_cast<xdaq::ApplicationContextImpl*>(this->getApplicationContext())->fireEvent(e);
}

xoap::MessageReference executive::Application::Configure (xoap::MessageReference message) 
	throw (xoap::exception::Exception)
{
  	xoap::MessageReference reply = xoap::createMessage();
	xoap::SOAPEnvelope envelope = reply->getSOAPPart().getEnvelope();
	xoap::SOAPBody b = envelope.getBody();
	xoap::SOAPName responseName = envelope.createName("ConfigureResponse", "xdaq", XDAQ_NS_URI);
	b.addBodyElement ( responseName );
	
	//extract <Configure/> tag from SOAPMessage
	DOMNode* node = message->getSOAPPart().getEnvelope().getBody().getDOMNode();

	DOMNodeList* bodyList = node->getOwnerDocument()->getElementsByTagNameNS( xoap::XStr("*"), xoap::XStr("Configure") );
	
	if ( bodyList->getLength() > 1 ) 
	{
		std::string msg = "Received invalid SOAP command. Found more than one 'Configure' tag is command.";
		LOG4CPLUS_ERROR (this->getApplicationLogger(), msg.c_str());
		XCEPT_RAISE (xoap::exception::Exception, msg);
	}
	
	DOMNode * configurationNode = bodyList->item(0);
	
	

	//extract <Partition/> tag from command
	DOMNodeList* partitionList = configurationNode->getOwnerDocument()->getElementsByTagNameNS( xoap::XStr("*"), xoap::XStr("Partition"));
	if ( partitionList->getLength() == 0 ) 
	{
		std::string msg = "Received invalid SOAP command. Missing 'Partition' tag in 'Configure' tag";
		LOG4CPLUS_ERROR (this->getApplicationLogger(), msg.c_str());
		XCEPT_RAISE (xoap::exception::Exception, msg);
	} 
	else 
	{
		DOMNode* partitionNode = partitionList->item(0);
	
		std::string nsURI = xoap::XMLCh2String(partitionNode->getNamespaceURI());
		if ( nsURI != "http://xdaq.web.cern.ch/xdaq/xsd/2004/XMLConfiguration-30" )
		{
			std::string msg = "Invalid namespace URI: '" + nsURI + "' for configuration, expected 'http://xdaq.web.cern.ch/xdaq/xsd/2004/XMLConfiguration-30'";
			LOG4CPLUS_ERROR (this->getApplicationLogger(), msg.c_str());
			XCEPT_RAISE(xoap::exception::Exception, msg);
		}
		// Configure the local executive		
		try
		{
			this->Configure(partitionNode);
		}
		catch (xdaq::exception::Exception& ce)
		{
			LOG4CPLUS_ERROR (this->getApplicationLogger(), "Failed to configure executive");
			XCEPT_RETHROW (xoap::exception::Exception, "Failed to configure executive", ce);
		}
	}	
	return reply;	
}


// Send back a list of all loaded modules and their versions
//
xoap::MessageReference executive::Application::ListModules (xoap::MessageReference message) 
	throw (xoap::exception::Exception)
{
  	xoap::MessageReference reply = xoap::createMessage();
	xoap::SOAPEnvelope envelope = reply->getSOAPPart().getEnvelope();
	xoap::SOAPBody b = envelope.getBody();
	xoap::SOAPName responseName = envelope.createName("ListModulesResponse", "xdaq", XDAQ_NS_URI);
	xoap::SOAPElement responseElement = b.addBodyElement ( responseName );
	
	//extract <Configure/> tag from SOAPMessage
	DOMNode* node = message->getSOAPPart().getEnvelope().getBody().getDOMNode();
	DOMNodeList* bodyList = node->getOwnerDocument()->getElementsByTagNameNS( xoap::XStr("*"), xoap::XStr("ListModules") );
	
	if ( bodyList->getLength() > 1 ) 
	{
		std::string msg = "Received invalid SOAP command. Found more than one 'ListModules' tag in command.";
		LOG4CPLUS_ERROR (this->getApplicationLogger(), msg.c_str());
		XCEPT_RAISE (xoap::exception::Exception, msg);
	}
	
	// Fill the reply in format
	// <module name="" version=""/>
	const xdaq::SharedObjectRegistry* sor = getApplicationContext()->getSharedObjectRegistry();
	std::vector<std::string> ov = sor->getObjectNames();
	for (std::vector<std::string>::size_type i = 0; i < ov.size(); i++)
	{
		try
		{
			config::PackageInfo pi = sor->getPackageInfo (ov[i]);
		
			xoap::SOAPName moduleTag = envelope.createName("module","xdaq", XDAQ_NS_URI);
			xoap::SOAPElement moduleElement = responseElement.addChildElement(moduleTag);
			xoap::SOAPName moduleName = envelope.createName("name");
			moduleElement.addAttribute(moduleName, pi.getName());
			xoap::SOAPName versionName = envelope.createName("version");
			moduleElement.addAttribute(versionName, pi.getLatestVersion());
		}
		catch ( xdaq::exception::SymbolLookupFailed &e )
		{
			// no package info available - ignore
		}
		catch ( config::PackageInfo::VersionException &e )
		{
			XCEPT_RAISE (xoap::exception::Exception, e.what());
		}
	}	
		
	return reply;	
}



// Take Unicast and Multicast tags and overrode the default routes
//
void executive::Application::overwriteDefaultRouting (DOMNode* partitionNode) throw (xdaq::exception::BadCommand, xdaq::exception::ConfigurationError)
{
	xdaq::RoutingTable *  routingTable = const_cast<xdaq::RoutingTable*> (this->getApplicationContext()->getRoutingTable());
	const xdaq::NetGroup *  netGroup = this->getApplicationContext()->getNetGroup();

	const xdaq::Zone* zone = this->getApplicationContext()->getDefaultZone();
		
	const xdaq::ContextDescriptor* localContext = this->getApplicationContext()->getContextDescriptor();
	const xdaq::ContextTable* contextTable = this->getApplicationContext()->getContextTable();
	
	// loop over all <Application> in local host
	DOMNodeList * hostList = partitionNode->getChildNodes();
	for ( XMLSize_t i = 0; i < hostList->getLength(); i++ )
	{
		DOMNode * hostNode = hostList->item(i);
				
		if ( ( hostNode->getNodeType() == DOMNode::ELEMENT_NODE ) && ( xoap::XMLCh2String(hostNode->getLocalName()) == "Context" ) )
		{
		
			std::string nsURI = xoap::XMLCh2String(hostNode->getNamespaceURI());
			XCEPT_ASSERT (nsURI == executive::Application::ConfigurationNamespaceURI, xdaq::exception::BadCommand, toolbox::toString("Invalid or missing namespace in SOAP configuration command. Expected %s, received %s", nsURI.c_str(), executive::Application::ConfigurationNamespaceURI.c_str()));
		
			std::string contextURL = xoap::getNodeAttribute(hostNode, "url" );
			XCEPT_ASSERT (contextURL != "", xdaq::exception::BadCommand, "Missing 'url' attribute in 'Context' tag");
			bool matches = false;
			try 
			{
			 	matches = localContext->matchURL(contextURL);
			}
			catch (xdaq::exception::InvalidURL & e)
			{
				XCEPT_RETHROW (xdaq::exception::ConfigurationError, "Cannot match context descriptors", e);
			}
			if (matches )
			{			
				DOMNodeList * applicationList = hostNode->getChildNodes();
				for ( XMLSize_t j = 0 ; j < applicationList->getLength(); j++ )
				{
					DOMNode * applicationNode = applicationList->item(j);
					
										
					if  (	( applicationNode->getNodeType() == DOMNode::ELEMENT_NODE ) && 
						(xoap::XMLCh2String(applicationNode->getLocalName()) == "Application" )
						)
					{
						std::string nsURI = xoap::XMLCh2String(applicationNode->getNamespaceURI());		
						XCEPT_ASSERT (nsURI == executive::Application::ConfigurationNamespaceURI, xdaq::exception::BadCommand, toolbox::toString("Invalid or missing namespace in SOAP configuration command. Expected %s, received %s", nsURI.c_str(), executive::Application::ConfigurationNamespaceURI.c_str()));		

						// if the local id is not specified in the XML file, a resolver may be consulted
						// with the information collected so far: context URL, classname
						#warning "At this place, a resolver may be called"

						std::string localIdStr = xoap::getNodeAttribute (applicationNode, "id");
						std::string network = xoap::getNodeAttribute (applicationNode, "network");
						XCEPT_ASSERT (network != "", xdaq::exception::BadCommand, "Missing 'network' attribute in 'Application' tag");	
						
						const xdaq::ApplicationDescriptor* from = 0;
						try
						{
							xdata::UnsignedInteger localId(localIdStr);
							from = zone->getApplicationDescriptor(localContext, static_cast<xdata::UnsignedIntegerT>(localId));
						}
						catch (xdata::exception::Exception& xde)
						{
							std::string msg = "Failed to parse local identifier '";
							msg += localIdStr;
							msg += "'";
							XCEPT_RETHROW (xdaq::exception::ConfigurationError, msg, xde);
						}
						
						DOMNodeList* overrideStatementList = applicationNode->getChildNodes();
						for (XMLSize_t k = 0; k < overrideStatementList->getLength(); k++)
						{
							DOMNode* overrideStatement = overrideStatementList->item(k);
							
							// ONLY cover now <Unicast>
							// <Multicast> implementation later
							//					
							if  (( overrideStatement->getNodeType() == DOMNode::ELEMENT_NODE ) && 
								( xoap::XMLCh2String(overrideStatement->getLocalName()) == "Unicast" ))
							{
								std::string nsURI = xoap::XMLCh2String(overrideStatement->getNamespaceURI());		
								XCEPT_ASSERT (nsURI == executive::Application::ConfigurationNamespaceURI, xdaq::exception::BadCommand, toolbox::toString("Invalid or missing namespace in SOAP configuration command. Expected %s, received %s", nsURI.c_str(), executive::Application::ConfigurationNamespaceURI.c_str()));		
								
								std::string network   = xoap::getNodeAttribute (overrideStatement, "network");
								std::string className = xoap::getNodeAttribute (overrideStatement, "class");
								
								if (className == "") 
								// not class name given. Therefore look for context url + lid
								{	
									std::string overrideURL = xoap::getNodeAttribute(overrideStatement, "url");								
									std::string overrideLocalIdString = xoap::getNodeAttribute (overrideStatement, "id");
									if (overrideURL == "")
									{
										XCEPT_RAISE (xdaq::exception::BadCommand, "Missing url in 'Unicast' tag");
									}
									if (overrideLocalIdString == "")
									{
										XCEPT_RAISE (xdaq::exception::BadCommand, "Missing id in 'Unicast' tag");
									}									
									
									xdata::UnsignedInteger overrideLocalId;
									
									try
									{
										overrideLocalId.fromString(overrideLocalIdString);
									}
									catch (xdata::exception::Exception& xde)
									{
										std::string msg = "Failed to parse local identifier '";
										msg += overrideLocalIdString;
										msg += "'";
										XCEPT_RETHROW (xdaq::exception::ConfigurationError, msg, xde);
									}
									
									//-------------------------------
									// check network available betwee local and remote host
									
									
									const xdaq::ContextDescriptor* remoteContext = contextTable->getContextDescriptor(overrideURL);
									const xdaq::ApplicationDescriptor* to =
										zone->getApplicationDescriptor(
											remoteContext, static_cast<xdata::UnsignedIntegerT>(overrideLocalId));
									
									if ( netGroup->isNetworkExisting(network) ) // check that network is locally available
									{
										if (( netGroup->getNetwork(network)->isEndpointExisting(localContext)  ) &&
										    ( netGroup->getNetwork(network)->isEndpointExisting(remoteContext)  ) )
										{											
											routingTable->setNetworkPath(from, to, network);
										}
										else
										{
											std::stringstream s;
											s << "Cannot create unicast route from application "
												<< from->getClassName() << " in context " << localContext->getURL()
												<< " to application " << to->getClassName() << " in context " << remoteContext->getURL()
												<< " over network " << network;
												
											XCEPT_RAISE (xdaq::exception::ConfigurationError,s.str());
										}
										
									}
									//---------------																	
								} 
								else 
								{
									// set by class name and instance
									std::string instanceString = xoap::getNodeAttribute (overrideStatement, "instance");
																		
									if (instanceString == "")
									{
										// override all instances
										try {
											std::set<const xdaq::ApplicationDescriptor*> applications = zone->getApplicationDescriptors (className);
											for (std::set<const xdaq::ApplicationDescriptor*>::iterator a = applications.begin(); a != applications.end(); a++)
											{
												// may throw!!!
												routingTable->setNetworkPath( from, *a, network);
											}
										} 
										catch (xdaq::exception::ApplicationDescriptorNotFound& acnf)
										{
											XCEPT_RETHROW (xdaq::exception::ConfigurationError, "Failed to set network path", acnf );
										}
									} 
									else
									{
										const xdaq::ApplicationDescriptor* to = 0;
										try
										{
											xdata::UnsignedInteger instance(instanceString);
											to = zone->getApplicationDescriptor 
												(className, static_cast<xdata::UnsignedIntegerT>(instance));
										} 
										catch(xdata::exception::Exception& xde)
										{
											std::string msg = "Failed to parse instance number '";
											msg += instanceString;
											msg += "'";
											XCEPT_RETHROW (xdaq::exception::ConfigurationError, msg, xde);
										}
										catch (xdaq::exception::Exception& inf)
										{
											std::stringstream s;
											s << "Cannot create unicast route for class " << className << " instance " << instanceString
												<< ", instance not set or existing for class.";
											XCEPT_RETHROW (xdaq::exception::ConfigurationError, s.str(), inf);
										}
										//-------------------------------
										// check network available betwee local and remote host
										const xdaq::ContextDescriptor* remoteContext = to->getContextDescriptor();
									
										if ( netGroup->isNetworkExisting(network) ) // check that network is locally available
										{
											if (( netGroup->getNetwork(network)->isEndpointExisting(localContext)  ) &&
										   	 ( netGroup->getNetwork(network)->isEndpointExisting(remoteContext)  ) )
											{
											
												routingTable->setNetworkPath(from,to, network);
											
											}
											else
											{
												std::stringstream s;
												s << "Cannot create unicast route for class " << className << " instance " << instanceString
													<< ", network " << network << " not available on context "
													<< localContext->getURL() << " or context "
													<< remoteContext->getURL();
												XCEPT_RAISE (xdaq::exception::ConfigurationError, s.str());
											}
										
										}
										
									}
								}
							}
						}
						
						#warning "Add <Multicast> configuration parsing"
					} // if <Application> tag
				} // for all applicatons
			} // if local host
		} // if it is <Host>
	} // for all Hosts
}

void executive::Application::configureDefaultRouting (DOMNode* partitionNode) 
	throw (xdaq::exception::BadCommand, xdaq::exception::ConfigurationError)
{
	const xdaq::ContextDescriptor* localContext = this->getApplicationContext()->getContextDescriptor();
	const xdaq::Zone* zone = this->getApplicationContext()->getDefaultZone();
	
	const xdaq::ContextTable* contextTable = this->getApplicationContext()->getContextTable();
	std::set<const xdaq::ApplicationDescriptor *> localApplicationDescriptors;
	localApplicationDescriptors = zone->getApplicationDescriptors(this->getApplicationContext()->getContextDescriptor());
	if (localApplicationDescriptors.size() == 0)
	{
		XCEPT_RAISE (xdaq::exception::ConfigurationError, "Not application configurations found");
	}
	
	const xdaq::NetGroup *  netGroup = this->getApplicationContext()->getNetGroup();
	xdaq::RoutingTable *  routingTable = const_cast<xdaq::RoutingTable*>(this->getApplicationContext()->getRoutingTable());

	// loop over all  Transport in local Hosts
	DOMNodeList * hostList = partitionNode->getChildNodes();
	for ( XMLSize_t i = 0 ; i < hostList->getLength(); i++ )
	{
		DOMNode * hostNode = hostList->item(i);
							
		if ( ( hostNode->getNodeType() == DOMNode::ELEMENT_NODE ) && ( xoap::XMLCh2String(hostNode->getLocalName()) == "Context" ) )
		{
			std::string nsURI = xoap::XMLCh2String(hostNode->getNamespaceURI());		
			XCEPT_ASSERT (nsURI == executive::Application::ConfigurationNamespaceURI, xdaq::exception::BadCommand, toolbox::toString("Invalid or missing namespace in SOAP configuration command. Expected %s, received %s", nsURI.c_str(), executive::Application::ConfigurationNamespaceURI.c_str()));		

			std::string contextURL = xoap::getNodeAttribute(hostNode, "url" );
			XCEPT_ASSERT (contextURL != "", xdaq::exception::BadCommand, "Missing 'url' attribute in 'Context' tag");
			
			const xdaq::ContextDescriptor* currentContext = contextTable->getContextDescriptor(contextURL);
			
			DOMNodeList * applicationList = hostNode->getChildNodes();
			for ( XMLSize_t j = 0; j < applicationList->getLength(); j++ )
			{
				DOMNode * applicationNode = applicationList->item(j);
				
				if  (	( applicationNode->getNodeType() == DOMNode::ELEMENT_NODE ) && 
					( xoap::XMLCh2String(applicationNode->getLocalName()) == "Application" ))
				{
					std::string nsURI = xoap::XMLCh2String(applicationNode->getNamespaceURI());		
					XCEPT_ASSERT (nsURI == executive::Application::ConfigurationNamespaceURI, xdaq::exception::BadCommand, toolbox::toString("Invalid or missing namespace in SOAP configuration command. Expected %s, received %s", nsURI.c_str(), executive::Application::ConfigurationNamespaceURI.c_str()));	
	
					std::string localIdString = xoap::getNodeAttribute (applicationNode, "id");
					std::string network = xoap::getNodeAttribute (applicationNode, "network");
					XCEPT_ASSERT (network != "", xdaq::exception::BadCommand, "Missing 'network' attribute in 'Application' tag");	
					
					// At this point, a resolver may be consulted to retrieve the local id if it is not
					// specified in the XML file. The information available is: context URL and class name
					#warning "At this place a resolver may be called"
						
					xdata::UnsignedInteger localId;
					try
					{
						localId.fromString(localIdString);
					}
					catch(xdata::exception::Exception& xde)
					{
						std::string msg = "Failed to parse local identifier '";
						msg += localIdString;
						msg += "'";
						XCEPT_RETHROW (xdaq::exception::ConfigurationError, msg, xde);
					}
								
					const xdaq::ApplicationDescriptor* currentDescriptor =
						zone->getApplicationDescriptor(
							currentContext, 
							static_cast<xdata::UnsignedIntegerT>(localId)
							);
							
					bool matches = false;
					try 
					{
			 			matches = localContext->matchURL(contextURL);
					}
					catch (xdaq::exception::InvalidURL & e)
					{
						XCEPT_RETHROW (xdaq::exception::ConfigurationError, "Cannot match context descriptors", e);
					}
					
					if (!matches)
					{						
						if ( netGroup->isNetworkExisting(network) ) // check that network is locally available
						{
							try {
								if ( netGroup->getNetwork(network)->isEndpointExisting(localContext) )
								{
									// for all local application add route to this destination
									std::set<const xdaq::ApplicationDescriptor *>::iterator k;
									for ( k = localApplicationDescriptors.begin(); k != localApplicationDescriptors.end(); k++ )
									{
										routingTable->addNetworkPath((*k), currentDescriptor, network);
									}
								}
							} 
							catch (xdaq::exception::NoNetwork& nn)
							{
								std::string msg = "The network provided with attribute 'network' in tag 'Application' was not found: ";
								msg += network;
								XCEPT_RETHROW (xdaq::exception::ConfigurationError, msg, nn);
							} 
							catch (xdaq::exception::DuplicateRoute& dr)
							{
								XCEPT_RETHROW (xdaq::exception::ConfigurationError, "Failed to add network path", dr);
							}
						}
						/* see BUG [1277448]
						else
						{
							std::string msg = "The network provided with attribute 'network' in tag 'Application' is locally not available: ";
							msg += network;
							XCEPT_RAISE (xdaq::exception::ConfigurationError, msg);
						}
						*/						
					}
					else // local application , are accessible through loopback (netowkr = "local") using the ptFifo)
					{
						if (( network != "" )&& ( netGroup->isNetworkExisting(network)  ))
						{
							try 
							{
								if ( !netGroup->getNetwork(network)->isEndpointExisting(localContext) )
								{
									std::string msg = "Missing ednpoint (address definition) for network: ";
									msg += network;
									XCEPT_RAISE (xdaq::exception::ConfigurationError, msg);
								}
							} 
							catch (xdaq::exception::NoNetwork& nn)
							{
								std::string msg = "The network provided with attribute 'network' in tag 'Application' was not found: ";
								msg += network;
								XCEPT_RETHROW (xdaq::exception::ConfigurationError, msg, nn);
							}
						}
						else
						{
							std::string msg = "The network provided with attribute 'network' in tag 'Application' is locally not available: ";
							msg += network;
							XCEPT_RAISE (xdaq::exception::ConfigurationError, msg);
						}
						
						// for all local application add route to this destination
						//
						std::set<const xdaq::ApplicationDescriptor *>::iterator k;
						for ( k = localApplicationDescriptors.begin(); k != localApplicationDescriptors.end(); k++ )
						{	
						
							try
							{
								routingTable->addNetworkPath((*k), currentDescriptor, "local");
							} 
							catch (xdaq::exception::DuplicateRoute& dr)
							{
								XCEPT_RETHROW (xdaq::exception::ConfigurationError, "Failed to add network path", dr);
							}
						}
					}
				}
			} // for all Applications
		} // if Host
	} // for all Hosts
}




void executive::Application::configureNetworks (DOMNode* partitionNode) throw (xdaq::exception::BadCommand, xdaq::exception::ConfigurationError)
{
	xdaq::NetGroup * netGroup = const_cast<xdaq::NetGroup *>(this->getApplicationContext()->getNetGroup()); //editable netgroup
	const xdaq::ContextDescriptor* localContext = this->getApplicationContext()->getContextDescriptor();

	if ( !netGroup->isNetworkExisting("local") ) 
	{
			// add netowkr 'local' of protocol 'loopback' of ptFifo
			netGroup->addNetwork("local", "loopback");
			// Add a single endpoint for the loopback (address is forced to null)
			xdaq::Network* localNetwork = const_cast<xdaq::Network*>(netGroup->getNetwork("local")); // editable network
			pt::Address::Reference localAddress = pt::getPeerTransportAgent()->createAddress("loopback://localhost", "i2o");
			localNetwork->addEndpoint(localAddress, this->getApplicationContext()->getContextDescriptor(), false);
	}

	DOMNodeList * hostList = partitionNode->getChildNodes();
	for ( XMLSize_t i = 0 ; i < hostList->getLength(); i++ )
	{
		DOMNode * hostNode = hostList->item(i);
		
		if ( ( hostNode->getNodeType() == DOMNode::ELEMENT_NODE ) && ( xoap::XMLCh2String(hostNode->getLocalName()) == "Context" ) )
		{
			std::string nsURI = xoap::XMLCh2String(hostNode->getNamespaceURI());		
			XCEPT_ASSERT (nsURI == executive::Application::ConfigurationNamespaceURI, xdaq::exception::BadCommand, toolbox::toString("Invalid or missing namespace in SOAP configuration command. Expected %s, received %s", nsURI.c_str(), executive::Application::ConfigurationNamespaceURI.c_str()));						

			std::string url = xoap::getNodeAttribute(hostNode, "url" );
			XCEPT_ASSERT (url != "", xdaq::exception::BadCommand, "Missing 'url' attribute in 'Context' tag");
			
			
			DOMNodeList * addressList = hostNode->getChildNodes();
			for ( XMLSize_t j = 0 ; j < addressList->getLength(); j++ )
			{
				DOMNode * addressNode = addressList->item(j);
			
				if  (	( addressNode->getNodeType() == DOMNode::ELEMENT_NODE ) && 
					( xoap::XMLCh2String(addressNode->getLocalName()) == "Endpoint" ))
				{
					std::string nsURI = xoap::XMLCh2String(addressNode->getNamespaceURI());		
					XCEPT_ASSERT (nsURI == executive::Application::ConfigurationNamespaceURI, xdaq::exception::BadCommand, toolbox::toString("Invalid or missing namespace in SOAP configuration command. Expected %s, received %s", nsURI.c_str(), executive::Application::ConfigurationNamespaceURI.c_str()));						

				
					std::string network  = xoap::getNodeAttribute (addressNode, "network");
					if (network == "")
					{
						XCEPT_RAISE (xdaq::exception::BadCommand, "Missing 'network' attribute in 'Endpoint' tag");
					}
					
					std::string protocol = xoap::getNodeAttribute (addressNode, "protocol");
					if (network == "")
					{
						XCEPT_RAISE (xdaq::exception::BadCommand, "Missing 'protocol' attribute in 'Endpoint' tag");
					}
					
					std::string service = xoap::getNodeAttribute (addressNode, "service");
					if (service == "")
					{
						XCEPT_RAISE (xdaq::exception::BadCommand, "Missing 'service' attribute in 'Endpoint' tag");
					}
						
					if ( !netGroup->isNetworkExisting(network) ) 
					{
						try
						{
							pt::getPeerTransportAgent()->getPeerTransport (protocol, service, pt::Sender);
							netGroup->addNetwork(network, protocol);
						} 
						catch (pt::exception::PeerTransportNotFound& pte)
						{
							// if the endpoint is local (not remote in another context) it is an error,
							// otherwise ignore and continue with other endpoints
							bool matches = false;
							try 
							{
			 					matches = localContext->matchURL(url);
							}
							catch (xdaq::exception::InvalidURL & e)
							{
								XCEPT_RETHROW (xdaq::exception::ConfigurationError, "Cannot match context descriptors", e);
							}
							if (matches)
							{
								XCEPT_RETHROW (xdaq::exception::ConfigurationError, "failed configuring networks, missing pt for endpoint", pte);
							}
							
							// std::cout << "*** No network for " << protocol << " can be created, continuing..." << std::endl;
							continue;
						}
					}
					xdaq::Network * net  = const_cast<xdaq::Network *>(netGroup->getNetwork(network)); //editable network
							
					// create address
					std::map<std::string, std::string, std::less<std::string> > addressProperties;
					DOMNamedNodeMap * attributes = addressNode->getAttributes();
					for (XMLSize_t k = 0; k < attributes->getLength(); k++)
					{
						addressProperties[xoap::XMLCh2String(attributes->item(k)->getNodeName())] = xoap::XMLCh2String(attributes->item(k)->getNodeValue());
					}
							
					try
					{
						pt::Address::Reference  address = pt::getPeerTransportAgent()->createAddress(addressProperties);
						// add address to network
						bool publish = false;
						if (addressProperties["publish"] == "true")
						{
							publish = true;
						}
					
						net->addEndpoint(address,  this->getApplicationContext()->getContextTable()->getContextDescriptor(url), false, publish);
					} 
					catch (pt::exception::InvalidAddress & iad)
					{
						XCEPT_RETHROW (xdaq::exception::ConfigurationError, "failed configuring networks", iad );
					}
					catch (xdaq::exception::DuplicateEndpoint& de)
					{
						XCEPT_RETHROW (xdaq::exception::ConfigurationError, "failed configuring networks", de );
					}
					catch (xdaq::exception::AddressMismatch& am)
					{
						XCEPT_RETHROW (xdaq::exception::ConfigurationError, "failed configuring networks",am );
					}
					catch (pt::exception::PeerTransportNotFound& ptnf)
					{
						XCEPT_RETHROW (xdaq::exception::ConfigurationError,"failed configuring networks", ptnf );
					}
					
				} // if Address
			} // for all Addresses
		} // if Host	
	} // all Hosts
}


void executive::Application::configureTransportReceivers() throw (xdaq::exception::BadCommand, xdaq::exception::ConfigurationError)
{
	const xdaq::NetGroup * netGroup = this->getApplicationContext()->getNetGroup();
	std::vector<const xdaq::Network*> netList = netGroup->getNetworks();
	
	for (size_t i = 0; i < netList.size(); i++)
	{
		try 
		{
			// Only configure receivers on endpoints that are existing on this host
			if ( netList[i]->isEndpointExisting(this->getApplicationContext()->getContextDescriptor()) )
			{
				const xdaq::Endpoint* e = netList[i]->getEndpoint( this->getApplicationContext()->getContextDescriptor() );
				if ( (! e->isAlias()) && (! e->isActive()))
				{
					pt::Address::Reference address = e->getAddress();
					std::string protocol = address->getProtocol();
					std::string service = address->getService();	
					
					LOG4CPLUS_DEBUG (this->getApplicationLogger(), toolbox::toString ("Configure endpoint %s/%s on network %s", protocol.c_str(), service.c_str(), netList[i]->getName().c_str()));
					
					pt::PeerTransportReceiver* transport = dynamic_cast<pt::PeerTransportReceiver*>(pt::getPeerTransportAgent()->getPeerTransport (protocol, service, pt::Receiver));
					transport->config(address);
					const_cast<xdaq::Endpoint*>(e)->setActive(true); //editable endpoint

					// Fire availability of endpoint (now reachable) - to be published by discovery service
					//
					xdaq::EndpointAvailableEvent endpointAvailableEvent(e, netList[i], this);
					dynamic_cast<xdaq::ApplicationContextImpl*>(this->getApplicationContext())->fireEvent(endpointAvailableEvent);
				}
				
			}
		} 
		catch (xdaq::exception::NoEndpoint& ne)
		{
			XCEPT_RETHROW (xdaq::exception::ConfigurationError,"Error when configuring the receiver" ,ne );
		}
		catch (pt::exception::PeerTransportNotFound& ptnf)
		{
			XCEPT_RETHROW (xdaq::exception::ConfigurationError, "Error when configuring the receiver", ptnf );
		}
		catch (pt::exception::Exception& ce)
		{
			XCEPT_RETHROW (xdaq::exception::ConfigurationError, "Error when configuring the receiver", ce);
		}
	}
	
}


void executive::Application::configureAliasNetworks (DOMNode* partitionNode) throw (xdaq::exception::BadCommand, xdaq::exception::ConfigurationError)
{
	xdaq::NetGroup * netGroup =const_cast<xdaq::NetGroup*>( this->getApplicationContext()->getNetGroup()); //editable netgroup
	DOMNodeList * hostList = partitionNode->getChildNodes();
	for ( XMLSize_t i = 0 ; i < hostList->getLength(); i++ ) 	// loop over all host and aliases 	
	{
		DOMNode * hostNode = hostList->item(i);

		if ( 	( hostNode->getNodeType() == DOMNode::ELEMENT_NODE ) && 
			( xoap::XMLCh2String(hostNode->getLocalName()) == "Context" ) )
		{			
			std::string nsURI = xoap::XMLCh2String(hostNode->getNamespaceURI());		
			XCEPT_ASSERT (nsURI == executive::Application::ConfigurationNamespaceURI, xdaq::exception::BadCommand, toolbox::toString("Invalid or missing namespace in SOAP configuration command. Expected %s, received %s", nsURI.c_str(), executive::Application::ConfigurationNamespaceURI.c_str()));						

			std::string url = xoap::getNodeAttribute(hostNode, "url" );
			XCEPT_ASSERT (url != "", xdaq::exception::BadCommand, "Missing 'url' attribute in 'Context' tag");
			
		
			
			DOMNodeList * addressList = hostNode->getChildNodes();
			for ( XMLSize_t j = 0 ; j < addressList->getLength(); j++ )
			{
				DOMNode * addressNode = addressList->item(j);
				
				if  (	( addressNode->getNodeType() == DOMNode::ELEMENT_NODE ) && 
					( xoap::XMLCh2String(addressNode->getLocalName()) == "Alias" ))
				{
				
					std::string nsURI = xoap::XMLCh2String(addressNode->getNamespaceURI());		
					XCEPT_ASSERT (nsURI == executive::Application::ConfigurationNamespaceURI, xdaq::exception::BadCommand, toolbox::toString("Invalid or missing namespace in SOAP configuration command. Expected %s, received %s", nsURI.c_str(), executive::Application::ConfigurationNamespaceURI.c_str()));					

				
					std::string aliasName  = xoap::getNodeAttribute (addressNode, "name");   // new network name
					if (aliasName == "")
					{
						XCEPT_RAISE (xdaq::exception::BadCommand, "Missing 'name' attribute in 'Alias' tag");
					}
					
					std::string existingNetwork = xoap::XMLCh2String(addressNode->getFirstChild()->getNodeValue());
					if (existingNetwork == "")
					{
						XCEPT_RAISE (xdaq::exception::BadCommand, "Missing network specification as node value of 'Alias' tag");
					}
					
					if ( netGroup->isNetworkExisting(existingNetwork) ) 
					{
						try
						{
							xdaq::Network *  network = const_cast<xdaq::Network *>(netGroup->getNetwork(existingNetwork)); //editable network
							pt::Address::Reference address = network->getAddress(this->getApplicationContext()->getContextTable()->getContextDescriptor(url));
							if (! netGroup->isNetworkExisting(aliasName))
							{
								netGroup->addNetwork(aliasName, address->getProtocol());
							}
							
							xdaq::Network * aliasNetwork = const_cast<xdaq::Network *>(netGroup->getNetwork(aliasName)); //editable network
							aliasNetwork->addEndpoint(address, this->getApplicationContext()->getContextTable()->getContextDescriptor(url), true);
						} 
						catch (xdaq::exception::NoEndpoint& ne)
						{
							XCEPT_RETHROW (xdaq::exception::ConfigurationError, "Failed to add network", ne);
						}
						catch (xdaq::exception::DuplicateEndpoint& de)
						{
							XCEPT_RETHROW (xdaq::exception::ConfigurationError, "Failed to add network", de);
						}
						catch (xdaq::exception::AddressMismatch& am)
						{
							XCEPT_RETHROW (xdaq::exception::ConfigurationError, "Failed to add network", am);
						}
					}
					else
					{
						std::string msg = "Network ";
						msg += existingNetwork;
						msg += " not found while trying to specify an 'Alias' entry for the network";
						XCEPT_RAISE (xdaq::exception::ConfigurationError, msg);
					}
					
				}
			}
		}	
	}

}




void executive::Application::instantiateApplications (DOMNode* partitionNode)  throw (xdaq::exception::BadCommand, xdaq::exception::ConfigurationError)
{
	const xdaq::ContextDescriptor* localContext = this->getApplicationContext()->getContextDescriptor();
	const xdaq::ContextTable* contextTable = this->getApplicationContext()->getContextTable();
	DOMNodeList * hostList = partitionNode->getChildNodes();
	for ( XMLSize_t i = 0 ; i < hostList->getLength(); i++ )
	{
		DOMNode * hostNode = hostList->item(i);				
		
		if ( 	( hostNode->getNodeType() == DOMNode::ELEMENT_NODE ) && 
			( xoap::XMLCh2String(hostNode->getLocalName()) == "Context" ) )
		{
			std::string nsURI = xoap::XMLCh2String(hostNode->getNamespaceURI());		
			XCEPT_ASSERT (nsURI == executive::Application::ConfigurationNamespaceURI, xdaq::exception::BadCommand, toolbox::toString("Invalid or missing namespace in SOAP configuration command. Expected %s, received %s", nsURI.c_str(), executive::Application::ConfigurationNamespaceURI.c_str()));						
		
			std::string contextURL = xoap::getNodeAttribute(hostNode, "url" );
			XCEPT_ASSERT (contextURL != "", xdaq::exception::BadCommand, "Missing 'url' attribute in 'Context' tag");
		
			const xdaq::ContextDescriptor * currentContext = contextTable->getContextDescriptor(contextURL);
			bool matches = false;
			try 
			{
			 	matches = localContext->matchURL(contextURL);
			}
			catch (xdaq::exception::InvalidURL & e)
			{
				XCEPT_RETHROW (xdaq::exception::ConfigurationError, "Cannot match context descriptors", e);
			}
			if (matches)
			{
				
				DOMNodeList * applicationList = hostNode->getChildNodes();
				for ( XMLSize_t j = 0; j < applicationList->getLength(); j++ )
				{
					DOMNode * applicationNode = applicationList->item(j);					
							
					if  (( applicationNode->getNodeType() == DOMNode::ELEMENT_NODE ) && 
					     ( xoap::XMLCh2String(applicationNode->getLocalName()) == "Application" ) )
					{
						std::string nsURI = xoap::XMLCh2String(applicationNode->getNamespaceURI());		
						XCEPT_ASSERT (nsURI == executive::Application::ConfigurationNamespaceURI, xdaq::exception::BadCommand, toolbox::toString("Invalid or missing namespace in SOAP configuration command. Expected %s, received %s", nsURI.c_str(), executive::Application::ConfigurationNamespaceURI.c_str()));					
												

						std::string className = xoap::getNodeAttribute (applicationNode, "class");
						
						XCEPT_ASSERT(className != "", xdaq::exception::BadCommand, "Missing 'class' attribute in 'Application' tag");
						 
						std::string instanceString = xoap::getNodeAttribute (applicationNode, "instance");
						

						std::string localIdString = xoap::getNodeAttribute (applicationNode, "id");
						
						XCEPT_ASSERT(localIdString != "", xdaq::exception::BadCommand, "Missing 'id' attribute in 'Application' tag");
						
						try
						{
							xdaq::Application * application = 0;
							xdata::UnsignedInteger localId(localIdString);
							const xdaq::Zone * zone = getApplicationContext()->getDefaultZone();
							const xdaq::ApplicationDescriptor* d =
								zone->
								getApplicationDescriptor(currentContext, static_cast<xdata::UnsignedIntegerT>(localId) );
							
							xdaq::ApplicationRegistry * registry =  const_cast<xdaq::ApplicationRegistry*>(this->getApplicationContext()->getApplicationRegistry());
							application =registry->instantiateApplication(const_cast<xdaq::ApplicationDescriptor*>(d));

							std::string msg = "Instantiated application for class '";
							msg += className;
							msg += "', id '";
							msg += localIdString;
							msg += "' on context '";
							msg += currentContext->getURL();
							msg += "'";
							
							LOG4CPLUS_DEBUG(getApplicationLogger(), msg);
							
							DOMNodeList* applicationProperties = applicationNode->getChildNodes();
							for ( XMLSize_t k = 0; k < applicationProperties->getLength(); k++) 
							{
								if (  xoap::XMLCh2String(applicationProperties->item(k)->getLocalName()) == "properties") 
								{
									LOG4CPLUS_DEBUG(getApplicationLogger(),"set default properties for application for class: " << className << " id: " << localIdString << " on context: " << currentContext->getURL() );

									application->setDefaultValues(applicationProperties->item(k));								
									break;
								}
							}
							
							xdaq::InstantiateApplicationEvent instantiateEvent(d,this);
							dynamic_cast<xdaq::ApplicationContextImpl*>(this->getApplicationContext())->fireEvent(instantiateEvent);
						}
						catch (xdata::exception::Exception& xde)
						{
							std::string msg = "Failed to parse local identifier '";
							msg += localIdString;
							msg += "'";
							XCEPT_RETHROW (xdaq::exception::ConfigurationError, msg, xde);
						}
						catch (xdaq::exception::ApplicationInstantiationFailed& inf)
						{
							std::string msg = "Failed to instantiate application class '";
							msg += className;
							msg += "', local identifier '";
							msg += localIdString;
							msg += "'";
							XCEPT_RETHROW (xdaq::exception::ConfigurationError, msg, inf);
						}
						catch (xdaq::exception::ParameterSetFailed& spf)
						{
							std::string msg = "Failed to set default parameters for application class '";
							msg += className;
							msg += "', local identifier '";
							msg += localIdString;
							msg += "'";
							XCEPT_RETHROW (xdaq::exception::ConfigurationError, msg, spf);
						}
						catch (xdaq::exception::ApplicationNotFound& anf)
						{
							std::string msg = "Failed to set default parameters for resident application '";
							msg += className;
							msg += "', local identifier '";
							msg += localIdString;
							msg += "'";
							XCEPT_RETHROW (xdaq::exception::ConfigurationError, msg, anf);
						}
						catch (xdaq::exception::ApplicationDescriptorNotFound& acnf)
						{
							std::string msg = "Failed to set default parameters for resident application '";
							msg += className;
							msg += "', local identifier '";
							msg += localIdString;
							msg += "'";
							XCEPT_RETHROW (xdaq::exception::ConfigurationError, msg, acnf);
						}
					}
				}
				// instantiated all application on the local host then return
				return;
			}
		}
	}	

}


void executive::Application::setContextPolicies (DOMNode* partitionNode) throw (xdaq::exception::BadCommand, xdaq::exception::ConfigurationError)
{
	// loop over all Applications and Transport in all Hosts
	DOMNodeList * hostList = partitionNode->getChildNodes();
	for ( XMLSize_t i = 0 ; i < hostList->getLength(); i++ )
	{
		DOMNode * hostNode = hostList->item(i);
		
		if ( ( hostNode->getNodeType() == DOMNode::ELEMENT_NODE ) && ( xoap::XMLCh2String(hostNode->getLocalName()) == "Context" ) )
		{
			std::string nsURI = xoap::XMLCh2String(hostNode->getNamespaceURI());		
			XCEPT_ASSERT (nsURI == executive::Application::ConfigurationNamespaceURI, xdaq::exception::BadCommand, toolbox::toString("Invalid or missing namespace in SOAP configuration command. Expected %s, received %s", nsURI.c_str(), executive::Application::ConfigurationNamespaceURI.c_str()));						
		
			
			std::string contextURL = xoap::getNodeAttribute(hostNode, "url" );
			XCEPT_ASSERT (contextURL != "", xdaq::exception::BadCommand, "Missing 'url' attribute in 'Context' tag");
			
			bool matches = false;
			try 
			{
			 	matches = getApplicationContext()->getContextDescriptor()->matchURL(contextURL);
			}
			catch (xdaq::exception::InvalidURL & e)
			{
				XCEPT_RETHROW (xdaq::exception::ConfigurationError, "Cannot match context descriptors", e);
			}
			
			if (matches)
			{
				// create policy configuration
				dynamic_cast<xdaq::ApplicationContextImpl*>(this->getApplicationContext())->configurePolicies((DOMElement*)hostNode);
			}
		}
	} // Host tag
}
void executive::Application::downloadModules (DOMNode* partitionNode) throw (xdaq::exception::BadCommand, xdaq::exception::ConfigurationError)
{
	// loop over all Applications and Transport in all Hosts
	DOMNodeList * hostList = partitionNode->getChildNodes();
	for ( XMLSize_t i = 0 ; i < hostList->getLength(); i++ )
	{
		DOMNode * hostNode = hostList->item(i);
		
		if ( ( hostNode->getNodeType() == DOMNode::ELEMENT_NODE ) && ( xoap::XMLCh2String(hostNode->getLocalName()) == "Context" ) )
		{
			std::string nsURI = xoap::XMLCh2String(hostNode->getNamespaceURI());		
			XCEPT_ASSERT (nsURI == executive::Application::ConfigurationNamespaceURI, xdaq::exception::BadCommand, toolbox::toString("Invalid or missing namespace in SOAP configuration command. Expected %s, received %s", nsURI.c_str(), executive::Application::ConfigurationNamespaceURI.c_str()));						
		
			
			std::string contextURL = xoap::getNodeAttribute(hostNode, "url" );
			XCEPT_ASSERT (contextURL != "", xdaq::exception::BadCommand, "Missing 'url' attribute in 'Context' tag");
			
			bool matches = false;
			try 
			{
			 	matches = getApplicationContext()->getContextDescriptor()->matchURL(contextURL);
			}
			catch (xdaq::exception::InvalidURL & e)
			{
				XCEPT_RETHROW (xdaq::exception::ConfigurationError, "Cannot match context descriptors", e);
			}
			
			if (matches)
			{
				DOMNodeList * urlList = hostNode->getChildNodes();
				for ( XMLSize_t j = 0 ; j < urlList->getLength(); j++ )
				{
					DOMNode * urlNode = urlList->item(j);
										
					if  (
						( urlNode->getNodeType() == DOMNode::ELEMENT_NODE ) && 
						( xoap::XMLCh2String(urlNode->getLocalName()) == "Module")
					     )
					{
					
						std::string nsURI = xoap::XMLCh2String(urlNode->getNamespaceURI());		
						XCEPT_ASSERT (nsURI == executive::Application::ConfigurationNamespaceURI, xdaq::exception::BadCommand, toolbox::toString("Invalid or missing namespace in SOAP configuration command. Expected %s, received %s", nsURI.c_str(), executive::Application::ConfigurationNamespaceURI.c_str()));								

						std::string filename = xoap::XMLCh2String ( (urlNode->getFirstChild())->getNodeValue() );
						
						// trim file name to remove leading and ending blanks
						filename = toolbox::trim(filename);
						XCEPT_ASSERT (filename != "", xdaq::exception::BadCommand, "Missing filename in 'Module' tag");
						
						// Load Module using ApplicationContextImpl API
						//						
						try
						{
							dynamic_cast<xdaq::ApplicationContextImpl*>(this->getApplicationContext())
								->loadModule(filename);
						} 
						catch (xdaq::exception::Exception& xle)
						{
							std::string msg = "Cannot load module from ";
							msg += filename;
							XCEPT_RETHROW (xdaq::exception::ConfigurationError, msg, xle);
						}
					} // url Tag	
				} // urls
				
				return;
			}
		} // Host tag
	} // hosts
}

void executive::Application::addApplicationConfigs (DOMNode* partitionNode) throw (xdaq::exception::BadCommand, xdaq::exception::ConfigurationError)
{
	const xdaq::ContextTable* contextTable = this->getApplicationContext()->getContextTable();
	const xdaq::ContextDescriptor* localContext = this->getApplicationContext()->getContextDescriptor();
	// loop over all Applications and Transport in all Hosts
	DOMNodeList * hostList = partitionNode->getChildNodes();
	for ( XMLSize_t i = 0 ; i < hostList->getLength(); i++ )
	{
		DOMNode * hostNode = hostList->item(i);
				
		if ( ( hostNode->getNodeType() == DOMNode::ELEMENT_NODE ) && ( xoap::XMLCh2String(hostNode->getLocalName()) == "Context" ) )
		{
			std::string nsURI = xoap::XMLCh2String(hostNode->getNamespaceURI());		
			XCEPT_ASSERT (nsURI == executive::Application::ConfigurationNamespaceURI, xdaq::exception::BadCommand, toolbox::toString("Invalid or missing namespace in SOAP configuration command. Expected %s, received %s", nsURI.c_str(), executive::Application::ConfigurationNamespaceURI.c_str()));					

			std::string contextURL = xoap::getNodeAttribute(hostNode, "url" );
			XCEPT_ASSERT (contextURL != "", xdaq::exception::BadCommand, "Missing 'url' attribute in 'Context' tag");
			
			bool matches = false;
			try 
			{
			 	matches = localContext->matchURL(contextURL);
			}
			catch (xdaq::exception::InvalidURL & e)
			{
				XCEPT_RETHROW (xdaq::exception::ConfigurationError, "Cannot match context descriptors", e);
			}
			if (!matches)
			{
				try
				{
					// Assume the sessionId of other contexts is not known, so create context with empty sessionId ""
					const_cast<xdaq::ContextTable*>(contextTable)->createContextDescriptor(contextURL);
				} 
				catch (xdaq::exception::DuplicateHost& dh)
				{
					XCEPT_RETHROW (xdaq::exception::ConfigurationError, "Failed to create context descriptor", dh);
				}
				catch (xdaq::exception::CreationFailed& cf)
				{
					XCEPT_RETHROW (xdaq::exception::ConfigurationError, "Failed to create context descriptor", cf);
				}
			}
			
			DOMNodeList * applicationList = hostNode->getChildNodes();
						
			for ( XMLSize_t j = 0 ; j < applicationList->getLength(); j++ )
			{
				DOMNode * applicationNode = applicationList->item(j);
							
				if  (	( applicationNode->getNodeType() == DOMNode::ELEMENT_NODE ) && 
					(( xoap::XMLCh2String(applicationNode->getLocalName()) == "Application" ))
					)
				{
				
					std::string nsURI = xoap::XMLCh2String(applicationNode->getNamespaceURI());		
					XCEPT_ASSERT (nsURI == executive::Application::ConfigurationNamespaceURI, xdaq::exception::BadCommand, toolbox::toString("Invalid or missing namespace in SOAP configuration command. Expected %s, received %s", nsURI.c_str(), executive::Application::ConfigurationNamespaceURI.c_str()));								
					
					// configure application in application group				
	
					std::string className = xoap::getNodeAttribute (applicationNode, "class");
					XCEPT_ASSERT (className != "", xdaq::exception::BadCommand, "Missing 'class' attribute in 'Application' tag");
					
					std::string idString = xoap::getNodeAttribute (applicationNode, "id");					
					XCEPT_ASSERT (idString != "", xdaq::exception::BadCommand, "Missing 'id' attribute in 'Application' tag");
					
					std::string instanceString = xoap::getNodeAttribute (applicationNode, "instance");					
					std::string  groupString = xoap::getNodeAttribute (applicationNode, "group");
					
					try
					{
						xdata::UnsignedInteger localId(idString);
						const xdaq::ContextDescriptor* c = contextTable->getContextDescriptor(contextURL);
						std::set<std::string> groups;
						toolbox::StringTokenizer strtok(groupString,",");
						while (strtok.hasMoreTokens())
						{
							std::string g = toolbox::trim(strtok.nextToken());
							groups.insert(g);
						}
						//all allowed zones
						std::set<std::string> allowedZones = xdaq::ApplicationDescriptorFactory::getInstance()->getZoneNames(); 
						
						xdaq::ApplicationDescriptor * d = xdaq::ApplicationDescriptorFactory::getInstance()->createApplicationDescriptor(c, className,
							static_cast<xdata::UnsignedIntegerT>(localId), allowedZones, groups); 
						
						
						LOG4CPLUS_DEBUG(getApplicationLogger(),"Create application descriptor for class: " << className << " id: " << idString); 
						
						// reset instance  number
						if ( instanceString != "")
						{
							static_cast<xdaq::ApplicationDescriptorImpl*>(d)->setInstance(std::atoi (instanceString.c_str()));
							LOG4CPLUS_DEBUG(getApplicationLogger(),"set application instance: "  <<  instanceString << " for class: " <<	className  << " id: " << idString);
						}
						
						// Set all application descriptor attributes: class, instance, ...
						DOMNamedNodeMap* attributeList = applicationNode->getAttributes();
						for (XMLSize_t j = 0; j < attributeList->getLength(); j++)
						{
							DOMNode* currentAttribute = attributeList->item(j);
							d->setAttribute(xoap::XMLCh2String(currentAttribute->getNodeName()),
									xoap::XMLCh2String(currentAttribute->getNodeValue()));
						}					
					}
					catch (xdata::exception::Exception& xde)
					{
						XCEPT_RETHROW (xdaq::exception::ConfigurationError, "Configuration failed", xde);
					}
					catch (xdaq::exception::ApplicationDescriptorNotFound & adnf )
					{
						XCEPT_RETHROW (xdaq::exception::ConfigurationError, "Configuration failed", adnf);
					}
					catch (xdaq::exception::DuplicateApplicationDescriptor& dad)
					{
						XCEPT_RETHROW (xdaq::exception::ConfigurationError, "Configuration failed", dad);
					}
					catch (xdaq::exception::DuplicateTid& dt)
					{
						XCEPT_RETHROW (xdaq::exception::ConfigurationError, "Configuration failed", dt);
					}
					catch (xdaq::exception::HostNotFound& hnf)
					{
						XCEPT_RETHROW (xdaq::exception::ConfigurationError, "Configuration failed", hnf);
					}
						
				}			
			} // for all applications
		}
	}
	
}



void executive::Application::configureProtocols (DOMNode* partitionNode) throw (xdaq::exception::BadCommand, xdaq::exception::ConfigurationError)
{
	// Extract all context ids
	DOMNodeList* contextList = partitionNode->getOwnerDocument()->getElementsByTagNameNS( xoap::XStr("*"), xoap::XStr("protocol") );	
	
	// loop over all context ids	
	for (XMLSize_t i = 0; i < contextList->getLength(); i++)
	{
		DOMNode* protocolNode = contextList->item(i);
		std::string nsURI = xoap::XMLCh2String(protocolNode->getNamespaceURI());
		if ( nsURI == "http://xdaq.web.cern.ch/xdaq/xsd/2004/I2OConfiguration-30" )
		{		
			i2o::utils::AddressMap * addressMap = i2o::utils::getAddressMap();
			const xdaq::Zone * zone = this->getApplicationContext()->getDefaultZone();
			
			DOMNodeList* targetList = protocolNode->getChildNodes();
			for (XMLSize_t j = 0; j < targetList->getLength(); j++)
			{
				DOMNode* targetNode = targetList->item(j);
				if ( xoap::XMLCh2String(targetNode->getLocalName()) == "target")
				{
					std::string className = xoap::getNodeAttribute ( targetNode, "class");
					std::string instanceNumber = xoap::getNodeAttribute ( targetNode, "instance");
					std::string targetIdentifier = "";
					
					try 
					{
						const xdaq::ApplicationDescriptor * d = zone->getApplicationDescriptor(className, atoi(instanceNumber.c_str()));
					
						targetIdentifier = xoap::getNodeAttribute ( targetNode, "tid");
					
						// Still needs porting to 32 bit integer types, e.g. i2o U32
						//
						addressMap->setApplicationDescriptor(atoi(targetIdentifier.c_str()), d );
					}
					catch (xdaq::exception::ApplicationDescriptorNotFound& acnf)
					{
						std::string msg = toolbox::toString("Cannot map i2o target for tid: %s",targetIdentifier.c_str());
						msg += acnf.what();
						XCEPT_RETHROW (xdaq::exception::ConfigurationError, msg, acnf);
					}	
					
				}
			}
			// no other protocol tags are supported
			return;		
		}
		else
		{
			XCEPT_RAISE (xdaq::exception::ConfigurationError,toolbox::toString("Protocol specification: %s not supported",nsURI.c_str()));		
		}
	}		
}

/*
<MULTICAST> handling
else if ( xoap::XMLCh2String(settingsList->item(k)->getNodeName()) == "Multicast") 
{
	std::string className = xoap::getNodeAttribute ( settingsList->item(k), "class");
	U16 classId = this->getApplicationContext()->getClassIdByName(className);
	this->getApplicationContext()->clearMulticastConfiguration(classId);

	DOMNodeList* networkList = settingsList->item(k)->getChildNodes();
	for (unsigned int i = 0; i < networkList->getLength(); i++)
	{
		DOMNode* nameNode = networkList->item(i);
		if ( xoap::XMLCh2String(nameNode->getNodeName()) == "Network")
		{
			std::string networkName = xoap::getNodeAttribute ( nameNode, "name");
			U32 networkCode = this->getApplicationContext()->getAddressNameCode( networkName );
			U8 transportType = this->getApplicationContext()->getTransportType( networkName );
			this->getApplicationContext()->addMulticastNetwork ( classId, networkCode, transportType );
		}
	}
}
*/


//
// run control requests current paramater values
//
void executive::Application::actionPerformed (xdata::Event& e) 
{ 
	if ( e.type() == "urn:xdata-event:ItemGroupRetrieveEvent" )
	{
			xdata::ItemGroupRetrieveEvent & event = dynamic_cast<xdata::ItemGroupRetrieveEvent&>(e);
			if ( event.infoSpace()->name().find("urn:executive-sysinfo") != std::string::npos )
			{
				// Sample the memory and CPU usage
				// Still needs porting to 64 bit - all values should be unsigned int
				//		
				processInfo_->sample();
				hostInfo_->sample();

				m_sampleDuration_ = processInfo_->sampleDuration();
				m_pid_ = processInfo_->pid();
				m_cpuUsage_ = processInfo_->cpuUsage();	
				m_virtualSize_ = processInfo_->vsize();	
				m_residentSize_ = processInfo_->rsize();
				
				m_totalCpuUsage_ = hostInfo_->getCpuUsage();	
				
				try
        			{
                			toolbox::FileSystemInfo::Reference fsi = toolbox::getRuntime()->getFileSystemInfo();
					toolbox::TimeVal sampleTime = fsi->sample ();
                       
                			std::set<std::string> fsList = fsi->getFileSystems();

					// may erase table content before writing
					
					unsigned int row = 0;
                       			for (std::set<std::string>::iterator i = fsList.begin(); i != fsList.end(); ++i)
                       			{
						xdata::String fs ( *i );
                               			m_diskUsage_.setValueAt( row, "fileSystem", fs );
						
						xdata::Double totalMB ( fsi->total( (*i), "MB" ) );
                               			m_diskUsage_.setValueAt( row, "totalMB", totalMB );
						
						xdata::Double usePercent ( fsi->used( (*i), "%" ) );
                               			m_diskUsage_.setValueAt( row, "usePercent", usePercent );
						
						xdata::TimeVal sampled ( sampleTime );
                               			m_diskUsage_.setValueAt( row, "sampleTime", sampled );	
						
						xdata::String state ( fsi->state( *i ) );
                               			m_diskUsage_.setValueAt( row, "state", state );	
						
						++row;					
                       			}
        			}
				catch ( toolbox::exception::ReadError& re)
				{
					std::cout << xcept::stdformat_exception_history(re) << std::endl;
				}
        			catch  (toolbox::exception::Exception & e)
        			{
                			std::cout << xcept::stdformat_exception_history(e) << std::endl;
        			}
					
			}	
	} 
	else if (e.type() == "ItemChangedEvent")
	{
		std::string item = dynamic_cast<xdata::ItemChangedEvent&>(e).itemName();
		if ( item == "logUrl")
		{
			if (logUrl_ != lastLogUrl_)
			{				
				try
				{
					dynamic_cast<xdaq::ApplicationContextImpl*>(this->getApplicationContext())->setAppender(logUrl_,
										this->getApplicationContext()->getContextDescriptor()->getURL());
					// set the new appender
					std::string msg = "Changed Log URL to: ";
					msg += logUrl_;
					LOG4CPLUS_INFO(getApplicationLogger(), msg);
					lastLogUrl_ = logUrl_;
				}
				catch (xdaq::exception::Exception& e)
				{
					std::string msg = "Failed to set log URL to ";
					msg += logUrl_;
					LOG4CPLUS_ERROR(getApplicationLogger(), msg << ", " << xcept::stdformat_exception_history(e));
				}
			}
			else
			{
				LOG4CPLUS_INFO(getApplicationLogger(), "Log URL not set (was already " << logUrl_.toString() << ")");
			}			
		} 
		else if ( item == "logLevel")
		{
			if (logLevel_ != lastLogLevel_)
			{
				try
				{					
					dynamic_cast<xdaq::ApplicationContextImpl*>(this->getApplicationContext())->setLogLevel(logLevel_);
					lastLogLevel_ = logLevel_;
					// set the new appender
					std::string msg = "Changed Log level to ";
					msg += logLevel_;
					LOG4CPLUS_INFO(getApplicationLogger(), msg);
				} 
				catch (xdaq::exception::Exception& xe)
				{
					logLevel_ = lastLogLevel_; // reset to original
					LOG4CPLUS_WARN(getApplicationLogger(), xcept::stdformat_exception_history(xe));
				}
			}
			else
			{
				LOG4CPLUS_INFO(getApplicationLogger(), "Log level not set (was already " << logLevel_.toString() << ")");
			}
		} 
	}				
}
