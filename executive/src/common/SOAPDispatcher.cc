// $Id: SOAPDispatcher.cc,v 1.6 2008/07/18 15:26:47 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xoap/domutils.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPPart.h"
#include "xoap/SOAPBody.h"
#include "xoap/Method.h"

#include "toolbox/string.h"
#include "xdaq/exception/ApplicationNotFound.h"
#include "xdaq/NamespaceURI.h"
#include "xcept/tools.h"
#include "xcept/tools.h"
#include "executive/SOAPDispatcher.h"
#include "executive/Application.h"
#include "xdata/UnsignedInteger.h"
#include "xoap/MessageFactory.h"
#include "xoap/SOAPConstants.h"
#include "xdaq/XceptSerializer.h"
	
executive::SOAPDispatcher::SOAPDispatcher(xdaq::ApplicationContext* context, log4cplus::Logger & logger): 
	logger_( log4cplus::Logger::getInstance( logger.getName() + ".xdaq.SOAPDispatcher"))
{
	this->registry_ = context->getApplicationRegistry();
	this->context_ = context;
}

executive::SOAPDispatcher::~SOAPDispatcher()
{
}

xoap::MessageReference executive::SOAPDispatcher::processIncomingMessage 
(
	xoap::MessageReference msg
) 
	throw (pt::exception::Exception)
{	
	std::string faultstring = "";
	DOMNodeList* bodyList = 0;
	
	// Parsing the Content-Location to retrieve the application pointer
	//
	std::string urn = "";
	try
	{
		xoap::SOAPPart part = msg->getSOAPPart();
		xoap::SOAPEnvelope env = part.getEnvelope();
		xoap::SOAPBody body = env.getBody();
		DOMNode* node = body.getDOMNode();
		bodyList = node->getChildNodes();


		// Check if URN is in request URL
		//
		std::vector<std::string> values = msg->getMimeHeaders()->getHeader("request_uri");
		if ( (values.size() == 1) )
		{
			std::string::size_type rqsturipos = values[0].find ("urn:xdaq-application:");
			if (rqsturipos != std::string::npos)
			{
				urn = values[0].substr(rqsturipos);
			}
		}

		// Check if URN is in SOAPAction, wsa or content-location
		//
		// Find the application address by looking in soapaction and content-location
		// The fields are case insensitive. If none of the two fields has the application's
		// address, assume the use of the ws-addressing standard.
		//
		values = msg->getMimeHeaders()->getHeader("soapaction");
		if ( ( urn == "" ) && (values.size() == 1) )
		{
			urn = values[0];		
		}

		values = msg->getMimeHeaders()->getHeader("content-location");
		if ( ( urn == "" ) && (values.size() == 1) )
		{
			urn = values[0];
		}

		if ( urn == "" ) // ws-addressing scheme
		{
			// TBD: Checck not only for one namespace, but for the two: 2004 and 2005
			//
			xoap::SOAPName actionName ("Action","wsa","http://www.w3.org/2005/08/addressing");
			std::vector<xoap::SOAPHeaderElement> elements = env.getHeader().examineHeaderElements(actionName);
			if (elements.size() != 1 )
			{
				XCEPT_RAISE (pt::exception::Exception, "Missing <Action/> tag in SOAP header");
			}
			else
			{
				urn = elements[0].getValue();
			}
		}			
	}
	catch (xoap::exception::Exception& e)
	{
		XCEPT_RETHROW (pt::exception::Exception, "Failed to process message", e);
	}

	// Extract urn
	urn = toolbox::trim(urn); // first remove left right blanks if any
	urn = toolbox::trim(urn,"\""); // unquote the string
	
	xdata::UnsignedIntegerT lid;
	xdaq::Application* application = 0;
	
	std::string::size_type pos = urn.find("lid=");
	if (pos != std::string::npos)
	{
		// extract id: +4 needs to change if it is not anymore "lid="
		std::string idNumberString = urn.substr(pos+4,  urn.size()-(pos+4) );

		if (idNumberString == "")
		{
			LOG4CPLUS_ERROR (logger_, "Missing id number in Content-Location");
			XCEPT_RAISE (pt::exception::Exception, "Missing id number in Content-Location");
		}

		//id = XDAQ_TID(context_->getContextId(), std::atoi(idNumber.c_str()));
		try
		{
			xdata::UnsignedInteger idNumber(idNumberString);
			lid = static_cast<xdata::UnsignedIntegerT>(idNumber);
			application = registry_->getApplication(lid);
		}
		catch (xdaq::exception::ApplicationNotFound& anf)
		{
			XCEPT_RETHROW(pt::exception::Exception, "Application not found by lid", anf);
		}
		catch (xdata::exception::Exception& e)
		{
			XCEPT_RETHROW(pt::exception::Exception, "Failed to parse local id", e);
		}
	}
	else if ( (pos = urn.find("class=")) != std::string::npos )
	{
		// find instance number
		std::string::size_type instancePos = urn.find(",instance=");
		if (instancePos != std::string::npos)
		{
			// Extract class name
			std::string className = urn.substr(pos+6, instancePos-(pos+6));
			// std::cout << "ClassName: " << className << std::endl;

			std::string instanceNumber = urn.substr(instancePos+10, urn.size()-(instancePos+10) );
			// std::cout << "InstanceNumber: " << instanceNumber << std::endl;

			if (className == "")
			{
				LOG4CPLUS_ERROR (logger_, "Missing class name in Content-Location");
				XCEPT_RAISE (pt::exception::Exception, "Missing class name in Content-Location");
			}

			if (instanceNumber == "")
			{
				LOG4CPLUS_ERROR (logger_, "Missing instance number in Content-Location");
				XCEPT_RAISE (pt::exception::Exception, "Missing instance number in Content-Location");
			}

			try 
			{
				xdata::UnsignedInteger instance(instanceNumber);
				application = registry_->getApplication( className, static_cast<xdata::UnsignedIntegerT>(instance) );
			} 
			catch (xdaq::exception::ApplicationNotFound& anf)
			{
				std::stringstream msg;
				msg << "Cannot find application for classname:" << className << " instance: "  << instanceNumber;
				XCEPT_RETHROW(pt::exception::Exception, msg.str(), anf);
			}
			catch(xdata::exception::Exception& e)
			{
				XCEPT_RETHROW (pt::exception::Exception, "Failed to parse instance number", e);
			}
			catch (xdaq::exception::ApplicationDescriptorNotFound& adnf)
			{
				std::string msg = "Could not dispatch incoming SOAP message to class ";
				msg += className;
				msg += " instance number ";
				msg += instanceNumber;
				XCEPT_RETHROW (pt::exception::Exception, msg, adnf);
			}
		}
		else
		{
			LOG4CPLUS_ERROR (logger_, "Missing instance number in Content-Location");
			XCEPT_RAISE(pt::exception::Exception, "Missing instance number in Content-Location");
		}
	}
	else if ( (pos = urn.find("service=")) != std::string::npos )
	{
		// service name must be terminated either by "/" or end of URN
                std::string::size_type endOfService = urn.find("/",pos+8);
                if (endOfService == std::string::npos)
                {
                        endOfService = urn.size();
                }

		std::string service = urn.substr(pos+8,  endOfService-(pos+8) );
	
		std::vector<xdaq::Application*> apps = registry_->getApplications("service", service);
		if (apps.size() != 1)
		{
			std::string msg = toolbox::toString("Cannot retrieve %d applications from ambiguous urn %s", apps.size(), urn.c_str());
			LOG4CPLUS_ERROR (logger_, msg);
			XCEPT_RAISE(pt::exception::Exception, msg);
		}
		else
		{
			application = apps[0];
		}
	}
	else
	{	
		LOG4CPLUS_ERROR (logger_, "Missing id or class specification in Content-Location");
		XCEPT_RAISE(pt::exception::Exception, "Missing id or class specification in Content-Location");
	}

	std::string namespaceURI = "";
	DOMNode* command = 0;
	for (size_t i = 0; i < bodyList->getLength(); i++) 
	{
		command = bodyList->item(i);

		if (command->getNodeType() == DOMNode::ELEMENT_NODE)
		{
			namespaceURI = xoap::XMLCh2String (command->getNamespaceURI());

			if (namespaceURI == "")
			{
				LOG4CPLUS_ERROR (logger_, "Invalid or missing namespace in SOAP message");
				XCEPT_RAISE (pt::exception::Exception, "Invalid or missing namespace in SOAP message");
			}
			
			break; // found namespace
		}
	}
	
	if (command == 0)
	{
		LOG4CPLUS_ERROR (this->logger_, "Missing command in SOAP message body");
		XCEPT_RAISE (pt::exception::Exception, "Missing command in SOAP message body");
	}

	// Call a filter method before the actual SOAP callback. The filter
	// method is bound to "*:*", namespace "*", method name "*". A method "*"
	// in a given namespace is now implemented, too
	//
	xoap::MethodSignature* filterMethod = 0;
	std::string filterName = "*:*";
	toolbox::lang::Method* genericMethod = application->getMethod(filterName);
	
	// if no "*:*" found, try with namespace:*
	if (genericMethod == 0)
	{
		filterName = namespaceURI + ":*";
		genericMethod = application->getMethod(filterName);
	}
	
	// if a filter callback is found invoke it
	if (genericMethod != 0)
	{
		filterMethod = static_cast<xoap::MethodSignature*>(genericMethod);
		try
		{
			xoap::MessageReference filterReply = filterMethod->invoke(msg);
			if (!filterReply.isNull())
			{
				// no further upcall, return reply
				return filterReply;
			}
		}
		catch (xoap::exception::Exception& xdaqe)
		{
			/*std::string msg = "Exception occured while calling SOAP filter method on urn ";
			msg += urn;
			LOG4CPLUS_ERROR (this->logger_, xcept::stdformat_exception_history(xdaqe));
			XCEPT_RETHROW(pt::exception::Exception, msg, xdaqe);
			*/
			application->qualifyException("error",xdaqe);
			
			xoap::MessageFactory * soapFactoryImpl = msg->getImplementationFactory();
			xoap::MessageReference reply = soapFactoryImpl->createMessage();
			xoap::SOAPBody b = reply->getSOAPPart().getEnvelope().getBody();
			xoap::SOAPFault f = b.addFault();
			if (soapFactoryImpl->getProtocolVersion() == xoap::SOAPConstants::SOAP_1_1_PROTOCOL )
			{
				f.setFaultCode ("Server");
				f.setFaultString (xdaqe.what());
			}
			else
			{
				 xoap::SOAPName faultCodeQName ( "Receiver", soapFactoryImpl->getEnvelopePrefix(), xoap::SOAPConstants::URI_NS_SOAP_1_2_ENVELOPE);
                         	f.setFaultCode (faultCodeQName);
                         	f.addFaultReasonText(xdaqe.what(),std::locale("en_US"));
			}
			xoap::SOAPElement detail = f.addDetail();
			xdaq::XceptSerializer::writeTo(xdaqe,detail.getDOM());
			return reply;
			
		}
		catch (...)
		{
			std::string msg = "Unknown exception occured when calling SOAP filter method on urn ";
			msg += urn;
			LOG4CPLUS_ERROR (this->logger_, msg);
			XCEPT_RAISE (pt::exception::Exception, msg);
		}
	}
			
	// Now search the actual SOAP callback
	std::string commandName = namespaceURI;
	commandName += ":";
	commandName += xoap::XMLCh2String (command->getLocalName());

	// Put try/catch around and free frame in catch!
	xoap::MethodSignature* method = 0;

	toolbox::lang::Method* soapGenericMethod = application->getMethod(commandName);
	if (soapGenericMethod == 0)
	{
		std::stringstream msg;
		msg << "No callback method found for incoming request [" << commandName << "] in " << application->getApplicationDescriptor()->getClassName();
		LOG4CPLUS_ERROR (this->logger_, msg.str());
		XCEPT_RAISE(pt::exception::Exception, msg.str());
	}

	method = static_cast<xoap::MethodSignature*>(soapGenericMethod);

	try
	{
		xoap::MessageReference reply = method->invoke(msg);
		return reply;
	}
	catch (xoap::exception::Exception& xdaqe)
	{
		/*std::string msg = "Exception occured in user callback while calling SOAP method ";
		msg += commandName;
		msg += " on urn ";
		msg += urn;
		LOG4CPLUS_ERROR (this->logger_, xcept::stdformat_exception_history(xdaqe));
		XCEPT_RETHROW(pt::exception::Exception, msg, xdaqe);
		*/
		application->qualifyException("error",xdaqe);
		
		xoap::MessageFactory * soapFactoryImpl = msg->getImplementationFactory();
		xoap::MessageReference reply = soapFactoryImpl->createMessage();
		xoap::SOAPBody b = reply->getSOAPPart().getEnvelope().getBody();
		xoap::SOAPFault f = b.addFault();
		if (soapFactoryImpl->getProtocolVersion() == xoap::SOAPConstants::SOAP_1_1_PROTOCOL )
		{
			f.setFaultCode ("Server");
			f.setFaultString (xdaqe.what());
		}
		else
		{
			 xoap::SOAPName faultCodeQName ( "Receiver", soapFactoryImpl->getEnvelopePrefix(), xoap::SOAPConstants::URI_NS_SOAP_1_2_ENVELOPE);
                         f.setFaultCode (faultCodeQName);
                         f.addFaultReasonText(xdaqe.what(),std::locale("en_US"));

		}
		xoap::SOAPElement detail = f.addDetail();
		xdaq::XceptSerializer::writeTo(xdaqe,detail.getDOM());
		return reply;
		
		
	}
	catch (std::exception& stde)
	{
		std::string msg = "Standard exception occured in user callback when calling SOAP method ";
		msg += commandName;
		msg += " on application with urn ";
		msg += urn;
		msg += ": ";
		msg += stde.what();
		LOG4CPLUS_ERROR (this->logger_, msg);
		XCEPT_RAISE (pt::exception::Exception, msg);
	}
	catch (...)
	{
		std::string msg = "Unknown exception occured in user callback when calling SOAP method ";
		msg += commandName;
		msg += " on application with urn ";
		msg += urn;
		LOG4CPLUS_ERROR (this->logger_, msg);
		XCEPT_RAISE (pt::exception::Exception, msg);
	}
}
	
