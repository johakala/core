// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A.Petrucci, A.Forrest								 *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include "hyperdaq/framework/Layout.h"

#include "xdaq/ApplicationRegistry.h"

#include "toolbox/TimeVal.h"
#include "toolbox/Runtime.h"

#include "cgicc/HTMLClasses.h"

#include "xgi/Method.h"
#include "xgi/Utils.h"

#include "hyperdaq/Application.h"

hyperdaq::framework::Layout::Layout () throw (xdaq::exception::Exception)
{
	this->defaultIcon_ = "/hyperdaq/images/framework/application-icon.png";
	this->currentAppIcon_ = "";
	this->currentURL_ = "";

	this->xdaqStartTime_ = toolbox::TimeVal::gettimeofday().toString(toolbox::TimeVal::gmt);

	this->showSidebar_ = false;
}

void hyperdaq::framework::Layout::resetCGICC () throw (xgi::exception::Exception)
{
	// Reset all the HTML elements that might have been used to their initial state so we get valid output
	cgicc::html::reset();
	cgicc::head::reset();
	cgicc::body::reset();
	cgicc::title::reset();
	cgicc::h1::reset();
	cgicc::h2::reset();
	cgicc::h3::reset();
	cgicc::h4::reset();
	cgicc::h5::reset();
	cgicc::h6::reset();
	cgicc::div::reset();
	cgicc::p::reset();
	cgicc::table::reset();
	cgicc::thead::reset();
	cgicc::tbody::reset();
	cgicc::tfoot::reset();
	cgicc::tr::reset();
	cgicc::td::reset();
	cgicc::comment::reset();
	cgicc::a::reset();
	cgicc::colgroup::reset();
	cgicc::caption::reset();
	cgicc::form::reset();
	cgicc::legend::reset();
}

void hyperdaq::framework::Layout::getHTMLHeaderTop (xgi::framework::UIManager * manager, xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	out->getHTTPResponseHeader().addHeader("Content-Type", "text/html; charset=utf-8");

	*out << "<!doctype html>" << std::endl;
	*out << "<html>" << std::endl;
	*out << "<head>" << std::endl;

	*out << "	<meta charset=\"UTF-8\" />" << std::endl;
	*out << "	<meta name=\"viewport\" content=\"width = device-width\">" << std::endl;

	*out << "	<title>XDAQ HyperDAQ</title>" << std::endl;

	*out << "	<link rel=\"shortcut icon\" href=\"/xdaq/images/xdaq.ico\">" << std::endl;
	*out << "	<link rel=\"apple-touch-icon\" href=\"/xdaq/images/xdaq.ico\"/>" << std::endl;

	*out << "	<link href=\"/hyperdaq/html/css/xdaq-normalize.css\" rel=\"stylesheet\" />" << std::endl;
	*out << "	<link href=\"/hyperdaq/html/css/xdaq-buttons.css\" rel=\"stylesheet\" />" << std::endl;
	*out << "	<link href=\"/hyperdaq/html/css/xdaq-generics.css\" rel=\"stylesheet\" />" << std::endl;

	*out << "	<link href=\"/hyperdaq/html/css/xdaq-layout.css\" rel=\"stylesheet\" />" << std::endl;

	// Media specific stylesheets
	//*out << "	<link href=\"/hyperdaq/html/css/xdaq-layout.css\" media=\"screen and (min-width: 700px)\" rel=\"stylesheet\" />" << std::endl;
	//*out << "	<link href=\"/hyperdaq/html/css/xdaq-layout-mobile.css\" media=\"screen and (max-width: 700px)\" rel=\"stylesheet\" />" << std::endl;

	*out << "	<link href=\"/hyperdaq/html/css/xdaq-fonts.css\" rel=\"stylesheet\" />" << std::endl;
	*out << "	<link href=\"/hyperdaq/html/css/xdaq-forms.css\" rel=\"stylesheet\" />" << std::endl;
	*out << "	<link href=\"/hyperdaq/html/css/xdaq-tables.css\" rel=\"stylesheet\" />" << std::endl;
	*out << "	<link href=\"/hyperdaq/html/css/xdaq-trees.css\" rel=\"stylesheet\" />" << std::endl;
	*out << "	<link href=\"/hyperdaq/html/css/xdaq-tooltips.css\" rel=\"stylesheet\" />" << std::endl;
	*out << "	<link href=\"/hyperdaq/html/css/xdaq-tabs.css\" rel=\"stylesheet\" />" << std::endl;
	*out << "	<link href=\"/hyperdaq/html/css/xdaq-console.css\" rel=\"stylesheet\" />" << std::endl;
	*out << "	<link href=\"/hyperdaq/html/css/xdaq-hyperdaq-app-widgets.css\" rel=\"stylesheet\" />" << std::endl;

	//*out << "	<script type=\"text/javascript\" src=\"/hyperdaq/html/js/jquery-2.0.0.min.js\"></script>" 					<< std::endl;
	*out << "	<script type=\"text/javascript\" src=\"/hyperdaq/html/js/jquery-2.1.0.min.js\"></script>" << std::endl;
	//*out << "	<script type=\"text/javascript\" src=\"/hyperdaq/html/js/jquery-2.1.0.js\"></script>" 						<< std::endl;

	*out << "	<script type=\"text/javascript\" src=\"/hyperdaq/html/js/xdaq-utils.js\"></script>" << std::endl;
	*out << "	<script type=\"text/javascript\" src=\"/hyperdaq/html/js/xdaq-main-layout.js\"></script>" << std::endl;
	*out << "	<script type=\"text/javascript\" src=\"/hyperdaq/html/js/xdaq-tabs.js\"></script>" << std::endl;

	//*out << "	<script type=\"text/javascript\" src=\"/hyperdaq/html/js/xdaq-trees.js\"></script>" << std::endl;
	//*out << "	<script type=\"text/javascript\" src=\"/hyperdaq/html/js/xdaq-trees-2.0.js\"></script>" << std::endl;
	//*out << "	<script type=\"text/javascript\" src=\"/hyperdaq/html/js/xdaq-trees-dev.js\"></script>" << std::endl;
	*out << "	<script type=\"text/javascript\" src=\"/hyperdaq/html/js/xdaq-treetable.js\"></script>" << std::endl;
	*out << "	<script type=\"text/javascript\" src=\"/hyperdaq/html/js/xdaq-tree-utils.js\"></script>" << std::endl;

	*out << "	<script type=\"text/javascript\" src=\"/hyperdaq/html/js/xdaq-console.js\"></script>" << std::endl;
	*out << "	<script type=\"text/javascript\" src=\"/hyperdaq/html/js/xdaq-sort.js\"></script>" << std::endl;
	*out << "	<script type=\"text/javascript\" src=\"/hyperdaq/html/js/xdaq-tablesortable.js\"></script>" << std::endl;
	*out << "	<script type=\"text/javascript\" src=\"/hyperdaq/html/js/xdaq-window-load.js\"></script>" << std::endl;

	*out << "</head>" << std::endl;

	std::stringstream instanceNumber;
	try
	{
		size_t instance = manager->getApplication()->getApplicationDescriptor()->getInstance();
		instanceNumber <<  "data-app-instance=\"" << instance << "\"";
	}
	catch (xdaq::exception::Exception& e)
	{
		// instance number not set
		instanceNumber <<  "data-app-instance=\"\""; // should be accepted by the script as a valid input
	}

	*out << "<body id=\"xdaq-body\" class=\"xdaq-nojs\" data-app-name=\"" << manager->getApplication()->getApplicationDescriptor()->getClassName() << "\" " << instanceNumber.str() << ">" << std::endl;

	if (showSidebar_)
	{
		*out << "	<div id=\"xdaq-sidebar\" class=\"xdaq-sidebar-on\">" << std::endl;
	}
	else
	{
		*out << "	<div id=\"xdaq-sidebar\" class=\"\">" << std::endl;
	}

	*out << "     	<ul>" << std::endl;
}

// getters for the two different menus
void hyperdaq::framework::Layout::getHTMLStandardMenu (xgi::framework::UIManager * manager, xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	std::string url = "/";
	url += manager->getApplication()->getApplicationDescriptor()->getURN();

	*out << "  			<li class=\"xdaq-sidebar-divider\"><a title=\"\" href=\"/urn:xdaq-application:service=hyperdaq/viewApplications\"><img src=\"/hyperdaq/images/framework/controlpanel/control-panel.png\" alt=\"Control Panel\"/>Control Panel</a></li>" << std::endl;

	std::string curURL = in->getenv("PATH_TRANSLATED");
	std::string linkURL = "";

	std::list<xdaq::Application*> applications = manager->getApplication()->getApplicationContext()->getApplicationRegistry()->getApplications();
	for (std::list<xdaq::Application*>::iterator i = applications.begin(); i != applications.end(); i++)
	{
		const xdaq::ApplicationDescriptor* d = (*i)->getApplicationDescriptor();

		std::string icon = d->getAttribute("icon");
		if (icon == "") // use default icon
		{
			icon = defaultIcon_;
		}
		linkURL = "/" + d->getURN();

		// check for active URL by url matching (which is on lid) or by service
		std::string activeClass = "";
		if (linkURL == curURL || (linkURL + "/") == curURL)
		{
			activeClass = "xdaq-sidebar-active-url";
		}
		else
		{
			// try by service
			std::stringstream service;
			service << "/urn:xdaq-application:service=" << d->getAttribute("service");

			if (service.str() == curURL)
			{
				activeClass = "xdaq-sidebar-active-url";
			}
		}

		// if on hyperdaq homepage
		if (d->getClassName() == "hyperdaq::Application" && curURL == "/urn:xdaq-application:service=hyperdaq")
		{
			activeClass = "xdaq-sidebar-active-url";
		}

		std::string::size_type pos = d->getClassName().rfind("::Application");
		std::string title = d->getClassName().substr(0, pos);
		pos = 0;

		// Replace "::" namespace delimiters with spaces " "
		while ((pos = title.find("::", pos)) != std::string::npos)
		{
			title.replace(pos, 2, " ");
		}

		/*
		 // If it is still too long, cut it and append "..."
		 if (title.size() > 19)
		 {
		 title = title.substr(0, 17);
		 title += "...";
		 }
		 */

		*out << "    	<li class=\"" << activeClass << "\"><a title=\"\" href=\"" << linkURL << "\"><img src=\"" << icon << "\" alt=\"" << d->getClassName() << "\"/>" << title << "</a></li>" << std::endl;
	}
}

/*
 * This is all the header content that should appear BELOW the menu
 */
void hyperdaq::framework::Layout::getHTMLHeaderBottom (xgi::framework::UIManager * manager, xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	std::stringstream appURL;
	appURL << manager->getApplication()->getApplicationDescriptor()->getContextDescriptor()->getURL();

	std::stringstream appURN;
	appURN << manager->getApplication()->getApplicationDescriptor()->getURN();

	*out << "  		</ul>" << std::endl;

	*out << "	</div>" << std::endl;

	*out << "	<div id=\"xdaq-wrapper\">" << std::endl;

	*out << "		<div id=\"xdaq-header\">" << std::endl;
	*out << "			<a href=\"/\"><img src=\"/xdaq/images/xdaq-logo.png\" id=\"xdaq-header-xdaq-img\" alt=\"\"></a>" << std::endl;

	if (showSidebar_)
	{
		*out << "			<div data-requrl=\"" << appURL.str() << "\" id=\"xdaq-header-hide-sidebar\" class=\"xdaq-header-hide-sidebar-on\"></div>" << std::endl;
	}
	else
	{
		*out << "			<div data-requrl=\"" << appURL.str() << "\" id=\"xdaq-header-hide-sidebar\" class=\"xdaq-header-hide-sidebar-off\"></div>" << std::endl;
	}

	*out << "			<div id=\"xdaq-header-right-content\">" << std::endl;

	std::string icon = currentAppIcon_;
	if (currentAppIcon_ == "")
	{
		icon = manager->getApplication()->getApplicationDescriptor()->getAttribute("icon");
	}
	if (icon == "") // use default icon
	{
		icon = defaultIcon_;
	}
	*out << "				<div id=\"xdaq-header-app-name\">" << manager->getApplication()->getApplicationDescriptor()->getClassName() << "</div>" << std::endl;

	*out << "				<table>" << std::endl;
	*out << "					<tr>" << std::endl;
	*out << "						<td>" << toolbox::printTokenSet(manager->getApplication()->getApplicationContext()->getZoneNames(), ",") << "</td>" << std::endl;
	*out << "				        <td id=\"xdaq-header-menu-control\" data-id=\"control\"><img src=\"/hyperdaq/images/framework/xdaq-header-icon-control.png\" alt=\"\" class=\"xdaq-header-icon-img\" /></td>" << std::endl;
	*out << "				        <td id=\"xdaq-header-menu-apps\" data-id=\"apps\"><img src=\"/hyperdaq/images/framework/xdaq-header-icon-apps.png\" alt=\"\" class=\"xdaq-header-icon-img\" /></td>" << std::endl;
	*out << "				        <td id=\"xdaq-header-menu-cluster\" data-id=\"cluster\"><img src=\"/hyperdaq/images/framework/xdaq-header-icon-cluster.png\" alt=\"\" class=\"xdaq-header-icon-img\" /></td>" << std::endl;
	*out << "				        <td id=\"xdaq-header-menu-spacer\"></td>" << std::endl;
	*out << "				        <td id=\"xdaq-header-menu-settings\" data-id=\"settings\"><img src=\"/hyperdaq/images/framework/xdaq-header-icon-settings.png\" alt=\"\" class=\"xdaq-header-icon-img\" /></td>" << std::endl;
	*out << "				        <td id=\"xdaq-header-menu-props\" data-id=\"props\"><img src=\"/hyperdaq/images/framework/xdaq-header-icon-props.png\" alt=\"\" class=\"xdaq-header-icon-img\" /></td>" << std::endl;
	// If current URL is set, it will be added to link path. Used by the hyperdaq control panel for navigation within min-applications
	*out << "				        <td id=\"xdaq-header-application-img\"><a href=\"" << appURL.str() << "/" << appURN.str() << "/" << currentURL_ << "\"><img src=\"" << icon << "\" alt=\"\" /></a></td>" << std::endl;
	*out << "					</tr>" << std::endl;
	*out << "				</table>" << std::endl;
	*out << "			</div>" << std::endl;
	*out << "		</div>" << std::endl;

	*out << "		<div id=\"xdaq-main-wrapper\">" << std::endl;

	if (showSidebar_)
	{
		*out << "			<div id=\"xdaq-main\">" << std::endl;
	}
	else
	{
		*out << "			<div id=\"xdaq-main\" class=\"xdaq-sidebar-hidden-main\">" << std::endl;
	}

	////////////////////
	////////// HEADER MENUS

	// DIV ARROWS
	*out << "				<div id=\"xdaq-menu-arrow-outer\" class=\"xdaq-menu-hidden\"></div>" << std::endl;
	*out << "				<div id=\"xdaq-menu-arrow-inner\" class=\"xdaq-menu-hidden\"></div>" << std::endl;

	// CONTROL MENU
	*out << "				<div id=\"xdaq-menu-control\" class=\"xdaq-menu-hidden\">" << std::endl;
	*out << "					<table>" << std::endl;
	*out << "						<tbody>" << std::endl;

	std::stringstream hyperDAQURL;
	hyperDAQURL << appURL.str() << "/urn:xdaq-application:service=hyperdaq/";

	*out << "	<tr>" << std::endl;
	*out << "    	<td><a href=\"" << hyperDAQURL.str() << "exploreCluster\"><img src=\"/hyperdaq/images/framework/controlpanel/cluster-explore.png\" alt=\"\"/><br />Cluster</a></td>" << std::endl;
	*out << "    	<td><a href=\"" << hyperDAQURL.str() << "viewApplications\"><img src=\"/hyperdaq/images/framework/controlpanel/view-applications.png\" alt=\"\"/><br />Applications</a></td>" << std::endl;
	*out << "    	<td><a href=\"" << hyperDAQURL.str() << "applicationGroups\"><img src=\"/hyperdaq/images/framework/controlpanel/application-groups.png\" alt=\"\"/><br />Groups</a></td>" << std::endl;
	*out << "	</tr><tr>" << std::endl;
	*out << "    	<td><a href=\"" << hyperDAQURL.str() << "viewEndpoints\"><img src=\"/hyperdaq/images/framework/controlpanel/endpoints.png\" alt=\"\"/><br />Endpoints</a></td>" << std::endl;
	*out << "    	<td><a href=\"" << hyperDAQURL.str() << "configureCluster\"><img src=\"/hyperdaq/images/framework/controlpanel/configure-cluster.png\" alt=\"\"/><br />Configure</a></td>" << std::endl;
	*out << "    	<td><a href=\"" << hyperDAQURL.str() << "controlCluster\"><img src=\"/hyperdaq/images/framework/controlpanel/control-cluster.png\" alt=\"\"/><br />Control</a></td>" << std::endl;
	*out << "	</tr><tr>" << std::endl;
	*out << "    	<td><a href=\"" << hyperDAQURL.str() << "uploadApplicationForm\"><img src=\"/hyperdaq/images/framework/controlpanel/upload-application.png\" alt=\"\"/><br />Upload</a></td>" << std::endl;
	*out << "    	<td><a href=\"" << hyperDAQURL.str() << "debugSOAP\"><img src=\"/hyperdaq/images/framework/controlpanel/debug-soap.png\" alt=\"\"/><br />SOAP</a></td>" << std::endl;
	*out << "    	<td><a href=\"" << hyperDAQURL.str() << "logPage\"><img src=\"/hyperdaq/images/framework/controlpanel/logging-information.png\" alt=\"\"/><br />Logging</a></td>" << std::endl;
	*out << "	</tr><tr>" << std::endl;
	*out << "    	<td><a href=\"" << hyperDAQURL.str() << "processInformation\"><img src=\"/hyperdaq/images/framework/controlpanel/process-information.png\" alt=\"\"/><br />Process</a></td>" << std::endl;
	*out << "    	<td><a href=\"" << hyperDAQURL.str() << "displayModules\"><img src=\"/hyperdaq/images/framework/controlpanel/display-modules.png\" alt=\"\"/><br />Modules</a></td>" << std::endl;
	*out << "    	<td><a href=\"" << hyperDAQURL.str() << "cpuPolicies\"><img src=\"/hyperdaq/images/framework/controlpanel/cpu-policies.png\" alt=\"\"/><br />CPU</a></td>" << std::endl;
	*out << "	</tr><tr>" << std::endl;
	*out << "    	<td><a href=\"" << hyperDAQURL.str() << "memoryPolicies\"><img src=\"/hyperdaq/images/framework/controlpanel/memory-policies.png\" alt=\"\"/><br />Memory</a></td>" << std::endl;
	*out << "    	<td class=\"xdaq-table-nohover\"></td>" << std::endl;
	*out << "    	<td class=\"xdaq-table-nohover\"></td>" << std::endl;
	*out << "	</tr>" << std::endl;

	*out << "						</tbody>" << std::endl;
	*out << "					</table>" << std::endl;

	*out << "				</div>" << std::endl;

	// APPS MENU
	*out << "				<div id=\"xdaq-menu-apps\" class=\"xdaq-menu-hidden\">" << std::endl;

	*out << "					<table>" << std::endl;
	*out << "						<tbody>" << std::endl;

	std::string linkURL = "";
	size_t rowCount = 3;
	size_t mod = 0;

	std::list<xdaq::Application*> applications = manager->getApplication()->getApplicationContext()->getApplicationRegistry()->getApplications();
	for (std::list<xdaq::Application*>::iterator i = applications.begin(); i != applications.end(); i++)
	{
		if (mod % rowCount == 0)
		{
			*out << "					<tr>" << std::endl;
		}
		mod++;

		const xdaq::ApplicationDescriptor* d = (*i)->getApplicationDescriptor();

		std::string icon = d->getAttribute("icon");
		if (icon == "") // use default icon
		{
			icon = defaultIcon_;
		}
		linkURL = "/" + d->getURN();

		std::string::size_type pos = d->getClassName().rfind("::Application");
		std::string displayString = d->getClassName().substr(0, pos);
		pos = 0;

		// Replace "::" namespace delimiters with spaces " "
		while ((pos = displayString.find("::", pos)) != std::string::npos)
		{
			displayString.replace(pos, 2, " ");
		}

		// If it is still too long, cut it and append "..."
		size_t displayLength = 14;
		if (displayString.size() > displayLength)
		{
			size_t lengthOver = displayString.size() - displayLength;
			if (lengthOver > 3)
			{
				lengthOver = 3;
			}
			displayString = displayString.substr(0, displayLength - lengthOver);
			for (size_t k = 0; k < lengthOver; k++)
			{
				displayString += ".";
			}
		}

		*out << "    	<td><a href=\"" << linkURL << "\"><img src=\"" << icon << "\" alt=\"" << d->getClassName() << "\"/><br />" << displayString << "</a></td>" << std::endl;

		if (mod % rowCount == 0)
		{
			*out << "					</tr>" << std::endl;
		}
	}

	if (mod % rowCount != 0)
	{
		*out << "    	<td class=\"xdaq-table-nohover\"></td>" << std::endl;
		if (mod % rowCount == 1)
		{
			*out << "    	<td class=\"xdaq-table-nohover\"></td>" << std::endl;
		}
		*out << "					</tr>" << std::endl;
	}

	*out << "				</tbody>" << std::endl;
	*out << "			</table>" << std::endl;

	*out << "		</div>" << std::endl;

	// Cluster MENU
	*out << "		<div id=\"xdaq-menu-cluster\" class=\"xdaq-menu-hidden\">" << std::endl;

	*out << "					<table>" << std::endl;
	*out << "						<tbody>" << std::endl;

	rowCount = 2;
	mod = 0;
	std::vector<xdaq::ContextDescriptor *> contexts = manager->getApplication()->getApplicationContext()->getContextTable()->getContextDescriptors();
	for (std::vector<xdaq::ContextDescriptor *>::size_type i = 0; i < contexts.size(); i++)
	{
		if (mod % rowCount == 0)
		{
			*out << "					<tr>" << std::endl;
		}
		mod++;

		std::string url = toolbox::toString("%s/urn:xdaq-application:service=hyperdaq", contexts[i]->getURL().c_str());

		std::string displayStringHost = contexts[i]->getURL().substr(7); // remove 'http://' from url
		std::string displayStringPort = "";

		std::size_t found = displayStringHost.find(":");
		if (found != std::string::npos)
		{
			displayStringPort = displayStringHost.substr(found + 1);
			displayStringHost = displayStringHost.substr(0, found);
		}

		*out << "    	<td><a href=\"" << url << "\"><img src=\"/hyperdaq/images/framework/xdaq-host.png\" alt=\"" << contexts[i]->getURL() << "\"/><br />" << displayStringHost << "<br /><span style=\"font-weight: 200;\">" << displayStringPort << "</span></a></td>" << std::endl;

		if (mod % rowCount == 0)
		{
			*out << "					</tr>" << std::endl;
		}
	}

	if (mod % rowCount != 0)
	{
		*out << "    	<td class=\"xdaq-table-nohover\"></td>" << std::endl;
		*out << "					</tr>" << std::endl;
	}

	*out << "				</tbody>" << std::endl;
	*out << "			</table>" << std::endl;

	*out << "		</div>" << std::endl;

	// SETTINGS MENU
	*out << "		<div id=\"xdaq-menu-settings\" class=\"xdaq-menu-hidden\">" << std::endl;

	size_t lid = manager->getApplication()->getApplicationDescriptor()->getLocalId();

	std::stringstream requestURL;
	requestURL << appURL.str() << "/urn:xdaq-application:lid=" << lid;

	std::stringstream soapAction;
	soapAction << "urn:xdaq-application:lid=" << lid;

	const xdaq::ApplicationDescriptor* app = manager->getApplication()->getApplicationContext()->getApplicationRegistry()->getApplication(lid)->getApplicationDescriptor();

	*out << cgicc::table().set("class", "xdaq-table-tree xdaq-param-viewer").set("data-allow-refresh", "false").set("id", "xdaqparamviewer-menu").set("data-requesturl", requestURL.str()).set("data-soapaction", soapAction.str()).set("data-urn", app->getClassName()) << std::endl;

	//<table class="xdaq-table-tree xdaq-param-viewer" id="xdaqparamviewer-menu" data-requesturl="" << requestURL.str() << "" data-soapaction="" << soapAction.str() << "" data-urn="" << app->getClassName()) << "">

	*out << cgicc::thead() << std::endl;
	*out << cgicc::tr() << std::endl;
	*out << cgicc::th("Name <img src=\"/hyperdaq/images/framework/xdaq-refresh-icon.png\" alt=\"XDAQ Parameter Viewer Refresh\" id=\"xdaqparamviewer-menu-refresh\" class=\"xdaqparamviewer-get\" data-linkedtable=\"xdaqparamviewer-menu\"/>") << std::endl;
	*out << cgicc::th("Value") << std::endl;
	*out << cgicc::tr() << std::endl;
	*out << cgicc::thead() << std::endl;
	*out << cgicc::tbody() << std::endl;

	// Table is populated dynamically with a jQuery script that runs on document.load

	*out << cgicc::tbody() << std::endl;
	*out << cgicc::table() << std::endl;
	//*out << "		<br />" << std::endl;
	*out << "		</div>" << std::endl;

	// PROPERTIES MENU
	*out << "		<div id=\"xdaq-menu-props\" class=\"xdaq-menu-hidden\">" << std::endl;

	*out << cgicc::table().set("class", "xdaq-table") << std::endl;

	*out << cgicc::thead() << std::endl;
	*out << cgicc::tr() << std::endl;
	*out << cgicc::th("Name") << std::endl;
	*out << cgicc::th("Value") << std::endl;
	*out << cgicc::tr() << std::endl;
	*out << cgicc::thead() << std::endl;
	*out << cgicc::tbody() << std::endl;

	*out << cgicc::tr() << std::endl;
	*out << cgicc::td("Zones") << std::endl;
	*out << cgicc::td(toolbox::printTokenSet(manager->getApplication()->getApplicationContext()->getZoneNames(), ",")) << std::endl;
	*out << cgicc::tr() << std::endl;

	*out << cgicc::tr() << std::endl;
	*out << cgicc::td("Groups") << std::endl;
	*out << cgicc::td(manager->getApplication()->getApplicationDescriptor()->getAttribute("group")) << std::endl;
	*out << cgicc::tr() << std::endl;

	char **env;
	for (env = environ; *env != NULL; ++env)
	{
		toolbox::StringTokenizer st(*env, "=");
		if (st.hasMoreTokens())
		{
			std::string tokName = st.nextToken();

			if (tokName == "HOSTNAME" || tokName == "USER" || tokName == "XDAQ_DOCUMENT_ROOT")
			{
				*out << cgicc::tr();
				*out << cgicc::td(tokName);
				*out << cgicc::td(st.remainingString());
				*out << cgicc::tr();
			}
		}
	}

	*out << cgicc::tr() << std::endl;
	*out << cgicc::td("PID") << std::endl;
	std::stringstream pid;
	pid << toolbox::getRuntime()->getPid();
	*out << cgicc::td(pid.str()) << std::endl;
	*out << cgicc::tr() << std::endl;

	*out << cgicc::tr() << std::endl;
	*out << cgicc::td("Start Time") << std::endl;
	*out << cgicc::td(this->xdaqStartTime_) << std::endl;
	*out << cgicc::tr() << std::endl;

	*out << cgicc::tbody() << std::endl;
	*out << cgicc::table() << std::endl;
	//*out << "		<br />" << std::endl;
	*out << "		</div>" << std::endl;

	//////////
	////////////////////

	*out << "				<br />" << std::endl;
}

void hyperdaq::framework::Layout::getHTMLHeader (xgi::framework::UIManager * manager, xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	this->resetCGICC();
	this->getHTMLHeaderTop(manager, in, out);
	this->getHTMLStandardMenu(manager, in, out);
	this->getHTMLHeaderBottom(manager, in, out);
}

void hyperdaq::framework::Layout::getHTMLFooter (xgi::framework::UIManager * manager, xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	this->resetCGICC();

	toolbox::TimeVal copyrightDate = toolbox::TimeVal::gettimeofday();

	#define QUOTE(str) #str
	#define EXPAND_AND_QUOTE(str) QUOTE(str)
	#define RELEASE EXPAND_AND_QUOTE(PACKAGE_RELEASE)
	std::string release = RELEASE;

	*out << "					<br />" << std::endl;

	*out << "				</div>" << std::endl;
	*out << "			</div>" << std::endl;

	*out << "	    </div>" << std::endl;

	*out << "	    <div id=\"xdaq-footer\">" << std::endl;
	*out << "			XDAQ Release " << release << " - Copyright &copy; 2000 - " << copyrightDate.toString("%Y", toolbox::TimeVal::gmt) << " CERN" << std::endl;
	*out << "	    </div>" << std::endl;

	*out << "	</body>" << std::endl;
	*out << "	</html>" << std::endl;
}

/*
 * 404 page
 */
void hyperdaq::framework::Layout::noCallbackFound (xgi::framework::UIManager * manager, xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	std::string className = manager->getApplication()->getApplicationDescriptor()->getClassName();

	out->getHTTPResponseHeader().getStatusCode(404);
	out->getHTTPResponseHeader().getReasonPhrase(xgi::Utils::getResponsePhrase(404));

	*out << "<div id=\"xdaq-404\">" << std::endl;

	*out << "	<img src=\"/hyperdaq/images/framework/xdaq-404-crack.png\" id=\"xdaq-404-crack\"><br /><br />" << std::endl;
	*out << "	We are sorry, but something has happened! <br />" << std::endl;
	*out << "	You have navigated to a page that doesn't exist! <br />" << std::endl;
	*out << "	If this is unexpected, please contact the creators of the application. <br />" << std::endl;
	*out << "	For XDAQ application's, please go <a href=\"https://svnweb.cern.ch/trac/cmsos/\" target=\"_newtab\">HERE</a> to submit a support ticket. <br />" << std::endl;
	*out << "	<br />" << std::endl;
	*out << "	Application Information : <br /><br />" << std::endl;

	*out << cgicc::table().set("class", "xdaq-table-vertical").set("style", "display: inline-block;");

	*out << cgicc::tbody();

	*out << cgicc::tr().set("class", "xdaq-table-nohover");
	*out << cgicc::th("Name") << cgicc::td(className) << std::endl;
	*out << cgicc::tr();

	/*
	*out << cgicc::tr().set("class", "xdaq-table-nohover");
	std::stringstream requestHeader;
	requestHeader << out->getHTTPResponseHeader();
	*out << cgicc::th("Request Header") << cgicc::td(requestHeader.str()) << std::endl;
	*out << cgicc::tr();
	*/

	*out << cgicc::tbody();

	*out << cgicc::table();

	*out << "</div>" << std::endl;
}

/*
 * Error page
 */
void hyperdaq::framework::Layout::errorPage (xgi::framework::UIManager * manager, xgi::Input * in, xgi::Output * out, xcept::Exception& e) throw (xgi::exception::Exception)
{
	// Log error to output
	LOG4CPLUS_ERROR(manager->getApplication()->getApplicationLogger(), stdformat_exception_history(e));

	out->getHTTPResponseHeader().getStatusCode(400); // bad request
	out->getHTTPResponseHeader().getReasonPhrase(xgi::Utils::getResponsePhrase(400));
	this->getHTMLHeader(manager, in, out);

	*out << "	<link href=\"/hyperdaq/html/css/xdaq-exception-viewer.css\" rel=\"stylesheet\" />" << std::endl;

	*out << cgicc::h3("A server side error has occurred") << std::endl;

	*out << "<textarea style=\"display:none;\" id=\"dlContent\">" << stdformat_exception_history(e) << "</textarea>" << std::endl;
	*out << "<input value='Download TXT' type='button' onclick='downloadExceptionHistory(document.getElementById(\"dlContent\").value)' /><br /><br />" << std::endl;

	*out << cgicc::h4("Active Log Appenders") << std::endl;
	// List all log appenders
	*out << cgicc::table().set("class", "xdaq-table") << std::endl;
	*out << cgicc::thead() << std::endl;
	*out << cgicc::tr() << std::endl;
	*out << cgicc::th("Appender") << std::endl;
	*out << cgicc::th("Level") << std::endl;
	*out << cgicc::th("Output") << std::endl;
	*out << cgicc::tr() << std::endl;
	*out << cgicc::thead() << std::endl;

	*out << cgicc::tbody() << std::endl;

	SharedAppenderPtrList appenderList = Logger::getRoot().getAllAppenders();
	for (SharedAppenderPtrList::iterator al = appenderList.begin(); al != appenderList.end(); ++al)
	{
		*out << cgicc::tr() << std::endl;

		*out << cgicc::td((*al)->getName()) << std::endl;

		*out << cgicc::td(getLogLevelManager().toString((*al)->getThreshold())) << std::endl;

		// Output log url
		//
		if ((*al)->getName().find("file") == 0)
		{
			std::stringstream link;
			link << "<a href=\"logContentPage?url=" << cgicc::form_urlencode((*al)->getName());
			link << "&tail=1000\">view</a>";
			*out << cgicc::td(link.str()) << std::endl;
		}
		else
		{
			*out << cgicc::td("n/a") << std::endl;
		}

		*out << cgicc::tr() << std::endl;
	}

	*out << cgicc::tbody() << std::endl;
	*out << cgicc::table() << std::endl;

	//////////
	*out << "<br />         " << std::endl;
	*out << cgicc::h4("Exception History") << std::endl;

	*out << "<table id=\"hyperdaq-exception-viewer-table-wrapper\">          " << std::endl;
	*out << "<tbody>          " << std::endl;
	*out << "<tr>          " << std::endl;
	*out << "<td style=\"vertical-align:top;\" id=\"hyperdaq-exception-viewer-td-tree\">          " << std::endl;

	//////////

	*out << "<table id=\"hyperdaq-exception-viewer-tree\" class=\"xdaq-table-tree\" data-expand=\"true\" data-allow-refresh=\"false\">          " << std::endl;
	*out << "    <thead>                                                 " << std::endl;
	*out << "        <tr>                                                " << std::endl;
	*out << "            <th style=\"min-width: 100px;\">Exception Stack </th>      " << std::endl;
	*out << "        </tr>                                               " << std::endl;
	*out << "    </thead>                                                " << std::endl;
	*out << "    <tbody>                                                " << std::endl;

	size_t treeDepth = 0;

	std::vector<xcept::ExceptionInformation> & history = e.getHistory();
	std::vector<xcept::ExceptionInformation>::reverse_iterator i = history.rbegin();
	if (i != history.rend())
	{
		if (treeDepth == 0)
		{
			*out << "<tr class=\"xdaq-table-tree-noclick hyperdaq-exceptions-table-selected\" data-treeid=\"" << treeDepth << "\" data-tree-depth=\"" << treeDepth << "\" data-label=\"" << (*i).getProperty("identifier") << "\"><td>" << (*i).getProperty("identifier") << "</td></tr>" << std::endl;
		}
		else
		{
			*out << "<tr class=\"xdaq-table-tree-noclick\" data-treeid=\"" << treeDepth << "\" data-tree-depth=\"" << treeDepth << "\" data-label=\"" << (*i).getProperty("identifier") << "\"><td>" << (*i).getProperty("identifier") << "</td></tr>" << std::endl;
		}
		treeDepth++;
		i++;
		while (i != history.rend())
		{
			*out << "<tr class=\"xdaq-table-tree-noclick\" data-label=\"" << (*i).getProperty("identifier") << "\" data-treeid=\"" << treeDepth << "\" data-tree-depth=\"" << treeDepth << "\" data-treeparent=\"" << (treeDepth - 1) << "\"><td>" << (*i).getProperty("identifier") << "</td></tr>" << std::endl;
			treeDepth++;
			i++;
		}
	}

	*out << "    </tbody>                                                " << std::endl;
	*out << "</table>                                                    " << std::endl;

	//////////
	*out << "</td><td style=\"vertical-align:top;\" id=\"hyperdaq-exception-viewer-td-table\">          " << std::endl;
	//////////

	*out << "<table id=\"hyperdaq-exceptions-properties\" class=\"xdaq-table\">          " << std::endl;
	*out << "    <thead>                                                 " << std::endl;
	*out << "        <tr>                                                " << std::endl;
	*out << "            <th style=\"min-width: 100px;\">Name</th>      " << std::endl;
	*out << "            <th style=\"min-width: 100px;\">Value</th>      " << std::endl;
	*out << "        </tr>                                               " << std::endl;
	*out << "    </thead>                                                " << std::endl;

	treeDepth = 0;

	i = history.rbegin();
	while (i != history.rend())
	{


		if (treeDepth == 0)
		{
			*out << "    <tbody id=\"hyperdaq-exception-view-" << "" << treeDepth << "\">" << std::endl;
		}
		else
		{
			*out << "    <tbody id=\"hyperdaq-exception-view-" << "" << treeDepth << "\" class=\"hyperdaq-exception-viewer-hidden\">" << std::endl;
		}

		std::map<std::string, std::string, std::less<std::string> > properties = (*i).getProperties();
		for ( std::map<std::string, std::string, std::less<std::string> >::iterator j = properties.begin(); j != properties.end(); j++)
		{
			if ((*j).first != "identifier")
			{
				*out << "	 	<tr><td>" << (*j).first << "</td><td>" << (*j).second << "</td></tr>";
			}
		}
		*out << "    </tbody >                                                " << std::endl;


		treeDepth++;
		i++;
	}

	//*out << "    </tbody>                                                " << std::endl;
	*out << "</table>                                                    " << std::endl;

	//////////
	*out << "</td>          " << std::endl;
	*out << "</tr>          " << std::endl;
	*out << "</tbody>          " << std::endl;
	*out << "</table>         " << std::endl;

	*out << "	<script type=\"text/javascript\" src=\"/hyperdaq/html/js/xdaq-exception-viewer.js\"></script>" 					<< std::endl;

	//////////
	this->getHTMLFooter(manager, in, out);
}

void hyperdaq::framework::Layout::Default (xgi::framework::UIManager * manager, xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	std::string className = manager->getApplication()->getApplicationDescriptor()->getClassName();

	*out << "<div id=\"xdaq-default-default\">" << std::endl;

	*out << "	" << className << " doesn't have a HyperDAQ page! <br />" << std::endl;
	*out << "	If this is unexpected, please contact the creators of the application. <br />" << std::endl;
	*out << "	For XDAQ application's, please go <a href=\"https://svnweb.cern.ch/trac/cmsos/\" target=\"_newtab\">HERE</a> to submit a support ticket. <br />" << std::endl;

	*out << "</div>" << std::endl;
}
