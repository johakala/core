// $Id: version.cc,v 1.2 2008/07/18 15:26:48 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "hyperdaq/version.h"
#include "config/version.h"
#include "xcept/version.h"
#include "xdata/version.h"
#include "xdaq/version.h"
#include "xgi/version.h"

GETPACKAGEINFO(hyperdaq)

void hyperdaq::checkPackageDependencies() throw (config::PackageInfo::VersionException)
{
	CHECKDEPENDENCY(config);
	CHECKDEPENDENCY(xcept);
	CHECKDEPENDENCY(xdaq);   
	CHECKDEPENDENCY(xgi);     
}

std::set<std::string, std::less<std::string> > hyperdaq::getPackageDependencies()
{
	std::set<std::string, std::less<std::string> > dependencies;

	ADDDEPENDENCY(dependencies,config); 
	ADDDEPENDENCY(dependencies,xcept); 
	ADDDEPENDENCY(dependencies,xdaq); 
	ADDDEPENDENCY(dependencies,xgi);
	  
	return dependencies;
}	
	
