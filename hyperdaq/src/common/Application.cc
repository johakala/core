// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2018, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A.Petrucci, A.Forrest and D. Simelevicius          *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include "hyperdaq/Application.h"

#include "xgi/Utils.h"

#include "xgi/Method.h"
#include "xgi/framework/Method.h"

#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"
#include "cgicc/HTTPResponseHeader.h"

#include "xdaq/ApplicationGroup.h"
#include "xdaq/ApplicationRegistry.h"
#include "xdaq/ApplicationContextImpl.h"
#include "xdaq/ApplicationDescriptorFactory.h"
#include "xdaq/InstantiateApplicationEvent.h"

#include "xdata/InfoSpaceFactory.h"
#include "xdata/exdr/Serializer.h"

#include "xdata/xdata.h"
#include "xdata/soap/Serializer.h"
#include "xdata/XMLDOM.h"

#include "xcept/tools.h"

#include "log4cplus/loggingmacros.h"

#include "executive/Application.h"

// XOAP Include files
#include "xoap/MessageFactory.h"
#include "xoap/SOAPMessage.h"
#include "xoap/SOAPBody.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPHeader.h"
#include "xoap/domutils.h"
#include "xoap/DOMParserFactory.h"
#include "xoap/DOMParser.h"
#include "toolbox/Runtime.h" 
#include "toolbox/PolicyFactory.h" 
#include "toolbox/string.h" 
#include "jansson.h"

#include <sys/resource.h>

extern char **environ;

XDAQ_INSTANTIATOR_IMPL (hyperdaq::Application)

hyperdaq::Application::Application (xdaq::ApplicationStub * s) throw (xdaq::exception::Exception)
	: xdaq::Application(s), xgi::framework::UIManager(this)
{
	s->getDescriptor()->setAttribute("icon", "/hyperdaq/images/framework/hyperdaq-icon.png");

	s->getDescriptor()->setAttribute("service", "hyperdaq");

	xgi::framework::deferredbind(this, this, &hyperdaq::Application::Default, "Default");

	xgi::bind(this, &hyperdaq::Application::configureCluster, "configureCluster");
	xgi::bind(this, &hyperdaq::Application::configureClusterResult, "configureClusterResult");
	xgi::bind(this, &hyperdaq::Application::controlCluster, "controlCluster");
	xgi::bind(this, &hyperdaq::Application::exploreCluster, "exploreCluster");

	xgi::bind(this, &hyperdaq::Application::viewApplications, "viewApplications");
	xgi::bind(this, &hyperdaq::Application::viewEndpoints, "viewEndpoints");
	xgi::bind(this, &hyperdaq::Application::applicationGroups, "applicationGroups");
	xgi::bind(this, &hyperdaq::Application::viewGroup, "viewGroup");
	xgi::bind(this, &hyperdaq::Application::uploadApplicationForm, "uploadApplicationForm");
	xgi::bind(this, &hyperdaq::Application::uploadApplication, "uploadApplication");

	xgi::bind(this, &hyperdaq::Application::displayModules, "displayModules");
	xgi::bind(this, &hyperdaq::Application::displayPackageInfo, "displayPackageInfo");
	xgi::bind(this, &hyperdaq::Application::editProperties, "editProperties");
	xgi::bind(this, &hyperdaq::Application::debugSOAP, "debugSOAP");
	xgi::bind(this, &hyperdaq::Application::processInformation, "processInformation");

	xgi::bind(this, &hyperdaq::Application::logPage, "logPage");
	xgi::bind(this, &hyperdaq::Application::configureLoggers, "configureLoggers");
	xgi::bind(this, &hyperdaq::Application::logContentPage, "logContentPage");
	xgi::bind(this, &hyperdaq::Application::getLogFile, "getLogFile");

	xgi::bind(this, &hyperdaq::Application::memoryPolicies, "memoryPolicies");
	xgi::bind(this, &hyperdaq::Application::cpuPolicies, "cpuPolicies");

	xgi::bind(this, &hyperdaq::Application::exit, "exit");

	xgi::bind(this, &hyperdaq::Application::parameterQuery, "parameterQuery");
	xgi::bind(this, &hyperdaq::Application::infospaceQuery, "infospaceQuery");

	 xgi::bind(this, &hyperdaq::Application::workLoopList, "workLoopList");

	// DEV AREA
	//xgi::bind(this, &hyperdaq::Application::devArea, "devArea");
	//xgi::framework::deferredbind(this, this, &hyperdaq::Application::devArea, "devArea");

	getApplicationInfoSpace()->fireItemAvailable("showSidebar", &(layout_.showSidebar_));

	// ID, URL, Title, Image URL
	controlPanelApps_.push_back(ControlPanelApplication("explore-cluster", "exploreCluster", "Explore Cluster", "/hyperdaq/images/framework/controlpanel/cluster-explore.png"));
	controlPanelApps_.push_back(ControlPanelApplication("view-applications", "viewApplications", "View Applications", "/hyperdaq/images/framework/controlpanel/view-applications.png"));
	controlPanelApps_.push_back(ControlPanelApplication("application-groups", "applicationGroups", "Application Groups", "/hyperdaq/images/framework/controlpanel/application-groups.png"));
	controlPanelApps_.push_back(ControlPanelApplication("endpoints", "viewEndpoints", "Endpoints", "/hyperdaq/images/framework/controlpanel/endpoints.png"));
	controlPanelApps_.push_back(ControlPanelApplication("configure-cluster", "configureCluster", "Configure Cluster", "/hyperdaq/images/framework/controlpanel/configure-cluster.png"));
	controlPanelApps_.push_back(ControlPanelApplication("control-cluster", "controlCluster", "Control Cluster", "/hyperdaq/images/framework/controlpanel/control-cluster.png"));
	controlPanelApps_.push_back(ControlPanelApplication("upload-application", "uploadApplicationForm", "Upload Application", "/hyperdaq/images/framework/controlpanel/upload-application.png"));
	controlPanelApps_.push_back(ControlPanelApplication("debug-soap", "debugSOAP", "Debug SOAP", "/hyperdaq/images/framework/controlpanel/debug-soap.png"));
	controlPanelApps_.push_back(ControlPanelApplication("log", "logPage", "Log Information", "/hyperdaq/images/framework/controlpanel/logging-information.png"));
	controlPanelApps_.push_back(ControlPanelApplication("process-information", "processInformation", "Process Information", "/hyperdaq/images/framework/controlpanel/process-information.png"));
	controlPanelApps_.push_back(ControlPanelApplication("display-modules", "displayModules", "Display Modules", "/hyperdaq/images/framework/controlpanel/display-modules.png"));
	controlPanelApps_.push_back(ControlPanelApplication("cpu-policies", "cpuPolicies", "CPU Policies", "/hyperdaq/images/framework/controlpanel/cpu-policies.png"));
	controlPanelApps_.push_back(ControlPanelApplication("memory-policies", "memoryPolicies", "Memory Policies", "/hyperdaq/images/framework/controlpanel/memory-policies.png"));
	controlPanelApps_.push_back(ControlPanelApplication("exit", "exit", "Exit Process", "/hyperdaq/images/framework/controlpanel/exit.png"));

	// DEV AREA
	//controlPanelApps_.push_back(ControlPanelApplication("dev-area", "devArea", "Dev Area", "/hyperdaq/images/framework/controlpanel/development-area.png"));

	xgi::framework::UIManager::setLayout (&layout_);
}

std::vector<struct hyperdaq::Application::ControlPanelApplication> hyperdaq::Application::getControlPanelApplications ()
{
	return controlPanelApps_;
}

void hyperdaq::Application::getHyperDAQHeader (xgi::Input * in, xgi::Output * out, std::string appName) throw (xgi::exception::Exception)
{
	for (std::vector<ControlPanelApplication>::iterator i = controlPanelApps_.begin(); i != controlPanelApps_.end(); i++)
	{
		if ((*i).id == appName)
		{
			layout_.currentAppIcon_ = (*i).img;
			layout_.currentURL_ = (*i).url;
			break;
		}
	}

	layout_.getHTMLHeaderTop(this, in, out);

	std::string rooturl = "/urn:xdaq-application:service=hyperdaq";

	std::string curURL = in->getenv("PATH_TRANSLATED");

	for (std::vector<ControlPanelApplication>::iterator i = controlPanelApps_.begin(); i != controlPanelApps_.end(); i++)
	{
		std::string linkURL = rooturl + "/" + (*i).url;

		if ((*i).url == "exit")
		{
			*out << "    	<li><a href=\"" << linkURL << "\" xdaq-confirm><img src=\"" << (*i).img << "\" />" << (*i).title << "</a></li>" << std::endl;
		}
		else
		{
			if (curURL == linkURL)
			{
				*out << "    	<li class=\"xdaq-sidebar-active-url\"><a href=\"" << linkURL << "\"><img src=\"" << (*i).img << "\" />" << (*i).title << "</a></li>" << std::endl;
			}
			else
			{
				*out << "    	<li><a href=\"" << linkURL << "\"><img src=\"" << (*i).img << "\" />" << (*i).title << "</a></li>" << std::endl;
			}
		}
	}

	layout_.getHTMLHeaderBottom(this, in, out);

	*out << "	<link href=\"/hyperdaq/html/css/xdaq-controlpanel.css\" rel=\"stylesheet\" type=\"text/css\" />" << std::endl;
}

void hyperdaq::Application::getHyperDAQFooter (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	layout_.currentAppIcon_ = "";
	layout_.currentURL_ = "";
	this->getHTMLFooter(in, out);
}

void hyperdaq::Application::Default (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	*out << "    	<div class=\"xdaq-hyperdaq-home-widget-search\">" << std::endl;
	*out << "    		Sort : <select id=\"xdaq-hyperdaq-home-sort\">" << std::endl;
	*out << "    		  <option value=\"default\">default (lid)</option>" << std::endl;
	*out << "    		  <option value=\"alphabetically\">alphabetically</option>" << std::endl;
	*out << "    		  <option value=\"service\">service</option>" << std::endl;
	*out << "    		  <option value=\"network\">network</option>" << std::endl;
	*out << "    		  </select>" << std::endl;
	*out << "    	</div>" << std::endl;

	*out << "    	<br />" << std::endl;

	*out << "    	<div id=\"xdaq-hyperdaq-home-widget-wrapper\">" << std::endl;

	size_t index = 0;

	std::list<xdaq::Application*> applications = this->getApplicationContext()->getApplicationRegistry()->getApplications();
	for (std::list<xdaq::Application*>::iterator i = applications.begin(); i != applications.end(); i++)
	{
		const xdaq::ApplicationDescriptor* d = (*i)->getApplicationDescriptor();

		std::string icon = d->getAttribute("icon");
		if (icon == "") // use default icon
		{
			icon = layout_.defaultIcon_;
		}

		std::stringstream linkURL;
		linkURL << "<a href=\"/" << d->getURN() << "\">";

		std::string::size_type pos = d->getClassName().rfind("::Application");
		std::string title = d->getClassName().substr(0, pos);
		pos = 0;

		// Replace "::" namespace delimiters with spaces " "
		while ((pos = title.find("::", pos)) != std::string::npos)
		{
			title.replace(pos, 2, " ");
		}

		std::string service = d->getAttribute("service");
		if (service == "")
		{
			service = "none";
		}
		service = "Service: " + service;

		*out << "<div class=\"xdaq-hyperdaq-home-widget-container\" data-lid=\"" << d->getLocalId() << "\" data-id=\"" << index << "\" data-name=\"" << d->getClassName() << "\" data-service=\"" << d->getAttribute("service") << "\" data-network=\"" << d->getAttribute("network") << "\">" << std::endl;
		*out << "    			" << linkURL.str() << "<img src=\"" << icon << "\" class=\"xdaq-hyperdaq-home-widget-image\" alt=\"XDAQ Application Icon\" /></a>" << std::endl;
		*out << "    			<div class=\"xdaq-hyperdaq-home-widget-text\">" << linkURL.str() << "" << std::endl;
		/*
		 *out << "    				" << title << "<br />" 																						<< std::endl;
		 *out << "    				<br />" 																									<< std::endl;
		 *out << "    				" << d->getURN() << "<br />" 																				<< std::endl;
		 *out << "    				<br />" 																									<< std::endl;
		 *out << "    				" 																											<< std::endl;
		 */
		*out << "    				<span style=\"font-weight: 600;\">" << title << "</span><br />" << std::endl;
		*out << "    				" << service << "<br />" << std::endl;
		*out << "    				Network: " << d->getAttribute("network") << "<br />" << std::endl;
		*out << "    				" << d->getURN() << "<br />" << std::endl;
		*out << "    				<br />" << std::endl;

		*out << "    			</a>" << std::endl;
		*out << "    			</div>" << std::endl;
		*out << "    		</div>";

		index++;
	}

	*out << "    	</div>" << std::endl;
}

void hyperdaq::Application::configureCluster (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	this->getHyperDAQHeader(in, out, "configure-cluster");

	*out << cgicc::h3("Configure Cluster") << std::endl;

	std::string url = "/";
	url += getApplicationDescriptor()->getURN();
	url += "/configureClusterResult";

	*out << cgicc::fieldset() << std::endl;

	*out << cgicc::form().set("method", "post").set("action", url).set("enctype", "multipart/form-data").set("class", "xdaq-form") << std::endl;

	*out << cgicc::legend("Configuration Data") << std::endl;

	*out << cgicc::table().set("class", "xdaq-table-nohover") << std::endl;
	*out << cgicc::tbody() << std::endl;

	*out << cgicc::tr() << std::endl;
	*out << cgicc::td("XML file") << std::endl;
	*out << cgicc::td(cgicc::input().set("type", "file").set("name", "XMLConfigurationFile").set("accept", "text/*")) << std::endl;
	*out << cgicc::tr() << std::endl;

	*out << cgicc::tr() << std::endl;
	*out << cgicc::td("Configure Cluster") << std::endl;
	*out << cgicc::td(cgicc::input().set("type", "checkbox").set("name", "forward")) << std::endl;
	*out << cgicc::tr() << std::endl;

	*out << cgicc::tr() << std::endl;
	*out << cgicc::td("") << std::endl;
	*out << cgicc::td(cgicc::input().set("type", "submit").set("name", "submit").set("value", "Submit").set("class", "xdaq-controlpanel-button")) << std::endl;
	*out << cgicc::tr() << std::endl;

	*out << cgicc::tbody() << std::endl;
	*out << cgicc::table() << std::endl;

	*out << cgicc::form() << std::endl;
	*out << cgicc::fieldset() << std::endl;

	this->getHyperDAQFooter(in, out);
}

void hyperdaq::Application::configureClusterResult (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	this->getHyperDAQHeader(in, out, "configure-cluster");

	*out << cgicc::h3("Configure Executive Result") << std::endl;

	try
	{
		cgicc::Cgicc cgi(in);

		*out << cgicc::h2("File Uploaded via Configure page") << std::endl;

		bool forward = cgi.queryCheckbox("forward");

		/* cgicc::const_form_iterator contextIdElement = cgi.getElement("ContextIdentifier");
		 std::string contextId = (*contextIdElement).getValue();


		 *out << "Context id is: " << contextId << cgicc::br() << std::endl;
		 */

		cgicc::const_file_iterator file;
		file = cgi.getFile("XMLConfigurationFile");

		if (file != cgi.getFiles().end())
		{
			*out << "Name: " << (*file).getName() << cgicc::br() << std::endl;
			*out << "Filetype: " << (*file).getDataType() << cgicc::br() << std::endl;
			*out << "Filename: " << (*file).getFilename() << cgicc::br() << std::endl;
			*out << "Size: " << (*file).getDataLength() << cgicc::br() << std::endl;

			// call the configuration
			DOMDocument* doc = 0;
			try
			{
				std::string data = (*file).getData();

				xoap::DOMParser* p = xoap::getDOMParserFactory()->get("configure");
				doc = p->parse(data);

				if (forward)
				{
					this->forwardConfiguration(doc);
					*out << cgicc::h2("Configured through XRelay") << cgicc::p() << std::endl;
				}
				else
				{
					executive::Application* e = dynamic_cast<executive::Application*>(getApplicationContext()->getApplicationRegistry()->getApplication(0));
					e->Configure(doc->getDocumentElement());
					*out << cgicc::h2("Configured local executive") << cgicc::p() << std::endl;
				}

				this->getHyperDAQFooter(in, out);

				doc->release();

				return;
			}
			catch (xdaq::exception::Exception& ce)
			{
				*out << cgicc::h2("Configuration failure") << cgicc::br() << std::endl;
				*out << cgicc::pre() << std::endl;
				*out << xcept::stdformat_exception_history(ce) << std::endl;
				*out << cgicc::pre() << std::endl;
				this->getHyperDAQFooter(in, out);
				if (doc != 0) doc->release();
				return;
			}
			catch (xoap::exception::Exception& se)
			{
				*out << cgicc::h2("Configuration failure") << cgicc::br() << std::endl;
				*out << "Error parsing configuration file:" << cgicc::br() << std::endl;
				*out << cgicc::pre() << std::endl;
				*out << se.what() << std::endl;
				*out << cgicc::pre() << std::endl;
				this->getHyperDAQFooter(in, out);
				if (doc != 0) doc->release();
				return;
			}
		}
		else
		{
			*out << cgicc::p() << std::endl;
			*out << "No file was uploaded." << std::endl << cgicc::p() << std::endl;
			this->getHyperDAQFooter(in, out);
			return;
		}

		/*
		 To write the contents of the file to a file "foo" on disk:

		 ofstream foo("foo");
		 (*file).writeToStream(foo);
		 */
	}
	catch (const std::exception& e)
	{
		*out << "Exception: " << e.what() << std::endl;
		this->getHyperDAQFooter(in, out);
		return;
	}

	this->getHyperDAQFooter(in, out);
}

void hyperdaq::Application::exploreCluster (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	this->getHyperDAQHeader(in, out, "explore-cluster");

	*out << cgicc::h3("Explore Cluster") << std::endl;

	*out << "<div>" << std::endl;

	std::vector<xdaq::ContextDescriptor *> contexts = getApplicationContext()->getContextTable()->getContextDescriptors();

	for (std::vector<xdaq::ContextDescriptor *>::size_type i = 0; i < contexts.size(); i++)
	{
		std::string url = toolbox::toString("%s/urn:xdaq-application:lid=%d", contexts[i]->getURL().c_str(), getApplicationDescriptor()->getLocalId());

		std::string displayStringHost = contexts[i]->getURL().substr(7);
		std::string displayStringPort = "";

		std::size_t found = displayStringHost.find(":");
		if (found != std::string::npos)
		{
			displayStringPort = displayStringHost.substr(found + 1);
			displayStringHost = displayStringHost.substr(0, found);
		}

		*out << "<div class=\"xdaq-hyperdaq-home-widget-container\" >" << std::endl;
		*out << "    			<a href=\"" << url << "\"><img src=\"/hyperdaq/images/framework/xdaq-host.png\" class=\"xdaq-hyperdaq-home-widget-image\" alt=\"XDAQ Cluster Host\" /></a>" << std::endl;
		*out << "    			<div class=\"xdaq-hyperdaq-home-widget-text\">" << "<a href=\"" << url << "\">" << std::endl;

		*out << "    				<span style=\"font-weight: 600;\">" << displayStringHost << "</span><br />" << std::endl;
		*out << "    				" << displayStringPort << "<br />" << std::endl;

		*out << "    			</a>" << std::endl;
		*out << "    			</div>" << std::endl;
		*out << "    		</div>";
	}

	*out << "</div>";

	this->getHyperDAQFooter(in, out);
}

void hyperdaq::Application::displayApplicationGroups (xgi::Output* out)
{
	layout_.currentAppIcon_ = "/hyperdaq/images/framework/controlpanel/application-groups.png";
	*out << cgicc::h3("Application Groups") << std::endl;

	*out << cgicc::table().set("class", "xdaq-table");

	*out << cgicc::thead();

	*out << cgicc::tr();
	*out << cgicc::th("Zone");
	*out << cgicc::th("Group");
	*out << cgicc::th("Number of Applications");
	*out << cgicc::tr() << std::endl;
	*out << cgicc::thead();

	*out << cgicc::tbody();

	std::set<xdaq::ApplicationDescriptor*> merged;
	std::set < std::string > zones = this->getApplicationContext()->getZoneNames();
	for (std::set<std::string>::iterator j = zones.begin(); j != zones.end(); j++)
	{
		std::set<const xdaq::ApplicationGroup *> groups = getApplicationContext()->getZone(*j)->getGroups();
		for (std::set<const xdaq::ApplicationGroup *>::iterator i = groups.begin(); i != groups.end(); i++)
		{
			std::string group = (*i)->getName();
			*out << cgicc::tr();
			std::string grouplink = "<a href=\"";
			grouplink += toolbox::toString("/urn:xdaq-application:service=hyperdaq/viewGroup?zone=%s&group=%s", (*j).c_str(), group.c_str());
			grouplink += "\">";
			grouplink += group;
			grouplink += "</a>";
			*out << cgicc::td(*j);
			*out << cgicc::td(grouplink);

			std::set<const xdaq::ApplicationDescriptor*> applications = (*i)->getApplicationDescriptors();
			*out << cgicc::td(toolbox::toString("%d", applications.size()));
			*out << cgicc::tr() << std::endl;

		}
	}
	*out << cgicc::tbody();
	*out << cgicc::table();
}

void hyperdaq::Application::displayApplicationTable (xgi::Output* out, std::set<const xdaq::ApplicationDescriptor*> & apps)
{
	layout_.currentAppIcon_ = "/hyperdaq/images/framework/controlpanel/view-applications.png";
	*out << cgicc::h3("Applications") << std::endl;

	*out << cgicc::table().set("class", "xdaq-table");

	*out << cgicc::thead();

	*out << cgicc::tr();
	*out << cgicc::th("Application");
	*out << cgicc::th("Instance");
	*out << cgicc::th("ID");
	*out << cgicc::th("Groups");
	*out << cgicc::th("Context");
	*out << cgicc::th("Properties");
	*out << cgicc::tr() << std::endl;

	*out << cgicc::thead();

	*out << cgicc::tbody();

	for (std::set<const xdaq::ApplicationDescriptor*>::iterator i = apps.begin(); i != apps.end(); i++)
	{
		const xdaq::ApplicationDescriptor* d = *i;

		*out << cgicc::tr();
		std::string applicationlink = "<a href=\"";
		applicationlink += d->getContextDescriptor()->getURL();
		applicationlink += toolbox::toString("/urn:xdaq-application:lid=%d", d->getLocalId());
		applicationlink += "\">";
		applicationlink += d->getClassName();
		applicationlink += "</a>";

		*out << cgicc::td(applicationlink);

		if (d->hasInstanceNumber())
		{
			*out << cgicc::td(toolbox::toString("%d", d->getInstance()));
		}
		else
		{
			*out << cgicc::td(toolbox::toString("%s", "-"));
		}

		*out << cgicc::td(toolbox::toString("%d", d->getLocalId()));

		// List groups the application belongs
		std::string groupList = "";
		//--
		std::set<xdaq::ApplicationDescriptor*> merged;
		std::set < std::string > zones = this->getApplicationContext()->getZoneNames();
		for (std::set<std::string>::iterator j = zones.begin(); j != zones.end(); j++)
		{
			std::set<const xdaq::ApplicationGroup *> groups = getApplicationContext()->getZone(*j)->getGroups();
			for (std::set<const xdaq::ApplicationGroup *>::iterator i = groups.begin(); i != groups.end(); i++)
			{
				if ((*i)->hasApplicationDescriptor(d))
				{
					if (groupList != "") groupList += ",";
					groupList += (*j) + ".";
					groupList += (*i)->getName();
				}
			}

		}

		*out << cgicc::td(groupList);

		// Prepare a hyperlink to the context in which the application resides
		std::string url = d->getContextDescriptor()->getURL();
		std::stringstream link;
		link << "<a href=\"" << url << "\">" << url << "</a>";
		*out << cgicc::td(link.str());

		std::stringstream editlink;
		editlink << "<a href=\"" << url << "/urn:xdaq-application:service=hyperdaq/parameterQuery?lid=" << toolbox::toString("%d", d->getLocalId()) << "\" target=\"_blank\">XML</a> / ";
		// Assume that HyperDAQ always service=hyperdaq
		editlink << "<a href=\"" << url << "/urn:xdaq-application:service=hyperdaq/editProperties?lid=" << toolbox::toString("%d", d->getLocalId()) << "\">Edit</a>";
		*out << cgicc::td(editlink.str());

		*out << cgicc::tr() << std::endl;
	}

	*out << cgicc::tbody();
	*out << cgicc::table();
}

void hyperdaq::Application::displayModules (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	this->getHyperDAQHeader(in, out, "display-modules");

	*out << cgicc::h3("Library Path Environment Variable") << std::endl;

	std::string url = "/";
	url += getApplicationDescriptor()->getURN();

	*out << "Name of library path variable is " << LIBRARY_PATH_VARIABLE << cgicc::p();

	std::string ldpath = std::getenv(LIBRARY_PATH_VARIABLE);
	if (ldpath == "")
	{
		*out << "Variable is empty" << std::endl;
	}
	else
	{
		*out << cgicc::table().set("class", "xdaq-table-nohover");
		*out << cgicc::tbody();

		size_t first = 0;
		size_t next = 0;
		while (next != std::string::npos)
		{
			*out << cgicc::tr();
			next = ldpath.find(':', first + 1);
			*out << cgicc::td(ldpath.substr(first + 1, next - first - 1));
			first = next;
			*out << cgicc::tr();
		}

		*out << cgicc::tbody();
		*out << cgicc::table();
	}

	*out << "<br />" << std::endl;
	*out << cgicc::h3("List of Modules") << std::endl;

	const xdaq::SharedObjectRegistry* sor = getApplicationContext()->getSharedObjectRegistry();
	std::vector < std::string > ov = sor->getObjectNames();

	*out << cgicc::table().set("class", "xdaq-table");

	*out << cgicc::thead();

	// Write the header line: Class, Instance, Id, Host, Properties
	*out << cgicc::tr();
	*out << cgicc::th("Entry");
	*out << cgicc::th("Module");
	*out << cgicc::th("Info");
	*out << cgicc::tr() << std::endl;

	*out << cgicc::thead();
	*out << cgicc::tbody();

	for (std::vector<std::string>::size_type i = 0; i < ov.size(); i++)
	{
		*out << cgicc::tr();
		*out << cgicc::td() << i << cgicc::td();
		*out << cgicc::td(ov[i]);
		std::string packageInfoLink = "<a href=\"";
		packageInfoLink += url + "/displayPackageInfo?module=";
		packageInfoLink += ov[i];
		packageInfoLink += "\">View<a>";
		*out << cgicc::td(packageInfoLink);
	}

	*out << cgicc::tbody();
	*out << cgicc::table();

	this->getHyperDAQFooter(in, out);
}

void hyperdaq::Application::displayPackageInfo (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	this->getHyperDAQHeader(in, out, "display-modules");

	*out << cgicc::h3("Package Info") << std::endl;

	cgicc::Cgicc cgi(in);

	//Returns the desired element, or an empty string if not found
	std::string module = cgi("module");

	const xdaq::SharedObjectRegistry* sor = getApplicationContext()->getSharedObjectRegistry();

	try
	{
		config::PackageInfo pi = sor->getPackageInfo(module);

		*out << cgicc::table().set("class", "xdaq-table-vertical");

		*out << cgicc::tbody();

		*out << cgicc::tr();
		*out << cgicc::th("Name").set("style", "font-weight:bold;") << cgicc::td(module) << std::endl;
		*out << cgicc::tr();

		*out << cgicc::tr();
		*out << cgicc::th("Version").set("style", "font-weight:bold;") << cgicc::td(pi.getLatestVersion()) << std::endl;
		*out << cgicc::tr();

		*out << cgicc::tr();
		*out << cgicc::th("Compile date").set("style", "font-weight:bold;") << cgicc::td(pi.getDate()) << std::endl;
		*out << cgicc::tr();

		*out << cgicc::tr();
		*out << cgicc::th("Compile time").set("style", "font-weight:bold;") << cgicc::td(pi.getTime()) << std::endl;
		*out << cgicc::tr();

		*out << cgicc::tr();
		*out << cgicc::th("Link").set("style", "font-weight:bold;") << "<td><a href=\"" << pi.getLink() << "\" target=\"_blank\">go</a></td>";
		*out << cgicc::tr();

		*out << cgicc::tr();
		*out << cgicc::th("Summary").set("style", "font-weight:bold;") << cgicc::td(pi.getSummary()) << std::endl;
		*out << cgicc::tr();

		*out << cgicc::tr();
		*out << cgicc::th("Description").set("style", "font-weight:bold;") << cgicc::td(pi.getDescription()) << std::endl;
		*out << cgicc::tr();

		*out << cgicc::tr();
		*out << cgicc::th("Authors").set("style", "font-weight:bold;") << cgicc::td(pi.getAuthors()) << std::endl;
		*out << cgicc::tr();

		*out << cgicc::tbody();

		*out << cgicc::table();

		*out << "<br /><a href=\"javascript:history.back()\">Go Back</a>" << std::endl;
	}
	catch (xdaq::exception::SymbolLookupFailed& slf)
	{
		/*
		 std::string failure = "No package information for module<br>";
		 failure += module;
		 std::string reason = "<font size=2>";
		 reason += "To learn about adding package information to a module ";
		 reason += "follow this <a href=\"http://cern.ch/xdaq/doc/3/toolbox/PackageInfo.html\" target=\"_blank\">link</a>.<p>";
		 reason += "If you think, this page has been displayed because of ";
		 reason += "a processing error, you may have a look at the exception trace below.<p>";
		 reason += "</font><font face=\"Courier New\" size=\"2\">";
		 reason += xcept::stdformat_exception_history(slf);
		 reason += "</font>";
		 *out << xgi::Utils::getFailurePage(failure, reason);
		 */

		///////////
		*out << "No package information available<br /><br />" << std::endl;

		*out << cgicc::table().set("class", "xdaq-table-vertical");

		*out << cgicc::tbody();

		*out << cgicc::tr();
		*out << cgicc::th("Name").set("style", "font-weight:bold;") << cgicc::td(module) << std::endl;
		*out << cgicc::tr();

		*out << cgicc::tbody();

		*out << cgicc::table();

		*out << "<br />" << std::endl;
		*out << "To learn about adding package information to a module follow this <a href=\"http://cern.ch/xdaq/doc/3/toolbox/PackageInfo.html\" target=\"_blank\">link</a><br /><br /><a href=\"javascript:history.back()\">Go Back</a>" << std::endl;

	}
	catch (config::PackageInfo::VersionException& ve)
	{
		std::string failure = "Version mismatch for package<br>";
		failure += module;
		*out << xgi::Utils::getFailurePage(failure, ve.what());
	}

	this->getHyperDAQFooter(in, out);
}

void hyperdaq::Application::debugSOAP (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	this->getHyperDAQHeader(in, out, "debug-soap");

	*out << cgicc::h3("SOAP Client") << std::endl;

	// input text area
	*out << "<textarea style=\"width: 800px; height: 320px;\" id=\"soapmsg\">" << std::endl;
	this->getSOAPMessage(out, "helloWorld");
	*out << "</textarea>" << std::endl;
	*out << "<br /><br />" << std::endl;

	// response text area
	*out << "<textarea id=\"soaprespdiv\" style=\"width: 800px; height: 320px;\"></textarea><br /><br />" << std::endl;

	// url to send soap message to
	*out << "<input type=\"text\" style=\"width: 800px;\" id=\"soapurl\" value=\"" << getApplicationContext()->getContextDescriptor()->getURL() << "/urn:xdaq-application:lid=#\">" << std::endl;

	// submit button
	*out << "<div><br /><input type=\"button\" class=\"xdaq-submit-button xdaq-controlpanel-button\" onclick=\"xdaqSendSoap()\" value=\"Send SOAP\" id=\"submitbut\"></div>" << std::endl;

	*out << "	<script type=\"text/javascript\" src=\"/hyperdaq/html/js/xdaq-controlpanel-debug-soap.js\"></script>                   " << std::endl;

	this->getHyperDAQFooter(in, out);
}

void hyperdaq::Application::controlCluster (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	this->getHyperDAQHeader(in, out, "control-cluster");

	*out << cgicc::h3("Control Cluster") << std::endl;

	*out << cgicc::form() << std::endl;

	*out << "	        Select where: <select id=\"xdaq-cc-col-selector\">			" << std::endl;
	*out << "	           <option value=\"class\">class</option>				            " << std::endl;
	*out << "	           <option value=\"instance\">instance</option>		                " << std::endl;
	*out << "	           <option value=\"id\">id</option>		                        " << std::endl;
	*out << "	           <option value=\"context\">context</option>		                    " << std::endl;
	*out << "	          </select>														" << std::endl;

	*out << "	        <select id=\"xdaq-cc-regex-modifier-selector\">							" << std::endl;
	*out << "	           <option value=\"starts\">starts with</option>				        " << std::endl;
	*out << "	           <option value=\"contains\">contains</option>		                " << std::endl;
	*out << "	           <option value=\"ends\">ends with</option>		                " << std::endl;
	*out << "	          </select>														" << std::endl;

	*out << "	          <input type=\"text\" id=\"xdaq-cc-regex-selector\" placeholder=\"\"/><br /><br />	" << std::endl;

	////////////////////
	/////

	*out << "<table class=\"xdaq-table\" id=\"xdaq-cc-table\" data-url=\"" << this->getApplicationDescriptor()->getContextDescriptor()->getURL() << "\">" << std::endl;
	*out << "	<thead>" << std::endl;
	*out << "		<tr>" << std::endl;
	*out << "			<th>Class</th>" << std::endl;
	*out << "			<th>Instance</th>" << std::endl;
	*out << "			<th>ID</th>" << std::endl;
	*out << "			<th>Context</th>" << std::endl;
	*out << "			<th>Selection</th>" << std::endl;
	*out << "		</tr>" << std::endl;
	*out << "	</thead>" << std::endl;

	*out << "	<tbody>" << std::endl;

	std::set<const xdaq::ApplicationDescriptor*> apps;

	std::set < std::string > zones = this->getApplicationContext()->getZoneNames();
	for (std::set<std::string>::iterator i = zones.begin(); i != zones.end(); i++)
	{
		std::set<const xdaq::ApplicationGroup *> groups = this->getApplicationContext()->getZone(*i)->getGroups();

		for (std::set<const xdaq::ApplicationGroup *>::iterator j = groups.begin(); j != groups.end(); j++)
		{
			std::set<const xdaq::ApplicationDescriptor*> descriptors = (*j)->getApplicationDescriptors();
			apps.insert(descriptors.begin(), descriptors.end());
		}
	}
	for (std::set<const xdaq::ApplicationDescriptor*>::iterator i = apps.begin(); i != apps.end(); i++)
	{
		const xdaq::ApplicationDescriptor* d = *i;

		std::string soapURL = d->getContextDescriptor()->getURL();

		std::stringstream soapURN;
		soapURN << "urn:xdaq-application:lid=" << toolbox::toString("%d", d->getLocalId());

		std::stringstream xhttpURL;
		xhttpURL << soapURL << "/" << soapURN;

		std::stringstream applicationlink;
		applicationlink << "<a href=\"" << xhttpURL.str() << "\">" << d->getClassName() << "</a>";

		*out << "<tr>" << std::endl;

		*out << "	<td data-cc-col=\"class\" data-cc-val=\"" << d->getClassName() << "\">" << applicationlink.str() << "</td>";

		if (d->hasInstanceNumber())
		{
			*out << "	<td data-cc-col=\"instance\" data-cc-val=\"" << toolbox::toString("%d", d->getInstance()) << "\">" << toolbox::toString("%d", d->getInstance()) << "</td>";
		}
		else
		{
			*out << "	<td data-cc-col=\"instance\" data-cc-val=\"" << toolbox::toString("%s", "-") << "\">" << toolbox::toString("%s", "-") << "</td>";
		}

		*out << "	<td data-cc-col=\"id\" data-cc-val=\"" << toolbox::toString("%d", d->getLocalId()) << "\">" << toolbox::toString("%d", d->getLocalId()) << "</td>";

		// Prepare a hyperlink to the context in which the application resides
		std::stringstream link;
		link << "	<a href=\"" << soapURL << "\">" << soapURL << "</a>";
		*out << "	<td data-cc-col=\"context\" data-cc-val=\"" << soapURL << "\">" << link.str() << "</td>";

		*out << "	<td><input type=\"checkbox\" data-cc-url=\"" << soapURL << "\" data-cc-urn=\"" << soapURN.str() << "\"/></td>";

		*out << "</tr>" << std::endl;
	}

	*out << "</tbody>" << std::endl;
	*out << "</table>" << std::endl;

	////////////////////
	/////
	/////
	////////////////////

	*out << "<br />" << std::endl;

	*out << cgicc::h4("SOAP Message <input type=\"reset\" value=\"Reset\" id=\"xdaq-cc-reset\" class=\"xdaq-controlpanel-button\"/> |  <input type=\"button\" class=\"xdaq-submit-button xdaq-controlpanel-button\" value=\"Post\" id=\"xdaq-cc-submit\" />") << std::endl;

	*out << "<textarea style=\"width: 100%; height: 340px; \" id=\"xdaq-cc-soapmsg\">" << std::endl;
	this->getRelaySOAPMessage(out, "COMMAND");
	*out << "</textarea>" << std::endl;

	/*
	 *out << "<br /><br />" << std::endl;

	 *out << cgicc::h4("SOAP Reply <input type=\"button\" class=\"xdaq-reset-button xdaq-controlpanel-button\" value=\"Clear\" id=\"xdaq-cc-clear-reply\" />").set("style", "margin-bottom: 10px;") << std::endl;

	 *out << "				<textarea style=\"width: 100%; height: 180px;\" id=\"xdaq-cc-soapreply\"></textarea>" << std::endl;
	 */
	*out << cgicc::form() << std::endl;

	*out << "	<script type=\"text/javascript\" src=\"/hyperdaq/html/js/xdaq-controlpanel-cluster-control.js\"></script>                   " << std::endl;

	this->getHyperDAQFooter(in, out);
}

void hyperdaq::Application::editProperties (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	this->getHyperDAQHeader(in, out, "view-applications");

	cgicc::Cgicc cgi(in);
	size_t lid;

	cgicc::form_iterator lidIt = cgi.getElement("lid");
	if ( lidIt != cgi.getElements().end() )
	{
		lid = lidIt->getIntegerValue(0);
	}
	else
	{
		XCEPT_RAISE(xgi::exception::Exception, "Missing lid!");
	}

	std::stringstream requestURL;
	requestURL << this->getApplicationDescriptor()->getContextDescriptor()->getURL() << "/urn:xdaq-application:lid=" << lid;

	std::stringstream soapAction;
	soapAction << "urn:xdaq-application:lid=" << lid;

	const xdaq::ApplicationDescriptor * app = 0;

	try
	{
		app = this->getApplicationContext()->getApplicationRegistry()->getApplication(lid)->getApplicationDescriptor();
	}
	catch (xdaq::exception::ApplicationNotFound& e)
	{
		XCEPT_DECLARE_NESTED(xgi::exception::Exception, ex, "Failed to find application to view/edit properties", e);
		out->str("");
		out->clear();
		this->errorPage(in, out, ex);
		return;
	}

	std::stringstream headerText;
	//headerText << "Parameter Viewer - " << geturl.str();
	headerText << "Parameter Viewer - " << app->getClassName();
	*out << cgicc::h3(headerText.str()) << std::endl;

	std::stringstream tableID;
	tableID << "xdaqparamviewer-" << app->getClassName();

	*out << cgicc::table().set("class", "xdaq-table-tree xdaq-param-viewer").set("id", tableID.str())
	// .set("data-allow-refresh", "false")
	.set("data-tablesize", "large").set("data-requesturl", requestURL.str()).set("data-soapaction", soapAction.str()).set("data-urn", app->getClassName()) << std::endl;

	*out << cgicc::thead() << std::endl;

	// Write the header line: Class, Instance, Id, Host, Properties
	*out << cgicc::tr() << std::endl;
	*out << cgicc::th("Name").set("style", "min-width: 200px;") << std::endl;
	*out << cgicc::th("<input type=\"checkbox\">") << std::endl;
	*out << cgicc::th("Type").set("style", "min-width: 100px;") << std::endl;

	*out << cgicc::th("Value").set("style", "min-width: 200px;") << std::endl;
	*out << cgicc::tr() << std::endl;

	*out << cgicc::thead() << std::endl;
	*out << cgicc::tbody() << std::endl;

	// Table is populated dynamically with a jQuery script that runs on document.load

	*out << cgicc::tbody() << std::endl;
	*out << cgicc::tfoot() << std::endl;
	*out << cgicc::tr() << std::endl;
	*out << cgicc::td("").set("colspan", "3") << std::endl;

	std::stringstream pvbuttons;
	pvbuttons << "	<button id=\"xdaqparamviewer-get\" class=\"xdaq-reset-button xdaqparamviewer-get xdaq-controlpanel-button\" data-linkedtable=\"" << tableID.str() << "\">Get</button> | ";
	pvbuttons << "	<button id=\"xdaqparamviewer-set\" class=\"xdaq-submit-button xdaq-controlpanel-button\" data-linkedtable=\"" << tableID.str() << "\">Set</button>";

	*out << cgicc::td(pvbuttons.str()) << std::endl;
	*out << cgicc::tr() << std::endl;
	*out << cgicc::tfoot() << std::endl;
	*out << cgicc::table() << std::endl;

	*out << "<br/><br/><div id=\"devtestarea\"></div>" << std::endl;

	*out << "	<script type=\"text/javascript\" src=\"/hyperdaq/html/js/xdaq-controlpanel-edit-properties.js\"></script>                   	" << std::endl;

	this->getHyperDAQFooter(in, out);
}

void hyperdaq::Application::exit (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	LOG4CPLUS_INFO(this->getApplicationLogger(), "Exiting process with code 10");
	toolbox::getRuntime()->halt(10); // upon exit code 10 automatically restart process

	*out << "<!doctype html>" << std::endl;
	*out << "<html xmlns=\"http://www.w3.org/1999/xhtml\">" << std::endl;
	*out << "<head>" << std::endl;
	*out << "       <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />" << std::endl;
	*out << "       <title>XDAQ Process Terminated...</title>" << std::endl;

	*out << "<style>                         " << std::endl;
	*out << "	body{                        " << std::endl;
	*out << "		background: black;       " << std::endl;
	*out << "		overflow: hidden;        " << std::endl;
	*out << "	}                            " << std::endl;

	*out << "	div#centerDiv{               " << std::endl;
	*out << "		position:absolute;       " << std::endl;
	*out << "		height:100%;             " << std::endl;
	*out << "		top: 0%;                 " << std::endl;
	*out << "		width:100%;              " << std::endl;
	*out << "		background: white;       " << std::endl;
	*out << "		z-index:1;               " << std::endl;
	*out << "	}                            " << std::endl;

	*out << "	div#centerDiv img{           " << std::endl;
	*out << "		display: inline;         " << std::endl;
	*out << "		margin-left: 50%;        " << std::endl;
	*out << "		margin-right: 50%;       " << std::endl;
	*out << "		position: absolute;      " << std::endl;
	*out << "		left: -238px;            " << std::endl;
	*out << "		top: 50%;                " << std::endl;
	*out << "		margin-top: -128px;      " << std::endl;
	*out << "		height:256px;            " << std::endl;
	*out << "		width: 476px;            " << std::endl;
	*out << "	}                            " << std::endl;
	*out << "</style>                        " << std::endl;

	*out << "</head>" << std::endl;
	*out << "<body>" << std::endl;
	*out << "<div id=\"centerDiv\">" << std::endl;
	*out << "   <img id=\"image\" src=\"/xdaq/images/xdaq-logo.png\" />" << std::endl;
	*out << "</div>" << std::endl;

	*out << "<script type=\"text/javascript\" src=\"http://code.jquery.com/jquery-2.1.0.min.js\"></script>" << std::endl;
	*out << "<script>" << std::endl;
	*out << "	$('div#centerDiv').animate({" << std::endl;
	*out << "		height: \"1px\"," << std::endl;
	*out << "		top: \"50%\"," << std::endl;
	*out << "		opacity: 1" << std::endl;
	*out << "	}, 2000, function(){" << std::endl;
	*out << "   		$('div#centerDiv').animate({" << std::endl;
	*out << "      			width: \"0%\"," << std::endl;
	*out << "     			left: \"50%\"" << std::endl;
	*out << "  		}, 2000, function(){" << std::endl;
	*out << "      				$('div#centerDiv').css({display: \"none\"})" << std::endl;
	*out << "  				}" << std::endl;
	*out << "			);" << std::endl;
	*out << " 		}" << std::endl;
	*out << "	);" << std::endl;
	*out << "</script>" << std::endl;

	*out << "</body>" << std::endl;
	*out << "</html>" << std::endl;
}

void hyperdaq::Application::forwardConfiguration (DOMDocument* doc)
{
	const xdaq::ApplicationDescriptor* xrelayApplication = 0;
	try
	{
		xdaq::Application* xa = getApplicationContext()->getFirstApplication("XRelay");
		xrelayApplication = xa->getApplicationDescriptor();
	}
	catch (xdaq::exception::Exception& e)
	{
		LOG4CPLUS_ERROR(getApplicationLogger(), xcept::stdformat_exception_history(e));
		return;
	}

	xoap::MessageReference msg = xoap::createMessage();
	xoap::SOAPEnvelope envelope = msg->getSOAPPart().getEnvelope();
	xoap::SOAPName envelopeName = envelope.getElementName();
	xoap::SOAPBody body = envelope.getBody();

	// add SOAP header
	xoap::SOAPHeader header = envelope.addHeader();
	xoap::SOAPName fanoutName = envelope.createName("relay", "xr", "http://xdaq.web.cern.ch/xdaq/xsd/2004/XRelay-10");
	xoap::SOAPHeaderElement fanoutElement = header.addHeaderElement(fanoutName);

	// Add the actor attribute
	xoap::SOAPName actorName = envelope.createName("actor", envelopeName.getPrefix(), envelopeName.getURI());
	fanoutElement.addAttribute(actorName, "http://xdaq.web.cern.ch/xdaq/xsd/2004/XRelay-10");

	// Add the notify attribute
	xoap::SOAPName notifyName = envelope.createName("notify");
	fanoutElement.addAttribute(notifyName, "true");

	// Add the Configure command to the SOAP message body
	xoap::SOAPName commandName = envelope.createName("Configure", "xdaq", "urn:xdaq-soap:3.0");
	xoap::SOAPElement command = body.addBodyElement(commandName);

	// import and append the configuration to <Configure> node
	DOMNode* commandDOMNode = command.getDOM();
	DOMNode* importedCommandDOMNode = commandDOMNode->getOwnerDocument()->importNode(doc->getDocumentElement(), true);
	commandDOMNode->appendChild(importedCommandDOMNode);

	xoap::SOAPName toName = envelope.createName("to", "xr", "http://xdaq.web.cern.ch/xdaq/xsd/2004/XRelay-10");
	xoap::SOAPName urlName = envelope.createName("url");
	xoap::SOAPName urnName = envelope.createName("urn");

	//extract <Context/> tag from command
	DOMNodeList* contextList = doc->getElementsByTagNameNS(xoap::XStr("*"), xoap::XStr("Context"));

	for (XMLSize_t i = 0; i < contextList->getLength(); i++)
	{
		DOMNode* contextNode = contextList->item(i);
		std::string url = xoap::getNodeAttribute(contextNode, "url");

		xoap::SOAPElement toElement = fanoutElement.addChildElement(toName);

		toElement.addAttribute(urlName, url);
		toElement.addAttribute(urnName, "urn:xdaq-application:service=executive"); // always the Executive URN

	}

	// Add a <from> tag for routing the notification to this host
	xoap::SOAPName fromName = envelope.createName("from", "xr", "http://xdaq.web.cern.ch/xdaq/xsd/2004/XRelay-10");
	xoap::SOAPElement fromElement = fanoutElement.addChildElement(fromName);
	xoap::SOAPName notifyToName = envelope.createName("to", "xr", "http://xdaq.web.cern.ch/xdaq/xsd/2004/XRelay-10");
	xoap::SOAPName notifyUrlName = envelope.createName("url");
	xoap::SOAPName notifyUrnName = envelope.createName("urn");
	xoap::SOAPElement notifyToElement = fromElement.addChildElement(notifyToName);
	notifyToElement.addAttribute(notifyUrlName, getApplicationContext()->getContextDescriptor()->getURL());
	notifyToElement.addAttribute(notifyUrnName, "urn:xdaq-application:service=xrelay"); // always the XRelay URN

	try
	{
		// send to xrelay application
		const xdaq::ApplicationDescriptor * from = this->getApplicationDescriptor();
		this->getApplicationContext()->postSOAP(msg, *from, *xrelayApplication);
	}
	catch (xdaq::exception::Exception& ce)
	{
		LOG4CPLUS_ERROR(this->getApplicationLogger(), "Failed to forward configuration");
		XCEPT_RETHROW(xdaq::exception::Exception, "Failed to forward configuration", ce);
	}

}

std::string hyperdaq::Application::getProcessLimit (int resource)
{
	struct rlimit rlim;

	if (getrlimit(resource, &rlim))
	{
		return "(unknown)";
	}
	else if (rlim.rlim_cur == RLIM_INFINITY)
	{
		return "infinite";
	}
	else
	{
		std::stringstream ss;
		ss << rlim.rlim_cur;
		return ss.str();
	}
}

void hyperdaq::Application::processInformation (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	this->getHyperDAQHeader(in, out, "process-information");

	*out << cgicc::h3("Process Information") << std::endl;

	std::string url = "/";
	url += getApplicationDescriptor()->getURN();

	*out << "<div class=\"xdaq-tab-wrapper\" id=\"processInfoTabPanel\">" << std::endl;

	// General Process Information
	*out << "<div class=\"xdaq-tab\" title=\"General\">" << std::endl;

	*out << cgicc::table().set("class", "xdaq-table");

	*out << cgicc::thead();
	*out << cgicc::tr();
	*out << cgicc::th("Name").set("class", "xdaq-sortable");
	*out << cgicc::th("Value");
	*out << cgicc::tr() << std::endl;
	*out << cgicc::thead();

	*out << cgicc::tbody();

	*out << cgicc::tr();
	*out << cgicc::td("start time");
	*out << cgicc::td(layout_.xdaqStartTime_);
	*out << cgicc::tr();

	std::stringstream pid;
	pid << toolbox::getRuntime()->getPid();
	*out << cgicc::tr();
	*out << cgicc::td("process identifier");
	*out << cgicc::td(pid.str());
	*out << cgicc::tr();

	*out << cgicc::tr();
	*out << cgicc::td("compiler version string");
	*out << cgicc::td(__VERSION__);
	*out << cgicc::tr();

	*out << cgicc::tr();
	*out << cgicc::td("argv");
	char **argv = dynamic_cast<xdaq::ApplicationContextImpl*>(getApplicationContext())->argv();
	int argc = dynamic_cast<xdaq::ApplicationContextImpl*>(getApplicationContext())->argc();
	std::stringstream comline;
	for (int i = 0; i < argc; i++)
	{
		comline << argv[i] << " ";
	}
	*out << cgicc::td(comline.str());
	*out << cgicc::tr();

	*out << cgicc::tbody();
	*out << cgicc::table();

	*out << "</div>";

	// Environmental Variables
	*out << "<div class=\"xdaq-tab\" title=\"Environment\">" << std::endl;

	*out << cgicc::table().set("class", "xdaq-table");

	*out << cgicc::thead();
	*out << cgicc::tr();
	*out << cgicc::th("Name").set("class", "xdaq-sortable");
	*out << cgicc::th("Value");
	*out << cgicc::tr() << std::endl;
	*out << cgicc::thead();

	*out << cgicc::tbody();

	char **env;
	for (env = environ; *env != NULL; ++env)
	{
		toolbox::StringTokenizer st(*env, "=");
		if (st.hasMoreTokens())
		{
			*out << cgicc::tr();
			*out << cgicc::td(st.nextToken());
			*out << cgicc::td(st.remainingString());
			*out << cgicc::tr();
		}
	}

	*out << cgicc::tbody();
	*out << cgicc::table();

	*out << "</div>";

	// Process limits
	*out << "<div class=\"xdaq-tab\" title=\"Limits\">" << std::endl;

	*out << cgicc::table().set("class", "xdaq-table");

	*out << cgicc::thead();
	*out << cgicc::tr();
	*out << cgicc::th("Resource").set("class", "xdaq-sortable");
	*out << cgicc::th("Limit");
	*out << cgicc::tr() << std::endl;
	*out << cgicc::thead();

	*out << cgicc::tbody();

	*out << cgicc::tr();
	*out << cgicc::td("core file size");
	*out << cgicc::td(getProcessLimit(RLIMIT_CORE));
	*out << cgicc::tr();

	*out << cgicc::tr();
	*out << cgicc::td("data seg size");
	*out << cgicc::td(getProcessLimit(RLIMIT_DATA));
	*out << cgicc::tr();

	*out << cgicc::tr();
	*out << cgicc::td("file size");
	*out << cgicc::td(getProcessLimit(RLIMIT_FSIZE));
	*out << cgicc::tr();

	*out << cgicc::tr();
	*out << cgicc::td("pending signals");
#if __APPLE__
	*out << cgicc::td("undefined");
#else
	*out << cgicc::td(getProcessLimit(RLIMIT_SIGPENDING));
#endif
	*out << cgicc::tr();

	*out << cgicc::tr();
	*out << cgicc::td("max locked memory");
	*out << cgicc::td(getProcessLimit(RLIMIT_MEMLOCK));
	*out << cgicc::tr();

	*out << cgicc::tr();
	*out << cgicc::td("open files");
	*out << cgicc::td(getProcessLimit(RLIMIT_NOFILE));
	*out << cgicc::tr();

	/*
	 *out << cgicc::tr();
	 *out << cgicc::td("pipe size");
	 *out << cgicc::td(getProcessLimit(0));
	 *out << cgicc::tr();
	 */

	*out << cgicc::tr();
	*out << cgicc::td("POSIX message queues");
#if __APPLE__
	*out << cgicc::td("undefined");
#else
	*out << cgicc::td(getProcessLimit(RLIMIT_MSGQUEUE));
#endif
	*out << cgicc::tr();

	*out << cgicc::tr();
	*out << cgicc::td("stack size");
	*out << cgicc::td(getProcessLimit(RLIMIT_STACK));
	*out << cgicc::tr();

	*out << cgicc::tr();
	*out << cgicc::td("cpu time");
	*out << cgicc::td(getProcessLimit(RLIMIT_CPU));
	*out << cgicc::tr();

	*out << cgicc::tr();
	*out << cgicc::td("max user processes");
	*out << cgicc::td(getProcessLimit(RLIMIT_NPROC));
	*out << cgicc::tr();

	/*
	 *out << cgicc::tr();
	 *out << cgicc::td("virtual memory");
	 *out << cgicc::td(getProcessLimit(0));
	 *out << cgicc::tr();
	 */

	*out << cgicc::tr();
	*out << cgicc::td("file locks");
#ifdef  RLIMIT_LOCKS
	*out << cgicc::td(getProcessLimit(RLIMIT_LOCKS));
#else
	*out << cgicc::td("unknown");
#endif
	*out << cgicc::tr();

	*out << cgicc::tbody();
	*out << cgicc::table();

	*out << "</div>";

	*out << "</div>";

	this->getHyperDAQFooter(in, out);
}

void hyperdaq::Application::getSOAPMessage (xgi::Output * out, std::string command) throw (xgi::exception::Exception)
{
	*out << "<SOAP-ENV:Envelope SOAP-ENV:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\" xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:SOAP-ENC=\"http://schemas.xmlsoap.org/soap/encoding/\">" << std::endl;
	*out << "	<SOAP-ENV:Header>" << std::endl;
	*out << "	</SOAP-ENV:Header>" << std::endl;
	*out << "	<SOAP-ENV:Body>" << std::endl;
	*out << "		<xdaq:" << command << " xmlns:xdaq=\"urn:xdaq-soap:3.0\" />" << std::endl;
	//*out << "		</xdaq:" << command << ">" << std::endl;
	*out << "	</SOAP-ENV:Body>" << std::endl;
	*out << "</SOAP-ENV:Envelope>";
}

void hyperdaq::Application::getRelaySOAPMessage (xgi::Output * out, std::string command) throw (xgi::exception::Exception)
{
	*out << "<SOAP-ENV:Envelope SOAP-ENV:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\" xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:SOAP-ENC=\"http://schemas.xmlsoap.org/soap/encoding/\">" << std::endl;
	*out << "	<SOAP-ENV:Header>" << std::endl;
	*out << "	<xr:relay SOAP-ENV:actor=\"http://xdaq.web.cern.ch/xdaq/xsd/2004/XRelay-10\" xmlns:xr=\"http://xdaq.web.cern.ch/xdaq/xsd/2004/XRelay-10\">" << std::endl;
	*out << "	</xr:relay>" << std::endl;
	*out << "	</SOAP-ENV:Header>" << std::endl;
	*out << "	<SOAP-ENV:Body>" << std::endl;
	*out << "		<xdaq:" << command << " xmlns:xdaq=\"urn:xdaq-soap:3.0\" />" << std::endl;
	//*out << "		</xdaq:" << command << ">" << std::endl;
	*out << "	</SOAP-ENV:Body>" << std::endl;
	*out << "</SOAP-ENV:Envelope>";
}

void hyperdaq::Application::cpuPolicies (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	this->getHyperDAQHeader(in, out, "cpu-policies");

	*out << cgicc::h3("CPU Policies") << std::endl;
	this->getPolicyHTML("thread", out);
	this->getHyperDAQFooter(in, out);
}

void hyperdaq::Application::memoryPolicies (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	this->getHyperDAQHeader(in, out, "memory-policies");

	*out << cgicc::h3("Memory Policies") << std::endl;
	this->getPolicyHTML("alloc", out);
	this->getHyperDAQFooter(in, out);
}

bool hyperdaq::Application::isDefault (std::list<toolbox::Policy*>::iterator policy)
{
	if ((*policy)->getType() == "thread")
	{
		return (*policy)->getPackage() == "posix" && (*policy)->getProperty("affinity") == "*";
	}

	return (*policy)->getPackage() == "posix";
}

xgi::Output* hyperdaq::Application::getPolicyHTML (const std::string & policyType, xgi::Output * out)
{
	*out << cgicc::table().set("width", "100%").set("class", "xdaq-table");

	*out << cgicc::thead();

	*out << cgicc::tr();
	*out << cgicc::th("Element");
	*out << cgicc::th("Package");
	*out << cgicc::th("Type");
	*out << cgicc::th("Properties");
	*out << cgicc::th("Matches");
	*out << cgicc::th("TIDs");
	*out << cgicc::th("#Matches");
	*out << cgicc::tr();
	*out << cgicc::thead();
	*out << cgicc::tbody();

	toolbox::PolicyFactory* factory = toolbox::getPolicyFactory();

	std::list<toolbox::Policy*> policies = factory->getPolicies(policyType);
	std::stringstream ss;
	for (std::list<toolbox::Policy*>::iterator i = policies.begin(); i != policies.end(); i++)
	{

		std::vector<std::string> pnames = (*i)->propertyNames();
		std::stringstream o;

		o << cgicc::tr();
		o << cgicc::td((*i)->getProperty("pattern"));
		o << cgicc::td((*i)->getPackage());
		o << cgicc::td((*i)->getType());
		//o << cgicc::td((*i)->toString());
		o << cgicc::td();
		for (std::vector<std::string>::iterator k = pnames.begin(); k != pnames.end(); k++ )
                {
                        if ( (*k).find("urn:toolbox-task-workloop-SYS_gettid") == std::string::npos )
                        {
				o << (*k)  << "=" << (*i)->getProperty(*k) << "<br/>";	
			}
		}

 		o << cgicc::td();

		o << cgicc::td();

		for (std::list<std::string>::iterator j = (*i)->users_.begin(); j != (*i)->users_.end(); j++)
		{
			o << (*j) << "<br />";
		}

		o << cgicc::td();

		// tids
		o << cgicc::td();
		for (std::vector<std::string>::iterator k = pnames.begin(); k != pnames.end(); k++ )
		{
			if ( (*k).find("urn:toolbox-task-workloop-SYS_gettid") != std::string::npos )
			{
				//toolbox::net::URN tidurn (*k);
				 o << (*k) << "=" << (*i)->getProperty(*k) << "<br/>";
			}
		}
		o << cgicc::td();

		o << cgicc::td(toolbox::toString("%d", (*i)->counter_));


		o << cgicc::tr();

		if (this->isDefault(i))
		{
			ss << o.str();
		}
		else
		{
			*out << o.str();
		}
	}

	*out << cgicc::tbody();
	*out << cgicc::table();
	*out << cgicc::br();

	//default policies

	*out << "<h2 style=\"font-size: 110%\">Default</h2>" << std::endl;

	*out << cgicc::table().set("width", "100%").set("class", "xdaq-table");

	*out << cgicc::thead();

	*out << cgicc::tr();
	*out << cgicc::th("Element");
	*out << cgicc::th("Package");
	*out << cgicc::th("Type");
	*out << cgicc::th("Properties");
	*out << cgicc::th("Matches");
	*out << cgicc::th("TIDs");
	*out << cgicc::th("# Matches");
	*out << cgicc::tr();
	*out << cgicc::thead();
	*out << cgicc::tbody();

	*out << ss.str();

	*out << cgicc::tbody();
	*out << cgicc::table();

	return out;
}

void hyperdaq::Application::configureLoggers (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	cgicc::Cgicc cgi(in);

	// Root logger
	LogLevel newLogLevel = -2;
	if (xgi::Utils::hasFormElement(cgi, Logger::getRoot().getName()))
	{
		newLogLevel = xgi::Utils::getFormElement(cgi, Logger::getRoot().getName())->getIntegerValue();
		if ((newLogLevel != Logger::getRoot().getLogLevel()) && (newLogLevel != NOT_SET_LOG_LEVEL))
		{
			Logger::getRoot().setLogLevel(newLogLevel);
			std::stringstream ss;
			ss << "root log level set to " << getLogLevelManager().toString(newLogLevel);
			LOG4CPLUS_INFO(this->getApplicationLogger(), ss.str());
		}
		else if (newLogLevel == NOT_SET_LOG_LEVEL)
		{
			std::stringstream ss;
			ss << "Cannot set log level to " << newLogLevel;
			LOG4CPLUS_INFO(this->getApplicationLogger(), ss.str());
		}
	}

	// Other loggers
	LoggerList loggerList = Logger::getRoot().getCurrentLoggers();
	for (LoggerList::iterator ll = loggerList.begin(); ll != loggerList.end(); ++ll)
	{
		if (xgi::Utils::hasFormElement(cgi, (*ll).getName()))
		{
			newLogLevel = xgi::Utils::getFormElement(cgi, (*ll).getName())->getIntegerValue();
			if ((newLogLevel != (*ll).getLogLevel()) && (newLogLevel != NOT_SET_LOG_LEVEL))
			{
				(*ll).setLogLevel(newLogLevel);
				std::stringstream ss;
				ss << (*ll).getName() << " log level set to " << getLogLevelManager().toString(newLogLevel);
				LOG4CPLUS_INFO(this->getApplicationLogger(), ss.str());
			}
			else if (newLogLevel == NOT_SET_LOG_LEVEL)
			{
				std::stringstream ss;
				ss << "Cannot set log level to " << newLogLevel;
				LOG4CPLUS_INFO(this->getApplicationLogger(), ss.str());
			}
		}
	}

	// forward to the log page
	// this->logPage(in, out);

	out->getHTTPResponseHeader().addHeader("Content-Type", "text/plain");
}

void hyperdaq::Application::logPage (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	this->getHyperDAQHeader(in, out, "log");

	*out << cgicc::h3("Logging") << std::endl;

	std::stringstream baseurl;
	baseurl << this->getApplicationDescriptor()->getContextDescriptor()->getURL() << "/" << this->getApplicationDescriptor()->getURN();
	*out << cgicc::form().set("method", "POST").set("action", baseurl.str() + "/configureLoggers").set("class", "xdaq-form") << std::endl;

	*out << cgicc::table().set("class", "xdaq-table").set("style", "width: 100%; vertical-align:middle;") << std::endl;
	*out << cgicc::thead() << std::endl;
	*out << cgicc::tr() << std::endl;
	*out << cgicc::th("Logger") << std::endl;
	*out << cgicc::th("Level") << std::endl;
	*out << cgicc::tr() << std::endl;
	*out << cgicc::thead() << std::endl;

	*out << cgicc::tbody() << std::endl;

	// Create table with all loggers
	//

	std::vector < LogLevel > logLevels(8);
	logLevels[0] = NOT_SET_LOG_LEVEL;
	logLevels[1] = TRACE_LOG_LEVEL;
	logLevels[2] = DEBUG_LOG_LEVEL;
	logLevels[3] = INFO_LOG_LEVEL;
	logLevels[4] = WARN_LOG_LEVEL;
	logLevels[5] = ERROR_LOG_LEVEL;
	logLevels[6] = FATAL_LOG_LEVEL;
	logLevels[7] = OFF_LOG_LEVEL;

	*out << cgicc::tr() << std::endl;

	*out << cgicc::td(Logger::getRoot().getName()) << std::endl;

	*out << "<td>";
	*out << "<select name=\"" << Logger::getRoot().getName() << "\" id=\"" << Logger::getRoot().getName() << "\" onchange=\"selectorChanged()\" class=\"xdaqLogController-Selector\" style=\"width:100px\">";
	for (std::vector<LogLevel>::iterator itr = logLevels.begin(); itr != logLevels.end(); itr++)
	{
		if (Logger::getRoot().getLogLevel() == (*itr))
		{
			*out << "<option value=\"" << *itr << "\" selected>" << getLogLevelManager().toString(*itr) << "</option>";
		}
		else
		{
			*out << "<option value=\"" << *itr << "\">" << getLogLevelManager().toString(*itr) << "</option>";
		}
	}
	*out << "</select>";
	*out << "</td>";

	*out << cgicc::tr() << std::endl;

	LoggerList loggerList = Logger::getRoot().getCurrentLoggers();

	for (LoggerList::iterator ll = loggerList.begin(); ll != loggerList.end(); ++ll)
	{
		*out << cgicc::tr() << std::endl;

		*out << cgicc::td((*ll).getName()) << std::endl;

		*out << "<td>";
		*out << "<select name=\"" << (*ll).getName() << "\" id=\"" << (*ll).getName() << "\" onchange=\"selectorChanged()\" class=\"xdaqLogController-Selector\" style=\"width:100px\">";
		for (std::vector<LogLevel>::iterator itr = logLevels.begin(); itr != logLevels.end(); itr++)
		{
			if ((*ll).getLogLevel() == (*itr))
			{
				*out << "<option value=\"" << *itr << "\" selected>" << getLogLevelManager().toString(*itr) << "</option>";
			}
			else
			{
				*out << "<option value=\"" << *itr << "\">" << getLogLevelManager().toString(*itr) << "</option>";
			}
		}
		*out << "</select>";
		*out << "</td>";

		*out << cgicc::tr() << std::endl;
	}

	*out << cgicc::tbody() << std::endl;

	*out << cgicc::tfoot() << std::endl;
	*out << cgicc::tr() << std::endl;
	*out << cgicc::td("") << std::endl;

	*out << cgicc::td("<input type=\"button\" class=\"xdaq-submit-button xdaq-controlpanel-button\" style=\"width:100%;\" onclick=\"configureLoggers()\" value=\"Submit\" id=\"logconfigsubmitbut\">") << std::endl;
	*out << cgicc::tr() << std::endl;
	*out << cgicc::tfoot() << std::endl;

	*out << cgicc::table() << std::endl;

	*out << cgicc::form() << std::endl;

	*out << "<br />" << std::endl;

	// List all log appenders
	*out << cgicc::table().set("class", "xdaq-table").set("style", "width: 100%;") << std::endl;
	*out << cgicc::thead() << std::endl;
	*out << cgicc::tr() << std::endl;
	*out << cgicc::th("Appender") << std::endl;
	*out << cgicc::th("Level") << std::endl;
	*out << cgicc::th("Output") << std::endl;
	*out << cgicc::tr() << std::endl;
	*out << cgicc::thead() << std::endl;

	*out << cgicc::tbody() << std::endl;

	SharedAppenderPtrList appenderList = Logger::getRoot().getAllAppenders();
	for (SharedAppenderPtrList::iterator al = appenderList.begin(); al != appenderList.end(); ++al)
	{
		*out << cgicc::tr() << std::endl;

		*out << cgicc::td((*al)->getName()) << std::endl;

		*out << cgicc::td(getLogLevelManager().toString((*al)->getThreshold())) << std::endl;

		// Output log url
		//
		if ((*al)->getName().find("file") == 0)
		{
			std::stringstream link;
			link << "<a href=\"logContentPage?url=" << cgicc::form_urlencode((*al)->getName());
			link << "&tail=1000\">view</a>";
			*out << cgicc::td(link.str()) << std::endl;
		}
		else
		{
			*out << cgicc::td("n/a") << std::endl;
		}

		*out << cgicc::tr() << std::endl;
	}

	*out << cgicc::tbody() << std::endl;
	*out << cgicc::table() << std::endl;

	*out << "	<script type=\"text/javascript\" src=\"/hyperdaq/html/js/xdaq-controlpanel-log-controller.js\"></script>                   	" << std::endl;

	this->getHyperDAQFooter(in, out);
}

void hyperdaq::Application::logContentPage (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	this->getHyperDAQHeader(in, out, "log");

	std::stringstream to;
	to << this->getApplicationDescriptor()->getContextDescriptor()->getURL() << "/" << this->getApplicationDescriptor()->getURN();

	std::string url = "";
	try
	{
		cgicc::Cgicc cgi(in);
		cgicc::const_form_iterator fi = cgi.getElement("url");

		if (fi != cgi.getElements().end())
		{
			url = (*fi).getValue();
		}

		if (url.find("file") != 0)
		{
			std::stringstream msg;
			msg << "Unsupported URL '" << cgicc::form_urldecode(url) << "' for viewing log";
			XCEPT_RAISE(xgi::exception::Exception, msg.str());
		}
	}
	catch (const std::exception & e)
	{
		XCEPT_RAISE(xgi::exception::Exception, e.what());
	}

	std::stringstream headerTitle;
	headerTitle << "Log Viewer - <input type=\"text\" style=\"width: 500px;\"id=\"xdaq-log-viewer-file\" value=\"" << url << "\">";
	*out << cgicc::h3(headerTitle.str()) << std::endl;

	*out << "<div id=\"xdaq-logviewer-wrapper\" data-callback=\"" << to.str() << "\">" << std::endl;

	*out << "Lines : <input type=\"text\" id=\"xdaq-log-viewer-length\" value=\"5000\" onChange=\"len=Number(this.value)\">" << std::endl;
	*out << "<input class=\"xdaq-controlpanel-button\" type=\"button\" value=\"All\" onClick=\"viewAll()\">" << std::endl;
	*out << "<input class=\"xdaq-controlpanel-button\" type=\"button\" value=\"Head\" onClick=\"viewHead()\">" << std::endl;
	*out << "<input class=\"xdaq-controlpanel-button\" type=\"button\" value=\"Tail\" onClick=\"viewTail()\">" << std::endl;
	*out << "<input class=\"xdaq-controlpanel-button\" type=\"button\" value=\"Forward\" onClick=\"viewForward()\">" << std::endl;
	*out << "<input class=\"xdaq-controlpanel-button\" type=\"button\" value=\"Backward\" onClick=\"viewBackward()\">" << std::endl;

	*out << "<br /><br />" << std::endl;

	*out << "<textarea id=\"xdaq-logviewer-textarea\" style=\"width: 100%; height: 600px;\"></textarea>" << std::endl;

	*out << "</div>";

	*out << "<script type=\"text/javascript\" src=\"/hyperdaq/html/js/xdaq-controlpanel-log-viewer.js\"></script>" << std::endl;

	this->getHyperDAQFooter(in, out);
}

void hyperdaq::Application::getLogFile (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	// If the log went into a file, it can be displayed
	// parameters: url in encoded format, e.g. file:/path
	//             tail=size .... from the end of the file 'size' bytes
	//             head=size .... from the beginning of the file 'size' bytes
	//             from=X&len=Y.. from position X of file, Y bytes
	// if the request reads beyond the file size or limits, only the data up to the end of the file are returned

	try
	{
		cgicc::Cgicc cgi(in);
		cgicc::const_form_iterator fi = cgi.getElement("url");
		std::string url = "";
		if (fi != cgi.getElements().end())
		{
			url = (*fi).getValue();
		}

		if (url.find("file") != 0)
		{
			std::stringstream msg;
			msg << "Cannot display log file. Unsupported URL: '" << cgicc::form_urldecode(url) << "'.";
			XCEPT_RAISE(xgi::exception::Exception, msg.str());
		}

		toolbox::net::URL decoded(cgicc::form_urldecode(url));
		std::ifstream logfile(decoded.getPath().c_str());
		if (!logfile.is_open())
		{
			std::stringstream msg;
			msg << "Failed to open file '" << decoded.getPath() << "' for viewing log";
			XCEPT_RAISE(xgi::exception::Exception, msg.str());
		}

		ssize_t len = -1;
		cgicc::const_form_iterator taili = cgi.getElement("tail");
		cgicc::const_form_iterator headi = cgi.getElement("head");
		cgicc::const_form_iterator fromi = cgi.getElement("from");
		if (taili != cgi.getElements().end())
		{
			logfile.seekg(-toolbox::toLong((*taili).getValue()), std::ios::end);
			if (logfile.rdstate() != std::ios::goodbit)
			{
				/*
				 if(logfile.rdstate() == std::ios::eofbit)
				 *out << "End of file!" << std::endl;
				 if(logfile.rdstate() == std::ios::badbit)
				 *out << "Fatal I/O error!" << std::endl;
				 if(logfile.rdstate() == std::ios::failbit)
				 *out << "Non-fatal I/O error!" << std::endl;
				 */
				// Failed to navigate backwards, most commonly because tried to go too far back, so read out whole file
				logfile.clear();
				logfile.seekg(0);
			}
		}
		else if (headi != cgi.getElements().end())
		{
			len = toolbox::toLong((*headi).getValue());
		}
		else if (fromi != cgi.getElements().end())
		{
			logfile.seekg(toolbox::toUnsignedLong((*fromi).getValue()), std::ios::beg);
			cgicc::const_form_iterator leni = cgi.getElement("len");
			if (leni != cgi.getElements().end())
			{
				len = toolbox::toLong((*leni).getValue());
			}
			else
			{
				len = 1000;
			}
		}

		char c;

		logfile.tie(0);
		logfile.unsetf(std::ios::skipws);

		while (logfile >> c)
		{
			*out << c;
			--len;
			if (len == 0) break;
		}

		logfile.close();

		out->getHTTPResponseHeader().addHeader("Content-Type", "text/plain");
	}
	catch (const std::exception & e)
	{
		XCEPT_RAISE(xgi::exception::Exception, e.what());
	}
}

/*
 * Upload Application
 */
void hyperdaq::Application::uploadApplicationForm (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	this->getHyperDAQHeader(in, out, "upload-application");

	std::string url = "/";
	url += getApplicationDescriptor()->getURN();

	url += "/uploadApplication";

	*out << cgicc::h3("Upload Application") << std::endl;

	*out << cgicc::form().set("method", "POST").set("action", url).set("class", "xdaq-form") << std::endl;

	*out << cgicc::table().set("class", "xdaq-table-nohover") << std::endl;
	*out << cgicc::tbody() << std::endl;

	*out << cgicc::tr() << std::endl;
	*out << cgicc::td(cgicc::label("Class Name ")) << std::endl;
	*out << cgicc::td(cgicc::input().set("type", "text").set("name", "ClassName").set("size", "30").set("value", "")) << std::endl;
	*out << cgicc::tr() << std::endl;

	*out << cgicc::tr() << std::endl;
	*out << cgicc::td(cgicc::label("Local Target Identifier ")) << std::endl;
	*out << cgicc::td(cgicc::input().set("type", "text").set("name", "LocalTargetIdentifier").set("size", "4").set("value", "0")) << std::endl;
	*out << cgicc::tr() << std::endl;

	*out << cgicc::tr() << std::endl;
	*out << cgicc::td(cgicc::label("Instance Number (0-n) ")) << std::endl;
	*out << "<td>" << cgicc::input().set("type", "text").set("name", "InstanceNumber").set("size", "4").set("value", "0") << " &nbsp; " << cgicc::input().set("type", "checkbox").set("name", "hasInstance") << "</td>" << std::endl;
	*out << cgicc::tr() << std::endl;

	*out << cgicc::tr() << std::endl;
	*out << cgicc::td(cgicc::label("Group List (e.g. g1,g2,...) ")) << std::endl;
	*out << cgicc::td(cgicc::input().set("type", "text").set("name", "GroupList").set("size", "80").set("value", "")) << std::endl;
	*out << cgicc::tr() << std::endl;

	*out << cgicc::tr() << std::endl;
	*out << cgicc::td(cgicc::label("Library Paths or Urls (e.g. p1,p2,...)")) << std::endl;
	*out << cgicc::td(cgicc::input().set("type", "text").set("name", "LibraryPath").set("size", "80").set("value", "")) << std::endl;
	*out << cgicc::tr() << std::endl;

	*out << cgicc::tr() << std::endl;
	*out << cgicc::td("") << std::endl;
	*out << cgicc::td(cgicc::input().set("type", "submit").set("name", "submit").set("value", "Submit").set("class", "xdaq-controlpanel-button")) << std::endl;
	*out << cgicc::tr() << std::endl;

	*out << cgicc::tbody() << std::endl;
	*out << cgicc::table() << std::endl;
	*out << cgicc::form() << std::endl;

	this->getHyperDAQFooter(in, out);
}

void hyperdaq::Application::uploadApplication (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	std::string url = "/";
	url += getApplicationDescriptor()->getURN();

	url += "/uploadApplication";

	cgicc::Cgicc cgi(in);

	//ClassName
	cgicc::form_iterator classNameIt = cgi.getElement("ClassName");
	std::string className;
	if ( classNameIt != cgi.getElements().end() )
	{
		className = classNameIt->getValue();
	}
	else
	{
		XCEPT_RAISE(xgi::exception::Exception, "Missing ClassName element");
	}

	//LocalTargetIdentifier
	cgicc::form_iterator localTargetIdentifierIt = cgi.getElement("LocalTargetIdentifier");
	unsigned long localTargetIdentifier;
	if ( localTargetIdentifierIt != cgi.getElements().end() )
	{
		localTargetIdentifier = localTargetIdentifierIt->getIntegerValue(0);
	}
	else
	{
		XCEPT_RAISE(xgi::exception::Exception, "Missing LocalTargetIdentifier element");
	}

	//LibraryPath
	cgicc::form_iterator libraryPathIt = cgi.getElement("LibraryPath");
	std::string libraryPath;
	if ( libraryPathIt != cgi.getElements().end() )
	{
		libraryPath = libraryPathIt->getValue();
	}
	else
	{
		XCEPT_RAISE(xgi::exception::Exception, "Missing LibraryPath element");
	}

	//InstanceNumber
	unsigned long instanceNumber = 0;
	bool hasInstance = false;
	if ( cgi.getElement("hasInstance") != cgi.getElements().end() )
	{
		hasInstance = cgi.queryCheckbox("hasInstance");
		if (hasInstance)
		{
			cgicc::form_iterator instanceNumberIt = cgi.getElement("InstanceNumber");
			if ( instanceNumberIt != cgi.getElements().end() )
			{
				instanceNumber = instanceNumberIt->getIntegerValue(0);
			}
			else
			{
				XCEPT_RAISE(xgi::exception::Exception, "Missing InstanceNumber element");
			}
		}
	}

	//GroupList
	cgicc::form_iterator groupListIt = cgi.getElement("GroupList");
	std::string groupList;
	if ( groupListIt != cgi.getElements().end() )
	{
		groupList = groupListIt->getValue();
	}
	else
	{
		XCEPT_RAISE(xgi::exception::Exception, "Missing GroupList element");
	}

	std::list < std::string > libs = toolbox::parseTokenList(libraryPath, ",");

	for (std::list<std::string>::iterator i = libs.begin(); i != libs.end(); i++)
	{
		try
		{
			std::cout << "Loading " << (*i) << std::endl;
			dynamic_cast<xdaq::ApplicationContextImpl*>(getApplicationContext())->loadModule(*i);
		}
		catch (xdaq::exception::Exception& xe)
		{
			XCEPT_RETHROW(xgi::exception::Exception, "Failed to load module", xe);
		}
	}

	try
	{
		std::set < std::string > allowedZones = xdaq::ApplicationDescriptorFactory::getInstance()->getZoneNames();
		std::set < std::string > groups;
		toolbox::StringTokenizer strtok(groupList, ",");
		while (strtok.hasMoreTokens())
		{
			std::string g = toolbox::trim(strtok.nextToken());
			groups.insert(g);
		}
		xdaq::ApplicationDescriptor * d = xdaq::ApplicationDescriptorFactory::getInstance()->createApplicationDescriptor(getApplicationContext()->getContextDescriptor(), className, localTargetIdentifier, allowedZones, groups);
		xdaq::ApplicationRegistry * registry = const_cast<xdaq::ApplicationRegistry *>(getApplicationContext()->getApplicationRegistry());
		xdaq::Application * appl = registry->instantiateApplication(d);
		if (hasInstance)
		{
			((xdaq::ApplicationDescriptorImpl*) d)->setInstance(instanceNumber);
		}

		xdaq::InstantiateApplicationEvent instantiateEvent(d, this);
		dynamic_cast<xdaq::ApplicationContextImpl*>(this->getApplicationContext())->fireEvent(instantiateEvent);

		// fake the set default parameters event
		LOG4CPLUS_INFO(this->getApplicationLogger(), "Setting default parameters for " << className);

		xdata::Event e("urn:xdaq-event:setDefaultValues", this);
		appl->getApplicationInfoSpace()->fireEvent(e);
	}
	catch (xdaq::exception::DuplicateTid& ex)
	{
		XCEPT_RETHROW(xgi::exception::Exception, "Failed to instantiate application", ex);
	}
	catch (xdaq::exception::LoadFailed& ex)
	{
		XCEPT_RETHROW(xgi::exception::Exception, "Failed to instantiate application", ex);
	}
	catch (xdaq::exception::SymbolLookupFailed& ex)
	{
		XCEPT_RETHROW(xgi::exception::Exception, "Failed to instantiate application", ex);
	}
	catch (config::PackageInfo::VersionException& ex)
	{
		XCEPT_RAISE(xgi::exception::Exception, ex.what());
	}
	catch (xdaq::exception::Exception& ex)
	{
		XCEPT_RETHROW(xgi::exception::Exception, "Failed to instantiate application", ex);
	}
	catch (const std::exception& ex)
	{
		XCEPT_RAISE(xgi::exception::Exception, ex.what());
	}

	std::stringstream redirectURL;
	redirectURL << this->getApplicationDescriptor()->getContextDescriptor()->getURL() << "/";	// << this->getApplicationDescriptor()->getURN;

	*out << "<meta http-equiv=\"Refresh\" content=\"0; url=" << redirectURL.str() << "\" />";
}

void hyperdaq::Application::applicationGroups (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	this->getHyperDAQHeader(in, out, "application-groups");
	this->displayApplicationGroups(out);
	this->getHyperDAQFooter(in, out);
}

void hyperdaq::Application::viewApplications (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	this->getHyperDAQHeader(in, out, "view-applications");

	std::set<const xdaq::ApplicationDescriptor*> merged;

	std::set < std::string > zones = this->getApplicationContext()->getZoneNames();
	for (std::set<std::string>::iterator i = zones.begin(); i != zones.end(); i++)
	{
		std::set<const xdaq::ApplicationGroup *> groups = this->getApplicationContext()->getZone(*i)->getGroups();

		for (std::set<const xdaq::ApplicationGroup *>::iterator j = groups.begin(); j != groups.end(); j++)
		{
			std::set<const xdaq::ApplicationDescriptor*> descriptors = (*j)->getApplicationDescriptors();
			merged.insert(descriptors.begin(), descriptors.end());
		}
	}

	this->displayApplicationTable(out, merged);

	this->getHyperDAQFooter(in, out);
}

void hyperdaq::Application::viewEndpoints (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	this->getHyperDAQHeader(in, out, "endpoints");

	*out << cgicc::h3("Endpoints") << std::endl;

	*out << cgicc::table().set("class", "xdaq-table");

	*out << cgicc::thead();

	*out << cgicc::tr();
	*out << cgicc::th("URL");
	*out << cgicc::th("Protocol");
	*out << cgicc::th("Service");
	*out << cgicc::th("Network");
	*out << cgicc::th("Alias");
	*out << cgicc::th("Active");
	*out << cgicc::tr() << std::endl;

	*out << cgicc::thead();
	*out << cgicc::tbody();

	std::vector<const xdaq::Network*> networks = this->getApplicationContext()->getNetGroup()->getNetworks();

	for (std::vector<const xdaq::Network*>::iterator j = networks.begin(); j != networks.end(); j++)
	{
		if ((*j)->isEndpointExisting(this->getApplicationContext()->getContextDescriptor()))
		{
			const xdaq::Endpoint* e = (*j)->getEndpoint(this->getApplicationContext()->getContextDescriptor());
			pt::Address::Reference address = e->getAddress();
			std::string protocol = address->getProtocol();
			std::string service = address->getService();

			*out << cgicc::tr();
			*out << cgicc::td(address->toString());
			*out << cgicc::td(protocol);
			*out << cgicc::td(service);
			*out << cgicc::td((*j)->getName());
			if (e->isAlias())
			{
				*out << cgicc::td("true");
			}
			else
			{
				*out << cgicc::td("false");
			}
			if (e->isActive())
			{
				*out << cgicc::td("true");
			}
			else
			{
				*out << cgicc::td("false");
			}

			*out << cgicc::tr() << std::endl;

		}
	}
	*out << cgicc::tbody();
	*out << cgicc::table();

	this->getHyperDAQFooter(in, out);
}

void hyperdaq::Application::viewGroup (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	this->getHyperDAQHeader(in, out, "application-groups");
	try
	{
		cgicc::Cgicc cgi(in);

		//Returns: The desired element, or an empty string if not found.
		std::string group = cgi("group");
		std::string zone = cgi("zone");

		// Show all applications in group

		const xdaq::ApplicationGroup * g = this->getApplicationContext()->getZone(zone)->getApplicationGroup(group);

		std::set<const xdaq::ApplicationDescriptor*> applications = g->getApplicationDescriptors();
		this->displayApplicationTable(out, applications);
	}
	catch (const std::exception& e)
	{
		*out << "<pre>" << e.what() << "</pre>" << std::endl;
	}

	this->getHyperDAQFooter(in, out);
}

void hyperdaq::Application::parameterQuery (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	size_t lid = 0;

	cgicc::Cgicc cgi(in);
	cgicc::const_form_iterator fi = cgi.getElement("lid");
	if (fi != cgi.getElements().end())
	{
		lid = (*fi).getIntegerValue();
	}
	else
	{
		this->getHyperDAQHeader(in, out, "view-applications");
		*out << "Failed to query for XML, a valid lid was not provided" << std::endl;
		this->getHyperDAQFooter(in, out);
		return;
	}

	xdaq::Application * application = 0;
	try
	{
		application = this->getApplicationContext()->getApplicationRegistry()->getApplication(lid);
	}
	catch (xdaq::exception::ApplicationNotFound& e)
	{
		this->getHyperDAQHeader(in, out, "view-applications");
		*out << "Failed to query for XML, the lid (" << lid << ") provided is invalid" << std::endl;
		this->getHyperDAQFooter(in, out);
		return;
	}

	this->infospace2XML(out, application->getApplicationInfoSpace());
}

void hyperdaq::Application::infospaceQuery (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	std::string infospaceName = "";

	cgicc::Cgicc cgi(in);
	cgicc::const_form_iterator fi = cgi.getElement("name");
	if (fi != cgi.getElements().end())
	{
		infospaceName = (*fi).getValue();
	}
	else
	{
		this->getHyperDAQHeader(in, out, "view-applications");
		*out << "Failed to query for XML, a valid name was not provided" << std::endl;
		this->getHyperDAQFooter(in, out);
		return;
	}

	xdata::InfoSpace* infospace = 0;
	try
	{
		infospace = xdata::getInfoSpaceFactory()->get(infospaceName);
	}
	catch (xdata::exception::Exception & e)
	{
		//XCEPT_RETHROW(xgi::exception::Exception, "Failed to query for infospace", e);
		this->getHyperDAQHeader(in, out, "view-applications");
		*out << e.what() << "<br /><br />" << std::endl;
		*out << "The following infospace exist <br /><br />" << std::endl;

		std::map<std::string, xdata::Serializable *, std::less<std::string> > iss = xdata::getInfoSpaceFactory()->match("(.*)");
		for (std::map<std::string, xdata::Serializable *, std::less<std::string> >::iterator it = iss.begin(); it != iss.end(); ++it)
		{
			*out << (dynamic_cast<xdata::InfoSpace *>((*it).second))->name() << "<br />" << std::endl;
		}

		this->getHyperDAQFooter(in, out);
		return;
	}

	this->infospace2XML(out, infospace);
}
void hyperdaq::Application::infospace2XML (xgi::Output * out, xdata::InfoSpace* infospace) throw (xgi::exception::Exception)
{
	out->getHTTPResponseHeader().addHeader("Content-Type", "application/xml");
	*out << "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" << std::endl;

	DOMImplementation* impl = DOMImplementationRegistry::getDOMImplementation(xdata::XStr("Core"));
	DOMDocument* exportDocument = impl->createDocument(xdata::XStr(""), xdata::XStr("root"), 0);
	DOMElement* node = exportDocument->createElementNS(xdata::XStr("any"), xdata::XStr("Properties"));
	node->setAttribute(xdata::XStr("xmlns"), xdata::XStr("any"));
	xdata::soap::Serializer serializer;
	try
	{
		serializer.exportAll(infospace, node, true);
	}
	catch (xdata::exception::Exception & e)
	{
		XCEPT_RETHROW(xgi::exception::Exception, "Faield to export infospace to XML", e);
	}
	std::string xml = "";
	xdata::XMLDOMSerializer s2(xml);
	s2.serialize(node);
	*out << xml << std::endl;
}

void hyperdaq::Application::workLoopList (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
   toolbox::PolicyFactory* factory = toolbox::getPolicyFactory();

        std::list<toolbox::Policy*> policies = factory->getPolicies("thread");
        std::stringstream ss;

	json_t *root = json_object();

	out->getHTTPResponseHeader().addHeader("Content-Type", "application/json");
        for (std::list<toolbox::Policy*>::iterator i = policies.begin(); i != policies.end(); i++)
        {
                std::vector<std::string> pnames = (*i)->propertyNames();
                std::stringstream o;

                for (std::vector<std::string>::iterator k = pnames.begin(); k != pnames.end(); k++ )
                {
			if ( (*k).find("urn:toolbox-task-workloop-SYS_gettid") != std::string::npos ) 
			{
				toolbox::net::URN urn(*k);
				json_t * workloop = json_object();
				json_object_set_new( workloop, "SYS_gettid", json_integer( toolbox::toLong((*i)->getProperty(*k) ) ));
				json_object_set_new( root, urn.getNSS().c_str(), workloop );
			}
		}
	}  // loop over known policies 
	*out << json_dumps(root, 0);
	json_decref(root);
}
// DEV AREA
void hyperdaq::Application::devArea (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	//this->getHyperDAQHeader(in, out, "dev-area");

	/*
	 *out << "<div id=\"xdaq-dev-1\">" << std::endl;
	 *out << "	<script>" << std::endl;
	 *out << "		$(function(){" << std::endl;
	 *out << "       	$(\"#xdaq-dev-1\").load(\"/hyperdaq/html/dev/tables.html\");" << std::endl;
	 *out << "     	  });" << std::endl;
	 *out << "	</script>" << std::endl;
	 *out << "</div>" << std::endl;
	 */

	// Test display of exception
	/*
	 XCEPT_DECLARE(xgi::exception::Exception, ex1, "Ex 1");
	 XCEPT_DECLARE_NESTED(xgi::exception::Exception, ex2, "Ex 2", ex1);
	 XCEPT_RETHROW(xgi::exception::Exception, "Head Exception", ex2);
	 */

	//*out << cgicc::h3("Error Test") << std::endl;
	//this->getHyperDAQFooter(in, out);
}
