// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _hyperdaq_Application_h_
#define _hyperdaq_Application_h_

#include <set>

#include "xdaq/Application.h"

#include "xgi/framework/UIManager.h"
#include "hyperdaq/framework/Layout.h"

#include "xgi/Utils.h"
#include "xgi/Method.h"

#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"

#include "xdaq/ApplicationGroup.h"
#include "xdaq/ApplicationRegistry.h"
#include "xcept/tools.h"
#include "toolbox/Policy.h" 

namespace hyperdaq
{
	class Application : public xdaq::Application, public xgi::framework::UIManager
	{
		public:

			XDAQ_INSTANTIATOR();

			Application (xdaq::ApplicationStub * s) throw (xdaq::exception::Exception);

			void Default (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception);

			void configureCluster (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception);
			void configureClusterResult (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception);
			void exploreCluster (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception);
			void controlCluster (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception);

			// Logging
			void configureLoggers (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception);
			void logContentPage (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception);
			void logPage (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception);

			/*! Retrieve a log file if existing
			 * parameters: url in encoded format, e.g. file:/path
			 * tail=size .... from the end of the file 'size' bytes (optional, by default 100)
			 * head=size .... from the beginning of the file 'size' bytes (optional, by default 100)
			 * from=X&len=Y.. from position X of file, Y bytes (len is optional, by default 100)
			 * if the request reads beyond the file size or limits, only the data up to the end of the file are returned
			 * If only the url is provided and nothing else, the whole file is retrieved
			 * If the file does not exist or cannot be opened a failure page is returned
			 */
			void getLogFile (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception);


			void memoryPolicies (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception);
			void cpuPolicies (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception);

			void displayModules (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception);
			void displayPackageInfo (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception);
			void debugSOAP (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception);
			void editProperties (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception);
			void exit (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception); // Exit currently running process with code 2

			void processInformation (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception);

			void uploadApplicationForm (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception);
			void uploadApplication (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception);

			void viewApplications (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception);
			void viewEndpoints (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception);

			void applicationGroups (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception);
			void viewGroup (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception);

			// returns pure XML
			void parameterQuery (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception);
			void infospaceQuery (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception);

			void workLoopList (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception);


			//////////
			// DEV HYPERDAQ FUNCTIONS
			//////////

			void devArea (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception);

			//////////
			//
			//////////

		private:

			void infospace2XML (xgi::Output * out, xdata::InfoSpace* infospace) throw (xgi::exception::Exception);

			// All Control Panel 'applications'
			struct ControlPanelApplication
			{
					std::string id;
					std::string url;
					std::string title;
					std::string img;
					ControlPanelApplication (std::string i, std::string ur, std::string ti, std::string im)
						: id(i), url(ur), title(ti), img(im)
					{
					}
			};

			// Give the layout access to the list of applications to avoid duplicating links everywhere
			std::vector<struct ControlPanelApplication> getControlPanelApplications();

			std::vector<ControlPanelApplication> controlPanelApps_;

			//////////
			// NEW HYPERDAQ FUNCTIONS
			//////////

			void getHyperDAQHeader (xgi::Input * in, xgi::Output * out, std::string appName) throw (xgi::exception::Exception);
			void getHyperDAQFooter (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception);

			// get SOAP message with command name
			void getSOAPMessage (xgi::Output * out, std::string command) throw (xgi::exception::Exception);
			void getRelaySOAPMessage (xgi::Output * out, std::string command) throw (xgi::exception::Exception);

			//////////
			//
			//////////

			void displayApplicationTable (xgi::Output* out, std::set<const xdaq::ApplicationDescriptor*> & applications);

			void displayApplicationGroups (xgi::Output* out);

			void forwardConfiguration (DOMDocument* doc);

			xgi::Output* getPolicyHTML (const std::string & policyType, xgi::Output * out);

			xgi::Output* getPolicyCSS (xgi::Output * out);

			bool isDefault (std::list<toolbox::Policy*>::iterator policy);

			//! Get a string representation of the current resource limit
			std::string getProcessLimit (int resource);

			hyperdaq::framework::Layout layout_;
	};

}
#endif
