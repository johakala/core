function selectorChanged ()
{
	$("#logconfigsubmitbut").removeClass("xdaq-submit-button xdaq-green");
	$("#logconfigsubmitbut").addClass("xdaq-red");
}

function changesSubmitted ()
{
	$("#logconfigsubmitbut").removeClass("xdaq-submit-button xdaq-red");
	$("#logconfigsubmitbut").addClass("xdaq-green");
}

function configureLoggers ()
{
	var url = "configureLoggers?";
	var first = true;
	$(".xdaqLogController-Selector").each(function(){
		if (first == true)
		{
			first = false;
		}
		else
		{
			url = url + "&";
		}
		url = url + $(this).attr("id") + "=" + $(this).val() + "";
	});
	
	var options = {
		url: url,
		type: "GET",
		error: function (xhr, textStatus, errorThrown) {
			selectorChanged();
		}
	};
	
	xdaqAJAX(options, function (data, textStatus, xhr) {
		changesSubmitted();
	});
}