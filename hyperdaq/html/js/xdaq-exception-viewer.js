/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.			                 			 *
 * All rights reserved.                                                  *
 * Authors: A. Forrest					 								 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                        	 *
 * For the list of contributors see CREDITS.   			       			 *
 *************************************************************************/

$(document).on("xdaq-post-load", function() {
	$("#xdaq-wrapper").on('click', '#hyperdaq-exception-viewer-tree tbody>tr', function(e) {
		//$("#xdaq-sentinel-arc-exceptions-properties tbody").addClass("xdaq-sentinel-arc-exception-viewer-hidden");
		$("#hyperdaq-exceptions-properties tbody").addClass("hyperdaq-exception-viewer-hidden");
		
		var identifier = $(this).attr("data-tree-depth");
		console.log(identifier);
		console.log($(this));
		
		$("#hyperdaq-exceptions-properties tbody[id=hyperdaq-exception-view-" + identifier + "]").removeClass("hyperdaq-exception-viewer-hidden");
		
		$(this).parent().children().removeClass("hyperdaq-exceptions-table-selected");
		$(this).addClass("hyperdaq-exceptions-table-selected");
	});
});

function downloadExceptionHistory(txt)
{
	function dataUrl(data) 
	{
		return "data:x-application/text," + escape(data);
	}

	window.open(dataUrl(txt));
}