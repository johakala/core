
$(function() {
	//xdaqInitParamViewer($("#xdaqparamviewer"));
}); 

//function xdaqWindowPostLoad() 
//{
$(document).on("xdaq-post-load", function(){
	$('#xdaqparamviewer-set').click(function() {
		
		var tableName = $(this).attr("data-linkedtable");
		var table = $(document.getElementById(tableName));
		//var table = $("#"+$(this).attr("data-linkedtable"));
		
		var tbody = table.children("tbody").first();
	
		var requestURL = table.attr("data-requesturl");
		var soapAction = table.attr("data-soapaction");
		
		var soapMsgSetTop = '<SOAP-ENV:Envelope SOAP-ENV:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/">';
    		soapMsgSetTop = soapMsgSetTop + '<SOAP-ENV:Header>';
    		soapMsgSetTop = soapMsgSetTop + '</SOAP-ENV:Header>';
    		soapMsgSetTop = soapMsgSetTop + '<SOAP-ENV:Body>';
    		soapMsgSetTop = soapMsgSetTop + '<xdaq:ParameterSet xmlns:xdaq="urn:xdaq-soap:3.0">';
    		soapMsgSetTop = soapMsgSetTop + '<p:properties xmlns:p="urn:xdaq-application:';
    		
    	var soapMsgSetMid = '" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="soapenc:Struct">';
    		
    	var soapMsgSetBot = '</p:properties>';
    		soapMsgSetBot = soapMsgSetBot + '</xdaq:ParameterSet>';
    		soapMsgSetBot = soapMsgSetBot + '</SOAP-ENV:Body>';
    		soapMsgSetBot = soapMsgSetBot + '</SOAP-ENV:Envelope>'; 	
    		
    	var soapMsgSetContent = "";
    		
    	// Gather all rows with checked ticks and get value
    	rowID = 0;
    	
    	var foundSelected = false;
    	
    	tbody.children("tr").each(function () {
    		var selected = $(this).find("td input:checkbox:checked");
    		var cur = $(this).find("td input[type=text]");
			if (selected.length > 0 && cur.length > 0)
			{
				foundSelected = true;
				
				var elValue = cur.first().val();
				
				var elName = $(this).children().eq(0).text();
				var elType = $(this).children().eq(2).text();
				
				// elValue == value for the row
				soapMsgSetContent = soapMsgSetContent + '<p:' + elName + ' xsi:type="' + elType + '">' + elValue + '</p:' + elName + '>';
			}
    	});
    	
    	if (foundSelected == false)
    	{
    		return;
    	}
    	
    	var soapMsgSet = soapMsgSetTop + table.attr("data-urn") + soapMsgSetMid + soapMsgSetContent + soapMsgSetBot;
    		
		var options = {
			url: requestURL,
			data: soapMsgSet,
			headers: {
				"SOAPAction": soapAction
			},
			error: function (xhr, textStatus, errorThrown) {
				console.error("Failed to get properties");
			}
		};
		
		$('#xdaqparamviewer-set').removeClass("xdaq-submit-button xdaq-green");
		$('#xdaqparamviewer-set').addClass("xdaq-red");
						
		xdaqAJAX(options, function (data, textStatus, xhr) {
			$('#xdaqparamviewer-set').removeClass("xdaq-red");
			$('#xdaqparamviewer-set').addClass("xdaq-green");
		});
	});
});






