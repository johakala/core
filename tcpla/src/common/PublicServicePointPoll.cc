// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2012, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A.Petrucci, C.Wakefield				 *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include <sstream>
#include "tcpla/PublicServicePointPoll.h"
#include "tcpla/InterfaceAdapter.h"
#include "tcpla/Utils.h"
#include "tcpla/EventHandler.h"

#include "toolbox/hexdump.h"

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

tcpla::PublicServicePointPoll::PublicServicePointPoll (tcpla::InterfaceAdapter * ia, pt::Address::Reference address, size_t ioQueueSize, size_t headerSize) throw (tcpla::exception::Exception, tcpla::exception::InvalidRcvTimeOut, tcpla::exception::NoFreePortInRange, tcpla::exception::CannotBind)
	: PublicServicePoint(ia, address, ioQueueSize, headerSize)
{
	// moved to IA
	// LOG4CPLUS_DEBUG(tcpla::InterfaceAdapter::getOwnerApplication()->getApplicationLogger(), __FUNCTION__); // no logger
	//std::cout << __FUNCTION__ << std::endl;

	// the first entry is the listen sockets
	this->maxPollfdsKey_ = 0;

	pollfds_ = new pollfd[this->maxClients_]; //array of pollfd structs

	//initialise receive socket data && poll filedescriptors array

	for (int i = 0; i < this->maxClients_; i++)
	{
		pollfds_[i].fd = tcpla::EMPTY_FD;
	}

	pollfds_[0].fd = listenfd_;
	pollfds_[0].events = POLLRDNORM;

	std::stringstream name;
	name << "tcpla-psp/" << listenAddress_->getProperty("hostname") << ":" << port_;

	try
	{
		toolbox::task::getWorkLoopFactory()->getWorkLoop(name.str(), "polling")->activate();
		toolbox::task::getWorkLoopFactory()->getWorkLoop(name.str(), "polling")->submit(process_);
	}
	catch (toolbox::task::exception::Exception& e)
	{
		std::stringstream ss;
		ss << "Failed to submit workloop " << name;
		//XCEPT_DECLARE_NESTED(tcpla::exception::WorkLoopSubmitFailed, ex, ss.str(), e);
		XCEPT_RETHROW(tcpla::exception::Exception, ss.str(), e);
	}
	catch (std::exception& se)
	{
		std::stringstream ss;
		ss << "Failed to submit notification to worker thread, caught standard exception";
		XCEPT_DECLARE(tcpla::exception::WorkerThreadFailed, ex, se.what());
		XCEPT_RETHROW(tcpla::exception::Exception, ss.str(), ex);
	}
	catch (...)
	{
		std::stringstream ss;
		ss << "Failed to submit notification to worker pool, caught unknown exception in PublicServicePoint " << this->toString();

		XCEPT_RAISE(tcpla::exception::Exception, ss.str());
	}
}

tcpla::PublicServicePointPoll::~PublicServicePointPoll ()
{
	delete[] pollfds_;
}

std::string tcpla::PublicServicePointPoll::getType ()
{
	// same as corresponding interface adapter
	return "poll";
}

void tcpla::PublicServicePointPoll::accept () throw (tcpla::exception::Exception)
{
	struct sockaddr_in cliaddr;

	int connfd = -1;
	try
	{
		connfd = this->acceptSocket(pollfds_[0].fd, &cliaddr);
	}
	catch (tcpla::exception::FailedToAccept & e)
	{
		XCEPT_RETHROW(tcpla::exception::Exception, "Failed to accept", e);
	}
	catch (tcpla::exception::FailedToGetFileControlSettings & e)
	{
		XCEPT_RETHROW(tcpla::exception::Exception, "Failed to accept", e);
	}
	catch (tcpla::exception::FailedToSetNonBlocking & e)
	{
		XCEPT_RETHROW(tcpla::exception::Exception, "Failed to accept", e);
	}
	//std::cout << "(tcpla::PublicServicePointPoll::accept ())  Creating file descriptor '" << connfd << "'" << std::endl;

	if (connfd == -1)
	{
		return;
	}

	int i;

	//find a file descriptor not in use
	for (i = 1; i < this->maxClients_; i++)
	{
		if (pollfds_[i].fd == tcpla::EMPTY_FD)
		{
			pollfds_[i].fd = connfd;
			pollfds_[i].events = 0;
			break;
		}
	}

	if (i == this->maxClients_)
	{
		std::stringstream ss;
		ss << "Too many connections: " << i << " (Max Allowed: " << this->maxClients_ << ") in PublicServicePoint " << this->toString();

		XCEPT_RAISE(tcpla::exception::Exception, ss.str());
	}

	rcvSocks_[i].hostname = inet_ntoa(cliaddr.sin_addr);
	rcvSocks_[i].port = ntohs(cliaddr.sin_port);
	rcvSocks_[i].pollfdsKey = i;
	rcvSocks_[i].ep_handle.clear();
	rcvSocks_[i].rcvnBytes = 0;
	rcvSocks_[i].connected = false;

	rcvSocks_[i].counter = 0;
	rcvSocks_[i].readCounter = 0;
	rcvSocks_[i].idleBlockingReadCounter = 0;
	rcvSocks_[i].idleEmptyQueueCounter = 0;
	rcvSocks_[i].length = 0;
	rcvSocks_[i].current.size = 0;

	try
	{
		rcvSocks_[i].freeQueue->clear();
	}
	catch (...)
	{
		std::stringstream ss;
		ss << "Internal error - clearing free receive buffers queue for index " << i << " in PublicServicePoint " << this->toString();

		XCEPT_RAISE(tcpla::exception::Exception, ss.str());
	}

	if (i > this->maxPollfdsKey_)
	{
		this->maxPollfdsKey_ = i;
	}

	totalClients_++;

	tcpla::Event event;

	event.event_number = TCPLA_CONNECTION_REQUEST_EVENT;
	//event.event_data.psp_event_data.ep = sd.ep;
	event.event_data.cr_event_data.psp = this;
	event.event_data.cr_event_data.cr_handle = i;

	this->ia_->pushEvent(ia_->creq_evd_hdl_, event);
}

void tcpla::PublicServicePointPoll::accept (tcpla::InterfaceAdapter::Command & c) // throw (tcpla::exception::Exception)
{

	tcpla::EndPoint & ep_handle = c.command.accept_command.ep_handle;
	int cr_handle = c.command.accept_command.cr_handle;

	rcvSocks_[cr_handle].connected = true;
	ep_handle.pollfdsKey_ = cr_handle;
	ep_handle.psp_ = this;
	rcvSocks_[cr_handle].ep_handle = ep_handle;
	pollfds_[cr_handle].events = POLLIN;

	//std::cout << "(tcpla::PublicServicePointPoll::accept ( tcpla::InterfaceAdapter::Command & c ))  Creating file descriptor '" << pollfds_[cr_handle].fd << "'" << " : UUID = '" << ep_handle.toString() << "'" << std::endl;

	//add pointer to mem pool to this event

	tcpla::Event event;

	event.event_number = TCPLA_CONNECTION_EVENT_ACCEPTED;
	event.event_data.connect_event_data.ep_handle = ep_handle;
	event.event_data.connect_event_data.user_cookie.as_ptr = c.command.accept_command.cookie.as_ptr;

	//this->ia_->pushEvent(ia_->conn_evd_hdl_, event);

	// should stop data race between thread posting buffers to a new connection and first frames received causing that connection to close (keep-alive or close mode)

	if (ia_->singleThread_)
	{
		ia_->eventHandler_->handleEvent(event);

	}
	else if (ia_->pspDispatcherThread_)
	{
		this->ia_->pushEvent(recv_evd_hdl_, event);
	}
	else
	{
		this->ia_->pushEvent(ia_->recv_evd_hdl_, event);
	}
}

int tcpla::PublicServicePointPoll::getFD (size_t pollfdsKey)
{
	return pollfds_[pollfdsKey].fd;
}

bool tcpla::PublicServicePointPoll::isReady (size_t pollfdsKey)
{
	if ((pollfds_[pollfdsKey].revents & POLLIN))
	{
		return true;
	}
	return false;
}

void tcpla::PublicServicePointPoll::disableFD (size_t pollfdsKey)
{
	pollfds_[pollfdsKey].events = 0;
}

bool tcpla::PublicServicePointPoll::process (toolbox::task::WorkLoop* wl)
{
	while (true)
	{
		if (!commandQueue_->empty())
		{
			//std::cout << "I am going to eval" << std::endl;

			this->eval();
		}
		errno = 0;
		int numberReady = poll(pollfds_, this->maxPollfdsKey_ + 1, rcvTimeout_);

		//if (numberReady > 0) std::cout << "PSP A: " << numberReady << std::endl;
		if (numberReady < 0)
		{
			if (errno == EINTR)
			{
				std::cout << "lost time" << std::endl;
				return true;
			}

			std::stringstream ss;

			ss << "Poll error in work loop " << wl->getName() << " in PublicServicePoint " << this->toString() << " totalClients: " << totalClients_ + 1 << " receiveTimeout: " << rcvTimeout_ << " errno: " << strerror(errno);

			tcpla::Event event;

			event.event_number = TCPLA_ASYNC_ERROR_IA_CATASTROPHIC;
			event.event_data.asynch_error_event_data.ep_handle.clear();
			event.event_data.asynch_error_event_data.reason = 6;
			event.event_data.asynch_error_event_data.exception = TCPLA_XCEPT_ALLOC (tcpla::exception::PollFailed, ss.str());

			this->ia_->pushEvent(ia_->reqt_evd_hdl_, event);

			return false;
		}

		if (numberReady == 0)
		{
			idleCounter_++;
			continue;
		}

		if (pollfds_[0].revents & POLLRDNORM) //a new client has request connection
		{
			LOG4CPLUS_DEBUG(ia_->getOwnerApplication()->getApplicationLogger(), "A new client has request connection");
			try
			{
				this->accept(); // this can be called several times till acceptance is actually performed
			}
			catch (tcpla::exception::Exception & e)
			{
				tcpla::Event event;

				event.event_number = TCPLA_ASYNC_ERROR_PROVIDER_INTERNAL_ERROR;
				event.event_data.asynch_error_event_data.ep_handle.clear();
				event.event_data.asynch_error_event_data.reason = 2;

				std::stringstream ss;
				ss << "Internal accept failed in poll in PublicServicePoint " << this->toString();

				event.event_data.asynch_error_event_data.exception = TCPLA_XCEPT_ALLOC_NESTED(tcpla::exception::FailedToAccept, ss.str(), e);

				this->ia_->pushEvent(ia_->reqt_evd_hdl_, event);
			}

			if (--numberReady <= 0)
			{
				continue;
			}

		}

		this->processSocketEntries();
		//std::cout << "I have returned" << std::endl;

	}

	return true;
}

void tcpla::PublicServicePointPoll::resetRcvEntry (tcpla::EndPoint & ep_handle) throw (tcpla::exception::FailedToResetReceiveEntry)
{
	int pollfdsKey = ep_handle.pollfdsKey_;

	if (pollfdsKey > 0) // any accepted connection ( 0 is the listen)
	{
		//std::cout << "tcpla::PublicServicePointPoll::resetRcvEntry Going to reset entry for file descriptor '" << pollfds_[pollfdsKey].fd << "'" << std::endl;
		::close(pollfds_[pollfdsKey].fd);

		pollfds_[pollfdsKey].fd = tcpla::EMPTY_FD;
		pollfds_[pollfdsKey].events = 0;

		rcvSocks_[pollfdsKey].ep_handle.clear();

		rcvSocks_[pollfdsKey].pollfdsKey = -1;
		rcvSocks_[pollfdsKey].connected = false;

		rcvSocks_[pollfdsKey].rcvnBytes = 0;
		rcvSocks_[pollfdsKey].length = 0;
		rcvSocks_[pollfdsKey].hostname = "";
		rcvSocks_[pollfdsKey].port = 0;
		rcvSocks_[pollfdsKey].counter = 0;
		rcvSocks_[pollfdsKey].readCounter = 0;
		rcvSocks_[pollfdsKey].idleBlockingReadCounter = 0;
		rcvSocks_[pollfdsKey].idleEmptyQueueCounter = 0;

		totalClients_--;

		while (!rcvSocks_[pollfdsKey].freeQueue->empty())
		{
			TCPLA_RECV_POST_DESC d = rcvSocks_[pollfdsKey].freeQueue->front();

			tcpla::Event event;

			event.event_number = TCPLA_DTO_RCV_UNUSED_BLOCK;
			event.event_data.tcpla_dto_block_event_data.user_cookie = d.cookie;
			event.event_data.tcpla_dto_block_event_data.block_length = d.size;
			event.event_data.tcpla_dto_block_event_data.buffer = d.local_iov;

			// This might need to push the psp recv_evd_hdl, depending upon the configuration settings regarding setup
			// pspDispatcherThread_ == false means use ia_, true then use psp
			if (this->ia_->pspDispatcherThread_)
			{
				this->ia_->pushEvent(recv_evd_hdl_, event);
			}
			else
			{
				this->ia_->pushEvent(ia_->recv_evd_hdl_, event);
			}

			rcvSocks_[pollfdsKey].freeQueue->pop_front();
		}

		int i = -1;
		for (i = this->maxPollfdsKey_; i > 0; i--)
		{
			if (pollfds_[i].fd != tcpla::EMPTY_FD)
			{
				this->maxPollfdsKey_ = i;
				break;
			}
		}

		// only listen socket remaining in array
		if (i == 0)
		{
			this->maxPollfdsKey_ = 0;
		}
	}
	else
	{
		std::stringstream ss;
		ss << "Cannot reset receive entry on invalid index " << pollfdsKey;

		XCEPT_RAISE(tcpla::exception::FailedToResetReceiveEntry, ss.str());
	}

	//mutex_.give();
}

int tcpla::PublicServicePointPoll::getFD (tcpla::EndPoint & ep_handle) throw (tcpla::exception::InvalidEndPoint)
{
	if (ep_handle.pollfdsKey_ != -1)
	{
		return this->pollfds_[ep_handle.pollfdsKey_].fd;
	}
	else
	{
		std::stringstream ss;
		ss << "Invalid EndPoint";

		XCEPT_RAISE(tcpla::exception::InvalidEndPoint, ss.str());
	}
}
