// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2011, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J.Adamczewski(EE/GSI), S.Linev(EE/GSI), L.Orsini, A.Petrucci *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include "tcpla/PollingWorkLoop.h"
#include "toolbox/task/WorkLoopFactory.h"

#include <sstream>
#include <pthread.h>

tcpla::PollingWorkLoop::PollingWorkLoop (const std::string & name, tcpla::EventDispatcher* evd_handle, tcpla::EventHandler * event_handler, const std::string & affinity) throw (tcpla::exception::Exception)
	: tcpla::WorkLoop(name, event_handler, affinity), evd_handle_(dynamic_cast<tcpla::PollingEventDispatcher*>(evd_handle))
{
	process_ = toolbox::task::bind(this, &PollingWorkLoop::process, "process");

	try
	{
		toolbox::task::getWorkLoopFactory()->getWorkLoop(name_, "polling")->activate();
		toolbox::task::getWorkLoopFactory()->getWorkLoop(name_, "polling")->submit(process_);
	}
	catch (toolbox::task::exception::Exception& e)
	{
		std::stringstream ss;
		ss << "Failed to submit workloop " << name_;
		//XCEPT_DECLARE_NESTED(tcpla::exception::WorkLoopSubmitFailed, ex, ss.str(), e);
		XCEPT_RETHROW(tcpla::exception::Exception, ss.str(), e);
	}
	catch (std::exception& se)
	{
		std::stringstream ss;
		ss << "Failed to submit notification to worker thread, caught standard exception";
		XCEPT_DECLARE(tcpla::exception::WorkerThreadFailed, ex, se.what());
		XCEPT_RETHROW(tcpla::exception::Exception, ss.str(), ex);
	}
	catch (...)
	{
		XCEPT_RAISE(tcpla::exception::Exception, "Failed to submit notification to worker pool, caught unknown exception");
	}
}

tcpla::PollingWorkLoop::~PollingWorkLoop ()
{
	toolbox::task::getWorkLoopFactory()->getWorkLoop(name_, "polling")->cancel();
}

bool tcpla::PollingWorkLoop::process (toolbox::task::WorkLoop* wl)
{
	while (true)
	{
		//std::cout << "Activated workloop " << name_ << std::endl;
		if (!evd_handle_->eventQueue_->empty())
		{
			tcpla::Event event = evd_handle_->eventQueue_->front();
			evd_handle_->eventQueue_->pop_front();

			event_handler_->handleEvent(event);
		}
		else
		{
			sched_yield();
		}
	}

	return true;
}

