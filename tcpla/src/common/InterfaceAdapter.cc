// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2011, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A.Petrucci, C.Wakefield					 *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include <sstream>

#include "tcpla/InterfaceAdapter.h"

#include "tcpla/PublicServicePointPoll.h"
#include "tcpla/PublicServicePointSelect.h"

#include "tcpla/WaitingWorkLoop.h"
#include "tcpla/PollingWorkLoop.h"
#include "tcpla/EndPoint.h"

#include "tcpla/exception/CannotConnect.h"
#include "tcpla/Utils.h"

#include "toolbox/hexdump.h"
#include "toolbox/string.h"

#include "xcept/tools.h"

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
//#include <linux/sdp_inet.h>

tcpla::InterfaceAdapter::InterfaceAdapter (xdaq::Application * parent, pt::Address::Reference address, tcpla::EventHandler * h, size_t maxClients, size_t ioQueueSize, size_t eventQueueSize, getHeaderSizeFunctionType ghf, getLengthFunctionType glf) throw (tcpla::exception::InvalidSndTimeOut, tcpla::exception::Exception)
	: xdaq::Object(parent), mutex_(toolbox::BSem::FULL)
{
	localAddress_ = dynamic_cast<tcpla::Address *>(&(*address));

	this->ghf_ = ghf;
	this->glf_ = glf;
	this->id_ = localAddress_->toURL();

	this->hostname_ = localAddress_->getProperty("hostname");
	this->maxClients_ = maxClients;
	this->ioQueueSize_ = ioQueueSize;
	this->totalClients_ = 0;
	this->sndTimeout_ = 100;
	this->idleCounter_ = 0;
	this->singleThread_ = false;
	this->eventQueueSize_ = eventQueueSize;

	this->eventHandler_ = h;

	try
	{
		this->sndTimeout_ = (int) toolbox::toLong(localAddress_->getProperty("sndTimeout"));
	}
	catch (toolbox::exception::Exception & e)
	{
		XCEPT_RETHROW(tcpla::exception::InvalidSndTimeOut, "Failed to set send timeout, invalid integer value provided in EndPoint configuration", e);
	}

	try
	{
		this->pollingCycle_ = (size_t) toolbox::toLong(localAddress_->getProperty("pollingCycle"));
	}
	catch (toolbox::exception::Exception & e)
	{
		XCEPT_RETHROW(tcpla::exception::InvalidSndTimeOut, "Failed to set cycle, invalid integer value provided in EndPoint configuration", e);
	}

	if (localAddress_->getProperty("singleThread") == "true")
	{
		this->singleThread_ = true;
	}

	this->pspDispatcherThread_ = false;

	if (localAddress_->getProperty("pspDispatcherThread") == "true")
	{
		this->pspDispatcherThread_ = true;
	}

	// The affinity is set as follows [THREAD:MODE,CORE] in fours triplets ( waiting mode and no affinity by default)
	// where
	//	THREAD is : RCV is the PublicServicePoint , SND is the InterfaceAdapter , DSR Dispatcher Receiver, DSS Dispatcher Sender
	//	MODE is: W or P ( waiting or polling)
	//    CORE 1..N (processor number)
	//  e.g. RCV:P:2,SND:W:3,DSR:P:1,DSS:W:6

	affinity_ = localAddress_->getAffinity();

	sendSocks_ = new TCPLA_SEND_SOCKET_ENTRY[this->maxClients_];

	for (int i = 0; i < this->maxClients_; i++)
	{
		//TCPLA_SEND_SOCKET_ENTRY sd;

		sendSocks_[i].ep_handle.clear();
		sendSocks_[i].pollfdsKey = -1;
		sendSocks_[i].connected = false;
		sendSocks_[i].failed = false;
		sendSocks_[i].postQueue = toolbox::rlist<TCPLA_SEND_POST_DESC>::create("tcpla-ia-post-queue/"+ this->hostname_ + ":" + localAddress_->getProtocol(), ioQueueSize);
		sendSocks_[i].written = 0;
		//sendSocks_[i].local = 0;
		//sendSocks_[i].destination = 0;
		sendSocks_[i].counter = 0;
		sendSocks_[i].idleBlockingWriteCounter = 0;
		sendSocks_[i].idleEmptyQueueCounter = 0;
		sendSocks_[i].writeCounter = 0;
		sendSocks_[i].cookie.as_ptr = 0;
		sendSocks_[i].port = 0;
		//sendSocks_[i].ip_addr = 0;

		sendSocks_[i].current.iovs = 0;
		//sendSocks_[i].current.local_iov = 0;

	}

	commandQueue_ = toolbox::rlist<Command>::create("tcpla-ia-command-queue/"+ this->hostname_ + ":" + localAddress_->getProtocol(),ioQueueSize);
	creq_evd_hdl_ = new tcpla::WaitingEventDispatcher(eventQueueSize);
	conn_evd_hdl_ = new tcpla::WaitingEventDispatcher(eventQueueSize);

	try
	{
		acceptorWorkLoop_ = new tcpla::WaitingWorkLoop("tcpla-ia-acceptor-dispatcher/" + this->hostname_ + ":" + localAddress_->getProtocol(), creq_evd_hdl_, h, "*");
		connectorWorkLoop_ = new tcpla::WaitingWorkLoop("tcpla-ia-connector-dispatcher/" + this->hostname_ + ":" + localAddress_->getProtocol(), conn_evd_hdl_, h, "*");

		if (!pspDispatcherThread_)
		{
			if (affinity_["DSR"] == "W")
			{
				recv_evd_hdl_ = new tcpla::WaitingEventDispatcher(eventQueueSize);
				receiverWorkLoop_ = new tcpla::WaitingWorkLoop("tcpla-ia-receiver-dispatcher/" + this->hostname_ + ":" + localAddress_->getProtocol(), recv_evd_hdl_, h, affinity_["DSR"]);

			}
			else if (affinity_["DSR"] == "P")
			{
				recv_evd_hdl_ = new tcpla::PollingEventDispatcher(eventQueueSize, "tcpla-ia-receiver-dispatcher/" + this->hostname_);
				receiverWorkLoop_ = new tcpla::PollingWorkLoop("tcpla-ia-receiver-dispatcher/" + this->hostname_ + ":" + localAddress_->getProtocol(), recv_evd_hdl_, h, affinity_["DSR"]);

			}
			else
			{
				std::stringstream ss;
				ss << "Invalid affinity " << localAddress_->getProperty("affinity") << " in InterfaceAdapter " << this->hostname_;

				XCEPT_RAISE(tcpla::exception::Exception, ss.str());
			}
		}

		if (affinity_["DSS"] == "W")
		{
			reqt_evd_hdl_ = new tcpla::WaitingEventDispatcher(eventQueueSize);
			senderWorkLoop_ = new tcpla::WaitingWorkLoop("tcpla-ia-sender-dispatcher/" + this->hostname_ + ":" + localAddress_->getProtocol(), reqt_evd_hdl_, h, affinity_["DSS"]);

		}
		else if (affinity_["DSS"] == "P")
		{
			reqt_evd_hdl_ = new tcpla::PollingEventDispatcher(eventQueueSize, "tcpla-ia-sender-dispatcher/" + this->hostname_);
			senderWorkLoop_ = new tcpla::PollingWorkLoop("tcpla-ia-sender-dispatcher/" + this->hostname_ + ":" + localAddress_->getProtocol(), reqt_evd_hdl_, h, affinity_["DSS"]);
		}
		else
		{
			std::stringstream ss;
			ss << "Invalid affinity " << localAddress_->getProperty("affinity") << " in InterfaceAdapter " << this->hostname_;

			XCEPT_RAISE(tcpla::exception::Exception, ss.str());
		}
	}
	catch (tcpla::exception::Exception & e)
	{
		XCEPT_RETHROW(tcpla::exception::Exception, "Failed to create IA workloops", e);
	}

	process_ = toolbox::task::bind(this, &InterfaceAdapter::process, "process");

}

tcpla::InterfaceAdapter::~InterfaceAdapter ()
{
	toolbox::rlist<Command>::destroy(commandQueue_);

	for (std::list<tcpla::PublicServicePoint *>::iterator i = pspRegistry_.begin(); i != pspRegistry_.end(); i++)
	{
		delete (*i);
	}

	for (int i = 0; i < this->maxClients_; i++)
	{
		toolbox::rlist<TCPLA_SEND_POST_DESC>::destroy(sendSocks_[i].postQueue);
	}
	delete[] sendSocks_;

	delete creq_evd_hdl_;
	delete conn_evd_hdl_;
	delete recv_evd_hdl_;
	delete reqt_evd_hdl_;
}

/*
 tcpla::EndPoint tcpla::InterfaceAdapter::duplicateEndPoint (tcpla::EndPoint & ep_handle)
 {
 tcpla::EndPoint* ep = new tcpla::EndPoint(this);

 *ep = *ep_handle;

 mutex_.take();
 endpointRegistry_.push_back(ep);
 mutex_.give();

 //return ep;
 LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), "Duplicating EndPoint in IA");
 std::cout << "Duplicating EndPoint in IA inUUID = " << ep_handle.toString() << "outUUID = " << ep_handle.toString() << std::endl;
 return *ep;
 }
 */

tcpla::EndPoint tcpla::InterfaceAdapter::createEndPoint ()
{
	tcpla::EndPoint ep;

	// manually fill in stuff, not an object it is a struct (now)

	uuid_generate(ep.uuid_);
	ep.pollfdsKey_ = -1;
	ep.psp_ = 0;
	ep.ia_ = this;

	mutex_.take();
	endpointRegistry_.push_back(ep);
	//uuidRegistry_.push_back(ep->uuid_);
	mutex_.give();

	//return ep;
	LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), "Creating EndPoint in IA");
	//std::cout << "Create EndPoint in IA UUID = " << ep.toString() << std::endl;
	return ep;
}

tcpla::EndPoint tcpla::InterfaceAdapter::createEndPoint (TCPLA_CR_ARRIVAL_EVENT_DATA & cr)
{
	tcpla::EndPoint ep;

	// manually fill in stuff, not an object it is a struct (now)

	uuid_generate(ep.uuid_);
	ep.pollfdsKey_ = cr.cr_handle;
	ep.psp_ = cr.psp;
	ep.ia_ = this;

	mutex_.take();
	endpointRegistry_.push_back(ep);
	//uuidRegistry_.push_back(ep->uuid_);
	mutex_.give();

	//return ep;
	LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), "Creating EndPoint from cr in IA");
	//std::cout << "Create EndPoint in IA UUID = " << ep.toString() << std::endl;
	return ep;
}

void tcpla::InterfaceAdapter::destroyEndPoint (tcpla::EndPoint & ep_handle) throw (tcpla::exception::IACommandQueueFull)
{
	mutex_.take();

	//std::cout << " ---->>>>> tcpla::InterfaceAdapter::destroyEndPointtcpla::EndPoint * ep_handle ) push into queue" << std::endl;
	try
	{
		tcpla::InterfaceAdapter::Command c;

		c.command_number = TCPLA_DESTROY_COMMAND_NUMBER;
		c.command.destroy_command.ep_handle = ep_handle;

		commandQueue_->push_back(c);

		mutex_.give();
	}
	catch (toolbox::exception::RingListFull & e)
	{
		mutex_.give();

		std::stringstream ss;
		ss << "Failed to post destroy EndPoint in InterfaceAdpater " << this->hostname_;

		XCEPT_RETHROW(tcpla::exception::IACommandQueueFull, ss.str(), e);
	}
}

void tcpla::InterfaceAdapter::destroyEndPoint (tcpla::InterfaceAdapter::Command & c) throw (tcpla::exception::Exception)
{
	tcpla::EndPoint & ep_handle = c.command.destroy_command.ep_handle;
	//std::cout << "ATTEMPTING destroy EndPoint in IA UUID = " << ep_handle.toString() << std::endl;

	mutex_.take();

	if (ep_handle.psp_ != 0) //endpoint belongs to a PSP so delegate destroy of endpoint to PSP loop
	{
		ep_handle.psp_->destroyEndPoint(ep_handle);
		mutex_.give();
	}
	else
	{
		std::list<tcpla::EndPoint>::iterator i;
		for (i = endpointRegistry_.begin(); i != endpointRegistry_.end(); i++)
		{
			if ((*i) == ep_handle)
			{
				try
				{
					this->resetSendEntry(ep_handle);
				}
				catch (tcpla::exception::InvalidEndPoint & e)
				{
					mutex_.give();
					std::stringstream ss;
					ss << "Failed to disconnect invalid EndPoint in InterfaceAdpater " << this->hostname_;
					XCEPT_RETHROW(tcpla::exception::Exception, ss.str(), e);
				}
				catch (tcpla::exception::UnitializedEndPoint & e)
				{
					mutex_.give();
					std::stringstream ss;
					ss << "Failed to disconnect uninitialized EndPoint in InterfaceAdpater " << this->hostname_;
					XCEPT_RETHROW(tcpla::exception::Exception, ss.str(), e);
				}

				endpointRegistry_.erase(i);
				break;
			}
		}
		if (i == endpointRegistry_.end())
		{
			LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), "Attempt to destroy duplicate end point in IA (tolerated) - UUID = " << ep_handle.toString());
		}

		mutex_.give();
	}
}

tcpla::PublicServicePoint* tcpla::InterfaceAdapter::listen (pt::Address::Reference address) throw (tcpla::exception::FailedToCreatePSP)
{
	tcpla::PublicServicePoint* psp = 0;
	tcpla::Address & listenAddress = dynamic_cast<tcpla::Address &>(*address);

	try
	{
		if (listenAddress.getProperty("rmode") == "poll")
		{
			psp = new tcpla::PublicServicePointPoll(this, address, ioQueueSize_, this->ghf_());
			LOG4CPLUS_DEBUG(tcpla::InterfaceAdapter::getOwnerApplication()->getApplicationLogger(), "Using PublicServicePointPoll");
		}
		else if (listenAddress.getProperty("rmode") == "select")
		{
			psp = new tcpla::PublicServicePointSelect(this, address, ioQueueSize_, this->ghf_());
			LOG4CPLUS_DEBUG(tcpla::InterfaceAdapter::getOwnerApplication()->getApplicationLogger(), "Using PublicServicePointSelect");
		}
		else
		{
			XCEPT_RAISE(tcpla::exception::FailedToCreatePSP, "invalid rmode specified");
		}
	}
	catch (tcpla::exception::InvalidRcvTimeOut & e)
	{

		std::stringstream ss;
		ss << "Failed to create PublicServicePoint for address " << listenAddress.toString() << " in InterfaceAdapter " << this->hostname_;

		XCEPT_RETHROW(tcpla::exception::FailedToCreatePSP, ss.str(), e);
	}
	catch (tcpla::exception::NoFreePortInRange & e)
	{

		std::stringstream ss;
		ss << "Failed to create PublicServicePoint for address " << listenAddress.toString() << " in InterfaceAdapter " << this->hostname_;

		XCEPT_RETHROW(tcpla::exception::FailedToCreatePSP, ss.str(), e);
	}
	catch (tcpla::exception::CannotBind & e)
	{

		std::stringstream ss;
		ss << "Failed to create PublicServicePoint for address " << listenAddress.toString() << " in InterfaceAdapter " << this->hostname_;

		XCEPT_RETHROW(tcpla::exception::FailedToCreatePSP, ss.str(), e);
	}
	catch (tcpla::exception::Exception & e)
	{

		std::stringstream ss;
		ss << "Failed to create PublicServicePoint for address " << listenAddress.toString() << " in InterfaceAdapter " << this->hostname_;

		XCEPT_RETHROW(tcpla::exception::FailedToCreatePSP, ss.str(), e);
	}

	pspRegistry_.push_back(psp);

	return psp;
}

void tcpla::InterfaceAdapter::shutdown (tcpla::PublicServicePoint* psp) throw (tcpla::exception::IAShutdownFailed)
{
	for (std::list<tcpla::PublicServicePoint*>::iterator i = pspRegistry_.begin(); i != pspRegistry_.end(); i++)
	{
		if ((*i) == psp)
		{
			pspRegistry_.erase(i);
			delete psp;
			return;
		}
	}

	std::stringstream ss;
	ss << "Cannot destroy PSP, not found in InterfaceAdpater " << this->hostname_;

	XCEPT_RAISE(tcpla::exception::IAShutdownFailed, ss.str());

}

void tcpla::InterfaceAdapter::connect (tcpla::EndPoint & ep_handle, tcpla::Address * local, tcpla::Address * destination, TCPLA_CONTEXT & cookie)
throw (tcpla::exception::InvalidFamily, tcpla::exception::HostnameResolveFailed, tcpla::exception::IACommandQueueFull)
{
	mutex_.take();

	try
	{
		tcpla::InterfaceAdapter::Command c;

		c.command_number = TCPLA_CONNECT_COMMAND_NUMBER;
		c.command.connect_command.ep_handle = ep_handle;
		c.command.connect_command.cookie = cookie;

		//struct sockaddr_in sockaddress;

		try
		{
			c.command.connect_command.lsockaddress  = toolbox::net::getSocketAddressByName(local->getProperty("hostname"), (uint16_t)(toolbox::toUnsignedLong(local->getProperty("port"))));
		}
		catch (toolbox::exception::Exception & e)
		{
			std::stringstream ss;
			ss << "Cannot resolve local address " << local->toString() << " in InterfaceAdapter " << this->hostname_;

			XCEPT_RETHROW(tcpla::exception::HostnameResolveFailed, ss.str(), e);
		}

		//struct sockaddr_in destinationSock;

		try
		{
			c.command.connect_command.dsockaddress = toolbox::net::getSocketAddressByName(destination->getProperty("hostname"), (uint16_t)(toolbox::toUnsignedLong(destination->getProperty("port"))));
		}
		catch (toolbox::net::exception::UnresolvedAddress & e)
		{
			std::stringstream ss;
			ss << "Cannot resolve destination address " << destination->toString() << " in InterfaceAdapter " << this->hostname_;

			XCEPT_RETHROW(tcpla::exception::HostnameResolveFailed, ss.str(), e);
		}

		c.command.connect_command.nonblock = false;
		if (local->getProperty("nonblock") == "true")
		{
			c.command.connect_command.nonblock = true;
		}

		if (local->getProperty("family") == "AF_INET")
		{
			c.command.connect_command.family = AF_INET;
		}
		else
		{
			std::stringstream ss;
			ss << "Failed to set socket invalid family " << local->getProperty("family") << " type in InterfaceAdapter" << this->hostname_;

			XCEPT_RAISE(tcpla::exception::InvalidFamily, ss.str());
		}

		commandQueue_->push_back(c);

		mutex_.give();
	}
	catch (toolbox::exception::RingListFull & e)
	{
		mutex_.give();
		std::stringstream ss;
		ss << "Failed to post connect in InterfaceAdpater " << this->hostname_;

		XCEPT_RETHROW(tcpla::exception::IACommandQueueFull, ss.str(), e);
	}
}

int tcpla::InterfaceAdapter::createSocket (int family, bool nonblock, struct sockaddr_in & sockaddress) throw (tcpla::exception::Exception, tcpla::exception::SocketOption, tcpla::exception::CannotBind, tcpla::exception::HostnameResolveFailed)
{
	int fd;

	fd = socket(family, SOCK_STREAM, 0);
	/*
	if (local->getProperty("family") == "AF_INET")
	{
		fd = socket(AF_INET, SOCK_STREAM, 0);
	}
	else if ( local->getProperty("family") == "AF_INET_SDP")
	 {
	 fd = socket(AF_INET_SDP, SOCK_STREAM, 0);
	 }
	else
	{
		std::stringstream ss;
		ss << "Failed to set socket invalid family " << local->getProperty("family") << " type in InterfaceAdapter" << this->hostname_;

		XCEPT_RAISE(tcpla::exception::Exception, ss.str());
	}
	*/
	//set to non-blocking

	errno = 0;

	if (nonblock)
	{
		int sockOpt = fcntl(fd, F_GETFL, 0);
		if (fcntl(fd, F_SETFL, sockOpt | O_NONBLOCK) < 0)
		{
			::close(fd);

			std::stringstream ss;
			ss << "Failed to set socket to non-blocking in InterfaceAdapter " << this->hostname_ << " socket error: " << strerror(errno);

			XCEPT_RAISE(tcpla::exception::SocketOption, ss.str());
		}
	}

	/*
	struct sockaddr_in sockaddress;

	try
	{
		sockaddress = toolbox::net::getSocketAddressByName(local->getProperty("hostname"), (uint16_t)(toolbox::toUnsignedLong(local->getProperty("port"))));
	}
	catch (toolbox::exception::Exception & e)
	{
		::close(fd);

		std::stringstream ss;
		ss << "Cannot resolve local address " << local->toString() << " in InterfaceAdapter " << this->hostname_;

		XCEPT_RETHROW(tcpla::exception::HostnameResolveFailed, ss.str(), e);
	}
	*/

	errno = 0;

	sockaddress.sin_port = htons(0);
	socklen_t sockaddressSize = sizeof(sockaddress);
	if (::bind(fd, (struct sockaddr *) &sockaddress, sockaddressSize) == -1)
	{
		::close(fd);

		std::stringstream ss;
		//ss << "Cannot bind local address " << local->toString() << " in InterfaceAdapter " << this->hostname_ << " socket error: " << strerror(errno);

		XCEPT_RAISE(tcpla::exception::CannotBind, ss.str());
	}

	return fd;
}

void tcpla::InterfaceAdapter::postSendBuffer (tcpla::EndPoint & ep_handle, TCPLA_CONTEXT & cookie, char * local_iov, size_t size, toolbox::exception::HandlerSignature* handler, void* context, uint16_t identification, uint8_t flags) throw (tcpla::exception::EndPointNotConnected, tcpla::exception::FailedToPostSend, tcpla::exception::InvalidEndPoint)
{
	//added mutex protection to queue push - cw
//#warning "this call should be protected by a mutex for multiple clients"

	if (ep_handle.pollfdsKey_ >= 0)
	{
		TCPLA_SEND_POST_DESC pd;

		pd.cookie = cookie;
		pd.local_iov[0].iov_base = local_iov;
		pd.local_iov[0].iov_len = size;
		pd.iovs = 1;
		pd.identification = identification;
		pd.context = context;
		pd.handler = handler;
		pd.flags = flags;

		//std::stringstream ss;
		//ss << "pushing to send queue: " << size << "pollfdsKey: " << ep_handle.pollfdsKey_;

		//LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), ss.str());
		if (!sendSocks_[ep_handle.pollfdsKey_].connected || sendSocks_[ep_handle.pollfdsKey_].failed)
		{
			tcpla::Event event;

			event.event_number = TCPLA_POST_BLOCK_FAILED;
			event.event_data.tcpla_dto_block_event_data.user_cookie = cookie;
			event.event_data.tcpla_dto_block_event_data.block_length = size;
			event.event_data.tcpla_dto_block_event_data.buffer = local_iov;
			event.event_data.tcpla_dto_block_event_data.handler = handler;
			event.event_data.tcpla_dto_block_event_data.context = context;

			this->pushEvent(reqt_evd_hdl_, event);

			/*std::stringstream ss;
			 ss << "Failed to enqueue buffer for index " << ep_handle.pollfdsKey_ << " of size " << size << " in InterfaceAdapter " << this->hostname_;

			 XCEPT_RAISE(tcpla::exception::EndPointNotConnected, ss.str());*/
			return;

		}

		try
		{
			//mutex_.take();
			sendSocks_[ep_handle.pollfdsKey_].postQueue->push_back(pd);
			//mutex_.give();
		}
		catch (toolbox::exception::RingListFull & e)
		{
			//mutex_.give();

			std::stringstream ss;
			ss << "Failed to enqueue buffer for index " << ep_handle.pollfdsKey_ << " of size " << size << " in InterfaceAdapter " << this->hostname_;

			XCEPT_RETHROW(tcpla::exception::FailedToPostSend, ss.str(), e);
		}
	}
	else
	{
		std::stringstream ss;
		ss << "Invalid index " << ep_handle.pollfdsKey_ << " for EndPoint in InterfaceAdapter " << this->hostname_;

		XCEPT_RAISE(tcpla::exception::InvalidEndPoint, ss.str());
	}
}

void tcpla::InterfaceAdapter::postSendBuffer (tcpla::EndPoint & ep_handle, TCPLA_CONTEXT & cookie, struct iovec * local_iov, size_t iovs, toolbox::exception::HandlerSignature* handler, void* context, uint16_t identification, uint8_t flags) throw (tcpla::exception::FailedToPostSend, tcpla::exception::InvalidEndPoint)
{
	if (iovs > TCPLA_MAX_IOV_SIZE || (local_iov == 0 && iovs != 0))
	{
		std::stringstream ss;
		ss << "Failed to post insufficient resources: max allowed " << TCPLA_MAX_IOV_SIZE << " in InterfaceAdapter " << this->hostname_;

		XCEPT_RAISE(tcpla::exception::FailedToPostSend, ss.str());
	}

	if (ep_handle.pollfdsKey_ >= 0)
	{
		TCPLA_SEND_POST_DESC pd;

		bzero(pd.local_iov, sizeof(struct iovec) * TCPLA_MAX_IOV_SIZE);

		pd.cookie = cookie;

		for (size_t i = 0; i < iovs; i++)
		{
			pd.local_iov[i] = local_iov[i];
		}

		pd.iovs = iovs;
		pd.identification = identification;
		pd.context = context;
		pd.handler = handler;
		pd.flags = flags;

		//std::stringstream ss;
		//ss << "pushing to send queue: " << size << "pollfdsKey: " << ep_handle.pollfdsKey_;

		//LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), ss.str());
		if (!sendSocks_[ep_handle.pollfdsKey_].connected || sendSocks_[ep_handle.pollfdsKey_].failed)
		{
			tcpla::Event event;

			event.event_data.tcpla_dto_block_event_data.handler = handler;
			event.event_data.tcpla_dto_block_event_data.context = context;
			event.event_data.tcpla_dto_block_event_data.user_cookie = cookie;
			event.event_number = TCPLA_POST_BLOCK_FAILED;
			event.event_data.tcpla_dto_block_event_data.block_length = 0;
			event.event_data.tcpla_dto_block_event_data.buffer = 0;

			this->pushEvent(reqt_evd_hdl_, event);

			return;
		}

		try
		{
			//mutex_.take();
			sendSocks_[ep_handle.pollfdsKey_].postQueue->push_back(pd);
			//mutex_.give();
		}
		catch (toolbox::exception::RingListFull & e)
		{
			//mutex_.give();

			std::stringstream ss;
			ss << "Failed to enqueue buffer for index " << ep_handle.pollfdsKey_ << " in InterfaceAdapter " << this->hostname_;

			XCEPT_RETHROW(tcpla::exception::FailedToPostSend, ss.str(), e);
		}
	}
	else
	{
		std::stringstream ss;
		ss << "Invalid index " << ep_handle.pollfdsKey_ << " for EndPoint in InterfaceAdapter " << this->hostname_;

		XCEPT_RAISE(tcpla::exception::InvalidEndPoint, ss.str());
	}
}

int tcpla::InterfaceAdapter::sendNonBlocking (int socket, const char * buf, int len) throw (tcpla::exception::FailedToSend)
{
	int nBytes = 0;

	errno = 0;

	nBytes = ::write(socket, buf, len);

	if (nBytes < 0)
	{
		if ( /*errno == EWOULDBLOCK ||*/errno == EAGAIN)
		{
			return 0;
		}
		else
		{

			std::stringstream ss;
			ss << "Failed to send non-blocking in Interface Adapter " << this->hostname_ << " buff: " << (unsigned long) buf << " lenL " << len << " socket error: " << strerror(errno);

			XCEPT_RAISE(tcpla::exception::FailedToSend, ss.str());
		}
	}

	return nBytes;
}

void tcpla::InterfaceAdapter::reset (int key) throw (tcpla::exception::InvalidPollfdsKey)
{
	if (key < this->maxClients_)
	{
		sendSocks_[key].written = 0;
		sendSocks_[key].current.iovs = 0;
		sendSocks_[key].current.handler = 0;
		sendSocks_[key].current.identification = 0;
		sendSocks_[key].current.context = 0;
	}
	else
	{
		std::stringstream ss;
		ss << "invalid index" << key << ", cannot reset output entry in InterfaceAdapter " << this->hostname_;

		XCEPT_RAISE(tcpla::exception::InvalidPollfdsKey, ss.str());
	}
}

bool tcpla::InterfaceAdapter::isConnected (tcpla::EndPoint & ep_handle)
{
	return (ep_handle.pollfdsKey_ >= 0) ? sendSocks_[ep_handle.pollfdsKey_].connected : false;
}

bool tcpla::InterfaceAdapter::hasFailed (tcpla::EndPoint & ep_handle)
{
	return (ep_handle.pollfdsKey_ >= 0) ? sendSocks_[ep_handle.pollfdsKey_].failed : false;
}

bool tcpla::InterfaceAdapter::canPost (tcpla::EndPoint & ep_handle, size_t total)
{
	if ((ioQueueSize_ - sendSocks_[ep_handle.pollfdsKey_].postQueue->elements()) >= total)
	{
		return true;
	}

	return false;
}

void tcpla::InterfaceAdapter::eval ()
{
	Command cmd = commandQueue_->front();
	commandQueue_->pop_front();

	if (cmd.command_number == TCPLA_CONNECT_COMMAND_NUMBER)
	{
		try
		{
			this->connect(cmd);
		}
		catch (tcpla::exception::MaxConnections & e)
		{
			/*
			 * This is the same response for each error, though some are configuration problems, and some are socket errors
			 * Changing the events that are fired means reworking pt::uTCP to accommodate the split of responsibility
			 */
			tcpla::Event event;

			event.event_number = TCPLA_CONNECTION_EVENT_PEER_REJECTED;
			event.event_data.asynch_error_event_data.ep_handle = cmd.command.connect_command.ep_handle;
			event.event_data.asynch_error_event_data.reason = 7;

			std::stringstream ss;
			ss << "Failed to process connect command in InterfaceAdapter " << this->hostname_;

			event.event_data.asynch_error_event_data.exception = TCPLA_XCEPT_ALLOC_NESTED(tcpla::exception::FailedToProcessConnectCommand, ss.str(), e);

			this->pushEvent(reqt_evd_hdl_, event);
		}
		catch (tcpla::exception::FailedToClearSendQueue & e)
		{
			tcpla::Event event;

			event.event_number = TCPLA_CONNECTION_EVENT_PEER_REJECTED;
			event.event_data.asynch_error_event_data.ep_handle = cmd.command.connect_command.ep_handle;
			event.event_data.asynch_error_event_data.reason = 7;

			std::stringstream ss;
			ss << "Failed to process connect command in InterfaceAdapter " << this->hostname_;

			event.event_data.asynch_error_event_data.exception = TCPLA_XCEPT_ALLOC_NESTED(tcpla::exception::FailedToProcessConnectCommand, ss.str(), e);

			this->pushEvent(reqt_evd_hdl_, event);
		}
		catch (tcpla::exception::InvalidPollfdsKey & e)
		{
			tcpla::Event event;

			event.event_number = TCPLA_CONNECTION_EVENT_PEER_REJECTED;
			event.event_data.asynch_error_event_data.ep_handle = cmd.command.connect_command.ep_handle;
			event.event_data.asynch_error_event_data.reason = 7;

			std::stringstream ss;
			ss << "Failed to process connect command in InterfaceAdapter " << this->hostname_;

			event.event_data.asynch_error_event_data.exception = TCPLA_XCEPT_ALLOC_NESTED(tcpla::exception::FailedToProcessConnectCommand, ss.str(), e);

			this->pushEvent(reqt_evd_hdl_, event);
		}
		catch (tcpla::exception::HostnameResolveFailed & e)
		{
			tcpla::Event event;

			event.event_number = TCPLA_CONNECTION_EVENT_PEER_REJECTED;
			event.event_data.asynch_error_event_data.ep_handle = cmd.command.connect_command.ep_handle;
			event.event_data.asynch_error_event_data.reason = 7;

			std::stringstream ss;
			ss << "Failed to process connect command in InterfaceAdapter " << this->hostname_;

			event.event_data.asynch_error_event_data.exception = TCPLA_XCEPT_ALLOC_NESTED(tcpla::exception::FailedToProcessConnectCommand, ss.str(), e);

			this->pushEvent(reqt_evd_hdl_, event);
		}
		catch (tcpla::exception::CannotConnect & e)
		{
			tcpla::Event event;

			event.event_number = TCPLA_CONNECTION_EVENT_PEER_REJECTED;
			event.event_data.asynch_error_event_data.ep_handle = cmd.command.connect_command.ep_handle;
			event.event_data.asynch_error_event_data.reason = 7;

			std::stringstream ss;
			ss << "Failed to process connect command in InterfaceAdapter " << this->hostname_;

			event.event_data.asynch_error_event_data.exception = TCPLA_XCEPT_ALLOC_NESTED(tcpla::exception::FailedToProcessConnectCommand, ss.str(), e);

			this->pushEvent(reqt_evd_hdl_, event);
		}
	}
	else if (cmd.command_number == TCPLA_DESTROY_COMMAND_NUMBER)
	{
		try
		{
			this->destroyEndPoint(cmd);
		}
		catch (tcpla::exception::Exception & e)
		{
			tcpla::Event event;

			event.event_number = TCPLA_ASYNC_ERROR_EP_BROKEN;
			event.event_data.asynch_error_event_data.ep_handle = cmd.command.destroy_command.ep_handle;
			event.event_data.asynch_error_event_data.reason = 8;

			std::stringstream ss;
			ss << "Failed to process destroy EndPoint command in InterfaceAdapter " << this->hostname_;

			event.event_data.asynch_error_event_data.exception = TCPLA_XCEPT_ALLOC_NESTED(tcpla::exception::FailedToProcessDestroyEndPointCommand, ss.str(), e);

			this->pushEvent(reqt_evd_hdl_, event);
		}
	}
	else
	{
		tcpla::Event event;

		event.event_number = TCPLA_SOFTWARE_EVENT;
		event.event_data.software_event_data.reason = 9;

		std::stringstream ss;
		ss << "Failed to process unknown command in InterfaceAdapter " << this->hostname_;

		event.event_data.software_event_data.exception = TCPLA_XCEPT_ALLOC(tcpla::exception::Exception, ss.str());

		this->pushEvent(reqt_evd_hdl_, event);
	}
}

void tcpla::InterfaceAdapter::processSocketEntries ()
{
	/*
	 fd_set sleepset;
	 fd_set readyset;
	 FD_ZERO(&sleepset);
	 FD_ZERO(&readyset);
	 */
	size_t readyset = 0;
	size_t sleepset = 0;

	for (int i = 0; i <= this->maxPollfdsKey_; i++)
	{

		int pollfdsKey = sendSocks_[i].pollfdsKey;

		if (pollfdsKey == -1)
		{
			// skip this entry
			continue;
		}

		int fd = this->getFD(pollfdsKey);

		//FD_SET(fd,&readyset);
		// check this fd is ready for writing
		if (this->isReady(pollfdsKey))
		{
			readyset++;
			for (size_t cycle = 0; cycle < this->pollingCycle_; cycle++)
			{
				if (!sendSocks_[i].connected) // the socket is finally connected
				{
					sendSocks_[i].idleEmptyQueueCounter++;
					/*
				 * 		The socket is nonblocking and the connection cannot be
					  completed immediately.  It is possible to select(2) or poll(2)
					  for completion by selecting the socket for writing.  After
					  select(2) indicates writability, use getsockopt(2) to read the
					  SO_ERROR option at level SOL_SOCKET to determine whether
					  connect() completed successfully (SO_ERROR is zero) or
					  unsuccessfully (SO_ERROR is one of the usual error codes
					  listed here, explaining the reason for the failure).
					 */

					int rbuf = 0;
					int ret = 0;
					socklen_t len = sizeof(int);
					ret = getsockopt(fd, SOL_SOCKET, SO_ERROR, &rbuf, &len);
					if (ret != 0)
					{
						// error
						LOG4CPLUS_FATAL(this->getOwnerApplication()->getApplicationLogger(), "Fatal: connection failure, could not getsockopt");
					}
					if ( rbuf != 0)
					{
						this->disableFD(pollfdsKey);
						std::stringstream ss;
						ss << "connection failed with error : " << strerror(rbuf);
						// not connected yet or error
						tcpla::Event event;

						event.event_number = TCPLA_CONNECTION_EVENT_UNREACHABLE;
						event.event_data.connect_error_event_data.ep_handle = sendSocks_[i].ep_handle;
						event.event_data.connect_error_event_data.reason = 1;
						event.event_data.connect_error_event_data.exception = TCPLA_XCEPT_ALLOC (tcpla::exception::CannotConnect, ss.str());

						this->pushEvent(reqt_evd_hdl_, event);
						sleepset++;
						break;
					}

					LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), "Connection established in IA");
					tcpla::Event event; // client event

					event.event_number = TCPLA_CONNECTION_EVENT_ESTABLISHED;
					event.event_data.connect_event_data.ep_handle = sendSocks_[i].ep_handle;
					event.event_data.connect_event_data.user_cookie = sendSocks_[i].cookie;
					sendSocks_[i].connected = true;

					this->pushEvent(conn_evd_hdl_, event);
					sleepset++;
					break;
				}

				// check if somethig to be enqueued
				if (sendSocks_[i].current.iovs == 0 && sendSocks_[i].written == 0)
				{
					if (sendSocks_[i].postQueue->empty())
					{
						sendSocks_[i].idleEmptyQueueCounter++;
						//FD_SET(fd, &sleepset);
						sleepset++;
						break;
					}

					sendSocks_[i].current = sendSocks_[i].postQueue->front();
					sendSocks_[i].iov_index = 0;

				}

				size_t iov_index = sendSocks_[i].iov_index;

				if ((iov_index < sendSocks_[i].current.iovs) && (sendSocks_[i].written < sendSocks_[i].current.local_iov[iov_index].iov_len))
				{
					int size = sendSocks_[i].current.local_iov[iov_index].iov_len - sendSocks_[i].written;
					char * cursor = (char*) (sendSocks_[i].current.local_iov[iov_index].iov_base) + sendSocks_[i].written;

					try
					{
						//toolbox::hexdump ((void*) cursor, 256);

						int nBytes = this->sendNonBlocking(fd, cursor, size);
						sendSocks_[i].writeCounter++;
						if (nBytes == 0)
						{ //socket would block break
							sendSocks_[i].idleBlockingWriteCounter++;
							sleepset++;
							break;
						}
						sendSocks_[i].written += nBytes;

						//std::cout << "Sent " << nBytes << " in IA" << std::endl;

						if (sendSocks_[i].current.local_iov[iov_index].iov_len == sendSocks_[i].written)
						{
							/*if ( sendSocks_[i].written != 2088 )
							 {
							 std::cout << "BAD SEND SIZE!!!!!!!!!!!!!!! " <<  sendSocks_[i].written << std::endl;
							 }

							 if ( sendSocks_[i].current.local_iov[iov_index].iov_len != 2088 )
							 {
							 std::cout << "BAD LENGTH SIZE!!!!!!!!!!!!!!! " <<  sendSocks_[i].current.local_iov[iov_index].iov_len << std::endl;
							 }*/

							sendSocks_[i].iov_index++;
							sendSocks_[i].written = 0;
						}
					}
					catch (tcpla::exception::FailedToSend & e)
					{
						TCPLA_SEND_POST_DESC d = sendSocks_[i].postQueue->front();

						tcpla::Event event;

						std::stringstream msg;
						std::string ipnumber = inet_ntoa(sendSocks_[i].ip_addr.sin_addr);
						struct hostent *he;
						he = gethostbyaddr(&(sendSocks_[i].ip_addr.sin_addr), sizeof(in_addr), AF_INET);
						std::string hostname = he->h_name;
						msg << "Non-blocking send to host " << hostname << " on port " << sendSocks_[i].port << "(IP:" << ipnumber << ")";
						
						event.event_number = TCPLA_ASYNC_ERROR_PROVIDER_INTERNAL_ERROR;
						event.event_data.asynch_error_event_data.ep_handle = sendSocks_[i].ep_handle;
						event.event_data.asynch_error_event_data.reason = 3;
						event.event_data.asynch_error_event_data.exception = TCPLA_XCEPT_ALLOC_NESTED (tcpla::exception::FailedToSend, msg.str(), e);

						//Set the cookie to NULL
						//event.event_data.asynch_error_event_data.user_cookie = d.cookie;
						tcpla::TCPLA_CONTEXT emptyCookie;
						memset(&emptyCookie, 0, sizeof(emptyCookie));
						event.event_data.asynch_error_event_data.user_cookie = emptyCookie;

						event.event_data.asynch_error_event_data.handler = d.handler;
						event.event_data.asynch_error_event_data.context = d.context;

						 // currently disable this remove from send queue and let recovery through destruction of endpoint recycle pending buffers.
						 //sendSocks_[i].postQueue->pop_front();
						 // It might happen that this exception occurs several times because of retries.
						 //  Need to be stress tested.

						this->pushEvent(reqt_evd_hdl_, event);

						sleepset++;
						break;

					}
				}

				if (sendSocks_[i].iov_index == sendSocks_[i].current.iovs)
				{
					//TCPLA_SEND_POST_DESC d = sendSocks_[i].postQueue->front();

					sendSocks_[i].postQueue->pop_front();
					sendSocks_[i].counter++;

					tcpla::Event event; // client event

					event.event_number = TCPLA_DTO_SND_COMPLETION_EVENT;
					event.event_data.dto_completion_event_data.ep_handle = sendSocks_[i].ep_handle;
					event.event_data.dto_completion_event_data.user_cookie = sendSocks_[i].current.cookie;
					/*event.event_data.dto_completion_event_data.identification = sendSocks_[i].current.identification;
					 event.event_data.dto_completion_event_data.total = sendSocks_[i].current.total;
					 event.event_data.dto_completion_event_data.packet = sendSocks_[i].current.packet;*/
					event.event_data.dto_completion_event_data.flags = sendSocks_[i].current.flags;
					try
					{
						this->reset(i);
					}
					catch (tcpla::exception::InvalidPollfdsKey & e)
					{
						//std::cerr << "panic, internal error, cannot reset output entry" << std::endl;
						//continue;

						tcpla::Event event;

						event.event_number = TCPLA_ASYNC_ERROR_IA_CATASTROPHIC;

						tcpla::EndPoint emptyEP;
						memset(&emptyEP, 0, sizeof(emptyEP));
						event.event_data.asynch_error_event_data.ep_handle = emptyEP;

						event.event_data.asynch_error_event_data.reason = 10;
						event.event_data.asynch_error_event_data.exception = TCPLA_XCEPT_ALLOC (tcpla::exception::Exception, e.message());

						tcpla::TCPLA_CONTEXT emptyCookie;
						memset(&emptyCookie, 0, sizeof(emptyCookie));
						event.event_data.asynch_error_event_data.user_cookie = emptyCookie;
						event.event_data.asynch_error_event_data.handler = 0;
						event.event_data.asynch_error_event_data.context = 0;

						this->pushEvent(reqt_evd_hdl_, event);
						sleepset++;

						continue; // still deliver the sent message command

					}

					if (singleThread_)
					{
						eventHandler_->handleEvent(event);

						continue;
					}

					this->pushEvent(this->creq_evd_hdl_, event);
				}
			} //end for cycle
		} // if ready
	} // end for socket entries

	//add listener socket fd to sleepset
	if (readyset <= sleepset)
	{
		usleep(10); // should be enough to force a context switch
	}
}

void tcpla::InterfaceAdapter::pushEvent (tcpla::EventDispatcher * evd, tcpla::Event & event)
{
	try
	{
		evd->push(event);
	}
	catch (tcpla::exception::EventQueueFull & e)
	{
		std::stringstream ss;
		ss << "Failed to push '" << tcpla::eventToString(event.event_number) << "' event (" << event.event_number << ") queue overflow";

		tcpla::Event asyncevent;

		asyncevent.event_number = TCPLA_ASYNC_ERROR_EVD_OVERFLOW;
		asyncevent.event_data.asynch_error_event_data.exception = TCPLA_XCEPT_ALLOC_NESTED(tcpla::exception::EventQueueFull, ss.str(), e);

		asyncevent.event_data.asynch_error_event_data.reason = 0;
		asyncevent.event_data.asynch_error_event_data.ep_handle.clear();
		asyncevent.event_data.asynch_error_event_data.handler = 0;
		asyncevent.event_data.asynch_error_event_data.context = 0;
		asyncevent.event_data.asynch_error_event_data.user_cookie.as_ptr = 0;

		if (event.event_number == TCPLA_DTO_RCV_COMPLETION_EVENT)
		{
			asyncevent.event_data.asynch_error_event_data.reason = 13;
			asyncevent.event_data.asynch_error_event_data.ep_handle = event.event_data.dto_completion_event_data.ep_handle;
			asyncevent.event_data.asynch_error_event_data.user_cookie = event.event_data.dto_completion_event_data.user_cookie;
		}

		eventHandler_->handleEvent(asyncevent);

		/*
		 	TCPLA_ASYNC_ERROR_EVD_OVERFLOW
		    tcpla::EndPoint ep_handle;
			size_t reason;
			xcept::Exception * exception;
			toolbox::exception::HandlerSignature* handler;
			TCPLA_CONTEXT user_cookie;
			void* context;


			TCPLA_DTO_RCV_COMPLETION_EVENT
			event.event_data.dto_completion_event_data.user_cookie = rcvSocks_[j].current.cookie;
			event.event_data.dto_completion_event_data.ep_handle = rcvSocks_[j].ep_handle;
			event.event_data.dto_completion_event_data.transfered_length = rcvSocks_[j].rcvnBytes;
			event.event_data.dto_completion_event_data.psp = this;

			event.event_data.dto_completion_event_data.block_length = rcvSocks_[j].current.size;
			event.event_data.dto_completion_event_data.buffer = rcvSocks_[j].current.local_iov;

			//chain info
			event.event_data.dto_completion_event_data.identification = rcvSocks_[j].identification;
		 */
	}
}

namespace tcpla
{

	std::ostream& operator<< (std::ostream &sout, tcpla::InterfaceAdapter & ia)
	{
		sout << cgicc::table().set("class", "xdaq-table") << std::endl;

		/*
		sout << cgicc::tr();
		sout << cgicc::th().set("colspan", "15").set("style", "text-align:center");
		sout << ia.id_;
		sout << cgicc::th();
		sout << cgicc::tr() << std::endl;
		*/

		sout << "<caption>" << ia.id_ << "</caption>" << std::endl;

		sout << cgicc::thead() << std::endl;
		sout << cgicc::tr();
		sout << cgicc::th("IP");
		sout << cgicc::th("Host");
		sout << cgicc::th("Port");
		sout << cgicc::th("Poll FDS Key");
		sout << cgicc::th("FD");
		sout << cgicc::th("Connected");
		sout << cgicc::th("Failed");
		sout << cgicc::th("Buffer Status");
		//sout << cgicc::th("To Write (Bytes)");
		sout << cgicc::th("Written (Bytes)");
		sout << cgicc::th("Queue");
		sout << cgicc::th("Messages Sent");
		sout << cgicc::th("Socket Writes");
		sout << cgicc::th("Idle Empty Queue");
		sout << cgicc::th("Idle Blocking");
		sout << cgicc::th("UUID");
		sout << cgicc::tr() << std::endl;
		sout << cgicc::thead() << std::endl;

		// Counters
		size_t written = 0;
		size_t queue = 0;
		size_t sent = 0;
		size_t sockWrites = 0;
		size_t idleEmpty = 0;
		size_t idleBlock = 0;

		size_t rows = 0;

		bool initBody = false;

		for (int j = 0; j < ia.maxClients_; j++)
		{
			if (ia.sendSocks_[j].pollfdsKey != -1)
			{
				if (!initBody)
				{
					initBody = true;
					sout << cgicc::tbody() << std::endl;
				}

				std::stringstream port;
				port << ntohs(ia.sendSocks_[j].ip_addr.sin_port);

				std::string IPNumber = inet_ntoa(ia.sendSocks_[j].ip_addr.sin_addr);

				struct hostent *he;
				he = gethostbyaddr(&(ia.sendSocks_[j].ip_addr.sin_addr), sizeof(in_addr), AF_INET);
				std::string hostname = he->h_name;

				//std::cout << "HYP : hostname = '" << hostname << "', port = '" << ia.sendSocks_[j].port << "', port string = '" << port.str() << "'" << std::endl;

				sout << cgicc::tr();
				sout << cgicc::td(IPNumber);
				sout << cgicc::td(hostname);
				sout << cgicc::td(port.str());
				sout << cgicc::td(toolbox::toString("%d", ia.sendSocks_[j].pollfdsKey));
				sout << cgicc::td(toolbox::toString("%d", ia.getFD(ia.sendSocks_[j].pollfdsKey)));
				sout << cgicc::td((ia.sendSocks_[j].connected) ? "true" : "<span style=\"color: red; font-weight: bold\">false</span>");
				sout << cgicc::td((ia.sendSocks_[j].failed) ? "<span style=\"color: red; font-weight: bold\">true</span>" : "false");
				sout << cgicc::td((ia.sendSocks_[j].current.local_iov != 0) ? "sending" : "pending");

				//int toWrite = ia.sendSocks_[j].current.local_iov[ia.sendSocks_[j].iov_index].iov_len;

				//sout << cgicc::td(toolbox::toString("%d",  ( toWrite > 0  ? toWrite : 0 )));
				sout << cgicc::td(toolbox::toString("%d", ia.sendSocks_[j].written));
				sout << cgicc::td(toolbox::toString("%d", ia.sendSocks_[j].postQueue->elements()));
				sout << cgicc::td(toolbox::toString("%lu", (unsigned long) ia.sendSocks_[j].counter));
				sout << cgicc::td(toolbox::toString("%lu", (unsigned long) ia.sendSocks_[j].writeCounter));
				sout << cgicc::td(toolbox::toString("%lu", (unsigned long) ia.sendSocks_[j].idleEmptyQueueCounter));
				sout << cgicc::td(toolbox::toString("%lu", (unsigned long) ia.sendSocks_[j].idleBlockingWriteCounter));
				sout << cgicc::td(ia.sendSocks_[j].ep_handle.toString());
				sout << cgicc::tr() << std::endl;

				written += ia.sendSocks_[j].written;
				queue += ia.sendSocks_[j].postQueue->elements();
				sent += ia.sendSocks_[j].counter;
				sockWrites += ia.sendSocks_[j].writeCounter;
				idleEmpty += ia.sendSocks_[j].idleEmptyQueueCounter;
				idleBlock += ia.sendSocks_[j].idleBlockingWriteCounter;
				rows++;
			}
		}

		if (initBody)
		{
			sout << cgicc::tbody() << std::endl;
		}

		sout << cgicc::tfoot();

		// Totals
		sout << cgicc::tr();
		std::stringstream totalString;
		totalString << "Total : " << rows;
		sout << cgicc::td(totalString.str()).set("colspan", "8");
		sout << cgicc::td(toolbox::toString("%d", written));
		sout << cgicc::td(toolbox::toString("%d", queue));
		sout << cgicc::td(toolbox::toString("%d", sent));
		sout << cgicc::td(toolbox::toString("%d", sockWrites));
		sout << cgicc::td(toolbox::toString("%d", (unsigned long) idleEmpty));
		sout << cgicc::td(toolbox::toString("%d", (unsigned long) idleBlock));
		sout << cgicc::td("");
		sout << cgicc::tr();

		// Info
		sout << cgicc::tr();
		sout << cgicc::td().set("colspan", "15");

		sout << "Idle evals: " << (toolbox::toString("%d", (unsigned long) ia.idleCounter_));
		sout << " | Mode: " << ia.getType();
		sout << " | Timeout: " << (toolbox::toString("%d", ia.sndTimeout_)) << "ms";
		sout << " | Non-blocking: " << ia.localAddress_->getProperty("nonblock");
		sout << " | Cycles per eval: " << ia.localAddress_->getProperty("pollingCycle");

		sout << cgicc::td();
		sout << cgicc::tr() << std::endl;
		sout << cgicc::tfoot();
		sout << cgicc::table();

		return sout;

	}
}
