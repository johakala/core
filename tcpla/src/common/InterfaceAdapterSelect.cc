// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2011, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A.Petrucci, C.Wakefield					 *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <sstream>
#include "tcpla/InterfaceAdapterSelect.h"
#include "tcpla/EndPoint.h"

#include "toolbox/hexdump.h"
#include "toolbox/string.h"
#include "xcept/tools.h"

tcpla::InterfaceAdapterSelect::InterfaceAdapterSelect (xdaq::Application * parent, pt::Address::Reference address, tcpla::EventHandler * h, size_t maxClients, size_t ioQueueSize, size_t eventQueueSize, getHeaderSizeFunctionType ghf, getLengthFunctionType glf) throw (tcpla::exception::Exception)
	: InterfaceAdapter(parent, address, h, maxClients, ioQueueSize, eventQueueSize, ghf, glf)
{
	LOG4CPLUS_DEBUG(tcpla::InterfaceAdapter::getOwnerApplication()->getApplicationLogger(), "Using InterfaceAdapterSelect");

	timeout_.tv_sec = (size_t)((double) this->sndTimeout_ / 1000.0);
	timeout_.tv_usec = ((size_t)((double) this->sndTimeout_ / 1000.0) - timeout_.tv_sec) * 1000000;

	//std::cout << "Snd Timeout " << timeout_.tv_sec << " sec " << timeout_.tv_usec << " uSec" << std::endl;

	FD_ZERO(&wset_);
	FD_ZERO(&watchwset_);

	//
	selectfds_ = new int[this->maxClients_]; //array of select fd

	for (int i = 0; i < this->maxClients_; i++)
	{
		selectfds_[i] = tcpla::EMPTY_FD;
	}
	this->maxfd_ = tcpla::EMPTY_FD;
	this->maxPollfdsKey_ = -1;

	//
	std::stringstream name;
	name << "tcpla-ia-workloop/" << this->hostname_ + ":" + localAddress_->getProtocol();

	try
	{
		toolbox::task::getWorkLoopFactory()->getWorkLoop(name.str(), "polling")->activate();
		toolbox::task::getWorkLoopFactory()->getWorkLoop(name.str(), "polling")->submit(process_);
	}
	catch (toolbox::task::exception::Exception& e)
	{
		std::stringstream ss;
		ss << "Failed to submit workloop in " << name;
		//XCEPT_DECLARE_NESTED(tcpla::exception::WorkLoopSubmitFailed, ex, ss.str(), e);
		XCEPT_RETHROW(tcpla::exception::Exception, ss.str(), e);
	}
	catch (std::exception& se)
	{
		std::stringstream ss;
		ss << "Failed to submit notification to worker thread, caught standard exception";
		XCEPT_DECLARE(tcpla::exception::WorkerThreadFailed, ex, se.what());
		XCEPT_RETHROW(tcpla::exception::Exception, ss.str(), ex);
	}
	catch (...)
	{
		XCEPT_RAISE(tcpla::exception::Exception, "Failed to submit notification to worker pool, caught unknown exception");
	}

}

tcpla::InterfaceAdapterSelect::~InterfaceAdapterSelect ()
{
	for (int i = 0; i < this->maxClients_; i++)
	{
		if (selectfds_[i] != tcpla::EMPTY_FD)
		{
			::close(selectfds_[i]);
		}
		toolbox::rlist<TCPLA_SEND_POST_DESC>::destroy(sendSocks_[i].postQueue);
	}

	delete[] selectfds_;
}

void tcpla::InterfaceAdapterSelect::resetSendEntry (tcpla::EndPoint & ep_handle) throw (tcpla::exception::UnitializedEndPoint, tcpla::exception::InvalidEndPoint)
{
	if (ep_handle.isNull())
	{
		std::stringstream ss;
		ss << "Cannot disconnect uninitialized EndPoint in InterfaceAdpater " << this->hostname_;

		XCEPT_RAISE(tcpla::exception::UnitializedEndPoint, ss.str());
	}

	for(int i = 0; i < this->maxClients_; i++)
	{
		if (sendSocks_[i].ep_handle == ep_handle)
		{
			LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), "Reset entry in sender");

			FD_CLR(selectfds_[i], &wset_);
			::close(selectfds_[i]);

			int fd = selectfds_[i];

			try
			{
				this->remove(fd);
			}
			catch (tcpla::exception::InvalidFD & e)
			{
				std::stringstream ss;
				ss << "Failed to disconnect on host '" << this->hostname_ << "' (PANIC internal error)";
				XCEPT_RETHROW(tcpla::exception::InvalidEndPoint, ss.str(), e);
			}

			totalClients_--;

			sendSocks_[i].connected = false;
			sendSocks_[i].written = 0;

			while (!sendSocks_[i].postQueue->empty())
			{
				TCPLA_SEND_POST_DESC d = sendSocks_[i].postQueue->front();

				tcpla::Event event;

				event.event_number = TCPLA_DTO_SND_UNDELIVERABLE_BLOCK;
				event.event_data.tcpla_dto_block_event_data.user_cookie = d.cookie;
				event.event_data.tcpla_dto_block_event_data.block_length = d.iovs;
				event.event_data.tcpla_dto_block_event_data.buffer = 0;
				event.event_data.tcpla_dto_block_event_data.handler = d.handler;
				event.event_data.tcpla_dto_block_event_data.context = d.context;

				this->pushEvent(reqt_evd_hdl_, event);

				sendSocks_[i].postQueue->pop_front();
			}

			sendSocks_[i].pollfdsKey = -1;
			sendSocks_[i].ep_handle.clear();
			sendSocks_[i].counter = 0;
			sendSocks_[i].port = 0;
			sendSocks_[i].writeCounter = 0;
			sendSocks_[i].idleBlockingWriteCounter = 0;
			sendSocks_[i].idleEmptyQueueCounter = 0;

			return;
		}
	}
	std::stringstream ss;
	ss << "Cannot destroy EndPoint " << ep_handle.toString() << ", could not find in registry in InterfaceAdpaterSelect " << this->hostname_;
	XCEPT_RAISE(tcpla::exception::InvalidEndPoint, ss.str());
}

int tcpla::InterfaceAdapterSelect::getFD (size_t pollfdsKey)
{
	return selectfds_[pollfdsKey];
}

bool tcpla::InterfaceAdapterSelect::isReady (size_t pollfdsKey)
{
	return FD_ISSET(selectfds_[pollfdsKey], &watchwset_);
}

bool tcpla::InterfaceAdapterSelect::process (toolbox::task::WorkLoop* wl)
{
	while (true)
	{
		while (!commandQueue_->empty())
		{
			this->eval();
		}

		errno = 0;
// NEW SELECT CODE BEGIN
		// Waiting for incoming connections and data. There is no timeout used right now
		watchwset_ = wset_;
		struct timeval to = timeout_;
		int nready = select(maxfd_ + 1, 0, &watchwset_, 0, &to);

		//std::cout << " nready " << nready << " maxfd_ " << maxfd_ << std::endl;

		if (nready == 0)
		{
			// timeout!
			idleCounter_++;
			continue;
		}
		else if (nready < 0)
		{
			// error
			if (errno == EINTR)
			{
				return true;
			}

			std::stringstream ss;
			ss << "Poll error, work loop name " << wl->getName() << " errno: " << strerror(errno);

			tcpla::Event event;

			event.event_number = TCPLA_ASYNC_ERROR_IA_CATASTROPHIC;
			event.event_data.asynch_error_event_data.ep_handle.clear();
			event.event_data.asynch_error_event_data.reason = 6;
			event.event_data.asynch_error_event_data.exception = TCPLA_XCEPT_ALLOC (tcpla::exception::PollFailed, ss.str());

			this->pushEvent(reqt_evd_hdl_, event);

			return false;
		}

		this->processSocketEntries();

	}		//while forever

	return true;
}

void tcpla::InterfaceAdapterSelect::connect (tcpla::InterfaceAdapter::Command & c) throw (tcpla::exception::MaxConnections, tcpla::exception::FailedToClearSendQueue, tcpla::exception::InvalidPollfdsKey, tcpla::exception::HostnameResolveFailed, tcpla::exception::CannotConnect) //change to use Address
{
	tcpla::EndPoint & ep_handle = c.command.connect_command.ep_handle;
	TCPLA_CONTEXT cookie = c.command.connect_command.cookie;

	int fd = -1;
	try
	{
		fd = this->createSocket(c.command.connect_command.family, c.command.connect_command.nonblock, c.command.connect_command.lsockaddress);
	}
	catch (tcpla::exception::SocketOption & e)
	{
		std::stringstream ss;
		//ss << "Failed to create socket from " << local->toString() << " to " << destination->toString() << " in IA (Select)";
		XCEPT_RETHROW(tcpla::exception::CannotConnect, ss.str(), e);
	}
	catch (tcpla::exception::CannotBind & e)
	{
		std::stringstream ss;
		//ss << "Failed to create socket from " << local->toString() << " to " << destination->toString() << " in IA (Select)";
		XCEPT_RETHROW(tcpla::exception::CannotConnect, ss.str(), e);
	}
	catch (tcpla::exception::HostnameResolveFailed & e)
	{
		std::stringstream ss;
		//ss << "Failed to create socket from " << local->toString() << " to " << destination->toString() << " in IA (Select)";
		XCEPT_RETHROW(tcpla::exception::CannotConnect, ss.str(), e);
	}
	catch (tcpla::exception::Exception & e)
	{
		std::stringstream ss;
		//ss << "Failed to create socket from " << local->toString() << " to " << destination->toString() << " in IA (Select)";
		XCEPT_RETHROW(tcpla::exception::CannotConnect, ss.str(), e);
	}

	if (fd == -1)
	{
		std::stringstream ss;
		//ss << "Failed to create socket from " << local->toString() << " to " << destination->toString() << " in IA (Select)";
		XCEPT_RAISE(tcpla::exception::CannotConnect, ss.str());
	}

	int i = -1;
	try
	{
		i = this->append(fd); // return the index were fd was inserted
	}
	catch (tcpla::exception::MaxConnections & e)
	{
		::close(fd);

		std::stringstream ss;
		ss << "Failed to connect on host '" << this->hostname_ << "'";
		XCEPT_RETHROW(tcpla::exception::MaxConnections, ss.str(), e);
	}

	ep_handle.pollfdsKey_ = i; // index

	try
	{
		sendSocks_[i].postQueue->clear();
	}
	catch (...)
	{
		std::stringstream ss;
		ss << "Internal error - clearing post queue for index " << i << " in InterfaceAdapter " << this->hostname_;

		XCEPT_RAISE(tcpla::exception::FailedToClearSendQueue, ss.str());
	}

	sendSocks_[i].connected = false;
	sendSocks_[i].failed = false;
	sendSocks_[i].written = 0;
	sendSocks_[i].pollfdsKey = i;
	sendSocks_[i].ep_handle = ep_handle;
	//sendSocks_[i].destination = destination;
	//sendSocks_[i].local = local;
	sendSocks_[i].counter = 0;
	sendSocks_[i].writeCounter = 0;
	sendSocks_[i].idleBlockingWriteCounter = 0;
	sendSocks_[i].idleEmptyQueueCounter = 0;
	sendSocks_[i].cookie = cookie; //connection cookie

	try
	{
		this->reset(i);
	}
	catch (tcpla::exception::InvalidPollfdsKey & e)
	{
		std::stringstream ss;
		ss << "Failed to reset entry on connect for index " << i << " in InterfaceAdapter " << this->hostname_;

		XCEPT_RETHROW(tcpla::exception::InvalidPollfdsKey, ss.str(), e);
	}

	int con;
	errno = 0;

	struct sockaddr_in destinationSock = c.command.connect_command.dsockaddress;
	if ((con = ::connect(fd, (sockaddr*) &destinationSock, sizeof(destinationSock))) < 0)
	{
		if (errno != EINPROGRESS && errno != EALREADY)
		{
			std::stringstream ss;
			//ss << "Failed to connect socket for address " << local->toString() << " in InterfaceAdapter " << this->hostname_ << " socket error: " << strerror(errno);

			XCEPT_RAISE(tcpla::exception::CannotConnect, ss.str());
		}
	}

	sendSocks_[i].ip_addr = destinationSock;
	sendSocks_[i].port = destinationSock.sin_port;

	totalClients_++;
	// enable select
	FD_SET(fd, &wset_);
}
std::string tcpla::InterfaceAdapterSelect::getType ()
{
	return "select";
}

int tcpla::InterfaceAdapterSelect::append (int fd) throw (tcpla::exception::MaxConnections)
{
	int i = 0;
	for (i = 0; i < this->maxClients_; i++)
	{
		if (selectfds_[i] == tcpla::EMPTY_FD)
		{
			selectfds_[i] = fd;
			break;
		}
	}
	if (i == this->maxClients_)
	{
		std::stringstream ss;
		ss << "Too many connections: " << i << " (Max Allowed: " << this->maxClients_ << ") in InterfaceAdapter " << this->hostname_;

		XCEPT_RAISE(tcpla::exception::MaxConnections, ss.str());
	}

	if (i > this->maxPollfdsKey_)
	{
		this->maxPollfdsKey_ = i;
	}
	if (fd > this->maxfd_)
	{
		this->maxfd_ = fd;
	}

	return i;
	// POSSIBLY MOVE THE CALL TO FD_SET HERE
}

void tcpla::InterfaceAdapterSelect::remove (int fd) throw (tcpla::exception::InvalidFD)
{
	// clear the entry
	int i;
	for (i = 0; i < this->maxClients_; i++)
	{
		if (selectfds_[i] == fd)
		{
			selectfds_[i] = tcpla::EMPTY_FD;
			break;
		}
	}

	if (i == this->maxClients_)
	{
		std::stringstream ss;
		ss << "Cannot find file descriptor #" << fd << " in file descriptor list" << this->hostname_;

		XCEPT_RAISE(tcpla::exception::InvalidFD, ss.str());
	}

	// refind the maximum fd
	if (fd == this->maxfd_)
	{
		// search for highest
		this->maxfd_ = tcpla::EMPTY_FD;

		for (i = 0; i < this->maxClients_; i++)
		{
			if (selectfds_[i] > this->maxfd_)
			{
				this->maxfd_ = selectfds_[i];
			}
		}
	}

	// find highest used index
	for (i = this->maxPollfdsKey_; i >= 0; i--)
	{
		if (selectfds_[i] != tcpla::EMPTY_FD)
		{
			this->maxPollfdsKey_ = i;
			break;
		}
	}

	// if the array is empty
	if (i == -1)
	{
		this->maxPollfdsKey_ = tcpla::EMPTY_FD;
	}

}
void tcpla::InterfaceAdapterSelect::disableFD(int pollfdsKey)
{
	FD_CLR(selectfds_[pollfdsKey], &wset_);
}

/*
 void tcpla::InterfaceAdapterSelect::updateFDs()
 {
 if ( ep_handle.pollfdsKey_ == (long) this->maxPollfdsKey_ )
 {
 //  lo must count bacward to find the first valid fd ( there could be intermediate fd -1) 16/11/12
 //for ( size_t i = 0; i < this->maxPollfdsKey_; i++ )
 for ( size_t i = this->maxPollfdsKey_ ; i > 0; i-- )
 {
 if ( selectfds_[i] > 0 )
 {
 this->maxPollfdsKey_ = i;
 break;
 }
 }
 }

 maxfd_ = 0;

 for ( int i = 0; i < this->maxClients_; i++ )
 {
 if ( selectfds_[i] != -1 )  // empty
 {
 if ( selectfds_[i] > maxfd_ )
 {
 maxfd_ = selectfds_[i];
 }
 }
 }
 }
 */
/*
 int tcpla::InterfaceAdapterSelect::getFD ( tcpla::EndPoint * ep_handle ) throw ( tcpla::exception::InvalidEndPoint )
 {
 if ( ep_handle.pollfdsKey_ == -1 )
 {
 std::stringstream ss;
 ss << "Cannot disconnect invalid EndPoint in InterfaceAdpater " << this->hostname_;

 XCEPT_RAISE(tcpla::exception::InvalidEndPoint, ss.str());
 }

 //mutex_.take();

 if ( ep_handle.psp_ != 0 ) //endpoint belongs to a PSP
 {
 return ep_handle.psp_->getFD(ep_handle);
 }
 else
 {
 return selectfds_[ep_handle.pollfdsKey_];
 }
 }
 */
