// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2012, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A.Petrucci, C.Wakefield					 *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include "tcpla/Utils.h"
#include "tcpla/Event.h"

#include <sstream>
#include <unistd.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <time.h>
#include <arpa/inet.h>
#include <ctype.h>
#include <dirent.h>
#include <dlfcn.h>
#include <fcntl.h>
#include <grp.h>
#include <errno.h>
#include <libgen.h>
#include <limits.h>
#include <netdb.h>
#include <net/if.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <netinet/ip.h>
#include <pthread.h>
#include <pwd.h>

std::string tcpla::eventToString (int event)
{
	switch (event)
	{
		case 0x00001:
			return tcpla::TCPLA_DTO_RCV_COMPLETION_EVENT_STR;
			break;

		case 0x00002:
			return tcpla::TCPLA_DTO_SND_COMPLETION_EVENT_STR;
			break;

		case 0x00003:
			return tcpla::TCPLA_CONNECTION_REQUEST_EVENT_STR;
			break;

		case 0x00004:
			return tcpla::TCPLA_CONNECTION_EVENT_ESTABLISHED_STR;
			break;

		case 0x00005:
			return tcpla::TCPLA_CONNECTION_EVENT_PEER_REJECTED_STR;
			break;

		case 0x00006:
			return tcpla::TCPLA_CONNECTION_EVENT_NON_PEER_REJECTED_STR;
			break;

		case 0x00007:
			return tcpla::TCPLA_CONNECTION_EVENT_ACCEPT_COMPLETION_ERROR_STR;
			break;

		case 0x00008:
			return tcpla::TCPLA_CONNECTION_EVENT_DISCONNECTED_STR;
			break;

		case 0x00009:
			return tcpla::TCPLA_CONNECTION_EVENT_BROKEN_STR;
			break;

		case 0x0000A:
			return tcpla::TCPLA_CONNECTION_EVENT_TIMED_OUT_STR;
			break;

		case 0x0000B:
			return tcpla::TCPLA_CONNECTION_EVENT_UNREACHABLE_STR;
			break;

		case 0x0000C:
			return tcpla::TCPLA_ASYNC_ERROR_EVD_OVERFLOW_STR;
			break;

		case 0x0000D:
			return tcpla::TCPLA_ASYNC_ERROR_IA_CATASTROPHIC_STR;
			break;

		case 0x0000E:
			return tcpla::TCPLA_ASYNC_ERROR_EP_BROKEN_STR;
			break;

		case 0x0000F:
			return tcpla::TCPLA_ASYNC_ERROR_TIMED_OUT_STR;
			break;

		case 0x00010:
			return tcpla::TCPLA_ASYNC_ERROR_PROVIDER_INTERNAL_ERROR_STR;
			break;

		case 0x00011:
			return tcpla::TCPLA_SOFTWARE_EVENT_STR;
			break;

		case 0x00012:
			return tcpla::TCPLA_DTO_SND_UNDELIVERABLE_BLOCK_STR;
			break;

		case 0x00013:
			return tcpla::TCPLA_DTO_RCV_UNUSED_BLOCK_STR;
			break;

		case 0x00014:
			return tcpla::TCPLA_CONNECTION_CLOSED_BY_PEER_STR;
			break;

		case 0x00015:
			return tcpla::TCPLA_POST_BLOCK_FAILED_STR;
			break;

		case 0x00016:
			return tcpla::TCPLA_CONNECTION_RESET_BY_PEER_STR;
			break;

		case 0x00017:
			return tcpla::TCPLA_CONNECTION_EVENT_ACCEPTED_STR;
			break;

		case 0x00018:
			return tcpla::TCPLA_MEMORY_UNDERFLOW_EVENT_STR;
			break;

		default:
			return tcpla::UNKNOWN_EVENT_STR;
	}
}

std::string tcpla::getHostBySubnet (const std::string & subnet) throw (tcpla::exception::MalformedSubnet, tcpla::exception::CannotOpenInterfaceDevice, tcpla::exception::NoIPInSubnet)
{
	struct sockaddr_in addr, mask; //bcast
	struct ifreq *ifrp, *endp;
	struct ifconf ifc;

	int sockFd;

	struct sockaddr_in socka;
	inet_aton(subnet.c_str(), &socka.sin_addr);
	in_addr_t snet = inet_network(subnet.c_str());
	//std::cout << "snet is " << std::hex << snet << " compared to " << socka.sin_addr.s_addr  << " maps to " << inet_ntoa(socka.sin_addr)  << std::dec << std::endl;
	if (snet == INADDR_NONE)
	{
		std::stringstream ss;
		ss << "Malformed subnet specification " << subnet;
		XCEPT_RAISE(tcpla::exception::MalformedSubnet, ss.str());
	}

	//  extract family mask from subnet ( limited to 255 precision  only)
	std::string fmask;
	char* dq;
	dq = (char*) &(socka.sin_addr.s_addr);
	for (int i = 0; i < 4; i++)
	{
		if (((short) dq[i] & 0x00ff) != 0)
		{
			fmask += "255";
		}
		else
		{
			fmask += "0";
		}
		if (i < 3) fmask += ".";
	}

	in_addr_t mnet = inet_network(fmask.c_str());
	//std::cout << "mnet is " << std::hex << mnet << " maps to " << fmask  << std::dec << std::endl;

	sockFd = socket(AF_INET, SOCK_DGRAM, 0);

	ifc.ifc_len = 16 * sizeof(struct ifreq);
	ifc.ifc_buf = (char*) new struct ifreq[16];

	if (ioctl(sockFd, SIOCGIFCONF, &ifc) < 0)
	{

		::close(sockFd);
		std::stringstream ss;
		ss << "Cannot open interface device for search of subnet" << subnet;
		XCEPT_RAISE(tcpla::exception::CannotOpenInterfaceDevice, ss.str());
	}

	ifrp = (struct ifreq*) ifc.ifc_buf;
	endp = (struct ifreq*) (&ifc.ifc_buf[ifc.ifc_len]);

	for (; ifrp < endp; ifrp++)
	{

		if (ioctl(sockFd, SIOCGIFADDR, ifrp) < 0)
		{
			continue;
		}

		memcpy(&addr, &ifrp->ifr_addr, sizeof(struct sockaddr));
		std::string host = inet_ntoa(addr.sin_addr);

		if (ioctl(sockFd, SIOCGIFNETMASK, ifrp) < 0)
		{
			continue;
		}

		memcpy(&mask, &ifrp->ifr_addr, sizeof(struct sockaddr));
		in_addr_t foundaddr = addr.sin_addr.s_addr;
		//std::cout << "found " << std::hex << foundaddr << " converted to ntoh " <<  ntohl(foundaddr) << " maps to "  << host <<  " and match is " << (snet  &  ntohl(foundaddr))<< std::dec << std::endl;
		if ((mnet & ntohl(foundaddr)) == snet)
		{
			delete ifc.ifc_buf;
			::close(sockFd);
			//std::cout << "match true for " << host <<  std::endl;
			return host;
		}
	}

	delete ifc.ifc_buf;
	::close(sockFd);

	std::stringstream ss;
	ss << "no IP found for subnet " << subnet;
	XCEPT_RAISE(tcpla::exception::NoIPInSubnet, ss.str());
}

std::string tcpla::getHostByInterface (const std::string & interface) throw (tcpla::exception::Exception, tcpla::exception::NoIPForInterface)
{
	struct sockaddr_in addr;

	struct ifreq *ifrp, *endp;
	struct ifconf ifc;

	int sockFd;

	sockFd = socket(AF_INET, SOCK_DGRAM, 0);

	ifc.ifc_len = 16 * sizeof(struct ifreq);
	ifc.ifc_buf = (char*) new struct ifreq[16];

	if (ioctl(sockFd, SIOCGIFCONF, &ifc) < 0)
	{

		::close(sockFd);
		std::stringstream msg;
		msg << "cannot open interface " << interface;
		XCEPT_RAISE(tcpla::exception::Exception, msg.str());
	}

	ifrp = (struct ifreq*) ifc.ifc_buf;
	endp = (struct ifreq*) (&ifc.ifc_buf[ifc.ifc_len]);

	for (; ifrp < endp; ifrp++)
	{
		if (ioctl(sockFd, SIOCGIFADDR, ifrp) < 0)
		{
			continue;
		}

		memcpy(&addr, &ifrp->ifr_addr, sizeof(struct sockaddr));

		std::string host = inet_ntoa(addr.sin_addr);

		if (interface == ifrp->ifr_name)
		{
			delete ifc.ifc_buf;
			close(sockFd);
			return host;
		}
	}

	delete ifc.ifc_buf;

	::close(sockFd);
	std::stringstream ss;

	ss << "No IP found for interface " << interface;
	XCEPT_RAISE(tcpla::exception::NoIPForInterface, ss.str());
}
