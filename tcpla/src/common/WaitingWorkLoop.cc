// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2012, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A.Petrucci, C.Wakefield					 *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include "tcpla/WaitingWorkLoop.h"
#include "toolbox/task/WorkLoopFactory.h"
#include "tcpla/EventHandler.h"
#include <sstream>

tcpla::WaitingWorkLoop::WaitingWorkLoop (const std::string & name, tcpla::EventDispatcher* evd_handle, tcpla::EventHandler * event_handler, const std::string & affinity) throw (tcpla::exception::Exception)
	: tcpla::WorkLoop(name, event_handler, affinity), evd_handle_(dynamic_cast<tcpla::WaitingEventDispatcher*>(evd_handle))
{
	process_ = toolbox::task::bind(this, &WaitingWorkLoop::process, "process");

	try
	{
		toolbox::task::getWorkLoopFactory()->getWorkLoop(name_, "waiting")->activate();
		toolbox::task::getWorkLoopFactory()->getWorkLoop(name_, "waiting")->submit(process_);
	}
	catch (toolbox::task::exception::Exception& e)
	{
		std::stringstream ss;
		ss << "Failed to submit workloop " << name_;
		//XCEPT_DECLARE_NESTED(tcpla::exception::WorkLoopSubmitFailed, ex, ss.str(), e);
		XCEPT_RETHROW(tcpla::exception::Exception, ss.str(), e);
	}
	catch (std::exception& se)
	{
		std::stringstream ss;
		ss << "Failed to submit notification to worker thread, caught standard exception";
		XCEPT_DECLARE(tcpla::exception::WorkerThreadFailed, ex, se.what());
		XCEPT_RETHROW(tcpla::exception::Exception, ss.str(), ex);
	}
	catch (...)
	{
		XCEPT_RAISE(tcpla::exception::Exception, "Failed to submit notification to worker pool, caught unknown exception");
	}
}

tcpla::WaitingWorkLoop::~WaitingWorkLoop ()
{
	toolbox::task::getWorkLoopFactory()->getWorkLoop(name_, "waiting")->cancel();
}

bool tcpla::WaitingWorkLoop::process (toolbox::task::WorkLoop* wl)
{
	while (true)
	{
		tcpla::Event event = evd_handle_->eventQueue_.pop();
		event_handler_->handleEvent(event);
	}

	return true;
}

