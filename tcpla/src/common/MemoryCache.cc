// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 		 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, C. Wakefield			 				 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         	 *
 *************************************************************************/

#include "tcpla/MemoryCache.h"

tcpla::MemoryCache::MemoryCache (const std::string & name, size_t frames, toolbox::mem::Pool * pool, size_t framesize) throw (tcpla::exception::Exception)
	: mutex_(toolbox::BSem::FULL), name_(name)
{
	cache_ = toolbox::rlist<toolbox::mem::Reference*>::create(name, frames);

	//for ( size_t k = 0; k < frames; k++ )
	for (size_t k = 0; k < (frames); k++)
	{
		//std::cout << "going to push buffer on receiver endpoint" << std::endl;
		toolbox::mem::Reference * reference;

		try
		{
			reference = toolbox::mem::getMemoryPoolFactory()->getFrame(pool, framesize);
		}
		catch (toolbox::mem::exception::Exception & e)
		{
			std::stringstream msg;
			msg << "Failed to allocate frames for memory cache";
			XCEPT_RETHROW(tcpla::exception::Exception, msg.str(), e);

		}

		this->grantFrame(reference);
	}
}

tcpla::MemoryCache::~MemoryCache ()
{
	mutex_.take();
	while(!cache_->empty())
	{
		toolbox::mem::Reference* frame = cache_->front();
		cache_->pop_front();
		frame->release();
	}
	mutex_.give();
}

toolbox::mem::Reference* tcpla::MemoryCache::waitFrame ()
{
	while (cache_->empty())
	{

	}

	toolbox::mem::Reference* frame = cache_->front();
	cache_->pop_front();

	return frame;
}

toolbox::mem::Reference* tcpla::MemoryCache::getFrame ()
{
	mutex_.take();
	if (!this->empty())
	{
		toolbox::mem::Reference* frame = cache_->front();
		cache_->pop_front();
		mutex_.give();
		return frame;
	}

	std::stringstream msg;
	msg << "Failed to get frame from cache";
	XCEPT_RAISE(tcpla::exception::Exception, msg.str());

}

void tcpla::MemoryCache::grantFrame (toolbox::mem::Reference* frame) throw (tcpla::exception::Exception)
{
	try
	{
		mutex_.take();
		cache_->push_back(frame);
		mutex_.give();
	}
	catch (toolbox::exception::RingListFull & e)
	{
		mutex_.give();
		std::stringstream msg;
		msg << "Overflow of cache";
		XCEPT_RETHROW(tcpla::exception::Exception, msg.str(), e);

	}
}

bool tcpla::MemoryCache::empty ()
{
	return cache_->empty();
}

