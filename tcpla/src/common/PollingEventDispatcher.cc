// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2011, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A.Petrucci, C.Wakefield					 *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include "tcpla/PollingEventDispatcher.h"

tcpla::PollingEventDispatcher::PollingEventDispatcher (size_t queueSize, const std::string & name)
	: mutex_(toolbox::BSem::FULL)
{
	eventQueue_ = toolbox::rlist < Event > ::create(name + "-event-queue", queueSize);
}

void tcpla::PollingEventDispatcher::push (tcpla::Event & e) throw (tcpla::exception::EventQueueFull)
{
	try
	{
		mutex_.take();
		eventQueue_->push_back(e);
		mutex_.give();
	}
	catch (toolbox::exception::RingListFull & e)
	{
		mutex_.give();
		XCEPT_RETHROW(tcpla::exception::EventQueueFull, "Failed to push in polling event dispatcher", e);
	}

}

tcpla::PollingEventDispatcher::~PollingEventDispatcher ()
{
	toolbox::rlist<Event>::destroy(eventQueue_);
}
