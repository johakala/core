// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2011, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A.Petrucci, C.Wakefield					 *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <sstream>

#include "tcpla/InterfaceAdapterPoll.h"
#include "tcpla/EndPoint.h"

#include "toolbox/hexdump.h"
#include "toolbox/string.h"
#include "xcept/tools.h"

tcpla::InterfaceAdapterPoll::InterfaceAdapterPoll (xdaq::Application * parent, pt::Address::Reference address, tcpla::EventHandler * h, size_t maxClients, size_t ioQueueSize, size_t eventQueueSize, getHeaderSizeFunctionType ghf, getLengthFunctionType glf) throw (tcpla::exception::Exception)
	: InterfaceAdapter(parent, address, h, maxClients, ioQueueSize, eventQueueSize, ghf, glf)
{
	LOG4CPLUS_DEBUG(tcpla::InterfaceAdapter::getOwnerApplication()->getApplicationLogger(), "Using InterfaceAdapterPoll");

	this->maxPollfdsKey_ = -1;

	pollfds_ = new pollfd[this->maxClients_]; //array of pollfd structs

	for (int i = 0; i < this->maxClients_; i++)
	{
		pollfds_[i].fd = tcpla::EMPTY_FD;
	}

	std::stringstream name;
	name << "tcpla-ia-workloop/" << this->hostname_ + ":" + localAddress_->getProtocol();

	try
	{
		toolbox::task::getWorkLoopFactory()->getWorkLoop(name.str(), "polling")->activate();
		toolbox::task::getWorkLoopFactory()->getWorkLoop(name.str(), "polling")->submit(process_);
	}
	catch (toolbox::task::exception::Exception& e)
	{
		std::stringstream ss;
		ss << "Failed to submit workloop in " << name;
		//XCEPT_DECLARE_NESTED(tcpla::exception::WorkLoopSubmitFailed, ex, ss.str(), e);
		XCEPT_RETHROW(tcpla::exception::Exception, ss.str(), e);
	}
	catch (std::exception& se)
	{
		std::stringstream ss;
		ss << "Failed to submit notification to worker thread, caught standard exception";
		XCEPT_DECLARE(tcpla::exception::WorkerThreadFailed, ex, se.what());
		XCEPT_RETHROW(tcpla::exception::Exception, ss.str(), ex);
	}
	catch (...)
	{
		XCEPT_RAISE(tcpla::exception::Exception, "Failed to submit notification to worker pool, caught unknown exception");
	}
}

tcpla::InterfaceAdapterPoll::~InterfaceAdapterPoll ()
{
	for (int i = 0; i < this->maxClients_; i++)
	{
		if (pollfds_[i].fd != tcpla::EMPTY_FD)
		{
			::close(pollfds_[i].fd);
		}
		toolbox::rlist<TCPLA_SEND_POST_DESC>::destroy(sendSocks_[i].postQueue);
	}

	delete[] pollfds_;
}
/*

 if ( ep_handle.psp_ != 0 ) //endpoint belongs to a PSP
 {
 ep_handle.psp_->resetRcvEntry(ep_handle.pollfdsKey_);
 }
 else
 */

void tcpla::InterfaceAdapterPoll::resetSendEntry (tcpla::EndPoint & ep_handle) throw (tcpla::exception::UnitializedEndPoint, tcpla::exception::InvalidEndPoint)
{
	if (ep_handle.isNull())
	{
		std::stringstream ss;
		ss << "Cannot disconnect uninitialized EndPoint in InterfaceAdpater " << this->hostname_;

		XCEPT_RAISE(tcpla::exception::UnitializedEndPoint, ss.str());
	}

	for(int i = 0; i < this->maxClients_; i++)
	{
		if (sendSocks_[i].ep_handle == ep_handle)
		{
			LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), "reset entry on sendSocks");

			::close(pollfds_[i].fd);

			sendSocks_[i].connected = false;
			sendSocks_[i].written = 0;

			while (!sendSocks_[i].postQueue->empty())
			{
				TCPLA_SEND_POST_DESC d = sendSocks_[i].postQueue->front();

				tcpla::Event event;

				event.event_number = TCPLA_DTO_SND_UNDELIVERABLE_BLOCK;
				event.event_data.tcpla_dto_block_event_data.user_cookie = d.cookie;
				event.event_data.tcpla_dto_block_event_data.block_length = d.iovs;
				event.event_data.tcpla_dto_block_event_data.buffer = 0;
				event.event_data.tcpla_dto_block_event_data.handler = d.handler;
				event.event_data.tcpla_dto_block_event_data.context = d.context;

				this->pushEvent(reqt_evd_hdl_, event);

				sendSocks_[i].postQueue->pop_front();
			}

			sendSocks_[i].pollfdsKey = -1;
			sendSocks_[i].ep_handle.clear();
			sendSocks_[i].counter = 0;
			sendSocks_[i].port = 0;
			sendSocks_[i].writeCounter = 0;
			sendSocks_[i].idleBlockingWriteCounter = 0;
			sendSocks_[i].idleEmptyQueueCounter = 0;

			pollfds_[i].fd = -1;
			pollfds_[i].events = 0;

			totalClients_--;

			int j = -1;
			for (j = this->maxPollfdsKey_; j >= 0; j--)
			{
				if (pollfds_[j].fd != tcpla::EMPTY_FD)
				{
					this->maxPollfdsKey_ = j;
					break;
				}
			}

			// only listen socket remaining in array
			if (j == -1)
			{
				this->maxPollfdsKey_ = -1;
			}
			return;
		}
	}
	std::stringstream ss;
	ss << "Cannot destroy EndPoint " << ep_handle.toString() << ", could not find in registry in InterfaceAdpaterPoll " << this->hostname_;
	XCEPT_RAISE(tcpla::exception::InvalidEndPoint, ss.str());
}

int tcpla::InterfaceAdapterPoll::getFD (size_t pollfdsKey)
{
	return pollfds_[pollfdsKey].fd;
}

bool tcpla::InterfaceAdapterPoll::isReady (size_t pollfdsKey)
{
	if (pollfds_[pollfdsKey].revents & (POLLOUT))
	{
		return true;
	}
	return false;
}

bool tcpla::InterfaceAdapterPoll::process (toolbox::task::WorkLoop* wl)
{
	while (true)
	{
		while (!commandQueue_->empty())
		{
			this->eval();
		}

		errno = 0;

		int numberReady = poll(pollfds_, this->maxPollfdsKey_ + 1, sndTimeout_);

		if (numberReady < 0)
		{
			if (errno == EINTR)
			{
				return true;
			}

			std::stringstream ss;
			ss << "Poll error, work loop name " << wl->getName() << " errno: " << strerror(errno);

			tcpla::Event event;

			event.event_number = TCPLA_ASYNC_ERROR_IA_CATASTROPHIC;
			event.event_data.asynch_error_event_data.ep_handle.clear();
			event.event_data.asynch_error_event_data.reason = 6;
			event.event_data.asynch_error_event_data.exception = TCPLA_XCEPT_ALLOC (tcpla::exception::PollFailed, ss.str());

			this->pushEvent(reqt_evd_hdl_, event);

			//mutex_.give();

			return false;
		}

		if (numberReady == 0)
		{
			idleCounter_++;
			//mutex_.give();
			//sched_yield();

			continue;
		}

		for (int i = 0; i <= this->maxPollfdsKey_; i++)
		{

			int pollfdsKey = sendSocks_[i].pollfdsKey;

			// check that this is a valid allocated entry LO 16/11/12
			if (pollfdsKey == -1)
			{
				// skip this entry
				continue;
			}

			if ((pollfds_[pollfdsKey].revents & (POLLHUP)) && sendSocks_[i].connected)
			{
				LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), "POLLHUP event in IA");

				tcpla::Event event; // client event

				event.event_number = TCPLA_CONNECTION_EVENT_DISCONNECTED;
				event.event_data.connection_broken_event_data.ep_handle = sendSocks_[i].ep_handle;
				event.event_data.connection_broken_event_data.reason = 4;

				std::stringstream ss;
				ss << "The device has been disconnected for socket index " << pollfdsKey << " in InterfaceAdapter " << this->hostname_;
				event.event_data.connection_broken_event_data.exception = TCPLA_XCEPT_ALLOC (tcpla::exception::SocketPollError, ss.str());

				sendSocks_[i].connected = false;

				this->pushEvent(creq_evd_hdl_, event);

				continue;
			}
			//if the socket can send something
			if ((pollfds_[pollfdsKey].revents & (POLLNVAL)))
			{
				LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), "POLLNVAL event in IA");
				continue;
				//ignore this status
			}

			if ((pollfds_[pollfdsKey].revents & (POLLERR)) && !sendSocks_[i].failed)
			{
				sendSocks_[i].failed = true;

				LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), "POLLERR event in IA");

				tcpla::Event event; // client event

				event.event_number = TCPLA_CONNECTION_EVENT_UNREACHABLE;
				event.event_data.connect_error_event_data.ep_handle = sendSocks_[i].ep_handle;
				event.event_data.connect_error_event_data.reason = 5;

				//no errno is message since it is success

				std::stringstream ss;
				ss << "Unreachable connection for socket index " << pollfdsKey << " in InterfaceAdapter " << this->hostname_;

				event.event_data.connect_error_event_data.exception = TCPLA_XCEPT_ALLOC (tcpla::exception::SocketPollError, ss.str());

				this->pushEvent(creq_evd_hdl_, event);

				continue;
			}

			/* performed already in processSocketEntries();

			 if ( pollfds_[pollfdsKey].revents & (POLLOUT) )
			 {
			 //numberReady--;

			 if ( !sendSocks_[i].connected ) // the socket is finally connected
			 {
			 LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), "Connection established in IA");
			 tcpla::Event event; // client event

			 event.event_number = TCPLA_CONNECTION_EVENT_ESTABLISHED;
			 event.event_data.connect_event_data.ep_handle = sendSocks_[i].ep_handle;
			 event.event_data.connect_event_data.user_cookie = sendSocks_[i].cookie;
			 sendSocks_[i].connected = true;
			 this->conn_evd_hdl_->push(event);
			 }


			 }
			 */
		}

		this->processSocketEntries();

	} //end forever

	return true;
}

void tcpla::InterfaceAdapterPoll::connect (tcpla::InterfaceAdapter::Command & c) throw (tcpla::exception::MaxConnections, tcpla::exception::FailedToClearSendQueue, tcpla::exception::InvalidPollfdsKey, tcpla::exception::HostnameResolveFailed, tcpla::exception::CannotConnect)
{
	tcpla::EndPoint & ep_handle = c.command.connect_command.ep_handle;
	TCPLA_CONTEXT cookie = c.command.connect_command.cookie;

	int fd = -1;
	try
	{
		fd = this->createSocket(c.command.connect_command.family, c.command.connect_command.nonblock, c.command.connect_command.lsockaddress);
	}
	catch (tcpla::exception::SocketOption & e)
	{
		std::stringstream ss;
		//ss << "Failed to create socket from " << local->toString() << " to " << destination->toString() << " in IA (Poll)";
		XCEPT_RETHROW(tcpla::exception::CannotConnect, ss.str(), e);
	}
	catch (tcpla::exception::CannotBind & e)
	{
		std::stringstream ss;
		//ss << "Failed to create socket from " << local->toString() << " to " << destination->toString() << " in IA (Poll)";
		XCEPT_RETHROW(tcpla::exception::CannotConnect, ss.str(), e);
	}
	catch (tcpla::exception::HostnameResolveFailed & e)
	{
		std::stringstream ss;
		//ss << "Failed to create socket from " << local->toString() << " to " << destination->toString() << " in IA (Poll)";
		XCEPT_RETHROW(tcpla::exception::CannotConnect, ss.str(), e);
	}
	catch (tcpla::exception::Exception & e)
	{
		std::stringstream ss;
		//ss << "Failed to create socket from " << local->toString() << " to " << destination->toString() << " in IA (Poll)";
		XCEPT_RETHROW(tcpla::exception::CannotConnect, ss.str(), e);
	}

	if (fd == -1)
	{
		std::stringstream ss;
		//ss << "Failed to create socket from " << local->toString() << " to " << destination->toString() << " in IA (Poll)";
		XCEPT_RAISE(tcpla::exception::CannotConnect, ss.str());
	}

	int i = 0;
	for (i = 0; i < this->maxClients_; i++)
	{
		if (pollfds_[i].fd == tcpla::EMPTY_FD)
		{
			pollfds_[i].fd = fd;
			pollfds_[i].events = POLLOUT | POLLHUP | POLLERR; //events to handle: output
			break;
		}
	}
	if (i == this->maxClients_)
	{
		::close(fd);

		std::stringstream ss;
		//ss << "Too many connections: " << i << " (Max Allowed: " << this->maxClients_ << ") in InterfaceAdapter " << this->hostname_;

		XCEPT_RAISE(tcpla::exception::MaxConnections, ss.str());
	}

	ep_handle.pollfdsKey_ = i;

	try
	{
		sendSocks_[i].postQueue->clear();
	}
	catch (...)
	{
		std::stringstream ss;
		ss << "Internal error - clearing post queue for index " << i << " in InterfaceAdapter " << this->hostname_;

		XCEPT_RAISE(tcpla::exception::FailedToClearSendQueue, ss.str());
	}

	sendSocks_[i].connected = false;
	sendSocks_[i].failed = false;
	sendSocks_[i].written = 0;
	sendSocks_[i].pollfdsKey = i;
	sendSocks_[i].ep_handle = ep_handle;
	//sendSocks_[i].destination = destination;
	//sendSocks_[i].local = local;
	sendSocks_[i].counter = 0;
	sendSocks_[i].writeCounter = 0;
	sendSocks_[i].idleBlockingWriteCounter = 0;
	sendSocks_[i].idleEmptyQueueCounter = 0;
	sendSocks_[i].cookie = cookie; //connection cookie

	try
	{
		this->reset(i);
	}
	catch (tcpla::exception::InvalidPollfdsKey & e)
	{
		std::stringstream ss;
		ss << "Failed to reset entry on connect for index " << i << " in InterfaceAdapter " << this->hostname_;

		XCEPT_RETHROW(tcpla::exception::InvalidPollfdsKey, ss.str(), e);
	}

	struct sockaddr_in destinationSock = c.command.connect_command.dsockaddress;

	int con;
	errno = 0;
	if ((con = ::connect(fd, (sockaddr*) &destinationSock, sizeof(destinationSock))) > 0)
	{
		std::stringstream ss;
		//ss << "Failed to connect socket for address " << local->toString() << " in InterfaceAdapter " << this->hostname_ << " socket error: " << strerror(errno);

		XCEPT_RAISE(tcpla::exception::CannotConnect, ss.str());
	}

	sendSocks_[ep_handle.pollfdsKey_].ip_addr = destinationSock;
	sendSocks_[ep_handle.pollfdsKey_].port = destinationSock.sin_port;

	//new this->maxPollfdsKey_

	if (i > this->maxPollfdsKey_)
	{
		this->maxPollfdsKey_ = i;
	}
	//new this->maxPollfdsKey_
	totalClients_++;
}


void tcpla::InterfaceAdapterPoll::disableFD(int pollfdsKey)
{
#warning "untested disableFD"
	pollfds_[pollfdsKey].events = 0;
}

std::string tcpla::InterfaceAdapterPoll::getType ()
{
	return "poll";
}

/*
 int tcpla::InterfaceAdapterPoll::getFD ( tcpla::EndPoint * ep_handle ) throw ( tcpla::exception::InvalidEndPoint )
 {
 if ( ep_handle.pollfdsKey_ == -1 )
 {
 std::stringstream ss;
 ss << "Cannot disconnect invalid EndPoint in InterfaceAdpater " << this->hostname_;

 XCEPT_RAISE(tcpla::exception::InvalidEndPoint, ss.str());
 }

 //mutex_.take();

 if ( ep_handle.psp_ != 0 ) //endpoint belongs to a PSP
 {
 return ep_handle.psp_->getFD(ep_handle);
 }
 else
 {
 return pollfds_[ep_handle.pollfdsKey_].fd;
 }
 }
 */
