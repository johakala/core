// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2012, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: C.Wakefield, L.Orsini, A.Petrucci 					 *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include <stdio.h>
#include <string.h>
#include <sstream>
#include "tcpla/EndPoint.h"
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <netinet/tcp.h>

#include "tcpla/InterfaceAdapter.h"
#include "tcpla/PublicServicePoint.h"

/*
 tcpla::EndPoint::EndPoint ( tcpla::InterfaceAdapter * ia ) throw ( tcpla::exception::Exception ):  ia_(ia)
 {
 pollfdsKey_ = -1;
 psp_ = 0;
 }

 tcpla::EndPoint::~EndPoint ()
 {

 }
 */

bool tcpla::EndPoint::canPost (size_t total) throw (tcpla::exception::InvalidEndPoint)
{
	if (this->ia_)
	{
		if (this->pollfdsKey_ != -1)
		{
			return this->ia_->canPost(*this, total);
		}
		else
		{
			std::stringstream msg;
			msg << "Invalid post index";
			XCEPT_RAISE(tcpla::exception::InvalidEndPoint, msg.str());
		}
	}

	return false;
}

int tcpla::EndPoint::getFD () throw (tcpla::exception::InvalidEndPoint)
{
	try
	{
		if (this->psp_ != 0)
		{
			return this->psp_->getFD(*this);
		}
		else if (pollfdsKey_ != -1)
		{
			return ia_->getFD(pollfdsKey_);
		}
	}
	catch (tcpla::exception::InvalidEndPoint & e)
	{
		std::stringstream msg;
		msg << "Invalid Endpoint";
		XCEPT_RETHROW(tcpla::exception::InvalidEndPoint, msg.str(), e);
	}

	std::stringstream msg;
	msg << "Invalid Endpoint";
	XCEPT_RAISE(tcpla::exception::InvalidEndPoint, msg.str());
}

void tcpla::EndPoint::clear ()
{
	uuid_clear(uuid_);
	pollfdsKey_ = -1;
	psp_ = 0;
	ia_ = 0;
}

int tcpla::EndPoint::compare (const tcpla::EndPoint &obj) const
{
	return uuid_compare(uuid_, obj.uuid_);
}
int tcpla::EndPoint::operator== (const tcpla::EndPoint &obj) const
{
	return (compare(obj) == 0);
}
int tcpla::EndPoint::operator!= (const tcpla::EndPoint &obj) const
{
	return (compare(obj) != 0);
}

bool tcpla::EndPoint::isNull ()
{
	return (uuid_is_null(uuid_) > 0);
}

std::string tcpla::EndPoint::toString (void)
{
	char uuidstr[37]; // 36 + \0
	uuid_unparse(uuid_, uuidstr);
	return uuidstr;
}

namespace tcpla
{
	std::ostream& operator << (std::ostream& s, const tcpla::EndPoint& ep)
	{
		char uuidstr[37]; // 36 + \0
		uuid_unparse(ep.uuid_, uuidstr);
		return s << uuidstr;
	}
}
