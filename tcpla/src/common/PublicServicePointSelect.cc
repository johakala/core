// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2012, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A.Petrucci, C.Wakefield				 *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include <sstream>
#include "tcpla/PublicServicePointSelect.h"
#include "tcpla/InterfaceAdapter.h"
#include "tcpla/EventHandler.h"
#include "tcpla/Utils.h"

#include "toolbox/hexdump.h"

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

tcpla::PublicServicePointSelect::PublicServicePointSelect (tcpla::InterfaceAdapter * ia, pt::Address::Reference address, size_t ioQueueSize, size_t headerSize) throw (tcpla::exception::Exception, tcpla::exception::InvalidRcvTimeOut, tcpla::exception::NoFreePortInRange, tcpla::exception::CannotBind)
	: PublicServicePoint(ia, address, ioQueueSize, headerSize)
{
	// moved to IA
	// LOG4CPLUS_DEBUG(tcpla::InterfaceAdapter::getOwnerApplication()->getApplicationLogger(), __FUNCTION__); // no logger
	//std::cout << __FUNCTION__ << std::endl;

	// the first entry is the listen sockets
	this->maxPollfdsKey_ = 0;

	timeout_.tv_sec = (size_t)((double) this->rcvTimeout_ / 1000.0);
	timeout_.tv_usec = ((double) ((double) this->rcvTimeout_ / 1000.0) - timeout_.tv_sec) * 1000000;

	//std::cout << "Recv Timeout " << timeout_.tv_sec << " sec reughoiregre " << timeout_.tv_usec << " uSec timeout " << rcvTimeout_ << std::endl;

	maxfd_ = tcpla::EMPTY_FD;
	FD_ZERO(&rset_);

	selectfds_ = new int[this->maxClients_]; //array of select fd

	for (int i = 0; i < this->maxClients_; i++)
	{
		selectfds_[i] = tcpla::EMPTY_FD;
	}

	maxfd_ = listenfd_;
	FD_SET(listenfd_, &rset_);

	std::stringstream name;
	name << "tcpla-psp/" << listenAddress_->getProperty("hostname") << ":" << port_;

	try
	{
		toolbox::task::getWorkLoopFactory()->getWorkLoop(name.str(), "polling")->activate();
		toolbox::task::getWorkLoopFactory()->getWorkLoop(name.str(), "polling")->submit(process_);
	}
	catch (toolbox::task::exception::Exception& e)
	{
		std::stringstream ss;
		ss << "Failed to submit workloop " << name;
		//XCEPT_DECLARE_NESTED(tcpla::exception::WorkLoopSubmitFailed, ex, ss.str(), e);
		XCEPT_RETHROW(tcpla::exception::Exception, ss.str(), e);
	}
	catch (std::exception& se)
	{
		std::stringstream ss;
		ss << "Failed to submit notification to worker thread, caught standard exception";
		XCEPT_DECLARE(tcpla::exception::WorkerThreadFailed, ex, se.what());
		XCEPT_RETHROW(tcpla::exception::Exception, ss.str(), ex);
	}
	catch (...)
	{
		std::stringstream ss;
		ss << "Failed to submit notification to worker pool, caught unknown exception in PublicServicePoint " << this->toString();

		XCEPT_RAISE(tcpla::exception::Exception, ss.str());
	}
}

tcpla::PublicServicePointSelect::~PublicServicePointSelect ()
{
	for (int i = 0; i < this->maxClients_; i++)
	{
		if (selectfds_[i] != tcpla::EMPTY_FD)
		{
			::close(selectfds_[i]);
		}
	}

	delete[] selectfds_;
}

std::string tcpla::PublicServicePointSelect::getType ()
{
	return "select";
}

void tcpla::PublicServicePointSelect::accept (tcpla::InterfaceAdapter::Command & c) // throw (tcpla::exception::Exception)
{

	tcpla::EndPoint & ep_handle = c.command.accept_command.ep_handle;
	int cr_handle = c.command.accept_command.cr_handle;

	rcvSocks_[cr_handle].connected = true;
	ep_handle.pollfdsKey_ = cr_handle;
	ep_handle.psp_ = this;
	rcvSocks_[cr_handle].ep_handle = ep_handle;
	FD_SET(selectfds_[cr_handle], &rset_); // enable select
	if (selectfds_[cr_handle] > maxfd_)
	{
		maxfd_ = selectfds_[cr_handle];
	}

	tcpla::Event event;

	event.event_number = TCPLA_CONNECTION_EVENT_ACCEPTED;
	event.event_data.connect_event_data.ep_handle = ep_handle;
	event.event_data.connect_event_data.user_cookie.as_ptr = c.command.accept_command.cookie.as_ptr;

	this->ia_->pushEvent(ia_->conn_evd_hdl_, event);
}

void tcpla::PublicServicePointSelect::accept () throw (tcpla::exception::Exception)
{
	struct sockaddr_in cliaddr;
	int connfd = -1;
	try
	{
		connfd = this->acceptSocket(listenfd_, &cliaddr);
	}
	catch (tcpla::exception::FailedToAccept & e)
	{
		XCEPT_RETHROW(tcpla::exception::Exception, "Failed to accept", e);
	}
	catch (tcpla::exception::FailedToGetFileControlSettings & e)
	{
		XCEPT_RETHROW(tcpla::exception::Exception, "Failed to accept", e);
	}
	catch (tcpla::exception::FailedToSetNonBlocking & e)
	{
		XCEPT_RETHROW(tcpla::exception::Exception, "Failed to accept", e);
	}

	if (connfd == -1)
	{
		// ignore
		return;
	}

	int i;

	//find a file descriptor not in use
	//start from 1 because listenfd
	for (i = 1; i < this->maxClients_; i++)
	{
		if (selectfds_[i] == tcpla::EMPTY_FD)
		{
			selectfds_[i] = connfd;
			break;
		}
	}

	if (i == this->maxClients_)
	{
		std::stringstream ss;
		ss << "Too many connections: " << i << " (Max Allowed: " << this->maxClients_ << ") in PublicServicePoint " << this->toString();

		XCEPT_RAISE(tcpla::exception::Exception, ss.str());
	}

	rcvSocks_[i].hostname = inet_ntoa(cliaddr.sin_addr);
	rcvSocks_[i].port = ntohs(cliaddr.sin_port);
	rcvSocks_[i].pollfdsKey = i;
	rcvSocks_[i].ep_handle.clear();
	rcvSocks_[i].rcvnBytes = 0;
	rcvSocks_[i].connected = false;

	rcvSocks_[i].counter = 0;
	rcvSocks_[i].readCounter = 0;
	rcvSocks_[i].idleBlockingReadCounter = 0;
	rcvSocks_[i].idleEmptyQueueCounter = 0;
	rcvSocks_[i].length = 0;
	rcvSocks_[i].current.size = 0;

	try
	{
		rcvSocks_[i].freeQueue->clear();
	}
	catch (...)
	{
		std::stringstream ss;
		ss << "Internal error - clearing free receive buffers queue for index " << i << " in PublicServicePoint " << this->toString();

		XCEPT_RAISE(tcpla::exception::Exception, ss.str());
	}

	if (i > this->maxPollfdsKey_)
	{
		this->maxPollfdsKey_ = i;
	}

	totalClients_++;

	tcpla::Event event;

	event.event_number = TCPLA_CONNECTION_REQUEST_EVENT;
	event.event_data.cr_event_data.psp = this;
	event.event_data.cr_event_data.cr_handle = i;

	this->ia_->pushEvent(ia_->creq_evd_hdl_, event);
}

int tcpla::PublicServicePointSelect::getFD (size_t pollfdsKey)
{
	return selectfds_[pollfdsKey];
}

bool tcpla::PublicServicePointSelect::isReady (size_t pollfdsKey)
{
	return FD_ISSET(selectfds_[pollfdsKey], &watchrset_);
}

void tcpla::PublicServicePointSelect::disableFD (size_t pollfdsKey)
{
	FD_CLR(selectfds_[pollfdsKey], &rset_);
}

bool tcpla::PublicServicePointSelect::process (toolbox::task::WorkLoop* wl)
{
	while (true)
	{
		if (!commandQueue_->empty())
		{
			this->eval();
		}

		errno = 0;
		// Waiting for incoming connections and data. There is no timeout used right now
		watchrset_ = rset_;
		struct timeval to = timeout_;
		//std::cout << "tcpla::PublicServicePointSelect::process: going to select" << std::endl;
		int nready = select(maxfd_ + 1, &watchrset_, 0, 0, &to);
		if (nready < 0)
		{
			if (errno == EINTR)
			{
				std::cout << "lost time" << std::endl;
				return true;
			}

			std::stringstream ss;

			ss << "Select error in work loop " << wl->getName() << " in PublicServicePoint " << this->toString() << " errno: " << strerror(errno);

			tcpla::Event event;

			event.event_number = TCPLA_ASYNC_ERROR_IA_CATASTROPHIC;
			event.event_data.asynch_error_event_data.ep_handle.clear();
			event.event_data.asynch_error_event_data.reason = 6;
			event.event_data.asynch_error_event_data.exception = TCPLA_XCEPT_ALLOC (tcpla::exception::PollFailed, ss.str());

			this->ia_->pushEvent(ia_->reqt_evd_hdl_, event);

			return false;
		}

		if (nready == 0)
		{
			idleCounter_++;
			continue;
		}

		if (FD_ISSET(listenfd_, &watchrset_)) //a new client has request connection
		{
			//std::cout << "tcpla::PublicServicePointSelect::process: a new client has request connection" << std::endl;
			try
			{
				this->accept(); // this can be called several times till acceptance is actually perfromed
			}
			catch (tcpla::exception::Exception & e)
			{
				tcpla::Event event;

				event.event_number = TCPLA_ASYNC_ERROR_PROVIDER_INTERNAL_ERROR;
				event.event_data.asynch_error_event_data.ep_handle.clear();
				event.event_data.asynch_error_event_data.reason = 2;

				std::stringstream ss;
				ss << "Internal accept failed in poll in PublicServicePoint " << this->toString();

				event.event_data.asynch_error_event_data.exception = TCPLA_XCEPT_ALLOC_NESTED(tcpla::exception::FailedToAccept, ss.str(), e);

				this->ia_->pushEvent(ia_->reqt_evd_hdl_, event);
			}

			/*
			 if ( --nready <= 0 )
			 {
			 //debug idleCounter_++;
			 continue;
			 }
			 */

			continue;

		}

		this->processSocketEntries();

	}	// forever loop

	return true;
}

void tcpla::PublicServicePointSelect::resetRcvEntry (tcpla::EndPoint & ep_handle) throw (tcpla::exception::FailedToResetReceiveEntry)
{
	//std::cout << "going to delete UUID:" << ep_handle.toString() << std::endl;
	int pollfdsKey = ep_handle.pollfdsKey_;

	if (pollfdsKey > 0) // point in the array is greater than 0 (which is the listen)
	{
		//std::cout << "tcpla::PublicServicePointSelect::resetRcvEntry Going to reset entry for file descriptor '" << selectfds_[pollfdsKey] << "'" << std::endl;
		FD_CLR(selectfds_[pollfdsKey], &rset_);
		::close(selectfds_[pollfdsKey]);

		selectfds_[pollfdsKey] = tcpla::EMPTY_FD;

		rcvSocks_[pollfdsKey].ep_handle.clear();

		rcvSocks_[pollfdsKey].pollfdsKey = -1;
		rcvSocks_[pollfdsKey].connected = false;

		rcvSocks_[pollfdsKey].rcvnBytes = 0;
		rcvSocks_[pollfdsKey].length = 0;
		rcvSocks_[pollfdsKey].hostname = "";
		rcvSocks_[pollfdsKey].port = 0;
		rcvSocks_[pollfdsKey].counter = 0;
		rcvSocks_[pollfdsKey].readCounter = 0;
		rcvSocks_[pollfdsKey].idleBlockingReadCounter = 0;
		rcvSocks_[pollfdsKey].idleEmptyQueueCounter = 0;

		totalClients_--;

		while (!rcvSocks_[pollfdsKey].freeQueue->empty())
		{
			TCPLA_RECV_POST_DESC d = rcvSocks_[pollfdsKey].freeQueue->front();

			tcpla::Event event;

			event.event_number = TCPLA_DTO_RCV_UNUSED_BLOCK;
			event.event_data.tcpla_dto_block_event_data.user_cookie = d.cookie;
			event.event_data.tcpla_dto_block_event_data.block_length = d.size;
			event.event_data.tcpla_dto_block_event_data.buffer = d.local_iov;

			this->ia_->pushEvent(ia_->reqt_evd_hdl_, event);

			rcvSocks_[pollfdsKey].freeQueue->pop_front();
		}

		//reset maxfd for select call
		maxfd_ = listenfd_;

		for (int i = 1; i < this->maxClients_; i++)
		{
			if (selectfds_[i] != tcpla::EMPTY_FD)  // empty
			{
				if (selectfds_[i] > maxfd_)
				{
					maxfd_ = selectfds_[i];
				}
			}
		}

		int i = -1;
		for (i = this->maxPollfdsKey_; i > 0; i--)
		{
			if (selectfds_[i] != tcpla::EMPTY_FD)
			{
				this->maxPollfdsKey_ = i;
				break;
			}
		}

		// only listen socket remaining in array
		if (i == 0)
		{
			this->maxPollfdsKey_ = 0;
		}

	}
	else
	{
		std::stringstream ss;
		ss << "Cannot reset receive entry on invalid index " << pollfdsKey;

		XCEPT_RAISE(tcpla::exception::FailedToResetReceiveEntry, ss.str());
	}
}

int tcpla::PublicServicePointSelect::getFD (tcpla::EndPoint & ep_handle) throw (tcpla::exception::InvalidEndPoint)
{
	if (ep_handle.pollfdsKey_ != -1)
	{
		return this->selectfds_[ep_handle.pollfdsKey_];
	}
	else
	{
		std::stringstream ss;
		ss << "Invalid  EndPoint";

		XCEPT_RAISE(tcpla::exception::InvalidEndPoint, ss.str());
	}
}
