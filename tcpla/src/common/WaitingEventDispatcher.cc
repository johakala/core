// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2011, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A.Petrucci, C.Wakefield					 *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include "tcpla/WaitingEventDispatcher.h"

tcpla::WaitingEventDispatcher::WaitingEventDispatcher (size_t queueSize)
	: eventQueue_(queueSize)
{

}

tcpla::WaitingEventDispatcher::~WaitingEventDispatcher ()
{

}

void tcpla::WaitingEventDispatcher::push (tcpla::Event & e) throw (tcpla::exception::EventQueueFull)
{
	try
	{
		eventQueue_.push(e);
	}
	catch (toolbox::exception::QueueFull & e)
	{
		XCEPT_RETHROW(tcpla::exception::EventQueueFull, "Failed to push in waiting event dispatcher", e);
	}
}
