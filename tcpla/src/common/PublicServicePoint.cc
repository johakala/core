// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2012, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A.Petrucci, C.Wakefield				 *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include <sstream>
#include "tcpla/PublicServicePoint.h"
#include "tcpla/InterfaceAdapter.h"
#include "tcpla/Utils.h"
#include "tcpla/EventHandler.h"
#include "tcpla/EventDispatcher.h"

#include "tcpla/WaitingWorkLoop.h"
#include "tcpla/PollingWorkLoop.h"
#include "tcpla/InputIndexGenerator.h"

#include "toolbox/hexdump.h"
#include "toolbox/utils.h"

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
//#include <linux/sdp_inet.h>

tcpla::PublicServicePoint::PublicServicePoint (tcpla::InterfaceAdapter * ia, pt::Address::Reference address, size_t ioQueueSize, size_t headerSize) throw (tcpla::exception::Exception, tcpla::exception::InvalidRcvTimeOut, tcpla::exception::NoFreePortInRange, tcpla::exception::CannotBind)
	: ia_(ia), mutex_(toolbox::BSem::FULL)
{
	this->maxClients_ = ia_->maxClients_;

	tcpla::InputIndexGenerator * iig = tcpla::InputIndexGenerator::getInstance();
	indexId_ = (iig->getIndex()  * this->maxClients_);

	process_ = toolbox::task::bind(this, &PublicServicePoint::process, "process");

	listenAddress_ = dynamic_cast<tcpla::Address *>(&(*address));


	this->idleCounter_ = 0;
	this->port_ = 0;

	this->glf_ = ia_->glf_;
	this->headerSize_ = headerSize;

	this->totalClients_ = 0;

	this->affinity_ = listenAddress_->getAffinity();

	this->bestEffort_ = listenAddress_->hasProperty("datagramSize") | listenAddress_->hasProperty("maxBulkSize") | listenAddress_->hasProperty("maxbulksize") ;

	//targetTid for FRL
	try
	{
		this->targetId_ = (size_t) toolbox::toUnsignedLong(listenAddress_->getProperty("targetId"));
	}
	catch (toolbox::exception::Exception & e)
	{
		XCEPT_RETHROW(tcpla::exception::Exception, "Failed to set send targetTid, invalid size_t value provided in EndPoint configutation", e);
	}

	try
	{
		this->rcvTimeout_ = (int) toolbox::toLong(listenAddress_->getProperty("rcvTimeout"));
	}
	catch (toolbox::exception::Exception & e)
	{
		XCEPT_RETHROW(tcpla::exception::InvalidRcvTimeOut, "Failed to set receive timeout, invalid integer value provided in EndPoint configutation", e);
	}

	try
	{
		this->underflowTimeout_ = (size_t) toolbox::toUnsignedLong(listenAddress_->getProperty("underflowTimeout"));
	}
	catch (toolbox::exception::Exception & e)
	{
		XCEPT_RETHROW(tcpla::exception::Exception, "Failed to set send underflowTimeout, invalid size_t value provided in EndPoint configutation", e);
	}

	try
	{
		this->createListenSocket();
	}
	catch (tcpla::exception::SocketOption & e)
	{
		XCEPT_RETHROW(tcpla::exception::Exception, "Failed to create listen socket", e);
	}
	catch (tcpla::exception::FailedToSeReUseAddress & e)
	{
		XCEPT_RETHROW(tcpla::exception::Exception, "Failed to create listen socket", e);
	}
	catch (tcpla::exception::Exception & e)
	{
		XCEPT_RETHROW(tcpla::exception::Exception, "Failed to create listen socket", e);
	}

	//begin

	struct sockaddr_in serveraddr;

	bzero(&serveraddr, sizeof(serveraddr));

	this->port_ = toolbox::toUnsignedLong(listenAddress_->getProperty("port"));
	std::string hostname = listenAddress_->getProperty("hostname");

	bool autoScan = false;
	//new scan code
	if (listenAddress_->hasProperty("autoscan"))
	{
		autoScan = (listenAddress_->getProperty("autoscan") == "true") ? true : false;
	}

	if (autoScan)
	{
		uint16_t maxport = port_ + 100;
		if (listenAddress_->hasProperty("maxport"))
		{
			maxport = toolbox::toUnsignedLong(listenAddress_->getProperty("maxport"));
		}

		uint16_t p;

		for (p = this->port_; p <= maxport; p++)
		{
			bzero(&serveraddr, sizeof(serveraddr));
			serveraddr = toolbox::net::getSocketAddressByName(hostname, p);
			errno = 0;
			if (::bind(listenfd_, (struct sockaddr *) &serveraddr, sizeof(serveraddr)) == 0)
			{
				errno = 0;
				if ((::listen(listenfd_, this->maxClients_ - 1)) == 0)
				{
					this->port_ = p;
					break;
				}
				else
				{
					LOG4CPLUS_WARN(ia_->getOwnerApplication()->getApplicationLogger(), "Autoscan recreating listen socket");

					::close(listenfd_);
					try
					{
						this->createListenSocket();
					}
					catch (tcpla::exception::SocketOption & e)
					{
						XCEPT_RETHROW(tcpla::exception::Exception, "Failed to create listen socket", e);
					}
					catch (tcpla::exception::FailedToSeReUseAddress & e)
					{
						XCEPT_RETHROW(tcpla::exception::Exception, "Failed to create listen socket", e);
					}
					catch (tcpla::exception::Exception & e)
					{
						XCEPT_RETHROW(tcpla::exception::Exception, "Failed to create listen socket", e);
					}
				}
			}
		}

		if (p > maxport)
		{
			::close(listenfd_);

			std::stringstream ss;
			ss << "Cannot find free port in the range " << this->port_ << "-" << maxport << " in PublicServicePoint " << this->toString();

			XCEPT_RAISE(tcpla::exception::NoFreePortInRange, ss.str());
		}

	}
	else
	{
		bzero(&serveraddr, sizeof(serveraddr));
		serveraddr = toolbox::net::getSocketAddressByName(hostname, this->port_);
		errno = 0;

		if (::bind(listenfd_, (struct sockaddr *) &serveraddr, sizeof(serveraddr)) != 0)
		{
			std::stringstream ss;
			ss << "Failed to bind on port " << this->port_ << " in PublicServicePoint " << this->toString() << " socket error: " << strerror(errno);

			XCEPT_RAISE(tcpla::exception::CannotBind, ss.str());
		}

		errno = 0;
		if ((::listen(listenfd_, this->maxClients_ - 1)) != 0)
		{
			std::stringstream ss;
			ss << "Failed to listen on port " << this->port_ << " in PublicServicePoint " << this->toString() << " socket error: " << strerror(errno);

			XCEPT_RAISE(tcpla::exception::Exception, ss.str());
		}
	}
	//change port number if scan

	std::string assignedPort = toolbox::toString("%d", this->port_);
	listenAddress_->setProperty("assignedPort", toolbox::toString("%d", this->port_));

	rcvSocks_ = new TCPLA_RECEIVE_SOCKET_ENTRY[this->maxClients_];

	//initialise receive socket data && poll filedescriptors array

	for (int i = 0; i < this->maxClients_; i++)
	{
		// fill TCPLA_RECEIVE_SOCKET_ENTRY

		rcvSocks_[i].ep_handle.clear();
		rcvSocks_[i].pollfdsKey = -1;
		rcvSocks_[i].connected = false;

		std::stringstream ss;
		ss << "tcpla-psp-free-queue/" << ia_->hostname_ << "/" << assignedPort;

		rcvSocks_[i].freeQueue = toolbox::rlist < TCPLA_RECV_POST_DESC > ::create(ss.str(), ioQueueSize); //free buffer queue
		rcvSocks_[i].rcvnBytes = 0;
		rcvSocks_[i].length = 0;
		rcvSocks_[i].hostname = "";
		rcvSocks_[i].port = 0;
		rcvSocks_[i].counter = 0;
		rcvSocks_[i].readCounter = 0;
		rcvSocks_[i].idleBlockingReadCounter = 0;
		rcvSocks_[i].idleEmptyQueueCounter = 0;

		rcvSocks_[i].current.size = 0;
		rcvSocks_[i].current.local_iov = 0;
		rcvSocks_[i].current.cookie.as_ptr = 0;
	}

       commandQueue_ = toolbox::rlist<tcpla::InterfaceAdapter::Command>::create("tcpla-psp-command-queue",ioQueueSize);


	if (ia_->pspDispatcherThread_)
	{
		affinity_ = listenAddress_->getAffinity();

		try
		{
			//multi-threaded
			if (affinity_["DSR"] == "W")
			{
				recv_evd_hdl_ = new tcpla::WaitingEventDispatcher(ia_->eventQueueSize_);
				receiverWorkLoop_ = new tcpla::WaitingWorkLoop("tcpla-psp-receiver-dispatcher/" + ia_->hostname_ + "/" + assignedPort, recv_evd_hdl_, ia_->eventHandler_, affinity_["DSR"]);

			}
			else if (affinity_["DSR"] == "P")
			{
				recv_evd_hdl_ = new tcpla::PollingEventDispatcher(ia_->eventQueueSize_, "tcpla-psp-receiver-dispatcher/" + ia_->hostname_ + "/" + assignedPort);
				receiverWorkLoop_ = new tcpla::PollingWorkLoop("tcpla-receiver-psp-dispatcher/" + ia_->hostname_ + "/" + assignedPort, recv_evd_hdl_, ia_->eventHandler_, affinity_["DSR"]);

			}
			else
			{
				std::stringstream ss;
				ss << "Invalid affinity " << listenAddress_->getProperty("affinity") << " in PublicServicePoint " << ia_->hostname_;

				XCEPT_RAISE(tcpla::exception::Exception, ss.str());
			}
		}
		catch (tcpla::exception::Exception & e)
		{
			XCEPT_RETHROW(tcpla::exception::Exception, "Failed to create PSP workloops", e);
		}
	}
	/* This might not work if we start multiple processes in true parallel with non-blocking options set and autoscan as bind does not fail as it should
	 errno = 0;
	 if ((::listen(listenfd_, this->maxClients_ -1)) != 0)
	 {
	 std::stringstream ss;
	 ss << "Failed to listen on port " << this->port_ << " in PublicServicePoint " << this->toString() << " socket error: " << strerror(errno);

	 XCEPT_RAISE(tcpla::exception::Exception, ss.str());
	 }
	 */
}

void tcpla::PublicServicePoint::createListenSocket () throw (tcpla::exception::Exception, tcpla::exception::SocketOption, tcpla::exception::FailedToSeReUseAddress)
{
	//preparing socket
	if (listenAddress_->getProperty("family") == "AF_INET")
	{
		listenfd_ = socket(AF_INET, SOCK_STREAM, 0);
	}
	/*else if ( listenAddress_->getProperty("family") == "AF_INET_SDP")
	 {
	 listenfd_ = socket(AF_INET_SDP, SOCK_STREAM, 0);
	 }*/
	else
	{
		std::stringstream ss;
		ss << "Failed to set socket invalid family " << listenAddress_->getProperty("family") << " type in PSP" << this->toString();

		XCEPT_RAISE(tcpla::exception::Exception, ss.str());
	}

	//int listenfd = socket(AF_INET_SDP, SOCK_STREAM, 0);

	//servaddr.sin_family = AF_INET;
	//servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
	//servaddr.sin_port = htons(port);
	errno = 0;

	if (listenAddress_->getProperty("nonblock") == "true")
	{
		int sockOpt = fcntl(listenfd_, F_GETFL, 0);
		if (fcntl(listenfd_, F_SETFL, sockOpt | O_NONBLOCK) < 0)
		{
			::close(listenfd_);

			std::stringstream ss;
			ss << "Failed to set socket to non-blocking in PublicServicePoint " << this->toString() << " socket error: " << strerror(errno);

			XCEPT_RAISE(tcpla::exception::SocketOption, ss.str());
		}
	}
	else
	{
		// if BLOCKING socket, pollingCycle must be 1 to avoid problems
		if (this->ia_->pollingCycle_ > 1)
		{
			XCEPT_RAISE(tcpla::exception::Exception, "pollingCycle must be set to '1' when using blocking socket");
		}
	}

	errno = 0;
	int optval = 1;
	if (setsockopt(listenfd_, SOL_SOCKET, SO_REUSEADDR, (char *) &optval, sizeof(optval)) < 0)
	{
		std::stringstream ss;
		ss << "Failed to set socket to non-blocking in PublicServicePoint " << this->toString() << " socket error: " << strerror(errno);

		XCEPT_RAISE(tcpla::exception::FailedToSeReUseAddress, ss.str());
	}
}

tcpla::PublicServicePoint::~PublicServicePoint ()
{
	for (int i = 0; i < this->maxClients_; i++)
	{
#warning "missing DTOR for PSP"
		//this->resetRcvEntry(i);
		toolbox::rlist<TCPLA_RECV_POST_DESC>::destroy(rcvSocks_[i].freeQueue);
	}

	toolbox::rlist<tcpla::InterfaceAdapter::Command>::destroy(commandQueue_);

	delete[] rcvSocks_;

	delete recv_evd_hdl_;

}

tcpla::Address* tcpla::PublicServicePoint::getAddress ()
{
	return listenAddress_;
}

void tcpla::PublicServicePoint::destroyEndPoint (tcpla::EndPoint & ep_handle) throw (tcpla::exception::PSPCommandQueueFull)
{
	mutex_.take();

	//std::cout << " ---->>>>> tcpla::InterfaceAdapter::destroyEndPointtcpla::EndPoint * ep_handle ) push into queue" << std::endl;
	try
	{
		tcpla::InterfaceAdapter::Command c;

		c.command_number = tcpla::InterfaceAdapter::TCPLA_DESTROY_COMMAND_NUMBER;
		c.command.destroy_command.ep_handle = ep_handle;

		commandQueue_->push_back(c);

		mutex_.give();
	}
	catch (toolbox::exception::RingListFull & e)
	{
		mutex_.give();

		std::stringstream ss;
		ss << "Failed to post destroy EndPoint on PSP " << this->toString();

		XCEPT_RETHROW(tcpla::exception::PSPCommandQueueFull, ss.str(), e);
	}
}
/*
 if ( ep_handle->psp_ != 0 ) //endpoint belongs to a PSP
 {
 ep_handle->psp_->resetRcvEntry(ep_handle->pollfdsKey_);
 }*/
void tcpla::PublicServicePoint::destroyEndPoint (tcpla::InterfaceAdapter::Command & c) throw (tcpla::exception::Exception)
{
	tcpla::EndPoint & ep_handle = c.command.destroy_command.ep_handle;
	// check if object is still existing and is a valid allocated object
	ia_->mutex_.take();

	if (ep_handle.psp_ != 0) //endpoint belongs to a PSP so delegate destroy of endpoint to PSP loop
	{
		std::list<tcpla::EndPoint>::iterator i;
		for (i = ia_->endpointRegistry_.begin(); i != ia_->endpointRegistry_.end(); i++)
		{
			if ((*i) == ep_handle)
			{
				try
				{
					this->resetRcvEntry(ep_handle);
				}
				catch (tcpla::exception::FailedToResetReceiveEntry & e)
				{
					ia_->mutex_.give();
					std::stringstream ss;
					ss << "Failed to reset in PSP " << this->toString();
					XCEPT_RETHROW(tcpla::exception::Exception, ss.str(), e);
				}

				ia_->endpointRegistry_.erase(i);
				break;
			}
		}
		if (i == ia_->endpointRegistry_.end())
		{
			LOG4CPLUS_DEBUG(ia_->getOwnerApplication()->getApplicationLogger(), "Attempt to destroy duplicate end point in PSP - UUID = " << ep_handle.toString());
		}

		ia_->mutex_.give();
	}
	else
	{
		ia_->mutex_.give();
		XCEPT_RAISE(tcpla::exception::Exception, "invalid PSP endpoint");
	}
}

void tcpla::PublicServicePoint::accept (tcpla::EndPoint & ep_handle, int cr_handle, TCPLA_CONTEXT & cookie) throw (tcpla::exception::PSPCommandQueueFull)
{
	mutex_.take();

	try
	{
		tcpla::InterfaceAdapter::Command c;

		c.command_number = tcpla::InterfaceAdapter::TCPLA_ACCEPT_COMMAND_NUMBER;

		c.command.accept_command.ep_handle = ep_handle;
		c.command.accept_command.cr_handle = cr_handle;
		c.command.accept_command.cookie = cookie;

		commandQueue_->push_back(c);

		mutex_.give();
	}
	catch (toolbox::exception::RingListFull & e)
	{
		mutex_.give();

		std::stringstream ss;
		ss << "Failed to post accept in PublicServicePoint " << this->toString();

		XCEPT_RETHROW(tcpla::exception::PSPCommandQueueFull, ss.str(), e);
	}
}

void tcpla::PublicServicePoint::safePostRecvBuffer (tcpla::EndPoint & ep_handle, TCPLA_CONTEXT & cookie, char * local_iov, size_t size) throw (tcpla::exception::PSPCommandQueueFull)
{
	mutex_.take();

	try
	{
		tcpla::InterfaceAdapter::Command c;

		c.command_number = tcpla::InterfaceAdapter::TCPLA_POST_RECV_BUF_COMMAND_NUMBER;

		c.command.post_recv_buf_command.ep_handle = ep_handle;
		c.command.post_recv_buf_command.local_iov = local_iov;
		c.command.post_recv_buf_command.size = size;
		c.command.post_recv_buf_command.cookie = cookie;

		commandQueue_->push_back(c);

		mutex_.give();
	}
	catch (toolbox::exception::RingListFull & e)
	{
		mutex_.give();

		std::stringstream ss;
		ss << "Failed to post receive buffer in PublicServicePoint " << this->toString();

		XCEPT_RETHROW(tcpla::exception::PSPCommandQueueFull, ss.str(), e);
	}
}

int tcpla::PublicServicePoint::acceptSocket (int fd, struct sockaddr_in * cliaddr) throw (tcpla::exception::FailedToAccept, tcpla::exception::FailedToGetFileControlSettings, tcpla::exception::FailedToSetNonBlocking)
{
	int connfd;
	errno = 0;

	socklen_t size = sizeof(struct sockaddr_in);

	if ((connfd = ::accept(fd, (sockaddr*) cliaddr, &size)) < 0)
	{
		if (errno != EWOULDBLOCK && errno != ECONNABORTED && errno != EPROTO && errno != EINTR)
		{
			std::stringstream ss;
			ss << "Failed to accept in PublicServicePoint " << this->toString() << " socket error: " << strerror(errno);

			XCEPT_RAISE(tcpla::exception::FailedToAccept, ss.str());
		}

		return -1;
	}

	std::stringstream ss;
	ss << "connection accepted from " << inet_ntoa(cliaddr->sin_addr) << ":" << ntohs(cliaddr->sin_port) << "fd = " << connfd;

	LOG4CPLUS_DEBUG(ia_->getOwnerApplication()->getApplicationLogger(), ss.str());

	if (listenAddress_->getProperty("nonblock") == "true")
	{
		int sockOpt;
		errno = 0;
		if ((sockOpt = fcntl(connfd, F_GETFL, 0)) < 0)
		{
			std::stringstream ss;
			ss << "Failed to retrieve file control settings in PublicServicePoint " << this->toString() << " file control error: " << strerror(errno);

			XCEPT_RAISE(tcpla::exception::FailedToGetFileControlSettings, ss.str());
		}

		sockOpt |= O_NONBLOCK;
		errno = 0;
		if (fcntl(connfd, F_SETFL, sockOpt) < 0)
		{
			std::stringstream ss;
			ss << "Failed to set socket to non-blocking in PublicServicePoint " << this->toString() << " socket error: " << strerror(errno);

			XCEPT_RAISE(tcpla::exception::FailedToSetNonBlocking, ss.str());
		}
	}

	return connfd;
}

size_t tcpla::PublicServicePoint::receive (int socket, char * buf, size_t len) throw (tcpla::exception::ConnectResetByPeer, tcpla::exception::FailedToReceive, tcpla::exception::ConnectionClosedByPeer)
{
	int nBytes = 0;

	errno = 0;

	nBytes = ::recv(socket, buf, len, 0);

	if (nBytes < 0)
	{
		if (errno == EAGAIN /*|| errno == EWOULDBLOCK*/)
		{
			return 0;
		}
		else if (errno == ECONNRESET)
		{
			std::stringstream ss;
			ss << "Connection reset by peer in PublicServicePoint " << this->toString() << " on file descriptor " << socket << " socket error: " << strerror(errno);

			XCEPT_RAISE(tcpla::exception::ConnectResetByPeer, ss.str());
		}
		else
		{
			std::stringstream ss;
			ss << "Failed to receive in PublicServicePoint " << this->toString() << " on file descriptor " << socket << " socket error: " << strerror(errno);

			XCEPT_RAISE(tcpla::exception::FailedToReceive, ss.str());
		}
	}
	else if (nBytes == 0)
	{
		std::stringstream ss;
		ss << "Connection closed by peer in PublicServicePoint " << this->toString() << " on file descriptor " << socket << " socket error: " << strerror(errno);

		XCEPT_RAISE(tcpla::exception::ConnectionClosedByPeer, ss.str());
	}

	return nBytes;
}

void tcpla::PublicServicePoint::postRecvBuffer (tcpla::EndPoint & ep_handle, TCPLA_CONTEXT & cookie, char * local_iov, size_t size) throw (tcpla::exception::FailedToPostSend, tcpla::exception::InvalidEndPoint)
{
	TCPLA_RECV_POST_DESC pd;

	pd.cookie = cookie;
	pd.local_iov = local_iov;
	pd.size = size;

	int index = ep_handle.pollfdsKey_;

	//if ( ep_handle != 0 )
	//{
	if (index != -1)
	{
		if (!rcvSocks_[index].connected)
		{
			std::stringstream ss;
			ss << "tcpla::PublicServicePoint::postRecvBuffer : EndPoint not connected in PSP " << this->toString() << ", tolerated to allow posting receive buffers before accepting connection";
			LOG4CPLUS_TRACE(ia_->getOwnerApplication()->getApplicationLogger(), ss.str());
			//XCEPT_RAISE(tcpla::exception::EndPointNotConnected, ss.str()); // if we want to be able to post to recieve before accepting, we need to not raise here
		}

		try
		{
			rcvSocks_[index].freeQueue->push_back(pd);
		}
		catch (toolbox::exception::RingListFull & e)
		{
			std::stringstream ss;
			ss << "Failed to enqueue buffer for index " << index << " of size " << size << " in PublicServicePoint " << this->toString();
			XCEPT_RETHROW(tcpla::exception::FailedToPostSend, ss.str(), e);
		}
	}
	else
	{
		std::stringstream ss;
		ss << "Invalid index " << index << " for EndPoint in InterfaceAdapter " << this->toString();
		XCEPT_RAISE(tcpla::exception::InvalidEndPoint, ss.str());
	}
}

/*
 void tcpla::PublicServicePoint::resetRcvEntry ( tcpla::EndPoint * ep_handle ) throw ( tcpla::exception::Exception )
 {
 mutex_.take();

 try
 {
 tcpla::InterfaceAdapter::Command c;

 c.command_number = tcpla::InterfaceAdapter::TCPLA_RESET_COMMAND_NUMBER;
 c.command.reset_command.pollfdsKey = ep_handle->pollfdsKey;

 commandQueue_->push_back(c);

 mutex_.give();
 }
 catch (toolbox::exception::RingListFull & e)
 {
 mutex_.give();

 std::stringstream ss;
 ss << "Failed to post reset receive entry in PublicServicePoint " << this->toString();

 XCEPT_RETHROW(tcpla::exception::PSPCommandQueueFull, ss.str(), e);
 }
 }
 */

std::string tcpla::PublicServicePoint::toString ()
{
	std::stringstream ss;

	ss << this->ia_->hostname_ << ":" << this->port_;

	return ss.str();
}

void tcpla::PublicServicePoint::eval () throw (tcpla::exception::Exception)
{
	tcpla::InterfaceAdapter::Command cmd = commandQueue_->front();
	commandQueue_->pop_front();

	if (cmd.command_number == tcpla::InterfaceAdapter::TCPLA_ACCEPT_COMMAND_NUMBER)
	{
		//try
		//{
			this->accept(cmd);
		//}
			/*
		catch (tcpla::exception::Exception & e)
		{
			tcpla::Event event;

			event.event_number = TCPLA_CONNECTION_EVENT_ACCEPT_COMPLETION_ERROR;
			event.event_data.asynch_error_event_data.ep_handle.clear();
			event.event_data.asynch_error_event_data.reason = 9;

			std::stringstream ss;
			ss << "Failed to process accept command in PublicServicePoint " << this->toString();

			event.event_data.asynch_error_event_data.exception = TCPLA_XCEPT_ALLOC_NESTED(tcpla::exception::FailedToProcessAcceptCommand, ss.str(), e);

			this->ia_->pushEvent(ia_->reqt_evd_hdl_, event);
		}
		*/
	}
	else if (cmd.command_number == tcpla::InterfaceAdapter::TCPLA_DESTROY_COMMAND_NUMBER)
	{
		try
		{
			//std::cout << "i am going to execute a reset endpoint command" << std::endl;
			this->destroyEndPoint(cmd);
		}
		catch (tcpla::exception::Exception & e)
		{
			tcpla::Event event;

			event.event_number = TCPLA_ASYNC_ERROR_EP_BROKEN;
			event.event_data.asynch_error_event_data.ep_handle.clear();
			event.event_data.asynch_error_event_data.reason = 10;

			std::stringstream ss;
			ss << "Failed to process reset receive entry command in PublicServicePoint " << this->toString();

			event.event_data.asynch_error_event_data.exception = TCPLA_XCEPT_ALLOC_NESTED(tcpla::exception::FailedToProcessResetReceiveEntryCommand, ss.str(), e);

			this->ia_->pushEvent(ia_->reqt_evd_hdl_, event);
		}
	}
	else if (cmd.command_number == tcpla::InterfaceAdapter::TCPLA_POST_RECV_BUF_COMMAND_NUMBER)
	{
		ia_->mutex_.take();
		// check ep_handle is still ready
		std::list<tcpla::EndPoint>::iterator i;
		for (i = ia_->endpointRegistry_.begin(); i != ia_->endpointRegistry_.end(); i++)
		{
			if ((*i) == cmd.command.post_recv_buf_command.ep_handle)
			{
				try
				{
					this->postRecvBuffer(cmd.command.post_recv_buf_command.ep_handle, cmd.command.post_recv_buf_command.cookie, cmd.command.post_recv_buf_command.local_iov, cmd.command.post_recv_buf_command.size);
					ia_->mutex_.give();
					return;
				}
				catch (tcpla::exception::FailedToPostSend & e)
				{
					ia_->mutex_.give();
					tcpla::Event event;

					event.event_number = TCPLA_SOFTWARE_EVENT;
					event.event_data.software_event_data.reason = 9;

					std::stringstream ss;
					ss << "Failed to post recv buffer in PSP " << this->toString() << " : " << e.message();

					event.event_data.software_event_data.exception = TCPLA_XCEPT_ALLOC(tcpla::exception::Exception, ss.str());

					this->ia_->pushEvent(ia_->reqt_evd_hdl_, event);
					break;
				}
				catch (tcpla::exception::InvalidEndPoint & e)
				{
					ia_->mutex_.give();
					tcpla::Event event;

					event.event_number = TCPLA_SOFTWARE_EVENT;
					event.event_data.software_event_data.reason = 9;

					std::stringstream ss;
					ss << "Failed to post recv buffer in PSP " << this->toString() << " : " << e.message();

					event.event_data.software_event_data.exception = TCPLA_XCEPT_ALLOC(tcpla::exception::Exception, ss.str());

					this->ia_->pushEvent(ia_->reqt_evd_hdl_, event);
					break;
				}
			}
		}

		ia_->mutex_.give();

		// EndPoint not valid, recover the block, alert user

		tcpla::Event event;

		event.event_number = TCPLA_DTO_RCV_UNUSED_BLOCK;
		event.event_data.tcpla_dto_block_event_data.user_cookie = cmd.command.post_recv_buf_command.cookie;
		event.event_data.tcpla_dto_block_event_data.block_length = cmd.command.post_recv_buf_command.size;
		event.event_data.tcpla_dto_block_event_data.buffer = cmd.command.post_recv_buf_command.local_iov;

		if (this->ia_->pspDispatcherThread_)
		{
			this->ia_->pushEvent(recv_evd_hdl_, event);
		}
		else
		{
			this->ia_->pushEvent(ia_->recv_evd_hdl_, event);
		}
	}
	else
	{
		tcpla::Event event;

		event.event_number = TCPLA_SOFTWARE_EVENT;
		event.event_data.software_event_data.reason = 9;

		std::stringstream ss;
		ss << "Failed to process unknown command in PSP " << this->toString();

		event.event_data.software_event_data.exception = TCPLA_XCEPT_ALLOC(tcpla::exception::Exception, ss.str());

		this->ia_->pushEvent(ia_->reqt_evd_hdl_, event);
	}

}

void tcpla::PublicServicePoint::processSocketEntries ()
{
	for (int j = 1; j <= this->maxPollfdsKey_; j++)
	{
		int pollfdsKey = rcvSocks_[j].pollfdsKey;
		if (pollfdsKey == -1)
		{
			idleCounter_++;
			continue;
		}

		int currentfd = this->getFD(pollfdsKey);

		if (currentfd < 0)
		{
			idleCounter_++;
			continue;
		}

		if (!rcvSocks_[j].connected) //socket accept pending
		{
			idleCounter_++;
			continue;
		}

		// check data available from  sockets
		if (this->isReady(pollfdsKey))
		{
			for (size_t cycle = 0; cycle < this->ia_->pollingCycle_; cycle++)
			{

				//LOG4CPLUS_DEBUG(ia_->getOwnerApplication()->getApplicationLogger(), "POLLIN event in PSP");

				if (rcvSocks_[j].freeQueue->empty() && rcvSocks_[j].connected)  // nothing to send
				{
					rcvSocks_[j].idleEmptyQueueCounter++;

					/*tcpla::Event event;

					 event.event_number = TCPLA_MEMORY_UNDERFLOW_EVENT;
					 event.event_data.tcpla_memory_underflow_event_data.ep_handle = rcvSocks_[j].ep_handle;
					 event.event_data.tcpla_memory_underflow_event_data.psp = this;

					 //ia_->eventHandler_->handleEvent(event);

					 this->ia_->pushEvent(recv_evd_hdl_, event);

					 toolbox::u_sleep(this->underflowTimeout_); //100ms
					 */
					break;
				}
				else
				{
					if (rcvSocks_[j].rcvnBytes == 0)
					{
						rcvSocks_[j].current = rcvSocks_[j].freeQueue->front();
					}
				}

				if (rcvSocks_[j].rcvnBytes == 0 || rcvSocks_[j].rcvnBytes < this->headerSize_) //need to read the header
				{
					//std::cout << "Going to receive header" << std::endl;
					try
					{
						size_t nbytes = this->receive(currentfd, rcvSocks_[j].current.local_iov + rcvSocks_[j].rcvnBytes, (int) (this->headerSize_ - rcvSocks_[j].rcvnBytes));

						if (nbytes == 0)
						{
							rcvSocks_[j].idleBlockingReadCounter++;
							break;
						}

						rcvSocks_[j].readCounter++;
						rcvSocks_[j].rcvnBytes += nbytes;
						//std::cout << "Header received " << nbytes << " in PSP" << std::endl;

					}
					catch (tcpla::exception::ConnectionClosedByPeer & e)
					{
						tcpla::Event event;

						event.event_number = TCPLA_CONNECTION_CLOSED_BY_PEER;
						//event.event_data.connection_broken_event_data.ep_handle = rcvSocks_[j].ep_handle;

						/*
						 if ( rcvSocks_[j].ep_handle  == 0 )
						 {
						 std::cout << "BAD BAD BAD BAD" << std::endl;
						 }
						 */

						event.event_data.connection_broken_event_data.ep_handle = rcvSocks_[j].ep_handle;

						event.event_data.connection_broken_event_data.ia = ia_;
						event.event_data.connection_broken_event_data.reason = 3;

						std::stringstream ss;
						ss << "Failed to receive, connection event closed in PublicServicePoint " << this->toString() << " : UUID = '" << rcvSocks_[j].ep_handle.toString() << "'";

						event.event_data.connection_broken_event_data.exception = TCPLA_XCEPT_ALLOC_NESTED (tcpla::exception::FailedToReceive, ss.str(), e);

						//pollfds_[pollfdsKey].events = 0;
						//FD_CLR(selectfds_[pollfdsKey],&rset_);
						this->disableFD(pollfdsKey);

						this->ia_->pushEvent(ia_->creq_evd_hdl_, event);

						break;
					}
					catch (tcpla::exception::ConnectResetByPeer & e)
					{

						tcpla::Event event;

						event.event_number = TCPLA_CONNECTION_RESET_BY_PEER;
						event.event_data.connection_broken_event_data.ep_handle = rcvSocks_[j].ep_handle;
						event.event_data.connection_broken_event_data.ia = ia_;
						event.event_data.connection_broken_event_data.reason = 3;

						std::stringstream ss;
						ss << "Failed to receive, connection event reset in PublicServicePoint " << this->toString() << " : UUID = '" << rcvSocks_[j].ep_handle.toString() << "'";

						event.event_data.connection_broken_event_data.exception = TCPLA_XCEPT_ALLOC_NESTED (tcpla::exception::FailedToReceive, ss.str(), e);

						//pollfds_[pollfdsKey].events = 0;
						//FD_CLR(selectfds_[pollfdsKey],&rset_);
						this->disableFD(pollfdsKey);

						this->ia_->pushEvent(ia_->creq_evd_hdl_, event);

						break;
					}
					catch (tcpla::exception::FailedToReceive & e)
					{
						tcpla::Event event;

						event.event_number = TCPLA_CONNECTION_EVENT_BROKEN;
						event.event_data.connection_broken_event_data.ep_handle = rcvSocks_[j].ep_handle;
						event.event_data.connection_broken_event_data.ia = ia_;
						event.event_data.connection_broken_event_data.reason = 3;

						std::stringstream ss;
						ss << "Failed to receive, connection event broken in PublicServicePoint " << this->toString() << " : UUID = '" << rcvSocks_[j].ep_handle.toString() << "'";

						event.event_data.connection_broken_event_data.exception = TCPLA_XCEPT_ALLOC_NESTED (tcpla::exception::FailedToReceive, ss.str(), e);

						//FD_CLR(selectfds_[pollfdsKey],&rset_);
						//pollfds_[pollfdsKey].events = 0;
						this->disableFD(pollfdsKey);

						this->ia_->pushEvent(ia_->creq_evd_hdl_, event);

						break;
					}

					if (bestEffort_)
					{
						rcvSocks_[j].length = rcvSocks_[j].rcvnBytes;
						/*	if ( ( rcvSocks_[j].rcvnBytes % 8) == 0 )
						 {
						 rcvSocks_[j].length = rcvSocks_[j].rcvnBytes;
						 }
						 else
						 {
						 size_t misaligned = (rcvSocks_[j].rcvnBytes  + 8 )  >> 4 ;
						 rcvSocks_[j].length = misaligned << 4 ;
						 }*/
						//std::cout << "Best effort header size (dgram): " << this->headerSize_ << std::endl;
					}
					else if (rcvSocks_[j].rcvnBytes == this->headerSize_) //the entire header has been received
					{
						//if ( (rcvSocks_[j].header.length - this->headerSize_) >= rcvSocks_[j].current.size )

						size_t packetLength = this->ia_->glf_((char *) rcvSocks_[j].current.local_iov);

						if ((packetLength) > rcvSocks_[j].current.size)
						{
							std::stringstream ss;

							ss << "Receiving block too small, allowed maximum size: " << (rcvSocks_[j].current.size) << " received size: " << packetLength << " in PublicServicePoint " << this->toString();

							tcpla::Event event;

							event.event_number = TCPLA_ASYNC_ERROR_IA_CATASTROPHIC;
							event.event_data.asynch_error_event_data.ep_handle = rcvSocks_[j].ep_handle;
							event.event_data.asynch_error_event_data.reason = 2;
							event.event_data.asynch_error_event_data.exception = TCPLA_XCEPT_ALLOC (tcpla::exception::Exception, ss.str());

							this->ia_->pushEvent(ia_->reqt_evd_hdl_, event);

							//mutex_.give();
							return;
						}

						rcvSocks_[j].length = packetLength;
						//store chain info
						//rcvSocks_[j].packet = rcvSocks_[j].header.packet;
						//rcvSocks_[j].total = rcvSocks_[j].header.total;
						//rcvSocks_[j].identification = rcvSocks_[j].header.identification;

					}
				}

				if (rcvSocks_[j].rcvnBytes >= this->headerSize_ && rcvSocks_[j].rcvnBytes < rcvSocks_[j].length)
				{
					try
					{
						//size_t offset = rcvSocks_[j].rcvnBytes + this->headerSize_;
						size_t offset = rcvSocks_[j].rcvnBytes;
						size_t size = (rcvSocks_[j].length) - offset; //header may need to be in struct??

						size_t nbytes = this->receive(currentfd, (char *) rcvSocks_[j].current.local_iov + offset, size);
						rcvSocks_[j].readCounter++;
						if (nbytes == 0)
						{
							rcvSocks_[j].idleBlockingReadCounter++;
							break;
						}

						//std::cout << "Payload received " << nbytes << " in PSP" << std::endl;

						rcvSocks_[j].rcvnBytes += nbytes;

					}
					catch (tcpla::exception::ConnectionClosedByPeer & e)
					{
						tcpla::Event event;

						event.event_number = TCPLA_CONNECTION_CLOSED_BY_PEER;
						event.event_data.connection_broken_event_data.ep_handle = rcvSocks_[j].ep_handle;
						event.event_data.connection_broken_event_data.ia = ia_;
						event.event_data.connection_broken_event_data.reason = 3;

						std::stringstream ss;
						ss << "Failed to receive, connection event closed in PublicServicePoint " << this->toString() << " : UUID = '" << rcvSocks_[j].ep_handle.toString() << "'";

						event.event_data.connection_broken_event_data.exception = TCPLA_XCEPT_ALLOC_NESTED (tcpla::exception::FailedToReceive, ss.str(), e);

						//pollfds_[pollfdsKey].events = 0;
						//FD_CLR(selectfds_[pollfdsKey],&rset_);
						this->disableFD(pollfdsKey);

						this->ia_->pushEvent(ia_->creq_evd_hdl_, event);

						break;
					}
					catch (tcpla::exception::ConnectResetByPeer & e)
					{

						tcpla::Event event;

						event.event_number = TCPLA_CONNECTION_RESET_BY_PEER;
						event.event_data.connection_broken_event_data.ep_handle = rcvSocks_[j].ep_handle;
						event.event_data.connection_broken_event_data.ia = ia_;
						event.event_data.connection_broken_event_data.reason = 3;

						std::stringstream ss;
						ss << "Failed to receive, connection event reset in PublicServicePoint " << this->toString() << " : UUID = '" << rcvSocks_[j].ep_handle.toString() << "'";

						event.event_data.connection_broken_event_data.exception = TCPLA_XCEPT_ALLOC_NESTED (tcpla::exception::FailedToReceive, ss.str(), e);

						//pollfds_[pollfdsKey].events = 0;
						//FD_CLR(selectfds_[pollfdsKey],&rset_);
						this->disableFD(pollfdsKey);

						ia_->creq_evd_hdl_->push(event);

						break;
					}
					catch (tcpla::exception::FailedToReceive & e)
					{
						tcpla::Event event;

						event.event_number = TCPLA_CONNECTION_EVENT_BROKEN;
						event.event_data.connection_broken_event_data.ep_handle = rcvSocks_[j].ep_handle;
						event.event_data.connection_broken_event_data.ia = ia_;
						event.event_data.connection_broken_event_data.reason = 3;

						std::stringstream ss;
						ss << "Failed to receive, connection event broken in PublicServicePoint " << this->toString() << " : UUID = '" << rcvSocks_[j].ep_handle.toString() << "'";

						event.event_data.connection_broken_event_data.exception = TCPLA_XCEPT_ALLOC_NESTED (tcpla::exception::FailedToReceive, ss.str(), e);

						//FD_CLR(selectfds_[pollfdsKey],&rset_);
						//pollfds_[pollfdsKey].events = 0;
						this->disableFD(pollfdsKey);

						this->ia_->pushEvent(ia_->creq_evd_hdl_, event);

						break;
					}
				}

				if (rcvSocks_[j].length > 0 && rcvSocks_[j].rcvnBytes == rcvSocks_[j].length) //the whole packet has been read
				{
					//std::cout << "Received full packet " << rcvSocks_[j].rcvnBytes << std::endl;

					rcvSocks_[j].freeQueue->pop_front();

					tcpla::Event event;

					event.event_number = TCPLA_DTO_RCV_COMPLETION_EVENT;
					event.event_data.dto_completion_event_data.user_cookie = rcvSocks_[j].current.cookie;
					event.event_data.dto_completion_event_data.ep_handle = rcvSocks_[j].ep_handle;
					event.event_data.dto_completion_event_data.transfered_length = rcvSocks_[j].rcvnBytes;
					event.event_data.dto_completion_event_data.psp = this;

					event.event_data.dto_completion_event_data.block_length = rcvSocks_[j].current.size;
					event.event_data.dto_completion_event_data.buffer = rcvSocks_[j].current.local_iov;

					//chain info
					event.event_data.dto_completion_event_data.identification = rcvSocks_[j].identification;
					//event.event_data.dto_completion_event_data.total = rcvSocks_[j].total;
					//event.event_data.dto_completion_event_data.packet = rcvSocks_[j].packet;
					//uint32_t flags = rcvSocks_[j].current.flags;

					rcvSocks_[j].rcvnBytes = 0;
					rcvSocks_[j].length = 0;
					rcvSocks_[j].current.size = 0;
					rcvSocks_[j].identification = 0;
					//rcvSocks_[j].total = 0;
					//rcvSocks_[j].packet = 0;
					rcvSocks_[j].counter++;

					if (ia_->singleThread_)
					{
						ia_->eventHandler_->handleEvent(event);

					}
					else if (ia_->pspDispatcherThread_)
					{
						this->ia_->pushEvent(recv_evd_hdl_, event);
					}
					else
					{
						this->ia_->pushEvent(ia_->recv_evd_hdl_, event);
					}

					//std::cout << " I am going to return if flags " << std::endl;
					// if a connection close is requested we should give a chance for the endpoint to be destroy
					//if (flags & tcpla::TCPLA_CONNECTION_CLOSE )
					//{
					// This means that we are passing a pointer to an object we intent to destroy into an asynchronous queue, which is DANGEROUS
					/*
					 tcpla::InterfaceAdapter::Command c;

					 c.command_number = TCPLA_DESTROY_COMMAND_NUMBER;
					 c.command.destroy_command.ep_handle = rcvSocks_[j].ep_handle;

					 this->destroyEndpoint(c); // synchronous
					 */

					// so instead, we are doing this to give an opportunity for the destroy endpoint to happen before we go back into receive, which takes time, and makes everything slow
					//return;
					//}
				}
			}					//for loop
		} // FD IS SET
	} //end of fds for loop
}

namespace tcpla
{
	std::ostream &operator<< (std::ostream &sout, tcpla::PublicServicePoint & psp)
	{
		sout << cgicc::table().set("class", "xdaq-table") << std::endl;

		size_t recvBlockSize = 0;
		if (psp.listenAddress_->hasProperty("recvBlockSize"))
		{
			try
			{
				recvBlockSize = toolbox::toUnsignedLong(psp.listenAddress_->getProperty("recvBlockSize"));
			}
			catch (toolbox::exception::Exception & ex)
			{
				// hyperdaq, so we dont care
			}
		}

		/*
		sout << cgicc::tr();
		sout << cgicc::th().set("colspan", "13").set("style", "text-align:center");
		sout << psp.ia_->id_ << ":" << toolbox::toString("%d", psp.port_) << " : recvBlockSize = " << recvBlockSize;
		sout << cgicc::th();
		sout << cgicc::tr() << std::endl;
		*/

		sout << "<caption>" << psp.ia_->id_ << ":" << toolbox::toString("%d", psp.port_) << "</caption>" << std::endl;

		sout << cgicc::thead() << std::endl;

		sout << cgicc::tr();
		sout << cgicc::th("IP");
		sout << cgicc::th("Port");
		sout << cgicc::th("Poll FDS Key");
		sout << cgicc::th("FD");
		sout << cgicc::th("Connected");
		sout << cgicc::th("Buffer Status");
		sout << cgicc::th("To Receive (Bytes)");
		sout << cgicc::th("Received (Bytes)");
		sout << cgicc::th("Available Buffers");
		sout << cgicc::th("Messages Received");
		sout << cgicc::th("Socket Reads");
		sout << cgicc::th("Idle Empty Queue");
		sout << cgicc::th("Idle Blocking");
		sout << cgicc::tr() << std::endl;
		//psp.//mutex_.take();
		sout << cgicc::thead() << std::endl;

		// Counters
		size_t toRecv = 0;
		size_t recvd = 0;
		size_t avaBuf = 0;
		size_t msgRecv = 0;
		size_t sockReads = 0;
		size_t idleEmpty = 0;
		size_t idleBlock = 0;

		size_t rows = 0;

		bool initBody = false;

		for (int j = 0; j < psp.maxClients_; j++)
		{
			if (psp.rcvSocks_[j].pollfdsKey != -1)
			{
				if (!initBody)
				{
					initBody = true;
					sout << cgicc::tbody() << std::endl;
				}

				sout << cgicc::tr();
				sout << cgicc::td(psp.rcvSocks_[j].hostname);
				sout << cgicc::td(toolbox::toString("%d", psp.rcvSocks_[j].port));
				sout << cgicc::td(toolbox::toString("%d", psp.rcvSocks_[j].pollfdsKey));
				sout << cgicc::td(toolbox::toString("%d", psp.getFD(psp.rcvSocks_[j].pollfdsKey)));
				sout << cgicc::td(((psp.rcvSocks_[j].connected) ? "true" : "false"));
				sout << cgicc::td(((psp.rcvSocks_[j].current.size != 0) ? "receiving" : "pending"));
				sout << cgicc::td(toolbox::toString("%d", psp.rcvSocks_[j].length));
				sout << cgicc::td(toolbox::toString("%d", psp.rcvSocks_[j].rcvnBytes));
				sout << cgicc::td(toolbox::toString("%d", psp.rcvSocks_[j].freeQueue->elements()));
				sout << cgicc::td(toolbox::toString("%lu", (unsigned long) psp.rcvSocks_[j].counter));
				sout << cgicc::td(toolbox::toString("%lu", (unsigned long) psp.rcvSocks_[j].readCounter));
				sout << cgicc::td(toolbox::toString("%lu", (unsigned long) psp.rcvSocks_[j].idleEmptyQueueCounter));
				sout << cgicc::td(toolbox::toString("%lu", (unsigned long) psp.rcvSocks_[j].idleBlockingReadCounter));
				sout << cgicc::tr() << std::endl;

				toRecv += psp.rcvSocks_[j].length;
				recvd += psp.rcvSocks_[j].rcvnBytes;
				avaBuf += psp.rcvSocks_[j].freeQueue->elements();
				msgRecv += psp.rcvSocks_[j].counter;
				sockReads += psp.rcvSocks_[j].readCounter;
				idleEmpty += psp.rcvSocks_[j].idleEmptyQueueCounter;
				idleBlock += psp.rcvSocks_[j].idleBlockingReadCounter;
				rows++;
			}
		}

		if (initBody)
		{
			sout << cgicc::tbody() << std::endl;
		}

		sout << cgicc::tfoot();

		// Totals
		sout << cgicc::tr();
		std::stringstream totalString;
		totalString << "Total : " << rows;
		sout << cgicc::td(totalString.str()).set("colspan", "6");
		sout << cgicc::td(toolbox::toString("%d", toRecv));
		sout << cgicc::td(toolbox::toString("%d", recvd));
		sout << cgicc::td(toolbox::toString("%d", avaBuf));
		sout << cgicc::td(toolbox::toString("%d", msgRecv));
		sout << cgicc::td(toolbox::toString("%d", sockReads));
		sout << cgicc::td(toolbox::toString("%d", (unsigned long) idleEmpty));
		sout << cgicc::td(toolbox::toString("%d", (unsigned long) idleBlock));
		sout << cgicc::tr();

		// Info
		sout << cgicc::tr();
		sout << cgicc::td().set("colspan", "13");

		sout << "Idle evals: " << (toolbox::toString("%d", (unsigned long) psp.idleCounter_));
		sout << " | Mode: " << psp.getType();
		sout << " | Timeout: " << (toolbox::toString("%d", psp.rcvTimeout_)) << "ms";
		sout << " | Non-blocking: " << psp.listenAddress_->getProperty("nonblock");
		sout << " | Cycles per eval: " << psp.listenAddress_->getProperty("pollingCycle");
		sout << " | Multi-threaded: " << (psp.ia_->pspDispatcherThread_ ? "true" : "false");
		sout << " | Best effort: " << (psp.bestEffort_ ? "true" : "false");
		sout << " | recvBlockSize: " << recvBlockSize;

		sout << cgicc::td();
		sout << cgicc::tr() << std::endl;

		//psp.//mutex_.give();

		sout << cgicc::tfoot();
		sout << cgicc::table();

		return sout;
	}
}
