// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2012, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A.Petrucci, C.Wakefield					 *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _tcpla_exception_FailedToProcessDestroyEndPointCommand_h_
#define _tcpla_exception_FailedToProcessDestroyEndPointCommand_h_

#include "tcpla/exception/Exception.h"

namespace tcpla
{
	namespace exception
	{
		class FailedToProcessDestroyEndPointCommand: public tcpla::exception::Exception
		{
			public:
				FailedToProcessDestroyEndPointCommand ( std::string name, std::string message, std::string module, int line, std::string function ) : tcpla::exception::Exception(name, message, module, line, function)
				{
				}

				FailedToProcessDestroyEndPointCommand ( std::string name, std::string message, std::string module, int line, std::string function, xcept::Exception& e ) : tcpla::exception::Exception(name, message, module, line, function, e)
				{
				}
		};
	}
}

#endif
