// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2011, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J.Adamczewski(EE/GSI), S.Linev(EE/GSI), L.Orsini, A.Petrucci *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _tcpla_exception_ConnectionNotEstablished_h_
#define _tcpla_exception_ConnectionNotEstablished_h_

#include "tcpla/exception/Exception.h"

namespace tcpla
{
	namespace exception { 
		class ConnectionNotEstablished: public tcpla::exception::Exception
		{
			public: 
			ConnectionNotEstablished ( std::string name, std::string message, std::string module, int line, std::string function ):
					tcpla::exception::Exception(name, message, module, line, function)
			{} 
			
			ConnectionNotEstablished ( std::string name, std::string message, std::string module, int line, std::string function,
				xcept::Exception& e ): 
					tcpla::exception::Exception(name, message, module, line, function, e)
			{} 
		}; 
	} 
}

#endif
