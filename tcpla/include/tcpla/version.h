// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2012, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A.Petrucci, C.Wakefield, A.Forrest			 	 *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

//
// Version definition for UDAPL peer transport
//
#ifndef _tcpla_version_h_
#define _tcpla_version_h_

#include "config/PackageInfo.h"
// !!! Edit this line to reflect the latest package version !!!

#define TCPLA_VERSION_MAJOR 1
#define TCPLA_VERSION_MINOR 9
#define TCPLA_VERSION_PATCH 6
// If any previous versions available E.g. #define TCPLA_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#undef TCPLA_PREVIOUS_VERSIONS
#define TCPLA_PREVIOUS_VERSIONS "1.9.0,1.9.1,1.9.2,1.9.3,1.9.4,1.9.5"


//
// Template macros
//
#define TCPLA_VERSION_CODE PACKAGE_VERSION_CODE(TCPLA_VERSION_MAJOR,TCPLA_VERSION_MINOR,TCPLA_VERSION_PATCH)
#ifndef TCPLA_PREVIOUS_VERSIONS
#define TCPLA_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(TCPLA_VERSION_MAJOR,TCPLA_VERSION_MINOR,TCPLA_VERSION_PATCH)
#else 
#define TCPLA_FULL_VERSION_LIST  TCPLA_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(TCPLA_VERSION_MAJOR,TCPLA_VERSION_MINOR,TCPLA_VERSION_PATCH)
#endif 
namespace tcpla
{
    const std::string package  =  "tcpla";
    const std::string versions =  TCPLA_FULL_VERSION_LIST;
    const std::string summary = "TCP Layered Architecture";
    const std::string description = "TCP Layered Architecture based on Direct Access Protocol Layer";
    const std::string authors = "Luciano Orsini, Andrea Petrucci, Chris Wakefield, Andy Forrest";
    const std::string link = "http://xdaq.web.cern.ch";
    config::PackageInfo getPackageInfo();
    void checkPackageDependencies() throw (config::PackageInfo::VersionException);
    std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif

