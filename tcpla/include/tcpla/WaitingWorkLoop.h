// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2011, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A.Petrucci, C.Wakefield							 *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _tcpla_WaitingWorkLoop_h_
#define _tcpla_WaitingWorkLoop_h_

#include <iostream>
#include <string>

#include "tcpla/WaitingEventDispatcher.h"
#include "tcpla/WorkLoop.h"
#include "tcpla/exception/Exception.h"
#include "tcpla/exception/WorkLoopSubmitFailed.h"
#include "tcpla/exception/WorkerThreadFailed.h"
#include "tcpla/exception/UnknownException.h"
#include "toolbox/task/WaitingWorkLoop.h"

namespace tcpla
{

	class EventHandler;

	class WaitingWorkLoop : public tcpla::WorkLoop
	{
		public:

			WaitingWorkLoop (const std::string & name, tcpla::EventDispatcher* evd_handle, tcpla::EventHandler * event_handler, const std::string & affinity) throw (tcpla::exception::Exception);

			virtual ~WaitingWorkLoop ();

			bool process (toolbox::task::WorkLoop* wl);

		protected:

			tcpla::WaitingEventDispatcher* evd_handle_;
	};
}

#endif
