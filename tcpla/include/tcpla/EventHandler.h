// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2011, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A.Petrucci, C.Wakefield							 *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _tcpla_EventHandler_h_
#define _tcpla_EventHandler_h_

#include <iostream>
#include <string>
#include <list>

#include "tcpla/Event.h"

namespace tcpla
{
	class PeerTransport;
	class InterfaceAdapter;

	class EventHandler
	{
		public:

			virtual ~EventHandler ()
			{
			}

			virtual void handleEvent (tcpla::Event & e) = 0;

			virtual std::string toHTML () = 0;

			friend std::ostream& operator<< (std::ostream &sout, tcpla::EventHandler & eh)
			{
				sout << eh.toHTML();

				return sout;
			}
	};
}

#endif
