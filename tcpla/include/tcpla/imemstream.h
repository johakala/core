// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 		 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, C. Wakefield			 				 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         	 *
 *************************************************************************/

#ifndef _tcpla_imemstream_h_
#define _tcpla_imemstream_h_

#include <cstring>
#include "toolbox/mem/Reference.h"

namespace tcpla
{

	class imemstream
	{
		public:

			imemstream ();
			imemstream (char * base, size_t size);

			void init (char * base, size_t size);
			size_t readsome (char * buf, size_t len);
			size_t seekg (size_t len);
			size_t tellg ();
			bool eof ();
			bool empty ();

			char * base_;
			size_t size_;
			size_t offset_;

	};

}

#endif
