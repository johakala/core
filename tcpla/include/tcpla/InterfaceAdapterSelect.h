// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2011, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, A. Petrucci, C. Wakefield			 *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _tcpla_InterfaceAdapterSelect_h_
#define _tcpla_InterfaceAdapterSelect_h_

/* According to POSIX.1-2001 */
#include <sys/select.h>
/* According to earlier standards */
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>

#include "tcpla/InterfaceAdapter.h"

#include "tcpla/exception/InvalidFD.h"

namespace tcpla
{
	class PublicServicePoint;

	class InterfaceAdapterSelect : public tcpla::InterfaceAdapter
	{
			friend class PublicServicePoint;

		public:

			InterfaceAdapterSelect (xdaq::Application * parent, pt::Address::Reference address, tcpla::EventHandler * h, size_t maxClients, size_t ioQueueSize, size_t eventQueueSize, getHeaderSizeFunctionType ghf, getLengthFunctionType glf) throw (tcpla::exception::Exception);
			virtual ~InterfaceAdapterSelect ();
			std::string getType ();

			int append (int fd) throw (tcpla::exception::MaxConnections);
			void remove (int fd) throw (tcpla::exception::InvalidFD);

		private:
			void disableFD(int pollfdsKey);

			void connect (tcpla::InterfaceAdapter::Command & c) throw (tcpla::exception::MaxConnections, tcpla::exception::FailedToClearSendQueue, tcpla::exception::InvalidPollfdsKey, tcpla::exception::HostnameResolveFailed, tcpla::exception::CannotConnect);
			bool process (toolbox::task::WorkLoop* wl); //asynchronous sending thread
			void resetSendEntry (EndPoint & ep_handle) throw (tcpla::exception::UnitializedEndPoint, tcpla::exception::InvalidEndPoint);
			int getFD (size_t pollfdsKey);
			bool isReady (size_t pollfdsKey);

			int maxfd_;
			fd_set watchwset_;
			int * selectfds_;
			struct timeval timeout_;

		protected:

			fd_set wset_;
	};
}

#endif
