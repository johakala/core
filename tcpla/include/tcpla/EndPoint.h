// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2012, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A.Petrucci, C.Wakefield                            *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _tcpla_dat_EndPoint_h_
#define _tcpla_dat_EndPoint_h_

#include <iostream>
#include <string>

#include "tcpla/exception/Exception.h"
#include "tcpla/exception/InvalidEndPoint.h"
//#include "tcpla/InterfaceAdapter.h"
//#include "tcpla/PublicServicePoint.h"
//#include "tcpla/Event.h"
//#include "toolbox/BSem.h"
//#include "toolbox/rlist.h"

#include <uuid/uuid.h>

namespace tcpla
{
	class PublicServicePoint;
	class InterfaceAdapter;
	struct EndPoint
	{

			//public:
			/*
			 EndPoint(tcpla::InterfaceAdapter * ia)
			 throw (tcpla::exception::Exception);

			 virtual ~EndPoint();
			 */
			bool canPost (size_t total) throw (tcpla::exception::InvalidEndPoint);
			int getFD () throw (tcpla::exception::InvalidEndPoint);

			void clear ();
			/*
			 friend std::ostream &operator<<(std::ostream &cout, EndPoint  ep);
			 */
			//public:
			int pollfdsKey_;

			InterfaceAdapter * ia_;
			PublicServicePoint * psp_;

			uuid_t uuid_;

			// toolbox::net::UUID uuid_;

			int compare (const EndPoint &_obj) const;
			int operator== (const EndPoint &_obj) const;
			int operator!= (const EndPoint &_obj) const;
			bool isNull ();

			std::string toString (void);
			friend std::ostream& operator << (std::ostream& s, const tcpla::EndPoint& ep);
	};

}

#endif
