// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2011, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J.Adamczewski(EE/GSI), S.Linev(EE/GSI), L.Orsini, A.Petrucci *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _tcpla_Utils_h_
#define _tcpla_Utils_h_

#include <iostream>
#include <string>
#include <cstdlib>
#include <cstring>

#include "tcpla/exception/Exception.h"
#include "tcpla/exception/MalformedSubnet.h"
#include "tcpla/exception/CannotOpenInterfaceDevice.h"
#include "tcpla/exception/NoIPInSubnet.h"
#include "tcpla/exception/NoIPForInterface.h"

namespace tcpla
{
	std::string eventToString (int event);
	std::string getHostByInterface (const std::string & interface) throw (tcpla::exception::Exception, tcpla::exception::NoIPForInterface);
	std::string getHostBySubnet (const std::string & subnet) throw (tcpla::exception::MalformedSubnet, tcpla::exception::CannotOpenInterfaceDevice, tcpla::exception::NoIPInSubnet);
}

#endif
