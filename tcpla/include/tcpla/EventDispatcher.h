#ifndef _tcpla_EventDispatcher_h_
#define _tcpla_EventDispatcher_h_

#include <iostream>
#include <string>
#include "toolbox/squeue.h"
#include "tcpla/exception/Exception.h"
#include "tcpla/exception/EventQueueFull.h"
#include "toolbox/BSem.h"

#include "tcpla/Event.h"

namespace tcpla
{
	class WaitingWorkLoop;
	class InterfaceAdapter;
	class PublicServicePoint;

	class EventDispatcher
	{
			friend class WaitingWorkLoop;
			friend class InterfaceAdapter;
			friend class PublicServicePoint;

		public:
			virtual ~EventDispatcher ()
			{
			}
			virtual void push (tcpla::Event & e) throw (tcpla::exception::EventQueueFull) = 0;

		protected:

			std::map<std::string, size_t> eventCounter_;
	};
}
#endif
