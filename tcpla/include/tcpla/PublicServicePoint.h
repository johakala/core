// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2012, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A.Petrucci, C.Wakefield				 *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _tcpla_PublicServicePoint_h_
#define _tcpla_PublicServicePoint_h_

#include <iostream>
#include <string>
#include <map>

#include <fcntl.h>
#include "toolbox/rlist.h"

#include "toolbox/task/WorkLoop.h"
#include "toolbox/lang/Class.h"

#include "tcpla/exception/Exception.h"
#include "tcpla/exception/PollFailed.h"
#include "tcpla/exception/InvalidRcvTimeOut.h"
#include "tcpla/exception/FailedToSetNonBlocking.h"
#include "tcpla/exception/FailedToSeReUseAddress.h"
#include "tcpla/exception/NoFreePortInRange.h"
#include "tcpla/exception/CannotBind.h"
#include "tcpla/exception/FailedToAccept.h"
#include "tcpla/exception/PSPCommandQueueFull.h"
#include "tcpla/exception/FailedToGetFileControlSettings.h"
#include "tcpla/exception/FailedToClearReceiveBuffersQueue.h"
#include "tcpla/exception/ConnectResetByPeer.h"
#include "tcpla/exception/ConnectionClosedByPeer.h"
#include "tcpla/exception/EndPointNotConnected.h"
#include "tcpla/exception/FailedToResetReceiveEntry.h"
#include "tcpla/exception/FailedToReceive.h"
#include "tcpla/exception/FailedToProcessAcceptCommand.h"
#include "tcpla/exception/FailedToProcessResetReceiveEntryCommand.h"
#include "tcpla/exception/EventQueueFull.h"

#include "toolbox/BSem.h"

#include "tcpla/Event.h"
#include "tcpla/EndPoint.h"

#include "xdata/UnsignedInteger.h"

#include "pt/Address.h"
#include "toolbox/mem/Pool.h"

#include "tcpla/InterfaceAdapter.h"

//
// cgicc
//
#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"
#include "xgi/Utils.h"
#include "xgi/Input.h"
#include "xgi/Output.h"

namespace tcpla
{
	//class EndPoint;
	class InterfaceAdapter;

	class PublicServicePoint : public toolbox::lang::Class
	{
			friend class InterfaceAdapter;
			friend class InterfaceAdapterPoll;
			friend class InterfaceAdapterSelect;

			//friend class EndPoint;

		public:

			PublicServicePoint (tcpla::InterfaceAdapter * ia, pt::Address::Reference address, size_t ioQueueSize, size_t headerSize) throw (tcpla::exception::Exception, tcpla::exception::InvalidRcvTimeOut, tcpla::exception::NoFreePortInRange, tcpla::exception::CannotBind);

			virtual ~PublicServicePoint ();

			void accept (tcpla::EndPoint & ep_handle, int cr_handle, TCPLA_CONTEXT & cookie) throw (tcpla::exception::PSPCommandQueueFull);

			friend std::ostream& operator<< (std::ostream &sout, tcpla::PublicServicePoint & psp);

			tcpla::InterfaceAdapter * ia_;

			typedef struct receive_socket_entry
			{
					std::string hostname;
					unsigned short port;

					tcpla::EndPoint ep_handle;
					int pollfdsKey;

					bool connected; //status of the socket

					toolbox::mem::Pool * pool;
					toolbox::rlist<TCPLA_RECV_POST_DESC> * freeQueue; //free buffer queue

					//receive
					TCPLA_RECV_POST_DESC current;
					size_t rcvnBytes;
					size_t length;
					size_t counter;
					//TCPLA_MESSAGE_FRAME header;
					size_t idleBlockingReadCounter;
					size_t idleEmptyQueueCounter;
					size_t readCounter;

					//chain support
					uint16_t identification;
					uint8_t packet;
					uint8_t total;

			} TCPLA_RECEIVE_SOCKET_ENTRY;
			void safePostRecvBuffer (tcpla::EndPoint & ep_handle, TCPLA_CONTEXT & cookie, char * local_iov, size_t size) throw (tcpla::exception::PSPCommandQueueFull);
			void postRecvBuffer (tcpla::EndPoint & ep_handle, TCPLA_CONTEXT & cookie, char * local_iov, size_t size) throw (tcpla::exception::FailedToPostSend, tcpla::exception::InvalidEndPoint);
			std::string toString ();

			virtual int getFD (tcpla::EndPoint & ep_handle) throw (tcpla::exception::InvalidEndPoint) = 0;
			virtual std::string getType () = 0;

			size_t targetId_;
			size_t indexId_;

			void destroyEndPoint (tcpla::EndPoint & ep_handle) throw (tcpla::exception::PSPCommandQueueFull);

			tcpla::Address * getAddress();

		protected:
			void destroyEndPoint (tcpla::InterfaceAdapter::Command & c) throw (tcpla::exception::Exception);

			size_t receive (int socket, char * buf, size_t len) throw (tcpla::exception::ConnectResetByPeer, tcpla::exception::FailedToReceive, tcpla::exception::ConnectionClosedByPeer);
			//void resetRcvEntry( tcpla::EndPoint * ep_handle ) throw ( tcpla::exception::Exception );
			virtual bool process (toolbox::task::WorkLoop* wl) = 0;
			virtual int getFD (size_t pollfdsKey) = 0;
			virtual bool isReady (size_t pollfdsKey) = 0;
			virtual void disableFD (size_t pollfdsKey) = 0;
			virtual void accept (tcpla::InterfaceAdapter::Command & c)/* throw (tcpla::exception::Exception)*/ = 0;
			virtual void resetRcvEntry (tcpla::EndPoint & ep_handle) throw (tcpla::exception::FailedToResetReceiveEntry) = 0;

			void eval () throw (tcpla::exception::Exception);
			void processSocketEntries ();
			int acceptSocket (int fd, struct sockaddr_in * cliaddr) throw (tcpla::exception::FailedToAccept, tcpla::exception::FailedToGetFileControlSettings, tcpla::exception::FailedToSetNonBlocking);
			//virtual void accept () throw ( tcpla::exception::Exception ) = 0;
			void createListenSocket () throw (tcpla::exception::Exception, tcpla::exception::SocketOption, tcpla::exception::FailedToSeReUseAddress);

			toolbox::BSem mutex_;

			toolbox::rlist<tcpla::InterfaceAdapter::Command> * commandQueue_;

			toolbox::task::WorkLoop* workLoop_;
			toolbox::task::ActionSignature* process_;

			size_t idleCounter_;

			TCPLA_RECEIVE_SOCKET_ENTRY* rcvSocks_;
			size_t port_;

			int maxClients_;
			size_t headerSize_;

			int rcvTimeout_;

			std::map<std::string, std::string> affinity_;
			tcpla::Address * listenAddress_;
			int listenfd_;
			size_t totalClients_;
			int maxPollfdsKey_;

			getLengthFunctionType glf_;

			bool bestEffort_;

			tcpla::EventDispatcher * recv_evd_hdl_;
			tcpla::WorkLoop * receiverWorkLoop_;

			size_t underflowTimeout_;

	};

}

#endif
