// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2011, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A.Petrucci, C.Wakefield						     *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _tcpla_InterfaceAdapter_h_
#define _tcpla_InterfaceAdapter_h_

#include <string>
#include <list>
#include <iostream>

#include <sys/poll.h>
#include <fcntl.h>

#include "tcpla/exception/Exception.h"
#include "tcpla/exception/InvalidEndPoint.h"
#include "tcpla/exception/UnitializedEndPoint.h"
#include "tcpla/exception/SocketOption.h"
#include "tcpla/exception/HostnameResolveFailed.h"
#include "tcpla/exception/CannotBind.h"
#include "tcpla/exception/MaxConnections.h"
#include "tcpla/exception/CannotConnect.h"
#include "tcpla/exception/EndPointNotConnected.h"
#include "tcpla/exception/FailedToPostSend.h"
#include "tcpla/exception/PollFailed.h"
#include "tcpla/exception/FailedToClearSendQueue.h"
#include "tcpla/exception/InvalidPollfdsKey.h"
#include "tcpla/exception/InvalidFamily.h"
#include "tcpla/exception/FailedToSend.h"
#include "tcpla/exception/WorkerThreadFailed.h"
#include "tcpla/exception/UnknownException.h"
#include "tcpla/exception/IAShutdownFailed.h"
#include "tcpla/exception/IACommandQueueFull.h"
#include "tcpla/exception/WorkLoopSubmitFailed.h"
#include "tcpla/exception/FailedToCreatePSP.h"
#include "tcpla/exception/FailedToProcessConnectCommand.h"
#include "tcpla/exception/FailedToProcessDestroyEndPointCommand.h"
#include "tcpla/exception/InvalidSndTimeOut.h"
#include "tcpla/exception/SocketPollError.h"
#include "tcpla/exception/EventQueueFull.h"

//#include "tcpla/Event.h"
//#include "tcpla/EventHandler.h"
//#include "tcpla/Address.h"

//#include "tcpla/EventDispatcher.h"
//

#include "tcpla/WorkLoop.h"
#include "tcpla/Address.h"

#include "tcpla/HeaderInfo.h"

#include "toolbox/task/WorkLoopFactory.h"
#include "toolbox/task/WorkLoop.h"
#include "toolbox/task/Action.h"

#include "toolbox/BSem.h"

#include "toolbox/lang/Class.h"

#include "xdata/UnsignedInteger.h"

#include "xdaq/Object.h"

#include "toolbox/mem/MemoryPoolFactory.h"

#include <uuid/uuid.h>

namespace tcpla
{

	const int EMPTY_FD = -1;

	//class EndPoint;
	class PublicServicePoint;

	class InterfaceAdapter : public toolbox::lang::Class, public xdaq::Object
	{

			friend class PublicServicePoint;
			friend class PublicServicePointPoll;
			friend class PublicServicePointSelect;

		public:

			getHeaderSizeFunctionType ghf_;
			getLengthFunctionType glf_;

			InterfaceAdapter (xdaq::Application * parent, pt::Address::Reference address, tcpla::EventHandler * h, size_t maxClients, size_t ioQueueSize, size_t eventQueueSize, getHeaderSizeFunctionType ghf, getLengthFunctionType glf) throw (tcpla::exception::InvalidSndTimeOut, tcpla::exception::Exception);

			virtual ~InterfaceAdapter ();

			friend std::ostream& operator<< (std::ostream &sout, tcpla::InterfaceAdapter & ia);

			EndPoint createEndPoint ();
			EndPoint createEndPoint (TCPLA_CR_ARRIVAL_EVENT_DATA & cr);

			void destroyEndPoint (EndPoint & ep_handle) throw (tcpla::exception::IACommandQueueFull);

			tcpla::PublicServicePoint* listen (pt::Address::Reference address) throw (tcpla::exception::FailedToCreatePSP); //listen return endpoint from listening socket

			void connect (tcpla::EndPoint & ep, tcpla::Address * local, tcpla::Address * destination, TCPLA_CONTEXT & cookie) throw (tcpla::exception::InvalidFamily, tcpla::exception::HostnameResolveFailed, tcpla::exception::IACommandQueueFull);

			//chain support
			void postSendBuffer (tcpla::EndPoint & ep_handle, TCPLA_CONTEXT & cookie, char * local_iov, size_t size, toolbox::exception::HandlerSignature* handler, void* context, uint16_t identification, uint8_t flags) throw (tcpla::exception::EndPointNotConnected, tcpla::exception::FailedToPostSend, tcpla::exception::InvalidEndPoint);

			void postSendBuffer (tcpla::EndPoint & ep_handle, TCPLA_CONTEXT & cookie, struct iovec * local_iov, size_t iovs, toolbox::exception::HandlerSignature* handler, void* context, uint16_t identification, uint8_t flags) throw (tcpla::exception::FailedToPostSend, tcpla::exception::InvalidEndPoint);

			bool canPost (tcpla::EndPoint & ep_handle, size_t total);
			virtual std::string getType () = 0;
			void pushEvent (tcpla::EventDispatcher *, tcpla::Event & e);

			tcpla::EventDispatcher * recv_evd_hdl_;
			tcpla::EventDispatcher * reqt_evd_hdl_; //send or recv post
			tcpla::EventDispatcher * conn_evd_hdl_; //connection
			tcpla::EventDispatcher * creq_evd_hdl_; //request connection

			tcpla::WorkLoop * acceptorWorkLoop_;
			tcpla::WorkLoop * connectorWorkLoop_;
			tcpla::WorkLoop * receiverWorkLoop_;
			tcpla::WorkLoop * senderWorkLoop_;

			std::string hostname_;

			typedef struct tcpla_send_socket_entry
			{
					bool connected; //status of the socket
					bool failed;
					toolbox::rlist<TCPLA_SEND_POST_DESC> * postQueue;
					size_t written;
					size_t iov_index;

					int pollfdsKey;
					//std::string hostname;
					struct sockaddr_in ip_addr;
					size_t port;
					//tcpla::Address * local;
					//tcpla::Address * destination;
					tcpla::EndPoint ep_handle;
					TCPLA_SEND_POST_DESC current;
					size_t counter;
					size_t idleBlockingWriteCounter;
					size_t idleEmptyQueueCounter;
					size_t writeCounter;
					TCPLA_CONTEXT cookie;
					//struct iovec sendv[TCPLA_MAX_IOV_SIZE];
			} TCPLA_SEND_SOCKET_ENTRY;

			//Commands

			typedef enum tcpla_command_number
			{
				TCPLA_CONNECT_COMMAND_NUMBER = 0x00001,
				TCPLA_DESTROY_COMMAND_NUMBER = 0x00002,
				TCPLA_ACCEPT_COMMAND_NUMBER = 0x00003,
				TCPLA_RESET_COMMAND_NUMBER = 0x00004,
				TCPLA_POST_RECV_BUF_COMMAND_NUMBER = 0x00005
			} TCPLA_COMMAND_NUMBER;

			typedef struct tcpla_connect_command
			{
					tcpla::EndPoint ep_handle;
					//tcpla::Address * local;
					//tcpla::Address * destination;
					struct sockaddr_in lsockaddress;
					struct sockaddr_in dsockaddress;
					bool nonblock;
					int family;
					TCPLA_CONTEXT cookie;
			} TCPLA_CONNECT_COMMAND;

			typedef struct tcpla_destroy_command
			{
					tcpla::EndPoint ep_handle;
			} TCPLA_DESTROY_COMMAND;

			typedef struct tcpla_accept_command
			{
					tcpla::EndPoint ep_handle;
					int cr_handle;
					TCPLA_CONTEXT cookie;
			} TCPLA_ACCEPT_COMMAND;

			typedef struct post_recv_buf_command
			{
					tcpla::EndPoint ep_handle;
					TCPLA_CONTEXT cookie;
					char * local_iov;
					size_t size;
			} TCPLA_POST_RECV_BUF_COMMAND;

			typedef struct tcpla_reset_command
			{
					int pollfdsKey;
			} TCPLA_RESET_COMMAND;

			typedef union tcpla_command
			{
					TCPLA_CONNECT_COMMAND connect_command;
					TCPLA_DESTROY_COMMAND destroy_command;
					TCPLA_ACCEPT_COMMAND accept_command;
					TCPLA_RESET_COMMAND reset_command;
					TCPLA_POST_RECV_BUF_COMMAND post_recv_buf_command;
			} TCPLA_COMMAND;

			class Command
			{

				public:
					TCPLA_COMMAND_NUMBER command_number;
					TCPLA_COMMAND command;
			};

			std::string id_;

			bool pspDispatcherThread_;

			virtual int getFD (size_t pollfdsKey) = 0;

		protected:
			virtual void connect (tcpla::InterfaceAdapter::Command & c) throw (tcpla::exception::MaxConnections, tcpla::exception::FailedToClearSendQueue, tcpla::exception::InvalidPollfdsKey, tcpla::exception::HostnameResolveFailed, tcpla::exception::CannotConnect) = 0;
			virtual bool process (toolbox::task::WorkLoop* wl) = 0; //asynchronous sending thread
			virtual void resetSendEntry (EndPoint & ep_handle) throw (tcpla::exception::UnitializedEndPoint, tcpla::exception::InvalidEndPoint) = 0;

			virtual void disableFD (int fd) = 0;
			/* virtual int getFD ( tcpla::EndPoint * ep_handle ) throw ( tcpla::exception::InvalidEndPoint ) = 0; */

			virtual bool isReady (size_t pollfdsKey) = 0;

			void eval ();
			void processSocketEntries ();
			void reset (int key) throw (tcpla::exception::InvalidPollfdsKey);
			int sendNonBlocking (int socket, const char * buf, int len) throw (tcpla::exception::FailedToSend);
			void destroyEndPoint (Command & c) throw (tcpla::exception::Exception);
			void shutdown (tcpla::PublicServicePoint* psp) throw (tcpla::exception::IAShutdownFailed);
			bool isConnected (tcpla::EndPoint & ep);
			bool hasFailed (tcpla::EndPoint & ep);
			void clearPostQueue (size_t key);

			int createSocket (int family, bool nonblock, struct sockaddr_in & sockaddress) throw (tcpla::exception::Exception, tcpla::exception::SocketOption, tcpla::exception::CannotBind, tcpla::exception::HostnameResolveFailed);

			//tcpla::EndPoint duplicateEndPoint (tcpla::EndPoint & ep_handle);

			size_t ioQueueSize_;
			int maxClients_;
			int maxPollfdsKey_;
			size_t idleCounter_;
			size_t totalClients_;
			size_t targetTid_;
			size_t eventQueueSize_;

			bool singleThread_;

			size_t pollingCycle_;

			size_t connectionAttempts_;
			size_t connectionCompletetions_;

			std::map<std::string, std::string> affinity_;

			int sndTimeout_;

			std::list<tcpla::EndPoint> endpointRegistry_;
			//std::list<uuid_t> uuidRegistry_;

			// keep memory of all endpoint, removed at DTOR if any
			std::list<tcpla::PublicServicePoint*> pspRegistry_; //receiving

			TCPLA_SEND_SOCKET_ENTRY * sendSocks_;

			toolbox::task::ActionSignature* process_;
			toolbox::BSem mutex_;

			toolbox::rlist<Command> * commandQueue_;

			tcpla::EventHandler * eventHandler_;

			tcpla::Address * localAddress_;
	};
}

#endif
