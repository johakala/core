// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2011, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J.Adamczewski(EE/GSI), S.Linev(EE/GSI), L.Orsini, A.Petrucci *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _tcpla_dat_PollingWorkLoop_h_
#define _tcpla_dat_PollingWorkLoop_h_

#include <iostream>
#include <string>

#include "tcpla/EventHandler.h"
#include "tcpla/PollingEventDispatcher.h"
#include "tcpla/WorkLoop.h"

#include "tcpla/exception/Exception.h"
#include "tcpla/exception/WorkLoopSubmitFailed.h"
#include "tcpla/exception/WorkerThreadFailed.h"
#include "tcpla/exception/UnknownException.h"

namespace tcpla
{
	class PollingWorkLoop : public tcpla::WorkLoop
	{
		public:

			PollingWorkLoop (const std::string & name, tcpla::EventDispatcher* evd_handle, tcpla::EventHandler * event_handler, const std::string & affinity) throw (tcpla::exception::Exception);
			virtual ~PollingWorkLoop ();
			bool process (toolbox::task::WorkLoop* wl);

		protected:

			tcpla::PollingEventDispatcher* evd_handle_;
	};
}

#endif
