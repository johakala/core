// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2011, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A.Petrucci, C.Wakefield					 *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _PollingEventDispatcher_h_
#define _PollingEventDispatcher_h_

#include "tcpla/EventDispatcher.h"

namespace tcpla
{
	class PollingEventDispatcher : public EventDispatcher
	{
			friend class PollingWorkLoop;
			friend class InterfaceAdapter;
			friend class PublicServicePoint;

		public:
			PollingEventDispatcher (size_t queueSize, const std::string & name);
			void push (tcpla::Event & e) throw (tcpla::exception::EventQueueFull);
			virtual ~PollingEventDispatcher ();

		protected:

			toolbox::rlist<Event>* eventQueue_;
			toolbox::BSem mutex_;
	};
}
#endif
