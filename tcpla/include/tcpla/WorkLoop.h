// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2011, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A.Petrucci, C.Wakefield							 *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _tcpla_WorkLoop_h_
#define _tcpla_WorkLoop_h_

#include <iostream>
#include <string>

#include "tcpla/EventDispatcher.h"
#include "tcpla/exception/Exception.h"
#include "tcpla/exception/WorkLoopSubmitFailed.h"
#include "tcpla/exception/WorkerThreadFailed.h"
#include "tcpla/exception/UnknownException.h"
#include "toolbox/lang/Class.h"
#include "toolbox/task/WorkLoop.h"

namespace tcpla
{
	class EventHandler;

	class WorkLoop : public toolbox::lang::Class
	{
		public:

			WorkLoop (const std::string & name, tcpla::EventHandler * event_handler, const std::string & affinity) throw (tcpla::exception::Exception)
				: name_(name), event_handler_(event_handler), affinity_(affinity)
			{
			}

			virtual ~WorkLoop ()
			{
			}

			virtual bool process (toolbox::task::WorkLoop* wl) = 0;

		protected:

			toolbox::task::WorkLoop* workLoop_;
			toolbox::task::ActionSignature* process_;
			std::string name_;
			tcpla::EventHandler * event_handler_;
			std::string affinity_;
	};
}

#endif
