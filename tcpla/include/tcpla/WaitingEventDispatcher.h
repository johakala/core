// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2011, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A.Petrucci, C.Wakefield					 *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _WaitingEventDispatcher_h_
#define _WaitingEventDispatcher_h_

#include "tcpla/EventDispatcher.h"

namespace tcpla
{
	class WaitingEventDispatcher : public EventDispatcher
	{
			friend class WaitingWorkLoop;
			friend class InterfaceAdapter;
			friend class PublicServicePoint;

		public:

			WaitingEventDispatcher (size_t queueSize);
			virtual ~WaitingEventDispatcher ();
			void push (tcpla::Event & e) throw (tcpla::exception::EventQueueFull);

		protected:

			toolbox::squeue<Event> eventQueue_;
	};
}
#endif
