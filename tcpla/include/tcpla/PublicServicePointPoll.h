// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2012, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A.Petrucci, C.Wakefield				 *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _tcpla_PublicServicePointPoll_h_
#define _tcpla_PublicServicePointPoll_h_

#include "tcpla/PublicServicePoint.h"

#include <sys/poll.h>

namespace tcpla
{
	class InterfaceAdapter;
	//class EndPoint;

	class PublicServicePointPoll : public tcpla::PublicServicePoint
	{
			friend class InterfaceAdapter;
			//friend class EndPoint;

		public:

			PublicServicePointPoll (tcpla::InterfaceAdapter * ia, pt::Address::Reference address, size_t ioQueueSize, size_t headerSize) throw (tcpla::exception::Exception, tcpla::exception::InvalidRcvTimeOut, tcpla::exception::NoFreePortInRange, tcpla::exception::CannotBind);

			virtual ~PublicServicePointPoll ();

			int getFD (tcpla::EndPoint & ep_handle) throw (tcpla::exception::InvalidEndPoint);

			std::string getType ();

		private:
			int getFD (size_t pollfdsKey);
			bool isReady (size_t pollfdsKey);
			void disableFD (size_t pollfdsKey);

			void resetRcvEntry (tcpla::EndPoint & ep_handle) throw (tcpla::exception::FailedToResetReceiveEntry);
			void accept (tcpla::InterfaceAdapter::Command & c);// throw (tcpla::exception::Exception);

			bool process (toolbox::task::WorkLoop* wl);
			void accept () throw (tcpla::exception::Exception);

			struct pollfd* pollfds_;

	};
}

#endif
