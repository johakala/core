// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2011, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A.Petrucci, C.Wakefield						     *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _tcpla_InterfaceAdapterPoll_h_
#define _tcpla_InterfaceAdapterPoll_h_

#include <string>

#include <sys/poll.h>
#include <fcntl.h>

#include "tcpla/InterfaceAdapter.h"

namespace tcpla
{
	class InterfaceAdapterPoll : public tcpla::InterfaceAdapter
	{

		public:

			InterfaceAdapterPoll (xdaq::Application * parent, pt::Address::Reference address, tcpla::EventHandler * h, size_t maxClients, size_t ioQueueSize, size_t eventQueueSize, getHeaderSizeFunctionType ghf, getLengthFunctionType glf) throw (tcpla::exception::Exception);
			virtual ~InterfaceAdapterPoll ();
			std::string getType ();

		private:
			void disableFD(int pollfdsKey);
			void connect (tcpla::InterfaceAdapter::Command & c) throw (tcpla::exception::MaxConnections, tcpla::exception::FailedToClearSendQueue, tcpla::exception::InvalidPollfdsKey, tcpla::exception::HostnameResolveFailed, tcpla::exception::CannotConnect);
			bool process (toolbox::task::WorkLoop* wl); //asynchronous sending thread
			void resetSendEntry (EndPoint & ep_handle) throw (tcpla::exception::UnitializedEndPoint, tcpla::exception::InvalidEndPoint);
			/* int getFD ( tcpla::EndPoint * ep_handle ) throw ( tcpla::exception::InvalidEndPoint ); */
			int getFD (size_t pollfdsKey);
			bool isReady (size_t pollfdsKey);

			struct pollfd* pollfds_;
	};
}

#endif
