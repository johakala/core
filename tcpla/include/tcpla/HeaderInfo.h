// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2012, CERN.			                 	       *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A.Petrucci, C.Wakefield				 	 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			             *
 *************************************************************************/

#ifndef _tcpla_header_info_h_
#define _tcpla_header_info_h_

#include "i2o/i2o.h"
#include <cstddef>

namespace tcpla
{

	typedef size_t (*getHeaderSizeFunctionType) ();
	typedef size_t (*getLengthFunctionType) (char * bufferPtr);

	class HeaderInfo
	{
		public:

			virtual size_t getLength (char * bufferPtr) = 0;
			virtual size_t getHeaderSize () = 0;
			virtual ~HeaderInfo ()
			{
			}
			;
	};
}

#endif
