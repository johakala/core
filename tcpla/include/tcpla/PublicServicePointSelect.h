// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2012, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A.Petrucci, C.Wakefield				 *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _tcpla_PublicServicePointSelect_h_
#define _tcpla_PublicServicePointSelect_h_

#include "tcpla/PublicServicePoint.h"

namespace tcpla
{
	class InterfaceAdapter;
	//class EndPoint;

	class PublicServicePointSelect : public tcpla::PublicServicePoint
	{
			friend class InterfaceAdapter;
			//friend class EndPoint;

		public:

			PublicServicePointSelect (tcpla::InterfaceAdapter * ia, pt::Address::Reference address, size_t ioQueueSize, size_t headerSize) throw (tcpla::exception::Exception, tcpla::exception::InvalidRcvTimeOut, tcpla::exception::NoFreePortInRange, tcpla::exception::CannotBind);

			virtual ~PublicServicePointSelect ();

			int getFD (tcpla::EndPoint & ep_handle) throw (tcpla::exception::InvalidEndPoint);

			std::string getType ();

		private:
			void resetRcvEntry (tcpla::EndPoint & ep_handle) throw (tcpla::exception::FailedToResetReceiveEntry);
			bool process (toolbox::task::WorkLoop* wl);
			void accept (tcpla::InterfaceAdapter::Command & c);// throw (tcpla::exception::Exception);
			void accept () throw (tcpla::exception::Exception);
			int getFD (size_t pollfdsKey);
			bool isReady (size_t pollfdsKey);
			void disableFD (size_t pollfdsKey);

			int maxfd_;
			fd_set rset_;
			fd_set watchrset_;
			int * selectfds_;
			struct timeval timeout_;

	};
}

#endif
