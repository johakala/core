// $Id: HyperDAQ.cc,v 1.3 2008/07/18 15:26:33 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini and G. Lo Presti			 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "b2in/eventing/HyperDAQ.h"
#include "xgi/framework/Method.h"

b2in::eventing::HyperDAQ::HyperDAQ(xdaq::Application* application, b2in::eventing::Statistics* statistics, std::map<std::string, std::list<Subscription*> > & subscriptions,
	b2in::eventing::Diagnostics* diagnostics)
	: xgi::framework::UIManager(application), application_(application), statistics_(statistics), subscriptions_(subscriptions), diagnostics_(diagnostics)
{
   	xgi::framework::deferredbind(application, this, &b2in::eventing::HyperDAQ::mainPage, "Default");
   	xgi::framework::deferredbind(application, this,  &b2in::eventing::HyperDAQ::diagnostics, "diagnostics");
}

b2in::eventing::HyperDAQ::~HyperDAQ()
{

}

void b2in::eventing::HyperDAQ::mainPage
(
	xgi::Input * in, 
	xgi::Output * out
) throw (xgi::exception::Exception)
{
	// Begin of tabs
        *out << "<div class=\"xdaq-tab-wrapper\">" << std::endl;
  
  	// Tab 1      
        *out << "<div class=\"xdaq-tab\" title=\"Statistics\">" << std::endl;
        this->StatisticsTabPage(out);
        *out << "</div>";
        
        // Tab 2
        *out << "<div class=\"xdaq-tab\"  title=\"Subscriptions\">" << std::endl;
        this->SubscriptionTabPage(out);
        *out << "</div>";
        
        // Tab 3
        *out << "<div class=\"xdaq-tab\"  title=\"Diagnostics\">" << std::endl;
        this->DiagnosticsTabPage(out);
        *out << "</div>";

        *out << "</div>"; // end of tab pane
}

void b2in::eventing::HyperDAQ::diagnostics(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception)
{
        try
        {
                cgicc::Cgicc cgi(in);

                std::string enable = xgi::Utils::getFormElement(cgi, "enable")->getValue();
                if (enable == "true")
                {
                        diagnostics_->enable();
                }
                else if(enable == "false")
                {
                        diagnostics_->disable();
                }
        }
        catch (const std::exception & e)
        {
                XCEPT_RAISE(xgi::exception::Exception, e.what());
        }
}


void b2in::eventing::HyperDAQ::StatisticsTabPage
(
	xgi::Output * out
) 
throw (xgi::exception::Exception)
{
	if (statistics_) statistics_->safeUpdateStats();

	// -- BEGIN PAGE CONTENT	
	//
	*out << cgicc::table().set("class","xdaq-table-vertical");
	*out << cgicc::tbody() << std::endl;

	// Number of messages received
	// 
	*out << cgicc::tr();
	*out << cgicc::th();
	*out << "Messages received";
	*out << cgicc::th();
	*out << cgicc::td();
	if (statistics_) {
		*out << statistics_->getTotalMessageCount();
	}
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;
	
	// Number of messages lost
	// 
	*out << cgicc::tr(); 
	*out << cgicc::th();
	*out << "Messages lost";
	*out << cgicc::th();
	*out << cgicc::td();
	if (statistics_) {
		*out << statistics_->getTotalLostMessageCount();
	}
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	// Last message received
	// 
	*out << cgicc::tr(); 
	*out << cgicc::th();
	*out << "Last received";
	*out << cgicc::th();
	*out << cgicc::td();
	if (statistics_)
	{
		*out << statistics_->getLastMsgReceived().toString(toolbox::TimeVal::loc);
	}
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	// Rate
	//
	*out << cgicc::tr(); 
	*out << cgicc::th();
	*out << "Rate (Hz)";
	*out << cgicc::th();
	*out << cgicc::td();
	if (statistics_)
	{
		*out << statistics_->getRate();
	}
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	// Average time to process message
	*out << cgicc::tr(); 
	*out << cgicc::th();
	*out << "Average time to process";
	*out << cgicc::th();
	*out << cgicc::td();
	if (statistics_)
	{
		*out << statistics_->getMessageServiceTime();
	}
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	// Average time to process message
	*out << cgicc::tr();
	*out << cgicc::th();
	*out << "Total broken connections";
	*out << cgicc::th();
	*out << cgicc::td();
	if (statistics_)
		{
	*out << statistics_->getBrokenConnections();
		}
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	*out << cgicc::tbody() << std::endl;
	*out << cgicc::table() << std::endl; 
	//
	// -- END PAGE CONTENT

}

void b2in::eventing::HyperDAQ::SubscriptionTabPage (xgi::Output * out) throw (xgi::exception::Exception)
{
	*out << cgicc::table().set("class","xdaq-table").set("style","width: 100%;") << std::endl;

	*out << cgicc::thead() << std::endl;
	*out << cgicc::tr() << std::endl;
	*out << cgicc::th("Topic").set("class", "xdaq-sortable") << std::endl;
	*out << cgicc::th("URL") << std::endl;
	*out << cgicc::th("Expires") << std::endl;
	*out << cgicc::th("Originator") << std::endl;
	*out << cgicc::th("LID") << std::endl;
	*out << cgicc::th("Service") << std::endl;
	*out << cgicc::th("Context") << std::endl;
	*out << cgicc::tr() << std::endl;
	*out << cgicc::thead() << std::endl;
	
	*out << cgicc::tbody() << std::endl;
	for ( std::map<std::string, std::list<Subscription*> >::iterator i = subscriptions_.begin(); i != subscriptions_.end(); i++ )
	{
		// Output property enabled, check box with uuid of event as value
		// All checked input fields are transmitted upon form submissen, therefore
		// upon receive of the form, all events must be set "false", only the received ones
		// are set to "true"
		//
		std::stringstream rows;
		rows <<  (*i).second.size();
		
		*out << cgicc::tr();

		*out << cgicc::td((*i).first).set("rowspan", rows.str()) << std::endl;
	
		for ( std::list<Subscription*>::iterator j = (*i).second.begin(); j !=  (*i).second.end(); j++ )
		{
			if (j != (*i).second.begin())
			{
				*out << cgicc::tr();
			}
			*out << cgicc::td((*j)->url) << std::endl;
			*out << cgicc::td((*j)->expires.toString(toolbox::TimeVal::gmt)) << std::endl;

			if ((*j)->originator != "")
			{
				std::stringstream link;
				link << "<a href=\"" << (*j)->originator << "\">" << (*j)->originator << "</a>";
				*out << cgicc::td(link.str()) << std::endl;
			}
			else
			{
				*out << cgicc::td("Not provided") << std::endl;
			}
			*out << cgicc::td((*j)->lid) << std::endl;
			*out << cgicc::td((*j)->service) << std::endl;
			*out << cgicc::td((*j)->context) << std::endl;

			//if (j != (*i).second.end())
			//{
					*out << cgicc::tr() << std::endl;
			//}
		}
	}
	
	*out << cgicc::tbody() << std::endl;	
	*out << cgicc::table() << std::endl;

}

void b2in::eventing::HyperDAQ::DiagnosticsTabPage
(
	xgi::Output * out
)
throw (xgi::exception::Exception)
{
	if(diagnostics_ == 0)
	{
		return;
	}

	diagnostics_->writeTo(out);
}

