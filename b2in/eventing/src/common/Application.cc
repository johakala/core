// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini and G. Lo Presti			 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/
#include <stdlib.h>

#include "xdaq/ApplicationGroup.h"
#include "xdaq/ApplicationDescriptor.h"
#include "xdaq/ApplicationRegistry.h"
#include "xdaq/Application.h"
#include "xdaq/ApplicationStub.h"
#include "xdaq/exception/Exception.h"
#include "xdaq/NamespaceURI.h"
#include "xdaq/Network.h"
#include "xdaq/NetGroup.h"
#include "xdaq/EndpointAvailableEvent.h"

#include "toolbox/Event.h"
#include "toolbox/string.h"
#include "toolbox/net/URL.h"
#include "toolbox/task/TimerTask.h"
#include "toolbox/task/Timer.h"
#include "toolbox/task/TimerFactory.h"
#include "toolbox/TimeInterval.h"


#include "xdata/exdr/AutoSizeOutputStreamBuffer.h"
#include "xdata/exdr/FixedSizeInputStreamBuffer.h"
#include "xdata/exdr/Serializer.h"
#include "xdata/Boolean.h"
#include "xdata/TimeVal.h"
#include "xdata/Float.h"
#include "xdata/UnsignedLong.h"
#include "xdata/UnsignedShort.h"
#include "xdata/UnsignedInteger.h"
#include "xdata/UnsignedInteger64.h"
#include "xdata/UnsignedInteger32.h"
#include "xdata/String.h"
#include "xdata/Double.h"
#include "xdata/Vector.h"
#include "xdata/InfoSpaceFactory.h"

#include "xcept/tools.h"
#include "pt/PeerTransportAgent.h"
#include "pt/tcp/Address.h"
#include "b2in/eventing/Application.h"
////
/*
 * Author:  David Robert Nadeau
 * Site:    http://NadeauSoftware.com/
 * License: Creative Commons Attribution 3.0 Unported License
 *          http://creativecommons.org/licenses/by/3.0/deed.en_US
 */

#if defined(_WIN32)
#include <windows.h>
#include <psapi.h>

#elif defined(__unix__) || defined(__unix) || defined(unix) || (defined(__APPLE__) && defined(__MACH__))
#include <unistd.h>
#include <sys/resource.h>

#if defined(__APPLE__) && defined(__MACH__)
#include <mach/mach.h>

#elif (defined(_AIX) || defined(__TOS__AIX__)) || (defined(__sun__) || defined(__sun) || defined(sun) && (defined(__SVR4) || defined(__svr4__)))
#include <fcntl.h>
#include <procfs.h>

#elif defined(__linux__) || defined(__linux) || defined(linux) || defined(__gnu_linux__)
#include <stdio.h>

#endif

#else
#error "Cannot define getPeakRSS( ) or getCurrentRSS( ) for an unknown OS."
#endif





/**
 * Returns the peak (maximum so far) resident set size (physical
 * memory use) measured in bytes, or zero if the value cannot be
 * determined on this OS.
 */
size_t getPeakRSS( )
{
#if defined(_WIN32)
	/* Windows -------------------------------------------------- */
	PROCESS_MEMORY_COUNTERS info;
	GetProcessMemoryInfo( GetCurrentProcess( ), &info, sizeof(info) );
	return (size_t)info.PeakWorkingSetSize;

#elif (defined(_AIX) || defined(__TOS__AIX__)) || (defined(__sun__) || defined(__sun) || defined(sun) && (defined(__SVR4) || defined(__svr4__)))
	/* AIX and Solaris ------------------------------------------ */
	struct psinfo psinfo;
	int fd = -1;
	if ( (fd = open( "/proc/self/psinfo", O_RDONLY )) == -1 )
		return (size_t)0L;		/* Can't open? */
	if ( read( fd, &psinfo, sizeof(psinfo) ) != sizeof(psinfo) )
	{
		close( fd );
		return (size_t)0L;		/* Can't read? */
	}
	close( fd );
	return (size_t)(psinfo.pr_rssize * 1024L);

#elif defined(__unix__) || defined(__unix) || defined(unix) || (defined(__APPLE__) && defined(__MACH__))
	/* BSD, Linux, and OSX -------------------------------------- */
	struct rusage rusage;
	getrusage( RUSAGE_SELF, &rusage );
#if defined(__APPLE__) && defined(__MACH__)
	return (size_t)rusage.ru_maxrss;
#else
	return (size_t)(rusage.ru_maxrss * 1024L);
#endif

#else
	/* Unknown OS ----------------------------------------------- */
	return (size_t)0L;			/* Unsupported. */
#endif
}





/**
 * Returns the current resident set size (physical memory use) measured
 * in bytes, or zero if the value cannot be determined on this OS.
 */
size_t getCurrentRSS( )
{
#if defined(_WIN32)
	/* Windows -------------------------------------------------- */
	PROCESS_MEMORY_COUNTERS info;
	GetProcessMemoryInfo( GetCurrentProcess( ), &info, sizeof(info) );
	return (size_t)info.WorkingSetSize;

#elif defined(__APPLE__) && defined(__MACH__)
	/* OSX ------------------------------------------------------ */
	struct mach_task_basic_info info;
	mach_msg_type_number_t infoCount = MACH_TASK_BASIC_INFO_COUNT;
	if ( task_info( mach_task_self( ), MACH_TASK_BASIC_INFO,
			(task_info_t)&info, &infoCount ) != KERN_SUCCESS )
		return (size_t)0L;		/* Can't access? */
	return (size_t)info.resident_size;

#elif defined(__linux__) || defined(__linux) || defined(linux) || defined(__gnu_linux__)
	/* Linux ---------------------------------------------------- */
	long rss = 0L;
	FILE* fp = NULL;
	if ( (fp = fopen( "/proc/self/statm", "r" )) == NULL )
		return (size_t)0L;		/* Can't open? */
	if ( fscanf( fp, "%*s%ld", &rss ) != 1 )
	{
		fclose( fp );
		return (size_t)0L;		/* Can't read? */
	}
	fclose( fp );
	return (size_t)rss * (size_t)sysconf( _SC_PAGESIZE);

#else
	/* AIX, BSD, Solaris, and Unknown OS ------------------------ */
	return (size_t)0L;			/* Unsupported. */
#endif
}
///

XDAQ_INSTANTIATOR_IMPL(b2in::eventing::Application)

b2in::eventing::Application::Application(xdaq::ApplicationStub * s)
throw (xdaq::exception::Exception)
        :
        xdaq::Application(s), mutex_(toolbox::BSem::FULL),statistics_(0), hyperdaq_(0),messengerCache_(0), communicationLoss_(0)
{

	s->getDescriptor()->setAttribute("icon", "/b2in/eventing/images/WSEventing_64x64.png");
    s->getDescriptor()->setAttribute("icon16x16", "/b2in/eventing/images/WSEventing_16x16.png");


	b2in::nub::bind(this, &b2in::eventing::Application::onMessage );

	watchdogDelay_ = "PT30S";
	this->getApplicationInfoSpace()->fireItemAvailable("watchdogDelay",&watchdogDelay_);


	getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");
	getApplicationContext()->addActionListener(this); // attach to endpoint available events
}

b2in::eventing::Application::~Application()
{
}

void b2in::eventing::Application::asynchronousExceptionNotification(xcept::Exception& e)
{
	// ignore loss of messages
	communicationLoss_++;
	if (statistics_) statistics_->incBrokenConnections();
	LOG4CPLUS_DEBUG (this->getApplicationLogger(), "b2in::eventing::Application::asynchronousExceptionNotification : " << e.what());
}
void b2in::eventing::Application::actionPerformed( xdata::Event& event)
{
	if (event.type() == "urn:xdaq-event:setDefaultValues")
	{
		toolbox::task::Timer * timer = 0;
		// Create timer for refreshing subscriptions
		if (!toolbox::task::getTimerFactory()->hasTimer("urn:b2in:eventing"))
		{
			timer = toolbox::task::getTimerFactory()->createTimer("urn:b2in:eventing");
		}
		else
		{
			timer = toolbox::task::getTimerFactory()->getTimer("urn:b2in:eventing");
		}

		toolbox::TimeInterval watchdogDelayInterval; // period of 2 second for expired topic watchdog updates
		try
		{
			watchdogDelayInterval.fromString(watchdogDelay_);
		}
		catch(toolbox::exception::Exception & e )
		{
			this->notifyQualified("error",e);
		}

		toolbox::TimeVal start = toolbox::TimeVal::gettimeofday();
		timer->scheduleAtFixedRate( start, this, watchdogDelayInterval,  0, "b2in-eventing-watchdog" );

	}

}

void b2in::eventing::Application::actionPerformed(toolbox::Event& e) 
{
	LOG4CPLUS_DEBUG (this->getApplicationLogger(), "Received event " << e.type());
	
	if ( e.type() == "xdaq::EndpointAvailableEvent" )
	{
		// If the available endpoint is a b2in endpoint we are ready to
		// discover other b2in endpoints on the network
		//
		std::string networkName = this->getApplicationDescriptor()->getAttribute("network");
		
		xdaq::EndpointAvailableEvent& ie = dynamic_cast<xdaq::EndpointAvailableEvent&>(e);
		// xdaq::Endpoint* endpoint = ie.getEndpoint();
		const xdaq::Network* network = ie.getNetwork();
		
		LOG4CPLUS_DEBUG (this->getApplicationLogger(), "Received endpoint available event for network " << network->getName());

		if (network->getName() == networkName)
		{		
			try
			{
				messengerCache_ = new b2in::utils::MessengerCache(this->getApplicationContext(), 
				  this->getApplicationDescriptor()->getAttribute("network"), this);
			}
			catch (b2in::utils::exception::Exception& e)
			{
				this->notifyQualified("error",e);
				return;
			}    
			
			statistics_ = new b2in::eventing::Statistics(this, network->getAddress(this->getApplicationContext()->getContextDescriptor())->toString());
			hyperdaq_ = new b2in::eventing::HyperDAQ(this, statistics_, subscriptions_, &diagnostics_);

		}
	}
	
}


void b2in::eventing::Application::onMessage (toolbox::mem::Reference *  msg, xdata::Properties & plist) 
	throw (b2in::nub::exception::Exception)
{
	toolbox::TimeVal timestamp = toolbox::TimeVal::gettimeofday();
	
	//std::cout << "on message" << std::endl;
	std::string action = plist.getProperty("urn:b2in-eventing:action");
	
	if ( action == "subscribe")
	{
		std::string id = plist.getProperty("urn:b2in-eventing:id");
		
		if ( id != "") 
		{
			try
			{
				this->subscribe(plist);
			}
			catch ( b2in::nub::exception::Exception & e )
			{
				XCEPT_DECLARE_NESTED (b2in::nub::exception::Exception, ex , "failed to subscribe",e);
				this->notifyQualified("error",ex);
			}
			if ( msg != 0 ) msg->release();
		}	
		else
		{
			if ( msg != 0 ) msg->release();
			XCEPT_DECLARE (b2in::nub::exception::Exception, e , "subscribe failed (missing id)");
			this->notifyQualified("error",e);
		}
		if (statistics_) statistics_->addServiceTime( timestamp);
	}
	else if (action == "notify" ) 
	{
		size_t len = 0; // count an empty notify message as 1 byte
		if (msg != 0) len = msg->getDataSize();

		try
		{		
			// output socket is driven by subscription and shpuld never use the close option.
			// This overrides  eventual clients that have requested a close connection 
			plist.setProperty("urn:b2in-protocol-tcp:connection","keep-alive");
			// notify releases chain in msg
			this->notify(msg,plist);

			if ( statistics_ ) statistics_->addIncomingPacket (
					timestamp,
					len,
					plist.getProperty("urn:xmas-flashlist:originator")
				);
		}
		catch ( b2in::nub::exception::Exception & e )
		{
			if (statistics_) statistics_->addLostPacket(len);
			if ( msg != 0 ) msg->release();
			XCEPT_DECLARE_NESTED (b2in::nub::exception::Exception, ex , "failed to notify",e);
			this->notifyQualified("error",ex);
		}
	}
	else
	{
		if ( msg != 0 ) msg->release();
		XCEPT_DECLARE (b2in::nub::exception::Exception, e , "unknown action "+ action);
		this->notifyQualified("error",e);
		if (statistics_) statistics_->addServiceTime( timestamp);
	}
	// simple perfromance meauserment
	/*
	static size_t counter = 0;
	static toolbox::TimeVal start_ = toolbox::TimeVal::gettimeofday();
	
	if ((counter % 10000) == 9999)
	{
		toolbox::TimeVal stop = toolbox::TimeVal::gettimeofday();
		std::cout << "Time: " << (double)(stop - start_) << ", rate: " << 10000/((double)(stop - start_)) << std::endl;
		start_ = toolbox::TimeVal::gettimeofday();
	}
	counter++;
	*/
}

void b2in::eventing::Application::subscribe(xdata::Properties & plist)
	throw (b2in::nub::exception::Exception)
{
	if ( messengerCache_ == 0 ) 
	{
		XCEPT_RAISE(b2in::nub::exception::Exception, "cannot perform subscription, messenger cache not created");
	}	

	mutex_.take();

	std::string topic = plist.getProperty("urn:b2in-eventing:topic");
	std::string uuid = plist.getProperty("urn:b2in-eventing:id");
	std::map<std::string, std::list<Subscription*> >::iterator i = subscriptions_.find(topic);
	
	Subscription  * subscription = 0;
	if ( i != subscriptions_.end() )
	{
		for (std::list<Subscription*>::iterator j = (*i).second.begin(); j != (*i).second.end(); j++)
		{
			if ((*j)->uuid == uuid)
			{
				//std::cout << " already existing UUID , it is a renew" << std::endl;
				toolbox::TimeInterval interval;
				try
				{
					interval.fromString(plist.getProperty("urn:b2in-eventing:expires"));
				}
				catch(toolbox::exception::Exception & e )
				{
					mutex_.give();
					XCEPT_RETHROW (b2in::nub::exception::Exception, "Failed to process expires attribute", e);
				}
				(*j)->expires = toolbox::TimeVal::gettimeofday() + interval;

				//std::cout << "check that messenger is valid, if not re-created it , as ticket https://svnweb.cern.ch/trac/cmsos/ticket/764" << std::endl;
				if ( ! messengerCache_->hasMessenger((*j)->url)  )
				{
					try
					{
						//std::cout << "messenger did not exists then create it upon renew"  << std::endl;
						messengerCache_->createMessenger((*j)->url);
					}
					catch (b2in::utils::exception::Exception  & e )
					{
						mutex_.give();
						XCEPT_RETHROW (b2in::nub::exception::Exception, "Failed to refresh subscription (attempt to re-create messenger failed) for " + subscription->url, e);
					}
				}
				
				mutex_.give();
				return;
			}
		}
		
		//std::cout << " New Subscription topic " << topic <<  " from " <<  plist.getProperty("urn:b2in-eventing:subscriberurl") << std::endl;

		LOG4CPLUS_DEBUG (this->getApplicationLogger(),  " New Subscription for existing topic " << topic <<  " from " <<  plist.getProperty("urn:b2in-eventing:subscriberurl"));

		
		subscription = new Subscription();
		subscription->url =  plist.getProperty("urn:b2in-eventing:subscriberurl");
		subscription->lid =  plist.getProperty("urn:b2in-eventing:subscriberlid");
		subscription->service =  plist.getProperty("urn:b2in-eventing:subscriberservice");
		subscription->context =  plist.getProperty("urn:b2in-eventing:context");
		subscription->originator =  plist.getProperty("urn:b2in-eventing:originator");
		subscription->uuid =  uuid;
		toolbox::TimeInterval interval;
		try
		{
			interval.fromString(plist.getProperty("urn:b2in-eventing:expires"));
		}
		catch(toolbox::exception::Exception & e )
		{
			delete subscription;
			mutex_.give();
			XCEPT_RETHROW (b2in::nub::exception::Exception, "Failed to process expires attribute", e);
		}
		subscription->expires = toolbox::TimeVal::gettimeofday() + interval;
		subscription->topic = topic; // redundant for time expiration handling
		

		if ( ! messengerCache_->hasMessenger(subscription->url)  )
		{
				
			try
			{
				//std::cout << " creating for this subscriber a messenger for topic " << topic << std::endl;
				messengerCache_->createMessenger(subscription->url);
			}
			catch (b2in::utils::exception::Exception	& e )
			{
				delete subscription;
				mutex_.give();
				XCEPT_RETHROW (b2in::nub::exception::Exception, "Failed to subscribe for " + subscription->url, e);
			}
		}
		
		(*i).second.push_back(subscription);


	}
	else
	{
		
		//std::cout << " New Topic  " <<  topic << std::endl;
		LOG4CPLUS_DEBUG (this->getApplicationLogger(),  " New Subscription for topic " << topic <<  " from " <<  plist.getProperty("urn:b2in-eventing:subscriberurl"));

		subscription = new Subscription();
		subscription->url = plist.getProperty("urn:b2in-eventing:subscriberurl");
		subscription->lid = plist.getProperty("urn:b2in-eventing:subscriberlid");
		subscription->service = plist.getProperty("urn:b2in-eventing:subscriberservice");
		subscription->context =  plist.getProperty("urn:b2in-eventing:context");
		subscription->originator =  plist.getProperty("urn:b2in-eventing:originator");
		subscription->uuid = uuid;
		toolbox::TimeInterval interval;
		try
		{
			interval.fromString(plist.getProperty("urn:b2in-eventing:expires"));
		}
		catch(toolbox::exception::Exception & e )
		{
			delete subscription;
			mutex_.give();
			XCEPT_RETHROW (b2in::nub::exception::Exception, "Failed to process expires attribute", e);
		}
		
		subscription->expires = toolbox::TimeVal::gettimeofday() + interval;
		subscription->topic = topic; // redundant for time expiration handling
		
		
		//std::cout << " check messenger  already exists for topic  " <<  topic << std::endl;
 		if ( ! messengerCache_->hasMessenger(subscription->url)  )
                {
			try
			{
				//std::cout << " Need to create  Messenger  for " <<  topic << std::endl;
				messengerCache_->createMessenger(subscription->url);
			}
			catch (b2in::utils::exception::Exception	& e )
			{
				delete subscription;
				mutex_.give();
				XCEPT_RETHROW (b2in::nub::exception::Exception, "Failed to create messenger upon subscription from url " + subscription->url, e);
			}
		}
		
		//std::cout << " set topic  " <<  topic << std::endl;
		subscriptions_[topic].push_back(subscription);		
	}


	mutex_.give();
}

void b2in::eventing::Application::notify(toolbox::mem::Reference *  msg, xdata::Properties & plist) throw (b2in::nub::exception::Exception)
{
	if ( messengerCache_ == 0 ) 
	{
		XCEPT_RAISE (b2in::nub::exception::Exception, "cannot perfrom notification, messenger cache not created");
	}	
	
	mutex_.take();
	
	std::string originator = plist.getProperty("urn:b2in-protocol:originator");
	std::string topic = plist.getProperty("urn:b2in-eventing:topic");
	std::map<std::string, std::list<Subscription*> >::iterator i = subscriptions_.find(topic);
	
	 diagnostics_.incrementIncoming(topic, originator);

	if ( i != subscriptions_.end() )
	{
		for ( std::list<Subscription*>::iterator j = (*i).second.begin(); j != (*i).second.end(); j++ )
		{
			// override service, with subscriber service
			if ((*j)->lid != "")
			{
				plist.setProperty("urn:b2in-protocol:lid", (*j)->lid );
			}
			else
			{
				plist.erase("urn:b2in-protocol:lid");
			}

			if ((*j)->service != "")
			{
				plist.setProperty("urn:b2in-protocol:service", (*j)->service ); // is the subscriber url
			}
			else
			{
				plist.erase("urn:b2in-protocol:service");
			}

			plist.setProperty("urn:b2in-eventing:id", (*j)->uuid );
			plist.setProperty("urn:b2in-eventing:context", (*j)->context );
			plist.setProperty("urn:b2in-protocol-tcp:connection-timeout", "5");
			
			//std::cout <<   "send to " << (*j)->service << " on " << (*j)->url << std::endl;

			if ( ! messengerCache_->hasMessenger((*j)->url) )
			{
				diagnostics_.incrementInternalLoss(topic, originator);	
				// ignore this notification, notification lost
				continue;
			}
			else
			{
				toolbox::mem::Reference* duplicated = 0;
				if ( msg != 0 ) 
				{ 
					duplicated = msg->duplicate(); 
					if ( duplicated == 0 )
					{
						diagnostics_.incrementMemoryLoss(topic, originator); 
						mutex_.give();
						std::stringstream errmsg;
						errmsg << "failed to allocate reference for sending to " << (*j)->url << " for topic " << topic;
						XCEPT_RAISE (b2in::nub::exception::Exception, errmsg.str());
					}
				}
				try
				{
					diagnostics_.incrementOutgoing(topic, originator);	
					messengerCache_->send((*j)->url,duplicated,plist);
				}
				catch (b2in::nub::exception::InternalError & e)
				{
					diagnostics_.incrementInternalLoss(topic, originator);	
					if ( duplicated != 0 ) duplicated->release();

					std::stringstream errmsg;
					errmsg << "failed to notify to " << (*j)->url << " for topic " << topic << ", invalidated URL";

					try
					{
						messengerCache_->invalidate((*j)->url);
					}
					catch (b2in::utils::exception::Exception & utilsex )
					{
						// ignore
						errmsg << " (messenger already invalidated)";
					}

					communicationLoss_++;
					if (statistics_) statistics_->incBrokenConnections();

					mutex_.give();
					XCEPT_RETHROW (b2in::nub::exception::Exception, errmsg.str(), e);

				}
				catch ( b2in::nub::exception::QueueFull & e )
				{
					diagnostics_.incrementEnqueuingLoss(topic, originator);	
					if ( duplicated != 0 ) duplicated->release();
					mutex_.give();
					std::stringstream errmsg;
					errmsg << "failed to notify to " << (*j)->url << " for topic " << topic;
					XCEPT_RETHROW (b2in::nub::exception::Exception, errmsg.str(), e);

				}
				catch ( b2in::nub::exception::OverThreshold & e)
				{
					if ( duplicated != 0 ) duplicated->release();
					size_t len = 1; // count an empty notify message as 1 byte
					if (msg != 0) len = msg->getDataSize();
					if (statistics_) statistics_->addLostPacket(len);
					diagnostics_.incrementEnqueuingLoss(topic, originator);	
				}
				
			}
		}
	}
	
	if ( msg != 0 ) msg->release();
	mutex_.give();
}

void b2in::eventing::Application::timeExpired(toolbox::task::TimerEvent& e)
{
	/// DEBUG
	static size_t currentSize = getCurrentRSS( );
	//static size_t peakSize    = getPeakRSS( );
	static size_t counting = 0;


	size_t currentSizeNow = getCurrentRSS( );

	if ( currentSizeNow > currentSize )
	{
		LOG4CPLUS_TRACE (this->getApplicationLogger(),  "New size :" << currentSizeNow <<  " delta is: " << currentSizeNow - currentSize << " on: " << counting);
		currentSize = currentSizeNow;
	}
	LOG4CPLUS_TRACE(this->getApplicationLogger(), "Processed  :" << counting << " rss peak:" << getPeakRSS( ) << " rss current size:" << getCurrentRSS( ));

	counting++;

	// DEBUG



	LOG4CPLUS_DEBUG (this->getApplicationLogger(), "Watchdog for expired topics");

	mutex_.take();
	// loop over all topics
	std::map<std::string, std::list<Subscription*> >::iterator i = subscriptions_.begin();
	while( i != subscriptions_.end())
	{
		std::string topic = (*i).first;

		std::list<Subscription*> & subscriptions = (*i).second;

		// loop over all subscriptions for the topic
		std::list<Subscription*>::iterator j = subscriptions.begin();
		while (j != subscriptions.end() )
		{
			Subscription * subscription =  (*j);
			toolbox::TimeVal now = toolbox::TimeVal::gettimeofday();
			if ( now >= subscription->expires )
			{
				LOG4CPLUS_DEBUG (this->getApplicationLogger(), "Timeout for topic " << (*i).first << " on url " << subscription->url << " time " << subscription->expires.toString(toolbox::TimeVal::gmt));
				LOG4CPLUS_DEBUG (this->getApplicationLogger(), "Delete for " << subscription->url << ", time: " << subscription->expires.toString(toolbox::TimeVal::gmt));

				LOG4CPLUS_DEBUG (this->getApplicationLogger(), "trigger a check connection message to see if the messenger has still a valid connection on " << subscription->url);
				toolbox::task::TimerEvent e(0,0);
				messengerCache_->timeExpired(e);
				messengerCache_->timeExpired(e);


				delete subscription;
				// Use iterator.
				// Note the post increment.
				// Increments the iterator but returns the
				// original value for use by erase
				subscriptions.erase(j++); // remove and get next iterator

			}
			else
			{
				++j;
			}

		}
		// it is the last subscription for this topic, therefore remove topic
		if (subscriptions.empty())
		{
			LOG4CPLUS_DEBUG (this->getApplicationLogger(), "Clear topic " << (*i).first);
			// Use iterator.
			// Note the post increment.
		    // Increments the iterator but returns the
			// original value for use by erase
			subscriptions_.erase(i++); // if there are no more subscriptions for a topic, also remove the topic

		}
		else
		{
			++i;
		}
	}

	mutex_.give();

}
