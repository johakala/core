<?xml version='1.0'?>
<!-- Order of specification will determine the sequence of installation. all modules are loaded prior instantiation of plugins -->
<xp:Profile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/" xmlns:xp="http://xdaq.web.cern.ch/xdaq/xsd/2005/XMLProfile-11">
	<!-- Compulsory  Plugins -->
	<xp:Application class="executive::Application" id="0" group="profile" service="executive" network="local">
		<properties xmlns="urn:xdaq-application:Executive" xsi:type="soapenc:Struct">
			<logUrl xsi:type="xsd:string">console</logUrl>
                	<logLevel xsi:type="xsd:string">INFO</logLevel>
                </properties>
	</xp:Application>
	<xp:Module>${XDAQ_ROOT}/lib/libb2innub.so</xp:Module>
	<xp:Module>${XDAQ_ROOT}/lib/libexecutive.so</xp:Module>

	<!-- XPlore requires the installation of Power Pack  -->
        <xp:Application class="xplore::Application" id="9"  network="local">
                <properties xmlns="urn:xdaq-application:xplore::Application" xsi:type="soapenc:Struct">
                        <settings xsi:type="xsd:string">${XDAQ_SETUP_ROOT}/${XDAQ_ZONE}/xplore/shortcuts.xml</settings>
                        <republishInterval xsi:type="xsd:string">60</republishInterval>
                </properties>
        </xp:Application>       
        <xp:Module>/lib/libslp.so</xp:Module>
        <xp:Module>${XDAQ_ROOT}/lib/libxslp.so</xp:Module>
        <xp:Module>${XDAQ_ROOT}/lib/libxploreutils.so</xp:Module>
        <xp:Module>${XDAQ_ROOT}/lib/libxplore.so</xp:Module>

 	<xp:Application class="pt::tcp::PeerTransportTCP" id="20" network="local">
                        <properties xmlns="urn:xdaq-application:PeerTransportTCP" xsi:type="soapenc:Struct">
                                <autoSize xsi:type="xsd:boolean">true</autoSize>
                                <maxPacketSize xsi:type="xsd:unsignedInt">4096</maxPacketSize>
                        </properties>
         </xp:Application>
          <xp:Module>${XDAQ_ROOT}/lib/libpttcp.so</xp:Module>

	<xp:Endpoint protocol="tcp" service="b2in" interface="eth0" port="1900"  maxport="1950" autoscan="true" network="xmas" publish="true"/>

	<xp:Application class="pt::http::PeerTransportHTTP" id="1" group="profile" network="local">
		 <properties xmlns="urn:xdaq-application:pt::http::PeerTransportHTTP" xsi:type="soapenc:Struct">
		 	<documentRoot xsi:type="xsd:string">${XDAQ_DOCUMENT_ROOT}</documentRoot>
                        <aliasName xsi:type="xsd:string">tmp</aliasName>
                        <aliasPath xsi:type="xsd:string">/tmp</aliasPath>
                </properties>
	</xp:Application>
	<xp:Module>${XDAQ_ROOT}/lib/libpthttp.so</xp:Module>

	<xp:Application class="pt::fifo::PeerTransportFifo" id="8" group="profile" network="local"/>
	<xp:Module>${XDAQ_ROOT}/lib/libptfifo.so</xp:Module>
	
	<!-- HyperDAQ -->
	<xp:Application class="hyperdaq::Application" id="3" group="profile" service="hyperdaq" network="local"/>
	<xp:Module>${XDAQ_ROOT}/lib/libhyperdaq.so</xp:Module>	
	
	<xp:Application class="b2in::eventing::Application" id="400" network="xmas" group="eventing" service="b2in-eventing" publish="true">
	 	<properties xmlns="urn:xdaq-application:b2in::eventing::Application" xsi:type="soapenc:Struct">
		</properties>
	</xp:Application>
	<xp:Module>${XDAQ_ROOT}/lib/libb2inutils.so</xp:Module>
	<xp:Module>${XDAQ_ROOT}/lib/libb2ineventing.so</xp:Module>
</xp:Profile>
