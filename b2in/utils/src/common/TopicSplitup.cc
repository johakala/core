// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "b2in/utils/TopicSplitup.h"
#include "b2in/utils/ConnectEvent.h"

#include "xcept/tools.h"
#include "toolbox/stl.h"
#include "toolbox/string.h"
#include "toolbox/net/URL.h"
#include "toolbox/exception/Handler.h"
#include "toolbox/exception/Processor.h"
#include "toolbox/task/exception/InvalidListener.h"
#include "toolbox/task/exception/NotActive.h"
#include "toolbox/task/exception/InvalidSubmission.h"
#include "toolbox/mem/Pool.h"
#include "toolbox/mem/Reference.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "toolbox/mem/CommittedHeapAllocator.h"
#include "toolbox/Event.h"
#include "toolbox/task/Timer.h"
#include "toolbox/task/TimerFactory.h"


b2in::utils::TopicSplitup::TopicSplitup(xdaq::Application * owner, const std::string & brokerURL, const std::string & networkName, toolbox::TimeInterval interval, std::list<std::string> & names)
	throw (b2in::utils::exception::Exception)
	:  xdaq::Object(owner),	topicMutex_(toolbox::BSem::FULL), nameList_(names), brokerURL_(brokerURL)
{
	// Initialize message counters
	brokerCounter_     = 0;
	brokerLostCounter_ = 0;

	try
	{
		brokerMessengerCache_ = new b2in::utils::MessengerCache( this->getOwnerApplication()->getApplicationContext(), networkName, this);
	}
	catch (b2in::utils::exception::Exception& e)
	{
		std::stringstream msg;
		msg << "Failed to create b2in messenger cache on network '";
		msg << this->getOwnerApplication()->getApplicationDescriptor()->getAttribute("network") << "'";
		LOG4CPLUS_FATAL(this->getOwnerApplication()->getApplicationLogger(), msg.str());
	}

	std::string name = "b2in-utils-topicsplitup-"+owner->getApplicationDescriptor()->getAttribute("service");	
        toolbox::task::getTimerFactory()->createTimer(name);
        toolbox::task::Timer * timer = toolbox::task::getTimerFactory()->getTimer(name);

        // submit task for dialing
        try
        {
		toolbox::TimeVal start;
        	start = toolbox::TimeVal::gettimeofday();
        	timer->scheduleAtFixedRate( start, this, interval, 0, "broker-staging" );
        }
        catch (toolbox::task::exception::InvalidListener& e)
        {
		XCEPT_RETHROW(b2in::utils::exception::Exception, "failed to schedule broker-staging in topicsplitup timer ", e );
        }
        catch (toolbox::task::exception::InvalidSubmission& e)
        {
		XCEPT_RETHROW(b2in::utils::exception::Exception, "failed to schedule broker-staging in topicsplitup timer ", e );
        }
        catch (toolbox::task::exception::NotActive& e)
        {
		XCEPT_RETHROW(b2in::utils::exception::Exception, "failed to schedule broker-staging in topicsplitup timer ", e );
        }
        catch (toolbox::exception::Exception& e)
        {
		XCEPT_RETHROW(b2in::utils::exception::Exception, "failed to schedule broker-staging in topicsplitup timer ", e );
        }
}

b2in::utils::TopicSplitup::~TopicSplitup()
{
}


void b2in::utils::TopicSplitup::timeExpired(toolbox::task::TimerEvent& e)
{
 	LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), "broker-staging time elapsed");

	std::string name = e.getTimerTask()->name;

	if ( name == "broker-staging" )
	{
		if( ! this->isSynchronized())
		{
			this->requesting();
		}
	}
}

void b2in::utils::TopicSplitup::onMessage (toolbox::mem::Reference *  msg, xdata::Properties & plist)
{
	std::string action = plist.getProperty("urn:xmasbroker2g:action");
	std::string model = plist.getProperty("urn:xmasbroker2g:model");
	if ( action == "allocate" && model == "topic")
	{
		// Extract all flashlist topics and set the topic name in the topic map ( empty topic means no publish )
		//
		topicMutex_.take();
		topicMap_.clear();
		for(std::list<std::string>::iterator iter = nameList_.begin() ; iter != nameList_.end() ; iter++)
		{
			std::string & name = (*iter);
			if ( plist.find(name) != plist.end() ) // has flashlist name
			{
				std::string topic = plist.getProperty(name);
				LOG4CPLUS_DEBUG (this->getOwnerApplication()->getApplicationLogger(), "Attributing topic " << topic << " to flashlist " << name );
				topicMap_[name] = topic;
			}
		}
		topicMutex_.give();
        }
}


// Finite State Machine
void b2in::utils::TopicSplitup::requesting ()
{
 	LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), "requesting name->topic mapping: " << toolbox::printTokenList(nameList_, ","));
	if (brokerURL_ == "")
	{
		// broker-less configuration, topic equals name
		topicMutex_.take();
		for(std::list<std::string>::iterator iter = nameList_.begin() ; iter != nameList_.end() ; iter++)
		{
			std::string & name = (*iter);
			topicMap_[name] = name;
		}
		topicMutex_.give();
		return;
	}

	// if it has a messenger already this is a re-dial
	if ( ! brokerMessengerCache_->hasMessenger(brokerURL_) )
	{
		try
		{
			brokerMessengerCache_->createMessenger(brokerURL_); // trigger  caching of messenger for the remote endpoint
		}
		catch (b2in::utils::exception::Exception& e)
		{
			LOG4CPLUS_FATAL(this->getOwnerApplication()->getApplicationLogger(), "Failed to create messenger on to broker" << brokerURL_ << ", " << e.what());
			return;
		}
	}

	xdata::Properties plist;
		
	plist.setProperty ("urn:b2in-protocol:service", "xmasbroker2g");
	plist.setProperty ("urn:b2in-protocol-tcp:connection", "close");

	plist.setProperty ("urn:xmasbroker2g:action", "query");
	plist.setProperty ("urn:xmasbroker2g:url", brokerMessengerCache_->getLocalAddress()->toString());
	plist.setProperty ("urn:xmasbroker2g:model", "topic");
	plist.setProperty ("urn:xmasbroker2g:service", "b2in-eventing");
	plist.setProperty ("urn:xmasbroker2g:originator", this->getOwnerApplication()->getApplicationDescriptor()->getAttribute("service"));
		
	// Tell the broker all the flashlists we can report
	//
	topicMutex_.take();
	std::stringstream flashlists;
	for(std::list<std::string>::iterator iter = nameList_.begin() ; iter != nameList_.end() ; iter++)
	{
		std::string name = (*iter);
		flashlists << name;
		if (iter != nameList_.end())
		{
			flashlists << ",";
		}
		std::map<std::string,std::string>::iterator found = topicMap_.find(name);
		if(found != topicMap_.end())
		{
			plist.setProperty((*found).first, (*found).second);
		}
	}
	topicMutex_.give();

	LOG4CPLUS_DEBUG (this->getOwnerApplication()->getApplicationLogger(), "Sending list of flashlists to broker: " << flashlists.str());
	plist.setProperty ("urn:xmasbroker2g:flashlists", flashlists.str());

	try
	{
		brokerCounter_++;
		brokerMessengerCache_->send(brokerURL_,0,plist);
	}
	catch (b2in::nub::exception::InternalError & e)
	{
		LOG4CPLUS_FATAL(this->getOwnerApplication()->getApplicationLogger(), "failed to send query to broker on url " << brokerURL_ << ", " << xcept::stdformat_exception_history(e));
		brokerLostCounter_++;
	}
	catch ( b2in::nub::exception::QueueFull & e )
	{
		// ignore
		brokerLostCounter_++;
		LOG4CPLUS_WARN(this->getOwnerApplication()->getApplicationLogger(), "failed to send query to broker on url " << brokerURL_ << ", " << e.what());
	}
	catch ( b2in::nub::exception::OverThreshold & e)
	{
		// ignore 
		brokerLostCounter_++;
		LOG4CPLUS_WARN(this->getOwnerApplication()->getApplicationLogger(), "failed to send query to broker on url " << brokerURL_ << ", " << e.what());
	}
}

std::string b2in::utils::TopicSplitup::getBrokerURL()
{
	return brokerURL_;
}

xdata::UnsignedInteger64T b2in::utils::TopicSplitup::getBrokerCounter()
{
	return (brokerCounter_-brokerLostCounter_);
}

xdata::UnsignedInteger64T b2in::utils::TopicSplitup::getBrokerLostCounter()
{
	return brokerLostCounter_;
}

std::string b2in::utils::TopicSplitup::getStateName()
{
	if(this->isSynchronized())
	{
		return "assigned";
	}
	else
	{
		return "requesting";
	}
}

bool b2in::utils::TopicSplitup::isSynchronized()
{
	topicMutex_.take();
	for(std::list<std::string>::iterator iter = nameList_.begin() ; iter != nameList_.end() ; iter++)
	{
		std::string & name = (*iter);
		if ( topicMap_.find(name) == topicMap_.end() ) // does not have flashlist name
		{
			topicMutex_.give();
			return false;
		}
	}
	topicMutex_.give();
	return true;
}

void b2in::utils::TopicSplitup::update(std::list<std::string> & names)
{
	topicMutex_.take();
	nameList_ = names;
	topicMutex_.give();
}

std::list<std::string> b2in::utils::TopicSplitup::getFlashLists()
{
	topicMutex_.take();
	std::list<std::string> ret = nameList_;
	topicMutex_.give();
	return ret;
}

std::string b2in::utils::TopicSplitup::getTopic(std::string & name)
{
	std::string topic;

	topicMutex_.take();
	std::map<std::string, std::string>::iterator found = topicMap_.find(name);
	if (found != topicMap_.end())
	{
		topic = (*found).second;
	}
	else
	{
		topic = "";
	}
	topicMutex_.give();

	return topic;
}

void b2in::utils::TopicSplitup::asynchronousExceptionNotification(xcept::Exception& ex)
{
        LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), "Sending a notification failed, " << ex.what());
        brokerLostCounter_++;
}

