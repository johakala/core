// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2017, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber, L. Orsini and D. Simelevicius                   *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include "b2in/utils/EventingDialup.h"
#include "b2in/utils/ConnectEvent.h"

#include "xcept/tools.h"
#include "toolbox/stl.h"
#include "toolbox/string.h"
#include "toolbox/net/URL.h"
#include "toolbox/exception/Handler.h"
#include "toolbox/exception/Processor.h"
#include "toolbox/task/exception/InvalidListener.h"
#include "toolbox/task/exception/NotActive.h"
#include "toolbox/task/exception/InvalidSubmission.h"
#include "toolbox/mem/Pool.h"
#include "toolbox/mem/Reference.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "toolbox/mem/CommittedHeapAllocator.h"
#include "toolbox/Event.h"
#include "toolbox/task/Timer.h"
#include "toolbox/task/TimerFactory.h"
#include "toolbox/fsm/FailedEvent.h"

#include "pt/exception/SendFailure.h"


b2in::utils::EventingDialup::EventingDialup(xdaq::Application * owner, const std::string & publishGroup, const std::string & brokerURL,
	const std::string & eventingURL, const std::string & networkName, toolbox::TimeInterval interval)
	throw (b2in::utils::exception::Exception)
	: xdaq::Object(owner), brokerAsynchronousExceptionNotification_(*this), eventingAsynchronousExceptionNotification_(*this),
	brokerURL_(brokerURL), publishGroup_(publishGroup), eventingURL_(eventingURL), fsm_("urn:b2in-utils-eventingdialup:workloop-"+owner->getApplicationDescriptor()->getAttribute("service"))
{
	// Initialize message counters
	outgoingReportCounter_     = 0;
	outgoingReportLostCounter_ = 0;
	brokerCounter_             = 0;
	brokerLostCounter_         = 0;

	try
	{
		brokerMessengerCache_ = new b2in::utils::MessengerCache(this->getOwnerApplication()->getApplicationContext(), networkName, &brokerAsynchronousExceptionNotification_);
	}
	catch (b2in::utils::exception::Exception& e)
	{
		std::stringstream msg;
		msg << "Failed to create b2in messenger cache on network '";
		msg << this->getOwnerApplication()->getApplicationDescriptor()->getAttribute("network") << "'";
		XCEPT_RETHROW(b2in::utils::exception::Exception, msg.str(), e);
	}
	
	try
	{
		eventingMessengerCache_ = new b2in::utils::MessengerCache(this->getOwnerApplication()->getApplicationContext(), networkName, &eventingAsynchronousExceptionNotification_);
	}
	catch (b2in::utils::exception::Exception& e)
	{
		std::stringstream msg;
		msg << "Failed to create b2in messenger cache on network '";
		msg << this->getOwnerApplication()->getApplicationDescriptor()->getAttribute("network") << "'";
		XCEPT_RETHROW(b2in::utils::exception::Exception, msg.str(), e);
	}

	fsm_.addState('I', "idle");
	fsm_.addState('N', "negotiating", this, &b2in::utils::EventingDialup::negociating);
	fsm_.addState('C', "connecting", this, &b2in::utils::EventingDialup::connecting);
	fsm_.addState('E', "established");

	fsm_.addStateTransition('I', 'N', "dial");
	fsm_.addStateTransition('N', 'N', "dial");
	fsm_.addStateTransition('N', 'C', "connect",  this, &b2in::utils::EventingDialup::setEventingURL);
	fsm_.addStateTransition('C', 'E', "establish");

	// re-dial
	fsm_.addStateTransition('I', 'I', "hangup", this, &b2in::utils::EventingDialup::hangup);
	fsm_.addStateTransition('N', 'I', "hangup", this, &b2in::utils::EventingDialup::hangup);
	fsm_.addStateTransition('C', 'I', "hangup", this, &b2in::utils::EventingDialup::hangup);
	fsm_.addStateTransition('E', 'I', "hangup", this, &b2in::utils::EventingDialup::hangup);

	// failed
	fsm_.setFailedStateTransitionAction(this, &b2in::utils::EventingDialup::failAction);

	fsm_.setInitialState('I');
	fsm_.reset();

	toolbox::task::Timer * timer = 0;
	std::string name = "b2in-utils-eventingdialup-" + owner->getApplicationDescriptor()->getAttribute("service");
	try
	{
		timer = toolbox::task::getTimerFactory()->createTimer(name);
	}
	catch(toolbox::task::exception::Exception & ex)
	{
		std::stringstream msg;
		msg << "failed to create timer name:" << name;
		XCEPT_RETHROW(b2in::utils::exception::Exception, msg.str(), ex);
		
	}
	// submit task for dialing
	// toolbox::TimeInterval interval;
	try
	{
		toolbox::TimeVal start;
		start = toolbox::TimeVal::gettimeofday();
		//interval.fromString("PT1S");
		timer->scheduleAtFixedRate( start, this, interval, 0, "broker-staging");
	}
	catch (toolbox::task::exception::InvalidListener& e)
	{
		XCEPT_RETHROW(b2in::utils::exception::Exception, "failed to schedule broker-staging in eventingdialup timer ", e);
	}
	catch (toolbox::task::exception::InvalidSubmission& e)
	{
		XCEPT_RETHROW(b2in::utils::exception::Exception, "failed to schedule broker-staging in eventingdialup timer ", e);
	}
	catch (toolbox::task::exception::NotActive& e)
	{
		XCEPT_RETHROW(b2in::utils::exception::Exception, "failed to schedule broker-staging in eventingdialup timer ", e);
	}
	catch (toolbox::exception::Exception& e)
	{
		XCEPT_RETHROW(b2in::utils::exception::Exception, "failed to schedule broker-staging in eventingdialup timer ", e);
	}

}

b2in::utils::EventingDialup::~EventingDialup()
{
}

void b2in::utils::EventingDialup::failAction(toolbox::Event::Reference e)
throw (toolbox::fsm::exception::Exception)
{
	toolbox::fsm::FailedEvent & event = dynamic_cast<toolbox::fsm::FailedEvent&>(*e);

	// Do nothing
	LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), "dial up has failed on event " << xcept::stdformat_exception_history(event.getException()));
}

void b2in::utils::EventingDialup::timeExpired(toolbox::task::TimerEvent& e)
{
	LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), "broker-staging time elapsed");

	std::string name = e.getTimerTask()->name;

	if ( name == "broker-staging" )
	{
		if ( fsm_.getCurrentState() == 'F' )
		{
			LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), "dial up has failed , try full reset " );
			fsm_.reset();
			return;
		}


		toolbox::Event::Reference e(new toolbox::Event("dial", this));
		try
		{

			fsm_.fireEvent(e);
		}
		catch(toolbox::fsm::exception::Exception & e )
		{
			std::cout << " ---- >>>> very bad " << e.what() << std::endl;
		}
	}
}

void b2in::utils::EventingDialup::negociate(const std::string & url)
{
	LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), "request connecting b2in eventing");

	if ( url == "" )
	{
		// empty string is ignored, since broker could not allocate
		return;
	}
	// negotiate a b2in-evening address
	toolbox::Event::Reference e(new b2in::utils::ConnectEvent(url, this));
	fsm_.fireEvent(e);
}

void b2in::utils::EventingDialup::onMessage (toolbox::mem::Reference * msg, xdata::Properties & plist)
{
	std::string action = plist.getProperty("urn:xmasbroker2g:action");
	std::string model = plist.getProperty("urn:xmasbroker2g:model");
	if ( action == "allocate" && model == "eventing")
	{
		std::string url = plist.getProperty("urn:xmasbroker2g:result0");

		LOG4CPLUS_DEBUG (this->getOwnerApplication()->getApplicationLogger(), "Broker attributed b2in-eventing at " << url);

		negociate(url);
	}
}


// Finite State Machine
void b2in::utils::EventingDialup::negociating (toolbox::fsm::FiniteStateMachine& fsm) throw (toolbox::fsm::exception::Exception)
{

	LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), "negotiating b2in eventing");
	if (brokerURL_ == "")
	{
		// broker-less configuration, eventingURL contains the eventing to connect to
		this->negociate(eventingURL_);
		return;
	}

	// if it has a messenger already this is a re-dial
	if ( ! brokerMessengerCache_->hasMessenger(brokerURL_) )
	{
		try
		{
			brokerMessengerCache_->createMessenger(brokerURL_); // trigger  caching of messenger for the remote endpoint
		}
		catch (b2in::utils::exception::Exception& e)
		{
			LOG4CPLUS_FATAL(this->getOwnerApplication()->getApplicationLogger(), "Failed to create messenger on to broker" << brokerURL_ << ", " << e.what());
			toolbox::Event::Reference evt(new toolbox::Event("hangup", this));
			fsm_.fireEvent(evt);
			return;
		}
	}

	xdata::Properties plist;

	plist.setProperty ("urn:b2in-protocol:service", "xmasbroker2g");
	plist.setProperty ("urn:b2in-protocol-tcp:connection", "close");

	plist.setProperty ("urn:xmasbroker2g:action", "query");
	plist.setProperty ("urn:xmasbroker2g:group", publishGroup_);
	plist.setProperty ("urn:xmasbroker2g:url", brokerMessengerCache_->getLocalAddress()->toString());
	plist.setProperty ("urn:xmasbroker2g:model", "eventing");
	plist.setProperty ("urn:xmasbroker2g:service", "b2in-eventing");
	plist.setProperty ("urn:xmasbroker2g:originator", this->getOwnerApplication()->getApplicationDescriptor()->getAttribute("service"));

	try
	{
		brokerCounter_++;
		brokerMessengerCache_->send(brokerURL_,0,plist);
	}
	catch (b2in::nub::exception::InternalError & e)
	{
		LOG4CPLUS_FATAL(this->getOwnerApplication()->getApplicationLogger(), "failed to send query to broker on url " << brokerURL_ << ", " << e.what());
		brokerLostCounter_++;
		toolbox::Event::Reference evt(new toolbox::Event("hangup", this));
		fsm_.fireEvent(evt);
	}
	catch ( b2in::nub::exception::QueueFull & e )
	{
		// ignore
		brokerLostCounter_++;
		LOG4CPLUS_WARN(this->getOwnerApplication()->getApplicationLogger(), "failed to send query to broker on url " << brokerURL_ << ", " << e.what());
	}
	catch ( b2in::nub::exception::OverThreshold & e)
	{
		// ignore 
		brokerLostCounter_++;
		LOG4CPLUS_WARN(this->getOwnerApplication()->getApplicationLogger(), "failed to send query to broker on url " << brokerURL_ << ", " << e.what());
	}
}

// transition that set the url address
void b2in::utils::EventingDialup::setEventingURL (toolbox::Event::Reference e) throw (toolbox::fsm::exception::Exception)
{
	LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), "assigning address for b2in eventing");
	b2in::utils::ConnectEvent& ae = dynamic_cast<b2in::utils::ConnectEvent&>(*e);

	// eventing_ used in state machine callbacks only!
	eventing_ = ae.getURL();
}

void b2in::utils::EventingDialup::hangup (toolbox::Event::Reference e) throw (toolbox::fsm::exception::Exception)
{
	LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), "Hang up");

	if (brokerURL_ != "")
	{
		try
		{
			brokerMessengerCache_->invalidate(brokerURL_);
		}
		catch(b2in::utils::exception::Exception &e)
		{
			LOG4CPLUS_WARN(this->getOwnerApplication()->getApplicationLogger(), "failed to invalidate messenger cache " << e.what());
		}
	}

	if (eventing_ != "")
	{
		try
		{
			eventingMessengerCache_->invalidate(eventing_);
		}
		catch(b2in::utils::exception::Exception &e)
		{
			LOG4CPLUS_WARN(this->getOwnerApplication()->getApplicationLogger(), "failed to invalidate messenger cache " << e.what());
		}
		eventing_ = "";
	}
}

void b2in::utils::EventingDialup::connecting (toolbox::fsm::FiniteStateMachine& fsm) throw (toolbox::fsm::exception::Exception)
{
	LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), "connecting b2in eventing");
	try
	{
		eventingMessengerCache_->createMessenger(eventing_);
		toolbox::Event::Reference e(new toolbox::Event("establish", this));
		fsm_.fireEvent(e);
	}
	catch (b2in::utils::exception::Exception& e)
	{
		LOG4CPLUS_FATAL(this->getOwnerApplication()->getApplicationLogger(), "Failed to create messenger on eventing at " << eventing_ << ", " << e.what());
		toolbox::Event::Reference evt(new toolbox::Event("hangup", this));
		fsm_.fireEvent(evt);
	}
}

void b2in::utils::EventingDialup::ignore (toolbox::Event::Reference e) throw (toolbox::fsm::exception::Exception)
{
	LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), "spurious state transition, ignored" << e->type() << " on state " << fsm_.getCurrentState());
}

void b2in::utils::EventingDialup::send(toolbox::mem::Reference* msg, xdata::Properties & plist) throw(b2in::utils::exception::Exception)
{
	LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), "sending data");
	if (fsm_.getCurrentState() == 'E' )
	{
		try
		{
			outgoingReportCounter_++;
			eventingMessengerCache_->send(eventing_,msg,plist);
		}
		catch(b2in::nub::exception::InternalError & ex)
		{
			toolbox::Event::Reference e(new toolbox::Event("hangup", this));
			fsm_.fireEvent(e);
			XCEPT_RETHROW(b2in::utils::exception::Exception, "failed to send", ex);
		}
		catch(b2in::nub::exception::QueueFull & ex)
		{	
			toolbox::Event::Reference e(new toolbox::Event("hangup", this));
			fsm_.fireEvent(e);
			XCEPT_RETHROW(b2in::utils::exception::Exception, "queue overflow, failed to send", ex);
		}
		catch(b2in::nub::exception::OverThreshold & ex)
		{
			toolbox::Event::Reference e(new toolbox::Event("hangup", this));
			fsm_.fireEvent(e);
			XCEPT_RETHROW(b2in::utils::exception::Exception, "over threshold, failed to send", ex);
		}
	}
	else
	{
		XCEPT_RAISE(b2in::utils::exception::Exception, "connection to b2in eventing is not established");
	}
}

std::string b2in::utils::EventingDialup::getEventingURL()
{
	return eventing_;
}

std::string b2in::utils::EventingDialup::getBrokerURL()
{
	return brokerURL_;
}

xdata::UnsignedInteger64T b2in::utils::EventingDialup::getOutgoingReportCounter()
{
	//return (outgoingReportCounter_-outgoingReportLostCounter_);
	return (outgoingReportCounter_);
}

xdata::UnsignedInteger64T b2in::utils::EventingDialup::getOutgoingReportLostCounter()
{
	return outgoingReportLostCounter_;
}

xdata::UnsignedInteger64T b2in::utils::EventingDialup::getBrokerCounter()
{
	//return (brokerCounter_-brokerLostCounter_);
	return (brokerCounter_);
}

xdata::UnsignedInteger64T b2in::utils::EventingDialup::getBrokerLostCounter()
{
	return brokerLostCounter_;
}

std::string b2in::utils::EventingDialup::getStateName()
{
	return fsm_.getStateName(fsm_.getCurrentState());
}

bool b2in::utils::EventingDialup::isEstablished ()
{
	return (fsm_.getCurrentState() == 'E');
}

b2in::utils::EventingDialup::BrokerAsynchronousExceptionNotification::~BrokerAsynchronousExceptionNotification()
{
}

void b2in::utils::EventingDialup::BrokerAsynchronousExceptionNotification::asynchronousExceptionNotification(xcept::Exception& ex)
{
	LOG4CPLUS_DEBUG(parent_.getOwnerApplication()->getApplicationLogger(), "Sending a notification failed, " << ex.what());

	if (ex.name() == "b2in::utils::exception::SendFailure")
	{
		parent_.brokerLostCounter_++;
	}
	toolbox::Event::Reference e(new toolbox::Event("hangup", this));
	parent_.fsm_.fireEvent(e);
}

b2in::utils::EventingDialup::EventingAsynchronousExceptionNotification::~EventingAsynchronousExceptionNotification()
{
}

void b2in::utils::EventingDialup::EventingAsynchronousExceptionNotification::asynchronousExceptionNotification(xcept::Exception& ex)
{
	LOG4CPLUS_DEBUG(parent_.getOwnerApplication()->getApplicationLogger(), "Sending a notification failed, " << ex.what());

	if (ex.name() == "b2in::utils::exception::SendFailure")
	{
		parent_.outgoingReportLostCounter_++;
	}
	toolbox::Event::Reference e(new toolbox::Event("hangup", this));
	parent_.fsm_.fireEvent(e);
}

