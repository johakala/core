// $Id: MessengerCache.cc,v 1.17 2008/08/14 08:23:10 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "b2in/nub/Messenger.h"
#include "b2in/utils/MessengerWrapper.h"
#include <sstream>

#include "toolbox/task/Timer.h"
#include "toolbox/task/TimerFactory.h"
#include "toolbox/net/UUID.h"

b2in::utils::MessengerWrapper::MessengerWrapper (xdaq::ApplicationContext* context, const std::string & networkName, MessengerWrapperListener * listener, const std::string & checkConnectionInerval) throw (b2in::utils::exception::Exception)
	: lock_(toolbox::BSem::FULL, true)
{
	listener_ = listener;
	context_ = context;
	const xdaq::Network * network = 0;

	messengerReference_ = 0;
	messenger_ = 0;

	try
	{
		network = context_->getNetGroup()->getNetwork(networkName);
	}
	catch (xdaq::exception::NoNetwork& nn)
	{
		std::stringstream msg;
		msg << "Failed to access b2in network " << networkName << ", not configured";
		XCEPT_RETHROW(b2in::utils::exception::Exception, msg.str(), nn);
	}
	try
	{
		localAddress_ = network->getAddress(context_->getContextDescriptor());
	}
	catch (xdaq::exception::NoEndpoint& nn)
	{
		std::stringstream msg;
		msg << "Failed to create messenger cache for network " << networkName << ", not configured";
		XCEPT_RETHROW(b2in::utils::exception::Exception, msg.str(), nn);
	}

	// asynchronus sending exception
	subscribeFailedHandler_ = toolbox::exception::bind(this, &b2in::utils::MessengerWrapper::subscribeFailed, "subscribeFailed");

	toolbox::task::Timer * timer = 0;
	std::string name = "urn:b2in-utils-messenger-cache:check-connection-timer";
	try
	{
		if (!toolbox::task::getTimerFactory()->hasTimer(name))
		{
			timer = toolbox::task::getTimerFactory()->createTimer(name);
		}
		else
		{
			timer = toolbox::task::getTimerFactory()->getTimer(name);
		}
	}
	catch (toolbox::task::exception::Exception & ex)
	{
		std::stringstream msg;
		msg << "failed to create timer name:" << name;
		XCEPT_RETHROW(b2in::utils::exception::Exception, msg.str(), ex);
	}
	// submit task for dialing
	toolbox::TimeInterval interval;
	toolbox::net::UUID uuid;

	try
	{
		toolbox::TimeVal start;
		start = toolbox::TimeVal::gettimeofday();
		interval.fromString(checkConnectionInerval);
		timer->scheduleAtFixedRate(start, this, interval, 0, "check-connection-staging" + uuid.toString());
	}
	catch (toolbox::task::exception::InvalidListener& e)
	{
		XCEPT_RETHROW(b2in::utils::exception::Exception, "failed to schedule broker-staging in eventingdialup timer ", e);
	}
	catch (toolbox::task::exception::InvalidSubmission& e)
	{
		XCEPT_RETHROW(b2in::utils::exception::Exception, "failed to schedule broker-staging in eventingdialup timer ", e);
	}
	catch (toolbox::task::exception::NotActive& e)
	{
		XCEPT_RETHROW(b2in::utils::exception::Exception, "failed to schedule broker-staging in eventingdialup timer ", e);
	}
	catch (toolbox::exception::Exception& e)
	{
		XCEPT_RETHROW(b2in::utils::exception::Exception, "failed to schedule broker-staging in eventingdialup timer ", e);
	}

}

pt::Address::Reference b2in::utils::MessengerWrapper::getLocalAddress ()
{
	return localAddress_;
}
//!
//
void b2in::utils::MessengerWrapper::invalidate () throw (b2in::utils::exception::Exception)
{
	lock_.take();

	if (messenger_ != 0)
	{
		messengerReference_ = 0;
		messenger_ = 0;
	}

	lock_.give();
}

void b2in::utils::MessengerWrapper::createMessenger (const std::string & url) throw (b2in::utils::exception::Exception)
{
	lock_.take();

	if (messenger_ == 0)
	{
		try
		{

			pt::Address::Reference remoteAddress = pt::getPeerTransportAgent()->createAddress(url, "b2in");

			if (localAddress_->equals(remoteAddress))
			{
				pt::Address::Reference loopbackAddress = pt::getPeerTransportAgent()->createAddress("loopback://localhost", "b2in");
				pt::Messenger::Reference mr = pt::getPeerTransportAgent()->getMessenger(loopbackAddress, loopbackAddress);
				messengerReference_ = mr;
				messenger_ = &(*mr);

			}
			else
			{
				pt::Messenger::Reference mr = pt::getPeerTransportAgent()->getMessenger(remoteAddress, localAddress_);
				messengerReference_ = mr;
				messenger_ = &(*mr);
			}

			lock_.give();
			return;
		}
		catch (xdaq::exception::NoRoute & e)
		{
			std::stringstream msg;
			msg << "Failed to create messenger to url '" << url << "'";
			lock_.give();
			XCEPT_RETHROW(b2in::utils::exception::Exception, msg.str(), e);
		}
		catch (xdaq::exception::NoNetwork & e)
		{
			std::stringstream msg;
			msg << "Failed to create messenger to url '" << url << "'";
			lock_.give();
			XCEPT_RETHROW(b2in::utils::exception::Exception, msg.str(), e);
		}
		catch (xdaq::exception::NoEndpoint & e)
		{
			std::stringstream msg;
			msg << "Failed to create messenger to url '" << url << "'";
			lock_.give();
			XCEPT_RETHROW(b2in::utils::exception::Exception, msg.str(), e);
		}
		catch (pt::exception::InvalidAddress& e)
		{
			std::stringstream msg;
			msg << "Failed to create messenger to url '" << url << "'";
			lock_.give();
			XCEPT_RETHROW(b2in::utils::exception::Exception, msg.str(), e);
		}
		catch (pt::exception::NoMessenger & e)
		{
			std::stringstream msg;
			msg << "Failed to create messenger to url '" << url << "'";
			lock_.give();
			XCEPT_RETHROW(b2in::utils::exception::Exception, msg.str(), e);
		}
	}
	else
	{
		std::stringstream msg;
		msg << "Failed to create messenger to url '" << url << "', a messenger has already been created (invalidate this first)";
		lock_.give();
		XCEPT_RAISE(b2in::utils::exception::Exception, msg.str());
	}

	lock_.give();
}

void b2in::utils::MessengerWrapper::send (toolbox::mem::Reference* msg, xdata::Properties & plist, void * cookie) throw (b2in::nub::exception::InternalError, b2in::nub::exception::QueueFull, b2in::nub::exception::OverThreshold)
{

	lock_.take();

	if (messenger_ == 0)
	{
		lock_.give();
		std::stringstream msg;
		msg << "No messenger has been created";
		XCEPT_RAISE(b2in::nub::exception::InternalError, msg.str());
	}

	try
	{
		plist.setProperty("urn:b2in-protocol:originator", context_->getContextDescriptor()->getURL());
		dynamic_cast<b2in::nub::Messenger*>(messenger_)->send(msg, plist, subscribeFailedHandler_, cookie);
	}
	catch (b2in::nub::exception::InternalError & e)
	{
		lock_.give();
		std::stringstream msg;
		msg << "Internal error, failed to send";
		XCEPT_RETHROW(b2in::nub::exception::InternalError, msg.str(), e);

	}
	catch (b2in::nub::exception::QueueFull & e)
	{
		lock_.give();
		std::stringstream msg;
		msg << "Failed to send";
		XCEPT_RETHROW(b2in::nub::exception::QueueFull, msg.str(), e);

	}
	catch (b2in::nub::exception::OverThreshold & e)
	{
		lock_.give();
		std::stringstream msg;
		msg << "Failed to send";
		XCEPT_RETHROW(b2in::nub::exception::OverThreshold, msg.str(), e);
	}

	lock_.give();
}

void b2in::utils::MessengerWrapper::timeExpired (toolbox::task::TimerEvent& e)
{
	lock_.take();

	if (messenger_ != 0)
	{
		xdata::Properties plist;
		//plist.setProperty("urn:b2in-protocol:originator", context_->getContextDescriptor()->getURL());
		plist.setProperty("urn:b2in-protocol:checkconnection", "yes");

		dynamic_cast<b2in::nub::Messenger*>(messenger_)->send(0, plist, subscribeFailedHandler_, 0);
	}

	lock_.give();
}

bool b2in::utils::MessengerWrapper::subscribeFailed (xcept::Exception& e, void * context)
{
	lock_.take();

	this->invalidate();
	//XCEPT_DECLARE_NESTED(b2in::utils::exception::Exception, ex, "Failed to send", e);
	lock_.give();
	listener_->asynchronousExceptionNotification(e, context);
	return false;
}

bool b2in::utils::MessengerWrapper::hasMessenger ()
{

	lock_.take();

	if (messenger_ != 0)
	{
		lock_.give();
		return true;
	}

	lock_.give();
	return false;

}

