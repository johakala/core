//$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/
#include <set>
#include <string>
#include "b2in/utils/ServiceProxy.h"
#include "xplore/Interface.h"
#include "xplore/exception/Exception.h"
#include "xplore/DiscoveryEvent.h"
#include "xcept/tools.h"

b2in::utils::ServiceProxy::ServiceProxy
(
	xdaq::Application * owner,
	const std::string& servicename, const std::string& group, b2in::utils::MessengerCacheListener * listener
) 
	throw(b2in::utils::exception::Exception)
	 :xdaq::Object(owner), messengerCache_(0),servicename_(servicename),group_(group), listener_(listener)
{
}

//!
//
bool b2in::utils::ServiceProxy::scan()
	throw (b2in::utils::exception::Exception)
{
	bool found = false;

	// Find all endpoints in the default network of las. Then query them to see, which ones
	
	if (messengerCache_ == 0)
	{
		try
		{
			messengerCache_ = new b2in::utils::MessengerCache(
							this->getOwnerApplication()->getApplicationContext(), 
							this->getOwnerApplication()->getApplicationDescriptor()->getAttribute("network"),
							listener_
						);
		}
		catch (b2in::utils::exception::Exception& e)
		{
			std::stringstream msg;
			msg << "Failed to create b2in messenger cache on network '";
			msg << this->getOwnerApplication()->getApplicationDescriptor()->getAttribute("network") << "'";
			XCEPT_RETHROW (b2in::utils::exception::Exception, msg.str(), e); 
		}
	}

	xplore::Interface * interface;
	try
	{
		interface = dynamic_cast<xplore::Interface*> (this->getOwnerApplication()->getApplicationContext()->getFirstApplication("xplore::Application"));	
	}
	catch(xdaq::exception::Exception & e)
	{
		std::stringstream msg;
		msg << "xplore application not found in this executive";
		XCEPT_RETHROW (b2in::utils::exception::Exception, msg.str(), e);
	}

	std::vector<xplore::Advertisement::Reference> applicationResultSet;
	std::vector<xplore::Advertisement::Reference> endpointResultSet;
	std::vector<xplore::Advertisement::Reference>::iterator vi;

	try
	{	
		std::set<std::string> groups = toolbox::parseTokenSet ( group_, "," );		
		std::stringstream sa;
		
		sa << "(&(service=" << servicename_ << ")";                
		sa << "(|";
		
		std::set<std::string>::iterator i;
		for (i = groups.begin(); i != groups.end(); ++i)
		{
			sa << "(group=*" << (*i) << "*)";
		}
		sa << "))";
		
		interface->search ("peer", sa.str(), applicationResultSet);
	}
	catch (xdaq::exception::Exception& e)
	{
		std::stringstream msg;
		msg << "Failed to discover b2in-eventing applications";
		XCEPT_RETHROW (b2in::utils::exception::Exception, msg.str(), e);
	}
	
	if (applicationResultSet.empty())
	{
		LOG4CPLUS_INFO (this->getOwnerApplication()->getApplicationLogger(), "No b2in-eventing applications in group " << group_ << ", retrying later...");
		return false;
	}
	
	// Now extract the context urls from these advertisements and build a query to 
	// find the b2in endpoints on those hosts	
	try
	{
		for (vi = applicationResultSet.begin(); vi != applicationResultSet.end(); )
		{
			std::vector<xplore::Advertisement::Reference> tmpResultSet;
			std::stringstream se;
			se << "(&(service=b2in)(network=" << this->getOwnerApplication()->getApplicationDescriptor()->getAttribute("network") << ")(|";
		
			for(int i=0 ; (i < 10) && (vi != applicationResultSet.end()) ; i++)
			{			
				toolbox::net::URL url((*vi)->getURL());
				LOG4CPLUS_DEBUG (this->getOwnerApplication()->getApplicationLogger(), "application URL: " << url.getScheme() << "://" << url.getAuthority() << "on network:" << this->getOwnerApplication()->getApplicationDescriptor()->getAttribute("network"));
			
				se << "(context=" << url.getScheme() << "://" << url.getAuthority() << ")";
				++vi;
			}
			se << "))";
		
			LOG4CPLUS_DEBUG (this->getOwnerApplication()->getApplicationLogger(), "search endpoint matching:" << se.str());
			interface->search ("endpoint", se.str(), tmpResultSet);
			endpointResultSet.insert(endpointResultSet.end(), tmpResultSet.begin(), tmpResultSet.end());
		}
	}
	catch (xdaq::exception::Exception& e)
	{
		std::stringstream msg;
		msg << "Failed to discover b2in endpoints";
		XCEPT_RETHROW (b2in::utils::exception::Exception, msg.str(), e);
	}
	
	if (endpointResultSet.empty())
	{
		LOG4CPLUS_ERROR (this->getOwnerApplication()->getApplicationLogger(), "No b2in endpoints found for b2in-eventing services for group " << group_);
		return false;
	}
	
	for (vi = endpointResultSet.begin(); vi != endpointResultSet.end(); ++vi)
	{
		// Check if for the discovered URL an entry exists already
		toolbox::net::URL url((*vi)->getURL());
		std::string scheme = url.getScheme();
		LOG4CPLUS_DEBUG (this->getOwnerApplication()->getApplicationLogger(), "endpoint URL: " << scheme << "://" << url.getAuthority());
		std::string protocolB2in = "b2in.";
		if (scheme.find(protocolB2in) != std::string::npos)
		{
			std::stringstream remoteAddressUrl;
			scheme.replace(0,protocolB2in.size(),"");
			remoteAddressUrl << scheme <<"://" << url.getAuthority();
			
			try
			{
				messengerCache_->createMessenger(remoteAddressUrl.str()); // trigger  caching of messenger for the remote endpoint
				found = true;
			}
			catch (b2in::utils::exception::Exception& e)
			{
				LOG4CPLUS_WARN(this->getOwnerApplication()->getApplicationLogger(), "Failed to create messenger on b2in network to url " << remoteAddressUrl.str());
			}
		}
	}
	return found;
}

b2in::utils::MessengerCache*  b2in::utils::ServiceProxy::getMessengerCache()
	throw (b2in::utils::exception::Exception)

{
	if ( messengerCache_ != 0 )
	{
		return messengerCache_;
	}
	else
	{
		XCEPT_RAISE (b2in::utils::exception::Exception, "no messenger cache created (need scanning)");
	}
}

void b2in::utils::ServiceProxy::addURL(const std::string & url)
	throw (b2in::utils::exception::Exception)
{
	if (messengerCache_ == 0)
	{
		try
		{
			messengerCache_ = new b2in::utils::MessengerCache(
							this->getOwnerApplication()->getApplicationContext(), 
							this->getOwnerApplication()->getApplicationDescriptor()->getAttribute("network"),
							listener_
						);
		}
		catch (b2in::utils::exception::Exception& e)
		{
			std::stringstream msg;
			msg << "Failed to create b2in messenger cache on network '";
			msg << this->getOwnerApplication()->getApplicationDescriptor()->getAttribute("network") << "'";
			XCEPT_RETHROW (b2in::utils::exception::Exception, msg.str(), e); 
		}
	}
	
	try
	{
		messengerCache_->createMessenger(url); 
	}
	catch (b2in::utils::exception::Exception& e)
	{
		std::stringstream msg;
		msg << "Failed to create  messenger for url '" << url  << "'";
		XCEPT_RETHROW (b2in::utils::exception::Exception, msg.str(), e); 
	}
}

