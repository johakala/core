// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "b2in/nub/Messenger.h"
#include "b2in/utils/MessengerCache.h"
#include <sstream>

#include "toolbox/task/Timer.h"
#include "toolbox/task/TimerFactory.h"
#include "toolbox/net/UUID.h"

#include "b2in/utils/exception/CheckConnectionFailure.h"
#include "b2in/utils/exception/SendFailure.h"
#include "b2in/utils/exception/InternalError.h"

b2in::utils::MessengerCache::MessengerCache
(
	xdaq::ApplicationContext* context,
	const std::string & networkName,
	MessengerCacheListener * listener//,
	//const std::string & watchdog
)
throw(b2in::utils::exception::Exception)
: lock_(toolbox::BSem::FULL,true)
{
	listener_ = listener;
	context_ = context;
	const xdaq::Network * network = 0;
	try
	{
		network = context_->getNetGroup()->getNetwork(networkName);
	}
	catch (xdaq::exception::NoNetwork& nn)
	{
		std::stringstream msg;
		msg << "Failed to access b2in network " << networkName << ", not configured";
		XCEPT_RETHROW (b2in::utils::exception::Exception, msg.str(), nn);
	}
	 try
        {
		localAddress_ = network->getAddress(context_->getContextDescriptor());
        }
        catch (xdaq::exception::NoEndpoint& nn)
        {
		 std::stringstream msg;
                msg << "Failed to create messenger cache for network " << networkName << ", not configured";
                XCEPT_RETHROW (b2in::utils::exception::Exception, msg.str(), nn);
	}

	// asynchronus sending exception
	subscribeFailedHandler_ = toolbox::exception::bind(this, &b2in::utils::MessengerCache::subscribeFailed, "subscribeFailed");
	checkConnectionFailedHandler_ = toolbox::exception::bind(this, &b2in::utils::MessengerCache::checkConnectionFailed, "checkConnectionFailed");

	/*
	toolbox::task::Timer * timer = 0;
	std::string name = "urn:b2in-utils-messenger-cache:check-connection-timer";
	try
	{
		if (!toolbox::task::getTimerFactory()->hasTimer(name))
		{
			timer = toolbox::task::getTimerFactory()->createTimer(name);
		}
		else
		{
			timer = toolbox::task::getTimerFactory()->getTimer(name);
		}
	}
	catch(toolbox::task::exception::Exception & ex)
	{
		std::stringstream msg;
		msg << "failed to create timer name:" <<  name;
		XCEPT_RETHROW(b2in::utils::exception::Exception, msg.str(), ex );
	}

		// submit task for dialing
	toolbox::TimeInterval interval;
	toolbox::net::UUID uuid;

	try
	{
		toolbox::TimeVal start;
		start = toolbox::TimeVal::gettimeofday();
		std::string watchdog = "PT10S";
		interval.fromString(watchdog);
		timer->scheduleAtFixedRate( start, this, interval, 0, "check-connection-staging" + uuid.toString());
	}
	catch (toolbox::task::exception::InvalidListener& e)
	{
		XCEPT_RETHROW(b2in::utils::exception::Exception, "failed to schedule broker-staging in eventingdialup timer ", e );
	}
	catch (toolbox::task::exception::InvalidSubmission& e)
	{
		XCEPT_RETHROW(b2in::utils::exception::Exception, "failed to schedule broker-staging in eventingdialup timer ", e );
	}
	catch (toolbox::task::exception::NotActive& e)
	{
		XCEPT_RETHROW(b2in::utils::exception::Exception, "failed to schedule broker-staging in eventingdialup timer ", e );
	}
	catch (toolbox::exception::Exception& e)
	{
		XCEPT_RETHROW(b2in::utils::exception::Exception, "failed to schedule broker-staging in eventingdialup timer ", e );
	}

	*/
}

pt::Address::Reference b2in::utils::MessengerCache::getLocalAddress() 
{
	return localAddress_;
}	
//!
//
void b2in::utils::MessengerCache::invalidate 
(
	const std::string & url)
	throw (b2in::utils::exception::Exception)
{
	lock_.take();
	pt::Messenger* m;
	
	std::map<std::string, pt::Messenger*>::iterator j =  messengers_.find(url);

	if ( j != messengers_.end())
	{
		m = (*j).second;
	}
	else
	{
		lock_.give();
		std::stringstream msg;
		msg << "No messenger to url '" << url << "' found for invalidation";
		XCEPT_RAISE (b2in::utils::exception::Exception, msg.str());
	}

	std::vector<pt::Messenger::Reference>::iterator i = messengerReferences_.begin();
	while (i != messengerReferences_.end())
	{
		if (m == &(*(*i)))
		{
			b2in::nub::Messenger * nubMessenger = 0;
			nubMessenger = dynamic_cast<b2in::nub::Messenger*>(m);
			if (nubMessenger != 0)
			{
				nubMessenger->invalidate();
			}
			else
			{
				std::stringstream msg;
				msg << "Failed to cast messenger to b2in::nub::Messenger";
				XCEPT_RAISE (b2in::utils::exception::Exception, msg.str());
			}
			messengers_.erase(url);
			messengerReferences_.erase(i);
			lock_.give();
			return;
		}
		i++;
	}
	lock_.give();
	
	std::stringstream msg;
	msg << "No messenger to url '" << url << "' found for invalidation";
	XCEPT_RAISE (b2in::utils::exception::Exception, msg.str());
}


void b2in::utils::MessengerCache::createMessenger
(
	const std::string & url
) 
	throw (b2in::utils::exception::Exception)
{
	lock_.take();

	
		
	if ( messengers_.find(url) == messengers_.end())
	{	
		try 
		{	
		
			pt::Address::Reference remoteAddress = pt::getPeerTransportAgent()->createAddress (url, "b2in" );	
			
			if ( localAddress_->equals(remoteAddress) )
			{
				pt::Address::Reference loopbackAddress = pt::getPeerTransportAgent()->createAddress ("loopback://localhost", "b2in" );
				pt::Messenger::Reference mr = pt::getPeerTransportAgent()->getMessenger ( loopbackAddress, loopbackAddress );
				messengerReferences_.push_back(mr);
				messengers_[url] = &(*mr);

			}
			else
			{ 						
				pt::Messenger::Reference mr = pt::getPeerTransportAgent()->getMessenger ( remoteAddress, localAddress_ );
				messengerReferences_.push_back(mr);
				messengers_[url] = &(*mr);
			}
			
			lock_.give();
			return;
		}
		catch (xdaq::exception::NoRoute & e)
		{
			std::stringstream msg;
			msg << "Failed to create messenger to url '" << url << "'";
			lock_.give();
			XCEPT_RETHROW(b2in::utils::exception::Exception,msg.str(), e);
		}
		catch (xdaq::exception::NoNetwork & e)
		{
			std::stringstream msg;
			msg << "Failed to create messenger to url '" << url << "'";
			lock_.give();
			XCEPT_RETHROW(b2in::utils::exception::Exception, msg.str(), e);
		}
		catch (xdaq::exception::NoEndpoint & e)
		{
			std::stringstream msg;
			msg << "Failed to create messenger to url '" << url << "'";
			lock_.give();
			XCEPT_RETHROW(b2in::utils::exception::Exception, msg.str(), e);
		}
		catch (pt::exception::InvalidAddress& e)
		{
			std::stringstream msg;
			msg << "Failed to create messenger to url '" << url << "'";
			lock_.give();
			XCEPT_RETHROW(b2in::utils::exception::Exception, msg.str(), e);
		}
		catch (pt::exception::NoMessenger & e)
		{
			std::stringstream msg;
			msg << "Failed to create messenger to url '" << url << "'";
			lock_.give();
			XCEPT_RETHROW(b2in::utils::exception::Exception, msg.str(), e);
		}
	}
	
	lock_.give();
}



std::list<std::string> b2in::utils::MessengerCache::getDestinations
(
) 
{

	std::list<std::string> destinations;
	lock_.take();
	for (std::map<std::string, pt::Messenger*>::iterator i =  messengers_.begin(); 	i != messengers_.end(); i++ )
	{
		destinations.push_back((*i).first);
	}
	lock_.give();
	return destinations;


}

void b2in::utils::MessengerCache::send (const std::string & url, toolbox::mem::Reference* msg, xdata::Properties & plist) 
				throw (b2in::nub::exception::InternalError, b2in::nub::exception::QueueFull, b2in::nub::exception::OverThreshold)
{

	lock_.take();
	std::map<std::string, pt::Messenger*>::iterator i=  messengers_.find(url);

	if ( i == messengers_.end())
	{
		lock_.give();
		std::stringstream msg;
		msg << "Failed to retrieve messenger for url '" << url << "'";
		XCEPT_RAISE(b2in::nub::exception::InternalError, msg.str());
	}
	
//#warning "B2IN close connection support is currently turned off"
	//std::cout << "Overriding tcp:connection option in b2in message to be 'keep-alive'" << std::endl;
	//plist.setProperty ("urn:b2in-protocol-tcp:connection", "keep-alive");

	try
	{
			plist.setProperty("urn:b2in-protocol:originator", context_->getContextDescriptor()->getURL());
			dynamic_cast<b2in::nub::Messenger*>((*i).second)->send(msg,plist,subscribeFailedHandler_,(*i).second);
	}
	catch (b2in::nub::exception::InternalError & e)
	{
		// find out if it is a ConnectionRequestTimeout or not. This will be the last attached exception in the exception history
		lock_.give();

		std::stringstream msg;
		msg << "Internal error, failed to send to url '" << url << "'";
		XCEPT_RETHROW(b2in::nub::exception::InternalError, msg.str(), e);
	}
	catch (  b2in::nub::exception::QueueFull & e)
	{
		lock_.give();
		std::stringstream msg;
		msg << "Failed to send to url '" << url << "'";
		XCEPT_RETHROW(b2in::nub::exception::QueueFull, msg.str(), e);

	}
	catch ( b2in::nub::exception::OverThreshold & e)
	{
		lock_.give();
		std::stringstream msg;
		msg << "Failed to send to url '" << url << "'";
		XCEPT_RETHROW(b2in::nub::exception::OverThreshold, msg.str(), e);
	}

	
	lock_.give();
}				

void b2in::utils::MessengerCache::timeExpired(toolbox::task::TimerEvent& e)
{
	lock_.take();
	for (std::map<std::string, pt::Messenger*>::iterator i =  messengers_.begin(); 	i != messengers_.end(); i++ )
	{
		xdata::Properties plist;
		//plist.setProperty("urn:b2in-protocol:originator", context_->getContextDescriptor()->getURL());
		plist.setProperty("urn:b2in-protocol:checkconnection", "yes");

		try
		{
			dynamic_cast<b2in::nub::Messenger*>((*i).second)->send(0,plist,checkConnectionFailedHandler_,(*i).second);
		}
		catch (b2in::nub::exception::InternalError & e )
		{
			this->invalidate((*i).first);
			XCEPT_DECLARE_NESTED (b2in::utils::exception::CheckConnectionFailure, ex, "Failed to check connection", e);
			lock_.give();
			listener_->asynchronousExceptionNotification(ex);
			return;
		}
		catch (b2in::nub::exception::QueueFull & e )
		{
			this->invalidate((*i).first);
			XCEPT_DECLARE_NESTED (b2in::utils::exception::CheckConnectionFailure, ex, "Failed to check connection", e);
			lock_.give();
			listener_->asynchronousExceptionNotification(ex);
			return;
		}
		catch(b2in::nub::exception::OverThreshold & e)
		{
			this->invalidate((*i).first);
			XCEPT_DECLARE_NESTED (b2in::utils::exception::CheckConnectionFailure, ex, "Failed to check connection", e);
			lock_.give();
			listener_->asynchronousExceptionNotification(ex);
			return;
		}

	}
	lock_.give();
}

bool b2in::utils::MessengerCache::subscribeFailed(xcept::Exception& e, void * context) 
{
	lock_.take();

	for (std::map<std::string, pt::Messenger*>::iterator i =  messengers_.begin(); 	i != messengers_.end(); i++ )
	{
		if (context == (*i).second )
		{
			try
			{
				this->invalidate((*i).first);
				if (e.name() == "pt::exception::SendFailure")
				{
					XCEPT_DECLARE_NESTED (b2in::utils::exception::SendFailure, ex, "Failed to send, invalidate cache", e);
					lock_.give();
					listener_->asynchronousExceptionNotification(ex);
				}
				else
				{
					XCEPT_DECLARE_NESTED (b2in::utils::exception::Exception, ex, "Failed to send, invalidate cache", e);
					lock_.give();
					listener_->asynchronousExceptionNotification(ex);
				}
				return false; 	
			}
			catch (b2in::utils::exception::Exception & utilsex )
			{
				// ignore if already invalidated
				XCEPT_DECLARE_NESTED (b2in::utils::exception::InternalError, ex, "Failed to send, cache already invalidated", e);
				lock_.give();
				listener_->asynchronousExceptionNotification(ex);
				return false; 				
			}
			break;
			
		}
	}
	
	XCEPT_DECLARE_NESTED (b2in::utils::exception::InternalError, ex, "Failed to send, no cache to invalidate", e);
	lock_.give();
	listener_->asynchronousExceptionNotification(ex);
	return false; 				
}	

bool b2in::utils::MessengerCache::checkConnectionFailed(xcept::Exception& e, void * context)
{
	lock_.take();

	for (std::map<std::string, pt::Messenger*>::iterator i =  messengers_.begin(); 	i != messengers_.end(); i++ )
	{
		if (context == (*i).second )
		{
			try
			{
				this->invalidate((*i).first);
				XCEPT_DECLARE_NESTED (b2in::utils::exception::CheckConnectionFailure, ex, "Failed to send, invalidate cache", e);
				lock_.give();
				listener_->asynchronousExceptionNotification(ex);
				return false;
			}
			catch (b2in::utils::exception::Exception & utilsex )
			{
				// ignore if already invalidated
				XCEPT_DECLARE_NESTED (b2in::utils::exception::InternalError, ex, "Failed to send, cache already invalidated", e);
				lock_.give();
				listener_->asynchronousExceptionNotification(ex);
				return false;
			}
			break;

		}
	}

	XCEPT_DECLARE_NESTED (b2in::utils::exception::InternalError, ex, "Failed to send, no cache to invalidate", e);
	lock_.give();
	listener_->asynchronousExceptionNotification(ex);
	return false;
}

bool b2in::utils::MessengerCache::hasMessenger (const std::string & url) 
{

	lock_.take();
	std::map<std::string, pt::Messenger*>::iterator i=  messengers_.find(url);

	if ( i != messengers_.end())
	{
		lock_.give();
		return true;
	}
	lock_.give();
	return false;
	
}				


