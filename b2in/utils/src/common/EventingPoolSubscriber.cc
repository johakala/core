// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "b2in/utils/EventingPoolSubscriber.h"
#include "b2in/utils/ConnectEvent.h"

#include "xcept/tools.h"
#include "toolbox/stl.h"
#include "toolbox/string.h"
#include "toolbox/net/URL.h"
#include "toolbox/exception/Handler.h"
#include "toolbox/exception/Processor.h"
#include "toolbox/task/exception/InvalidListener.h"
#include "toolbox/task/exception/NotActive.h"
#include "toolbox/task/exception/InvalidSubmission.h"
#include "toolbox/mem/Pool.h"
#include "toolbox/mem/Reference.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "toolbox/mem/CommittedHeapAllocator.h"
#include "toolbox/Event.h"
#include "toolbox/task/Timer.h"
#include "toolbox/task/TimerFactory.h"

#include <algorithm>

b2in::utils::EventingPoolSubscriber::EventingPoolSubscriber(xdaq::Application * owner, const std::string & brokerURL, const std::string & networkName, toolbox::TimeInterval scanPeriod,
	toolbox::TimeInterval expirationPeriod, std::set<std::string> & topics,	std::set<std::string> groups)
	throw (b2in::utils::exception::Exception)
	:  xdaq::Object(owner),	brokerAsynchronousExceptionNotification_(*this), eventingAsynchronousExceptionNotification_(*this), mutex_(toolbox::BSem::FULL), groups_(groups), brokerURL_(brokerURL)
{
	if (brokerURL_ == "")
	{
 		XCEPT_RAISE(b2in::utils::exception::Exception, "Broker URL is empty");
		return;
	}

	// Initialize message counters
	brokerCounter_           = 0;
	brokerLostCounter_       = 0;
	subscriptionCounter_     = 0;
	subscriptionLostCounter_ = 0;

	expirationPeriod_  = expirationPeriod;

	this->updateTopics(topics);

	try
	{
		brokerMessengerCache_ = new b2in::utils::MessengerCache( this->getOwnerApplication()->getApplicationContext(), networkName, &brokerAsynchronousExceptionNotification_);
	}
	catch (b2in::utils::exception::Exception& e)
	{
		std::stringstream msg;
		msg << "Failed to create b2in messenger cache on network '";
		msg << this->getOwnerApplication()->getApplicationDescriptor()->getAttribute("network") << "'";
		XCEPT_RETHROW(b2in::utils::exception::Exception, msg.str(), e);
	}

	try
	{
		eventingMessengerCache_ = new b2in::utils::MessengerCache( this->getOwnerApplication()->getApplicationContext(), networkName,  &eventingAsynchronousExceptionNotification_);
	}
	catch (b2in::utils::exception::Exception& e)
	{
		delete brokerMessengerCache_;
		std::stringstream msg;
		msg << "Failed to create b2in messenger cache on network '";
		msg << this->getOwnerApplication()->getApplicationDescriptor()->getAttribute("network") << "'";
		XCEPT_RETHROW(b2in::utils::exception::Exception, msg.str(), e);
	}

	std::string name = "b2in-utils-eventingpoolsubscriber-"+owner->getApplicationDescriptor()->getAttribute("service");	
        toolbox::task::getTimerFactory()->createTimer(name);
        toolbox::task::Timer * timer = toolbox::task::getTimerFactory()->getTimer(name);

        // submit task for dialing
        try
        {
		toolbox::TimeVal start;
        	start = toolbox::TimeVal::gettimeofday();
        	timer->scheduleAtFixedRate( start, this, scanPeriod, 0, "eventingpoolsubscriber-staging" );
        }
        catch (toolbox::task::exception::InvalidListener& e)
        {
		XCEPT_RETHROW(b2in::utils::exception::Exception, "failed to schedule eventingpoolsubscriber-staging", e );
        }
        catch (toolbox::task::exception::InvalidSubmission& e)
        {
		XCEPT_RETHROW(b2in::utils::exception::Exception, "failed to schedule eventingpoolsubscriber-staging", e );
        }
        catch (toolbox::task::exception::NotActive& e)
        {
		XCEPT_RETHROW(b2in::utils::exception::Exception, "failed to schedule eventingpoolsubscriber-staging", e );
        }
        catch (toolbox::exception::Exception& e)
        {
		XCEPT_RETHROW(b2in::utils::exception::Exception, "failed to schedule eventingpoolsubscriber-staging", e );
        }
}

b2in::utils::EventingPoolSubscriber::~EventingPoolSubscriber()
{
}

void b2in::utils::EventingPoolSubscriber::timeExpired(toolbox::task::TimerEvent& e)
{
 	LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), "eventingpoolsubscriber time elapsed");

	std::string name = e.getTimerTask()->name;

	if ( name == "eventingpoolsubscriber-staging" )
	{
		this->requesting();
		this->refreshSubscriptions();
	}
}

void b2in::utils::EventingPoolSubscriber::updateEventings (xdata::Properties & plist)
{
	std::string action = plist.getProperty("urn:xmasbroker2g:action");
	std::string model = plist.getProperty("urn:xmasbroker2g:model");
	if ( action == "allocate" && model == "eventingpool")
	{
		LOG4CPLUS_DEBUG (this->getOwnerApplication()->getApplicationLogger(), "Received subscribeurl: " << plist.getProperty ("urn:xmasbroker2g:subscribeurl"));

		mutex_.take();
		std::set<std::string> eventings = toolbox::parseTokenSet(plist.getProperty ("urn:xmasbroker2g:subscribeurl"), ",");

		// invalidate old eventings
		std::set<std::string> obsolete = toolbox::stl::difference(eventingURLs_, eventings);
		for(std::set<std::string>::iterator i = obsolete.begin() ; i != obsolete.end() ; i++)
		{
			try
			{
				eventingMessengerCache_->invalidate(*i);
			}
			catch(b2in::utils::exception::Exception &e)
			{
				LOG4CPLUS_WARN(this->getOwnerApplication()->getApplicationLogger(), "failed to invalidate messenger cache " << e.what());
			}
		}

		// recreate new list of eventing Messengers
		eventingURLs_ = eventings;
		for(std::set<std::string>::iterator i = eventingURLs_.begin() ; i != eventingURLs_.end() ; i++)
		{
			if( ! eventingMessengerCache_->hasMessenger(*i))
			{
				try
				{
					eventingMessengerCache_->createMessenger(*i);
				}
				catch (b2in::utils::exception::Exception& e)
				{
					LOG4CPLUS_FATAL(this->getOwnerApplication()->getApplicationLogger(), "Failed to create messenger on to broker" << brokerURL_ << ", " << e.what());
					continue;
				}
			}
		}

		mutex_.give();
	}
}


void b2in::utils::EventingPoolSubscriber::refreshSubscriptions ()
{
	mutex_.take();

	LOG4CPLUS_DEBUG (this->getOwnerApplication()->getApplicationLogger(), "subscribe/Renew");
	for (std::set<std::string>::iterator j = eventingURLs_.begin() ; j != eventingURLs_.end() ; j++)
	{
		//
		// Subscribe to OR Renew all existing subscriptions
		//
		std::map<std::string, xdata::Properties>::iterator i;
		for ( i = subscriptions_.begin(); i != subscriptions_.end(); ++i)
		{
			try
			{
				// plist is already prepared for a subscribe/resubscribe message
				//
				subscriptionCounter_++;
				LOG4CPLUS_DEBUG (this->getOwnerApplication()->getApplicationLogger(), "Subscribe to topic " << (*i).first);
        			eventingMessengerCache_->send((*j),0,(*i).second);
			}
			catch (b2in::nub::exception::InternalError & e)
                        {
				subscriptionLostCounter_++;

                                std::stringstream msg;
                                msg << "failed to send subscription for topic (internal error) " << (*i).first;
                                XCEPT_DECLARE_NESTED(b2in::utils::exception::Exception, ex , msg.str(), e);
                                this->getOwnerApplication()->notifyQualified("fatal",ex);
				mutex_.give();
				return;
                        }
                        catch ( b2in::nub::exception::QueueFull & e )
                        {                                
				subscriptionLostCounter_++;

                                std::stringstream msg;
                                msg << "failed to send subscription for topic (queue full) " << (*i).first;
                                XCEPT_DECLARE_NESTED(b2in::utils::exception::Exception, ex , msg.str(), e);
                                this->getOwnerApplication()->notifyQualified("error",ex);
				mutex_.give();
				return;
                        }
                        catch ( b2in::nub::exception::OverThreshold & e)
                        {
                                // ignore just count to avoid verbosity                                
				subscriptionLostCounter_++;
				mutex_.give();
                                return; 
                        }
		}
	}
	mutex_.give();
}

void b2in::utils::EventingPoolSubscriber::requesting ()
{
 	LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), "requesting eventings in groups '" << toolbox::printTokenSet(groups_, ",") << "'");

	if ( ! brokerMessengerCache_->hasMessenger(brokerURL_) )
	{
		try
		{
			brokerMessengerCache_->createMessenger(brokerURL_); // trigger  caching of messenger for the remote endpoint
		}
		catch (b2in::utils::exception::Exception& e)
		{
			LOG4CPLUS_FATAL(this->getOwnerApplication()->getApplicationLogger(), "Failed to create messenger on to broker" << brokerURL_ << ", " << e.what());
			return;
		}
	}

	xdata::Properties plist;
		
	plist.setProperty ("urn:b2in-protocol:service", "xmasbroker2g");
	plist.setProperty ("urn:b2in-protocol-tcp:connection", "close");

	plist.setProperty ("urn:xmasbroker2g:action", "query");
	plist.setProperty ("urn:xmasbroker2g:service", "b2in-eventing");
	plist.setProperty ("urn:xmasbroker2g:model", "eventingpool");
	plist.setProperty ("urn:xmasbroker2g:group", toolbox::printTokenSet(groups_, ","));
	plist.setProperty ("urn:xmasbroker2g:url", brokerMessengerCache_->getLocalAddress()->toString());
	plist.setProperty ("urn:xmasbroker2g:originator", this->getOwnerApplication()->getApplicationDescriptor()->getAttribute("service"));

	try
	{
		brokerCounter_++;
		brokerMessengerCache_->send(brokerURL_,0,plist);
	}
	catch (b2in::nub::exception::InternalError & e)
	{
		LOG4CPLUS_FATAL(this->getOwnerApplication()->getApplicationLogger(), "failed to send query to broker on url " << brokerURL_ << ", " << e.what());
		brokerLostCounter_++;
	}
	catch ( b2in::nub::exception::QueueFull & e )
	{
		// ignore
		brokerLostCounter_++;
		LOG4CPLUS_WARN(this->getOwnerApplication()->getApplicationLogger(), "failed to send query to broker on url " << brokerURL_ << ", " << e.what());
	}
	catch ( b2in::nub::exception::OverThreshold & e)
	{
		// ignore 
		brokerLostCounter_++;
		LOG4CPLUS_WARN(this->getOwnerApplication()->getApplicationLogger(), "failed to send query to broker on url " << brokerURL_ << ", " << e.what());
	}
}

std::string b2in::utils::EventingPoolSubscriber::getBrokerURL()
{
	return brokerURL_;
}

xdata::UnsignedInteger64T b2in::utils::EventingPoolSubscriber::getBrokerCounter()
{
	return (brokerCounter_ - brokerLostCounter_);
}

xdata::UnsignedInteger64T b2in::utils::EventingPoolSubscriber::getBrokerLostCounter()
{
	return brokerLostCounter_;
}

std::set<std::string> b2in::utils::EventingPoolSubscriber::getEventingURLs()
{
	std::set<std::string> eventings;
	mutex_.take();
	eventings = eventingURLs_;
	mutex_.give();
	return eventings;
}

xdata::UnsignedInteger64T b2in::utils::EventingPoolSubscriber::getEventingCounter()
{
	return (subscriptionCounter_ - subscriptionLostCounter_);
}

xdata::UnsignedInteger64T b2in::utils::EventingPoolSubscriber::getEventingLostCounter()
{
	return subscriptionLostCounter_;
}

void b2in::utils::EventingPoolSubscriber::updateTopics(std::set<std::string> & topics)
{
	mutex_.take();

	std::set<std::string> tmp;
	for(std::map<std::string, xdata::Properties>::iterator i = subscriptions_.begin() ; i != subscriptions_.end() ; i++)
	{
		tmp.insert((*i).first);
	}

	// delete old subscriptions
	std::set<std::string> oldtopics = toolbox::stl::difference(tmp, topics);
	for(std::set<std::string>::iterator i = oldtopics.begin() ; i != oldtopics.end() ; i++)
	{
		LOG4CPLUS_DEBUG (this->getOwnerApplication()->getApplicationLogger(), "Remove old subscription record for topic '" << (*i) << "'");
		subscriptions_.erase(*i);
	}

	// add new subscriptions
	std::set<std::string> newtopics = toolbox::stl::difference(topics, tmp);
	for(std::set<std::string>::iterator i = newtopics.begin() ; i != newtopics.end() ; i++)
	{
		LOG4CPLUS_DEBUG (this->getOwnerApplication()->getApplicationLogger(), "Prepare new subscription records for topic '" << (*i) << "'");
		xdata::Properties plist;
		toolbox::net::UUID identifier;
		plist.setProperty("urn:b2in-eventing:action", "subscribe");
		plist.setProperty("urn:b2in-eventing:id", identifier.toString());
		plist.setProperty("urn:b2in-eventing:topic", (*i)); // Subscribe to collected data
		plist.setProperty("urn:b2in-protocol:service", "b2in-eventing"); // for remote dispatching
		plist.setProperty("urn:b2in-eventing:expires", expirationPeriod_.toString("xs:duration"));
		// Subscriber url and service
		plist.setProperty("urn:b2in-eventing:subscriberurl", eventingMessengerCache_->getLocalAddress()->toString());
		plist.setProperty("urn:b2in-eventing:subscriberservice", this->getOwnerApplication()->getApplicationDescriptor()->getAttribute("service"));
		subscriptions_[(*i)] = plist;
	}

	mutex_.give();
}

void b2in::utils::EventingPoolSubscriber::BrokerAsynchronousExceptionNotification::asynchronousExceptionNotification(xcept::Exception& ex)
{
        LOG4CPLUS_DEBUG(parent_.getOwnerApplication()->getApplicationLogger(), "Sending a notification failed, " << ex.what());
        parent_.brokerLostCounter_++;
}

void b2in::utils::EventingPoolSubscriber::EventingAsynchronousExceptionNotification::asynchronousExceptionNotification(xcept::Exception& ex)
{
        LOG4CPLUS_DEBUG(parent_.getOwnerApplication()->getApplicationLogger(), "Sending a notification failed, " << ex.what());
        parent_.subscriptionLostCounter_++;
}

