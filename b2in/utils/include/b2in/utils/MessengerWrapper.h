// $Id: MessengerCache.h,v 1.7 2008/07/18 15:26:35 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _b2in_utils_MessengerWrapper_h_
#define _b2in_utils_MessengerWrapper_h_

#include <vector>
#include <map>
#include <string>
#include "xdaq/ApplicationContext.h"
#include "xdaq/NetGroup.h"
#include "xdaq/Network.h"
#include "xdata/Properties.h"
#include "pt/Messenger.h"
#include "pt/PeerTransportAgent.h"
#include "b2in/utils/exception/Exception.h"
#include "b2in/utils/MessengerWrapperListener.h"

#include "toolbox/BSem.h"
#include "toolbox/lang/Class.h"
#include "toolbox/exception/Handler.h"
#include "b2in/nub/exception/InternalError.h"
#include "b2in/nub/exception/QueueFull.h"
#include "b2in/nub/exception/OverThreshold.h"

#include "toolbox/task/Timer.h"

namespace b2in
{
	namespace utils
	{
		//! This class maintains a cache of messenger pointers for fast access to
		//! the messenger by giving a source and destination tid.
		//
		class MessengerWrapper : public toolbox::lang::Class, public toolbox::task::TimerListener
		{
				xdaq::ApplicationContext* context_;

			public:

				MessengerWrapper (xdaq::ApplicationContext* context, const std::string & networkName, MessengerWrapperListener * listener, const std::string & checkConnectionInerval) throw (b2in::utils::exception::Exception);

				void timeExpired (toolbox::task::TimerEvent& e);

				//!
				//
				void invalidate () throw (b2in::utils::exception::Exception);

				//!
				//

				void createMessenger (const std::string& url) throw (b2in::utils::exception::Exception);

				void send (toolbox::mem::Reference* msg, xdata::Properties & plist, void * cookie) throw (b2in::nub::exception::InternalError, b2in::nub::exception::QueueFull, b2in::nub::exception::OverThreshold);

				bool hasMessenger ();

				pt::Address::Reference getLocalAddress ();

			protected:

				bool subscribeFailed (xcept::Exception& e, void * context);

				pt::Messenger* getMessenger (const std::string& url) throw (b2in::utils::exception::Exception);

				pt::Messenger::Reference messengerReference_;
				pt::Messenger* messenger_;
				toolbox::BSem lock_;
				pt::Address::Reference localAddress_;

				toolbox::exception::HandlerSignature * subscribeFailedHandler_;
				MessengerWrapperListener * listener_;

		};
	}
}

#endif

