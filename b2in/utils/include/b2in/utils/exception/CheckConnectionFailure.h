// $Id: Exception.h,v 1.2 2008/07/18 15:26:38 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _b2in_utils_exception_CheckConnectionFailure_h_
#define _b2in_utils_exception_CheckConnectionFailure_h_

#include "b2in/utils/exception/Exception.h"

namespace b2in
{
	namespace utils
	{
		namespace exception
		{
			class CheckConnectionFailure : public b2in::utils::exception::Exception
			{
				public:
					CheckConnectionFailure (std::string name, std::string message, std::string module, int line, std::string function)
						: b2in::utils::exception::Exception(name, message, module, line, function)
					{
					}
					CheckConnectionFailure (std::string name, std::string message, std::string module, int line, std::string function, xcept::Exception & e)
						: b2in::utils::exception::Exception(name, message, module, line, function, e)
					{
					}
			};
		}
	}
}

#endif
