// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "b2in/nub/Dispatcher.h"
#include "toolbox/mem/Reference.h"
#include "toolbox/string.h"
#include "xdata/exdr/FixedSizeInputStreamBuffer.h"
#include "xdata/exdr/Serializer.h"
#include "xdaq/ApplicationContext.h"
#include "xdaq/ApplicationRegistry.h"
#include "b2in/nub/Method.h"

b2in::nub::Dispatcher::Dispatcher
(
 xdaq::ApplicationContext* context, 
 Logger & logger
)
:logger_( log4cplus::Logger::getInstance( logger.getName() + ".b2in.nub.Dispatcher"))

{
	this->context_  = context;
	this->registry_ = context->getApplicationRegistry();
}

b2in::nub::Dispatcher::~Dispatcher()
{
}

void b2in::nub::Dispatcher::processIncomingMessage 
(
 toolbox::mem::Reference* msg, xdata::Properties& plist
) 
throw (b2in::nub::exception::Exception)
{



	if (plist.hasProperty("urn:b2in-protocol:checkconnection"))
	{
		//std::cout << "b2in::nub::Dispatcher::processIncomingMessage  received check connection message" << std::endl;
		if (msg != 0)
		{
			msg->release();
		}
		return;
	}

	std::vector<xdaq::Application*> applications;

	// dispatch

	try
	{
		// If plist has a lid, iterate through the applications and find by lid
		//
		if (plist.hasProperty("urn:b2in-protocol:lid"))
		{
			size_t lid = toolbox::toUnsignedLong(plist.getProperty("urn:b2in-protocol:lid"));
			std::list<xdaq::Application*>  l = this->registry_->getApplications();
			for ( std::list<xdaq::Application*>::iterator i = l.begin(); i != l.end(); i++ )
			{
				const xdaq::ApplicationDescriptor * d = (*i)->getApplicationDescriptor();
				if ( d->getLocalId() == lid) 
				{ 
					xdaq::Application* application = this->registry_->getApplication(d->getLocalId());
					applications.push_back(application);
					//method = dynamic_cast<b2in::nub::MethodSignature*>(application->getMethod(0));
					break;
				}
			}
		}
		else if (plist.hasProperty("urn:b2in-protocol:service"))
		{
			//std::vector<xdaq::Application*> apps = registry_->getApplications("service", plist.getProperty("urn:b2in-protocol:service"));
			applications = registry_->getApplications("service", plist.getProperty("urn:b2in-protocol:service"));
			/*if (apps.size() != 1)
			{
				std::stringstream msg;
				msg << "Failed to dispatch b2in message to a single application, " << apps.size() << " applications respond to service '";
				msg << plist.getProperty("urn:b2in-protocol:service") << "'";
				// Add all properties to the error message
				std::map<std::string, std::string, std::less<std::string> >& props = plist.getProperties();
				std::map<std::string, std::string, std::less<std::string> >::iterator pi = props.begin();
				while (pi != props.end())
				{
					msg << "(" << (*pi).first << "=" << (*pi).second << ")";
					++pi;
					if (pi != props.end())
					{
						msg << ", ";
					}
				}

				XCEPT_RAISE (b2in::nub::exception::Exception, msg.str());
			}
			else
			{
				method = dynamic_cast<b2in::nub::MethodSignature*>(apps[0]->getMethod(0));
			}
			*/


		}
		else
		{
			std::stringstream msg;
			msg << "Missing information in incomming b2in message to dispatch";
			XCEPT_RAISE (b2in::nub::exception::Exception, msg.str());
		}
	}
	catch (xdaq::exception::ApplicationNotFound& e)
	{
		//ref->release();
		XCEPT_RETHROW(b2in::nub::exception::Exception, "Failed to process incoming B2IN  message", e);
	}
	catch (xdaq::exception::ApplicationDescriptorNotFound& e)
	{
		//ref->release();
		XCEPT_RETHROW(b2in::nub::exception::Exception, "Failed to process incoming B2IN  message", e);
	}
	
	if (applications.size() == 0)
	{
		std::stringstream msg;
		msg << "No application found for delivering of B2IN  message, ";
		msg << " no applications respond to service '";
		msg << plist.getProperty("urn:b2in-protocol:service") << "'";
		// Add all properties to the error message
		//std::map<std::string, std::string, std::less<std::string> >& props = plist.getProperties();
		std::map<std::string, std::string, std::less<std::string> >::iterator pi = plist.begin();
		while (pi != plist.end())
		{
			msg << "(" << (*pi).first << "=" << (*pi).second << ")";
			++pi;
			if (pi != plist.end())
			{
				msg << ", ";
			}
		}
		XCEPT_RAISE(b2in::nub::exception::Exception, msg.str());
	}

	// split properties and payload
	toolbox::mem::Reference * properties = msg;
	toolbox::mem::Reference * payload = 0;
	if ( msg != 0 )
	{
		payload = msg->getNextReference();
		msg->setNextReference(0);
	}

	for (std::vector<xdaq::Application*>::iterator i = applications.begin(); i != applications.end(); i++ )
	{
		b2in::nub::MethodSignature* method = 0;
		method = dynamic_cast<b2in::nub::MethodSignature*>((*i)->getMethod(0));


		if (method == 0)
		{
			if ( payload != 0 )
			{
				payload->release();
			}
			std::stringstream msg;
			msg << "No callback method found for incoming B2IN  message";
			XCEPT_RAISE(b2in::nub::exception::Exception, msg.str());
		}

		// duplicating reference counting for multiple delivery
		toolbox::mem::Reference* duplicated = 0;
		if ( payload != 0 )
		{
			duplicated = payload->duplicate();
			if ( duplicated == 0 )
			{
				if ( payload != 0 )
						payload->release();
				std::stringstream errmsg;
				errmsg << "failed to duplicate reference for dispatching";
				XCEPT_RAISE (b2in::nub::exception::Exception, errmsg.str());
			}
		}


		try
		{
			method->invoke(duplicated, plist);

		}
		catch (b2in::nub::exception::Exception& e)
		{
			if ( payload != 0 )
				payload->release();

			if ( duplicated != 0 )
				duplicated->release();

			std::stringstream msg;
			msg << "Exception occured during B2IN method dispatch";
			XCEPT_RETHROW (b2in::nub::exception::Exception, msg.str(), e);
		}
		catch (...)
		{
			if ( payload != 0 )
				payload->release();

			if ( duplicated != 0 )
				duplicated->release();
			std::stringstream  msg;
			msg << "Caught unhandled exception raised in user program during B2in method dispatch";
			XCEPT_RAISE (b2in::nub::exception::Exception, msg.str());
		}

	}

	if ( payload != 0 )
	{
		payload->release();
	}

	if ( properties != 0 )
	{
		properties->release();
	}

}


