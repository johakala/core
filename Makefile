# $Id$

#########################################################################
# XDAQ Components for Distributed Data Acquisition                      #
# Copyright (C) 2000-2019, CERN.                                        #
# All rights reserved.                                                  #
# Authors: L. Orsini and D. Simelevicius                                #
#                                                                       #
# For the licensing terms see LICENSE.                                  #
# For the list of contributors see CREDITS.                             #
#########################################################################

##
#
# Project level Makefile
#
##

BUILD_HOME:=$(shell pwd)

ifndef PACKAGES
PACKAGES= extern/asyncresolv \
	extern/i2o \
	extern/xerces \
	extern/log4cplus \
	extern/cgicc \
	extern/xalan \
	extern/mimetic \
	extern/slp \
	extern/pstreams \
	extern/jansson \
	extern/sqlite \
	extern/highcharts \
	build \
	config \
	xcept \
	log/udpappender \
	log/xmlappender \
	toolbox \
	xoap \
	xoap/filter \
	xdata \
	pt \
	xgi \
	i2o \
	xdaq \
	i2o/utils \
	pt/http \
	pt/fifo \
	executive \
	hyperdaq \
	xrelay \
	b2in/nub \
	b2in/utils \
	b2in/eventing \
	xplore/utils \
	xslp \
	xplore \
	ws/addressing \
	benchmark/roundtrip \
	benchmark/mstreamio2g \
	xmem/probe \
	tcpla \
	pt/tcp \
	pt/utcp \
	pt/blit \
	xaccess \
	tracer/timeline \
	tracer/probe \
	tracer/tracerd \
	eventing/core \
	eventing/api \
	eventing/duple
endif
export PACKAGES

ifndef BUILD_SUPPORT
BUILD_SUPPORT=build
endif
export BUILD_SUPPORT

ifndef MFDEFS_SUPPORT
MFDEFS_SUPPORT=1
endif
export MFDEFS_SUPPORT

ifndef PROJECT_NAME
PROJECT_NAME=$(shell pwd | awk -F"/" '{split($$0,a,"/");  print a[NF]}')
endif
export PROJECT_NAME

include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfAutoconf.rules

Project=$(PROJECT_NAME)

include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfDefs.$(XDAQ_OS)

Packages=$(PACKAGES)

ifdef Set
include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfSet.${Set}
endif

include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/Makefile.rules
include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfRPM.rules

