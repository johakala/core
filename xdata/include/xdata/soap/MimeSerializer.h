// $Id: $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.			                			 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini and A. Forrest					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                        	 *
 * For the list of contributors see CREDITS.   			        		 *
 *************************************************************************/

#ifndef _xdata_soap_MimeSerializer_h_
#define _xdata_soap_MimeSerializer_h_

#include "xdata/soap/ObjectSerializer.h"

namespace xdata
{
	namespace soap
	{

		class MimeSerializer : public xdata::soap::ObjectSerializer
		{
			public:
				virtual ~MimeSerializer ();

				std::string type () const;
				DOMElement * exportAll (xdata::Serializer * serializer, xdata::Serializable * serializable, DOMElement * targetNode);
				void exportQualified (xdata::Serializer * serializer, xdata::Serializable * serializable, DOMNode* queryNode, DOMElement* resultNode) throw (xdata::exception::Exception);
				void import (xdata::Serializer * serializer, xdata::Serializable * serializable, DOMNode* targetNode) throw (xdata::exception::Exception);
		};

	}
}

#endif
