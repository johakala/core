// $Id: DeclarationTestSuite.cc,v 1.6 2008/07/18 15:28:14 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "DeclarationTestSuite.h"
#include "TestBag.h"

CPPUNIT_TEST_SUITE_REGISTRATION( DeclarationTestSuite );

void DeclarationTestSuite::setUp()
{
}

void DeclarationTestSuite::tearDown()
{
}


void DeclarationTestSuite::declareInteger()
{
	xdata::Integer i;
}
void DeclarationTestSuite::declareFloat()
{
	xdata::Float f;
	
}
void DeclarationTestSuite::declareUnsignedLong()
{
	xdata::UnsignedLong ul;
}
void DeclarationTestSuite::declareUnsignedShort()
{
	xdata::UnsignedLong us;
}

void DeclarationTestSuite::declareBag()
{
	//xdata::Bag b;
	BagByInheritance b;
	b.f();
}

void DeclarationTestSuite::declareVectorOfInteger()
{
	xdata::Vector<xdata::Integer> iv;
}

void DeclarationTestSuite::declareVectorOfBag()
{
	// xdata::Vector<xdata::Bag> bv;
}

void DeclarationTestSuite::declareVectorOfVectorOfString()
{
	// the vector cannot accept sub type as pointers but only valid xdata objects
	xdata::Vector<xdata::Vector<xdata::String> > svv;
}


