// $Id: TestBag.h,v 1.5 2008/07/18 15:28:14 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _TestBag_h_
#define _TestBag_h_


#include "xdata/Bag.h"

class TestEmptyBag
{
	public:	
	void registerFields(xdata::Bag<TestEmptyBag> * bag)
	{	
	}
	
      

};

class TestBag
{
	public:	
	void registerFields(xdata::Bag<TestBag> * bag)
	{		
		bag->addField("a", &a);
                bag->addField("b", &b);
		bag->addField("c", &c);
                bag->addField("d", &d);	
	}
	
        xdata::Integer a;
        xdata::Float b;
	xdata::Boolean c;
        xdata::String d;

};

class TestBagWithVector
{
	public:	
	void registerFields(xdata::Bag<TestBagWithVector> * bag)
	{		
		bag->addField("v", &v);
                
	}
	
        xdata::Vector<xdata::Integer> v;

};

class TestBagWithBag
{
	public:	
	void registerFields(xdata::Bag<TestBagWithBag> * bag)
	{		
                bag->addField("b", &b);
	}
	
        xdata::Bag<TestBag> b;

};

class AnotherTestBag
{
	public:	
	void registerFields(xdata::Bag<AnotherTestBag> * bag)
	{		
		bag->addField("i", &a);
                bag->addField("j", &b);
		bag->addField("k", &c);
                bag->addField("l", &d);	
	}
	
        xdata::Integer a;
        xdata::UnsignedLong b;
	xdata::String c;
        xdata::UnsignedShort d;

};

// Same as TestBag, but one field (last) is different
class YetAnotherTestBag
{
	public:	
	void registerFields(xdata::Bag<YetAnotherTestBag> * bag)
	{		
		bag->addField("a", &a);
                bag->addField("b", &b);
		bag->addField("c", &c);
                bag->addField("d", &d);	
	}
	
        xdata::Integer a;
        xdata::Float b;
	xdata::Boolean c;
        xdata::UnsignedShort d;

};


// Inheritance Bag
class BagByInheritance: public xdata::Bag<YetAnotherTestBag>
{
	public:
	
	void f()
	{
		xdata::Integer b = this->bag.a;
	}
};

#endif
