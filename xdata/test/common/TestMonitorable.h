// $Id: TestMonitorable.h,v 1.3 2008/07/18 15:28:14 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _TestMonitorable_h_
#define _TestMonitorable_h_


#include "xdata/Monitorable.h"

class TestEmptyMonitorable
{
	public:	
	void registerFields(xdata::Monitorable<TestEmptyMonitorable> * monitorable)
	{	
	}
	
      

};

class TestMonitorable
{
	public:	
	void registerFields(xdata::Monitorable<TestMonitorable> * monitorable)
	{		
		monitorable->addUserField("a", &a);
                monitorable->addUserField("b", &b);
		monitorable->addUserField("c", &c);
                monitorable->addUserField("d", &d);	
	}
	
        xdata::Integer a;
        xdata::Float b;
	xdata::Boolean c;
        xdata::String d;

};

class MyBag
{
	public:	
	void registerFields(xdata::Bag<MyBag> * monitorable)
	{		
		monitorable->addField("a", &a);
                monitorable->addField("b", &b);
		monitorable->addField("c", &c);
                monitorable->addField("d", &d);	
	}
	
        xdata::Integer a;
        xdata::Float b;
	xdata::Boolean c;
        xdata::String d;

};

class TestMonitorableWithVector
{
	public:	
	void registerFields(xdata::Monitorable<TestMonitorableWithVector> * monitorable)
	{		
		monitorable->addUserField("v", &v);
                
	}
	
        xdata::Vector<xdata::Integer> v;

};

class TestMonitorableWithBag
{
	public:	
	void registerFields(xdata::Monitorable<TestMonitorableWithBag> * monitorable)
	{		
                monitorable->addUserField("b", &b);
	}
	
        xdata::Bag<MyBag> b;

};

class AnotherTestMonitorable
{
	public:	
	void registerFields(xdata::Monitorable<AnotherTestMonitorable> * monitorable)
	{		
		monitorable->addUserField("i", &a);
                monitorable->addUserField("j", &b);
		monitorable->addUserField("k", &c);
                monitorable->addUserField("l", &d);	
	}
	
        xdata::Integer a;
        xdata::UnsignedLong b;
	xdata::String c;
        xdata::UnsignedShort d;

};

// Same as TestMonitorable, but one field (last) is different
class YetAnotherTestMonitorable
{
	public:	
	void registerFields(xdata::Monitorable<YetAnotherTestMonitorable> * monitorable)
	{		
		monitorable->addUserField("a", &a);
                monitorable->addUserField("b", &b);
		monitorable->addUserField("c", &c);
                monitorable->addUserField("d", &d);	
	}
	
        xdata::Integer a;
        xdata::Float b;
	xdata::Boolean c;
        xdata::UnsignedShort d;

};


// Inheritance Monitorable
class MonitorableByInheritance: public xdata::Monitorable<YetAnotherTestMonitorable>
{
	public:
	
	void f()
	{
		xdata::Integer b = this->fields.a;
	}
};


class TestMonitorableWithPropertiesOnly: public xdata::Monitorable<>
{
	public:
		TestMonitorableWithPropertiesOnly()
		{
			this->setProperty("description", "This is a property");
			this->setProperty("format", "text");
		}
};

class TestMonitorableWithPropertiesAndFields: public xdata::Monitorable<TestMonitorable>
{
	public:
		TestMonitorableWithPropertiesAndFields()
		{
			this->setProperty("description", "This is a property");
			this->setProperty("format", "text");
		}
};



#endif
