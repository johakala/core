// $Id: InfoSpaceTestSuite.h,v 1.3 2008/07/18 15:28:14 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _InfoSpaceTestSuite_h_
#define _InfoSpaceTestSuite_h_

#include <cppunit/extensions/HelperMacros.h>

#include "xdata/InfoSpace.h"
#include "xdata/Integer.h"

/* 
 * A test case that is designed to produce
 * example errors and failures
 *
 */

class InfoSpaceTestSuite : public CPPUNIT_NS::TestFixture
{
	CPPUNIT_TEST_SUITE( InfoSpaceTestSuite );
	
	CPPUNIT_TEST( GetNewInfoSpace );
	CPPUNIT_TEST( ItemFind );	
	CPPUNIT_TEST( ItemNotFound);
	CPPUNIT_TEST( GetExistingInfoSpace );
	CPPUNIT_TEST( DestroyInfoSpace );
	CPPUNIT_TEST( DestroyInfoSpaceWrongName );
	
	CPPUNIT_TEST( FireItemAvailableTwice );	
	CPPUNIT_TEST( ItemAvailableEventNotification );
	
	CPPUNIT_TEST( FireItemRevoked );
	CPPUNIT_TEST( FireItemRevokedTwice );	
	CPPUNIT_TEST( ItemRevokedEventNotification );
	
	CPPUNIT_TEST( RemoveAvailableListener );
	CPPUNIT_TEST( RemoveAvailableListenerWrongListener );
	
	CPPUNIT_TEST( RemoveRevokedListener );
	CPPUNIT_TEST( RemoveRevokedListenerWrongListener );

	CPPUNIT_TEST( FireItemValueChanged );
	CPPUNIT_TEST( FireItemValueChangedWrongName );	

	CPPUNIT_TEST( FireItemValueRetrieve );
	CPPUNIT_TEST( FireItemValueRetrieveWrongName );	

	CPPUNIT_TEST( RemoveItemChangeListener );
	CPPUNIT_TEST( RemoveItemChangeListenerWrongName );
	CPPUNIT_TEST( RemoveItemChangeListenerWrongListener );	

	CPPUNIT_TEST( RemoveItemRetrieveListener );
	CPPUNIT_TEST( RemoveItemRetrieveListenerWrongName );
	CPPUNIT_TEST( RemoveItemRetrieveListenerWrongListener );	

	CPPUNIT_TEST_SUITE_END();

protected:
  xdata::Integer * testInteger;
  xdata::ActionListener * integerListener;
  
public:
  void setUp();
  void tearDown();

protected:

	// Done
	void GetNewInfoSpace();
	void ItemFind();	
	void ItemNotFound();
	void GetExistingInfoSpace();
	void DestroyInfoSpace();
	void DestroyInfoSpaceWrongName();

	void FireItemAvailableTwice();
	void ItemAvailableEventNotification();
	  
	void FireItemRevoked(); 
	void ItemRevokedEventNotification(); 
	
	// This tests revoked twice and revoke of name not existing
	void FireItemRevokedTwice();
	
	void RemoveAvailableListener();
	void RemoveAvailableListenerWrongListener();
	 
	void RemoveRevokedListener();
	void RemoveRevokedListenerWrongListener();
	
	void FireItemValueChanged();
	void FireItemValueChangedWrongName();

	void FireItemValueRetrieve();
	void FireItemValueRetrieveWrongName();	
	
	void RemoveItemChangeListener();
	void RemoveItemChangeListenerWrongName();
	void RemoveItemChangeListenerWrongListener();
	
	void RemoveItemRetrieveListener();
	void RemoveItemRetrieveListenerWrongName();
	void RemoveItemRetrieveListenerWrongListener();
};


#endif
