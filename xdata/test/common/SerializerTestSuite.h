// $Id: SerializerTestSuite.h,v 1.3 2008/07/18 15:28:14 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef SerializerTestSuite_H
#define SerializerTestSuite_H

#include <cppunit/extensions/HelperMacros.h>

#include "xdata/xdata.h"
#include "xdata/XMLDOM.h"

/* 
 * A test case that is designed to stimulate 
 * significant compiler template instantiation 
 *
 */

class SerializerTestSuite : public CPPUNIT_NS::TestFixture
{
  CPPUNIT_TEST_SUITE( SerializerTestSuite );
  CPPUNIT_TEST( loader );
  CPPUNIT_TEST_SUITE_END();

protected:
  
public:
  void setUp();
  void tearDown();

protected:

  void loader();
  

};


#endif
