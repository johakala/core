#include "EXDRSerializerTestSuite.h"
#include <limits>
#include "xdata/Serializable.h"
#include "xdata/exdr/Serializer.h"
#include "xdata/exception/Exception.h"
#include "xdata/XStr.h"
#include "xdata/Float.h"
#include "xdata/Integer.h"
#include "xdata/UnsignedInteger.h"
#include "xdata/Integer32.h"
#include "xdata/UnsignedInteger32.h"
#include "xdata/Integer64.h"
#include "xdata/UnsignedInteger64.h"
#include "xdata/Boolean.h"
#include "xdata/String.h"
#include "xdata/Bag.h"
#include "xdata/Table.h"
#include "xdata/TableIterator.h"
#include "TestBag.h"

#include "xdata/exdr/FixedSizeInputStreamBuffer.h"
#include "xdata/exdr/FixedSizeOutputStreamBuffer.h"
#include "xdata/exdr/Serializer.h"

CPPUNIT_TEST_SUITE_REGISTRATION( EXDRSerializerTestSuite );

void EXDRSerializerTestSuite::setUp()
{
 	
	
}

void EXDRSerializerTestSuite::tearDown()
{
	
}

void EXDRSerializerTestSuite::process(xdata::Serializable * in, xdata::Serializable * out) 
throw (CppUnit::Exception, xdata::exception::Exception::Exception)
{	
	xdata::exdr::Serializer serializer;
	
	// Allocate 8KB for the buffer - sufficient for the tests
	char buf[8192];
	int size = 8192;
		
	xdata::exdr::FixedSizeOutputStreamBuffer outBuffer(buf, size);
	
	serializer.exportAll (in, &outBuffer);	
	
	xdata::exdr::FixedSizeInputStreamBuffer	 inBuffer(buf, size);
	
	serializer.import(out, &inBuffer);
}

void EXDRSerializerTestSuite::process_large(xdata::Serializable * in, xdata::Serializable * out) 
throw (CppUnit::Exception, xdata::exception::Exception::Exception)
{	
	xdata::exdr::Serializer serializer;
	
	// Allocate 8KB for the buffer - sufficient for the tests
	char buf[1024*1024];
	int size = 1024*1024;
		
	xdata::exdr::FixedSizeOutputStreamBuffer outBuffer(buf, size);
	
	serializer.exportAll (in, &outBuffer);	
	
	xdata::exdr::FixedSizeInputStreamBuffer	 inBuffer(buf, size);
	
	serializer.import(out, &inBuffer);
}


//
// Tests for Integer
//

void EXDRSerializerTestSuite::serializeInteger()
{
	std::numeric_limits<xdata::IntegerT> traits;
	xdata::Integer in = traits.min();
	xdata::Integer out;
	
	this->process(&in,&out);
	CPPUNIT_ASSERT ( in == out );
}

void EXDRSerializerTestSuite::serializeIntegerNaN()
{
	xdata::Integer in = std::numeric_limits<xdata::Integer>::quiet_NaN();
	CPPUNIT_ASSERT (in.isNaN());
	xdata::Integer out = std::numeric_limits<xdata::Integer>::min();	
	CPPUNIT_ASSERT (out.isFinite());	
	this->process(&in,&out);
	CPPUNIT_ASSERT ( out.isNaN() );
}

void EXDRSerializerTestSuite::serializeIntegerInfinite()
{
	xdata::Integer in = std::numeric_limits<xdata::Integer>::min();
	--in;
	CPPUNIT_ASSERT (in.isInf());
	xdata::Integer out = std::numeric_limits<xdata::Integer>::max();	
	CPPUNIT_ASSERT (out.isFinite());	
	this->process(&in,&out);
	CPPUNIT_ASSERT ( out.isInf() );
}

void EXDRSerializerTestSuite::serializeUnsignedInteger()
{
	std::numeric_limits<xdata::UnsignedIntegerT> traits;
	xdata::Integer in = traits.max();
	xdata::Integer out;
	
	this->process(&in,&out);
	CPPUNIT_ASSERT ( in == out );
}

void EXDRSerializerTestSuite::serializeInteger32()
{
	std::numeric_limits<xdata::Integer32T> traits;
	xdata::Integer in = traits.min();
	xdata::Integer out;
	
	this->process(&in,&out);
	CPPUNIT_ASSERT ( in == out );
}

void EXDRSerializerTestSuite::serializeInteger64()
{
	std::numeric_limits<xdata::Integer64T> traits;
	xdata::Integer in = traits.min();
	xdata::Integer out;
	
	this->process(&in,&out);
	CPPUNIT_ASSERT ( in == out );
}

void EXDRSerializerTestSuite::serializeUnsignedInteger32()
{
	std::numeric_limits<xdata::UnsignedInteger32T> traits;
	xdata::Integer in = traits.max();
	xdata::Integer out;
	
	this->process(&in,&out);
	CPPUNIT_ASSERT ( in == out );
}

void EXDRSerializerTestSuite::serializeUnsignedInteger64()
{
	std::numeric_limits<xdata::UnsignedInteger64T> traits;
	xdata::Integer in = traits.max();
	xdata::Integer out;
	
	this->process(&in,&out);
	CPPUNIT_ASSERT ( in == out );
}

void EXDRSerializerTestSuite::serializeIntegerWrongTag()
{
	xdata::Integer in = 3;
	xdata::Float out;

	// Import into a float -> comparison of tags fails, wrong format	
	CPPUNIT_ASSERT_THROW (this->process(&in,&out), xdata::exception::Exception);
}

void EXDRSerializerTestSuite::serializeIntegerExportBufferOverflow()
{
	xdata::exdr::Serializer serializer;
	
	char buf[2];
	int size = 2;
		
	xdata::exdr::FixedSizeOutputStreamBuffer outBuffer(buf, size);
	
	xdata::Integer in = 3;
	CPPUNIT_ASSERT_THROW (serializer.exportAll (&in, &outBuffer),  xdata::exception::Exception);
}


void EXDRSerializerTestSuite::serializeIntegerImportBufferOverflow()
{
	xdata::exdr::Serializer serializer;
	
	char buf[8];
	int size = 8;
		
	xdata::exdr::FixedSizeOutputStreamBuffer outBuffer(buf, size);
	
	xdata::Integer in = 3;
	serializer.exportAll (&in, &outBuffer);
	
	// Too short buffer given for input stream
	xdata::exdr::FixedSizeInputStreamBuffer inBuffer(buf, size-2);
	
	CPPUNIT_ASSERT_THROW (serializer.import (&in, &inBuffer),  xdata::exception::Exception);
}




//
// Tests for Float
//

void EXDRSerializerTestSuite::serializeFloat()
{
	xdata::Float in = 3.1415927;
	xdata::Float out;
	
	this->process(&in,&out);
		
	CPPUNIT_ASSERT ( in == out );	
}

void EXDRSerializerTestSuite::serializeFloatNaN()
{
	xdata::Float in = std::numeric_limits<xdata::Float>::quiet_NaN();
	CPPUNIT_ASSERT (in.isNaN());
	xdata::Float out = std::numeric_limits<xdata::Float>::min();	
	CPPUNIT_ASSERT (out.isFinite());	
	this->process(&in,&out);
	CPPUNIT_ASSERT ( out.isNaN() );
}

void EXDRSerializerTestSuite::serializeFloatInfinite()
{
	xdata::Float in = std::numeric_limits<xdata::Float>::infinity();
	CPPUNIT_ASSERT (in.isInf());
	xdata::Float out = std::numeric_limits<xdata::Float>::max();	
	CPPUNIT_ASSERT (out.isFinite());	
	this->process(&in,&out);
	CPPUNIT_ASSERT ( out.isInf() );
}

//
// Tests for Boolean
//

void EXDRSerializerTestSuite::serializeBoolean()
{	
	xdata::Boolean in = false;
	xdata::Boolean out;
	
	this->process(&in,&out);
		
	CPPUNIT_ASSERT ( in == out );	
}

void EXDRSerializerTestSuite::serializeBooleanNaN()
{
	xdata::Boolean in = std::numeric_limits<xdata::Boolean>::quiet_NaN();
	CPPUNIT_ASSERT (in.isNaN());
	xdata::Boolean out = false;	
	CPPUNIT_ASSERT (out.isFinite());	
	this->process(&in,&out);
	CPPUNIT_ASSERT ( out.isNaN() );
}


//
// Tests forString
//

void EXDRSerializerTestSuite::serializeString()
{
	xdata::String in = "";
	xdata::String out;

	// Make s string of 1000000 characters

	std::stringstream m;
	for (int i = 0; i < 1000000; i++)
	{
		m << "x";
	}

	in = m.str();
	
	this->process_large(&in,&out);
		
	CPPUNIT_ASSERT ( in == out );	
}



void EXDRSerializerTestSuite::serializeStringTooLong()
{
	// Make s string of 5000 characters
	std::string s = "";
	for (int i = 0; i < 5000; i++)
	{
		s += "x";
	}
	
	xdata::String in = s;
	
	// Make a buffer that could contain the string
	char buf[4096];
	xdata::exdr::FixedSizeOutputStreamBuffer outBuffer(buf, 4096);
	
	xdata::exdr::Serializer serializer;
	CPPUNIT_ASSERT_THROW (serializer.exportAll (&in, &outBuffer),  xdata::exception::Exception);	
}

void EXDRSerializerTestSuite::deserializeStringTooLong()
{
	// Make s string of 3000 characters
	std::string s = "";
	for (int i = 0; i < 3000; i++)
	{
		s += "x";
	}
	
	char buf[4096];
	XDR xdr;	
	xdrmem_create (&xdr, buf, 4096, XDR_ENCODE);
	
	// put the tag
	unsigned long tag = xdata::exdr::Serializer::String;
	xdr_u_long (&xdr, &tag);
	
	// put the string
	char* tmp = (char*) s.c_str();
	xdr_wrapstring (&xdr, &tmp);
	
	// Make a buffer that cannot contain the string
	xdata::exdr::FixedSizeInputStreamBuffer inBuffer(buf, 1024);
	
	xdata::exdr::Serializer serializer;
	xdata::String in;
	CPPUNIT_ASSERT_THROW (serializer.import (&in, &inBuffer),  xdata::exception::Exception);	
}


//
// Tests for Vector
//


void EXDRSerializerTestSuite::serializeVector()
{
	xdata::Vector<xdata::Integer> in;
	xdata::Vector<xdata::Integer> out;
	
	// Fill a vector with values
	for (int i = 0; i < 100; i++)
	{
		in.push_back(i);
	}
	
	this->process(&in,&out);
		
	CPPUNIT_ASSERT ( in == out );	
}

void EXDRSerializerTestSuite::serializeVectorofVectorInteger()
{
	xdata::Vector<xdata::Vector<xdata::Integer> > in;
	xdata::Vector<xdata::Vector<xdata::Integer> > out;
	
	// Fill a vector with values: diagnoal matrix
	in.resize(10);
	for (int j = 0; j < 10; j++)
	{		
		for (int i = 0; i < j; i++)
		{
			in[j].push_back(i);
		}
	}
	
	this->process(&in,&out);
		
	CPPUNIT_ASSERT ( in == out );	
}

void EXDRSerializerTestSuite::serializeVectorofVectorofBag()
{
	xdata::Vector<xdata::Vector<xdata::Bag<TestBag> > > in;
	xdata::Vector<xdata::Vector<xdata::Bag<TestBag> > > out;
	
	// Fill a vector with values: diagnoal matrix
	xdata::Bag<TestBag> b;
	
	b.bag.a = 11;
	b.bag.b = 3.1459265;
	b.bag.c = false;
	b.bag.d = "string";
	
	
	in.resize(5);
	for (int j = 0; j < 5; j++)
	{		
		for (int i = 0; i < j; i++)
		{
			//b.bag.a = i;
			in[j].push_back(b);
		}
	}
	
	
	this->process(&in,&out);
		
	CPPUNIT_ASSERT ( in == out );	
}


//
// Test for Monitorable
//
/*
void EXDRSerializerTestSuite::serializeMonitorableNoPropertiesNoFields()
{
	xdata::Monitorable<> in;
	xdata::Monitorable<> out;
	
	this->process(&in,&out);	
	
	CPPUNIT_ASSERT ( in == out );
}

void EXDRSerializerTestSuite::serializeMonitorablePropertiesNoFields()
{
	xdata::Monitorable<> in;
	xdata::Monitorable<> out;
	//xdata::Monitorable<> junk;
	
	in.setProperty("description", "I am a property");
	in.setProperty("type", "I am a type");
	
	//junk.setProperty("pippo", "ciccio");
	
	this->process(&in,&out);
	
	CPPUNIT_ASSERT ( in == out );
}

void EXDRSerializerTestSuite::serializeMonitorablePropertiesFields()
{
	xdata::Monitorable<TestMonitorable> in;
	xdata::Monitorable<TestMonitorable> out;
	
	in.setProperty("description", "I am a property");
	in.setProperty("type", "I am a type");
	
	in.fields.a = 11;
	in.fields.b = 3.1459265;
	in.fields.c = false;
	in.fields.d = "This is a test string";
	
	this->process(&in,&out);
	
	CPPUNIT_ASSERT ( in == out );
}
*/

//
// Tests for Bag
//

void EXDRSerializerTestSuite::serializeBag()
{
	xdata::Bag<TestBag> in;
	xdata::Bag<TestBag> out;
	
	in.bag.a = 11;
	in.bag.b = 3.1459265;
	in.bag.c = false;
	in.bag.d = "This is a test string";
	
	this->process(&in,&out);
	
	
	CPPUNIT_ASSERT ( in == out );
}

/*
void EXDRSerializerTestSuite::serializeBagWrongTag()
{
	xdata::Bag<TestBag> b;
	CPPUNIT_ASSERT_THROW( this->validate("../../data/BagWrongTag.xml", &b, importedXML, exportedXML), xdata::exception::Exception );
}


void EXDRSerializerTestSuite::serializeBagWrongValue()
{
	xdata::Bag<TestBag> b;
	CPPUNIT_ASSERT_THROW( this->validate("../../data/BagWrongValue.xml", &b, importedXML, exportedXML), xdata::exception::Exception );
}


void EXDRSerializerTestSuite::serializeBagMissingType()
{
	xdata::Bag<TestBag> b;
	CPPUNIT_ASSERT_THROW( this->validate("../../data/BagMissingType.xml", &b, importedXML, exportedXML), xdata::exception::Exception );
}


void EXDRSerializerTestSuite::serializeBagWrongType()
{
	xdata::Bag<TestBag> b;
	CPPUNIT_ASSERT_THROW( this->validate("../../data/BagWrongType.xml", &b, importedXML, exportedXML), xdata::exception::Exception );
}

void EXDRSerializerTestSuite::serializeBagMissingFieldName()
{
	xdata::Bag<TestBag> b;
	CPPUNIT_ASSERT_THROW( this->validate("../../data/BagMissingFieldName.xml", &b, importedXML, exportedXML), xdata::exception::Exception );
}

void EXDRSerializerTestSuite::serializeBagWrongFieldType()
{
	xdata::Bag<TestBag> b;
	CPPUNIT_ASSERT_THROW( this->validate("../../data/BagMissingFieldType.xml", &b, importedXML, exportedXML), xdata::exception::Exception );
}

void EXDRSerializerTestSuite::serializeBagWrongElement()
{
	xdata::Bag<TestBag> b;
	CPPUNIT_ASSERT_THROW( this->validate("../../data/BagWrongElement.xml", &b, importedXML, exportedXML) , xdata::exception::Exception);
}
*/
void EXDRSerializerTestSuite::serializeBagEmpty()
{
	xdata::Bag<TestEmptyBag> in;
	xdata::Bag<TestEmptyBag> out;
	
	this->process(&in,&out);
	
	CPPUNIT_ASSERT ( in == out );
}



void EXDRSerializerTestSuite::serializeBagContentMismatchFieldName()
{
	xdata::Bag<TestBag> in;
	xdata::Bag<AnotherTestBag> out;
	
	in.bag.a = 11;
	in.bag.b = 3.1459265;
	in.bag.c = false;
	in.bag.d = "This is a test string";
	
	CPPUNIT_ASSERT_THROW (this->process(&in,&out), xdata::exception::Exception);
}

void EXDRSerializerTestSuite::serializeBagContentMismatchFieldType()
{
	xdata::Bag<TestBag> in;
	xdata::Bag<YetAnotherTestBag> out;
	
	in.bag.a = 11;
	in.bag.b = 3.1459265;
	in.bag.c = false;
	in.bag.d = "This is a test string";
	
	CPPUNIT_ASSERT_THROW (this->process(&in,&out), xdata::exception::Exception);
}

void EXDRSerializerTestSuite::serializeBagWithVector()
{
	xdata::Bag<TestBagWithVector> in;
	xdata::Bag<TestBagWithVector> out;
	
	// fill vector
	for (int i = 0; i < 100; i++)
	{
		in.bag.v.push_back(i);
	}
	
	this->process(&in,&out);
	
	CPPUNIT_ASSERT ( in == out );
	
}


void EXDRSerializerTestSuite::serializeBagWithBag()
{
	xdata::Bag<TestBagWithBag> in;
	xdata::Bag<TestBagWithBag> out;
	
	in.bag.b.bag.a = 11;
	in.bag.b.bag.b = 3.1459265;
	in.bag.b.bag.c = false;
	in.bag.b.bag.d = "This is a test string";
	
	this->process(&in,&out);
	
	CPPUNIT_ASSERT ( in == out );
}

void EXDRSerializerTestSuite::serializeVectorWithBag()
{
	xdata::Vector<xdata::Bag<TestBag> > in;
	xdata::Vector<xdata::Bag<TestBag> > out;
	
	// fill the vector
	xdata::Bag<TestBag> b;
	b.bag.a = 11;
	b.bag.b = 3.1459265;
	b.bag.c = false;
	b.bag.d = "This is a test string";
	
	for (int i = 0; i < 10; i++)
	{
		in.push_back (b);
	}
	
	this->process(&in,&out);
	
	CPPUNIT_ASSERT ( in == out );	
}


//
// Tests for UnsignedShort
//

void EXDRSerializerTestSuite::serializeUnsignedShort()
{
	xdata::UnsignedShort in = 2;
	xdata::UnsignedShort out;
		
	this->process(&in,&out);
	CPPUNIT_ASSERT ( in == out );
}

/*
void EXDRSerializerTestSuite::serializeUnsignedShortWrongTag()
{
	xdata::UnsignedShort  v;
	CPPUNIT_ASSERT_THROW( this->validate("../../data/UnsignedShortWrongTag.xml", &v, importedXML, exportedXML) , xdata::exception::Exception);
}

void EXDRSerializerTestSuite::serializeUnsignedShortWrongValue()
{
	xdata::UnsignedShort v;
	CPPUNIT_ASSERT_THROW( this->validate("../../data/UnsignedShortWrongValue.xml", &v, importedXML, exportedXML), xdata::exception::Exception );
}


void EXDRSerializerTestSuite::serializeUnsignedShortMissingType()
{
	xdata::UnsignedShort v;
	CPPUNIT_ASSERT_THROW( this->validate("../../data/UnsignedShortMissingType.xml", &v, importedXML, exportedXML) , xdata::exception::Exception);
}

void EXDRSerializerTestSuite::serializeUnsignedShortMissingValue()
{
	xdata::UnsignedShort v;
	CPPUNIT_ASSERT_THROW( this->validate("../../data/UnsignedShortMissingValue.xml", &v, importedXML, exportedXML), xdata::exception::Exception );
}

void EXDRSerializerTestSuite::serializeUnsignedShortWrongType()
{
	xdata::UnsignedShort v;
	CPPUNIT_ASSERT_THROW( this->validate("../../data/UnsignedShortWrongType.xml", &v, importedXML, exportedXML), xdata::exception::Exception );
}
*/

//
// Tests for UnsignedLong
//

void EXDRSerializerTestSuite::serializeUnsignedLong()
{
	xdata::UnsignedLong in = 2;
	xdata::UnsignedLong out;
		
	this->process(&in,&out);
	CPPUNIT_ASSERT ( in == out );
}

/*
void EXDRSerializerTestSuite::serializeUnsignedLongWrongTag()
{
	xdata::UnsignedLong  v;
	CPPUNIT_ASSERT_THROW( this->validate("../../data/UnsignedLongWrongTag.xml", &v, importedXML, exportedXML) , xdata::exception::Exception);
}

void EXDRSerializerTestSuite::serializeUnsignedLongWrongValue()
{
	xdata::UnsignedLong v;
	CPPUNIT_ASSERT_THROW( this->validate("../../data/UnsignedLongWrongValue.xml", &v, importedXML, exportedXML), xdata::exception::Exception );
}


void EXDRSerializerTestSuite::serializeUnsignedLongMissingType()
{
	xdata::UnsignedLong v;
	CPPUNIT_ASSERT_THROW( this->validate("../../data/UnsignedLongMissingType.xml", &v, importedXML, exportedXML) , xdata::exception::Exception);
}

void EXDRSerializerTestSuite::serializeUnsignedLongMissingValue()
{
	xdata::UnsignedLong v;
	CPPUNIT_ASSERT_THROW( this->validate("../../data/UnsignedLongMissingValue.xml", &v, importedXML, exportedXML), xdata::exception::Exception );
}

void EXDRSerializerTestSuite::serializeUnsignedLongWrongType()
{
	xdata::UnsignedLong v;
	CPPUNIT_ASSERT_THROW( this->validate("../../data/UnsignedLongWrongType.xml", &v, importedXML, exportedXML), xdata::exception::Exception );
}
*/

//
// Tests for Double
//

void EXDRSerializerTestSuite::serializeDouble()
{
	xdata::Double in = 3.141592653589793238462643383279502884197169399375;
	xdata::Double out;
		
	this->process(&in,&out);
	CPPUNIT_ASSERT ( in == out );
}

void EXDRSerializerTestSuite::serializeDoubleNaN()
{
	xdata::Double in = std::numeric_limits<xdata::Double>::quiet_NaN();
	CPPUNIT_ASSERT (in.isNaN());
	xdata::Double out = std::numeric_limits<xdata::Double>::min();	
	CPPUNIT_ASSERT (out.isFinite());	
	this->process(&in,&out);
	CPPUNIT_ASSERT ( out.isNaN() );
}

void EXDRSerializerTestSuite::serializeDoubleInfinite()
{
	xdata::Double in = std::numeric_limits<xdata::Double>::infinity();
	CPPUNIT_ASSERT (in.isInf());
	xdata::Double out = std::numeric_limits<xdata::Double>::max();	
	CPPUNIT_ASSERT (out.isFinite());	
	this->process(&in,&out);
	CPPUNIT_ASSERT ( out.isInf() );
}

/*
void EXDRSerializerTestSuite::serializeDoubleWrongTag()
{
	xdata::Double v;
	CPPUNIT_ASSERT_THROW( this->validate("../../data/DoubleWrongTag.xml", &v, importedXML, exportedXML) , xdata::exception::Exception);
}

void EXDRSerializerTestSuite::serializeDoubleWrongValue()
{
	xdata::Double v;
	CPPUNIT_ASSERT_THROW( this->validate("../../data/DoubleWrongValue.xml", &v, importedXML, exportedXML), xdata::exception::Exception );
}


void EXDRSerializerTestSuite::serializeDoubleMissingType()
{
	xdata::Double v;
	CPPUNIT_ASSERT_THROW( this->validate("../../data/DoubleMissingType.xml", &v, importedXML, exportedXML) , xdata::exception::Exception);
}

void EXDRSerializerTestSuite::serializeDoubleMissingValue()
{
	xdata::Double v;
	CPPUNIT_ASSERT_THROW( this->validate("../../data/DoubleMissingValue.xml", &v, importedXML, exportedXML), xdata::exception::Exception );
}

void EXDRSerializerTestSuite::serializeDoubleWrongType()
{
	xdata::Double v;
	CPPUNIT_ASSERT_THROW( this->validate("../../data/DoubleWrongType.xml", &v, importedXML, exportedXML), xdata::exception::Exception );
}

void EXDRSerializerTestSuite::serializeDoubleTruncate()
{
	xdata::Double v;
	
	CPPUNIT_ASSERT_NO_THROW( this->validate("../../data/DoubleTruncate.xml", &v, importedXML, exportedXML) );
	string expected = "<Parameter type=\"double\">3.141593e+00</Parameter>";
	CPPUNIT_ASSERT_EQUAL(expected, exportedXML);
	
}

*/

//
// Tests for TimeVal
//

void EXDRSerializerTestSuite::serializeTimeVal()
{
	xdata::TimeVal in = toolbox::TimeVal::gettimeofday ();
	xdata::TimeVal out;
	
	this->process(&in,&out);

	// std::cout << "IN: " << in.toString() << std::endl;
	// std::cout << "OUT: " << out.toString() << std::endl;
	
	CPPUNIT_ASSERT ( in == out );
}

//
// Tests for TimeVal
//

void EXDRSerializerTestSuite::serializeTable()
{
	xdata::Table in;
 	try
        {
                // Fill table object
                //
                in.addColumn("Number", "unsigned long");
                in.addColumn("Time", "time");
                in.addColumn("Float", "float");
                in.addColumn("Double","double");
                in.addColumn("String", "string");
                
                for (size_t i = 0; i< 10; ++i)
                {
                        xdata::UnsignedLong  number;
                        number = i;
                        in.setValueAt(i,"Number", number);
                        
                        xdata::TimeVal time(toolbox::TimeVal::gettimeofday());
                        in.setValueAt(i,"Time", time);
                        
			xdata::Float  f;
                        f = 123.456;
                        in.setValueAt(i,"Float", f);
			
                        xdata::Double  d;
                        d = 987.654321;
                        in.setValueAt(i,"Double", d);
			
			xdata::String  s("Test String");
                        in.setValueAt(i,"String", s);
                }
        } 
        catch (xdata::exception::Exception& e)
        {
                std::cout << "Error in create table" << e.what() << std::endl;
        }

	xdata::Table out;
	
	this->process(&in,&out);
	
	xdata::Table::iterator ti = in.begin();
	xdata::Table::iterator to = out.begin();

	do                
        {
		xdata::UnsignedLong * nIn = dynamic_cast<xdata::UnsignedLong *>((*ti).getField("Number"));
                xdata::TimeVal * tIn = dynamic_cast<xdata::TimeVal *>((*ti).getField("Time"));
		xdata::Float * fIn = dynamic_cast<xdata::Float *>((*ti).getField("Float"));
		xdata::Double * dIn = dynamic_cast<xdata::Double *>((*ti).getField("Double"));
                xdata::String * sIn = dynamic_cast<xdata::String *>((*ti).getField("String"));
	
                xdata::UnsignedLong * nOut = dynamic_cast<xdata::UnsignedLong *>((*to).getField("Number"));
                xdata::TimeVal * tOut = dynamic_cast<xdata::TimeVal *>((*to).getField("Time"));
		xdata::Float * fOut = dynamic_cast<xdata::Float *>((*to).getField("Float"));
		xdata::Double * dOut = dynamic_cast<xdata::Double *>((*to).getField("Double"));
                xdata::String * sOut = dynamic_cast<xdata::String *>((*to).getField("String"));

		CPPUNIT_ASSERT ( *nIn == *nOut );
		CPPUNIT_ASSERT ( *tIn == *tOut );
		CPPUNIT_ASSERT ( *fIn == *fOut );
		CPPUNIT_ASSERT ( *dIn == *dOut );
		CPPUNIT_ASSERT ( *sIn == *sOut );
		
		++to;
		++ti;
        } while (ti != in.end());
}

void EXDRSerializerTestSuite::serializeTableNaN()
{
	xdata::Table in;
	
        // Fill table object
        //
        in.addColumn("Number", "unsigned long");
        in.addColumn("Time", "time");
        in.addColumn("Float", "float");
        in.addColumn("Double","double");
        in.addColumn("String", "string");

	// Add 2 rows
	// the first row is normal
	// The second row has NaNs

	// NORMAL
        xdata::UnsignedLong  number;
        number = 0;
        in.setValueAt(0,"Number", number);

        xdata::TimeVal time(toolbox::TimeVal::gettimeofday());
        in.setValueAt(0,"Time", time);

	xdata::Float  f;
        f = 123.456;
        in.setValueAt(0,"Float", f);

        xdata::Double  d;
        d = 987.654321;
        in.setValueAt(0,"Double", d);

	xdata::String  s("Test String");
        in.setValueAt(0,"String", s);

	// NAN
	xdata::UnsignedLong nNaN;
	xdata::TimeVal tNaN;
	xdata::Float  fNaN;
	xdata::Double  dNaN;
	xdata::String  sNaN("NaN");
	
        in.setValueAt(1,"Number", nNaN);
        in.setValueAt(1,"Time", tNaN);
        in.setValueAt(1,"Float", fNaN);
        in.setValueAt(1,"Double", dNaN);
        in.setValueAt(1,"String", sNaN);

	xdata::Table out;
	
	this->process(&in,&out);
	
	xdata::Table::iterator ti = in.begin();
	xdata::Table::iterator to = out.begin();
	
	xdata::UnsignedLong * nIn = dynamic_cast<xdata::UnsignedLong *>((*ti).getField("Number"));
        xdata::TimeVal * tIn = dynamic_cast<xdata::TimeVal *>((*ti).getField("Time"));
	xdata::Float * fIn = dynamic_cast<xdata::Float *>((*ti).getField("Float"));
	xdata::Double * dIn = dynamic_cast<xdata::Double *>((*ti).getField("Double"));
        xdata::String * sIn = dynamic_cast<xdata::String *>((*ti).getField("String"));

        xdata::UnsignedLong * nOut = dynamic_cast<xdata::UnsignedLong *>((*to).getField("Number"));
        xdata::TimeVal * tOut = dynamic_cast<xdata::TimeVal *>((*to).getField("Time"));
	xdata::Float * fOut = dynamic_cast<xdata::Float *>((*to).getField("Float"));
	xdata::Double * dOut = dynamic_cast<xdata::Double *>((*to).getField("Double"));
        xdata::String * sOut = dynamic_cast<xdata::String *>((*to).getField("String"));

	CPPUNIT_ASSERT ( *nIn == *nOut );
	CPPUNIT_ASSERT ( *tIn == *tOut );
	CPPUNIT_ASSERT ( *fIn == *fOut );
	CPPUNIT_ASSERT ( *dIn == *dOut );
	CPPUNIT_ASSERT ( *sIn == *sOut );

	++to;
	++ti;
	
	nIn = dynamic_cast<xdata::UnsignedLong *>((*ti).getField("Number"));
        tIn = dynamic_cast<xdata::TimeVal *>((*ti).getField("Time"));
	fIn = dynamic_cast<xdata::Float *>((*ti).getField("Float"));
	dIn = dynamic_cast<xdata::Double *>((*ti).getField("Double"));
        sIn = dynamic_cast<xdata::String *>((*ti).getField("String"));

        nOut = dynamic_cast<xdata::UnsignedLong *>((*to).getField("Number"));
        tOut = dynamic_cast<xdata::TimeVal *>((*to).getField("Time"));
	fOut = dynamic_cast<xdata::Float *>((*to).getField("Float"));
	dOut = dynamic_cast<xdata::Double *>((*to).getField("Double"));
        sOut = dynamic_cast<xdata::String *>((*to).getField("String"));

	CPPUNIT_ASSERT ( nIn->isNaN() );
	CPPUNIT_ASSERT ( tIn->isNaN() );
	CPPUNIT_ASSERT ( fIn->isNaN() );
	CPPUNIT_ASSERT ( dIn->isNaN() );
	
	CPPUNIT_ASSERT ( nOut->isNaN() );
	CPPUNIT_ASSERT ( tOut->isNaN() );
	CPPUNIT_ASSERT ( fOut->isNaN() );
	CPPUNIT_ASSERT ( dOut->isNaN() );
	
	CPPUNIT_ASSERT ( *sIn == *sOut );  
	
	++to;
	++ti;
	
	CPPUNIT_ASSERT (to == out.end());
	CPPUNIT_ASSERT (ti == in.end());
	
}
