// $Id: EXDRSerializerTestSuite.h,v 1.13 2008/07/18 15:28:14 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _EXDRSerializerTestSuite_h_
#define _EXDRSerializerTestSuite_h_

#include <cppunit/Exception.h>
#include <cppunit/extensions/HelperMacros.h>

#include "xdata/xdata.h"

class EXDRSerializerTestSuite : public CPPUNIT_NS::TestFixture
{
	CPPUNIT_TEST_SUITE( EXDRSerializerTestSuite );
	
	// Tests for Integer
	CPPUNIT_TEST( serializeInteger );
	CPPUNIT_TEST( serializeIntegerNaN );
	CPPUNIT_TEST( serializeIntegerInfinite );
	CPPUNIT_TEST( serializeUnsignedInteger );
	CPPUNIT_TEST( serializeInteger32 );
	CPPUNIT_TEST( serializeInteger64 );
	CPPUNIT_TEST( serializeUnsignedInteger32 );
	CPPUNIT_TEST( serializeUnsignedInteger64 );
	
	CPPUNIT_TEST( serializeIntegerWrongTag );
	CPPUNIT_TEST( serializeIntegerExportBufferOverflow );	
	CPPUNIT_TEST( serializeIntegerImportBufferOverflow );
	
	
	// Tests for Float
	
	CPPUNIT_TEST( serializeFloat );
	CPPUNIT_TEST( serializeFloatNaN );
	CPPUNIT_TEST( serializeFloatInfinite );
	
	// Tests for Boolean
	CPPUNIT_TEST( serializeBoolean );
	CPPUNIT_TEST( serializeBooleanNaN );
	
	// Tests for String
	CPPUNIT_TEST( serializeString );
	CPPUNIT_TEST( serializeStringTooLong );
	CPPUNIT_TEST( deserializeStringTooLong );

	// Tests for UnsignedShort
	
	CPPUNIT_TEST( serializeUnsignedShort );
	

	
	// Tests for UnsignedLong
	
	CPPUNIT_TEST( serializeUnsignedLong );
	

	// Tests for Double
	
	CPPUNIT_TEST( serializeDouble );
	CPPUNIT_TEST( serializeDoubleNaN );
	CPPUNIT_TEST( serializeDoubleInfinite );	

	// Tests for Vector
	
	CPPUNIT_TEST( serializeVector );
	CPPUNIT_TEST(serializeVectorofVectorInteger);
	CPPUNIT_TEST(serializeVectorofVectorofBag);

	// Test functions for Bag
	CPPUNIT_TEST( serializeBag);
	CPPUNIT_TEST( serializeBagEmpty);
	CPPUNIT_TEST( serializeBagContentMismatchFieldName);
	CPPUNIT_TEST( serializeBagContentMismatchFieldType);
	CPPUNIT_TEST( serializeBagWithVector);
	CPPUNIT_TEST( serializeBagWithBag);
	CPPUNIT_TEST( serializeVectorWithBag);
	
	// Test functions for Monitorable
	/*
	CPPUNIT_TEST( serializeMonitorableNoPropertiesNoFields);
	CPPUNIT_TEST( serializeMonitorablePropertiesNoFields);
	CPPUNIT_TEST( serializeMonitorablePropertiesFields);
	*/
	
	// Tests for TimeVal
	
	CPPUNIT_TEST( serializeTimeVal );
	
	// Tests for Table
	
	CPPUNIT_TEST( serializeTable );
	CPPUNIT_TEST( serializeTableNaN );
	
	CPPUNIT_TEST_SUITE_END();

protected:
  
public:
	void setUp();
	void tearDown();

protected:

	// General function. Reads a file that defines a data type. Then creates
	// a serializable. Streams the serializable to a string and compares it
	// with the content of the input file. Throws an exception if the two don't match.
	//
	void process(xdata::Serializable * in, xdata::Serializable * out) throw (CppUnit::Exception, xdata::exception::Exception );

	// Same as process(), but buffer is 1 MB large
	void process_large(xdata::Serializable * in, xdata::Serializable * out) throw (CppUnit::Exception, xdata::exception::Exception );

	// Test functions for Integer
	void serializeInteger();
	void serializeIntegerNaN();
	void serializeIntegerInfinite();	
	void serializeUnsignedInteger();
	void serializeInteger32();
	void serializeUnsignedInteger32();
	void serializeInteger64();
	void serializeUnsignedInteger64();
	void serializeIntegerWrongTag();
	void serializeIntegerExportBufferOverflow();
	void serializeIntegerImportBufferOverflow();
		
	// Test functions for Float	
	void serializeFloat();
	void serializeFloatNaN();
	void serializeFloatInfinite();
	
	// Test functions for Boolean
	void serializeBoolean();
	void serializeBooleanNaN();
	
	// Test functions for String
	void serializeString();
	void serializeStringTooLong();
	void deserializeStringTooLong();

	// Test functions for UnsignedShort	
	void serializeUnsignedShort();
	
	// Test functions for UnsignedLong	
	void serializeUnsignedLong();
	
	// Test functions for Double
	void serializeDouble();
	void serializeDoubleNaN();
	void serializeDoubleInfinite();
	
	// Test functions for Vector
	void serializeVector();
	void serializeVectorofVectorInteger();
	void serializeVectorofVectorofBag();
	
	// Test functions for Bag
	void serializeBag();
	void serializeBagEmpty();
	void serializeBagContentMismatchFieldName();
	void serializeBagContentMismatchFieldType();
	void serializeBagWithVector();
	void serializeBagWithBag();
	void serializeVectorWithBag();
	
	// Test functions for Monitorable
	void serializeMonitorableNoPropertiesNoFields();
	void serializeMonitorablePropertiesNoFields();
	void serializeMonitorablePropertiesFields();
	
	// Test functions for TimeVal
	void serializeTimeVal();
	
	// Test functions for Table
	void serializeTable();
	void serializeTableNaN();
	
};


#endif
