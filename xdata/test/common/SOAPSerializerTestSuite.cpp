#include "SOAPSerializerTestSuite.h"
#include "xdata/Serializable.h"
#include "xdata/soap/Serializer.h"
#include "xdata/exception/Exception.h"
#include "xdata/XStr.h"
#include "xdata/Float.h"
#include "xdata/Integer.h"
#include "xdata/Integer32.h"
#include "xdata/Integer64.h"
#include "xdata/UnsignedInteger.h"
#include "xdata/UnsignedInteger32.h"
#include "xdata/UnsignedInteger64.h"
#include "xdata/Boolean.h"
#include "xdata/String.h"
#include "xdata/TimeVal.h"
#include "xdata/Bag.h"
#include "TestBag.h"
#include "xoap/domutils.h"

CPPUNIT_TEST_SUITE_REGISTRATION( SOAPSerializerTestSuite );

void SOAPSerializerTestSuite::setUp()
{
 	loader_ = new xdata::XMLDOMLoader();	
}

void SOAPSerializerTestSuite::tearDown()
{
	delete loader_;
}

void SOAPSerializerTestSuite::validate
(
	const std::string  & filename, xdata::Serializable * serializable, std::string& iXML, std::string& eXML
) 
	throw (CppUnit::Exception, xdata::exception::Exception::Exception)
{	
	DOMDocument * doc = 0;
	xdata::soap::Serializer * serializer = 0;
	
	try
	{ 
		doc = loader_->load(filename);
	}
	catch (xdata::exception::Exception::Exception & e )
	{
		throw CppUnit::Exception ( CppUnit::Message(e.what()), CPPUNIT_SOURCELINE() );	
	}
	
	try
	{ 
		serializer = new xdata::soap::Serializer();
	}
	catch (xdata::exception::Exception & e )
	{
		loader_->release(doc);
		throw CppUnit::Exception ( CppUnit::Message(e.what()), CPPUNIT_SOURCELINE() );	
	}

	try
	{ 

		DOMNode * root = doc->getDocumentElement();
		
		// create <test xmlns="any" xmlns xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding">
		//
		DOMImplementation* impl = DOMImplementationRegistry::getDOMImplementation ( xdata::XStr(xoap::DOMImplementationFeatures) );
		DOMDocument* exportDocument = impl->createDocument (xdata::XStr (""), xdata::XStr("root"), 0);			
		
		DOMElement* targetNode = exportDocument->createElementNS(xdata::XStr("any"),xdata::XStr("test"));
		targetNode->setAttribute (xdata::XStr("xmlns"),xdata::XStr("any")); 
		//targetNode->setAttribute (xdata::XStr("xmlns:soapenc"),xdata::XStr("http://schemas.xmlsoap.org/soap/encoding")); 
		//targetNode->setAttribute (xdata::XStr("xmlns:xsi"),xdata::XStr("http://www.w3.org/2001/XMLSchema-instance")); 

		// import DOM into xdata variable
		serializer->import(serializable,root);
		
		//std::cout << "Import done." << std::endl;

		// export xdata variable into a dom node
		//DOMElement * exportedNode = serializer->exportAll(serializable, targetNode, true);
		(void) serializer->exportAll(serializable, targetNode, true);

		//std::cout << "Export done." << std::endl;

		// serialize into a XML
		eXML = "";
		xdata::XMLDOMSerializer s2(eXML);
		s2.serialize(targetNode);
		
		//std::cout << "Put exported to string" << std::endl;		
		//std::cout << eXML << std::endl << "--------------------------------" << std::endl;

		iXML = "";
		xdata::XMLDOMSerializer s(iXML);
		s.serialize(root);

		loader_->release(doc);
		exportDocument->release();
		delete serializer;
	} 
	catch (xdata::exception::Exception & e )
	{
		// free document
		//cout << "error" << e.what() << endl;
		loader_->release(doc);
		delete 	serializer;
		throw e;	
	}	
}


//
// Tests for Integer
//

void SOAPSerializerTestSuite::serializeInteger()
{
	xdata::Integer v;
	CPPUNIT_ASSERT_NO_THROW( this->validate("${XDAQ_ROOT}/daq/xdata/test/data/soap/Integer.xml", &v, importedXML, exportedXML) );
	CPPUNIT_ASSERT_EQUAL( importedXML, exportedXML);
}

void SOAPSerializerTestSuite::serializeIntegerNaN()
{
	xdata::Integer v = std::numeric_limits<xdata::Integer>::quiet_NaN();
	CPPUNIT_ASSERT_EQUAL (v.isNaN(), true);
	CPPUNIT_ASSERT_NO_THROW( this->validate("${XDAQ_ROOT}/daq/xdata/test/data/soap/IntegerNaN.xml", &v, importedXML, exportedXML) );
	CPPUNIT_ASSERT_EQUAL( importedXML, exportedXML);
}

void SOAPSerializerTestSuite::serializeIntegerInfinite()
{
	xdata::Integer v = std::numeric_limits<xdata::Integer>::max();
	++v;
	CPPUNIT_ASSERT_EQUAL (v.isInf(), true);
	CPPUNIT_ASSERT_NO_THROW( this->validate("${XDAQ_ROOT}/daq/xdata/test/data/soap/IntegerInfinite.xml", &v, importedXML, exportedXML) );
	CPPUNIT_ASSERT_EQUAL( importedXML, exportedXML);
}

void SOAPSerializerTestSuite::serializeInteger32()
{
	xdata::Integer32 v;
	CPPUNIT_ASSERT_NO_THROW( this->validate("${XDAQ_ROOT}/daq/xdata/test/data/soap/Integer32.xml", &v, importedXML, exportedXML) );
	CPPUNIT_ASSERT_EQUAL( importedXML, exportedXML);
}

void SOAPSerializerTestSuite::serializeInteger32NaN()
{
	xdata::Integer32 v = std::numeric_limits<xdata::Integer32>::quiet_NaN();
	CPPUNIT_ASSERT_EQUAL (v.isNaN(), true);
	CPPUNIT_ASSERT_NO_THROW( this->validate("${XDAQ_ROOT}/daq/xdata/test/data/soap/Integer32NaN.xml", &v, importedXML, exportedXML) );
	CPPUNIT_ASSERT_EQUAL( importedXML, exportedXML);
}

void SOAPSerializerTestSuite::serializeInteger32Infinite()
{
	xdata::Integer32 v = std::numeric_limits<xdata::Integer32>::max();
	++v;
	CPPUNIT_ASSERT_EQUAL (v.isInf(), true);
	CPPUNIT_ASSERT_NO_THROW( this->validate("${XDAQ_ROOT}/daq/xdata/test/data/soap/Integer32Infinite.xml", &v, importedXML, exportedXML) );
	CPPUNIT_ASSERT_EQUAL( importedXML, exportedXML);
}

void SOAPSerializerTestSuite::serializeInteger64()
{
	xdata::Integer64 v;
	CPPUNIT_ASSERT_NO_THROW( this->validate("${XDAQ_ROOT}/daq/xdata/test/data/soap/Integer64.xml", &v, importedXML, exportedXML) );
	CPPUNIT_ASSERT_EQUAL( importedXML, exportedXML);
}

void SOAPSerializerTestSuite::serializeInteger64NaN()
{
	xdata::Integer64 v = std::numeric_limits<xdata::Integer64>::quiet_NaN();
	CPPUNIT_ASSERT_EQUAL (v.isNaN(), true);
	CPPUNIT_ASSERT_NO_THROW( this->validate("${XDAQ_ROOT}/daq/xdata/test/data/soap/Integer64NaN.xml", &v, importedXML, exportedXML) );
	CPPUNIT_ASSERT_EQUAL( importedXML, exportedXML);
}

void SOAPSerializerTestSuite::serializeInteger64Infinite()
{
	xdata::Integer64 v = std::numeric_limits<xdata::Integer64>::max();
	++v;
	CPPUNIT_ASSERT_EQUAL (v.isInf(), true);
	CPPUNIT_ASSERT_NO_THROW( this->validate("${XDAQ_ROOT}/daq/xdata/test/data/soap/Integer64Infinite.xml", &v, importedXML, exportedXML) );
	CPPUNIT_ASSERT_EQUAL( importedXML, exportedXML);
}

void SOAPSerializerTestSuite::serializeUnsignedInteger()
{
	xdata::UnsignedInteger v;
	CPPUNIT_ASSERT_NO_THROW( this->validate("${XDAQ_ROOT}/daq/xdata/test/data/soap/UnsignedInteger.xml", &v, importedXML, exportedXML) );
	CPPUNIT_ASSERT_EQUAL( importedXML, exportedXML);
}

void SOAPSerializerTestSuite::serializeUnsignedInteger32()
{
	xdata::UnsignedInteger32 v;
	CPPUNIT_ASSERT_NO_THROW( this->validate("${XDAQ_ROOT}/daq/xdata/test/data/soap/UnsignedInteger32.xml", &v, importedXML, exportedXML) );
	CPPUNIT_ASSERT_EQUAL( importedXML, exportedXML);
}

void SOAPSerializerTestSuite::serializeUnsignedInteger64()
{
	xdata::UnsignedInteger64 v;
	CPPUNIT_ASSERT_NO_THROW( this->validate("${XDAQ_ROOT}/daq/xdata/test/data/soap/UnsignedInteger64.xml", &v, importedXML, exportedXML) );
	CPPUNIT_ASSERT_EQUAL( importedXML, exportedXML);
}



void SOAPSerializerTestSuite::serializeIntegerWrongValue()
{
	xdata::Integer v;
	CPPUNIT_ASSERT_THROW( this->validate("${XDAQ_ROOT}/daq/xdata/test/data/soap/IntegerWrongValue.xml", &v, importedXML, exportedXML), xdata::exception::Exception );
}


void SOAPSerializerTestSuite::serializeIntegerMissingType()
{
	xdata::Integer v;
	CPPUNIT_ASSERT_THROW( this->validate("${XDAQ_ROOT}/daq/xdata/test/data/soap/IntegerMissingType.xml", &v, importedXML, exportedXML) , xdata::exception::Exception);
}

void SOAPSerializerTestSuite::serializeIntegerMissingValue()
{
	xdata::Integer v;
	CPPUNIT_ASSERT_THROW( this->validate("${XDAQ_ROOT}/daq/xdata/test/data/soap/IntegerMissingValue.xml", &v, importedXML, exportedXML), xdata::exception::Exception );
}

void SOAPSerializerTestSuite::serializeIntegerWrongType()
{
	xdata::Integer v;
	CPPUNIT_ASSERT_THROW( this->validate("${XDAQ_ROOT}/daq/xdata/test/data/soap/IntegerWrongType.xml", &v, importedXML, exportedXML), xdata::exception::Exception );
}


//
// Tests for Float
//

void SOAPSerializerTestSuite::serializeFloat()
{
	xdata::Float v;
	CPPUNIT_ASSERT_NO_THROW( this->validate("${XDAQ_ROOT}/daq/xdata/test/data/soap/Float.xml", &v, importedXML, exportedXML) );
	CPPUNIT_ASSERT_EQUAL( importedXML, exportedXML);
}

void SOAPSerializerTestSuite::serializeFloatNaN()
{
	xdata::Float v = std::numeric_limits<xdata::Float>::quiet_NaN();
	CPPUNIT_ASSERT_EQUAL (v.isNaN(), true);
	CPPUNIT_ASSERT_NO_THROW( this->validate("${XDAQ_ROOT}/daq/xdata/test/data/soap/FloatNaN.xml", &v, importedXML, exportedXML) );
	CPPUNIT_ASSERT_EQUAL( importedXML, exportedXML);
}

void SOAPSerializerTestSuite::serializeFloatInfinite()
{
	xdata::Float v = std::numeric_limits<xdata::Float>::infinity();
	CPPUNIT_ASSERT_EQUAL (v.isInf(), true);
	CPPUNIT_ASSERT_NO_THROW( this->validate("${XDAQ_ROOT}/daq/xdata/test/data/soap/FloatInfinite.xml", &v, importedXML, exportedXML) );
	CPPUNIT_ASSERT_EQUAL( importedXML, exportedXML);
}

void SOAPSerializerTestSuite::serializeFloatWrongValue()
{
	xdata::Float v;
	CPPUNIT_ASSERT_THROW( this->validate("${XDAQ_ROOT}/daq/xdata/test/data/soap/FloatWrongValue.xml", &v, importedXML, exportedXML), xdata::exception::Exception );
}


void SOAPSerializerTestSuite::serializeFloatMissingType()
{
	xdata::Float v;
	CPPUNIT_ASSERT_THROW( this->validate("${XDAQ_ROOT}/daq/xdata/test/data/soap/FloatMissingType.xml", &v, importedXML, exportedXML) , xdata::exception::Exception);
}

void SOAPSerializerTestSuite::serializeFloatMissingValue()
{
	xdata::Float v;
	CPPUNIT_ASSERT_THROW( this->validate("${XDAQ_ROOT}/daq/xdata/test/data/soap/FloatMissingValue.xml", &v, importedXML, exportedXML), xdata::exception::Exception );
}

void SOAPSerializerTestSuite::serializeFloatWrongType()
{
	xdata::Float v;
	CPPUNIT_ASSERT_THROW( this->validate("${XDAQ_ROOT}/daq/xdata/test/data/soap/FloatWrongType.xml", &v, importedXML, exportedXML), xdata::exception::Exception );
}

void SOAPSerializerTestSuite::serializeFloatTruncate()
{
	xdata::Float v;
	
	CPPUNIT_ASSERT_NO_THROW( this->validate("${XDAQ_ROOT}/daq/xdata/test/data/soap/FloatTruncate.xml", &v, importedXML, exportedXML) );
	
	std::string::size_type pos = exportedXML.find("3.141593");
	CPPUNIT_ASSERT(pos != std::string::npos);
	
}



//
// Tests for Boolean
//

void SOAPSerializerTestSuite::serializeBoolean()
{
	
	xdata::Boolean v;
	CPPUNIT_ASSERT_NO_THROW( this->validate("${XDAQ_ROOT}/daq/xdata/test/data/soap/Boolean.xml", &v, importedXML, exportedXML) );
	CPPUNIT_ASSERT_EQUAL( importedXML, exportedXML);
}

void SOAPSerializerTestSuite::serializeBooleanNaN()
{
	xdata::Boolean v = std::numeric_limits<xdata::Boolean>::quiet_NaN();
	CPPUNIT_ASSERT_EQUAL (v.isNaN(), true);
	CPPUNIT_ASSERT_NO_THROW( this->validate("${XDAQ_ROOT}/daq/xdata/test/data/soap/BooleanNaN.xml", &v, importedXML, exportedXML) );
	CPPUNIT_ASSERT_EQUAL( importedXML, exportedXML);
}

void SOAPSerializerTestSuite::serializeBooleanWrongValue()
{
	xdata::Boolean v;
	CPPUNIT_ASSERT_THROW( this->validate("${XDAQ_ROOT}/daq/xdata/test/data/soap/BooleanWrongValue.xml", &v, importedXML, exportedXML), xdata::exception::Exception );
}


void SOAPSerializerTestSuite::serializeBooleanMissingType()
{
	xdata::Boolean v;
	CPPUNIT_ASSERT_THROW( this->validate("${XDAQ_ROOT}/daq/xdata/test/data/soap/BooleanMissingType.xml", &v, importedXML, exportedXML) , xdata::exception::Exception);
}

void SOAPSerializerTestSuite::serializeBooleanMissingValue()
{
	xdata::Boolean v;
	CPPUNIT_ASSERT_THROW( this->validate("${XDAQ_ROOT}/daq/xdata/test/data/soap/BooleanMissingValue.xml", &v, importedXML, exportedXML), xdata::exception::Exception );
}

void SOAPSerializerTestSuite::serializeBooleanWrongType()
{
	xdata::Boolean v;
	CPPUNIT_ASSERT_THROW( this->validate("${XDAQ_ROOT}/daq/xdata/test/data/soap/BooleanWrongType.xml", &v, importedXML, exportedXML), xdata::exception::Exception );
}

//
// Tests for TimeVal
//

void SOAPSerializerTestSuite::serializeTimeVal()
{	
	xdata::TimeVal v;
	CPPUNIT_ASSERT_NO_THROW( this->validate("${XDAQ_ROOT}/daq/xdata/test/data/soap/TimeVal.xml", &v, importedXML, exportedXML) );
	CPPUNIT_ASSERT_EQUAL( importedXML, exportedXML);
}

void SOAPSerializerTestSuite::serializeTimeValNaN()
{
	xdata::TimeVal v = std::numeric_limits<xdata::TimeVal>::quiet_NaN();
	CPPUNIT_ASSERT_EQUAL (v.isNaN(), true);
	CPPUNIT_ASSERT_NO_THROW( this->validate("${XDAQ_ROOT}/daq/xdata/test/data/soap/TimeValNaN.xml", &v, importedXML, exportedXML) );
	CPPUNIT_ASSERT_EQUAL( importedXML, exportedXML);
}

void SOAPSerializerTestSuite::serializeTimeValWrongValue()
{
	xdata::TimeVal v;
	CPPUNIT_ASSERT_THROW( this->validate("${XDAQ_ROOT}/daq/xdata/test/data/soap/TimeValWrongValue.xml", &v, importedXML, exportedXML), xdata::exception::Exception );
}

void SOAPSerializerTestSuite::serializeTimeValMissingType()
{
	xdata::TimeVal v;
	CPPUNIT_ASSERT_THROW( this->validate("${XDAQ_ROOT}/daq/xdata/test/data/soap/TimeValMissingType.xml", &v, importedXML, exportedXML) , xdata::exception::Exception);
}

void SOAPSerializerTestSuite::serializeTimeValMissingValue()
{
	xdata::TimeVal v;
	CPPUNIT_ASSERT_THROW( this->validate("${XDAQ_ROOT}/daq/xdata/test/data/soap/TimeValMissingValue.xml", &v, importedXML, exportedXML), xdata::exception::Exception );
}

void SOAPSerializerTestSuite::serializeTimeValWrongType()
{
	xdata::TimeVal v;
	CPPUNIT_ASSERT_THROW( this->validate("${XDAQ_ROOT}/daq/xdata/test/data/soap/TimeValWrongType.xml", &v, importedXML, exportedXML), xdata::exception::Exception );
}


//
// Tests forString
//

void SOAPSerializerTestSuite::serializeString()
{
	xdata::String v;
	CPPUNIT_ASSERT_NO_THROW( this->validate("${XDAQ_ROOT}/daq/xdata/test/data/soap/String.xml", &v, importedXML, exportedXML) );
	CPPUNIT_ASSERT_EQUAL( importedXML, exportedXML);
}



void SOAPSerializerTestSuite::serializeStringMissingType()
{
	xdata::String v;
	CPPUNIT_ASSERT_THROW( this->validate("${XDAQ_ROOT}/daq/xdata/test/data/soap/StringMissingType.xml", &v, importedXML, exportedXML) , xdata::exception::Exception);
}

void SOAPSerializerTestSuite::serializeStringEmptyValue()
{
	xdata::String v;
	CPPUNIT_ASSERT_NO_THROW( this->validate("${XDAQ_ROOT}/daq/xdata/test/data/soap/StringEmptyValue.xml", &v, importedXML, exportedXML));
	CPPUNIT_ASSERT_EQUAL( importedXML, exportedXML);
}

void SOAPSerializerTestSuite::serializeStringWrongType()
{
	xdata::String v;
	CPPUNIT_ASSERT_THROW( this->validate("${XDAQ_ROOT}/daq/xdata/test/data/soap/StringWrongType.xml", &v, importedXML, exportedXML), xdata::exception::Exception );
}



//
// Tests for Vector
//

void SOAPSerializerTestSuite::serializeVector()
{
	xdata::Vector<xdata::Integer> v;
	CPPUNIT_ASSERT_NO_THROW( this->validate("${XDAQ_ROOT}/daq/xdata/test/data/soap/VectorInteger.xml", &v, importedXML, exportedXML) );
	
	// cout << endl << importedXML << endl << endl << exportedXML << endl;
	
	CPPUNIT_ASSERT_EQUAL( importedXML, exportedXML);
}



void SOAPSerializerTestSuite::serializeVectorWrongValue()
{
	xdata::Vector<xdata::Integer> v;
	CPPUNIT_ASSERT_THROW( this->validate("${XDAQ_ROOT}/daq/xdata/test/data/soap/VectorIntegerWrongValue.xml", &v, importedXML, exportedXML), xdata::exception::Exception );
	
}


void SOAPSerializerTestSuite::serializeVectorMissingType()
{
	xdata::Vector<xdata::Integer> v;
	CPPUNIT_ASSERT_THROW( this->validate("${XDAQ_ROOT}/daq/xdata/test/data/soap/VectorIntegerMissingType.xml", &v, importedXML, exportedXML) , xdata::exception::Exception);
}

void SOAPSerializerTestSuite::serializeVectorMissingValue()
{
	xdata::Vector<xdata::Integer> v;
	CPPUNIT_ASSERT_NO_THROW( this->validate("${XDAQ_ROOT}/daq/xdata/test/data/soap/VectorIntegerMissingValue.xml", &v, importedXML, exportedXML) );
	CPPUNIT_ASSERT_EQUAL( importedXML, exportedXML);
}

void SOAPSerializerTestSuite::serializeVectorWrongType()
{
	xdata::Vector<xdata::Integer> v;
	CPPUNIT_ASSERT_THROW( this->validate("${XDAQ_ROOT}/daq/xdata/test/data/soap/VectorIntegerWrongType.xml", &v, importedXML, exportedXML), xdata::exception::Exception );
}


void SOAPSerializerTestSuite::serializeVectorMissingIndex()
{
	xdata::Vector<xdata::Integer> v;
	CPPUNIT_ASSERT_THROW( this->validate("${XDAQ_ROOT}/daq/xdata/test/data/soap/VectorIntegerMissingIndex.xml", &v, importedXML, exportedXML), xdata::exception::Exception );
}

void SOAPSerializerTestSuite::serializeVectorWrongElementType()
{
	xdata::Vector<xdata::Integer> v;
	CPPUNIT_ASSERT_THROW( this->validate("${XDAQ_ROOT}/daq/xdata/test/data/soap/VectorIntegerWrongElementType.xml", &v, importedXML, exportedXML), xdata::exception::Exception );
}

void SOAPSerializerTestSuite::serializeVectorWrongElement()
{
	xdata::Vector<xdata::Integer> v;
	CPPUNIT_ASSERT_THROW( this->validate("${XDAQ_ROOT}/daq/xdata/test/data/soap/VectorIntegerWrongElement.xml", &v, importedXML, exportedXML), xdata::exception::Exception );
}

//
// Tests for Bag
//

void SOAPSerializerTestSuite::serializeBag()
{
	xdata::Bag<TestBag> b;
	CPPUNIT_ASSERT_NO_THROW( this->validate("${XDAQ_ROOT}/daq/xdata/test/data/soap/Bag.xml", &b, importedXML, exportedXML ));
	CPPUNIT_ASSERT_EQUAL( importedXML, exportedXML);
}





void SOAPSerializerTestSuite::serializeBagWrongValue()
{
	xdata::Bag<TestBag> b;
	CPPUNIT_ASSERT_THROW( this->validate("${XDAQ_ROOT}/daq/xdata/test/data/soap/BagWrongValue.xml", &b, importedXML, exportedXML), xdata::exception::Exception );
}


void SOAPSerializerTestSuite::serializeBagMissingType()
{
	xdata::Bag<TestBag> b;
	CPPUNIT_ASSERT_THROW( this->validate("${XDAQ_ROOT}/daq/xdata/test/data/soap/BagMissingType.xml", &b, importedXML, exportedXML), xdata::exception::Exception );
}


void SOAPSerializerTestSuite::serializeBagWrongType()
{
	xdata::Bag<TestBag> b;
	CPPUNIT_ASSERT_THROW( this->validate("${XDAQ_ROOT}/daq/xdata/test/data/soap/BagWrongType.xml", &b, importedXML, exportedXML), xdata::exception::Exception );
}


void SOAPSerializerTestSuite::serializeBagWrongFieldType()
{
	xdata::Bag<TestBag> b;
	CPPUNIT_ASSERT_THROW( this->validate("${XDAQ_ROOT}/daq/xdata/test/data/soap/BagMissingFieldType.xml", &b, importedXML, exportedXML), xdata::exception::Exception );
}

void SOAPSerializerTestSuite::serializeBagWrongElement()
{
	xdata::Bag<TestBag> b;
	CPPUNIT_ASSERT_THROW( this->validate("${XDAQ_ROOT}/daq/xdata/test/data/soap/BagWrongElement.xml", &b, importedXML, exportedXML) , xdata::exception::Exception);
}

void SOAPSerializerTestSuite::serializeBagEmpty()
{
	xdata::Bag<TestEmptyBag> b;
	CPPUNIT_ASSERT_NO_THROW( this->validate("${XDAQ_ROOT}/daq/xdata/test/data/soap/BagEmpty.xml", &b, importedXML, exportedXML));
	CPPUNIT_ASSERT_EQUAL( importedXML, exportedXML);
}

void SOAPSerializerTestSuite::serializeBagContentMismatchFieldName()
{
	xdata::Bag<TestBag> b;
	CPPUNIT_ASSERT_THROW( this->validate("${XDAQ_ROOT}/daq/xdata/test/data/soap/BagContentMismatchFieldName.xml", &b, importedXML, exportedXML) , xdata::exception::Exception);
}
void SOAPSerializerTestSuite::serializeBagContentMismatchFieldType()
{
	xdata::Bag<TestBag> b;
	CPPUNIT_ASSERT_THROW( this->validate("${XDAQ_ROOT}/daq/xdata/test/data/soap/BagContentMismatchFieldType.xml", &b, importedXML, exportedXML) , xdata::exception::Exception);
}

void SOAPSerializerTestSuite::serializeBagWithVector()
{
	xdata::Bag<TestBagWithVector> b;
	CPPUNIT_ASSERT_NO_THROW( this->validate("${XDAQ_ROOT}/daq/xdata/test/data/soap/BagWithVector.xml", &b, importedXML, exportedXML));
	CPPUNIT_ASSERT_EQUAL( importedXML, exportedXML);
}

void SOAPSerializerTestSuite::serializeBagWithBag()
{
	xdata::Bag<TestBagWithBag> b;
	CPPUNIT_ASSERT_NO_THROW( this->validate("${XDAQ_ROOT}/daq/xdata/test/data/soap/BagWithBag.xml", &b, importedXML, exportedXML) );
	CPPUNIT_ASSERT_EQUAL( importedXML, exportedXML);
}

void SOAPSerializerTestSuite::serializeVectorWithBag()
{
	xdata::Vector<xdata::Bag<TestBag> > v;
	CPPUNIT_ASSERT_NO_THROW( this->validate("${XDAQ_ROOT}/daq/xdata/test/data/soap/VectorWithBag.xml", &v, importedXML, exportedXML));

	CPPUNIT_ASSERT_EQUAL( importedXML, exportedXML);
}


//
// Tests for UnsignedShort
//

void SOAPSerializerTestSuite::serializeUnsignedShort()
{
	xdata::UnsignedShort v;
	CPPUNIT_ASSERT_NO_THROW( this->validate("${XDAQ_ROOT}/daq/xdata/test/data/soap/UnsignedShort.xml", &v, importedXML, exportedXML) );
	CPPUNIT_ASSERT_EQUAL( importedXML, exportedXML);
}



void SOAPSerializerTestSuite::serializeUnsignedShortWrongValue()
{
	xdata::UnsignedShort v;
	CPPUNIT_ASSERT_THROW( this->validate("${XDAQ_ROOT}/daq/xdata/test/data/soap/UnsignedShortWrongValue.xml", &v, importedXML, exportedXML), xdata::exception::Exception );
}


void SOAPSerializerTestSuite::serializeUnsignedShortMissingType()
{
	xdata::UnsignedShort v;
	CPPUNIT_ASSERT_THROW( this->validate("${XDAQ_ROOT}/daq/xdata/test/data/soap/UnsignedShortMissingType.xml", &v, importedXML, exportedXML) , xdata::exception::Exception);
}

void SOAPSerializerTestSuite::serializeUnsignedShortMissingValue()
{
	xdata::UnsignedShort v;
	CPPUNIT_ASSERT_THROW( this->validate("${XDAQ_ROOT}/daq/xdata/test/data/soap/UnsignedShortMissingValue.xml", &v, importedXML, exportedXML), xdata::exception::Exception );
}

void SOAPSerializerTestSuite::serializeUnsignedShortWrongType()
{
	xdata::UnsignedShort v;
	CPPUNIT_ASSERT_THROW( this->validate("${XDAQ_ROOT}/daq/xdata/test/data/soap/UnsignedShortWrongType.xml", &v, importedXML, exportedXML), xdata::exception::Exception );
}

//
// Tests for UnsignedLong
//

void SOAPSerializerTestSuite::serializeUnsignedLong()
{
	xdata::UnsignedLong v;
	CPPUNIT_ASSERT_NO_THROW( this->validate("${XDAQ_ROOT}/daq/xdata/test/data/soap/UnsignedLong.xml", &v, importedXML, exportedXML) );
	CPPUNIT_ASSERT_EQUAL( importedXML, exportedXML);
}



void SOAPSerializerTestSuite::serializeUnsignedLongWrongValue()
{
	xdata::UnsignedLong v;
	CPPUNIT_ASSERT_THROW( this->validate("${XDAQ_ROOT}/daq/xdata/test/data/soap/UnsignedLongWrongValue.xml", &v, importedXML, exportedXML), xdata::exception::Exception );
}


void SOAPSerializerTestSuite::serializeUnsignedLongMissingType()
{
	xdata::UnsignedLong v;
	CPPUNIT_ASSERT_THROW( this->validate("${XDAQ_ROOT}/daq/xdata/test/data/soap/UnsignedLongMissingType.xml", &v, importedXML, exportedXML) , xdata::exception::Exception);
}

void SOAPSerializerTestSuite::serializeUnsignedLongMissingValue()
{
	xdata::UnsignedLong v;
	CPPUNIT_ASSERT_THROW( this->validate("${XDAQ_ROOT}/daq/xdata/test/data/soap/UnsignedLongMissingValue.xml", &v, importedXML, exportedXML), xdata::exception::Exception );
}

void SOAPSerializerTestSuite::serializeUnsignedLongWrongType()
{
	xdata::UnsignedLong v;
	CPPUNIT_ASSERT_THROW( this->validate("${XDAQ_ROOT}/daq/xdata/test/data/soap/UnsignedLongWrongType.xml", &v, importedXML, exportedXML), xdata::exception::Exception );
}

//
// Tests for Double
//

void SOAPSerializerTestSuite::serializeDouble()
{
	xdata::Double v;
	CPPUNIT_ASSERT_NO_THROW( this->validate("${XDAQ_ROOT}/daq/xdata/test/data/soap/Double.xml", &v, importedXML, exportedXML) );
	CPPUNIT_ASSERT_EQUAL( importedXML, exportedXML);
}

void SOAPSerializerTestSuite::serializeDoubleNaN()
{
	xdata::Double v = std::numeric_limits<xdata::Double>::quiet_NaN();
	CPPUNIT_ASSERT_EQUAL (v.isNaN(), true);
	CPPUNIT_ASSERT_NO_THROW( this->validate("${XDAQ_ROOT}/daq/xdata/test/data/soap/DoubleNaN.xml", &v, importedXML, exportedXML) );
	CPPUNIT_ASSERT_EQUAL( importedXML, exportedXML);
}

void SOAPSerializerTestSuite::serializeDoubleInfinite()
{
	xdata::Double v = std::numeric_limits<xdata::Double>::infinity();
	CPPUNIT_ASSERT_EQUAL (v.isInf(), true);
	CPPUNIT_ASSERT_NO_THROW( this->validate("${XDAQ_ROOT}/daq/xdata/test/data/soap/DoubleInfinite.xml", &v, importedXML, exportedXML) );
	CPPUNIT_ASSERT_EQUAL( importedXML, exportedXML);
}

void SOAPSerializerTestSuite::serializeDoubleWrongValue()
{
	xdata::Double v;
	CPPUNIT_ASSERT_THROW( this->validate("${XDAQ_ROOT}/daq/xdata/test/data/soap/DoubleWrongValue.xml", &v, importedXML, exportedXML), xdata::exception::Exception );
}


void SOAPSerializerTestSuite::serializeDoubleMissingType()
{
	xdata::Double v;
	CPPUNIT_ASSERT_THROW( this->validate("${XDAQ_ROOT}/daq/xdata/test/data/soap/DoubleMissingType.xml", &v, importedXML, exportedXML) , xdata::exception::Exception);
}

void SOAPSerializerTestSuite::serializeDoubleMissingValue()
{
	xdata::Double v;
	CPPUNIT_ASSERT_THROW( this->validate("${XDAQ_ROOT}/daq/xdata/test/data/soap/DoubleMissingValue.xml", &v, importedXML, exportedXML), xdata::exception::Exception );
}

void SOAPSerializerTestSuite::serializeDoubleWrongType()
{
	xdata::Double v;
	CPPUNIT_ASSERT_THROW( this->validate("${XDAQ_ROOT}/daq/xdata/test/data/soap/DoubleWrongType.xml", &v, importedXML, exportedXML), xdata::exception::Exception );
}

void SOAPSerializerTestSuite::serializeDoubleTruncate()
{
	xdata::Double v;
	
	CPPUNIT_ASSERT_NO_THROW( this->validate("${XDAQ_ROOT}/daq/xdata/test/data/soap/DoubleTruncate.xml", &v, importedXML, exportedXML) );

	std::string::size_type pos = exportedXML.find("3.141593e+00");
	CPPUNIT_ASSERT(pos != std::string::npos);
}


//
// Tests for Monitorable
//
/*
void SOAPSerializerTestSuite::serializeMonitorable()
{
	xdata::Monitorable<TestMonitorable> b;
	CPPUNIT_ASSERT_NO_THROW( this->validate("${XDAQ_ROOT}/daq/xdata/test/data/soap/Monitorable.xml", &b, importedXML, exportedXML ));
	CPPUNIT_ASSERT_EQUAL( importedXML, exportedXML);
}





void SOAPSerializerTestSuite::serializeMonitorableWrongValue()
{
	xdata::Monitorable<TestMonitorable> b;
	CPPUNIT_ASSERT_THROW( this->validate("${XDAQ_ROOT}/daq/xdata/test/data/soap/MonitorableWrongValue.xml", &b, importedXML, exportedXML), xdata::exception::Exception );
}


void SOAPSerializerTestSuite::serializeMonitorableMissingType()
{
	xdata::Monitorable<TestMonitorable> b;
	CPPUNIT_ASSERT_THROW( this->validate("${XDAQ_ROOT}/daq/xdata/test/data/soap/MonitorableMissingType.xml", &b, importedXML, exportedXML), xdata::exception::Exception );
}


void SOAPSerializerTestSuite::serializeMonitorableWrongType()
{
	xdata::Monitorable<TestMonitorable> b;
	CPPUNIT_ASSERT_THROW( this->validate("${XDAQ_ROOT}/daq/xdata/test/data/soap/MonitorableWrongType.xml", &b, importedXML, exportedXML), xdata::exception::Exception );
}


void SOAPSerializerTestSuite::serializeMonitorableWrongFieldType()
{
	xdata::Monitorable<TestMonitorable> b;
	CPPUNIT_ASSERT_THROW( this->validate("${XDAQ_ROOT}/daq/xdata/test/data/soap/MonitorableMissingFieldType.xml", &b, importedXML, exportedXML), xdata::exception::Exception );
}

void SOAPSerializerTestSuite::serializeMonitorableWrongElement()
{
	xdata::Monitorable<TestMonitorable> b;
	CPPUNIT_ASSERT_THROW( this->validate("${XDAQ_ROOT}/daq/xdata/test/data/soap/MonitorableWrongElement.xml", &b, importedXML, exportedXML) , xdata::exception::Exception);
}

void SOAPSerializerTestSuite::serializeMonitorableEmpty()
{
	xdata::Monitorable<TestEmptyMonitorable> b;
	CPPUNIT_ASSERT_NO_THROW( this->validate("${XDAQ_ROOT}/daq/xdata/test/data/soap/MonitorableEmpty.xml", &b, importedXML, exportedXML));
	CPPUNIT_ASSERT_EQUAL( importedXML, exportedXML);
}

void SOAPSerializerTestSuite::serializeMonitorableContentMismatchFieldName()
{
	xdata::Monitorable<TestMonitorable> b;
	CPPUNIT_ASSERT_THROW( this->validate("${XDAQ_ROOT}/daq/xdata/test/data/soap/MonitorableContentMismatchFieldName.xml", &b, importedXML, exportedXML) , xdata::exception::Exception);
}
void SOAPSerializerTestSuite::serializeMonitorableContentMismatchFieldType()
{
	xdata::Monitorable<TestMonitorable> b;
	CPPUNIT_ASSERT_THROW( this->validate("${XDAQ_ROOT}/daq/xdata/test/data/soap/MonitorableContentMismatchFieldType.xml", &b, importedXML, exportedXML) , xdata::exception::Exception);
}

void SOAPSerializerTestSuite::serializeMonitorableWithVector()
{
	xdata::Monitorable<TestMonitorableWithVector> b;
	CPPUNIT_ASSERT_NO_THROW( this->validate("${XDAQ_ROOT}/daq/xdata/test/data/soap/MonitorableWithVector.xml", &b, importedXML, exportedXML));
	CPPUNIT_ASSERT_EQUAL( importedXML, exportedXML);
}

void SOAPSerializerTestSuite::serializeMonitorableWithBag()
{
	xdata::Monitorable<TestMonitorableWithBag> b;
	CPPUNIT_ASSERT_NO_THROW( this->validate("${XDAQ_ROOT}/daq/xdata/test/data/soap/MonitorableWithBag.xml", &b, importedXML, exportedXML) );
	CPPUNIT_ASSERT_EQUAL( importedXML, exportedXML);
}

void SOAPSerializerTestSuite::serializeVectorWithMonitorable()
{
	xdata::Vector<xdata::Monitorable<TestMonitorable> > v;
	CPPUNIT_ASSERT_NO_THROW( this->validate("${XDAQ_ROOT}/daq/xdata/test/data/soap/VectorWithMonitorable.xml", &v, importedXML, exportedXML));

	CPPUNIT_ASSERT_EQUAL( importedXML, exportedXML);
}

void SOAPSerializerTestSuite::serializeMonitorableWithPropertiesOnly()
{
	TestMonitorableWithPropertiesOnly b;
	CPPUNIT_ASSERT_NO_THROW( this->validate("${XDAQ_ROOT}/daq/xdata/test/data/soap/MonitorableWithPropertiesOnly.xml", &b, importedXML, exportedXML ));
	CPPUNIT_ASSERT_EQUAL( importedXML, exportedXML);
}

void SOAPSerializerTestSuite::serializeMonitorableWithPropertiesAndFields()
{
	TestMonitorableWithPropertiesAndFields b;
	CPPUNIT_ASSERT_NO_THROW( this->validate("${XDAQ_ROOT}/daq/xdata/test/data/soap/MonitorableWithPropertiesAndFields.xml", &b, importedXML, exportedXML ));
	CPPUNIT_ASSERT_EQUAL( importedXML, exportedXML);
}

void SOAPSerializerTestSuite::serializeMonitorableWithMissingPropertyName()
{
	TestMonitorableWithPropertiesOnly b;
	CPPUNIT_ASSERT_THROW( this->validate("${XDAQ_ROOT}/daq/xdata/test/data/soap/MonitorableWithMissingPropertyName.xml", &b, importedXML, exportedXML), xdata::exception::Exception );
}

void SOAPSerializerTestSuite::serializeMonitorableWithWrongPropertyType()
{
	TestMonitorableWithPropertiesOnly b;
	CPPUNIT_ASSERT_THROW( this->validate("${XDAQ_ROOT}/daq/xdata/test/data/soap/MonitorableWithWrongPropertyType.xml", &b, importedXML, exportedXML), xdata::exception::Exception );
}
*/

//
// Tests forInfospace
//

void SOAPSerializerTestSuite::serializeInfoSpace()
{
	xdata::Integer a;
        xdata::Float b;
	xdata::Boolean c;
        xdata::String d;
	xdata::InfoSpace * is = xdata::InfoSpace::get("test");
	is->fireItemAvailable("a", &a , is);
	is->fireItemAvailable("b", &b , is);
	is->fireItemAvailable("c", &c , is);
	is->fireItemAvailable("d", &d , is);
	
	CPPUNIT_ASSERT_NO_THROW( this->validate("${XDAQ_ROOT}/daq/xdata/test/data/soap/Bag.xml", is, importedXML, exportedXML ));
	CPPUNIT_ASSERT_EQUAL( importedXML, exportedXML);
	
	xdata::InfoSpace::remove("test");
}

void SOAPSerializerTestSuite::serializeInfoSpaceWrongValue()
{
	xdata::Integer a;
        xdata::Float b;
	xdata::Boolean c;
        xdata::String d;
	xdata::InfoSpace * is = xdata::InfoSpace::get("test");
	is->fireItemAvailable("a", &a , is);
	is->fireItemAvailable("b", &b , is);
	is->fireItemAvailable("c", &c , is);
	is->fireItemAvailable("d", &d , is);
	CPPUNIT_ASSERT_THROW( this->validate("${XDAQ_ROOT}/daq/xdata/test/data/soap/BagWrongValue.xml", is, importedXML, exportedXML), xdata::exception::Exception );
	xdata::InfoSpace::remove("test");
}


void SOAPSerializerTestSuite::serializeInfoSpaceMissingType()
{
	xdata::Integer a;
        xdata::Float b;
	xdata::Boolean c;
        xdata::String d;
	xdata::InfoSpace * is = xdata::InfoSpace::get("test");
	is->fireItemAvailable("a", &a , is);
	is->fireItemAvailable("b", &b , is);
	is->fireItemAvailable("c", &c , is);
	is->fireItemAvailable("d", &d , is);
	CPPUNIT_ASSERT_THROW( this->validate("${XDAQ_ROOT}/daq/xdata/test/data/soap/BagMissingType.xml", is, importedXML, exportedXML), xdata::exception::Exception );
	xdata::InfoSpace::remove("test");
}


void SOAPSerializerTestSuite::serializeInfoSpaceWrongType()
{
	xdata::Integer a;
        xdata::Float b;
	xdata::Boolean c;
        xdata::String d;
	xdata::InfoSpace * is = xdata::InfoSpace::get("test");
	is->fireItemAvailable("a", &a , is);
	is->fireItemAvailable("b", &b , is);
	is->fireItemAvailable("c", &c , is);
	is->fireItemAvailable("d", &d , is);
	CPPUNIT_ASSERT_THROW( this->validate("${XDAQ_ROOT}/daq/xdata/test/data/soap/BagWrongType.xml", is, importedXML, exportedXML), xdata::exception::Exception );
	xdata::InfoSpace::remove("test");
}


void SOAPSerializerTestSuite::serializeInfoSpaceWrongFieldType()
{
	xdata::Integer a;
        xdata::Float b;
	xdata::Boolean c;
        xdata::String d;
	xdata::InfoSpace * is = xdata::InfoSpace::get("test");
	is->fireItemAvailable("a", &a , is);
	is->fireItemAvailable("b", &b , is);
	is->fireItemAvailable("c", &c , is);
	is->fireItemAvailable("d", &d , is);
	CPPUNIT_ASSERT_THROW( this->validate("${XDAQ_ROOT}/daq/xdata/test/data/soap/BagMissingFieldType.xml", is, importedXML, exportedXML), xdata::exception::Exception );
	xdata::InfoSpace::remove("test");
}

void SOAPSerializerTestSuite::serializeInfoSpaceWrongElement()
{
	xdata::Integer a;
        xdata::Float b;
	xdata::Boolean c;
        xdata::String d;
	xdata::InfoSpace * is = xdata::InfoSpace::get("test");
	is->fireItemAvailable("a", &a , is);
	is->fireItemAvailable("b", &b , is);
	is->fireItemAvailable("c", &c , is);
	is->fireItemAvailable("d", &d , is);
	CPPUNIT_ASSERT_THROW( this->validate("${XDAQ_ROOT}/daq/xdata/test/data/soap/BagWrongElement.xml", is, importedXML, exportedXML) , xdata::exception::Exception);
	xdata::InfoSpace::remove("test");
}

void SOAPSerializerTestSuite::serializeInfoSpaceEmpty()
{
	xdata::InfoSpace * is = xdata::InfoSpace::get("test");	
	CPPUNIT_ASSERT_NO_THROW( this->validate("${XDAQ_ROOT}/daq/xdata/test/data/soap/BagEmpty.xml", is, importedXML, exportedXML));
	CPPUNIT_ASSERT_EQUAL( importedXML, exportedXML);
	xdata::InfoSpace::remove("test");
	
}

void SOAPSerializerTestSuite::serializeInfoSpaceContentMismatchFieldName()
{
	xdata::Integer a;
        xdata::Float b;
	xdata::Boolean c;
        xdata::String d;
	xdata::InfoSpace * is = xdata::InfoSpace::get("test");
	is->fireItemAvailable("a", &a , is);
	is->fireItemAvailable("b", &b , is);
	is->fireItemAvailable("c", &c , is);
	is->fireItemAvailable("d", &d , is);	
	CPPUNIT_ASSERT_THROW( this->validate("${XDAQ_ROOT}/daq/xdata/test/data/soap/BagContentMismatchFieldName.xml", is, importedXML, exportedXML) , xdata::exception::Exception);
	xdata::InfoSpace::remove("test");
}

void SOAPSerializerTestSuite::serializeInfoSpaceContentMismatchFieldType()
{
	xdata::Integer a;
        xdata::Float b;
	xdata::Boolean c;
        xdata::String d;
	xdata::InfoSpace * is = xdata::InfoSpace::get("test");
	is->fireItemAvailable("a", &a , is);
	is->fireItemAvailable("b", &b , is);
	is->fireItemAvailable("c", &c , is);
	is->fireItemAvailable("d", &d , is);
	CPPUNIT_ASSERT_THROW( this->validate("${XDAQ_ROOT}/daq/xdata/test/data/soap/BagContentMismatchFieldType.xml", is, importedXML, exportedXML) , xdata::exception::Exception);
	xdata::InfoSpace::remove("test");
}
