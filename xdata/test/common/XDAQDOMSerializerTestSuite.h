// $Id: XDAQDOMSerializerTestSuite.h,v 1.10 2008/07/18 15:28:14 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _XDAQDOMSerializerTestSuite_h_
#define _XDAQDOMSerializerTestSuite_h_

#include <cppunit/Exception.h>
#include <cppunit/extensions/HelperMacros.h>

#include "xdata/xdata.h"

#include "xdata/XMLDOM.h"

/* 
 * A test case that is designed to stimulate 
 * significant compiler template instantiation 
 *
 */

class XDAQDOMSerializerTestSuite : public CPPUNIT_NS::TestFixture
{
	CPPUNIT_TEST_SUITE( XDAQDOMSerializerTestSuite );
	
	// Tests for Integer
	CPPUNIT_TEST( serializeInteger );
	
	CPPUNIT_TEST( serializeIntegerWrongTag );
	CPPUNIT_TEST( serializeIntegerWrongValue );
	CPPUNIT_TEST( serializeIntegerMissingType );
	CPPUNIT_TEST( serializeIntegerMissingValue );
	CPPUNIT_TEST( serializeIntegerWrongType );
	
	// Tests for Float
	CPPUNIT_TEST( serializeFloat );
	CPPUNIT_TEST( serializeFloatWrongTag );
	CPPUNIT_TEST( serializeFloatWrongValue );
	CPPUNIT_TEST( serializeFloatMissingType );
	CPPUNIT_TEST( serializeFloatMissingValue );
	CPPUNIT_TEST( serializeFloatWrongType );
	CPPUNIT_TEST( serializeFloatTruncate );
	
	// Tests for Boolean
	CPPUNIT_TEST( serializeBoolean );
	CPPUNIT_TEST( serializeBooleanWrongTag );
	CPPUNIT_TEST( serializeBooleanWrongValue );
	CPPUNIT_TEST( serializeBooleanMissingType );
	CPPUNIT_TEST( serializeBooleanMissingValue );
	CPPUNIT_TEST( serializeBooleanWrongType );
	
	// Tests for String
	CPPUNIT_TEST( serializeString );
	CPPUNIT_TEST( serializeStringWrongTag );
	CPPUNIT_TEST( serializeStringMissingType );
	CPPUNIT_TEST( serializeStringEmptyValue );
	CPPUNIT_TEST( serializeStringWrongType );
	
	// Tests for UnsignedShort
	CPPUNIT_TEST( serializeUnsignedShort );
	CPPUNIT_TEST( serializeUnsignedShortWrongTag );
	CPPUNIT_TEST( serializeUnsignedShortWrongValue );
	CPPUNIT_TEST( serializeUnsignedShortMissingType );
	CPPUNIT_TEST( serializeUnsignedShortMissingValue );
	CPPUNIT_TEST( serializeUnsignedShortWrongType );
	
	
	// Tests for UnsignedLong
	CPPUNIT_TEST( serializeUnsignedLong );
	CPPUNIT_TEST( serializeUnsignedLongWrongTag );
	CPPUNIT_TEST( serializeUnsignedLongWrongValue );
	CPPUNIT_TEST( serializeUnsignedLongMissingType );
	CPPUNIT_TEST( serializeUnsignedLongMissingValue );
	CPPUNIT_TEST( serializeUnsignedLongWrongType );
	
	// Tests for Double
	CPPUNIT_TEST( serializeDouble );
	CPPUNIT_TEST( serializeDoubleWrongTag );
	CPPUNIT_TEST( serializeDoubleWrongValue );
	CPPUNIT_TEST( serializeDoubleMissingType );
	CPPUNIT_TEST( serializeDoubleMissingValue );
	CPPUNIT_TEST( serializeDoubleWrongType );
	CPPUNIT_TEST( serializeDoubleTruncate );
	
	// Tests for Vector
	CPPUNIT_TEST( serializeVector );
	CPPUNIT_TEST( serializeVectorWrongTag );
	CPPUNIT_TEST( serializeVectorWrongValue );
	CPPUNIT_TEST( serializeVectorMissingType );
	CPPUNIT_TEST( serializeVectorMissingValue );
	CPPUNIT_TEST( serializeVectorWrongType );
	CPPUNIT_TEST( serializeVectorMissingIndex );
	CPPUNIT_TEST( serializeVectorWrongElementType );
	CPPUNIT_TEST( serializeVectorWrongElement );
	
	// Test functions for Bag
	CPPUNIT_TEST( serializeBag);
	CPPUNIT_TEST( serializeBagWrongTag);
	CPPUNIT_TEST( serializeBagWrongValue);
	CPPUNIT_TEST( serializeBagMissingType);
	CPPUNIT_TEST( serializeBagWrongType);
	CPPUNIT_TEST( serializeBagMissingFieldName);
	CPPUNIT_TEST( serializeBagWrongFieldType);
	CPPUNIT_TEST( serializeBagWrongElement);
	CPPUNIT_TEST( serializeBagEmpty);
	CPPUNIT_TEST( serializeBagContentMismatchFieldName);
	CPPUNIT_TEST( serializeBagContentMismatchFieldType);
	CPPUNIT_TEST( serializeBagWithVector);
	CPPUNIT_TEST( serializeBagWithBag);
	CPPUNIT_TEST( serializeVectorWithBag);
	
	// Test functions for Monitorable
	CPPUNIT_TEST( serializeMonitorable);
	CPPUNIT_TEST( serializeMonitorableWrongTag);
	CPPUNIT_TEST( serializeMonitorableWrongValue);
	CPPUNIT_TEST( serializeMonitorableMissingType);
	CPPUNIT_TEST( serializeMonitorableWrongType);
	CPPUNIT_TEST( serializeMonitorableMissingFieldName);
	CPPUNIT_TEST( serializeMonitorableWrongFieldType);
	CPPUNIT_TEST( serializeMonitorableWrongElement);
	CPPUNIT_TEST( serializeMonitorableEmpty);
	CPPUNIT_TEST( serializeMonitorableContentMismatchFieldName);
	CPPUNIT_TEST( serializeMonitorableContentMismatchFieldType);
	CPPUNIT_TEST( serializeMonitorableWithVector);
	CPPUNIT_TEST( serializeMonitorableWithBag);
	CPPUNIT_TEST( serializeVectorWithMonitorable);
	
	CPPUNIT_TEST( serializeMonitorableWithPropertiesOnly);
	CPPUNIT_TEST( serializeMonitorableWithPropertiesAndFields);
	CPPUNIT_TEST( serializeMonitorableWithMissingPropertyName);
	CPPUNIT_TEST( serializeMonitorableWithWrongPropertyType);
	
	
	// Test functions for InfoSpace
	//CPPUNIT_TEST( serializeInfoSpace);
	
	CPPUNIT_TEST_SUITE_END();

protected:
  
public:
	void setUp();
	void tearDown();

protected:

	// General function. Reads a file that defines a data type. Then creates
	// a serializable. Streams the serializable to a string and compares it
	// with the content of the input file. Throws an exception if the two don't match.
	//
	void validate(const string  & filename, xdata::Serializable * serializable, string &, string &) throw (CppUnit::Exception, xdata::exception::Exception );

	// Test functions for Integer
	void serializeInteger();
	void serializeIntegerWrongTag();
	void serializeIntegerWrongValue();
	void serializeIntegerMissingType();
	void serializeIntegerMissingValue();
	void serializeIntegerWrongType();
	
	// Test functions for Float
	void serializeFloat();
	void serializeFloatWrongTag();
	void serializeFloatWrongValue();
	void serializeFloatMissingType();
	void serializeFloatMissingValue();
	void serializeFloatWrongType();
	void serializeFloatTruncate();
	
	// Test functions for Boolean
	void serializeBoolean();
	void serializeBooleanWrongTag();
	void serializeBooleanWrongValue();
	void serializeBooleanMissingType();
	void serializeBooleanMissingValue();
	void serializeBooleanWrongType();

	// Test functions for String
	void serializeString();
	void serializeStringWrongTag();
	void serializeStringMissingType();
	void serializeStringEmptyValue();
	void serializeStringWrongType();
	
	// Test functions for UnsignedShort
	void serializeUnsignedShort();
	void serializeUnsignedShortWrongTag();
	void serializeUnsignedShortWrongValue();
	void serializeUnsignedShortMissingType();
	void serializeUnsignedShortMissingValue();
	void serializeUnsignedShortWrongType();
	
	// Test functions for UnsignedLong
	void serializeUnsignedLong();
	void serializeUnsignedLongWrongTag();
	void serializeUnsignedLongWrongValue();
	void serializeUnsignedLongMissingType();
	void serializeUnsignedLongMissingValue();
	void serializeUnsignedLongWrongType();
	
	// Test functions for Double
	void serializeDouble();
	void serializeDoubleWrongTag();
	void serializeDoubleWrongValue();
	void serializeDoubleMissingType();
	void serializeDoubleMissingValue();
	void serializeDoubleWrongType();
	void serializeDoubleTruncate();
	
	// Test functions for Vector
	void serializeVector();
	void serializeVectorWrongTag();
	void serializeVectorWrongValue();
	void serializeVectorMissingType();
	void serializeVectorMissingValue();
	void serializeVectorWrongType();
	void serializeVectorMissingIndex();
	void serializeVectorWrongElementType();
	void serializeVectorWrongElement();
	
	// Test functions for Bag
	void serializeBag();
	void serializeBagWrongTag();
	void serializeBagWrongValue();
	void serializeBagMissingType();
	void serializeBagWrongType();
	void serializeBagMissingFieldName();
	void serializeBagWrongFieldType();
	void serializeBagWrongElement();
	void serializeBagEmpty();
	void serializeBagContentMismatchFieldName();
	void serializeBagContentMismatchFieldType();
	void serializeBagWithVector();
	void serializeBagWithBag();
	void serializeVectorWithBag();
	
	// Test functions for Monitorable
	void serializeMonitorable();
	void serializeMonitorableWrongTag();
	void serializeMonitorableWrongValue();
	void serializeMonitorableMissingType();
	void serializeMonitorableWrongType();
	void serializeMonitorableMissingFieldName();
	void serializeMonitorableWrongFieldType();
	void serializeMonitorableWrongElement();
	void serializeMonitorableEmpty();
	void serializeMonitorableContentMismatchFieldName();
	void serializeMonitorableContentMismatchFieldType();
	void serializeMonitorableWithVector();
	void serializeMonitorableWithBag();
	void serializeVectorWithMonitorable();
	
	void serializeMonitorableWithPropertiesOnly();
	void serializeMonitorableWithPropertiesAndFields();
	void serializeMonitorableWithMissingPropertyName();
	void serializeMonitorableWithWrongPropertyType();
	
	// Test functions for InfoSpace
	void serializeInfoSpace();
	
	xdata::XMLDOMLoader  * loader_;
	string importedXML;
	string exportedXML;

};


#endif
