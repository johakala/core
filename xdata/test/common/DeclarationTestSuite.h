// $Id: DeclarationTestSuite.h,v 1.3 2008/07/18 15:28:14 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef DeclarationTestSuite_H
#define DeclarationTestSuite_H

#include <cppunit/extensions/HelperMacros.h>

#include "xdata/xdata.h"

/* 
 * A test case that is designed to stimulate 
 * significant compiler template instantiation 
 *
 */

class DeclarationTestSuite : public CPPUNIT_NS::TestFixture
{
  CPPUNIT_TEST_SUITE( DeclarationTestSuite );
  CPPUNIT_TEST( declareInteger );
  CPPUNIT_TEST( declareFloat );
  CPPUNIT_TEST( declareUnsignedLong );
  CPPUNIT_TEST( declareUnsignedShort );
  CPPUNIT_TEST( declareBag );
  CPPUNIT_TEST( declareVectorOfInteger );
  CPPUNIT_TEST( declareVectorOfBag );
  CPPUNIT_TEST( declareVectorOfVectorOfString );
  CPPUNIT_TEST_SUITE_END();

protected:
  
public:
  void setUp();
  void tearDown();

protected:

  void declareInteger();
  void declareFloat();
  void declareUnsignedLong();
  void declareUnsignedShort();
  void declareBag();
  void declareVectorOfInteger();
  void declareVectorOfBag();
  void declareVectorOfVectorOfString();

};


#endif
