// $Id: FloatTestSuite.h,v 1.4 2008/07/18 15:28:14 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef FlaotTestSuite_H
#define FloatTestSuite_H

#include <cppunit/extensions/HelperMacros.h>

#include "xdata/Float.h"

/* 
 * A test case that is designed to produce
 * example errors and failures
 *
 */

class FloatTestSuite : public CPPUNIT_NS::TestFixture
{
  CPPUNIT_TEST_SUITE( FloatTestSuite );
  CPPUNIT_TEST( testAdd );
  CPPUNIT_TEST( testEquals );
  CPPUNIT_TEST( testNaN );
  CPPUNIT_TEST( testAssignNaN );
  CPPUNIT_TEST_SUITE_END();

protected:
  xdata::Float i1;
  xdata::Float i2;
  
public:
  void setUp();
  void tearDown();

protected:

  void testAdd();
  void testEquals();
  void testNaN();
  void testAssignNaN();
};


#endif
