// $Id: SOAPSerializerTestSuite.h,v 1.9 2008/07/18 15:28:14 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _SOAPSerializerTestSuite_h_
#define _SOAPSerializerTestSuite_h_

#include <cppunit/Exception.h>
#include <cppunit/extensions/HelperMacros.h>

#include "xdata/xdata.h"
#include "xdata/XMLDOM.h"

/* 
 * A test case that is designed to stimulate 
 * significant compiler template instantiation 
 *
 */

class SOAPSerializerTestSuite : public CPPUNIT_NS::TestFixture
{
	CPPUNIT_TEST_SUITE( SOAPSerializerTestSuite );
	
	// Tests for Integer
	CPPUNIT_TEST( serializeInteger );
	CPPUNIT_TEST( serializeIntegerNaN );
	CPPUNIT_TEST( serializeIntegerInfinite );
	CPPUNIT_TEST( serializeInteger32 );
	CPPUNIT_TEST( serializeInteger32NaN );
	CPPUNIT_TEST( serializeInteger32Infinite );
	CPPUNIT_TEST( serializeInteger64 );
	CPPUNIT_TEST( serializeInteger64NaN );
	CPPUNIT_TEST( serializeInteger64Infinite );
	CPPUNIT_TEST( serializeUnsignedInteger );
	CPPUNIT_TEST( serializeUnsignedInteger32 );
	CPPUNIT_TEST( serializeUnsignedInteger64 );
	
	CPPUNIT_TEST( serializeIntegerWrongValue );
	CPPUNIT_TEST( serializeIntegerMissingType );
	CPPUNIT_TEST( serializeIntegerMissingValue );
	CPPUNIT_TEST( serializeIntegerWrongType );
	
	// Tests for Float
	CPPUNIT_TEST( serializeFloat );
	CPPUNIT_TEST( serializeFloatNaN );
	CPPUNIT_TEST( serializeFloatInfinite );
	CPPUNIT_TEST( serializeFloatWrongValue );
	CPPUNIT_TEST( serializeFloatMissingType );
	CPPUNIT_TEST( serializeFloatMissingValue );
	CPPUNIT_TEST( serializeFloatWrongType );
	CPPUNIT_TEST( serializeFloatTruncate );
	
	// Tests for Boolean
	CPPUNIT_TEST( serializeBoolean );
	CPPUNIT_TEST( serializeBooleanNaN );
	CPPUNIT_TEST( serializeBooleanWrongValue );
	CPPUNIT_TEST( serializeBooleanMissingType );
	CPPUNIT_TEST( serializeBooleanMissingValue );
	CPPUNIT_TEST( serializeBooleanWrongType );
	
	// Tests for TimeVal
	CPPUNIT_TEST( serializeTimeVal );
	CPPUNIT_TEST( serializeTimeValNaN );
	CPPUNIT_TEST( serializeTimeValWrongValue );
	CPPUNIT_TEST( serializeTimeValMissingType );
	CPPUNIT_TEST( serializeTimeValMissingValue );
	CPPUNIT_TEST( serializeTimeValWrongType );
	
	// Tests for String
	CPPUNIT_TEST( serializeString );
	CPPUNIT_TEST( serializeStringMissingType );
	CPPUNIT_TEST( serializeStringEmptyValue );
	CPPUNIT_TEST( serializeStringWrongType );
	
	// Tests for UnsignedShort
	CPPUNIT_TEST( serializeUnsignedShort );
	CPPUNIT_TEST( serializeUnsignedShortWrongValue );
	CPPUNIT_TEST( serializeUnsignedShortMissingType );
	CPPUNIT_TEST( serializeUnsignedShortMissingValue );
	CPPUNIT_TEST( serializeUnsignedShortWrongType );
	
	// Tests for UnsignedLong
	CPPUNIT_TEST( serializeUnsignedLong );
	CPPUNIT_TEST( serializeUnsignedLongWrongValue );
	CPPUNIT_TEST( serializeUnsignedLongMissingType );
	CPPUNIT_TEST( serializeUnsignedLongMissingValue );
	CPPUNIT_TEST( serializeUnsignedLongWrongType );
	
	// Tests for Double
	CPPUNIT_TEST( serializeDouble );
	CPPUNIT_TEST( serializeDoubleNaN );
	CPPUNIT_TEST( serializeDoubleInfinite );
	CPPUNIT_TEST( serializeDoubleWrongValue );
	CPPUNIT_TEST( serializeDoubleMissingType );
	CPPUNIT_TEST( serializeDoubleMissingValue );
	CPPUNIT_TEST( serializeDoubleWrongType );
	CPPUNIT_TEST( serializeDoubleTruncate );
	
	// Tests for Vector
	CPPUNIT_TEST( serializeVector );
	CPPUNIT_TEST( serializeVectorWrongValue );
	CPPUNIT_TEST( serializeVectorMissingType );
	CPPUNIT_TEST( serializeVectorMissingValue );
	CPPUNIT_TEST( serializeVectorWrongType );
	CPPUNIT_TEST( serializeVectorMissingIndex );
	CPPUNIT_TEST( serializeVectorWrongElementType );
	CPPUNIT_TEST( serializeVectorWrongElement );
	
	// Test functions for Bag
	CPPUNIT_TEST( serializeBag);
	CPPUNIT_TEST( serializeBagWrongValue);
	CPPUNIT_TEST( serializeBagMissingType);
	CPPUNIT_TEST( serializeBagWrongType);
	CPPUNIT_TEST( serializeBagWrongFieldType);
	CPPUNIT_TEST( serializeBagWrongElement);
	CPPUNIT_TEST( serializeBagEmpty);
	CPPUNIT_TEST( serializeBagContentMismatchFieldName);
	CPPUNIT_TEST( serializeBagContentMismatchFieldType);
	CPPUNIT_TEST( serializeBagWithVector);
	CPPUNIT_TEST( serializeBagWithBag);
	CPPUNIT_TEST( serializeVectorWithBag);
	
	// Test functions for Monitorable
	/*
	CPPUNIT_TEST( serializeMonitorable);
	CPPUNIT_TEST( serializeMonitorableWrongValue);
	CPPUNIT_TEST( serializeMonitorableMissingType);
	CPPUNIT_TEST( serializeMonitorableWrongType);
	CPPUNIT_TEST( serializeMonitorableWrongFieldType);
	CPPUNIT_TEST( serializeMonitorableWrongElement);
	CPPUNIT_TEST( serializeMonitorableEmpty);
	CPPUNIT_TEST( serializeMonitorableContentMismatchFieldName);
	CPPUNIT_TEST( serializeMonitorableContentMismatchFieldType);
	CPPUNIT_TEST( serializeMonitorableWithVector);
	CPPUNIT_TEST( serializeMonitorableWithBag);
	CPPUNIT_TEST( serializeVectorWithMonitorable);
	
	CPPUNIT_TEST( serializeMonitorableWithPropertiesOnly);
	CPPUNIT_TEST( serializeMonitorableWithPropertiesAndFields);
	CPPUNIT_TEST( serializeMonitorableWithMissingPropertyName);
	CPPUNIT_TEST( serializeMonitorableWithWrongPropertyType);
	*/
	
	// Test functions for InfoSpace
	CPPUNIT_TEST( serializeInfoSpace);
	CPPUNIT_TEST( serializeInfoSpaceWrongValue);
	CPPUNIT_TEST( serializeInfoSpaceMissingType);
	CPPUNIT_TEST( serializeInfoSpaceWrongType);
	CPPUNIT_TEST( serializeInfoSpaceWrongFieldType);
	CPPUNIT_TEST( serializeInfoSpaceWrongElement);
	CPPUNIT_TEST( serializeInfoSpaceEmpty);
	CPPUNIT_TEST( serializeInfoSpaceContentMismatchFieldName);
	CPPUNIT_TEST( serializeInfoSpaceContentMismatchFieldType);
	
	CPPUNIT_TEST_SUITE_END();

protected:
  
public:
	void setUp();
	void tearDown();

protected:

	// General function. Reads a file that defines a data type. Then creates
	// a serializable. Streams the serializable to a string and compares it
	// with the content of the input file. Throws an exception if the two don't match.
	//
	void validate
	(
		const std::string  & filename, 
		xdata::Serializable * serializable, 
		std::string &, 
		std::string &
	) throw (CppUnit::Exception, xdata::exception::Exception );

	// Test functions for Integer
	void serializeInteger();
	void serializeIntegerNaN();
	void serializeIntegerInfinite();
	void serializeInteger32();
	void serializeInteger32NaN();
	void serializeInteger32Infinite();
	void serializeInteger64();
	void serializeInteger64NaN();
	void serializeInteger64Infinite();
	void serializeUnsignedInteger();
	void serializeUnsignedInteger32();
	void serializeUnsignedInteger64();
	
	void serializeIntegerWrongValue();
	void serializeIntegerMissingType();
	void serializeIntegerMissingValue();
	void serializeIntegerWrongType();
	
	// Test functions for Float
	void serializeFloat();
	void serializeFloatNaN();
	void serializeFloatInfinite();
	void serializeFloatWrongValue();
	void serializeFloatMissingType();
	void serializeFloatMissingValue();
	void serializeFloatWrongType();
	void serializeFloatTruncate();
	
	// Test functions for Boolean
	void serializeBoolean();
	void serializeBooleanNaN();
	void serializeBooleanWrongValue();
	void serializeBooleanMissingType();
	void serializeBooleanMissingValue();
	void serializeBooleanWrongType();
	
	// Test functions for TimeVal
	void serializeTimeVal();
	void serializeTimeValNaN();
	void serializeTimeValWrongValue();
	void serializeTimeValMissingType();
	void serializeTimeValMissingValue();
	void serializeTimeValWrongType();

	// Test functions for String
	void serializeString();
	void serializeStringMissingType();
	void serializeStringEmptyValue();
	void serializeStringWrongType();
	
	// Test functions for UnsignedShort
	void serializeUnsignedShort();
	void serializeUnsignedShortWrongValue();
	void serializeUnsignedShortMissingType();
	void serializeUnsignedShortMissingValue();
	void serializeUnsignedShortWrongType();
	
	// Test functions for UnsignedLong
	void serializeUnsignedLong();
	void serializeUnsignedLongWrongValue();
	void serializeUnsignedLongMissingType();
	void serializeUnsignedLongMissingValue();
	void serializeUnsignedLongWrongType();
	
	// Test functions for Double
	void serializeDouble();
	void serializeDoubleNaN();
	void serializeDoubleInfinite();
	void serializeDoubleWrongValue();
	void serializeDoubleMissingType();
	void serializeDoubleMissingValue();
	void serializeDoubleWrongType();
	void serializeDoubleTruncate();
	
	// Test functions for Vector
	void serializeVector();
	void serializeVectorWrongValue();
	void serializeVectorMissingType();
	void serializeVectorMissingValue();
	void serializeVectorWrongType();
	void serializeVectorMissingIndex();
	void serializeVectorWrongElementType();
	void serializeVectorWrongElement();
	
	// Test functions for Bag
	void serializeBag();
	void serializeBagWrongValue();
	void serializeBagMissingType();
	void serializeBagWrongType();
	void serializeBagWrongFieldType();
	void serializeBagWrongElement();
	void serializeBagEmpty();
	void serializeBagContentMismatchFieldName();
	void serializeBagContentMismatchFieldType();
	void serializeBagWithVector();
	void serializeBagWithBag();
	void serializeVectorWithBag();
	
	// Test functions for Monitorable
	void serializeMonitorable();
	void serializeMonitorableWrongValue();
	void serializeMonitorableMissingType();
	void serializeMonitorableWrongType();
	void serializeMonitorableWrongFieldType();
	void serializeMonitorableWrongElement();
	void serializeMonitorableEmpty();
	void serializeMonitorableContentMismatchFieldName();
	void serializeMonitorableContentMismatchFieldType();
	void serializeMonitorableWithVector();
	void serializeMonitorableWithBag();
	void serializeVectorWithMonitorable();
	
	void serializeMonitorableWithPropertiesOnly();
	void serializeMonitorableWithPropertiesAndFields();
	void serializeMonitorableWithMissingPropertyName();
	void serializeMonitorableWithWrongPropertyType();
	
	// Test functions for InfoSpace
	void serializeInfoSpace();
	void serializeInfoSpaceWrongValue();
	void serializeInfoSpaceMissingType();
	void serializeInfoSpaceWrongType();
	void serializeInfoSpaceWrongFieldType();
	void serializeInfoSpaceWrongElement();
	void serializeInfoSpaceEmpty();
	void serializeInfoSpaceContentMismatchFieldName();
	void serializeInfoSpaceContentMismatchFieldType();
	
	xdata::XMLDOMLoader  * loader_;
	std::string importedXML;
	std::string exportedXML;

};


#endif
