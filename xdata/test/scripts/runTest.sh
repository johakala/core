#!/bin/bash
#-------------------------------------------------------------------------------
# Some useful shortcuts
#-------------------------------------------------------------------------------
prepend() {
  if [ ".$LD_LIBRARY_PATH" == "." ] ; then
    export LD_LIBRARY_PATH=$1
  else
    export LD_LIBRARY_PATH=$1:${LD_LIBRARY_PATH}
  fi
}
#-------------------------------------------------------------------------------
if test ".$XDAQ_ROOT" = "."; then
  echo "Error: XDAQ_ROOT environment variable not set"
  exit 1
fi

if test ".$XDAQ_PLATFORM" = "."; then
  export XDAQ_PLATFORM=`uname -m`
  if test ".$XDAQ_PLATFORM" != ".x86_64"; then
    export XDAQ_PLATFORM=x86
  fi
  echo "Warning: PLATFORM env. variable not set, guessed to " ${XDAQ_PLATFORM}
fi

if test ".$XDAQ_OS" = "."; then
  export XDAQ_OS=`uname`
  if test ".$XDAQ_OS" = ".Linux"; then
    export XDAQ_OS=linux
  fi
  echo "Warning: OS env. variable not set, guessed to " ${XDAQ_OS}
fi

# Start with a clean slate
export LD_LIBRARY_PATH="";

# Add the various paths
prepend "$XDAQ_ROOT/daq/extern/xerces/linux${XDAQ_PLATFORM}/lib"
prepend "$XDAQ_ROOT/daq/extern/cppunit/linux${XDAQ_PLATFORM}/lib"
prepend "./:$XDAQ_ROOT/daq/xdaq/lib/linux/${XDAQ_PLATFORM}"
prepend "$XDAQ_ROOT/daq/xtuple/lib/linux/${XDAQ_PLATFORM}"
prepend "$XDAQ_ROOT/daq/extern/asyncresolv/linux${XDAQ_PLATFORM}/lib"
prepend "$XDAQ_ROOT/daq/toolbox/lib/linux/${XDAQ_PLATFORM}"
prepend "$XDAQ_ROOT/daq/xoap/lib/linux/${XDAQ_PLATFORM}"
prepend "$XDAQ_ROOT/daq/pt/lib/linux/${XDAQ_PLATFORM}"
prepend "$XDAQ_ROOT/daq/xdaq/executive/lib/linux/${XDAQ_PLATFORM}"
prepend "$XDAQ_ROOT/daq/xdaq/hyperdaq/lib/linux/${XDAQ_PLATFORM}"
prepend "$XDAQ_ROOT/daq/pt/fifo/lib/linux/${XDAQ_PLATFORM}"
prepend "$XDAQ_ROOT/daq/pt/http/lib/linux/${XDAQ_PLATFORM}"
prepend "$XDAQ_ROOT/daq/extern/log4cplus/linux${XDAQ_PLATFORM}/lib"
prepend "$XDAQ_ROOT/daq/extern/mimetic/linux${XDAQ_PLATFORM}/lib"
prepend "$XDAQ_ROOT/daq/extern/log4cplus/udpappender/lib/linux/${XDAQ_PLATFORM}"
prepend "$XDAQ_ROOT/daq/extern/log4cplus/xmlappender/lib/linux/${XDAQ_PLATFORM}"
prepend "$XDAQ_ROOT/daq/extern/cgicc/linux${XDAQ_PLATFORM}/lib"
prepend "$XDAQ_ROOT/daq/xdata/lib/linux/${XDAQ_PLATFORM}"
prepend "$XDAQ_ROOT/daq/xcept/lib/linux/${XDAQ_PLATFORM}"
prepend "$XDAQ_ROOT/daq/xgi/lib/linux/${XDAQ_PLATFORM}"
prepend "$XDAQ_ROOT/daq/xrelay/lib/linux/${XDAQ_PLATFORM}"
#prepend "/usr/local/gcc-alt-3.2/lib"
#echo "$LD_LIBRARY_PATH"

${XDAQ_ROOT}/daq/xdata/test/${XDAQ_OS}/${XDAQ_PLATFORM}/Main.exe

