// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xdata/soap/PropertiesSerializer.h"
#include "xdata/soap/NamespaceURI.h"
#include "xdata/soap/Serializer.h"
#include "xdata/Properties.h"
#include "xdata/XStr.h"

XERCES_CPP_NAMESPACE_USE

xdata::soap::PropertiesSerializer::~PropertiesSerializer()
{
}

std::string xdata::soap::PropertiesSerializer::type() const
{
	return "properties";
}

DOMElement *  xdata::soap::PropertiesSerializer::exportAll(xdata::Serializer * serializer,  xdata::Serializable * serializable, DOMElement * targetNode)
{	
	// Set type attribute for "this" bag
	DOMDocument* d = targetNode->getOwnerDocument();	
	targetNode->setAttributeNS ( XStr("http://www.w3.org/2001/XMLSchema-instance"),  XStr("xsi:type"), XStr( "soapenc:Struct" ) );
	
	// Create the properties bag
	DOMElement* propertiesBag = d->createElementNS ( targetNode->getNamespaceURI(), XStr("properties") );
	propertiesBag->setPrefix(targetNode->getPrefix());
	propertiesBag->setAttributeNS ( XStr("http://www.w3.org/2001/XMLSchema-instance"),  XStr("xsi:type"), XStr( "soapenc:Struct" ) );
	targetNode->appendChild (propertiesBag);		

	// serialization of properties
	//	
	xdata::Properties & p = dynamic_cast<xdata::Properties&>(*serializable);
	std::map<std::string, std::string, std::less<std::string> >::const_iterator mi;
	
	for (mi = p.begin(); mi != p.end(); mi++)
	{
		DOMElement* pe = d->createElementNS ( targetNode->getNamespaceURI(), XStr( (*mi).first ) );
		pe->setPrefix(targetNode->getPrefix());
		pe->setAttributeNS (  XStr("http://www.w3.org/2001/XMLSchema-instance"), XStr("xsi:type"), XStr( "xsd:string" ) );
		DOMText* t = d->createTextNode ( XStr( (*mi).second ) );
		pe->appendChild(t);		
		propertiesBag->appendChild (pe);
	}


	return 0;	
}



void xdata::soap::PropertiesSerializer::exportQualified (xdata::Serializer * serializer,  xdata::Serializable * serializable,
DOMNode* queryNode, DOMElement * resultNode) throw (xdata::exception::Exception)
{
	DOMDocument* d = resultNode->getOwnerDocument();	
	std::string type = XMLCh2String( ((DOMElement*) queryNode)->getAttributeNS (XStr(XSI_NAMESPACE_URI), XStr("type")));
	XCEPT_ASSERT (type != "", xdata::exception::Exception,  XMLCh2String(queryNode->getNodeName())+ ": Missing type attribute or type namespace wrong");

	if (type != "soapenc:Struct")
	{
		std::string msg = "Export type mismatch for tag ";
		msg += XMLCh2String(queryNode->getNodeName());
		msg += ", expected type is soapenc:Struct, received type is ";		
		msg += type;
		XCEPT_RAISE (xdata::exception::Exception, msg);	
	}
	
	resultNode->setAttributeNS ( XStr("http://www.w3.org/2001/XMLSchema-instance"),  XStr("xsi:type"), XStr( "soapenc:Struct" ) );

	DOMNodeList* children = queryNode->getChildNodes();
	for (unsigned int i = 0; i < children->getLength(); i++)
	{
		DOMNode* current = children->item(i);

		if (current->getNodeType() == DOMNode::ELEMENT_NODE)
		{
			std::string currentName = XMLCh2String(current->getLocalName());
			
			// Serialize properties: for the future the name should be scoped like xdata::properties
			// to avoid conflicts with names that the user puts
			//
			if (currentName == "properties")
			{
				// Create the properties bag
				DOMElement* propertiesBag = d->createElementNS ( resultNode->getNamespaceURI(), XStr("properties") );
				propertiesBag->setPrefix(resultNode->getPrefix());
				propertiesBag->setAttributeNS ( XStr("http://www.w3.org/2001/XMLSchema-instance"),  XStr("xsi:type"), XStr( "soapenc:Struct" ) );
				resultNode->appendChild (propertiesBag);
			
				xdata::Properties* m = dynamic_cast<xdata::Properties*>(serializable);
				// loop over children of 'current' dom node
				DOMNodeList* propertiesList = current->getChildNodes();
				for (unsigned int i = 0; i < propertiesList->getLength(); i++)
				{
					DOMNode* currentProperty = propertiesList->item(i);
					if (currentProperty->getNodeType() == DOMNode::ELEMENT_NODE)
					{
						std::string propertyName = XMLCh2String( currentProperty->getLocalName() );
						std::string propertyType = XMLCh2String( ((DOMElement*) currentProperty)->getAttributeNS (XStr(XSI_NAMESPACE_URI), XStr("type")));
						if ( propertyType != "xsd:string")
						{
							std::string msg = "Wrong property type ";
							msg += propertyType;
							msg += " for ";
							msg += propertyName;
							XCEPT_RAISE( xdata::exception::Exception,msg);						
						}
						// Error handling to be done: missing name attribute for example						
						
						if (m->hasProperty(propertyName))
						{
							// set the new value
							std::string propertyValue =  m->getProperty(propertyName);
							DOMElement* pe = d->createElementNS ( resultNode->getNamespaceURI(), XStr( propertyName ) );
							pe->setPrefix(resultNode->getPrefix());
							pe->setAttributeNS ( XStr("http://www.w3.org/2001/XMLSchema-instance"),  XStr("xsi:type"), XStr( "xsd:string" ) );
							DOMText* t = d->createTextNode ( XStr( propertyValue ) );
							pe->appendChild(t);		
							propertiesBag->appendChild (pe);
						}
						else
						{
							std::string msg = "Requested non existing property name ";
							msg += propertyName;
							msg += " in exported tag name ";
							msg += XMLCh2String (queryNode->getNodeName());
							XCEPT_RAISE (xdata::exception::Exception, msg);
						}
					}
				}
			}
			
		}
	}	

}


void xdata::soap::PropertiesSerializer::import (xdata::Serializer * serializer,  xdata::Serializable * serializable, DOMNode* targetNode) throw (xdata::exception::Exception)
{
	std::string type = XMLCh2String( ((DOMElement*) targetNode)->getAttributeNS (XStr(XSI_NAMESPACE_URI), XStr("type")));
	XCEPT_ASSERT (type != "", xdata::exception::Exception,  XMLCh2String(targetNode->getNodeName())+ ": Missing type attribute or type namespace wrong");

	if (type != "soapenc:Struct")
	{
		std::string msg = "Type import mismatch. Expected type soapenc:Struct, received type ";
		msg += type;
		XCEPT_RAISE (xdata::exception::Exception, msg);	
	}

	DOMNodeList* children = targetNode->getChildNodes();
	for (unsigned int i = 0; i < children->getLength(); i++)
	{
		DOMNode* current = children->item(i);

		if (current->getNodeType() == DOMNode::ELEMENT_NODE)
		{	
			std::string currentName = XMLCh2String(current->getLocalName());

			// Import properties: for the future the name should be scoped like xdata::properties
			// to avoid conflicts with names that the user puts
			//
			if (currentName == "properties")
			{
				xdata::Properties* m = dynamic_cast<xdata::Properties*>(serializable);
				// loop over children of 'current' dom node
				DOMNodeList* propertiesList = current->getChildNodes();
				for (unsigned int i = 0; i < propertiesList->getLength(); i++)
				{
					DOMNode* currentProperty = propertiesList->item(i);
					if (currentProperty->getNodeType() == DOMNode::ELEMENT_NODE)
					{
						std::string propertyName = XMLCh2String( currentProperty->getLocalName() );						
						std::string propertyType = XMLCh2String( ((DOMElement*) currentProperty)->getAttributeNS (XStr(XSI_NAMESPACE_URI), XStr("type")));
						if ( propertyType != "xsd:string")
						{
							std::string msg = "Wrong property type ";
							msg += propertyType;
							msg += " for ";
							msg += propertyName;
							XCEPT_RAISE( xdata::exception::Exception,msg);						
						}

						std::string propertyValue;
						if (currentProperty->hasChildNodes())
						{
							propertyValue = XMLCh2String ( currentProperty->getFirstChild()->getNodeValue() );
						}
										
						m->setProperty(propertyName, propertyValue);

					}
				}
			}
			
		}
	}	
}
