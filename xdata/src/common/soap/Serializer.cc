// $Id: Serializer.cc,v 1.13 2008/07/18 15:28:13 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/


#include "xdata/soap/Serializer.h"  
#include "xdata/soap/VectorSerializer.h"  
#include "xdata/soap/IntegerSerializer.h"  
#include "xdata/soap/FloatSerializer.h" 
#include "xdata/soap/BooleanSerializer.h" 
#include "xdata/soap/StringSerializer.h"  
#include "xdata/soap/BagSerializer.h"  
#include "xdata/soap/UnsignedShortSerializer.h"  
#include "xdata/soap/UnsignedLongSerializer.h"  
#include "xdata/soap/UnsignedIntegerSerializer.h"  
#include "xdata/soap/UnsignedInteger32Serializer.h" 
#include "xdata/soap/UnsignedInteger64Serializer.h" 
#include "xdata/soap/Integer32Serializer.h" 
#include "xdata/soap/Integer64Serializer.h" 
#include "xdata/soap/DoubleSerializer.h"  
#include "xdata/soap/PropertiesSerializer.h"  
#include "xdata/soap/InfoSpaceSerializer.h"  
#include "xdata/soap/TimeValSerializer.h"  
#include "xdata/soap/MimeSerializer.h"
#include "xdata/soap/TableSerializer.h"

//
// Serializer * soapSerializer = new xdata::soap::Serializer();
// xdata::Vector<xdata::Integer> myVector; 
// Node * myNode = soapSerializer.serialize(myVector,node);
//
xdata::soap::Serializer::Serializer()
{
	// grabage collection is perfromed automatically at destruction time
	this->addObjectSerializer(new xdata::soap::VectorSerializer());
	this->addObjectSerializer(new xdata::soap::IntegerSerializer());
	this->addObjectSerializer(new xdata::soap::UnsignedShortSerializer());
	this->addObjectSerializer(new xdata::soap::UnsignedLongSerializer());
	this->addObjectSerializer(new xdata::soap::UnsignedIntegerSerializer());
	this->addObjectSerializer(new xdata::soap::UnsignedInteger32Serializer());
	this->addObjectSerializer(new xdata::soap::UnsignedInteger64Serializer());
	this->addObjectSerializer(new xdata::soap::Integer32Serializer());
	this->addObjectSerializer(new xdata::soap::Integer64Serializer());
	this->addObjectSerializer(new xdata::soap::FloatSerializer());
	this->addObjectSerializer(new xdata::soap::DoubleSerializer());
	this->addObjectSerializer(new xdata::soap::BooleanSerializer());
	this->addObjectSerializer(new xdata::soap::StringSerializer());
	this->addObjectSerializer(new xdata::soap::BagSerializer());
	this->addObjectSerializer(new xdata::soap::PropertiesSerializer());
	this->addObjectSerializer(new xdata::soap::InfoSpaceSerializer());
	this->addObjectSerializer(new xdata::soap::TimeValSerializer());
	this->addObjectSerializer(new xdata::soap::MimeSerializer());
	this->addObjectSerializer(new xdata::soap::TableSerializer());
}	
	
DOMElement * xdata::soap::Serializer::exportAll
(
	xdata::Serializable * s,
	DOMElement * targetNode,
	bool createNS
) 
	throw (xdata::exception::Exception)
{
	// Assume that the namespace of the given targetNode has been set outside.
	// Resude the namespace of this node 
	
	if (XMLCh2String(targetNode->getNamespaceURI()) == "")
	{
		XCEPT_RAISE (xdata::exception::Exception, "Missing namespace declaration in soap serializer export all");
	}

	if (createNS == true)
	{
		// Add the namespaces needed for SOAP encoding	
		targetNode->setAttribute (xdata::XStr("xmlns:soapenc"),xdata::XStr("http://schemas.xmlsoap.org/soap/encoding/")); 
		targetNode->setAttribute (xdata::XStr("xmlns:xsi"),xdata::XStr("http://www.w3.org/2001/XMLSchema-instance"));
		targetNode->setAttribute (xdata::XStr("xmlns:xsd"),xdata::XStr("http://www.w3.org/2001/XMLSchema"));  
		// targetNode->setAttribute (xdata::XStr("xmlns:xdata"),xdata::XStr("http://xdaq.web.cern.ch/xdaq/xsd/2006/xdata-10.xsd"));  
	}
	
	xdata::soap::ObjectSerializer * os = dynamic_cast<xdata::soap::ObjectSerializer*>(this->getObjectSerializer(s->type()));
	return os->exportAll(this, s, targetNode);
}
		
void xdata::soap::Serializer::exportQualified 
(
	xdata::Serializable * s,
	DOMNode* queryNode,
	DOMElement* resultNode,
	bool createNS
) 
	throw (xdata::exception::Exception) 
{
	if (XMLCh2String(resultNode->getNamespaceURI()) == "")
	{
		XCEPT_RAISE (xdata::exception::Exception, "Missing namespace declaration in soap serializer export qualified");
	}
	
	if (createNS == true)
	{
		// Add the namespaces needed for SOAP encoding	
		resultNode->setAttribute (xdata::XStr("xmlns:soapenc"),xdata::XStr("http://schemas.xmlsoap.org/soap/encoding/")); 
		resultNode->setAttribute (xdata::XStr("xmlns:xsi"),xdata::XStr("http://www.w3.org/2001/XMLSchema-instance")); 
		resultNode->setAttribute (xdata::XStr("xmlns:xsd"),xdata::XStr("http://www.w3.org/2001/XMLSchema")); 
		// targetNode->setAttribute (xdata::XStr("xmlns:xdata"),xdata::XStr("http://xdaq.web.cern.ch/xdaq/xsd/2006/xdata-10.xsd"));  
	}
	
	xdata::soap::ObjectSerializer * os = dynamic_cast<xdata::soap::ObjectSerializer*>(this->getObjectSerializer(s->type()));	
	return os->exportQualified(this, s, queryNode, resultNode);
}

void xdata::soap::Serializer::import 
(
	xdata::Serializable * s,
	DOMNode* targetNode
) 
	throw (xdata::exception::Exception)
{
	if (XMLCh2String(targetNode->getNamespaceURI()) == "")
	{
		XCEPT_RAISE (xdata::exception::Exception, "Missing namespace declaration in soap serializer import");
	}
	
	xdata::soap::ObjectSerializer * os = dynamic_cast<xdata::soap::ObjectSerializer*>(this->getObjectSerializer(s->type()));
	return os->import(this, s, targetNode);
}
