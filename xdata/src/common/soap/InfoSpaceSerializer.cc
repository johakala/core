// $Id: InfoSpaceSerializer.cc,v 1.9 2008/07/18 15:28:13 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include <sstream>

#include "xdata/soap/InfoSpaceSerializer.h"
#include "xdata/soap/Serializer.h"
#include "xdata/soap/NamespaceURI.h"
#include "xdata/XStr.h"

xdata::soap::InfoSpaceSerializer::~InfoSpaceSerializer()
{
}

std::string xdata::soap::InfoSpaceSerializer::type() const
{
	return "infospace";
}


DOMElement *  xdata::soap::InfoSpaceSerializer::exportAll(xdata::Serializer * serializer,  xdata::Serializable * serializable, DOMElement * targetNode)
{
	DOMDocument* d = targetNode->getOwnerDocument();
	targetNode->setAttributeNS ( XStr("http://www.w3.org/2001/XMLSchema-instance"),  XStr("xsi:type"), XStr( "soapenc:Struct" ) );

	std::map< std::string, xdata::Serializable*, std::less<std::string> > * b = dynamic_cast<std::map< std::string, xdata::Serializable*, std::less<std::string> >* >(serializable);
	std::map< std::string, xdata::Serializable*, std::less<std::string> >::iterator i;
	for ( i = b->begin(); i != b->end(); i++)
	{
		// fire listener before export
		dynamic_cast<xdata::InfoSpace*>(serializable)->fireItemValueRetrieve((*i).first, this );

		xdata::Serializable* var = (*i).second;
                try {
                  DOMElement* e = d->createElementNS ( targetNode->getNamespaceURI(), XStr( (*i).first ) );
                  e->setPrefix(targetNode->getPrefix());
                  targetNode->appendChild(e);
                  //DOMElement * child = dynamic_cast<xdata::soap::Serializer*>(serializer)->exportAll(var, e);
                  dynamic_cast<xdata::soap::Serializer*>(serializer)->exportAll(var, e);
                }
                catch( xercesc_3_1::DOMException e) {
                  std::stringstream msg;
                  msg << "failed to create DOM element for item '" << (*i).first << "' with: " << xdata::XMLCh2String(e.getMessage());
                  XCEPT_RAISE(xdata::exception::Exception, msg.str());
                }
 	}

	return 0;
}



void  xdata::soap::InfoSpaceSerializer::exportQualified (xdata::Serializer * serializer,  xdata::Serializable * serializable, DOMNode* queryNode, DOMElement * resultNode) throw (xdata::exception::Exception)
{
	std::string type = XMLCh2String( ((DOMElement*) queryNode)->getAttributeNS (XStr(XSI_NAMESPACE_URI), XStr("type")));
	XCEPT_ASSERT (type != "", xdata::exception::Exception,  XMLCh2String(queryNode->getNodeName())+ ": Missing type attribute or type namespace wrong");

	if (type != "soapenc:Struct")
	{
		std::string msg = "Export type mismatch for tag ";
		msg += XMLCh2String(queryNode->getNodeName());
		msg += ", expected type is soapenc:Struct, received type ";
		msg += type;
		XCEPT_RAISE (xdata::exception::Exception, msg);
	}

	DOMDocument* d = resultNode->getOwnerDocument();
	resultNode->setAttributeNS (  XStr("http://www.w3.org/2001/XMLSchema-instance"),  XStr("xsi:type"), XStr( "soapenc:Struct" ) );

	DOMNodeList* children = queryNode->getChildNodes();
	for (unsigned int i = 0; i < children->getLength(); i++)
	{
		DOMNode* current = children->item(i);

		if (current->getNodeType() == DOMNode::ELEMENT_NODE)
		{
			std::string currentName = XMLCh2String(current->getLocalName());
			std::map< std::string, xdata::Serializable*, std::less<std::string> > * b = dynamic_cast<std::map< std::string, xdata::Serializable*, std::less<std::string> >* >(serializable);
			std::map< std::string, xdata::Serializable*, std::less<std::string> >::iterator i;
			i = b->find(currentName);
			if ( i != b->end() )
			{
				// fire listener before export
				dynamic_cast<xdata::InfoSpace*>(serializable)->fireItemValueRetrieve(currentName, this );

				DOMElement* e = d->createElementNS ( resultNode->getNamespaceURI(), XStr( currentName ) );
				e->setPrefix(resultNode->getPrefix());
				resultNode->appendChild(e);

				dynamic_cast<xdata::soap::Serializer*>(serializer)->exportQualified((*i).second, current, e);

			}
			else
			{
				std::string msg = "Bag member ";
				msg += currentName;
				msg += " not exported in tag name ";
				msg += XMLCh2String (queryNode->getNodeName());
				XCEPT_RAISE (xdata::exception::Exception, msg);
			}
		}
	}
}


void  xdata::soap::InfoSpaceSerializer::import (xdata::Serializer * serializer,  xdata::Serializable * serializable, DOMNode* targetNode) throw (xdata::exception::Exception)
{
	std::string type = XMLCh2String( ((DOMElement*) targetNode)->getAttributeNS (XStr(XSI_NAMESPACE_URI), XStr("type")));
	XCEPT_ASSERT (type != "", xdata::exception::Exception,  XMLCh2String(targetNode->getNodeName())+ ": Missing type attribute or type namespace wrong");

	if (type != "soapenc:Struct")
	{
		std::string msg = "Import type mismatch in imported tag <";
		msg += XMLCh2String (targetNode->getNodeName());
		msg += ">, expected type soapenc:Struct, found ";
		msg += type;
		XCEPT_RAISE (xdata::exception::Exception, msg);
	}

	DOMNodeList* children = targetNode->getChildNodes();
	for (unsigned int i = 0; i < children->getLength(); i++)
	{
		DOMNode* current = children->item(i);

		if (current->getNodeType() == DOMNode::ELEMENT_NODE)
		{
			std::string currentName = XMLCh2String(current->getLocalName());
			std::map< std::string, xdata::Serializable*, std::less<std::string> > * b = dynamic_cast<std::map< std::string, xdata::Serializable*, std::less<std::string> >* >(serializable);
			std::map< std::string, xdata::Serializable*, std::less<std::string> >::iterator i;
			i = b->find(currentName);
			if ( i != b->end() )
			{
				dynamic_cast<xdata::soap::Serializer*>(serializer)->import((*i).second, current);
				// Fire listener
				dynamic_cast<xdata::InfoSpace*>(serializable)->fireItemValueChanged(currentName, this );

			} else
                        {
				std::string msg = "Infospace member '";
				msg += currentName;
				msg += "' not found in imported tag <";
				msg += XMLCh2String (targetNode->getNodeName());
				msg += ">";
				XCEPT_RAISE (xdata::exception::Exception, msg);
			}
		}
	}
}
