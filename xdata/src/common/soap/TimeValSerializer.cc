// $Id: TimeValSerializer.cc,v 1.3 2008/07/18 15:28:13 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xdata/soap/TimeValSerializer.h"
#include "xdata/soap/NamespaceURI.h"
#include "xdata/XStr.h"

xdata::soap::TimeValSerializer::~TimeValSerializer()
{
}

std::string xdata::soap::TimeValSerializer::type() const
{
	return "time";
}


DOMElement * xdata::soap::TimeValSerializer::exportAll
(
	xdata::Serializer * serializer,
	xdata::Serializable * serializable,
	DOMElement * targetNode
)
{
	// <Name xsi:type="xsd:dateTime">[-]CCYY-MM-DDThh:mm:ss[Z|(+|-)hh:mm]</Name>
	// Name tag has already been created outside and is given in targetNode
	// See also: [-]CCYY-MM-DDThh:mm:ss[Z|(+|-)hh:mm]
	//
	DOMDocument* d = targetNode->getOwnerDocument();	
	targetNode->setAttributeNS ( XStr("http://www.w3.org/2001/XMLSchema-instance"),  XStr("xsi:type"), XStr( "xsd:dateTime" ) );	
	xdata::TimeVal * variable = dynamic_cast<xdata::TimeVal*>(serializable);
	DOMText* t = d->createTextNode( XStr(variable->toString()) );
	targetNode->appendChild(t);
	return 0;	
}



void xdata::soap::TimeValSerializer::exportQualified
(
	xdata::Serializer * serializer,
	xdata::Serializable * serializable,
	DOMNode* queryNode,
	DOMElement * resultNode
)
	throw (xdata::exception::Exception)
{
	std::string type = XMLCh2String( ((DOMElement*) queryNode)->getAttributeNS (XStr(XSI_NAMESPACE_URI), XStr("type")));	
	XCEPT_ASSERT (type != "", xdata::exception::Exception,  XMLCh2String(queryNode->getNodeName())+ ": Missing type attribute or type namespace wrong");

	if (type == "xsd:dateTime" )
	{
		// override value in  DOM  this->toString();
		DOMDocument* d = resultNode->getOwnerDocument();
		resultNode->setAttributeNS ( XStr("http://www.w3.org/2001/XMLSchema-instance"),  XStr("xsi:type"), XStr( "xsd:dateTime" ) );
		xdata::TimeVal * variable = dynamic_cast<xdata::TimeVal*>(serializable);
		DOMText* t = d->createTextNode( XStr(variable->toString()) );
		resultNode->appendChild(t);
	} 
	else 
        {
		std::string msg = "Parameter import mismatch for tag ";
		msg += XMLCh2String (queryNode->getNodeName());
		msg +=". Expected type xsd::dateTime, received type '";
		msg += type;
		msg += "'";
		XCEPT_RAISE (xdata::exception::Exception, msg);
	}	
}


void xdata::soap::TimeValSerializer::import
(
	xdata::Serializer * serializer,
	xdata::Serializable * serializable,
	DOMNode* targetNode
) 
	throw (xdata::exception::Exception)
{
	std::string type = XMLCh2String( ((DOMElement*) targetNode)->getAttributeNS (XStr(XSI_NAMESPACE_URI), XStr("type")));		
	XCEPT_ASSERT (type != "", xdata::exception::Exception,  XMLCh2String(targetNode->getNodeName())+ ": Missing type attribute or type namespace wrong");
	
	// Check if tag is called <Parameter> and node type is ELEMENT node
	//
	if ( (type != "xsd:dateTime") || (targetNode->getNodeType() != DOMNode::ELEMENT_NODE) )
	{
		std::string msg = "Wrong node type or tag name '";
		msg += XMLCh2String(targetNode->getNodeName());
		msg += "', expected type xsd:dateTime";
		XCEPT_RAISE (xdata::exception::Exception, msg); 
	}

	xdata::TimeVal * variable = dynamic_cast<xdata::TimeVal*>(serializable);
	
	if (!targetNode->hasChildNodes())
	{
		// ERROR don't allow setting with empty tag
		std::string msg = "Missing time value for parameter: ";
		msg += XMLCh2String(targetNode->getNodeName());
                XCEPT_RAISE (xdata::exception::Exception, msg);
	} 
	else 
        {					
		std::string tmp = XMLCh2String ( targetNode->getFirstChild()->getNodeValue() );
		if (tmp != "")
		{
			variable->fromString(tmp);
		} 
		else 
		{
			std::string msg = "xsd:dateTime type for imported tag '";
			msg += XMLCh2String(targetNode->getNodeName());
			msg += "' has empty value";
			XCEPT_RAISE (xdata::exception::Exception, msg);
		}
	}	
}
