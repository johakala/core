// $Id: VectorSerializer.cc,v 1.12 2008/07/18 15:28:13 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xdata/soap/VectorSerializer.h"
#include "xdata/soap/NamespaceURI.h"
#include "xdata/XStr.h"
#include <sstream>

xdata::soap::VectorSerializer::~VectorSerializer()
{
}

std::string xdata::soap::VectorSerializer::type() const
{
	return "vector";		
}

DOMElement*  xdata::soap::VectorSerializer::exportAll(xdata::Serializer * serializer, xdata::Serializable * serializable, DOMElement * targetNode)
{
	AbstractVector * v = dynamic_cast<AbstractVector*>(serializable);
	DOMDocument* d = targetNode->getOwnerDocument();

	// Array and type sepcification
	targetNode->setAttributeNS ( XStr("http://www.w3.org/2001/XMLSchema-instance"),  XStr("xsi:type"), XStr( "soapenc:Array" ) );		
	
	// SOAP 1.1
	// Us the generic ur-type as the array type: Version 1.1: soapenc:ur-type[<size>]
	// --
	// SOAP 1.2
	// For version 1.2 there are two attributes: ENC:arraySize="<size>" ENC:itemType="xsd:<type>"
	//
	std::stringstream arrayType;
	arrayType << "xsd:ur-type[" << v->elements() << "]";
	targetNode->setAttributeNS ( XStr("http://schemas.xmlsoap.org/soap/encoding/"), XStr("soapenc:arrayType"), XStr( arrayType.str() ) );

	for (size_t i = 0; i < v->elements(); i++)
	{
		DOMElement* e = d->createElementNS (targetNode->getNamespaceURI(), XStr("item"));
		e->setPrefix(targetNode->getPrefix());
		std::stringstream position;
		position << '[' << i << ']';
		e->setAttributeNS ( XStr("http://schemas.xmlsoap.org/soap/encoding/"), XStr("soapenc:position"), XStr(position.str()));		
		targetNode->appendChild(e);		
		//DOMElement * child =  dynamic_cast<xdata::soap::Serializer*>(serializer)->exportAll(v->elementAt(i), e);
		dynamic_cast<xdata::soap::Serializer*>(serializer)->exportAll(v->elementAt(i), e);
	}
	return 0;
}



void xdata::soap::VectorSerializer::exportQualified (xdata::Serializer * serializer,  Serializable * serializable,
 DOMNode* queryNode, DOMElement* resultNode) throw (xdata::exception::Exception)
{
	DOMDocument* d = resultNode->getOwnerDocument();
	AbstractVector * v = dynamic_cast<AbstractVector*>(serializable);

	std::string type = XMLCh2String( ((DOMElement*) queryNode)->getAttributeNS (XStr(XSI_NAMESPACE_URI), XStr("type")));
	XCEPT_ASSERT (type != "", xdata::exception::Exception,  XMLCh2String(queryNode->getNodeName())+ ": Missing type attribute or type namespace wrong");

	
	if (type != "soapenc:Array")
	{
		std::string msg = "Array type import mismatch in tag name ";
		msg += XMLCh2String(queryNode->getNodeName());
		msg += ", expected type soapenc:Array, received type ";
		msg += type;
		XCEPT_RAISE (xdata::exception::Exception, msg);
	}
	
	// arrayType string is in format: soapenc:ArrayType="xsd:something[size]"
	std::string arrayType = XMLCh2String( ((DOMElement*) queryNode)->getAttributeNS (XStr(SOAPENC_NAMESPACE_URI), XStr("arrayType")));
	
	if (arrayType == "")
	{
		std::string msg = "Missing soapenc:arrayType specifier in SOAP input stream for tag ";
		msg += XMLCh2String (queryNode->getNodeName());
		XCEPT_RAISE (xdata::exception::Exception, msg);
	}
	
	
	if (arrayType.find ("xsd:ur-type[") == std::string::npos)
	{
		std::string msg = "Could not find ur-type specifier in ArrayType attribute of tag ";
		msg += XMLCh2String (queryNode->getNodeName());
		XCEPT_RAISE (xdata::exception::Exception, msg);
	}

	// resultNode->setAttribute ( XStr("xsi:type"), XStr( "soapenc:Array" ) );	
		
	std::string sizeStr = arrayType.substr(arrayType.find("[")+1, arrayType.find("]")-1);
	std::stringstream sizeStream(sizeStr);
	size_t size = 0;
	sizeStream >> size;
	
	if (size != v->elements())
	{
		// if size different than local vector size, re-export the whole vector
		// This will also put the soapenc:Array and arrayTpe attributes
		this->exportAll(serializer,  serializable, resultNode);
	}
	else
	{
		// Re-put the array size into the response
		resultNode->setAttributeNS ( XStr("http://www.w3.org/2001/XMLSchema-instance"),  XStr("xsi:type"), XStr( "soapenc:Array" ) );	
		resultNode->setAttributeNS ( XStr("http://schemas.xmlsoap.org/soap/encoding/"), XStr("soapenc:arrayType"), XStr( arrayType ) );
	
		// Export elements indicated by position attribute
		//
		DOMNodeList* children = queryNode->getChildNodes();
		for (unsigned int i = 0; i < children->getLength(); i++)
		{
			DOMNode* current = children->item(i);
			if (current->getNodeType() == DOMNode::ELEMENT_NODE)
			{
				// index is indicated as soapenc:position="[<number>]"
				std::string currentIndex = XMLCh2String( ((DOMElement*) current)->getAttributeNS (XStr(SOAPENC_NAMESPACE_URI), XStr("position")));
				size_t index = 0;
				std::string indexStr = currentIndex.substr(currentIndex.find("[")+1, currentIndex.find("]")-1);
				std::stringstream indexStream(indexStr);
				indexStream >> index;

				if (index < v->elements())
				{
					DOMElement* e = d->createElementNS (resultNode->getNamespaceURI(), XStr("item"));
					e->setPrefix(resultNode->getPrefix());
					e->setAttributeNS ( XStr("http://schemas.xmlsoap.org/soap/encoding/"), XStr("soapenc:position"), XStr(currentIndex));
					resultNode->appendChild(e);
					dynamic_cast<xdata::soap::Serializer*>(serializer)->exportQualified(v->elementAt(index), current, e);
				} 
				else
				{
					std::string msg = "Vector element position out of bounds during the qualified export of tag ";
					msg +=  XMLCh2String (current->getNodeName());
					msg += ", position: ";
					msg += currentIndex;
					XCEPT_RAISE (xdata::exception::Exception, msg);
				}
			}
		}	
	}		
}


void xdata::soap::VectorSerializer::import (xdata::Serializer * serializer,  Serializable * serializable, DOMNode* targetNode) throw (xdata::exception::Exception)
{
	AbstractVector * v = dynamic_cast<AbstractVector*>(serializable);

	std::string type = XMLCh2String( ((DOMElement*) targetNode)->getAttributeNS (XStr(XSI_NAMESPACE_URI), XStr("type")));
	XCEPT_ASSERT (type != "", xdata::exception::Exception,  XMLCh2String(targetNode->getNodeName())+ ": Missing type attribute or type namespace wrong");
	
	if (type != "soapenc:Array")
	{
		std::string msg = "Array import mismatch, expected type soapenc:Array, received type ";
		msg += type;
		msg += " for tag ";
		msg += XMLCh2String (targetNode->getNodeName());
		XCEPT_RAISE (xdata::exception::Exception, msg);
	}
	
	// arrayType string is in format: soapenc:ArrayType="xsd:something[size]"
	std::string arrayType = XMLCh2String( ((DOMElement*) targetNode)->getAttributeNS (XStr(SOAPENC_NAMESPACE_URI), XStr("arrayType")));
	
	if (arrayType == "")
	{
		std::string msg = "Missing soapenc:arrayType specifier in SOAP input stream for tag ";
		msg += XMLCh2String (targetNode->getNodeName());
		XCEPT_RAISE (xdata::exception::Exception, msg);
	}	
	
	if (arrayType.find ("xsd:ur-type[") == std::string::npos)
	{
		std::string msg = "Could not find ur-type specifier in arrayType attribute of tag ";
		msg += XMLCh2String (targetNode->getNodeName());
		XCEPT_RAISE (xdata::exception::Exception, msg);
	}
	
	std::string sizeStr = arrayType.substr(arrayType.find("[")+1, arrayType.find("]")-1);
	std::stringstream sizeStream(sizeStr);
	size_t size = 0;
	sizeStream >> size;

	DOMNodeList* l = targetNode->getChildNodes();

	// Resize vector according to received size specification
	v->setSize( size );

	// Import all children
	for (unsigned int i = 0; i < l->getLength(); i++)
	{
		DOMNode* current = l->item(i);

		if (current->getNodeType() == DOMNode::ELEMENT_NODE)
		{					
			//string indexStr = getNodeAttribute (current, "index");
			std::string currentIndex = XMLCh2String( ((DOMElement*) current)->getAttributeNS (XStr(SOAPENC_NAMESPACE_URI), XStr("position")));
			if ( currentIndex == "" )
			{

				std::string msg = "Missing soapenc:position in item in array";
				msg += XMLCh2String (targetNode->getNodeName());
				msg += ", at item ";
				std::stringstream tmpIndex;
				tmpIndex << i;
				msg += tmpIndex.str();
				XCEPT_RAISE (xdata::exception::Exception, msg);
			}

			size_t index = 0;
			std::string indexStr = currentIndex.substr(currentIndex.find("[")+1, currentIndex.find("]")-1);
			std::stringstream indexStream(indexStr);
			indexStream >> index;

			if (index < v->elements()) 
			{
				dynamic_cast<xdata::soap::Serializer*>(serializer)->import(v->elementAt(index), current);

			} else 
			{
				// TBD
				// Need to add new values from a
				// type factory
				std::string msg = "Vector element position out of bounds during the import of tag ";
				msg += XMLCh2String (targetNode->getNodeName());
				msg += ", position: ";
				msg += currentIndex;
				XCEPT_RAISE (xdata::exception::Exception, msg);
			}	
		}
	}
}

