// $Id: BagSerializer.cc,v 1.8 2008/07/18 15:28:13 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xdata/soap/BagSerializer.h"
#include "xdata/soap/NamespaceURI.h"
#include "xdata/soap/Serializer.h"
#include "xdata/XStr.h"

XERCES_CPP_NAMESPACE_USE

xdata::soap::BagSerializer::~BagSerializer()
{
}

std::string xdata::soap::BagSerializer::type() const
{
	return "bag";
}

DOMElement *  xdata::soap::BagSerializer::exportAll(xdata::Serializer * serializer,  xdata::Serializable * serializable, DOMElement * targetNode)
{
	DOMDocument* d = targetNode->getOwnerDocument();
	targetNode->setAttributeNS (  XStr("http://www.w3.org/2001/XMLSchema-instance"), XStr("xsi:type"), XStr( "soapenc:Struct" ) );	

	std::map< std::string, xdata::Serializable*, std::less<std::string> > * b = dynamic_cast<std::map< std::string, xdata::Serializable*, std::less<std::string> >* >(serializable);
	std::map< std::string, xdata::Serializable*, std::less<std::string> >::iterator i;
	for ( i = b->begin(); i != b->end(); i++)
	{
		xdata::Serializable* var = (*i).second;		
		DOMElement* e = d->createElementNS ( targetNode->getNamespaceURI(), XStr( (*i).first ) );	
		e->setPrefix(targetNode->getPrefix());	
		targetNode->appendChild(e);
		//DOMElement * child = dynamic_cast<xdata::soap::Serializer*>(serializer)->exportAll(var, e);
		dynamic_cast<xdata::soap::Serializer*>(serializer)->exportAll(var, e);
	}

	return 0;	
}



void xdata::soap::BagSerializer::exportQualified (xdata::Serializer * serializer,  xdata::Serializable * serializable, DOMNode* queryNode, DOMElement * resultNode) throw (xdata::exception::Exception)
{
	std::string type = XMLCh2String( ((DOMElement*) queryNode)->getAttributeNS (XStr(XSI_NAMESPACE_URI), XStr("type")));
	XCEPT_ASSERT (type != "", xdata::exception::Exception,  XMLCh2String(queryNode->getNodeName())+ ": Missing type attribute or type namespace wrong");

	if (type != "soapenc:Struct")
	{
		std::string msg = "Export type mismatch for tag name ";
		msg += XMLCh2String(queryNode->getNodeName());
		msg += ", expected type is soapenc:Struct, received type ";		
		msg += type;
		XCEPT_RAISE (xdata::exception::Exception, msg);	
	}
	
	DOMDocument* d = resultNode->getOwnerDocument();
	resultNode->setAttributeNS ( XStr("http://www.w3.org/2001/XMLSchema-instance"),  XStr("xsi:type"), XStr( "soapenc:Struct" ) );

	DOMNodeList* children = queryNode->getChildNodes();
	for (unsigned int i = 0; i < children->getLength(); i++)
	{
		DOMNode* current = children->item(i);
		if (current->getNodeType() == DOMNode::ELEMENT_NODE)
		{
			std::string currentName = XMLCh2String(current->getLocalName());
			std::map< std::string, xdata::Serializable*, std::less<std::string> > * b = dynamic_cast<std::map< std::string, xdata::Serializable*, std::less<std::string> >* >(serializable);
			std::map< std::string, xdata::Serializable*, std::less<std::string> >::iterator i;
			i = b->find(currentName);  
			if ( i != b->end() )
			{
				DOMElement* e = d->createElementNS ( resultNode->getNamespaceURI(), XStr( currentName ) );	
				e->setPrefix(resultNode->getPrefix());	
				resultNode->appendChild(e);
			
				dynamic_cast<xdata::soap::Serializer*>(serializer)->exportQualified((*i).second, current, e);

			} else 
			{
				std::string msg = "Name not exported: ";
				msg += currentName;
				XCEPT_RAISE (xdata::exception::Exception, msg);
			}
		}
	}	
}


void xdata::soap::BagSerializer::import (xdata::Serializer * serializer,  xdata::Serializable * serializable, DOMNode* targetNode) throw (xdata::exception::Exception)
{
	std::string type = XMLCh2String( ((DOMElement*) targetNode)->getAttributeNS (XStr(XSI_NAMESPACE_URI), XStr("type")));
	
	XCEPT_ASSERT (type != "", xdata::exception::Exception,  XMLCh2String(targetNode->getNodeName())+ ": Missing type attribute or type namespace wrong");
	
	if (type != "soapenc:Struct")
	{
		std::string msg = "Type import mismatch for imported tag name ";
		msg += XMLCh2String(targetNode->getNodeName());
		msg += ", expected type soapenc:Struct, received type ";
		msg += type;
		XCEPT_RAISE (xdata::exception::Exception, msg);	
	}

	DOMNodeList* children = targetNode->getChildNodes();
	for (unsigned int i = 0; i < children->getLength(); i++)
	{
		DOMNode* current = children->item(i);

		if (current->getNodeType() == DOMNode::ELEMENT_NODE)
		{	
			std::string currentName = XMLCh2String(current->getLocalName());
			std::map< std::string, xdata::Serializable*, std::less<std::string> > * b = dynamic_cast<std::map< std::string, xdata::Serializable*, std::less<std::string> >* >(serializable);
			std::map< std::string, xdata::Serializable*, std::less<std::string> >::iterator i;
			i = b->find(currentName);
			if ( i != b->end() )
			{
				dynamic_cast<xdata::soap::Serializer*>(serializer)->import((*i).second, current);

			} 
			else 
                        {
				std::string msg = "Bag member ";
				msg += currentName;
				msg += " not found in imported tag name ";
				msg += XMLCh2String(targetNode->getNodeName());
				XCEPT_RAISE (xdata::exception::Exception, msg);
			}
		}
	}	
}
