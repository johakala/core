// $Id: Integer64Serializer.cc,v 1.3 2008/07/18 15:28:13 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xdata/soap/Integer64Serializer.h"
#include "xdata/Integer64.h"
#include "xdata/soap/NamespaceURI.h"
#include "xdata/XStr.h"

xdata::soap::Integer64Serializer::~Integer64Serializer()
{
}

std::string xdata::soap::Integer64Serializer::type() const
{
	return "int 64";		
}


DOMElement* xdata::soap::Integer64Serializer::exportAll
(
	xdata::Serializer * serializer,  
	xdata::Serializable * serializable, 
	DOMElement * targetNode
)
{
	DOMDocument* d = targetNode->getOwnerDocument();
	targetNode->setAttributeNS ( XStr("http://www.w3.org/2001/XMLSchema-instance"),  XStr("xsi:type"), XStr( "xsd:long" ) );
	xdata::Integer64 * variable = dynamic_cast<xdata::Integer64*>(serializable);
	DOMText* t = d->createTextNode( XStr(variable->toString()) );
	targetNode->appendChild (t);
	return 0;
}



void  xdata::soap::Integer64Serializer::exportQualified 
(
	xdata::Serializer * serializer,  
	xdata::Serializable * serializable,
	DOMNode* queryNode, DOMElement* resultNode
) 
throw (xdata::exception::Exception)
{
	std::string type = XMLCh2String( ((DOMElement*) queryNode)->getAttributeNS (XStr(XSI_NAMESPACE_URI), XStr("type")));
	XCEPT_ASSERT (type != "", xdata::exception::Exception,  XMLCh2String(queryNode->getNodeName())+ ": Missing type attribute or type namespace wrong");

	if (type == "xsd:long" )
	{
		// override value in DOM  this->toString();
		DOMDocument* d = resultNode->getOwnerDocument();
		resultNode->setAttributeNS (  XStr("http://www.w3.org/2001/XMLSchema-instance"), XStr("xsi:type"), XStr( "xsd:long" ) );
		xdata::Integer64 * variable = dynamic_cast<xdata::Integer64*>(serializable);
		DOMText* t = d->createTextNode( XStr(variable->toString()) );
		resultNode->appendChild (t);
	} 
	else 
        {
		std::string msg = "Type import mismatch for tag name ";
		msg += XMLCh2String (queryNode->getNodeName());
		msg +=", expected type xsd:long, received type ";
		msg += type;
		XCEPT_RAISE (xdata::exception::Exception, msg);
	}	
}


void  xdata::soap::Integer64Serializer::import 
(
	xdata::Serializer * serializer,  
	xdata::Serializable * serializable, 
	DOMNode* targetNode
) 
throw (xdata::exception::Exception)
{
	std::string type = XMLCh2String( ((DOMElement*) targetNode)->getAttributeNS (XStr(XSI_NAMESPACE_URI), XStr("type")));	
	XCEPT_ASSERT (type != "", xdata::exception::Exception,  XMLCh2String(targetNode->getNodeName())+ ": Missing type attribute or type namespace wrong");

	// Check if tag is called <Parameter> and node type is ELEMENT node
	//
	if ( (type != "xsd:long") || (targetNode->getNodeType() != DOMNode::ELEMENT_NODE) )
	{
		std::string msg = "Type import mismatch for tag name ";
		msg += XMLCh2String(targetNode->getNodeName());
		msg += ", expected type xsd:long, received type ";
		msg += type;
		XCEPT_RAISE (xdata::exception::Exception, msg); 
	}
		
	xdata::Integer64 * variable = dynamic_cast<xdata::Integer64*>(serializable);
		
	if (!targetNode->hasChildNodes())
	{
		// ERROR don't allow setting with empty tag
		std::string msg = "Missing int 64 value for imported tag name ";
		msg += XMLCh2String(targetNode->getNodeName());
                XCEPT_RAISE (xdata::exception::Exception, msg);
	} 
	else 
        {					
		std::string tmp = XMLCh2String ( targetNode->getFirstChild()->getNodeValue() );
		variable->fromString(tmp);
	}
}
