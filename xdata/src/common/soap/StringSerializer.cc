// $Id: StringSerializer.cc,v 1.7 2008/07/18 15:28:13 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xdata/soap/StringSerializer.h"
#include "xdata/soap/NamespaceURI.h"
#include "xdata/XStr.h"

xdata::soap::StringSerializer::~StringSerializer()
{
}

std::string xdata::soap::StringSerializer::type() const
{
	return "string";		
}


DOMElement* xdata::soap::StringSerializer::exportAll(xdata::Serializer * serializer,  xdata::Serializable * serializable, DOMElement * targetNode)
{
	// <Parameter name="" type=""> value </Parameter>
	DOMDocument* d = targetNode->getOwnerDocument();
	targetNode->setAttributeNS ( XStr("http://www.w3.org/2001/XMLSchema-instance"),  XStr("xsi:type"), XStr( "xsd:string" ) );
	xdata::String * variable = dynamic_cast<xdata::String*>(serializable);
	std::string value = variable->toString();

	// Create only a text node if the string is not empty.
	if (value != "")
	{
		DOMText* t = d->createTextNode( XStr(value) );
		targetNode->appendChild(t);
	}

	return 0;
}



void xdata::soap::StringSerializer::exportQualified (xdata::Serializer * serializer,  xdata::Serializable * serializable,
DOMNode* queryNode, DOMElement* resultNode) throw (xdata::exception::Exception)
{
	std::string type = XMLCh2String( ((DOMElement*) queryNode)->getAttributeNS (XStr(XSI_NAMESPACE_URI), XStr("type")));
	XCEPT_ASSERT (type != "", xdata::exception::Exception,  XMLCh2String(queryNode->getNodeName())+ ": Missing type attribute or type namespace wrong");

	if (type == "xsd:string" )
	{
		DOMDocument* d = resultNode->getOwnerDocument();
		resultNode->setAttributeNS ( XStr("http://www.w3.org/2001/XMLSchema-instance"),  XStr("xsi:type"), XStr( "xsd:string" ) );
		xdata::String * variable = dynamic_cast<xdata::String*>(serializable);
		std::string value = variable->toString();

		// Create only a text node if the string is not empty.
		if (value != "")
		{
			DOMText* t = d->createTextNode( XStr(value) );
			resultNode->appendChild(t);
		}
	} 
	else 
        {
		std::string msg = "Type import mismatch for tag name ";
		msg += XMLCh2String (queryNode->getNodeName());
		msg +=", expected type xsd:string, received type ";
		msg += type;
		XCEPT_RAISE (xdata::exception::Exception, msg);
	}	
}


void xdata::soap::StringSerializer::import (xdata::Serializer * serializer,  xdata::Serializable * serializable, DOMNode* targetNode) throw (xdata::exception::Exception)
{
	std::string type = XMLCh2String( ((DOMElement*) targetNode)->getAttributeNS (XStr(XSI_NAMESPACE_URI), XStr("type")));	
	XCEPT_ASSERT (type != "", xdata::exception::Exception,  XMLCh2String(targetNode->getNodeName())+ ": Missing type attribute or type namespace wrong");
	
	// Check if tag is called <Parameter> and node type is ELEMENT node
	//
	if ( (type != "xsd:string") || (targetNode->getNodeType() != DOMNode::ELEMENT_NODE) )
	{
		std::string msg = "Wrong node type or imported tag name ";
		msg += XMLCh2String(targetNode->getNodeName());
		msg += ", expected type xsd:string received type ";
		msg += type;
		XCEPT_RAISE (xdata::exception::Exception, msg); 
	}
		
	xdata::String * variable = dynamic_cast<xdata::String*>(serializable);
		
	if (targetNode->hasChildNodes())
	{
		std::string tmp = XMLCh2String ( targetNode->getFirstChild()->getNodeValue() );
		variable->fromString(tmp);
	}
}
	
