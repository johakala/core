// $Id: Mime.cc,v 1.5 2008/07/18 15:28:12 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xdata/Mime.h"


xdata::Mime::Mime() throw (xdata::exception::Exception)
{
	mimeEntity_ = 0;
}

xdata::Mime::Mime(const xdata::Mime & m) throw (xdata::exception::Exception)
{
	mimeEntity_ = 0;
	*this = m;
}

xdata::Mime::~Mime() 
{
	if (mimeEntity_ != 0)
        {
                delete mimeEntity_;
        }
}

xdata::Mime & xdata::Mime::operator=(const xdata::Mime & m) throw (xdata::exception::Exception)
{ 
	if (mimeEntity_ != 0)
	{
		delete mimeEntity_;
		mimeEntity_ = 0;
	}

	if (m.mimeEntity_ != 0)
	{	
		std::stringstream s;
		s << *(m.mimeEntity_); // read into buffer
		mimeEntity_ = new mimetic::MimeEntity(s);
	}
	return *this;
}

void xdata::Mime::setEntity(mimetic::MimeEntity* entity)
{ 
	if (mimeEntity_ != 0)
	{
		delete mimeEntity_;
	}
	mimeEntity_ = entity;
}


// Comparison operators
int xdata::Mime::operator==(const xdata::Mime & m) const
{
	XCEPT_RAISE (xdata::exception::Exception, "not implemented");
	return 0;	
}

int xdata::Mime::operator!=(const xdata::Mime & m) const
{
	XCEPT_RAISE (xdata::exception::Exception, "not implemented");
	return 0;	
}


int xdata::Mime::equals(const xdata::Serializable & s) const
{
	return this->operator==(dynamic_cast<const Mime&>(s) );
}


void xdata::Mime::setValue(const xdata::Serializable & s) throw (xdata::exception::Exception)
{
	this->operator=(dynamic_cast<const xdata::Mime&>(s));
}

std::string xdata::Mime::type() const
{
	return "mime";
}


std::string xdata::Mime::toString() const throw (xdata::exception::Exception)
{
	return "xdata::mime";
}


void xdata::Mime::fromString(const std::string& value) throw (xdata::exception::Exception)
{
	XCEPT_RAISE (xdata::exception::Exception, "xdata::Mime fromString not implemented");
}

mimetic::MimeEntity* xdata::Mime::getEntity()
{
	return mimeEntity_;
}
