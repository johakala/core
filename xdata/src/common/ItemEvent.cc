// $Id: ItemEvent.cc,v 1.5 2008/07/18 15:28:12 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xdata/ItemEvent.h"

xdata::ItemEvent::ItemEvent(const std::string& type, const std::string& itemName, xdata::Serializable* item, xdata::InfoSpace* is, void* originator):
	xdata::Event(type, originator)
{
	item_ = item;
	itemName_ = itemName;
	infoSpace_ = is;
}


std::string xdata::ItemEvent::itemName()
{
	return itemName_;
}


xdata::Serializable* xdata::ItemEvent::item()
{
	return item_;
}

xdata::InfoSpace* xdata::ItemEvent::infoSpace()
{
	return infoSpace_;
}


xdata::ItemAvailableEvent::ItemAvailableEvent(const std::string& name, xdata::Serializable* item, xdata::InfoSpace* is, void* originator):
	xdata::ItemEvent("ItemAvailableEvent", name,item, is, originator)
{
}

xdata::ItemRevokedEvent::ItemRevokedEvent(const std::string& name, xdata::Serializable* item, xdata::InfoSpace* is, void* originator):
	xdata::ItemEvent("ItemRevokedEvent", name,item, is, originator)
{
}

xdata::ItemChangedEvent::ItemChangedEvent(const std::string& name, xdata::Serializable* item, xdata::InfoSpace* is, void* originator):
	xdata::ItemEvent("ItemChangedEvent", name,item, is, originator)
{
}

xdata::ItemRetrieveEvent::ItemRetrieveEvent(const std::string& name, xdata::Serializable* item, xdata::InfoSpace* is, void* originator):
	xdata::ItemEvent("ItemRetrieveEvent", name,item, is, originator)
{
}
