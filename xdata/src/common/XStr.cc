// $Id: XStr.cc,v 1.4 2008/07/18 15:28:12 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xdata/XStr.h"

// include for getting auto_ptr
#include<memory>

#include "xercesc/util/XMLString.hpp"

XERCES_CPP_NAMESPACE_USE

std::string xdata::XMLCh2String (const XMLCh* ch)
{
	if (ch == 0) return "";
	
	std::auto_ptr<char> v(XMLString::transcode (ch));
	return std::string(v.get());
}


xdata::XStr::XStr(const char* const toTranscode)
{
	// Call the private transcoding method
	fUnicodeForm = XMLString::transcode(toTranscode);
}

xdata::XStr::XStr(const std::string& toTranscode)
{
	// Call the private transcoding method
	fUnicodeForm = XMLString::transcode(toTranscode.c_str());
}

xdata::XStr::~XStr()
{
	XMLString::release(&fUnicodeForm);
}

const XMLCh* xdata::XStr::unicodeForm() const
{
	return fUnicodeForm;
}

xdata::XStr::operator const XMLCh*()
{
	return this->unicodeForm();
}

