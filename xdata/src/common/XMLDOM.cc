// $Id: XMLDOM.cc,v 1.12 2008/07/18 15:28:12 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xdata/XMLDOM.h"
#include "xdata/XStr.h"
#include "xdata/exception/Exception.h"
#include "toolbox/Runtime.h"
#include <xercesc/sax/SAXException.hpp>
#include <xercesc/sax/SAXParseException.hpp>
#include <xercesc/util/OutOfMemoryException.hpp>
#include "xoap/domutils.h"


XERCES_CPP_NAMESPACE_USE


xdata::XMLDOMErrorHandler::XMLDOMErrorHandler()
{
	this->resetErrors();
}

bool xdata::XMLDOMErrorHandler::hasErrors()
{
	return errors_;
}

std::string xdata::XMLDOMErrorHandler::getErrors()
{
	return msg_.str();
}

void xdata::XMLDOMErrorHandler::resetErrors()
{
	errors_ = false;
        msg_.clear();

}

bool xdata::XMLDOMErrorHandler::handleError(const DOMError& domError)
{
        bool continueParsing = true;
        if (errors_ == true)
        {
                msg_ << std::endl;
        }

        if (domError.getSeverity() == DOMError::DOM_SEVERITY_WARNING)
        {
                msg_ << "warning, ";
        }
        else if (domError.getSeverity() == DOMError::DOM_SEVERITY_ERROR)
        {
                msg_ << "error, ";
        }
        else
        {
                msg_ << "fatal, ";
        }

        msg_ << XMLCh2String(domError.getLocation()->getURI())
        << ", line " << domError.getLocation()->getLineNumber()
        << ", char " << domError.getLocation()->getColumnNumber()
        << "\n  Message: " << XMLCh2String(domError.getMessage()) << XERCES_STD_QUALIFIER endl;


        errors_ = true;

        return continueParsing;
}

/*
void xdata::XMLDOMErrorHandler::warning(const SAXParseException& toCatch)
{
  // If there were already errors, append the 
        // new messages after a newline
        if (errors_ == true)
        {
                msg_ << std::endl;
        }

        msg_ << "warning, ";

        msg_ << xdata::XMLCh2String(toCatch.getSystemId());
        msg_ << ", line ";
        msg_ << toCatch.getLineNumber();
        msg_ << ", column ";
        msg_ << toCatch.getColumnNumber();
        msg_ << ", message: ";
        msg_ << xdata::XMLCh2String(toCatch.getMessage());

        errors_ = true;

}

void xdata::XMLDOMErrorHandler::error(const SAXParseException& toCatch)
{
         if (errors_ == true)
        {
                msg_ << std::endl;
        }

        msg_ << "error, ";

        msg_ << xdata::XMLCh2String(toCatch.getSystemId());
        msg_ << ", line ";
        msg_ << toCatch.getLineNumber();
        msg_ << ", column ";
        msg_ << toCatch.getColumnNumber();
        msg_ << ", message: ";
        msg_ << xdata::XMLCh2String(toCatch.getMessage());

        errors_ = true;

}

void xdata::XMLDOMErrorHandler::fatalError(const SAXParseException& toCatch)
{
         if (errors_ == true)
        {
                msg_ << std::endl;
        }

        msg_ << "fatal, ";

        msg_ << xdata::XMLCh2String(toCatch.getSystemId());
        msg_ << ", line ";
        msg_ << toCatch.getLineNumber();
        msg_ << ", column ";
        msg_ << toCatch.getColumnNumber();
        msg_ << ", message: ";
        msg_ << xdata::XMLCh2String(toCatch.getMessage());

        errors_ = true;

}

*/



xdata::XMLDOMLoader::XMLDOMLoader(AbstractDOMParser::ValSchemes valScheme) : mutex_(toolbox::BSem::FULL)
{
 try
        {

                DOMImplementation* xqillaImplementation = DOMImplementationRegistry::getDOMImplementation(XStr(xoap::DOMImplementationFeatures));
                parser = xqillaImplementation->createLSParser(DOMImplementationLS::MODE_SYNCHRONOUS, 0);
    
                parser->getDomConfig()->setParameter(XMLUni::fgDOMNamespaces, true);
                parser->getDomConfig()->setParameter(XMLUni::fgXercesSchema, false);
                parser->getDomConfig()->setParameter(XMLUni::fgDOMValidateIfSchema, false);
		parser->getDomConfig()->setParameter(XMLUni::fgXercesHandleMultipleImports, true);
        
     
        
        }
        catch (const DOMException& e)
        {
                XCEPT_RAISE (xdata::exception::Exception, xoap::XMLCh2String(e.msg));

        
        }
        catch (...)
        {
                XCEPT_RAISE (xdata::exception::Exception, "error detected during parser creation");
        }


}

xdata::XMLDOMLoader::~XMLDOMLoader()
{
	delete parser;
	
}


DOMDocument * xdata::XMLDOMLoader::load( const std::string & url)  throw (xdata::exception::Exception)
{	
	// Apply filename expansion to the url given
	std::vector<std::string> expandedURL;
	std::string filename = "";
	try
	{
		expandedURL = toolbox::getRuntime()->expandPathName(url);
	} 
	catch (toolbox::exception::Exception& tbe)
	{
		std::string msg = "Cannot expand url '";
		msg += url;
		msg += "'";
		XCEPT_RETHROW (xdata::exception::Exception, msg, tbe);
	}
	
	if (expandedURL.size() == 1)
	{
		filename = expandedURL[0];
		if ((filename.find("file:/") == std::string::npos) && (filename.find("http://") == std::string::npos ))
		{
			// MUST have always the "/" in the beginning
			filename.insert(0, "file:");
		}
	}
	else
	{
		std::string msg = "Failed to load from url '";
		msg += url;
		msg += "', expansion leads to multiple files.";
		XCEPT_RAISE (xdata::exception::Exception, msg);
	}

	//reset error count first
	xdata::XMLDOMErrorHandler errorHandler;
        parser->getDomConfig()->setParameter(XMLUni::fgDOMErrorHandler, &errorHandler);
	errorHandler.resetErrors();
	//XERCES_CPP_NAMESPACE_QUALIFIER 
	

	try
	{
  		parser->resetDocumentPool();
                DOMDocument *doc = parser->parseURI(filename.c_str());
	
		if (errorHandler.hasErrors())
                {
                        std::string msg = errorHandler.getErrors();
                        doc->release();
                        this->unlock();
                        XCEPT_RAISE(xdata::exception::Exception, msg);
                }
                else
                {
                        this->unlock();
                        return doc;
                }

	}
	
	catch (const DOMLSException& toCatch)
	{      
		std::string msg = "Error during parsing in file:";
		msg += url;
		msg += " ";
		msg += xdata::XMLCh2String(toCatch.getMessage());
		
                this->unlock();
		XCEPT_RAISE(xdata::exception::Exception, msg);
	}
	catch (const DOMException& toCatch)
	{
		const unsigned int maxChars = 2047;
		XMLCh errText[maxChars + 1];
		std::string message;
		if (DOMImplementation::loadDOMExceptionMsg(toCatch.code, errText, maxChars))
		{
			message = "Error during parsing in file: ";
			message += url;
			message += " ";
			message += xdata::XMLCh2String(errText);
                	this->unlock();
			XCEPT_RAISE(xdata::exception::Exception,message); 

		}		
		else
		{
			message = "Error during parsing in file:";
			message += url;
			this->unlock();
			XCEPT_RAISE(xdata::exception::Exception,message); 
		}
	    
	}
	catch (const XMLException& xmle)
        {
                std::string msg = "DOM parse error: ";
                msg += xoap::XMLCh2String(xmle.getMessage());
                this->unlock();
                XCEPT_RAISE(xdata::exception::Exception, msg);
        }

	
}

void xdata::XMLDOMLoader::release(DOMDocument* doc)
{
	doc->release();
}

void xdata::XMLDOMLoader::lock()
{
        this->mutex_.take();
}

void xdata::XMLDOMLoader::unlock()
{
        this->mutex_.give();
}



static const XMLCh  gEndElement[] = { chOpenAngle, chForwardSlash, chNull };
static const XMLCh  gEndPI[] = { chQuestion, chCloseAngle, chNull};
static const XMLCh  gStartPI[] = { chOpenAngle, chQuestion, chNull };
static const XMLCh  gXMLDecl1[] =
{
	chOpenAngle, chQuestion, chLatin_x, chLatin_m, chLatin_l
    ,   chSpace, chLatin_v, chLatin_e, chLatin_r, chLatin_s, chLatin_i
    ,   chLatin_o, chLatin_n, chEqual, chDoubleQuote, chNull
};
static const XMLCh  gXMLDecl2[] =
{
	chDoubleQuote, chSpace, chLatin_e, chLatin_n, chLatin_c
    ,   chLatin_o, chLatin_d, chLatin_i, chLatin_n, chLatin_g, chEqual
    ,   chDoubleQuote, chNull
};
static const XMLCh  gXMLDecl3[] =
{
	chDoubleQuote, chSpace, chLatin_s, chLatin_t, chLatin_a
    ,   chLatin_n, chLatin_d, chLatin_a, chLatin_l, chLatin_o
    ,   chLatin_n, chLatin_e, chEqual, chDoubleQuote, chNull
};
static const XMLCh  gXMLDecl4[] =
{
	chDoubleQuote, chQuestion, chCloseAngle
    ,   chCR, chLF, chNull
};

static const XMLCh  gStartCDATA[] =
{ 
	chOpenAngle, chBang, chOpenSquare, chLatin_C, chLatin_D,
	chLatin_A, chLatin_T, chLatin_A, chOpenSquare, chNull
};

static const XMLCh  gEndCDATA[] =
{
    chCloseSquare, chCloseSquare, chCloseAngle, chNull
};
static const XMLCh  gStartComment[] =
{ 
    chOpenAngle, chBang, chDash, chDash, chNull
};

static const XMLCh  gEndComment[] =
{
    chDash, chDash, chCloseAngle, chNull
};

static const XMLCh  gStartDoctype[] =
{ 
    chOpenAngle, chBang, chLatin_D, chLatin_O, chLatin_C, chLatin_T,
    chLatin_Y, chLatin_P, chLatin_E, chSpace, chNull
};
static const XMLCh  gPublic[] =
{ 
    chLatin_P, chLatin_U, chLatin_B, chLatin_L, chLatin_I,
    chLatin_C, chSpace, chDoubleQuote, chNull
};
static const XMLCh  gSystem[] =
{ 
    chLatin_S, chLatin_Y, chLatin_S, chLatin_T, chLatin_E,
    chLatin_M, chSpace, chDoubleQuote, chNull
};
static const XMLCh  gStartEntity[] =
{ 
    chOpenAngle, chBang, chLatin_E, chLatin_N, chLatin_T, chLatin_I,
    chLatin_T, chLatin_Y, chSpace, chNull
};

static const XMLCh  gNotation[] =
{ 
    chLatin_N, chLatin_D, chLatin_A, chLatin_T, chLatin_A,
    chSpace, chDoubleQuote, chNull
};



void xdata::XMLDOMSerializer::serialize(DOMNode* toWrite ) throw ( xdata::exception::Exception)
{
    if ( toWrite == 0 ) 
    {
    	*gFormatter_ << chNull; 
    	return;
    }
    
    // Get the name and value out for convenience
    const XMLCh*   nodeName = toWrite->getNodeName();
    const XMLCh*   nodeValue = toWrite->getNodeValue();
    unsigned int lent = XMLString::stringLen (nodeValue);
	
    switch (toWrite->getNodeType())
    {
        case DOMNode::TEXT_NODE:
        {
		// CRLF, tabls and CRLF again
		//*gFormatter_ << chCR << chLF;
		indent_+=4;
		chTab(indent_, gFormatter_);
		gFormatter_->formatBuf(nodeValue, lent, XMLFormatter::StdEscapes);
		indent_-=4;
		*gFormatter_ << chCR << chLF;
		break;
        }						
        case DOMNode::PROCESSING_INSTRUCTION_NODE :
        {
            *gFormatter_ << escapeStyle_<< gStartPI  << nodeName;
            if (lent > 0)
            {
                *gFormatter_ << chSpace << nodeValue;
            }
            *gFormatter_ << escapeStyle_ << gEndPI;
            break;
        }
        case DOMNode::DOCUMENT_NODE :
        {			
            DOMNode* child = toWrite->getFirstChild();
            while( child != 0)
            {
                serialize (child);
                child = child->getNextSibling();
            }
            break;
        }
        case DOMNode::ELEMENT_NODE :
        {
            // Output the element start tag.
	    chTab(indent_, gFormatter_);
            *gFormatter_  << escapeStyle_ << chOpenAngle << nodeName;
			
            // Output any attributes on this element
            DOMNamedNodeMap* attributes = toWrite->getAttributes();
            int attrCount = attributes->getLength();
            for (int i = 0; i < attrCount; i++)
            {
                DOMNode*  attribute = attributes->item(i);
				
                //
                //  Again the name has to be completely representable. But the
                //  attribute can have refs and requires the attribute style
                //  escaping.
                //
		*gFormatter_	<< escapeStyle_
				<< chSpace << attribute->getNodeName()
				<< chEqual << chDoubleQuote
				<< XMLFormatter::AttrEscapes
				<< attribute->getNodeValue()
				<< escapeStyle_
				<< chDoubleQuote;
            }
			
            //
            //  Test for the presence of children, which includes both
            //  text content and nested elements.
            //
            DOMNode* child = toWrite->getFirstChild();
            if (child != 0)
            {
                // There are children. Close start-tag, and output children.
                *gFormatter_ << escapeStyle_ << chCloseAngle << chCR << chLF;
		
		// indent here
		indent_+=4;	
                while( child != 0)
                {
                    serialize (child);
                    child = child->getNextSibling();
                }
		indent_-=4;
				
                //
                // Done with children.  Output the end tag.
                // Had to put chNull at the end to be sure
		// that there are no additional data in the stream.
		//
		chTab(indent_, gFormatter_);
                *gFormatter_ << escapeStyle_ << gEndElement << nodeName << chCloseAngle << chCR << chLF;
            }
            else
            {
                //
                //  There were no children. Output the short form close of
                //  the element start tag, making it an empty-element tag.
                //
                *gFormatter_ << escapeStyle_ << chForwardSlash << chCloseAngle << chCR << chLF;
            }
            break;
        }
        case DOMNode::ENTITY_REFERENCE_NODE:
        {
            DOMNode* child;
            for (child = toWrite->getFirstChild();
                 child != 0;
                 child = child->getNextSibling())
            {
                serialize (child);
            }
            break;
        }			
        case DOMNode::CDATA_SECTION_NODE:
        {
            *gFormatter_ << escapeStyle_ << gStartCDATA	<< nodeValue << gEndCDATA;
            break;
        }			
        case DOMNode::COMMENT_NODE:
        {
            *gFormatter_ << escapeStyle_ << gStartComment << nodeValue << gEndComment;
            break;
        }			
        case DOMNode::DOCUMENT_TYPE_NODE:
        {
		DOMDocumentType* doctype = (DOMDocumentType *)toWrite;

		*gFormatter_ << escapeStyle_  << gStartDoctype << nodeName;
		const XMLCh* id = doctype->getPublicId();
			
		if (id != 0)
		{
                	*gFormatter_ << escapeStyle_ << chSpace << gPublic << id << chDoubleQuote;
		}
			
		id = doctype->getSystemId();
		if (id != 0)
		{
			*gFormatter_ << escapeStyle_ << chSpace << gSystem << id << chDoubleQuote;
		}
            
		id = doctype->getInternalSubset(); 
		if (id !=0)
		{
			*gFormatter_ << escapeStyle_ << chOpenSquare << id << chCloseSquare;
		}
			
		*gFormatter_ << escapeStyle_ << chCloseAngle << chCR << chLF;;
		break;
        }
        case DOMNode::ENTITY_NODE:
        {
		*gFormatter_ << escapeStyle_ << gStartEntity << nodeName;

		const XMLCh* id = ((DOMEntity *)toWrite)->getPublicId();
		if (id != 0)
			*gFormatter_ << escapeStyle_ << gPublic	<< id << chDoubleQuote;

		id = ((DOMEntity *)toWrite)->getSystemId();
		if (id != 0)
			*gFormatter_ << escapeStyle_ << gSystem	<< id << chDoubleQuote;

		id = ((DOMEntity *)toWrite)->getNotationName();
		if (id != 0)
			*gFormatter_ << escapeStyle_ << gNotation << id << chDoubleQuote;

		*gFormatter_ << escapeStyle_ << chCloseAngle << chCR << chLF;

		break;
        }
			
			
			/*
			 XML_DECL_NODE does not exist in Xerces 2.3
			 
			 case DOMNode::XML_DECL_NODE:
			 {
				 const XMLCh*  str;
				 
				 *gFormatter_ << gXMLDecl1 << ((DOMXMLDecl *)toWrite)->getVersion();
				 
				 *gFormatter_ << gXMLDecl2 << gEncodingName;
				 
				 str = ((DOMXMLDecl *)toWrite)->getStandalone();
				 if (str != 0)
					 *gFormatter_ << gXMLDecl3 << str;
				 
				 *gFormatter_ << gXMLDecl4;
				 
				 break;
			 }
				 */
			
			
        default:
	{
		std::stringstream msg;
		msg <<  "Unrecognized node type = " << (long)toWrite->getNodeType();
	   	XCEPT_RAISE (xdata::exception::Exception, msg.str());
	}
    }
    
    return;
}


xdata::XMLDOMSerializer::XMLDOMSerializer(std::string& s, bool escape) : stream_(s) 
{
	indent_			= 0;
	gXmlFile	       	= 0;
	gDoNamespaces   	= false;
	gDoExpand	       	= false;
	gEncodingName	= 0;
	gUnRepFlags	       	= XMLFormatter::UnRep_CharRef;
	gFormatter_	       	= 0;
	
	if (escape)
	{	
		escapeStyle_ = XMLFormatter::StdEscapes;
	}
	else
	{
		escapeStyle_ = XMLFormatter::NoEscapes;
	}
	
	gFormatter_ = new XMLFormatter(XStr ("UTF-8"), 
					   0,
					   this, 
					   escapeStyle_, 
					   gUnRepFlags);
}

xdata::XMLDOMSerializer::~XMLDOMSerializer() 
{
    delete gFormatter_;
    delete gEncodingName;
} 

void xdata::XMLDOMSerializer::chTab(unsigned long n, XMLFormatter* f)
{
	for (unsigned long i = 0; i < n; i++)
	{
		*f << chSpace;
	}
}


