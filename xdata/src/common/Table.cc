// $Id: Table.cc,v 1.33 2008/07/21 08:06:37 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                             *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					                 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                             *
 * For the list of contributors see CREDITS.   			                 *
 *************************************************************************/

#include <sstream> 
#include "xdata/Table.h"
#include "xdata/Vector.h"
#include "xdata/UnsignedLong.h"
#include "xdata/UnsignedInteger.h"
#include "xdata/UnsignedInteger8.h"
#include "xdata/UnsignedInteger16.h"
#include "xdata/UnsignedInteger32.h"
#include "xdata/UnsignedInteger64.h"
#include "xdata/UnsignedShort.h"
#include "xdata/Integer.h"
#include "xdata/Integer8.h"
#include "xdata/Integer16.h"
#include "xdata/Integer32.h"
#include "xdata/Integer64.h"
#include "xdata/Float.h"
#include "xdata/Double.h"
#include "xdata/String.h"
#include "xdata/Boolean.h"
#include "xdata/TimeVal.h"
#include "xdata/Mime.h"
#include "toolbox/string.h"

#include "xdata/TableIterator.h"

/* function object to check the value of a map element
 */
// pos = find_if(my.begin(),my.end(), value_equals<string,my>("mystring"));

template <class K, class V>
class value_equals 
{
private:
	K key;
public:
	// constructor (initialize value to compare with)
	value_equals (const K& k) 
{
		key = toolbox::toupper(k);
}
	// comparison
	bool operator() (std::pair<const K, V> elem) 
	{
		return toolbox::toupper (elem.first) == key;
	}
};




xdata::Table::Table() throw (xdata::exception::Exception)
		{
	numberOfRows_ = 0;
	memorySize_ = 10;
		}

xdata::Table::Table(const xdata::Table & t) throw (xdata::exception::Exception)
		{
	this->operator=(t);
		}

xdata::Table::Table(const std::map<std::string, std::string, xdata::Table::ci_less >& columns)
{
	numberOfRows_ = 0;
	memorySize_ = 10;
	std::map<std::string, std::string, xdata::Table::ci_less >::const_iterator i;

	for ( i= columns.begin(); i != columns.end(); i++ )
	{
		this->addColumn((*i).first, (*i).second );
	}

}

/* Second option using iterator
int xdata::Table::operator=(const xdata::Table & rhs )
{
	this->clear();

	numberOfRows_ = 0;
	memorySize_ = 10;
	std::map<std::string, std::string, std::less<std::string> >::iterator i;

	for ( i= columns.begin(); i != columns.end(); i++ )
	{
		this->addColumn((*i).first, (*i).second );
	}

	// fill all data
	for ( it =  rhs.begin(); it != rhs.end(); it++ )
	{
		this->insert(*it);
	}

}	

 */

void xdata::Table::reserve(size_t entries) throw (xdata::exception::Exception)
		{
	if (entries > xdata::Table::MaxRows)
	{
		XCEPT_RAISE(xdata::exception::Exception, "Exceeded maximum table size");
	}

	// ignore if size is already available
	if ( entries <= numberOfRows_ )
		return;

	if ( entries <=  memorySize_)
		return;

	memorySize_ = entries;	

	// if any data need resizing of the existing
	if (! columnDefinitions_.empty() )
	{
		for (std::map<std::string, AbstractVector *, std::less<std::string> >::iterator i = columnData_.begin(); i != columnData_.end(); i++)
		{
			(*i).second->setSize(memorySize_);
		}		
	}
		}

xdata::Table::~Table() 
{
	// LO because of BUG  1207813	AbstractVector does not destruct
	// need to cast to the exact vector type

	for (std::map<std::string, AbstractVector *, std::less<std::string> >::iterator i = columnData_.begin(); i != columnData_.end(); i++)
	{
		std::string &def = columnDefinitions_[(*i).first];
		if ( def == "unsigned long" )
		{
			delete dynamic_cast<xdata::Vector<xdata::UnsignedLong>*>((*i).second);
		}
		else if ( def == "unsigned int" )
		{
			delete dynamic_cast<xdata::Vector<xdata::UnsignedInteger>*>((*i).second);
		}
		else if ( def == "unsigned int 8" )
		{
			delete dynamic_cast<xdata::Vector<xdata::UnsignedInteger8>*>((*i).second);
		}
		else if ( def == "unsigned int 16" )
		{
			delete dynamic_cast<xdata::Vector<xdata::UnsignedInteger16>*>((*i).second);
		}
		else if ( def == "unsigned int 32" )
		{
			delete dynamic_cast<xdata::Vector<xdata::UnsignedInteger32>*>((*i).second);
		}
		else if ( def == "unsigned int 64" )
		{
			delete dynamic_cast<xdata::Vector<xdata::UnsignedInteger64>*>((*i).second);
		}
		else if ( def == "unsigned short" )
		{
			delete dynamic_cast<xdata::Vector<xdata::UnsignedShort>*>((*i).second);
		}
		else if ( def == "float" )
		{
			delete dynamic_cast<xdata::Vector<xdata::Float>*>((*i).second);
		}
		else if ( def == "int 8" )
		{
			delete dynamic_cast< xdata::Vector<xdata::Integer8>*>((*i).second);
		}
		else if ( def == "int 16" )
		{
			delete dynamic_cast< xdata::Vector<xdata::Integer16>*>((*i).second);
		}
		else if ( def == "int 32" )
		{
			delete dynamic_cast< xdata::Vector<xdata::Integer32>*>((*i).second);
		}
		else if ( def == "int 64" )
		{
			delete dynamic_cast< xdata::Vector<xdata::Integer64>*>((*i).second);
		}
		else if ( def == "int" )
		{
			delete dynamic_cast< xdata::Vector<xdata::Integer>*>((*i).second);
		}
		else if ( def == "string" )
		{
			delete dynamic_cast<xdata::Vector<xdata::String>*>((*i).second);
		}
		else if ( def == "bool" )
		{
			delete dynamic_cast<xdata::Vector<xdata::Boolean>*>((*i).second);
		}
		else if ( def == "double" )
		{
			delete dynamic_cast<xdata::Vector<xdata::Double>*>((*i).second);
		}
		else if ( def == "time" )
		{
			delete dynamic_cast<xdata::Vector<xdata::TimeVal>*>((*i).second);
		}
		else if ( def == "mime" )
		{
			delete dynamic_cast<xdata::Vector<xdata::Mime>*>((*i).second);
		}
		else if ( def == "table" )
		{
			delete dynamic_cast<xdata::Vector<xdata::Table>*>((*i).second);
		}
		else if ( def == "vector int" )
		{
			delete dynamic_cast<xdata::Vector<xdata::Vector<xdata::Integer> >*>((*i).second);
		}
		else if ( def == "vector int 8" )
		{
			delete dynamic_cast<xdata::Vector<xdata::Vector<xdata::Integer8> >*>((*i).second);
		}
		else if ( def == "vector int 16" )
		{
			delete dynamic_cast<xdata::Vector<xdata::Vector<xdata::Integer16> >*>((*i).second);
		}
		else if ( def == "vector int 32" )
		{
			delete dynamic_cast<xdata::Vector<xdata::Vector<xdata::Integer32> >*>((*i).second);
		}
		else if ( def == "vector int 64" )
		{
			delete dynamic_cast<xdata::Vector<xdata::Vector<xdata::Integer64> >*>((*i).second);
		}
		else if ( def == "vector unsigned int" )
		{
			delete dynamic_cast<xdata::Vector<xdata::Vector<xdata::UnsignedInteger> >*>((*i).second);
		}
		else if ( def == "vector unsigned int 8" )
		{
			delete dynamic_cast<xdata::Vector<xdata::Vector<xdata::UnsignedInteger8> >*>((*i).second);
		}
		else if ( def == "vector unsigned int 16" )
		{
			delete dynamic_cast<xdata::Vector<xdata::Vector<xdata::UnsignedInteger16> >*>((*i).second);
		}
		else if ( def == "vector unsigned int 32" )
		{
			delete dynamic_cast<xdata::Vector<xdata::Vector<xdata::UnsignedInteger32> >*>((*i).second);
		}
		else if ( def == "vector unsigned int 64" )
		{
			delete dynamic_cast<xdata::Vector<xdata::Vector<xdata::UnsignedInteger64> >*>((*i).second);
		}
		else if ( def == "vector unsigned short" )
		{
			delete dynamic_cast<xdata::Vector<xdata::Vector<xdata::UnsignedShort> >*>((*i).second);
		}
		else if ( def == "vector float" )
		{
			delete dynamic_cast<xdata::Vector<xdata::Vector<xdata::Float> >*>((*i).second);
		}
		else if ( def == "vector string" )
		{
			delete dynamic_cast<xdata::Vector<xdata::Vector<xdata::String> >*>((*i).second);
		}
		else if ( def == "vector bool" )
		{
			delete dynamic_cast<xdata::Vector<xdata::Vector<xdata::Boolean> >*>((*i).second);
		}
		else if ( def == "vector double" )
		{
			delete dynamic_cast<xdata::Vector<xdata::Vector<xdata::Double> >*>((*i).second);
		}
		else if ( def == "vector time" )
		{
			delete dynamic_cast<xdata::Vector<xdata::Vector<xdata::TimeVal> >*>((*i).second);
		}
	}

}

xdata::Table & xdata::Table::operator=(const xdata::Table & t) throw (xdata::exception::Exception)
		{
	// clear destination table
	try
	{
		this->clear();

		// Prepare  table
		std::map<std::string, std::string, std::less<std::string> >::const_iterator i;
		for ( i = t.columnDefinitions_.begin(); i != t.columnDefinitions_.end(); i++ )
		{
			this->addColumn((*i).first, (*i).second );
			xdata::AbstractVector * v = t.columnData_.find((*i).first)->second;
			for ( size_t r = 0; r < t.numberOfRows_; r++ )
			{		
				xdata::Serializable * s = v->elementAt(r);
				this->setValueAt(r, (*i).first, *s );	
			}
		}

		return *this;
	}
	catch (xdata::exception::Exception& e)
	{
		std::stringstream msg;
		msg << "Failed to assign table to table" << std::endl;
		XCEPT_RETHROW (xdata::exception::Exception, msg.str(), e);
	}
		}

// Comparison operators

int xdata::Table::operator==(const xdata::Table & t) const
		{
	XCEPT_RAISE (xdata::exception::Exception, "not implemented");
	return 0;	
		}

int xdata::Table::operator!=(const xdata::Table & t) const
		{
	XCEPT_RAISE (xdata::exception::Exception, "not implemented");
	return 0;	
		}


int xdata::Table::equals(const xdata::Serializable & s) const
{
	// apply to this the function "==" with the parameter "const Monitorable<T>&"
	// inverting the two elements will cause C++ to call another "==" operator.
	//
	return this->operator==(dynamic_cast<const Table&>(s) );
}


void xdata::Table::setValue(const xdata::Serializable & s) throw (xdata::exception::Exception)
		{
	try
	{
		this->operator=(dynamic_cast<const xdata::Table&>(s));
	}
	catch (xdata::exception::Exception& e)
	{
		std::stringstream msg;
		msg << "Failed to assign table to table";
		XCEPT_RETHROW (xdata::exception::Exception, msg.str(), e);
	}
		}


std::string xdata::Table::type() const
{
	return "table";
}


std::string xdata::Table::toString() const throw (xdata::exception::Exception)
		{
	// Serialize in JSCON format. Each row is an array of values [a,b,c], inner tables are possible.
	// A table is an array of rows: [[a,b,c],[d,e,f],[g,h,i]]
	// DIFFERENCE to JSON: Strings are not quoted at this point, order of columns is accoring to what
	// this->getColumns() returns in the vector.

	std::stringstream data;

	this->writeTo(data);

	return data.str();
		}

void xdata::Table::fromString(const std::string& value) throw (xdata::exception::Exception)
		{
	// Serialize in JSCON format. Each row is an array of values [a,b,c], inner tables are possible.
	// A table is an array of rows: [[a,b,c],[d,e,f],[g,h,i]]
	// DIFFERENCE to JSON: Strings are not quoted at this point, order of columns is accoring to what
	// this->getColumns() returns in the vector.

	std::istringstream data(value);

	this->readFrom(data);
		}


std::string xdata::Table::getColumnType(const std::string & name) const throw (xdata::exception::Exception)
		{
	std::map<std::string, std::string, std::less<std::string> >::const_iterator i = columnDefinitions_.find(name);
	if (  i != columnDefinitions_.end() )
	{
		return (*i).second;
	}
	else
	{
		std::string msg = "invalid column name:";
		msg += name;
		XCEPT_RAISE (xdata::exception::Exception, msg);
	}
		}

// Abstract Table interface

size_t xdata::Table::getRowCount() const
{
	return numberOfRows_;
}

std::vector<std::string> xdata::Table::getColumns() const
{
	std::vector<std::string> columns;
	for (std::map<std::string, std::string, std::less<std::string> >::const_iterator i = columnDefinitions_.begin(); i != columnDefinitions_.end(); i++)
		columns.push_back((*i).first);
	return columns;
}

const std::map<std::string, std::string, xdata::Table::ci_less >& xdata::Table::getTableDefinition() const
{
	return columnDefinitions_;
}


void xdata::Table::clear()
{
	numberOfRows_ = 0;
	memorySize_ = 10;
	for (std::map<std::string, AbstractVector *, std::less<std::string> >::iterator i = columnData_.begin(); i != columnData_.end(); i++)
	{
		std::string &def = columnDefinitions_[(*i).first];
		if ( def == "unsigned long" )
		{
			delete dynamic_cast<xdata::Vector<xdata::UnsignedLong>*>((*i).second);
		}
		else if ( def == "unsigned int" )
		{
			delete dynamic_cast<xdata::Vector<xdata::UnsignedInteger>*>((*i).second);
		}
		else if ( def == "unsigned int 8" )
		{
			delete dynamic_cast<xdata::Vector<xdata::UnsignedInteger8>*>((*i).second);
		}
		else if ( def == "unsigned int 16" )
		{
			delete dynamic_cast<xdata::Vector<xdata::UnsignedInteger16>*>((*i).second);
		}
		else if ( def == "unsigned int 32" )
		{
			delete dynamic_cast<xdata::Vector<xdata::UnsignedInteger32>*>((*i).second);
		}
		else if ( def == "unsigned int 64" )
		{
			delete dynamic_cast<xdata::Vector<xdata::UnsignedInteger64>*>((*i).second);
		}
		else if ( def == "unsigned short" )
		{
			delete dynamic_cast<xdata::Vector<xdata::UnsignedShort>*>((*i).second);
		}
		else if ( def == "float" )
		{
			delete dynamic_cast<xdata::Vector<xdata::Float>*>((*i).second);
		}
		else if ( def == "int" )
		{
			delete dynamic_cast< xdata::Vector<xdata::Integer>*>((*i).second);
		}
		else if ( def == "int 8" )
		{
			delete dynamic_cast< xdata::Vector<xdata::Integer8>*>((*i).second);
		}
		else if ( def == "int 16" )
		{
			delete dynamic_cast< xdata::Vector<xdata::Integer16>*>((*i).second);
		}
		else if ( def == "int 32" )
		{
			delete dynamic_cast< xdata::Vector<xdata::Integer32>*>((*i).second);
		}
		else if ( def == "int 64" )
		{
			delete dynamic_cast< xdata::Vector<xdata::Integer64>*>((*i).second);
		}
		else if ( def == "string" )
		{
			delete dynamic_cast<xdata::Vector<xdata::String>*>((*i).second);
		}
		else if ( def == "bool" )
		{
			delete dynamic_cast<xdata::Vector<xdata::Boolean>*>((*i).second);
		}
		else if ( def == "double" )
		{
			delete dynamic_cast<xdata::Vector<xdata::Double>*>((*i).second);
		}
		else if ( def == "time" )
		{
			delete dynamic_cast<xdata::Vector<xdata::TimeVal>*>((*i).second);
		}
		else if ( def == "mime" )
		{
			delete dynamic_cast<xdata::Vector<xdata::Mime>*>((*i).second);
		}
		else if ( def == "table" )
		{
			delete dynamic_cast<xdata::Vector<xdata::Table>*>((*i).second);
		}
		else if ( def == "vector unsigned int" )
		{
			delete dynamic_cast<xdata::Vector<xdata::Vector<xdata::UnsignedInteger> >*>((*i).second);
		}
		else if ( def == "vector unsigned int 8" )
		{
			delete dynamic_cast<xdata::Vector<xdata::Vector<xdata::UnsignedInteger8> >*>((*i).second);
		}
		else if ( def == "vector unsigned int 16" )
		{
			delete dynamic_cast<xdata::Vector<xdata::Vector<xdata::UnsignedInteger16> >*>((*i).second);
		}
		else if ( def == "vector unsigned int 32" )
		{
			delete dynamic_cast<xdata::Vector<xdata::Vector<xdata::UnsignedInteger32> >*>((*i).second);
		}
		else if ( def == "vector unsigned int 64" )
		{
			delete dynamic_cast<xdata::Vector<xdata::Vector<xdata::UnsignedInteger64> >*>((*i).second);
		}
		else if ( def == "vector unsigned short" )
		{
			delete dynamic_cast<xdata::Vector<xdata::Vector<xdata::UnsignedShort> >*>((*i).second);
		}
		else if ( def == "vector int" )
		{
			delete dynamic_cast<xdata::Vector<xdata::Vector<xdata::Integer> >*>((*i).second);
		}
		else if ( def == "vector int 8" )
		{
			delete dynamic_cast<xdata::Vector<xdata::Vector<xdata::Integer8> >*>((*i).second);
		}
		else if ( def == "vector int 16" )
		{
			delete dynamic_cast<xdata::Vector<xdata::Vector<xdata::Integer16> >*>((*i).second);
		}
		else if ( def == "vector int 32" )
		{
			delete dynamic_cast<xdata::Vector<xdata::Vector<xdata::Integer32> >*>((*i).second);
		}
		else if ( def == "vector int 64" )
		{
			delete dynamic_cast<xdata::Vector<xdata::Vector<xdata::Integer64> >*>((*i).second);
		}
		else if ( def == "vector float" )
		{
			delete dynamic_cast<xdata::Vector<xdata::Vector<xdata::Float> >*>((*i).second);
		}
		else if ( def == "vector string" )
		{
			delete dynamic_cast<xdata::Vector<xdata::Vector<xdata::String> >*>((*i).second);
		}
		else if ( def == "vector bool" )
		{
			delete dynamic_cast<xdata::Vector<xdata::Vector<xdata::Boolean> >*>((*i).second);
		}
		else if ( def == "vector double" )
		{
			delete dynamic_cast<xdata::Vector<xdata::Vector<xdata::Double> >*>((*i).second);
		}
		else if ( def == "vector time" )
		{
			delete dynamic_cast<xdata::Vector<xdata::Vector<xdata::TimeVal> >*>((*i).second);
		}


	}
	columnData_.clear();
	columnDefinitions_.clear();
}

xdata::Serializable * xdata::Table::getValueAt(size_t rowIndex, const std::string & name) const
throw (xdata::exception::Exception)
{

	if  (( rowIndex < numberOfRows_ ) && (columnDefinitions_.find(name) != columnDefinitions_.end() ) ) 
	{
		std::map<std::string, xdata::AbstractVector *, ci_less >::const_iterator i = columnData_.find(name);;
		return ((*i).second)->elementAt(rowIndex); 
		//return columnData_[name]->elementAt(rowIndex);
	}
	else
	{
		std::stringstream msg;
		msg << "invalid row index/name combination (" << rowIndex << "," << name << ")";
		XCEPT_RAISE (xdata::exception::Exception, msg.str());
	}
}

void xdata::Table::setValueAt(size_t rowIndex, const std::string & name, xdata::Serializable & object )
throw (xdata::exception::Exception)
{
	if (rowIndex > xdata::Table::MaxRows)
	{
		std::stringstream msg;
		msg << "Exceeded maximum table size of " << xdata::Table::MaxRows << " rows";
		XCEPT_RAISE(xdata::exception::Exception, msg.str());
	}

	if  (columnDefinitions_.find(name) == columnDefinitions_.end()  )
	{
		std::stringstream msg;
		msg << "Column '" << name << "' not found in target table";
		XCEPT_RAISE (xdata::exception::Exception, msg.str());
	}

	std::string type = object.type();
	if ( type  == "vector" )
	{
		type =  "vector " + dynamic_cast<xdata::AbstractVector&>(object).getElementType();
	}
	if ( type != columnDefinitions_[name])
	{
		std::string msg = "invalid type ";
		msg += type;
		msg += " for name ";
		msg += name;
		XCEPT_RAISE (xdata::exception::Exception, msg);
	}

	// resize table is required
	if  ( rowIndex >= numberOfRows_ )
	{
		numberOfRows_ = rowIndex +1;
		while (  memorySize_ < numberOfRows_ ) 
		{
			memorySize_ = memorySize_ + 10;
			for (std::map<std::string, AbstractVector *, std::less<std::string> >::iterator i = columnData_.begin(); i != columnData_.end(); i++)
			{
				//std::cout << "increase size index : " << rowIndex << "rows :" << numberOfRows_ << std::endl;
				(*i).second->setSize(memorySize_);
			}	
		}

	}

	// assign value
	if ( object.type() == "unsigned long" )
	{
		//xdata::UnsignedLong* s = *dynamic_cast<xdata::UnsignedLong*>(columnData_[name]->elementAt(rowIndex));
		//xdata::UnsignedLong & u =  dynamic_cast<xdata::UnsignedLong&>(object);
		*dynamic_cast<xdata::UnsignedLong*>(columnData_[name]->elementAt(rowIndex)) =  dynamic_cast<xdata::UnsignedLong&>(object);
		//*s = u;
	}
	else if ( object.type() == "unsigned int" )
	{
		*dynamic_cast<xdata::UnsignedInteger*>(columnData_[name]->elementAt(rowIndex)) =  dynamic_cast<xdata::UnsignedInteger&>(object);
	}
	else if ( object.type() == "unsigned int 8" )
	{
		*dynamic_cast<xdata::UnsignedInteger8*>(columnData_[name]->elementAt(rowIndex)) =  dynamic_cast<xdata::UnsignedInteger8&>(object);
	}
	else if ( object.type() == "unsigned int 16" )
	{
		*dynamic_cast<xdata::UnsignedInteger16*>(columnData_[name]->elementAt(rowIndex)) =  dynamic_cast<xdata::UnsignedInteger16&>(object);
	}
	else if ( object.type() == "unsigned int 32" )
	{
		*dynamic_cast<xdata::UnsignedInteger32*>(columnData_[name]->elementAt(rowIndex)) =  dynamic_cast<xdata::UnsignedInteger32&>(object);
	}
	else if ( object.type() == "unsigned int 64" )
	{
		*dynamic_cast<xdata::UnsignedInteger64*>(columnData_[name]->elementAt(rowIndex)) =  dynamic_cast<xdata::UnsignedInteger64&>(object);
	}
	else if ( object.type() == "unsigned short" )
	{
		*dynamic_cast<xdata::UnsignedShort*>(columnData_[name]->elementAt(rowIndex)) =  dynamic_cast<xdata::UnsignedShort&>(object);
	}
	else if ( object.type() == "float" )
	{
		*dynamic_cast<xdata::Float*>(columnData_[name]->elementAt(rowIndex)) =  dynamic_cast<xdata::Float&>(object);
	}
	else if ( object.type() == "int" )
	{
		*dynamic_cast<xdata::Integer*>(columnData_[name]->elementAt(rowIndex)) =  dynamic_cast<xdata::Integer&>(object);
	}
	else if ( object.type() == "int 8" )
	{
		*dynamic_cast<xdata::Integer8*>(columnData_[name]->elementAt(rowIndex)) =  dynamic_cast<xdata::Integer8&>(object);
	}
	else if ( object.type() == "int 16" )
	{
		*dynamic_cast<xdata::Integer16*>(columnData_[name]->elementAt(rowIndex)) =  dynamic_cast<xdata::Integer16&>(object);
	}
	else if ( object.type() == "int 32" )
	{
		*dynamic_cast<xdata::Integer32*>(columnData_[name]->elementAt(rowIndex)) =  dynamic_cast<xdata::Integer32&>(object);
	}
	else if ( object.type() == "int 64" )
	{
		*dynamic_cast<xdata::Integer64*>(columnData_[name]->elementAt(rowIndex)) =  dynamic_cast<xdata::Integer64&>(object);
	}
	else if ( object.type() == "double" )
	{
		*dynamic_cast<xdata::Double*>(columnData_[name]->elementAt(rowIndex)) =  dynamic_cast<xdata::Double&>(object);
	}
	else if ( object.type() == "bool" )
	{
		*dynamic_cast<xdata::Boolean*>(columnData_[name]->elementAt(rowIndex)) =  dynamic_cast<xdata::Boolean&>(object);
	}
	else if ( object.type() == "time" )
	{
		*dynamic_cast<xdata::TimeVal*>(columnData_[name]->elementAt(rowIndex)) =  dynamic_cast<xdata::TimeVal&>(object);
	}
	else if ( object.type() == "string" )
	{
		*dynamic_cast<xdata::String*>(columnData_[name]->elementAt(rowIndex)) =  dynamic_cast<xdata::String&>(object);
	}
	else if ( object.type() == "mime" )
	{
		*dynamic_cast<xdata::Mime*>(columnData_[name]->elementAt(rowIndex)) =  dynamic_cast<xdata::Mime&>(object);
	}
	else if ( object.type() == "table" )
	{
		*dynamic_cast<xdata::Table*>(columnData_[name]->elementAt(rowIndex)) =  dynamic_cast<xdata::Table&>(object);
	}
	else if ( object.type()  == "vector" )
	{
		if ( dynamic_cast<xdata::AbstractVector&>(object).getElementType() == "unsigned int"  ) 
		{
			*dynamic_cast<xdata::Vector<xdata::UnsignedInteger>*>(columnData_[name]->elementAt(rowIndex)) =  dynamic_cast<xdata::Vector<xdata::UnsignedInteger>&>(object);
		}
		else if ( dynamic_cast<xdata::AbstractVector&>(object).getElementType() == "unsigned int 8"  )
		{
			*dynamic_cast<xdata::Vector<xdata::UnsignedInteger8>*>(columnData_[name]->elementAt(rowIndex)) =  dynamic_cast<xdata::Vector<xdata::UnsignedInteger8>&>(object);
		}
		else if ( dynamic_cast<xdata::AbstractVector&>(object).getElementType() == "unsigned int 16"  )
		{
			*dynamic_cast<xdata::Vector<xdata::UnsignedInteger16>*>(columnData_[name]->elementAt(rowIndex)) =  dynamic_cast<xdata::Vector<xdata::UnsignedInteger16>&>(object);
		}
		else if ( dynamic_cast<xdata::AbstractVector&>(object).getElementType() == "unsigned int 32"  )
		{
			*dynamic_cast<xdata::Vector<xdata::UnsignedInteger32>*>(columnData_[name]->elementAt(rowIndex)) =  dynamic_cast<xdata::Vector<xdata::UnsignedInteger32>&>(object);
		}
		else if  ( dynamic_cast<xdata::AbstractVector&>(object).getElementType() == "unsigned int 64"  )
		{
			*dynamic_cast<xdata::Vector<xdata::UnsignedInteger64>*>(columnData_[name]->elementAt(rowIndex)) =  dynamic_cast<xdata::Vector<xdata::UnsignedInteger64>&>(object);

		}
		else if  ( dynamic_cast<xdata::AbstractVector&>(object).getElementType() == "unsigned short"  )
		{
			*dynamic_cast<xdata::Vector<xdata::UnsignedShort>*>(columnData_[name]->elementAt(rowIndex)) =  dynamic_cast<xdata::Vector<xdata::UnsignedShort>&>(object);

		}
		else if  ( dynamic_cast<xdata::AbstractVector&>(object).getElementType() == "int"  )
		{
			*dynamic_cast<xdata::Vector<xdata::Integer>*>(columnData_[name]->elementAt(rowIndex)) =  dynamic_cast<xdata::Vector<xdata::Integer>&>(object);
		}
		else if  ( dynamic_cast<xdata::AbstractVector&>(object).getElementType() == "int 8"  )
		{
			*dynamic_cast<xdata::Vector<xdata::Integer8>*>(columnData_[name]->elementAt(rowIndex)) =  dynamic_cast<xdata::Vector<xdata::Integer8>&>(object);
		}
		else if  ( dynamic_cast<xdata::AbstractVector&>(object).getElementType() == "int 16"  )
		{
			*dynamic_cast<xdata::Vector<xdata::Integer16>*>(columnData_[name]->elementAt(rowIndex)) =  dynamic_cast<xdata::Vector<xdata::Integer16>&>(object);
		}
		else if  ( dynamic_cast<xdata::AbstractVector&>(object).getElementType() == "int 32"  )
		{
			*dynamic_cast<xdata::Vector<xdata::Integer32>*>(columnData_[name]->elementAt(rowIndex)) =  dynamic_cast<xdata::Vector<xdata::Integer32>&>(object);
		}
		else if  ( dynamic_cast<xdata::AbstractVector&>(object).getElementType() == "int 64"  )
		{
			*dynamic_cast<xdata::Vector<xdata::Integer64>*>(columnData_[name]->elementAt(rowIndex)) =  dynamic_cast<xdata::Vector<xdata::Integer64>&>(object);
		}
		else if  ( dynamic_cast<xdata::AbstractVector&>(object).getElementType() == "float"  )
		{
			*dynamic_cast<xdata::Vector<xdata::Float>*>(columnData_[name]->elementAt(rowIndex)) =  dynamic_cast<xdata::Vector<xdata::Float>&>(object);
		}
		else if  ( dynamic_cast<xdata::AbstractVector&>(object).getElementType() == "double"  )
		{
			*dynamic_cast<xdata::Vector<xdata::Double>*>(columnData_[name]->elementAt(rowIndex)) =  dynamic_cast<xdata::Vector<xdata::Double>&>(object);

		}
		else if  ( dynamic_cast<xdata::AbstractVector&>(object).getElementType() == "bool"  )
		{
			*dynamic_cast<xdata::Vector<xdata::Boolean>*>(columnData_[name]->elementAt(rowIndex)) =  dynamic_cast<xdata::Vector<xdata::Boolean>&>(object);

		}
		else if  ( dynamic_cast<xdata::AbstractVector&>(object).getElementType() == "time"  )
		{
			*dynamic_cast<xdata::Vector<xdata::TimeVal>*>(columnData_[name]->elementAt(rowIndex)) =  dynamic_cast<xdata::Vector<xdata::TimeVal>&>(object);

		}
		else if  ( dynamic_cast<xdata::AbstractVector&>(object).getElementType() == "string"  )
		{
			*dynamic_cast<xdata::Vector<xdata::String>*>(columnData_[name]->elementAt(rowIndex)) =  dynamic_cast<xdata::Vector<xdata::String>&>(object);

		}

	}
}
void xdata::Table::removeColumn(const std::string & name) throw (xdata::exception::Exception)
		{
	std::map<std::string, std::string, std::less<std::string> >::iterator i = columnDefinitions_.find(name);
	if  ( i == columnDefinitions_.end()  )
	{
		std::string msg = "invalid column name: ";
		msg += name;
		XCEPT_RAISE (xdata::exception::Exception, msg);
	}

	if ( (*i).second == "unsigned long" )
	{
		delete dynamic_cast<xdata::Vector<xdata::UnsignedLong>*>(columnData_[name]);
	}
	else if (  (*i).second == "unsigned int" )
	{
		delete dynamic_cast<xdata::Vector<xdata::UnsignedInteger>*>(columnData_[name]);
	}
	else if (  (*i).second == "unsigned int 8" )
	{
		delete dynamic_cast<xdata::Vector<xdata::UnsignedInteger8>*>(columnData_[name]);
	}
	else if (  (*i).second == "unsigned int 16" )
	{
		delete dynamic_cast<xdata::Vector<xdata::UnsignedInteger16>*>(columnData_[name]);
	}
	else if (  (*i).second == "unsigned int 32" )
	{
		delete dynamic_cast<xdata::Vector<xdata::UnsignedInteger32>*>(columnData_[name]);
	}
	else if (  (*i).second == "unsigned int 64" )
	{
		delete dynamic_cast<xdata::Vector<xdata::UnsignedInteger64>*>(columnData_[name]);
	}
	else if (  (*i).second == "unsigned short" )
	{
		delete dynamic_cast<xdata::Vector<xdata::UnsignedShort>*>(columnData_[name]);
	}
	else if (  (*i).second == "float" )
	{
		delete dynamic_cast<xdata::Vector<xdata::Float>*>(columnData_[name]);
	}
	else if (  (*i).second == "int" )
	{
		delete dynamic_cast< xdata::Vector<xdata::Integer>*>(columnData_[name]);
	}
	else if (  (*i).second == "int 8" )
	{
		delete dynamic_cast< xdata::Vector<xdata::Integer8>*>(columnData_[name]);
	}
	else if (  (*i).second == "int 16" )
	{
		delete dynamic_cast< xdata::Vector<xdata::Integer16>*>(columnData_[name]);
	}
	else if (  (*i).second == "int 32" )
	{
		delete dynamic_cast< xdata::Vector<xdata::Integer32>*>(columnData_[name]);
	}
	else if (  (*i).second == "int 64" )
	{
		delete dynamic_cast< xdata::Vector<xdata::Integer64>*>(columnData_[name]);
	}
	else if (  (*i).second == "string" )
	{
		delete dynamic_cast<xdata::Vector<xdata::String>*>(columnData_[name]);
	}
	else if (  (*i).second == "bool" )
	{
		delete dynamic_cast<xdata::Vector<xdata::Boolean>*>(columnData_[name]);
	}
	else if (  (*i).second == "double" )
	{
		delete dynamic_cast<xdata::Vector<xdata::Double>*>(columnData_[name]);
	}
	else if (  (*i).second == "time" )
	{
		delete dynamic_cast<xdata::Vector<xdata::TimeVal>*>(columnData_[name]);
	}
	else if (  (*i).second == "mime" )
	{
		delete dynamic_cast<xdata::Vector<xdata::Mime>*>(columnData_[name]);
	}
	else if ( (*i).second == "table" )
	{
		delete dynamic_cast<xdata::Vector<xdata::Table>*>(columnData_[name]);
	}
	else if ( (*i).second == "vector unsigned int" )
	{
		delete dynamic_cast<xdata::Vector<xdata::Vector<xdata::UnsignedInteger> >*>(columnData_[name]);
	}
	else if ( (*i).second == "vector unsigned int 8" )
	{
		delete dynamic_cast<xdata::Vector<xdata::Vector<xdata::UnsignedInteger8> >*>(columnData_[name]);
	}
	else if ( (*i).second == "vector unsigned int 16" )
	{
		delete dynamic_cast<xdata::Vector<xdata::Vector<xdata::UnsignedInteger16> >*>(columnData_[name]);
	}
	else if ( (*i).second == "vector unsigned int 32" )
	{
		delete dynamic_cast<xdata::Vector<xdata::Vector<xdata::UnsignedInteger32> >*>(columnData_[name]);
	}
	else if ( (*i).second == "vector unsigned int 64" )
	{
		delete dynamic_cast<xdata::Vector<xdata::Vector<xdata::UnsignedInteger64> >*>(columnData_[name]);
	}
	else if ( (*i).second == "vector unsigned short" )
	{
		delete dynamic_cast<xdata::Vector<xdata::Vector<xdata::UnsignedShort> >*>(columnData_[name]);
	}
	else if ( (*i).second == "vector int" )
	{
		delete dynamic_cast<xdata::Vector<xdata::Vector<xdata::Integer> >*>(columnData_[name]);
	}
	else if ( (*i).second == "vector int 8" )
	{
		delete dynamic_cast<xdata::Vector<xdata::Vector<xdata::Integer8> >*>(columnData_[name]);
	}
	else if ( (*i).second == "vector int 16" )
	{
		delete dynamic_cast<xdata::Vector<xdata::Vector<xdata::Integer16> >*>(columnData_[name]);
	}
	else if ( (*i).second == "vector int 32" )
	{
		delete dynamic_cast<xdata::Vector<xdata::Vector<xdata::Integer32> >*>(columnData_[name]);
	}
	else if ( (*i).second == "vector int 64" )
	{
		delete dynamic_cast<xdata::Vector<xdata::Vector<xdata::Integer64> >*>(columnData_[name]);
	}
	else if ( (*i).second == "vector float" )
	{
		delete dynamic_cast<xdata::Vector<xdata::Vector<xdata::Float> >*>(columnData_[name]);
	}
	else if ( (*i).second == "vector string" )
	{
		delete dynamic_cast<xdata::Vector<xdata::Vector<xdata::String> >*>(columnData_[name]);
	}
	else if ( (*i).second == "vector bool" )
	{
		delete dynamic_cast<xdata::Vector<xdata::Vector<xdata::Boolean> >*>(columnData_[name]);
	}
	else if ( (*i).second == "vector double" )
	{
		delete dynamic_cast<xdata::Vector<xdata::Vector<xdata::Double> >*>(columnData_[name]);
	}
	else if ( (*i).second == "vector time" )
	{
		delete dynamic_cast<xdata::Vector<xdata::Vector<xdata::TimeVal> >*>(columnData_[name]);
	}

	columnData_.erase(name);
	columnDefinitions_.erase(i);
		}


void xdata::Table::addColumn(const std::string & name, const std::string & type) throw (xdata::exception::Exception)
		{
	if ( columnDefinitions_.find(name) == columnDefinitions_.end() )
	{
		if ( type == "unsigned long" )
		{
			columnData_[name] = new xdata::Vector<xdata::UnsignedLong>();
		}
		else if ( type == "unsigned int" )
		{
			columnData_[name] = new xdata::Vector<xdata::UnsignedInteger>();
		}
		else if ( type == "unsigned int 8" )
		{
			columnData_[name] = new xdata::Vector<xdata::UnsignedInteger8>();
		}
		else if ( type == "unsigned int 16" )
		{
			columnData_[name] = new xdata::Vector<xdata::UnsignedInteger16>();
		}
		else if ( type == "unsigned int 32" )
		{
			columnData_[name] = new xdata::Vector<xdata::UnsignedInteger32>();
		}
		else if ( type == "unsigned int 64" )
		{
			columnData_[name] = new xdata::Vector<xdata::UnsignedInteger64>();
		}
		else if ( type == "unsigned short" )
		{
			columnData_[name] = new xdata::Vector<xdata::UnsignedShort>();
		}
		else if ( type == "float" )
		{
			columnData_[name] = new xdata::Vector<xdata::Float>();
		}
		else if ( type == "int" )
		{
			columnData_[name] = new xdata::Vector<xdata::Integer>();
		}
		else if ( type == "int 8" )
		{
			columnData_[name] = new xdata::Vector<xdata::Integer8>();
		}
		else if ( type == "int 16" )
		{
			columnData_[name] = new xdata::Vector<xdata::Integer16>();
		}
		else if ( type == "int 32" )
		{
			columnData_[name] = new xdata::Vector<xdata::Integer32>();
		}
		else if ( type == "int 64" )
		{
			columnData_[name] = new xdata::Vector<xdata::Integer64>();
		}
		else if ( type == "string" )
		{
			columnData_[name] = new xdata::Vector<xdata::String>();
		}
		else if ( type == "bool" )
		{
			columnData_[name] = new xdata::Vector<xdata::Boolean>();
		}
		else if ( type == "double" )
		{
			columnData_[name] = new xdata::Vector<xdata::Double>();
		}
		else if ( type == "time" )
		{
			columnData_[name] = new xdata::Vector<xdata::TimeVal>();
		}
		else if ( type == "mime" )
		{
			columnData_[name] = new xdata::Vector<xdata::Mime>();
		}
		else if ( type == "table" )
		{
			columnData_[name] = new xdata::Vector<xdata::Table>();
		}
		else if ( type == "vector unsigned int" )
		{
			columnData_[name] = new xdata::Vector<xdata::Vector<xdata::UnsignedInteger> >();
		}
		else if ( type == "vector unsigned int 8" )
		{
			columnData_[name] = new xdata::Vector<xdata::Vector<xdata::UnsignedInteger8> >();
		}
		else if ( type == "vector unsigned int 16" )
		{
			columnData_[name] = new xdata::Vector<xdata::Vector<xdata::UnsignedInteger16> >();
		}
		else if ( type == "vector unsigned int 32" )
		{
			columnData_[name] = new xdata::Vector<xdata::Vector<xdata::UnsignedInteger32> >();
		}
		else if ( type == "vector unsigned int 64" )
		{
			columnData_[name] = new xdata::Vector<xdata::Vector<xdata::UnsignedInteger64> >();
		}
		else if ( type == "vector unsigned short" )
		{
			columnData_[name] = new xdata::Vector<xdata::Vector<xdata::UnsignedShort> >();
		}
		else if ( type == "vector int" )
		{
			columnData_[name] = new xdata::Vector<xdata::Vector<xdata::Integer> >();
		}
		else if ( type == "vector int 8" )
		{
			columnData_[name] = new xdata::Vector<xdata::Vector<xdata::Integer8> >();
		}
		else if ( type == "vector int 16" )
		{
			columnData_[name] = new xdata::Vector<xdata::Vector<xdata::Integer16> >();
		}
		else if ( type == "vector int 32" )
		{
			columnData_[name] = new xdata::Vector<xdata::Vector<xdata::Integer32> >();
		}
		else if ( type == "vector int 64" )
		{
			columnData_[name] = new xdata::Vector<xdata::Vector<xdata::Integer64> >();
		}
		else if ( type == "vector float" )
		{
			columnData_[name] = new xdata::Vector<xdata::Vector<xdata::Float> >();
		}
		else if ( type == "vector string" )
		{
			columnData_[name] = new xdata::Vector<xdata::Vector<xdata::String> >();
		}
		else if ( type == "vector bool" )
		{
			columnData_[name] = new xdata::Vector<xdata::Vector<xdata::Boolean> >();
		}
		else if ( type == "vector double" )
		{
			columnData_[name] = new xdata::Vector<xdata::Vector<xdata::Double> >();
		}
		else if ( type == "vector time" )
		{
			columnData_[name] = new xdata::Vector<xdata::Vector<xdata::TimeVal> >();
		}
		else
		{
			std::string msg = "invalid column type:";
			msg += type;
			XCEPT_RAISE (xdata::exception::Exception, msg);	
		}

		columnDefinitions_[name] = type;
		columnData_[name]->setSize(memorySize_);
	}
	else
	{
		std::string msg = "column ";
		msg += name;
		msg += " already declared";
		XCEPT_RAISE (xdata::exception::Exception, msg);
	}

		}

xdata::Table * xdata::Table::cartesianJoin(xdata::Table * t1,  xdata::Table * t2)
throw (xdata::exception::Exception)
{

	if ((t1 == 0) || (t2 ==0))
	{
		return 0;
	}


	xdata::Table * table = new xdata::Table();

	if ( ( t1->getRowCount() == 0 ) || (t2->getRowCount() == 0 ) )
	{
		// empty table
		return table;
	}
	else
	{
		// Prepare product table
		std::map<std::string, std::string, std::less<std::string> >::iterator i;
		for ( i = t1->columnDefinitions_.begin(); i != t1->columnDefinitions_.end(); i++ )
		{
			table->addColumn((*i).first, (*i).second );
		}
		for ( i = t2->columnDefinitions_.begin(); i != t2->columnDefinitions_.end(); i++ )
		{
			table->addColumn((*i).first, (*i).second );
		}

		// Fill data 
		size_t row = 0;
		std::map<std::string, xdata::AbstractVector *, std::less<std::string> >::iterator j;
		std::map<std::string, xdata::AbstractVector *, std::less<std::string> >::iterator k;
		for ( size_t r1 = 0; r1 < t1->numberOfRows_; r1++ )
		{
			for ( size_t r2 = 0; r2 < t2->numberOfRows_; r2++ )
			{
				for ( j = t1->columnData_.begin(); j != t1->columnData_.end(); j++ )
				{
					xdata::Serializable * s = ((*j).second)->elementAt(r1);
					table->setValueAt(row, (*j).first, *s );
				}

				for ( k = t2->columnData_.begin(); k != t2->columnData_.end(); k++ )
				{
					xdata::Serializable * s = ((*k).second)->elementAt(r2);
					table->setValueAt(row, (*k).first, *s );
				}

				row++;
			}			
		}

		return table;
	}
}

void xdata::Table::merge(xdata::Table * destination,  xdata::Table * source)
throw (xdata::exception::Exception)
{

	if (destination == 0)
	{
		XCEPT_RAISE (xdata::exception::Exception, "Destination table is empty");
	}


	if (source == 0) return;	

	std::map<std::string, std::string, std::less<std::string> >::iterator i;

	size_t row = destination->getRowCount();

	for ( size_t r = 0; r < source->numberOfRows_; r++ )
	{
		for ( i = destination->columnDefinitions_.begin(); i != destination->columnDefinitions_.end(); i++ )
		{
			xdata::Serializable * s = (source->columnData_[(*i).first])->elementAt(r);
			destination->setValueAt(row, (*i).first, *s );
		}
		row++;
	}	
}


xdata::Table *  xdata::Table::denormalize(xdata::Table * t)
throw (xdata::exception::Exception)
{	
	// denormalization of empty table is an empty table
	if (t == 0) return 0;

	xdata::Table * merged = 0;

	for ( size_t r = 0; r < t->numberOfRows_; r++ )
	{
		xdata::Table * normalized = 0;
		//std::cout << "normalize row: " << r <<  std::endl;

		std::map<std::string, std::string, std::less<std::string> >::iterator i;
		for ( i = t->columnDefinitions_.begin(); i != t->columnDefinitions_.end(); i++ )
		{

			xdata::Serializable * s = t->columnData_[(*i).first]->elementAt(r);
			xdata::Table * temporary = 0;

			//std::cout << "process field type: " << s->type() <<  std::endl;

			if (  s->type()  != "table" ) // simple type
			{

				temporary = new xdata::Table();
				temporary->addColumn((*i).first, (*i).second );
				temporary->setValueAt(0, (*i).first, *s );


			}
			else // it is a table
			{

				//std::cout << "denormalize inner table for row  (" << r <<  std::endl;

				//dynamic_cast<xdata::Table*>(s)->writeTo(std::cout);

				temporary  =  xdata::Table::denormalize(dynamic_cast<xdata::Table*>(s));
				//temporary->writeTo(std::cout);
				//std::cout << "normalize inner table end )" <<  std::endl << std::endl;

			}		

			if ( normalized != 0 ) 
			{
				//std::cout << "build cartesian product for row (: " << r <<  std::endl;
				//std::cout << "Normalized:" << std::endl;
				//normalized->writeTo(std::cout);
				//std::cout << "Temporary:" << std::endl;
				//temporary->writeTo(std::cout);
				//std::cout << "Cartesian:" << std::endl;
				if ( temporary != 0 )
				{
					xdata::Table * result = xdata::Table::cartesianJoin( normalized, temporary);
					//result->writeTo(std::cout);
					//std::cout << "build cartesian product end ) " <<  std::endl << std::endl;
					delete normalized;
					normalized = result;
					delete temporary;
				}
			}
			else
			{
				normalized = temporary;
			}	


		} // end of columns

		if (merged != 0)
		{
			//std::cout << "build merge  ( " <<  std::endl;
			xdata::Table::merge( merged, normalized);
			//merged->writeTo(std::cout);
			//std::cout << "build merge end  )" <<  std::endl;

			delete normalized;
			normalized = 0;
		}
		else
		{
			merged = normalized;
		}


	}

	return merged;			

}


xdata::Table::Row::Row(iterator & i): i_(i)
{
}


xdata::Table::Row::Row(const xdata::Table::Row & rhs) throw (xdata::exception::Exception)
		: i_(rhs.i_)
{
	// deep copy
	try
	{
		this->operator=(rhs);
	}
	catch (xdata::exception::Exception& e)
	{
		std::stringstream msg;
		msg << "Failed to copy table row into row";
		XCEPT_RETHROW (xdata::exception::Exception, msg.str(), e);
	}
}

xdata::Serializable * xdata::Table::Row::getField(const std::string & name) throw (xdata::exception::Exception)
		{
	try
	{
		return i_.m_pCont->getValueAt( i_.m_Pos, name);
	}
	catch (xdata::exception::Exception& e)
	{
		std::stringstream msg;
		msg << "Failed to get field named '" << name << "'";
		XCEPT_RETHROW (xdata::exception::Exception, msg.str(), e);
	}
		}

void xdata::Table::Row::setField ( const std::string & name, Serializable & object ) throw (xdata::exception::Exception)
		{
	try
	{
		i_.m_pCont->setValueAt( i_.m_Pos, name, object);
	}
	catch (xdata::exception::Exception& e)
	{
		std::stringstream msg;
		msg << "Failed to set field named '" << name << "'";
		XCEPT_RETHROW (xdata::exception::Exception, msg.str(), e);
	}
		}


xdata::Table::Row & xdata::Table::Row::operator=(const Row & rhs) throw (xdata::exception::Exception)
		{
	if ( this != &rhs )
	{
		std::vector<std::string> fields = rhs.i_.m_pCont->getColumns();

		try
		{
			for ( std::vector<std::string>::size_type i = 0; i < fields.size(); i++ )
			{
				// if field is not found in rhs an exception will be thrownn
				i_.m_pCont->setValueAt( i_.m_Pos, fields[i], *(rhs.i_.m_pCont->getValueAt( rhs.i_.m_Pos, fields[i])));
			}
		}
		catch (xdata::exception::Exception& e)
		{
			std::stringstream msg;
			msg << "Failed to assign row to table";
			XCEPT_RETHROW (xdata::exception::Exception, msg.str(), e);
		}
	}
	return *this;
		}



// Iterator support
xdata::Table::iterator xdata::Table::begin()
{
	return iterator(this);
}

xdata::Table::iterator xdata::Table::end()
{
	iterator i (this);
	i.m_End = true;
	i.m_Pos = 0;

	return i; 
}

// This is similar to an STL vector iterator
xdata::Table::iterator xdata::Table::erase(xdata::Table::iterator pos)
{
	if ( pos.m_pCont == 0 ) 
		return this->end();

	if ( pos == this->end() ) 
		return this->end();

	if ( pos.m_Pos >=  numberOfRows_ ) 
		return this->end();

	std::map<std::string, xdata::AbstractVector *, std::less<std::string> >::iterator mi;

	for ( mi = columnData_.begin(); mi != columnData_.end(); mi++ )
	{
		(*mi).second->erase(pos.m_Pos); 
	}

	numberOfRows_--;
	memorySize_--;

	if ( pos.m_Pos == numberOfRows_ ) // I am at the end
	{
		xdata::Table::iterator newpos(this);
		newpos.m_End = true;
		newpos.m_Pos = 0;
		return newpos;
	}

	return pos;
}

xdata::Table::iterator xdata::Table::append()
throw (xdata::exception::Exception)
{
	if  (columnDefinitions_.empty())
	{
		std::string msg = "Cannot append, no column definitions";
		XCEPT_RAISE (xdata::exception::Exception, msg);
	}

	// resize table is required
	if (  memorySize_ < (numberOfRows_+1) ) 
	{
		memorySize_ = memorySize_ + 10;
		for (std::map<std::string, AbstractVector *, std::less<std::string> >::iterator i = columnData_.begin(); i != columnData_.end(); i++)
		{
			(*i).second->setSize(memorySize_);
		}	
	}

	xdata::Table::iterator i (this);
	i.m_End = false;
	i.m_Pos = numberOfRows_;

	numberOfRows_++;

	return i;
}


std::pair<xdata::Table::iterator, bool> xdata::Table::insert(xdata::Table::Row & value)
throw (xdata::exception::Exception)
{
	size_t position = this->getRowCount();

	std::vector<std::string> fields = value.i_.m_pCont->getColumns();

	for ( std::vector<std::string>::size_type i = 0; i < fields.size(); i++ )
	{
		// if field is not found in rhs an exception will be thrownn
		this->setValueAt( position, fields[i], *(value.getField(fields[i])) );
	}

	xdata::Table::iterator i (this);
	i.m_End = false;
	i.m_Pos = position;
	std::pair<xdata::Table::iterator, bool> p(i,true);
	return p;

	// std::pair<xdata::Table::iterator, bool>  p(this->end(),false);
	// return p;
}

void xdata::Table::readFrom(std::istream & stream)
{
	size_t rows = 0;
	size_t cols = 0;

	// Read the two values "rows" and "cols"

	for (size_t i = 0; i < 2; ++i)
	{	
		std::string key;
		stream.ignore (256, '"');
		(void) std::getline (stream, key, '"');
		stream.ignore (256, ':');

		if (key == "rows")
		{
			stream >> std::skipws >> rows;
			std::cout << "Rows: " << rows << std::endl;
		}
		else if (key == "cols")
		{
			stream >> std::skipws >> cols;
			std::cout << "Columns: " << cols << std::endl;
		}
		else
		{
			std::cout << "ERROR PARSING TABLE, key is: " << key << std::endl;
			return;
		}
	}

	this->reserve(rows);
	numberOfRows_ = rows;

	// Read: , "columns": [
	stream.ignore (256, '[');

	// Parse the column names
	std::string name;
	size_t columns = 0;

	std::vector<xdata::AbstractVector *> columnVector;

	// Read column definitions in form {"NAME","TYPE"},{...}

	for (size_t i = 0; i < cols; ++i)
	{
		stream.ignore(256,'[');

		// Read column name/type up to ending ,
		std::string definitionLine;
		stream.ignore(256,'"');
		std::string name;
		(void) std::getline (stream, name, '"');
		stream.ignore(256,'"');
		std::string value;
		(void) std::getline (stream, value, '"');

		std::cout << "Add Column " << name << ", type: " << value << std::endl;
		this->addColumn(name,value);
		columnVector.push_back(columnData_[name]);
		++columns;

		stream.ignore(256,']');		
	}

	// Skip everyting up to next [:  "data": [
	stream.ignore (256,'[');

	// Now read one row after the other
	for (size_t r = 0; r < rows; ++r)
	{
		stream.ignore (256,'[');

		for (size_t col = 0; col < columns; ++col)
		{
			// Read column value up to comma separator
			bool isString = false;

			std::string value;
			stream >> std::skipws;
			char c;
			for (;;)
			{				
				stream >> c;
				switch (c)
				{
				case ' ':
				case '\r':
				case '\n':
				case '\t':
				case ',': // also ignore leading commas
					continue;
				case '"': // also ignore leading quote
					isString = true;
					stream >> std::noskipws;
					goto label_A;
					break;
				default:
					value.append(1,c);
					goto label_A;
					break;
				}						
			}

			label_A:

			for (;;)
			{				
				stream >> c;
				switch (c)
				{
				case ',':
				case ']':
					if (isString)
					{
						value.append(1,c);
					}
					else
					{
						goto label_B; // end of string
					}
					break;
				case '"':
					if (isString)
					{
						goto label_B; // end of string
					}
				case '\\':
					stream >> c;// unquote, skip quote and read following character to string
				default:
					value.append(1,c);
					break;
				}						
			}

			label_B:

			xdata::AbstractVector* v = columnVector[col];
			Serializable* serializable = v->elementAt(r);
			if (serializable->type() == "string")
			{
				serializable->fromString(toolbox::unquote(value));
			}
			else
			{
				serializable->fromString(value);
			}
		}
	}
}

void xdata::Table::writeTo(std::ostream & stream) const
{	
	std::map<std::string, std::string, ci_less >::const_iterator ci = columnDefinitions_.begin();

	std::vector<std::string> columns;

	stream << "{\"rows\":" << numberOfRows_ << ",\"cols\":" << columnDefinitions_.size() << ",\"definition\":[";

	// Stream out header
	while(ci != columnDefinitions_.end())
	{	
		if (ci != columnDefinitions_.begin())
		{
			stream << ",";
		}
		columns.push_back((*ci).first);
		// Format is {"key": "NAME", "type":"TYPE"}
		//
		//stream << "{\"key\":\"" << (*ci).first << "\",\"type\":\"" << (*ci).second << "\"}";

		// Format is ["KEY","TYPE"]
		//
		stream << "[\"" << (*ci).first << "\",\"" << (*ci).second << "\"]";

		++ci;
	}

	stream << "],\"data\":[";

	// Stream out data
	for ( size_t j = 0; j < this->getRowCount(); ++j)
	{
		if (j != 0)
		{
			stream << ",["; // following rows
		}
		else
		{
			stream << "["; // first row without leading comma
		}


		for (std::vector<std::string>::size_type k = 0; k < columns.size(); k++ )
		{
			if (k != 0) 
			{
				stream << ","; // not first column in row
			}
			xdata::Serializable * s = this->getValueAt(j, columns[k]);
			if (s != 0)
			{
				if (s->type() == "string")
				{
					// Output with quoted text
					stream << "\"" << dynamic_cast<xdata::String*>(s)->toString(true) << "\"";
				}
				else
				{
					stream << s->toString();
				}
			}
			else
			{
				stream << "null";
			}
		}

		stream << "]"; // close row
	}

	stream << "]}";
}
