// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xdata/Properties.h"
#include "xdata/XStr.h"

xdata::Properties::Properties() throw (xdata::exception::Exception)
{
}

xdata::Properties::Properties(const xdata::Properties & b) throw (xdata::exception::Exception)
{
	this->setValue(b); // also copies the properties map
}

xdata::Properties::~Properties()
{
}

int  xdata::Properties::equals(const xdata::Serializable& s) const throw (xdata::exception::Exception)
{
	// apply to this the function "==" with the parameter "const Monitorable<T>&"
	// inverting the two elements will cause C++ to call another "==" operator.
	//
	if (s.type() != "properties")
	{
		std::string msg = "Cannot compare properties with serializable of type ";
		msg += s.type();
		XCEPT_RAISE (xdata::exception::Exception, msg);
	}
	return *this == dynamic_cast<const  xdata::Properties&>(s);
}


std::string xdata::Properties::type() const
{
	return "properties";
}

std::string xdata::Properties::getProperty(const std::string & name) const
{
	std::map<std::string, std::string, std::less<std::string> >::const_iterator i =  (*this).find(name);
	if ( i != (*this).end() )
		return (*i).second;
	else 
		return "";
}

void  xdata::Properties::setProperty(const std::string& name, const std::string& value) 
{
	(*this)[name] = value;
}

bool xdata::Properties::hasProperty (const std::string& name) const
{
	if ((*this).find(name) !=(*this).end())
	{
		return true;
	}
	else
	{
		return false;
	}
}


std::string xdata::Properties::toString() const throw (xdata::exception::Exception)
{
	XCEPT_RAISE (xdata::exception::Exception, "toString of xdata::Properties not provided");
	return "";
}

void xdata::Properties::fromString(const std::string& value) throw (xdata::exception::Exception)
{
	XCEPT_RAISE (xdata::exception::Exception, "fromString of xdata::Properties not provided");
}


void xdata::Properties::setValue(const xdata::Serializable & s) throw (xdata::exception::Exception)
{
	(*this).std::map<std::string, std::string, std::less<std::string> >::operator=(dynamic_cast<const xdata::Properties &>(s));
}

