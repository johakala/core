// $Id: TableIndex.cc,v 1.5 2008/07/18 15:28:12 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "toolbox/string.h"
#include "xdata/TableIndex.h"
#include "xdata/Table.h"
#include "xdata/TableIterator.h"

#include <sstream>

// Begin of TableIndex::Key implementation
// ---------------------------------------

xdata::TableIndex::Key::Key(std::list<std::string> & key, xdata::Table::Row & r )
throw (xdata::exception::Exception)
{
	try
	{
		std::stringstream s;
		for (std::list<std::string>::iterator i = key.begin(); i != key.end(); i++ )
		{
			s << "-" << r.getField(*i)->toString();
		}
		key_ = s.str();
	}
	catch (xdata::exception::Exception& e)
	{
		std::stringstream msg;
		msg << "Failed to create key from row using key values '" << toolbox::printTokenList(key,",") << "'";
		XCEPT_RETHROW (xdata::exception::Exception, msg.str(), e);
	}
}

bool xdata::TableIndex::Key::operator<(const xdata::TableIndex::Key& k) const
{
	return key_ < k.key_ ? true : false;
}

bool xdata::TableIndex::Key::operator>(const xdata::TableIndex::Key& k) const
{
	return key_ > k.key_ ? true : false;
}

bool xdata::TableIndex::Key::operator==(const xdata::TableIndex::Key& k) const
{
	return key_ == k.key_ ? true : false;
}

std::string xdata::TableIndex::Key::toString() const
{
	return key_;
}

// END of TableIndex::Key implementation
// -------------------------------------


xdata::TableIndex::TableIndex(xdata::Table* pCont, std::list<std::string> & key) throw (xdata::exception::Exception)
{
	key_ = key;
	m_pCont = pCont;
	xdata::Table::iterator i;
	
	try
	{
		for ( i = pCont->begin(); i !=  pCont->end(); i++ )
		{
			xdata::TableIndex::Key k(key_, (*i));
			index_[k] = i;
		}
	}
	catch (xdata::exception::Exception& e)
	{
		std::stringstream msg;
		msg << "Failed tocreate table index with key '" << toolbox::printTokenList(key, ",") << "'";
		XCEPT_RETHROW (xdata::exception::Exception, msg.str(), e);
	}
}

xdata::Table::Row & xdata::TableIndex::operator[](xdata::TableIndex::Key & k) throw (xdata::exception::Exception)
{
	std::map<xdata::TableIndex::Key, xdata::Table::iterator >::iterator i = index_.find(k);
	if ( i != index_.end() )
	{
		return *((*i).second);
	}
	else
	{
		try
		{
			index_[k] = m_pCont->append();
			return *(index_[k]);
		}
		catch (xdata::exception::Exception& e)
		{
			std::stringstream msg;
			msg << "Failed to append a row to table index with key '" << toolbox::printTokenList(key_, ",") << "'";
			XCEPT_RETHROW (xdata::exception::Exception, msg.str(), e);
		}
	}
}

xdata::TableIndex::Key xdata::TableIndex::key(xdata::Table::Row & r) throw (xdata::exception::Exception)
{
	try
	{
		return xdata::TableIndex::Key(key_, r);
	}
	catch (xdata::exception::Exception& e)
	{
		std::stringstream msg;
		msg << "Failed to create index from row for table with key '" << toolbox::printTokenList(key_, ",") << "'";
		XCEPT_RETHROW (xdata::exception::Exception, msg.str(), e);
	}
}

xdata::TableIndex::Key xdata::TableIndex::insert( xdata::Table::Row & r) throw (xdata::exception::Exception)
{
	try
	{
		xdata::TableIndex::Key k(key_, r);
		
		std::map<xdata::TableIndex::Key, xdata::Table::iterator >::iterator i = index_.find(k);
		if ( i != index_.end() )
		{
			*((*i).second) = r;
		}
		else
		{
			try
			{
				std::pair<xdata::Table::iterator, bool> newRow = m_pCont->insert(r);
				index_[k] = newRow.first;
			}
			catch (xdata::exception::Exception& e)
			{
				std::stringstream msg;
				msg << "Failed to append a row to table index with key '" << toolbox::printTokenList(key_, ",") << "'";
				XCEPT_RETHROW (xdata::exception::Exception, msg.str(), e);
			}
		}
		
		return k;
	}
	catch (xdata::exception::Exception& e)
	{
		std::stringstream msg;
		msg << "Failed to insert into table using index with key '" << toolbox::printTokenList(key_, ",") << "'";
		XCEPT_RETHROW (xdata::exception::Exception, msg.str(), e);	
	}
}		

