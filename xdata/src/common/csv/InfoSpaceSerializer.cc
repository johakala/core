// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xdata/csv/InfoSpaceSerializer.h"
#include "xdata/csv/Serializer.h"
#include <fstream>

std::string xdata::csv::InfoSpaceSerializer::type() const
{
	return "infospace";
}


void xdata::csv::InfoSpaceSerializer::exportAll(xdata::Serializer * serializer,  xdata::Serializable * serializable, std::ofstream& f)
{
	std::map< std::string, xdata::Serializable*, std::less<std::string> > * b = dynamic_cast<std::map< std::string, xdata::Serializable*, std::less<std::string> >* >(serializable);
	std::map< std::string, xdata::Serializable*, std::less<std::string> >::iterator i;
	for ( i = b->begin(); i != b->end(); i++)
	{
		// fire listener before export
		dynamic_cast<xdata::InfoSpace*>(serializable)->fireItemValueRetrieve((*i).first, this );

		xdata::Serializable* var = (*i).second;		
		dynamic_cast<xdata::csv::Serializer*>(serializer)->exportAll(var, f);
	}
}

void  xdata::csv::InfoSpaceSerializer::import (xdata::Serializer * serializer,  xdata::Serializable * serializable, std::ifstream& f) 
	throw (xdata::exception::Exception)
{
	XCEPT_RAISE (xdata::exception::Exception, "Not implemented");	
	/*
	std::string type = XMLCh2String( ((DOMElement*) targetNode)->getAttributeNS (XStr(XSI_NAMESPACE_URI), XStr("type")));
	if (type != "csvenc:Struct")
	{
		std::string msg = "Import type mismatch in imported tag name ";
		msg += XMLCh2String (targetNode->getNodeName());
		msg += ", expected type csvenc:Struct, received type ";
		msg += type;
		XCEPT_RAISE (xdata::exception::Exception, msg);	
	}

	DOMNodeList* children = targetNode->getChildNodes();
	for (unsigned int i = 0; i < children->getLength(); i++)
	{
		DOMNode* current = children->item(i);

		if (current->getNodeType() == DOMNode::ELEMENT_NODE)
		{	
			std::string currentName = XMLCh2String(current->getLocalName());
			std::map< std::string, xdata::Serializable*, std::less<std::string> > * b = dynamic_cast<std::map< std::string, xdata::Serializable*, std::less<std::string> >* >(serializable);
			std::map< std::string, xdata::Serializable*, std::less<std::string> >::iterator i;
			i = b->find(currentName);
			if ( i != b->end() )
			{
				dynamic_cast<xdata::csv::Serializer*>(serializer)->import((*i).second, current);
				// Fire listener
				dynamic_cast<xdata::InfoSpace*>(serializable)->fireItemValueChanged(currentName, this );

			} else 
                        {
				std::string msg = "Infospace member ";
				msg += currentName;
				msg += " not found in imported tag name ";
				msg += XMLCh2String (targetNode->getNodeName());
				XCEPT_RAISE (xdata::exception::Exception, msg);
			}
		}
	}	
	*/
}

