// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include <fstream>
#include "xdata/csv/UnsignedShortSerializer.h"

std::string xdata::csv::UnsignedShortSerializer::type() const
{
	return "unsigned short";		
}


void xdata::csv::UnsignedShortSerializer::exportAll(xdata::Serializer * serializer,  xdata::Serializable * serializable, std::ofstream& f)
{
	xdata::UnsignedShort * variable = dynamic_cast<xdata::UnsignedShort*>(serializable);
	f << variable->toString();
}

void  xdata::csv::UnsignedShortSerializer::import (xdata::Serializer * serializer,  xdata::Serializable * serializable, std::ifstream& f) 
	throw (xdata::exception::Exception)
{
	XCEPT_RAISE (xdata::exception::Exception, "Not implemented"); 
	/*
	std::string type = XMLCh2String( ((DOMElement*) targetNode)->getAttributeNS (XStr(XSI_NAMESPACE_URI), XStr("type")));	
	// Check if tag is called <Parameter> and node type is ELEMENT node
	//
	if ( (type != "xsd:unsignedShort") || (targetNode->getNodeType() != DOMNode::ELEMENT_NODE) )
	{
		string msg = "Type import mismatch for tag name ";
		msg += XMLCh2String(targetNode->getNodeName());
		msg += ", expected type xsd:unsignedShort, received type ";
		msg += type;
		XCEPT_RAISE (xdata::exception::Exception, msg); 
	}
		
	xdata::UnsignedShort * variable = dynamic_cast<xdata::UnsignedShort*>(serializable);
		
	if (!targetNode->hasChildNodes())
	{
		// ERROR don't allow setting with empty tag
		std::string msg = "Missing unsigned short value for imported tag name ";
		msg += XMLCh2String(targetNode->getNodeName());
                XCEPT_RAISE (xdata::exception::Exception, msg);
	} else 
        {					
		std::string tmp = XMLCh2String ( targetNode->getFirstChild()->getNodeValue() );
		variable->fromString(tmp);
	}
	*/
}
