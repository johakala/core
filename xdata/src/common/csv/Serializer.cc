// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/


#include "xdata/csv/Serializer.h"  
#include "xdata/csv/VectorSerializer.h"  
#include "xdata/csv/IntegerSerializer.h"  
#include "xdata/csv/FloatSerializer.h" 
#include "xdata/csv/BooleanSerializer.h" 
#include "xdata/csv/StringSerializer.h"  
#include "xdata/csv/BagSerializer.h"  
#include "xdata/csv/UnsignedShortSerializer.h"  
#include "xdata/csv/UnsignedLongSerializer.h"  
#include "xdata/csv/DoubleSerializer.h"  
#include "xdata/csv/InfoSpaceSerializer.h"  
#include <fstream>

//
// Serializer * csvSerializer = new xdata::csv::Serializer("/tmp/filename.txt", true);
// xdata::Vector<xdata::Integer> myVector; 
// csvSerializer.serialize(myVector,node);
//
xdata::csv::Serializer::Serializer(std::string filename, bool append) throw (xdata::exception::Exception)
{
	// grabage collection is perfromed automatically at destruction time
	this->addObjectSerializer(new xdata::csv::VectorSerializer());
	this->addObjectSerializer(new xdata::csv::IntegerSerializer());
	this->addObjectSerializer(new xdata::csv::UnsignedShortSerializer());
	this->addObjectSerializer(new xdata::csv::UnsignedLongSerializer());
	this->addObjectSerializer(new xdata::csv::FloatSerializer());
	this->addObjectSerializer(new xdata::csv::DoubleSerializer());
	this->addObjectSerializer(new xdata::csv::BooleanSerializer());
	this->addObjectSerializer(new xdata::csv::StringSerializer());
	this->addObjectSerializer(new xdata::csv::BagSerializer());
	this->addObjectSerializer(new xdata::csv::InfoSpaceSerializer());

	// check if the file exists
	std::ios_base::openmode mode = std::ios::out | std::ios::trunc;
	if (append == true)
	{
		mode = std::ios::out | std::ios::app;
	}

	try
	{
		file_.exceptions ( std::ofstream::eofbit | std::ofstream::failbit | std::ofstream::badbit );
		file_.open (filename.c_str(), mode);
	} catch (std::ofstream::failure & fe)
	{
		// maybe need to close the file here
		std::string msg = "Error opening file ";
		msg += filename;
		msg += fe.what();
		XCEPT_RAISE (xdata::exception::Exception, msg);
	}
}	

xdata::csv::Serializer::~Serializer()
{
	file_.close();
}
	
void xdata::csv::Serializer::exportAll(xdata::Serializable * s, bool append) throw (xdata::exception::Exception)
{
	xdata::csv::ObjectSerializer * os = dynamic_cast<xdata::csv::ObjectSerializer*>(this->getObjectSerializer(s->type()));
	os->exportAll(this, s, file_);
	file_ << std::endl;
}
		
void xdata::csv::Serializer::import (xdata::Serializable * s) throw (xdata::exception::Exception)
{
	XCEPT_RAISE (xdata::exception::Exception, "Not implemented");
	
	// xdata::csv::ObjectSerializer * os = dynamic_cast<xdata::csv::ObjectSerializer*>(this->getObjectSerializer(s->type()));
	// os->import(this, s, targetNode);
}
