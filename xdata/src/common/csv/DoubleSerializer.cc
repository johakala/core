// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xdata/csv/DoubleSerializer.h"
#include <fstream>

std::string xdata::csv::DoubleSerializer::type() const
{
	return "double";		
}


void xdata::csv::DoubleSerializer::exportAll(xdata::Serializer * serializer,  xdata::Serializable * serializable, std::ofstream& f)
{
	xdata::Double * variable = dynamic_cast<xdata::Double*>(serializable);
	f << variable->toString();
}

void  xdata::csv::DoubleSerializer::import (xdata::Serializer * serializer,  xdata::Serializable * serializable, std::ifstream& f) 
	throw (xdata::exception::Exception)
{
	XCEPT_RAISE (xdata::exception::Exception, "Not implemented"); 
	/*
	std::string type = XMLCh2String( ((DOMElement*) targetNode)->getAttributeNS (XStr(XSI_NAMESPACE_URI), XStr("type")));	

	if ( (type != "xsd:double") || (targetNode->getNodeType() != DOMNode::ELEMENT_NODE) )
	{
		string msg = "Wrong node type or tag name ";
		msg += XMLCh2String(targetNode->getNodeName());
		msg += ", expected type xsd:double, received type ";
		XCEPT_RAISE (xdata::exception::Exception, msg); 
	}
		
	xdata::Double * variable = dynamic_cast<xdata::Double*>(serializable);
		
	if (!targetNode->hasChildNodes())
	{
		// ERROR don't allow setting with empty tag
		std::string msg = "Missing double value for imported tag name ";
		msg += XMLCh2String(targetNode->getNodeName());
                XCEPT_RAISE (xdata::exception::Exception, msg);
	} else 
        {					
		std::string tmp = XMLCh2String ( targetNode->getFirstChild()->getNodeValue() );
		variable->fromString(tmp);
	}
	*/
}
