// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xdata/csv/BooleanSerializer.h"
#include <fstream>

std::string xdata::csv::BooleanSerializer::type() const
{
	return "bool";
}


void xdata::csv::BooleanSerializer::exportAll( xdata::Serializer * serializer,  xdata::Serializable * serializable, std::ofstream& f)
{
	xdata::Boolean * variable = dynamic_cast<xdata::Boolean*>(serializable);
	f << variable->toString();
}



void xdata::csv::BooleanSerializer::import (xdata::Serializer * serializer,  xdata::Serializable * serializable, std::ifstream& f) 
	throw (xdata::exception::Exception)
{
	XCEPT_RAISE (xdata::exception::Exception, "Not implemented."); 
	/*
	std::string type = XMLCh2String( ((DOMElement*) targetNode)->getAttributeNS (XStr(XSI_NAMESPACE_URI), XStr("type")));		
	
	// Check if tag is called <Parameter> and node type is ELEMENT node
	//
	if ( (type != "xsd:boolean") || (targetNode->getNodeType() != DOMNode::ELEMENT_NODE) )
	{
		string msg = "Wrong node type or tag name ";
		msg += XMLCh2String(targetNode->getNodeName());
		msg += ", expected type xsd:boolean";
		XCEPT_RAISE (xdata::exception::Exception, msg); 
	}

	xdata::Boolean * variable = dynamic_cast<xdata::Boolean*>(serializable);
	
	if (!targetNode->hasChildNodes())
	{
		// ERROR don't allow setting with empty tag
		std::string msg = "Missing boolean value for parameter: ";
		msg += XMLCh2String(targetNode->getNodeName());
                XCEPT_RAISE (xdata::exception::Exception, msg);
	} else 
        {					
		std::string tmp = XMLCh2String ( targetNode->getFirstChild()->getNodeValue() );
		if ((tmp == "true") || (tmp == "false"))
		{
			variable->fromString(tmp);
		} else 
		{
			std::string msg = "Boolean type for imported tag ";
			msg += XMLCh2String(targetNode->getNodeName());
			msg += " has wrong value (true|false): ";
			msg += tmp;
			XCEPT_RAISE (xdata::exception::Exception, msg);
		}
	}	
	*/
}
