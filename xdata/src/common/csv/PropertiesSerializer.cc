// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xdata/csv/PropertiesSerializer.h"
#include "xdata/csv/Serializer.h"
#include "xdata/Properties.h"
#include <fstream>

std::string xdata::csv::PropertiesSerializer::type() const
{
	return "properties";
}

void xdata::csv::PropertiesSerializer::exportAll(xdata::Serializer * serializer,  xdata::Serializable * serializable, std::ofstream& f)
{	
	// serialization of properties
	//	
	std::map<std::string, std::string, std::less<std::string> >& p =  dynamic_cast<xdata::Properties&>(*serializable);
	
	for (std::map<std::string, std::string, std::less<std::string> >::iterator pi = p.begin(); pi != p.end(); pi++)
	{
		// write tuples: name, value
		f << (*pi).first << ", " << (*pi).second;
	}
	
}

void xdata::csv::PropertiesSerializer::import (xdata::Serializer * serializer,  xdata::Serializable * serializable, std::ifstream& f) 
	throw (xdata::exception::Exception)
{
	XCEPT_RAISE (xdata::exception::Exception, "Not implemented");	

}
