// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include <fstream>
#include "xdata/csv/StringSerializer.h"

std::string xdata::csv::StringSerializer::type() const
{
	return "string";		
}


void xdata::csv::StringSerializer::exportAll(xdata::Serializer * serializer,  xdata::Serializable * serializable, std::ofstream& f)
{
	xdata::String * variable = dynamic_cast<xdata::String*>(serializable);
	f << variable->toString();
}

void xdata::csv::StringSerializer::import (xdata::Serializer * serializer,  xdata::Serializable * serializable, std::ifstream& f) 
	throw (xdata::exception::Exception)
{
	XCEPT_RAISE (xdata::exception::Exception, "Not implemented"); 
	/*
	std::string type = XMLCh2String( ((DOMElement*) targetNode)->getAttributeNS (XStr(XSI_NAMESPACE_URI), XStr("type")));	
	// Check if tag is called <Parameter> and node type is ELEMENT node
	//
	if ( (type != "xsd:string") || (targetNode->getNodeType() != DOMNode::ELEMENT_NODE) )
	{
		string msg = "Wrong node type or imported tag name ";
		msg += XMLCh2String(targetNode->getNodeName());
		msg += ", expected type xsd:string received type ";
		msg += type;
		XCEPT_RAISE (xdata::exception::Exception, msg); 
	}
		
	xdata::String * variable = dynamic_cast<xdata::String*>(serializable);
		
	if (targetNode->hasChildNodes())
	{
		std::string tmp = XMLCh2String ( targetNode->getFirstChild()->getNodeValue() );
		variable->fromString(tmp);
	}
	*/
}

