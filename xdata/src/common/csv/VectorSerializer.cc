// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xdata/csv/VectorSerializer.h"
#include <sstream>
#include <fstream>

std::string xdata::csv::VectorSerializer::type() const
{
	return "vector";		
}

void xdata::csv::VectorSerializer::exportAll(xdata::Serializer * serializer, xdata::Serializable * serializable, std::ofstream& f)
{
	AbstractVector * v = dynamic_cast<AbstractVector*>(serializable);

	unsigned int i = 0;
	while (i < v->elements())
	{
		dynamic_cast<xdata::csv::Serializer*>(serializer)->exportAll(v->elementAt(i), f);
		
		if (++i < v->elements())
		{
			f << ", ";
		}
	}
}

void xdata::csv::VectorSerializer::import (xdata::Serializer * serializer,  Serializable * serializable, std::ifstream& f) 
	throw (xdata::exception::Exception)
{
	XCEPT_RAISE (xdata::exception::Exception, "Not implemented");
	/*
	AbstractVector * v = dynamic_cast<AbstractVector*>(serializable);

	std::string type = XMLCh2String( ((DOMElement*) targetNode)->getAttributeNS (XStr(XSI_NAMESPACE_URI), XStr("type")));
	
	if (type != "csvenc:Array")
	{
		string msg = "Array import mismatch, expected type csvenc:Array, received type ";
		msg += type;
		msg += " for tag ";
		msg += XMLCh2String (targetNode->getNodeName());
		XCEPT_RAISE (xdata::exception::Exception, msg);
	}
	
	// arrayType string is in format: csvenc:ArrayType="xsd:something[size]"
	std::string arrayType = XMLCh2String( ((DOMElement*) targetNode)->getAttributeNS (XStr(SOAPENC_NAMESPACE_URI), XStr("arrayType")));
	
	if (arrayType == "")
	{
		std::string msg = "Missing csvenc:arrayType specifier in SOAP input stream for tag ";
		msg += XMLCh2String (targetNode->getNodeName());
		XCEPT_RAISE (xdata::exception::Exception, msg);
	}	
	
	if (arrayType.find ("xsd:ur-type[") == std::string::npos)
	{
		std::string msg = "Could not find ur-type specifier in arrayType attribute of tag ";
		msg += XMLCh2String (targetNode->getNodeName());
		XCEPT_RAISE (xdata::exception::Exception, msg);
	}
	
	std::string sizeStr = arrayType.substr(arrayType.find("[")+1, arrayType.find("]")-1);
	std::stringstream sizeStream(sizeStr);
	int size = 0;
	sizeStream >> size;

	DOMNodeList* l = targetNode->getChildNodes();

	// Resize vector according to received size specification
	v->setSize( size );

	// Import all children
	for (unsigned int i = 0; i < l->getLength(); i++)
	{
		DOMNode* current = l->item(i);

		if (current->getNodeType() == DOMNode::ELEMENT_NODE)
		{					
			//string indexStr = getNodeAttribute (current, "index");
			std::string currentIndex = XMLCh2String( ((DOMElement*) current)->getAttributeNS (XStr(SOAPENC_NAMESPACE_URI), XStr("position")));


			if ( currentIndex == "" )
			{

				string msg = "Missing csvenc:position in item in array";
				msg += XMLCh2String (targetNode->getNodeName());
				msg += ", at item ";
				std::stringstream tmpIndex;
				tmpIndex << i;
				msg += tmpIndex.str();
				XCEPT_RAISE (xdata::exception::Exception, msg);
			}

			unsigned int index = 0;
			std::string indexStr = currentIndex.substr(currentIndex.find("[")+1, currentIndex.find("]")-1);
			std::stringstream indexStream(indexStr);
			indexStream >> index;

			if (index < v->elements()) 
			{
				dynamic_cast<xdata::csv::Serializer*>(serializer)->import(v->elementAt(index), current);

			} else 
			{
				// TBD
				// Need to add new values from a
				// type factory
				string msg = "Vector element position out of bounds during the import of tag ";
				msg += XMLCh2String (targetNode->getNodeName());
				msg += ", position: ";
				msg += currentIndex;
				XCEPT_RAISE (xdata::exception::Exception, msg);
			}	
		}
	}
	*/
}

