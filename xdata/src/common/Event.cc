// $Id: Event.cc,v 1.4 2008/07/18 15:28:12 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xdata/Event.h"

xdata::Event::Event(const std::string& type, void* originator)
{
	type_ = type;
	originator_ = originator;
}

xdata::Event::~Event()
{
}

std::string xdata::Event::type() const
{
	return type_;
}

void* xdata::Event::originator()
{
	return originator_;
}

