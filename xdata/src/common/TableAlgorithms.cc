// $Id: TableAlgorithms.cc,v 1.7 2008/10/13 12:58:58 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/


#include "xdata/TableAlgorithms.h"


xdata::Table::iterator xdata::find(xdata::Table * destination,  xdata::Table::Row & row, std::set<std::string> & key)
	throw (xdata::exception::Exception)
{
	if (destination == 0)
	{
		XCEPT_RAISE (xdata::exception::Exception, "Destination table is empty");
	}
			
	xdata::Table::iterator ti;
	for ( ti = destination->begin(); ti != destination->end(); ti++)
        {
		bool match = true;
		for (std::set<std::string>::iterator i = key.begin(); i != key.end(); ++i)
		{
			xdata::Serializable * s = row.getField(*i);
			xdata::Serializable * d = (*ti).getField(*i);
			if ( ! s->equals(*d) )
			{
				match = false;
				break;
			}
			
		}
		
		if ( match )
			return ti;
		
	}
	return 	destination->end();
	
}

void xdata::merge(xdata::Table * destination,  xdata::Table * source, std::set<std::string> & key)
	throw (xdata::exception::Exception)
{
	if (destination == 0)
	{
		XCEPT_RAISE (xdata::exception::Exception, "Destination table is empty");
	}
		
	if (source == 0) return;	

	
	
	std::map<std::string, xdata::AbstractVector *, xdata::Table::ci_less >& dstColumnData = destination->columnData_;
	std::map<std::string, xdata::AbstractVector *, xdata::Table::ci_less >& srcColumnData = source->columnData_;

	// check if the two tables have equal column definitions
	std::map<std::string, xdata::AbstractVector *, xdata::Table::ci_less >::iterator i;
	for (i = dstColumnData.begin(); i != dstColumnData.end(); ++i)
	{
		if (srcColumnData.find( (*i).first ) == srcColumnData.end())
		{
			std::stringstream msg;
			msg << "Failed to find column '" << (*i).first << "' in source table. Merge failed";
			XCEPT_RAISE (xdata::exception::Exception, msg.str());
		}
	}

	std::set<std::string>::iterator ki;
		
	for (size_t srci = 0; srci < source->numberOfRows_; ++srci)
	{
		bool merged = false;
		for (size_t dsti = 0; dsti < destination->numberOfRows_; ++dsti)
		{
			size_t matches = 0;
			for (ki = key.begin(); ki != key.end(); ++ki)
			{
				std::map<std::string, xdata::AbstractVector *, xdata::Table::ci_less >::iterator dstKey = dstColumnData.find(*ki);
				if (dstKey == dstColumnData.end())
				{
					std::stringstream msg;
					msg << "Failed to merge tables, hashkey '" << (*ki) << "' not found in destination table";
					XCEPT_RAISE (xdata::exception::Exception,msg.str());
				}
				xdata::Serializable* dstSerializable = (*dstKey).second->elementAt(dsti);
				
				std::map<std::string, xdata::AbstractVector *, xdata::Table::ci_less >::iterator srcKey = srcColumnData.find(*ki);
				if (srcKey == srcColumnData.end())
				{
					std::stringstream msg;
					msg << "Failed to merge tables, hashkey '" << (*ki) << "' not found in source table";
					XCEPT_RAISE (xdata::exception::Exception,msg.str());
				}
				xdata::Serializable* srcSerializable = (*srcKey).second->elementAt(srci);
				
				if (dstSerializable->equals(*srcSerializable))
				{
					++matches;				
				}
			}
			
			if (matches == key.size())
			{
				merged = true;
				// copy src into dst
				std::map<std::string, std::string, xdata::Table::ci_less >::iterator ci;
				for (ci = destination->columnDefinitions_.begin(); ci != destination->columnDefinitions_.end(); ++ci)
				{
					try
					{
						dstColumnData[(*ci).first]->elementAt(dsti)->setValue(*(srcColumnData[(*ci).first]->elementAt(srci)));
					}
					catch (xdata::exception::Exception& e)
					{
						std::stringstream msg;
						msg << "Failed to merge column definition '" << (*ci).first << "'";
						XCEPT_RETHROW (xdata::exception::Exception, msg.str(), e);
					}
				}	
			}
		}
		
		if (!merged)
		{
			// append the row of the src table
			std::map<std::string, std::string, xdata::Table::ci_less >::iterator ci;
			size_t row = destination->numberOfRows_;
			for (ci = destination->columnDefinitions_.begin(); ci != destination->columnDefinitions_.end(); ++ci)
			{
				//std::cout << "Insert, Number of rows in dst table: " << row << std::endl;
				try
				{
					destination->setValueAt(row, (*ci).first, *(srcColumnData[(*ci).first]->elementAt(srci)) );
				}
				catch (xdata::exception::Exception& e)
				{
					std::stringstream msg;
					msg << "Failed to merge column '" << (*ci).first << "' row " << row;
					XCEPT_RETHROW (xdata::exception::Exception, msg.str(), e);
				}
			}
		}		
	}	
}

