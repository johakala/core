// $Id: String.cc,v 1.7 2008/07/18 15:28:12 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xdata/String.h"
#include "toolbox/string.h"

xdata::String::String(std::string value):value_(value)
{
}

// uninitialized variable
xdata::String::String()
{
}

xdata::String::~String()
{
}

//! Creation from const char *
xdata::String::String( const char* s )
{
	value_ = s;
}

xdata::String::operator std::string ()  const
{ 
	return value_; 
}

void xdata::String::setValue(const xdata::Serializable & s)  throw (xdata::exception::Exception)
{	
 	try
    	{
		const xdata::String & v  = dynamic_cast<const xdata::String &>(s);
        	value_ = v.value_;
    	}
    	catch(std::bad_cast exp)
    	{
                XCEPT_RAISE(xdata::exception::Exception, exp.what());
    	}

}

int xdata::String::equals(const xdata::Serializable & s) const
{
	return (*this == dynamic_cast<const xdata::String&>(s));
}

xdata::String& xdata::String::operator=(const char * s)
{
	value_ = s;
	return *this;
}

int xdata::String::operator==(const std::string & s) const
{
	return (value_ == s);
}

int xdata::String::operator!=(const std::string & s) const
{
	return (value_ != s);
}

int xdata::String::operator<(const std::string & s) const
{
	return (value_ < s);
}

int xdata::String::operator>(const std::string & s) const
{
	return (value_ > s);
}


int xdata::String::operator==(const char* b) const
{
	return (value_ == b);
}

int xdata::String::operator!=(const char* b) const
{
	return (value_ != b);
}

int xdata::String::operator<(const char* b) const
{
	return (value_ < b);
}

int xdata::String::operator>(const char* b) const
{
	return (value_ > b);
}

std::string xdata::String::type() const
{
	return "string";
}

std::string xdata::String::toString () const throw (xdata::exception::Exception)
{	
	return value_;
}

std::string xdata::String::toString (bool quoted) const throw (xdata::exception::Exception)
{	
	if (!quoted)
	{
		return value_;
	}
	else
	{
		return toolbox::quote(value_);
	}
}

void  xdata::String::fromString (const std::string& value) throw (xdata::exception::Exception)
{
	value_ = value;
}


int xdata::String::operator==(const xdata::String & b) const
{
	return (value_ == b.value_);
}

int xdata::String::operator!=(const xdata::String & b) const
{
	return (value_ != b.value_);
}

int xdata::String::operator<(const xdata::String & b) const
{
	return (value_ < b.value_);
}

int xdata::String::operator<=(const xdata::String & b) const
{
	return (value_ <= b.value_);
}

int xdata::String::operator>(const xdata::String & b) const
{
	return (value_ > b.value_);
}

int xdata::String::operator>=(const xdata::String & b) const
{
	return (value_ >= b.value_);
}

const char* xdata::String::c_str() const
{
	return value_.c_str();
}

	
