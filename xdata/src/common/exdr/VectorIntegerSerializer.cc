// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xdata/exdr/VectorIntegerSerializer.h"
#include "xdata/AbstractVector.h"
#include "xdata/exdr/Serializer.h"
#include "xdata/exception/Exception.h"
#include "xdata/Integer.h"

std::string xdata::exdr::VectorIntegerSerializer::type() const
{
	return "vector int";
}

void xdata::exdr::VectorIntegerSerializer::exportAll
(
	xdata::exdr::Serializer * serializer,  
	xdata::Serializable * serializable, 
	xdata::exdr::OutputStreamBuffer * sbuf
) 
	throw (xdata::exception::Exception)
{
	// redundant it is already defined in the tag for the table column
	//serializer->encodeTag(xdata::exdr::Serializer::VectorInteger, sbuf);

	xdata::AbstractVector * v = dynamic_cast<xdata::AbstractVector*>(serializable);

	// encode the vector size always as a 64-bit integer, also on 32-bit platforms
	//
	size_t size = v->elements();
	if (size >= 0xFFFFFFFFU)
	{
		XCEPT_RAISE (xdata::exception::Exception, "Exceeded maximum vector size (> 2^32 elements)");
	}
	
	sbuf->encodeUInt32(static_cast<uint32_t>(size));

	for ( size_t i = 0; i < size; i++)
	{
		xdata::Integer *  e = dynamic_cast<xdata::Integer*>(v->elementAt(i));
		if ( e == 0 )
		{
                	XCEPT_RAISE(xdata::exception::Exception, "failed to cast serializable of xdata::Integer object during export " );
		}
		sbuf->encodeInt32(e->value_);

	}
}


void xdata::exdr::VectorIntegerSerializer::import 
(
	xdata::exdr::Serializer * serializer,  
	xdata::Serializable * serializable, 
	xdata::exdr::InputStreamBuffer * sbuf
) 
	throw (xdata::exception::Exception)
{
	// redundant it is already defined in the tag for the table column
	//serializer->decodeTag(xdata::exdr::Serializer::VectorInteger, sbuf);
	
	uint32_t size;
	sbuf->decodeUInt32(size);
	
	xdata::AbstractVector * v = dynamic_cast<xdata::AbstractVector*>(serializable);
	v->setSize(size);
	
	for (uint32_t i = 0; i < size; i++)
	{
		xdata::Integer * e = dynamic_cast<xdata::Integer*>(v->elementAt(i));
		if ( e == 0 )
		{
                	XCEPT_RAISE(xdata::exception::Exception, "failed to cast serializable of xdata::Integer object during import" );
		}

		e->limits_.reset();
		sbuf->decodeInt32(e->value_);
	}
}
