// $Id: VectorBooleanSerializer.cc,v 1.3 2008/07/18 15:28:13 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xdata/exdr/VectorBooleanSerializer.h"
#include "xdata/AbstractVector.h"
#include "xdata/exdr/Serializer.h"
#include "xdata/exception/Exception.h"
#include "xdata/Boolean.h"

xdata::exdr::VectorBooleanSerializer::~VectorBooleanSerializer()
{
}

std::string xdata::exdr::VectorBooleanSerializer::type() const
{
	return "vector bool";
}

void xdata::exdr::VectorBooleanSerializer::exportAll
(
	xdata::exdr::Serializer * serializer,  
	xdata::Serializable * serializable, 
	xdata::exdr::OutputStreamBuffer * sbuf
) 
	throw (xdata::exception::Exception)
{
	// redundant it is already defined in the tag for the table column
	//serializer->encodeTag(xdata::exdr::Serializer::VectorBoolean, sbuf);

	xdata::AbstractVector * v = dynamic_cast<xdata::AbstractVector*>(serializable);

	// encode the vector size always as a 64-bit integer, also on 32-bit platforms
	//
	size_t size = v->elements();
	if (size >= 0xFFFFFFFFU)
	{
		XCEPT_RAISE (xdata::exception::Exception, "Exceeded maximum vector size (> 2^32 elements)");
	}
	
	sbuf->encodeUInt32(static_cast<uint32_t>(size));

	for ( size_t i = 0; i < size; i++)
	{
		xdata::Boolean *  e = dynamic_cast<xdata::Boolean*>(v->elementAt(i));
		if ( e == 0 )
		{
                	XCEPT_RAISE(xdata::exception::Exception, "failed to cast serializable of xdata::Boolean object during export " );
		}
		sbuf->encode(e->value_);

	}
}


void xdata::exdr::VectorBooleanSerializer::import 
(
	xdata::exdr::Serializer * serializer,  
	xdata::Serializable * serializable, 
	xdata::exdr::InputStreamBuffer * sbuf
) 
	throw (xdata::exception::Exception)
{
	// redundant it is already defined in the tag for the table column
	//serializer->decodeTag(xdata::exdr::Serializer::VectorBoolean, sbuf);
	
	uint32_t size;
	sbuf->decodeUInt32(size);
	
	xdata::AbstractVector * v = dynamic_cast<xdata::AbstractVector*>(serializable);
	v->setSize(size);
	
	for (uint32_t i = 0; i < size; i++)
	{
		xdata::Boolean * e = dynamic_cast<xdata::Boolean*>(v->elementAt(i));
		if ( e == 0 )
		{
                	XCEPT_RAISE(xdata::exception::Exception, "failed to cast serializable of xdata::Boolean object during import" );
		}

		e->limits_.reset();
		sbuf->decode(e->value_);
	}
}
