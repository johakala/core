// $Id: FloatSerializer.cc,v 1.7 2008/07/18 15:28:13 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xdata/exdr/FloatSerializer.h"
#include "xdata/exdr/Serializer.h"

xdata::exdr::FloatSerializer::~FloatSerializer()
{
}

std::string xdata::exdr::FloatSerializer::type() const
{
	return "float";
}


void xdata::exdr::FloatSerializer::exportAll(xdata::exdr::Serializer * serializer,  xdata::Serializable * serializable, xdata::exdr::OutputStreamBuffer * sbuf) throw (xdata::exception::Exception)
{
	// cast serializable to Float
	xdata::Float * f = dynamic_cast<xdata::Float*>(serializable);
	if ( f == 0 )
        {
                XCEPT_RAISE(xdata::exception::Exception, "failed to cast serializable of xdata::Float object during export " );
        }

	serializer->encodeTag( xdata::exdr::Serializer::Float, f->limits_, sbuf);
	sbuf->encode(f->value_);
}

void xdata::exdr::FloatSerializer::import (xdata::exdr::Serializer * serializer,  xdata::Serializable * serializable, xdata::exdr::InputStreamBuffer * sbuf) throw (xdata::exception::Exception)
{
	xdata::Float * f = dynamic_cast<xdata::Float*>(serializable);
	if ( f == 0 )
        {
                XCEPT_RAISE(xdata::exception::Exception, "failed to cast serializable of xdata::Float object during import " );
        }

	serializer->decodeTag(xdata::exdr::Serializer::Float, f->limits_, sbuf);
	sbuf->decode(f->value_);
}
