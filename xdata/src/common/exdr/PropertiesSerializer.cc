// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xdata/exdr/PropertiesSerializer.h"
#include "xdata/exdr/Serializer.h"
#include "xdata/exception/Exception.h"
#include "xdata/UnsignedInteger32.h"

xdata::exdr::PropertiesSerializer::~PropertiesSerializer()
{
}

std::string xdata::exdr::PropertiesSerializer::type() const
{
	return "properties";
}


void xdata::exdr::PropertiesSerializer::exportAll(xdata::exdr::Serializer * serializer,  xdata::Serializable * serializable, xdata::exdr::OutputStreamBuffer * sbuf) throw (xdata::exception::Exception)
{
		serializer->encodeTag(xdata::exdr::Serializer::Properties, sbuf);
		
		// Stream:
		// "Type Properties"
		// number of properties
		// name/value pairs
		// fields encoded as in a bag (they have to match the C structure)
		
		// first serialize all the properties
		//
		xdata::Properties & p = dynamic_cast<xdata::Properties&>(*serializable);

		std::map<std::string, std::string, std::less<std::string> >::size_type size = p.size();
		
		if (size >= 0xFFFFFFFF)
		{
			XCEPT_RAISE (xdata::exception::Exception,"Exceeded size of properties map (> 2^32 entries)");
		}
		
		serializer->encodeTag ( xdata::exdr::Serializer::UnsignedInteger32, sbuf );
		sbuf->encodeUInt32 ( static_cast<uint32_t>(size) );
		
		for (std::map<std::string, std::string, std::less<std::string> >::const_iterator mi = p.begin(); mi != p.end(); mi++)
		{
			serializer->encodeTag ( xdata::exdr::Serializer::String, sbuf );
			sbuf->encode((*mi).first);
			serializer->encodeTag ( xdata::exdr::Serializer::String, sbuf );
			sbuf->encode((*mi).second);			
		}
		

}

void xdata::exdr::PropertiesSerializer::import (xdata::exdr::Serializer * serializer,  xdata::Serializable * serializable, xdata::exdr::InputStreamBuffer * sbuf) throw (xdata::exception::Exception)
{
	serializer->decodeTag(xdata::exdr::Serializer::Properties, sbuf);
	
	// first decode the number of properties and the name/value property pairs
	//
	serializer->decodeTag ( xdata::exdr::Serializer::UnsignedInteger32, sbuf );
	xdata::UnsignedInteger32T numProperties = 0;
	sbuf->decodeUInt32 ( numProperties );
	
	std::string name;
	std::string value;
	
	xdata::Properties* m = dynamic_cast<xdata::Properties*>(serializable);
	
	for (xdata::UnsignedInteger32T i = 0; i < numProperties; i++)
	{
		serializer->decodeTag ( xdata::exdr::Serializer::String, sbuf );		
		sbuf->decode(name);
		serializer->decodeTag ( xdata::exdr::Serializer::String, sbuf );		
		sbuf->decode(value);
		
		m->setProperty(name, value);	
	}
}
