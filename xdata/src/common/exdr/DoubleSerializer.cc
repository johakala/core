// $Id: DoubleSerializer.cc,v 1.6 2008/07/18 15:28:13 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xdata/exdr/DoubleSerializer.h"
#include "xdata/exdr/Serializer.h"

xdata::exdr::DoubleSerializer::~DoubleSerializer()
{
}

std::string xdata::exdr::DoubleSerializer::type() const
{
	return "double";
}


void xdata::exdr::DoubleSerializer::exportAll(xdata::exdr::Serializer * serializer,  xdata::Serializable * serializable, xdata::exdr::OutputStreamBuffer * sbuf) throw (xdata::exception::Exception)
{
	// cast serializable to Float
	xdata::Double * d = dynamic_cast<xdata::Double*>(serializable);
	if ( d == 0 )
        {
                XCEPT_RAISE(xdata::exception::Exception, "failed to cast serializable of xdata::Double object during export " );
        }

	serializer->encodeTag( xdata::exdr::Serializer::Double, d->limits_, sbuf);
	sbuf->encode(d->value_);

	return;
}

void xdata::exdr::DoubleSerializer::import (xdata::exdr::Serializer * serializer,  xdata::Serializable * serializable, xdata::exdr::InputStreamBuffer * sbuf) throw (xdata::exception::Exception)
{
	xdata::Double * d = dynamic_cast<xdata::Double*>(serializable);
	if ( d == 0 )
        {
                XCEPT_RAISE(xdata::exception::Exception, "failed to cast serializable of xdata::Double object during import " );
        }

	serializer->decodeTag(xdata::exdr::Serializer::Double, d->limits_, sbuf);
	sbuf->decode(d->value_);
}
