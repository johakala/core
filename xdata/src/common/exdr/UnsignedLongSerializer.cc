// $Id: UnsignedLongSerializer.cc,v 1.8 2008/07/18 15:28:13 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xdata/exdr/UnsignedLongSerializer.h"
#include "xdata/exdr/Serializer.h"

xdata::exdr::UnsignedLongSerializer::~UnsignedLongSerializer()
{
}

std::string xdata::exdr::UnsignedLongSerializer::type() const
{
	return "unsigned long";
}


void xdata::exdr::UnsignedLongSerializer::exportAll(xdata::exdr::Serializer * serializer,  xdata::Serializable * serializable, xdata::exdr::OutputStreamBuffer * sbuf) throw (xdata::exception::Exception)
{
	// cast serializable to Integer
	xdata::UnsignedLong *  i = dynamic_cast<xdata::UnsignedLong*>(serializable);
	if ( i == 0 )
        {
                XCEPT_RAISE(xdata::exception::Exception, "failed to cast serializable of xdata::UnsignedLong object during export" );
        }

	serializer->encodeTag( xdata::exdr::Serializer::UnsignedLong, i->limits_, sbuf);
	sbuf->encodeUInt64(i->value_);
}

void xdata::exdr::UnsignedLongSerializer::import (xdata::exdr::Serializer * serializer,  xdata::Serializable * serializable, xdata::exdr::InputStreamBuffer * sbuf) throw (xdata::exception::Exception)
{
	xdata::UnsignedLong * i = dynamic_cast<xdata::UnsignedLong*>(serializable);
	if ( i == 0 )
        {
                XCEPT_RAISE(xdata::exception::Exception, "failed to cast serializable of xdata::UnsignedLong object during import" );
        }

	serializer->decodeTag(xdata::exdr::Serializer::UnsignedLong, i->limits_, sbuf);
	sbuf->decodeUInt64(i->value_);
}
