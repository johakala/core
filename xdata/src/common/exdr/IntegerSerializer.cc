// $Id: IntegerSerializer.cc,v 1.8 2008/07/18 15:28:13 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xdata/exdr/IntegerSerializer.h"
#include "xdata/exdr/Serializer.h"

xdata::exdr::IntegerSerializer::~IntegerSerializer()
{
}

std::string xdata::exdr::IntegerSerializer::type() const
{
	return "int";
}


void xdata::exdr::IntegerSerializer::exportAll
(
	xdata::exdr::Serializer * serializer,
	xdata::Serializable * serializable,
	xdata::exdr::OutputStreamBuffer * sbuf
)
throw (xdata::exception::Exception)
{
	// cast serializable to Integer
	xdata::Integer *  i = dynamic_cast<xdata::Integer*>(serializable);
	if ( i == 0 )
        {
                XCEPT_RAISE(xdata::exception::Exception, "failed to cast serializable of xdata::Integer object during export " );
        }

	serializer->encodeTag( xdata::exdr::Serializer::Integer, i->limits_, sbuf);
	sbuf->encodeInt32(i->value_);
}

void xdata::exdr::IntegerSerializer::import
(
	xdata::exdr::Serializer * serializer,
	xdata::Serializable * serializable,
	xdata::exdr::InputStreamBuffer * sbuf
)
	throw (xdata::exception::Exception)
{
	xdata::Integer * i = dynamic_cast<xdata::Integer*>(serializable);
	if ( i == 0 )
        {
                XCEPT_RAISE(xdata::exception::Exception, "failed to cast serializable of xdata::Integer object during import" );
        }

	serializer->decodeTag(xdata::exdr::Serializer::Integer,i->limits_,sbuf);
	sbuf->decodeInt32(i->value_);
}

