// $Id: StringSerializer.cc,v 1.5 2008/07/18 15:28:13 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xdata/exdr/StringSerializer.h"
#include "xdata/exdr/Serializer.h"

xdata::exdr::StringSerializer::~StringSerializer()
{
}

std::string xdata::exdr::StringSerializer::type()  const
{
	return "string";
}


void xdata::exdr::StringSerializer::exportAll(xdata::exdr::Serializer * serializer,  xdata::Serializable * serializable, xdata::exdr::OutputStreamBuffer * sbuf) throw (xdata::exception::Exception)
{
	// cast serializable to String
	xdata::String * s = dynamic_cast<xdata::String*>(serializable);
 	if ( s == 0 )
        {
                XCEPT_RAISE(xdata::exception::Exception, "failed to cast serializable of xdata::String object during export " );
        }

	serializer->encodeTag( xdata::exdr::Serializer::String, sbuf);
	sbuf->encode(s->value_);
}

void xdata::exdr::StringSerializer::import (xdata::exdr::Serializer * serializer,  xdata::Serializable * serializable, xdata::exdr::InputStreamBuffer * sbuf) throw (xdata::exception::Exception)
{
	xdata::String * s = dynamic_cast<xdata::String*>(serializable);
	if ( s == 0 )
        {
                XCEPT_RAISE(xdata::exception::Exception, "failed to cast serializable of xdata::String object during import" );
        }

	serializer->decodeTag(xdata::exdr::Serializer::String, sbuf);
	sbuf->decode(s->value_); // this is a force cast to non const string
}
