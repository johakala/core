// $Id: VectorTimeValSerializer.cc,v 1.3 2008/07/18 15:28:13 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xdata/exdr/VectorTimeValSerializer.h"
#include "xdata/AbstractVector.h"
#include "xdata/exdr/Serializer.h"
#include "xdata/exception/Exception.h"
#include "xdata/TimeVal.h"

xdata::exdr::VectorTimeValSerializer::~VectorTimeValSerializer()
{
}

std::string xdata::exdr::VectorTimeValSerializer::type() const
{
	return "vector time";
}

void xdata::exdr::VectorTimeValSerializer::exportAll
(
	xdata::exdr::Serializer * serializer,  
	xdata::Serializable * serializable, 
	xdata::exdr::OutputStreamBuffer * sbuf
) 
	throw (xdata::exception::Exception)
{
	// redundant it is already defined in the tag for the table column
	//serializer->encodeTag(xdata::exdr::Serializer::VectorTimeVal, sbuf);

	xdata::AbstractVector * v = dynamic_cast<xdata::AbstractVector*>(serializable);

	// encode the vector size always as a 64-bit integer, also on 32-bit platforms
	//
	size_t size = v->elements();
	if (size >= 0xFFFFFFFFU)
	{
		XCEPT_RAISE (xdata::exception::Exception, "Exceeded maximum vector size (> 2^32 elements)");
	}
	
	sbuf->encodeUInt32(static_cast<uint32_t>(size));

	for ( size_t i = 0; i < size; i++)
	{
		xdata::TimeVal *  e = dynamic_cast<xdata::TimeVal*>(v->elementAt(i));
		if ( e == 0 )
		{
                	XCEPT_RAISE(xdata::exception::Exception, "failed to cast serializable of xdata::TimeVal object during export " );
		}
		sbuf->encode(e->toString());


	}
}


void xdata::exdr::VectorTimeValSerializer::import 
(
	xdata::exdr::Serializer * serializer,  
	xdata::Serializable * serializable, 
	xdata::exdr::InputStreamBuffer * sbuf
) 
	throw (xdata::exception::Exception)
{
	// redundant it is already defined in the tag for the table column
	//serializer->decodeTag(xdata::exdr::Serializer::VectorTimeVal, sbuf);
	
	uint32_t size;
	sbuf->decodeUInt32(size);
	
	xdata::AbstractVector * v = dynamic_cast<xdata::AbstractVector*>(serializable);
	v->setSize(size);
	
	for (uint32_t i = 0; i < size; i++)
	{
		xdata::TimeVal * e = dynamic_cast<xdata::TimeVal*>(v->elementAt(i));
		if ( e == 0 )
		{
                	XCEPT_RAISE(xdata::exception::Exception, "failed to cast serializable of xdata::TimeVal object during import" );
		}

		std::string timeString;
		sbuf->decode(timeString);
		e->fromString(timeString);
	}
}
