// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2017, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xdata/exdr/UnsignedInteger8Serializer.h"
#include "xdata/exdr/Serializer.h"

xdata::exdr::UnsignedInteger8Serializer::~UnsignedInteger8Serializer()
{
}

std::string xdata::exdr::UnsignedInteger8Serializer::type() const
{
	return "unsigned int 8";
}


void xdata::exdr::UnsignedInteger8Serializer::exportAll(xdata::exdr::Serializer * serializer,  xdata::Serializable * serializable, xdata::exdr::OutputStreamBuffer * sbuf) throw (xdata::exception::Exception)
{
	// cast serializable to Integer
	xdata::UnsignedInteger8 *  i = dynamic_cast<xdata::UnsignedInteger8*>(serializable);
	if ( i == 0 )
        {
                XCEPT_RAISE(xdata::exception::Exception, "failed to cast serializable of xdata::UnsignedInteger8 object during export " );
        }

	serializer->encodeTag( xdata::exdr::Serializer::UnsignedInteger8, i->limits_, sbuf);
	sbuf->encodeUInt8(i->value_);
}

void xdata::exdr::UnsignedInteger8Serializer::import (xdata::exdr::Serializer * serializer,  xdata::Serializable * serializable, xdata::exdr::InputStreamBuffer * sbuf) throw (xdata::exception::Exception)
{
	xdata::UnsignedInteger8 * i = dynamic_cast<xdata::UnsignedInteger8*>(serializable);
	if ( i == 0 )
        {
                XCEPT_RAISE(xdata::exception::Exception, "failed to cast serializable of xdata::UnsignedInteger8 object during import" );
        }

	serializer->decodeTag(xdata::exdr::Serializer::UnsignedInteger8, i->limits_, sbuf);
	sbuf->decodeUInt8(i->value_);
}
