// $Id: UnsignedShortSerializer.cc,v 1.7 2008/07/18 15:28:13 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xdata/exdr/UnsignedShortSerializer.h"
#include "xdata/exdr/Serializer.h"

xdata::exdr::UnsignedShortSerializer::~UnsignedShortSerializer()
{
}

std::string xdata::exdr::UnsignedShortSerializer::type() const
{
	return "unsigned short";
}


void xdata::exdr::UnsignedShortSerializer::exportAll(xdata::exdr::Serializer * serializer,  xdata::Serializable * serializable, xdata::exdr::OutputStreamBuffer * sbuf) throw (xdata::exception::Exception)
{
	// cast serializable to Integer
	xdata::UnsignedShort *  i = dynamic_cast<xdata::UnsignedShort*>(serializable);
	if ( i == 0 )
        {
                XCEPT_RAISE(xdata::exception::Exception, "failed to cast serializable of xdata::UnsignedShort object during export" );
        }
	serializer->encodeTag( xdata::exdr::Serializer::UnsignedShort, i->limits_, sbuf);
	sbuf->encodeUInt16(i->value_);
}

void xdata::exdr::UnsignedShortSerializer::import (xdata::exdr::Serializer * serializer,  xdata::Serializable * serializable, xdata::exdr::InputStreamBuffer * sbuf) throw (xdata::exception::Exception)
{
	xdata::UnsignedShort * i = dynamic_cast<xdata::UnsignedShort*>(serializable);
	if ( i == 0 )
        {
                XCEPT_RAISE(xdata::exception::Exception, "failed to cast serializable of xdata::UnsignedShort object during import" );
        }

	serializer->decodeTag(xdata::exdr::Serializer::UnsignedShort, i->limits_, sbuf);
	sbuf->decodeUInt16(i->value_);
}
