// $Id: UnsignedIntegerSerializer.cc,v 1.5 2008/07/18 15:28:13 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xdata/exdr/UnsignedIntegerSerializer.h"
#include "xdata/exdr/Serializer.h"

xdata::exdr::UnsignedIntegerSerializer::~UnsignedIntegerSerializer()
{
}

std::string xdata::exdr::UnsignedIntegerSerializer::type() const
{
	return "unsigned int";
}


void xdata::exdr::UnsignedIntegerSerializer::exportAll(xdata::exdr::Serializer * serializer,  xdata::Serializable * serializable, xdata::exdr::OutputStreamBuffer * sbuf) throw (xdata::exception::Exception)
{
	// cast serializable to Integer
	xdata::UnsignedInteger *  i = dynamic_cast<xdata::UnsignedInteger*>(serializable);
	if ( i == 0 )
        {
                XCEPT_RAISE(xdata::exception::Exception, "failed to cast serializable of xdata::UnsignedInteger object during export " );
        }

	serializer->encodeTag( xdata::exdr::Serializer::UnsignedInteger, i->limits_, sbuf);
	sbuf->encodeUInt32(i->value_);
}

void xdata::exdr::UnsignedIntegerSerializer::import (xdata::exdr::Serializer * serializer,  xdata::Serializable * serializable, xdata::exdr::InputStreamBuffer * sbuf) throw (xdata::exception::Exception)
{
	xdata::UnsignedInteger * i = dynamic_cast<xdata::UnsignedInteger*>(serializable);
	if ( i == 0 )
        {
                XCEPT_RAISE(xdata::exception::Exception, "failed to cast serializable of xdata::UnsignedInteger object during import" );
        }

	serializer->decodeTag(xdata::exdr::Serializer::UnsignedInteger, i->limits_, sbuf);
	sbuf->decodeUInt32(i->value_);
}
