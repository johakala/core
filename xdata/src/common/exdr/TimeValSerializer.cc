// $Id: TimeValSerializer.cc,v 1.3 2008/07/18 15:28:13 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xdata/exdr/TimeValSerializer.h"
#include "xdata/exdr/Serializer.h"

xdata::exdr::TimeValSerializer::~TimeValSerializer()
{
}

std::string xdata::exdr::TimeValSerializer::type() const
{
	return "time";
}

void xdata::exdr::TimeValSerializer::exportAll
(
	xdata::exdr::Serializer * serializer,
	xdata::Serializable * serializable,
	xdata::exdr::OutputStreamBuffer * sbuf
) 
throw (xdata::exception::Exception)
{
	// cast serializable to xdata::TimeVal pointer
	xdata::TimeVal * t  = dynamic_cast<xdata::TimeVal*>(serializable);
	if ( t == 0 )
	{
		XCEPT_RAISE(xdata::exception::Exception, "failed to cast serializable of xdata::TimeVal object during export" );
	}

	serializer->encodeTag( xdata::exdr::Serializer::TimeVal, sbuf);
	
	// the time types have different sizes on different machines
	// therefore the only safe way to encode the time is to
	// format it into a string with GMT time zone format
	// We would like to use local time zone information, but this does not work
	// see strptime problem.
	//
	sbuf->encode(t->toString());
}

void xdata::exdr::TimeValSerializer::import
(
	xdata::exdr::Serializer * serializer,
	xdata::Serializable * serializable,
	xdata::exdr::InputStreamBuffer * sbuf
) 
throw (xdata::exception::Exception)
{
	xdata::TimeVal * t = dynamic_cast<xdata::TimeVal*>(serializable);
	if ( t == 0 )
	{
		XCEPT_RAISE(xdata::exception::Exception, "failed to cast serializable of xdata::TimeVal object during import" )	;
	}
	serializer->decodeTag(xdata::exdr::Serializer::TimeVal, sbuf);
	
	// the time types have different sizes on different machines
	// therefore the only safe way to decode the time is to
	// read it from a string in GMT time zone format
	//
	std::string timeString;
	sbuf->decode(timeString);
	t->fromString(timeString);
}
