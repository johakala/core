// $Id: BooleanSerializer.cc,v 1.6 2008/07/18 15:28:13 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xdata/exdr/BooleanSerializer.h"
#include "xdata/exdr/Serializer.h"

xdata::exdr::BooleanSerializer::~BooleanSerializer()
{
}

std::string xdata::exdr::BooleanSerializer::type() const
{
	return "bool";
}


void xdata::exdr::BooleanSerializer::exportAll(xdata::exdr::Serializer * serializer,  xdata::Serializable * serializable, xdata::exdr::OutputStreamBuffer * sbuf) throw (xdata::exception::Exception)
{
	// cast serializable to Boolean
	xdata::Boolean * b  = dynamic_cast<xdata::Boolean*>(serializable);
	if ( b == 0 )
        {
                XCEPT_RAISE(xdata::exception::Exception, "failed to cast serializable of xdata::Boolean object during export " );
        }

	serializer->encodeTag( xdata::exdr::Serializer::Boolean, b->limits_, sbuf);
	sbuf->encode(b->value_);
}

void xdata::exdr::BooleanSerializer::import (xdata::exdr::Serializer * serializer,  xdata::Serializable * serializable, xdata::exdr::InputStreamBuffer * sbuf) throw (xdata::exception::Exception)
{
	xdata::Boolean * b = dynamic_cast<xdata::Boolean*>(serializable);
	if ( b == 0 )
        {
                XCEPT_RAISE(xdata::exception::Exception, "failed to cast serializable of xdata::Boolean object during import " );
        }

	serializer->decodeTag(xdata::exdr::Serializer::Boolean, b->limits_, sbuf);
	sbuf->decode(b->value_);
}
