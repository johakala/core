// $Id: Integer64Serializer.cc,v 1.4 2008/07/18 15:28:13 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xdata/exdr/Integer64Serializer.h"
#include "xdata/exdr/Serializer.h"

xdata::exdr::Integer64Serializer::~Integer64Serializer()
{
}

std::string xdata::exdr::Integer64Serializer::type() const
{
	return "int 64";
}


void xdata::exdr::Integer64Serializer::exportAll
(
	xdata::exdr::Serializer * serializer,
	xdata::Serializable * serializable,
	xdata::exdr::OutputStreamBuffer * sbuf
) 
	throw (xdata::exception::Exception)
{
	// cast serializable to Integer
	xdata::Integer64 *  i = dynamic_cast<xdata::Integer64*>(serializable);
	if ( i == 0 )
        {
                XCEPT_RAISE(xdata::exception::Exception, "failed to cast serializable of xdata::Integer64 object during export " );
        }

	serializer->encodeTag( xdata::exdr::Serializer::Integer64, i->limits_, sbuf);
	sbuf->encodeInt64(i->value_);
}

void xdata::exdr::Integer64Serializer::import 
(
	xdata::exdr::Serializer * serializer,
	xdata::Serializable * serializable,
	xdata::exdr::InputStreamBuffer * sbuf
) 
	throw (xdata::exception::Exception)
{
	xdata::Integer64 * i = dynamic_cast<xdata::Integer64*>(serializable);
	if ( i == 0 )
        {
                XCEPT_RAISE(xdata::exception::Exception, "failed to cast serializable of xdata::Integer64  object during import" );
        }

	serializer->decodeTag(xdata::exdr::Serializer::Integer64, i->limits_, sbuf);
	sbuf->decodeInt64(i->value_);
}
