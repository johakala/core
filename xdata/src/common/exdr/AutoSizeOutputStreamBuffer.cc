// $Id: AutoSizeOutputStreamBuffer.cc,v 1.4 2008/07/18 15:28:13 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xdata/exdr/AutoSizeOutputStreamBuffer.h"
#include <errno.h>

// It starts from 4k and it grows in LOG2 e.g. 8, 16, 32 etc.
xdata::exdr::AutoSizeOutputStreamBuffer::AutoSizeOutputStreamBuffer()
	throw (xdata::exception::Exception)
	: OutputStreamBuffer((char*)0, 0)
{
	buffer_ = (char*)::malloc(512);
        size_ = 512;
	xdr_destroy(&xdr_);
	xdrmem_create(&xdr_, buffer_, size_ , XDR_ENCODE);
        if ( buffer_ == 0 )
        {
		XCEPT_RAISE(xdata::exception::Exception, "error in allocating memory for stream");
        }

} 

xdata::exdr::AutoSizeOutputStreamBuffer::~AutoSizeOutputStreamBuffer()
{
	::free(buffer_); 
}

void xdata::exdr::AutoSizeOutputStreamBuffer::overflow() throw (xdata::exception::Exception)
{
	// re-allocate buffer size * 2
	size_ = size_ * 2;

	errno = 0;
	char * buffer = (char*)::realloc(buffer_, size_ );
	//std::cout << "*** realloc memory for xdata::exdr::AutoSizeOutputStreamBuffer::~AutoSizeOutputStreamBuffer()"<< size_ << "address:" << std::hex << (size_t)buffer_ << std::dec << std::endl;
	if ( errno != 0 )
	{
		XCEPT_RAISE(xdata::exception::Exception, "failed to expand buffer size");
	}
	u_int current = xdr_getpos(&xdr_);

	buffer_ = buffer;
	xdr_destroy(&xdr_);

	xdrmem_create(&xdr_, buffer_, size_ , XDR_ENCODE);
	xdr_setpos(&xdr_, current);
}


char * xdata::exdr::AutoSizeOutputStreamBuffer::getBuffer()
{
	return buffer_;
}

