// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2017, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xdata/exdr/Integer16Serializer.h"
#include "xdata/exdr/Serializer.h"

xdata::exdr::Integer16Serializer::~Integer16Serializer()
{
}

std::string xdata::exdr::Integer16Serializer::type() const
{
	return "int 16";
}


void xdata::exdr::Integer16Serializer::exportAll
(
	xdata::exdr::Serializer * serializer,
	xdata::Serializable * serializable,
	xdata::exdr::OutputStreamBuffer * sbuf
) 
	throw (xdata::exception::Exception)
{
	// cast serializable to Integer
	xdata::Integer16 *  i = dynamic_cast<xdata::Integer16*>(serializable);
	if ( i == 0 )
        {
                XCEPT_RAISE(xdata::exception::Exception, "failed to cast serializable of xdata::Integer16 object during export " );
        }

	serializer->encodeTag( xdata::exdr::Serializer::Integer16, i->limits_, sbuf);
	sbuf->encodeInt16(i->value_);
}

void xdata::exdr::Integer16Serializer::import
(
	xdata::exdr::Serializer * serializer,
	xdata::Serializable * serializable,
	xdata::exdr::InputStreamBuffer * sbuf
) 
	throw (xdata::exception::Exception)
{
	xdata::Integer16 * i = dynamic_cast<xdata::Integer16*>(serializable);
	if ( i == 0 )
        {
                XCEPT_RAISE(xdata::exception::Exception, "failed to cast serializable of xdata::Integer16 object during import" );
        }

	serializer->decodeTag(xdata::exdr::Serializer::Integer16, i->limits_, sbuf);
	sbuf->decodeInt16(i->value_);
}
