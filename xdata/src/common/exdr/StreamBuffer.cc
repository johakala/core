// $Id: StreamBuffer.cc,v 1.14 2008/07/18 15:28:13 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xdata/exdr/StreamBuffer.h"

#include <sstream>
#include <string>
#include <stdint.h>

#if __APPLE__
#define xdr_int8_t(x,y)  xdr_u_char(x,y)
#define xdr_uint8_t(x,y)  xdr_u_char(x,y)
#define xdr_uint16_t(x,y) xdr_u_int16_t(x,y)
#define xdr_uint32_t(x,y) xdr_u_int32_t(x,y)
#define xdr_uint64_t(x,y) xdr_u_int64_t(x,y)
#endif

void xdata::exdr::StreamBuffer::encodeInt8(const  int8_t & i) throw (xdata::exception::Exception)
{	
	u_int pos = xdr_getpos(&xdr_); // remember last valid position for rewind
#if __APPLE__
	while ( ! xdr_int8_t(&xdr_, (unsigned char*)&i) ) 
#else
	while ( ! xdr_int8_t(&xdr_, (int8_t*)&i) ) 
#endif
	{
		// buffer is too small, rewind to last known good positition before taking action
		if (xdr_setpos(&xdr_, pos) != 1)
		{
			std::stringstream msg;
			msg << "Cannot rewind to last good position when writing int8_t " << i;
			XCEPT_RAISE (xdata::exception::Exception, msg.str());
		}
		// buffer is too small
		this->overflow(); // function throws if error occurs
	}
}

void xdata::exdr::StreamBuffer::decodeInt8( int8_t & i) throw (xdata::exception::Exception)
{
#if __APPLE__
	while ( ! xdr_int8_t(&xdr_, (unsigned char*)&i) ) 
#else
	while ( ! xdr_int8_t(&xdr_, &i) ) 
#endif
	{
		// buffer is too small
		this->overflow();
	}
}

void xdata::exdr::StreamBuffer::encodeInt16(const  int16_t & i) throw (xdata::exception::Exception)
{	
	u_int pos = xdr_getpos(&xdr_); // remember last valid position for rewind
	while ( ! xdr_int16_t(&xdr_, (int16_t*)&i) ) 
	{
		// buffer is too small, rewind to last known good positition before taking action
		if (xdr_setpos(&xdr_, pos) != 1)
		{
			std::stringstream msg;
			msg << "Cannot rewind to last good position when writing int16_t " << i;
			XCEPT_RAISE (xdata::exception::Exception, msg.str());
		}
		// buffer is too small
		this->overflow(); // function throws if error occurs
	}
}

void xdata::exdr::StreamBuffer::decodeInt16( int16_t & i) throw (xdata::exception::Exception)
{
	while ( ! xdr_int16_t(&xdr_, &i) ) 
	{
		// buffer is too small
		this->overflow();
	}
}

void xdata::exdr::StreamBuffer::encodeInt32(const  int32_t & i) throw (xdata::exception::Exception)
{	
	u_int pos = xdr_getpos(&xdr_); // remember last valid position for rewind
	while ( ! xdr_int32_t(&xdr_, (int32_t*)&i) ) 
	{
		// buffer is too small, rewind to last known good positition before taking action
		if (xdr_setpos(&xdr_, pos) != 1)
		{
			std::stringstream msg;
			msg << "Cannot rewind to last good position when writing int32_t " << i;
			XCEPT_RAISE (xdata::exception::Exception, msg.str());
		}
		// buffer is too small
		this->overflow(); // function throws if error occurs
	}
}

void xdata::exdr::StreamBuffer::decodeInt32( int32_t & i) throw (xdata::exception::Exception)
{
	while ( ! xdr_int32_t(&xdr_, &i) ) 
	{
		// buffer is too small
		this->overflow();
	}
}

void xdata::exdr::StreamBuffer::encodeInt64(const  int64_t & i) throw (xdata::exception::Exception)
{	
	u_int pos = xdr_getpos(&xdr_); // remember last valid position for rewind
	while ( ! xdr_int64_t(&xdr_, (int64_t*)&i) ) 
	{
		// buffer is too small, rewind to last known good positition before taking action
		if (xdr_setpos(&xdr_, pos) != 1)
		{
			std::stringstream msg;
			msg << "Cannot rewind to last good position when writing int64_t " << i;
			XCEPT_RAISE (xdata::exception::Exception, msg.str());
		}
		// buffer is too small
		this->overflow(); // function throws if error occurs
	}
}

void xdata::exdr::StreamBuffer::decodeInt64( int64_t & i) throw (xdata::exception::Exception)
{
	while ( ! xdr_int64_t(&xdr_, &i) ) 
	{
		// buffer is too small
		this->overflow();
	}
}

//----------

void xdata::exdr::StreamBuffer::encodeUInt8(const  uint8_t & i) throw (xdata::exception::Exception)
{	
	u_int pos = xdr_getpos(&xdr_); // remember last valid position for rewind
	while ( ! xdr_uint8_t(&xdr_, (uint8_t*)&i) ) 
	{
		// buffer is too small, rewind to last known good positition before taking action
		if (xdr_setpos(&xdr_, pos) != 1)
		{
			std::stringstream msg;
			msg << "Cannot rewind to last good position when writing uint8_t " << i;
			XCEPT_RAISE (xdata::exception::Exception, msg.str());
		}
		// buffer is too small
		this->overflow(); // function throws if error occurs
	}
}

void xdata::exdr::StreamBuffer::decodeUInt8( uint8_t & i) throw (xdata::exception::Exception)
{
	while ( ! xdr_uint8_t(&xdr_, &i) ) 
	{
		// buffer is too small
		this->overflow();
	}
}

void xdata::exdr::StreamBuffer::encodeUInt16(const  uint16_t & i) throw (xdata::exception::Exception)
{	
	u_int pos = xdr_getpos(&xdr_); // remember last valid position for rewind
	while ( ! xdr_uint16_t(&xdr_, (uint16_t*)&i) ) 
	{
		// buffer is too small, rewind to last known good positition before taking action
		if (xdr_setpos(&xdr_, pos) != 1)
		{
			std::stringstream msg;
			msg << "Cannot rewind to last good position when writing uint16_t " << i;
			XCEPT_RAISE (xdata::exception::Exception, msg.str());
		}
		// buffer is too small
		this->overflow(); // function throws if error occurs
	}
}

void xdata::exdr::StreamBuffer::decodeUInt16( uint16_t & i) throw (xdata::exception::Exception)
{
	while ( ! xdr_uint16_t(&xdr_, &i) ) 
	{
		// buffer is too small
		this->overflow();
	}
}

void xdata::exdr::StreamBuffer::encodeUInt32(const  uint32_t & i) throw (xdata::exception::Exception)
{	
	u_int pos = xdr_getpos(&xdr_); // remember last valid position for rewind
	while ( ! xdr_uint32_t(&xdr_, (uint32_t*)&i) ) 
	{
		// buffer is too small, rewind to last known good positition before taking action
		if (xdr_setpos(&xdr_, pos) != 1)
		{
			std::stringstream msg;
			msg << "Cannot rewind to last good position when writing uint32_t " << i;
			XCEPT_RAISE (xdata::exception::Exception, msg.str());
		}
		// buffer is too small
		this->overflow(); // function throws if error occurs
	}
}

void xdata::exdr::StreamBuffer::decodeUInt32( uint32_t & i) throw (xdata::exception::Exception)
{
	while ( ! xdr_uint32_t(&xdr_, &i) ) 
	{
		// buffer is too small
		this->overflow();
	}
}

void xdata::exdr::StreamBuffer::encodeUInt64(const  uint64_t & i) throw (xdata::exception::Exception)
{	
	u_int pos = xdr_getpos(&xdr_); // remember last valid position for rewind
	while ( ! xdr_uint64_t(&xdr_, (uint64_t*)&i) ) 
	{
		// buffer is too small, rewind to last known good positition before taking action
		if (xdr_setpos(&xdr_, pos) != 1)
		{
			std::stringstream msg;
			msg << "Cannot rewind to last good position when writing uint64_t " << i;
			XCEPT_RAISE (xdata::exception::Exception, msg.str());
		}
		// buffer is too small
		this->overflow(); // function throws if error occurs
	}
}

void xdata::exdr::StreamBuffer::decodeUInt64( uint64_t & i) throw (xdata::exception::Exception)
{
	while ( ! xdr_uint64_t(&xdr_, &i) ) 
	{
		// buffer is too small
		this->overflow();
	}
}

//
void xdata::exdr::StreamBuffer::encode(const float & i) throw (xdata::exception::Exception)
{
	u_int pos = xdr_getpos(&xdr_); // remember last valid position for rewind

	while ( ! xdr_float(&xdr_, (float *)&i) ) 
	{
		// buffer is too small, rewind to last known good positition before taking action
		if (xdr_setpos(&xdr_, pos) != 1)
		{
			std::stringstream msg;
			msg << "Cannot rewind to last good position when writing float " << i;
			XCEPT_RAISE (xdata::exception::Exception, msg.str());
		}
		// buffer is too small
		this->overflow();
	}
}

void xdata::exdr::StreamBuffer::decode(float & i) throw (xdata::exception::Exception)
{
	while ( ! xdr_float(&xdr_, &i) ) 
	{
		// buffer is too small
		this->overflow();
	}
}

void xdata::exdr::StreamBuffer::encode(const double & i) throw (xdata::exception::Exception)
{
	u_int pos = xdr_getpos(&xdr_); // remember last valid position for rewind

	while ( ! xdr_double(&xdr_, (double*)&i) ) 
	{
		// buffer is too small, rewind to last known good positition before taking action
		if (xdr_setpos(&xdr_, pos) != 1)
		{
			std::stringstream msg;
			msg << "Cannot rewind to last good position when writing double " << i;
			XCEPT_RAISE (xdata::exception::Exception, msg.str());
		}
		// buffer is too small
		this->overflow();
	}
}

void xdata::exdr::StreamBuffer::decode(double & i) throw (xdata::exception::Exception)
{
	while ( ! xdr_double(&xdr_, &i) ) 
	{
		// buffer is too small
		this->overflow();
	}
}




void xdata::exdr::StreamBuffer::decode(std::string & s) throw (xdata::exception::Exception)
{
	char * tmp = 0;

	while ( ! xdr_string(&xdr_,&tmp, UINT_MAX) ) 
	{
		// buffer is too small - overflow may raise if limits reached
		try
		{
			this->overflow();
		}
		catch (xdata::exception::Exception& e)
		{
			if (tmp != 0) ::free(tmp);
			XCEPT_RETHROW (xdata::exception::Exception, "Failed to decode string", e);
		}
	}
	
	s  = tmp;
	::free(tmp);
}


void xdata::exdr::StreamBuffer::encode(const std::string & s) throw (xdata::exception::Exception)
{
	if (s.length() >= (UINT_MAX-1))
	{
		std::stringstream msg;
		msg << "Failed to encode string (too large, " << s.length() << " characters)";
		XCEPT_RAISE (xdata::exception::Exception, msg.str());
	}

	char * tmp = (char*)s.c_str();
	u_int pos = xdr_getpos(&xdr_); // remember last valid position for rewind
	while ( ! xdr_string(&xdr_,&tmp, UINT_MAX) ) 
	{
		// buffer is too small, rewind to last known good positition before taking action
		if (xdr_setpos(&xdr_, pos) != 1)
		{
			std::string msg = "Cannot rewind to last good position when writing string ";
			msg += s;
			XCEPT_RAISE (xdata::exception::Exception, msg);
		}
		this->overflow();
	}
	
}

void xdata::exdr::StreamBuffer::encode(const bool & i) throw (xdata::exception::Exception)
{
	u_int pos = xdr_getpos(&xdr_); // remember last valid position for rewind

	bool_t tmp = (i?1:0);
	while ( ! xdr_bool(&xdr_, &tmp) ) 
	{
		// buffer is too small, rewind to last known good positition before taking action
		if (xdr_setpos(&xdr_, pos) != 1)
		{
			std::stringstream msg;
			msg << "Cannot rewind to last good position when writing bool " << i;
			XCEPT_RAISE (xdata::exception::Exception, msg.str());
		}
		// buffer is too small
		this->overflow();
	}
	
}

void xdata::exdr::StreamBuffer::decode(bool & i) throw (xdata::exception::Exception)
{
	bool_t tmp;
	while ( ! xdr_bool(&xdr_, &tmp) ) 
	{
		// buffer is too small
		this->overflow();
	}
	i = (tmp == 1 ?true:false);
}

void xdata::exdr::StreamBuffer::decode(char* c, unsigned int len) throw (xdata::exception::Exception)
{
	while ( ! xdr_opaque(&xdr_, c, len) ) 
	{
		// buffer is too small
		this->overflow();
	}	
}


void xdata::exdr::StreamBuffer::encode(char* c, unsigned int len) throw (xdata::exception::Exception)
{
	u_int pos = xdr_getpos(&xdr_); // remember last valid position for rewind

	while ( ! xdr_opaque(&xdr_,c, len) ) 
	{
		// buffer is too small, rewind to last known good positition before taking action
		if (xdr_setpos(&xdr_, pos) != 1)
		{
			std::stringstream msg;
			msg << "Cannot rewind to last good position when writing char * " << c;
			XCEPT_RAISE (xdata::exception::Exception, msg.str());
		}
		// buffer is too small
		this->overflow();
	}
	
}

u_int xdata::exdr::StreamBuffer::tellp() 
{
	return xdr_getpos(&xdr_);
}
