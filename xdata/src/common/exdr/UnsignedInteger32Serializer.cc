// $Id: UnsignedInteger32Serializer.cc,v 1.4 2008/07/18 15:28:13 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xdata/exdr/UnsignedInteger32Serializer.h"
#include "xdata/exdr/Serializer.h"

xdata::exdr::UnsignedInteger32Serializer::~UnsignedInteger32Serializer()
{
}

std::string xdata::exdr::UnsignedInteger32Serializer::type() const
{
	return "unsigned int 32";
}


void xdata::exdr::UnsignedInteger32Serializer::exportAll(xdata::exdr::Serializer * serializer,  xdata::Serializable * serializable, xdata::exdr::OutputStreamBuffer * sbuf) throw (xdata::exception::Exception)
{
	// cast serializable to Integer
	xdata::UnsignedInteger32 *  i = dynamic_cast<xdata::UnsignedInteger32*>(serializable);
	if ( i == 0 )
        {
                XCEPT_RAISE(xdata::exception::Exception, "failed to cast serializable of xdata::UnsignedInteger32 object during export " );
        }

	serializer->encodeTag( xdata::exdr::Serializer::UnsignedInteger32, i->limits_, sbuf);
	sbuf->encodeUInt32(i->value_);
}

void xdata::exdr::UnsignedInteger32Serializer::import (xdata::exdr::Serializer * serializer,  xdata::Serializable * serializable, xdata::exdr::InputStreamBuffer * sbuf) throw (xdata::exception::Exception)
{
	xdata::UnsignedInteger32 * i = dynamic_cast<xdata::UnsignedInteger32*>(serializable);
	if ( i == 0 )
        {
                XCEPT_RAISE(xdata::exception::Exception, "failed to cast serializable of xdata::UnsignedInteger32 object during import" );
        }

	serializer->decodeTag(xdata::exdr::Serializer::UnsignedInteger32, i->limits_, sbuf);
	sbuf->decodeUInt32(i->value_);
}
