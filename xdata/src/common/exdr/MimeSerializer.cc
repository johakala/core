// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2017, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber, L. Orsini and D. Simelevicius                   *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include "xdata/exdr/MimeSerializer.h"
#include "xdata/exdr/Serializer.h"
#include "xdata/exception/Exception.h"
#include "xdata/UnsignedInteger64.h"

xdata::exdr::MimeSerializer::~MimeSerializer()
{
}

std::string xdata::exdr::MimeSerializer::type() const
{
	return "mime";
}

void xdata::exdr::MimeSerializer::exportAll
(
	xdata::exdr::Serializer * serializer,  
	xdata::Serializable * serializable, 
	xdata::exdr::OutputStreamBuffer * sbuf
) 
	throw (xdata::exception::Exception)
{
		
		serializer->encodeTag(xdata::exdr::Serializer::Mime, sbuf);
		
		xdata::Mime * m = dynamic_cast<xdata::Mime*>(serializable);
		
		std::ostringstream s;
		s << *(m->getEntity());
				
		std::stringstream::pos_type size = s.tellp();
		
		// Runtime check if greater than a uint_32
		if (static_cast<size_t>(size) >= UINT_MAX)
		{
			XCEPT_RAISE (xdata::exception::Exception, "Exceeded maximum mime size (2^32 bytes)");
		}
					
		if (sizeof(uint32_t) == sizeof(unsigned int))
		{
			sbuf->encodeUInt32(static_cast<uint32_t>(size)); // length, always 32-bit encoded			
			sbuf->encode( (char*) s.str().c_str(), static_cast<unsigned int>(size)); // opaque content
		}
		else
		{
			XCEPT_RAISE (xdata::exception::Exception, "Type size mismatch 'uint32_t', 'unsigned int'");
		}	
}


void xdata::exdr::MimeSerializer::import
(
	xdata::exdr::Serializer * serializer,
	xdata::Serializable * serializable,
	xdata::exdr::InputStreamBuffer * sbuf
)
	throw (xdata::exception::Exception)
{
	serializer->decodeTag(xdata::exdr::Serializer::Mime, sbuf);

	uint32_t size;
	sbuf->decodeUInt32(size);

	char* buffer = new char [size];

	sbuf->decode(buffer, size); // opaque content

	std::istringstream inputStream;
	inputStream.rdbuf()->pubsetbuf(buffer,size);

	xdata::Mime * m = dynamic_cast<xdata::Mime*>(serializable);
	m->setEntity( new mimetic::MimeEntity(inputStream) );
	delete[] buffer;
}
