// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2017, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xdata/exdr/VectorUnsignedInteger8Serializer.h"
#include "xdata/AbstractVector.h"
#include "xdata/exdr/Serializer.h"
#include "xdata/exception/Exception.h"
#include "xdata/UnsignedInteger8.h"

xdata::exdr::VectorUnsignedInteger8Serializer::~VectorUnsignedInteger8Serializer()
{
}

std::string xdata::exdr::VectorUnsignedInteger8Serializer::type() const
{
	return "vector unsigned int 8";
}

void xdata::exdr::VectorUnsignedInteger8Serializer::exportAll
(
	xdata::exdr::Serializer * serializer,  
	xdata::Serializable * serializable, 
	xdata::exdr::OutputStreamBuffer * sbuf
) 
	throw (xdata::exception::Exception)
{
	// redundant it is already defined in the tag for the table column
	//serializer->encodeTag(xdata::exdr::Serializer::VectorUnsignedInteger8, sbuf);

	xdata::AbstractVector * v = dynamic_cast<xdata::AbstractVector*>(serializable);

	// encode the vector size always as a 64-bit integer, also on 32-bit platforms
	//
	size_t size = v->elements();
	if (size >= 0xFFFFFFFFU)
	{
		XCEPT_RAISE (xdata::exception::Exception, "Exceeded maximum vector size (> 2^32 elements)");
	}
	
	sbuf->encodeUInt32(static_cast<uint32_t>(size));

	for ( size_t i = 0; i < size; i++)
	{
		xdata::UnsignedInteger8 *  e = dynamic_cast<xdata::UnsignedInteger8*>(v->elementAt(i));
		if ( e == 0 )
		{
                	XCEPT_RAISE(xdata::exception::Exception, "failed to cast serializable of xdata::UnsignedInteger8 object during export " );
		}
		sbuf->encodeUInt8(e->value_);

	}
}


void xdata::exdr::VectorUnsignedInteger8Serializer::import
(
	xdata::exdr::Serializer * serializer,  
	xdata::Serializable * serializable, 
	xdata::exdr::InputStreamBuffer * sbuf
) 
	throw (xdata::exception::Exception)
{
	// redundant it is already defined in the tag for the table column
	//serializer->decodeTag(xdata::exdr::Serializer::VectorUnsignedInteger8, sbuf);
	
	uint32_t size;
	sbuf->decodeUInt32(size);
	
	xdata::AbstractVector * v = dynamic_cast<xdata::AbstractVector*>(serializable);
	v->setSize(size);
	
	for (uint32_t i = 0; i < size; i++)
	{
		xdata::UnsignedInteger8 * e = dynamic_cast<xdata::UnsignedInteger8*>(v->elementAt(i));
		if ( e == 0 )
		{
                	XCEPT_RAISE(xdata::exception::Exception, "failed to cast serializable of xdata::UnsignedInteger8 object during import" );
		}

		e->limits_.reset();
		sbuf->decodeUInt8(e->value_);
	}
}
