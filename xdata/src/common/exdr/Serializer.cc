// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2017, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xdata/exdr/Serializer.h"  
#include "xdata/exdr/VectorSerializer.h"  
#include "xdata/exdr/IntegerSerializer.h"  
#include "xdata/exdr/FloatSerializer.h" 
#include "xdata/exdr/BooleanSerializer.h" 
#include "xdata/exdr/StringSerializer.h"  
#include "xdata/exdr/BagSerializer.h"  
#include "xdata/exdr/UnsignedShortSerializer.h"  
#include "xdata/exdr/UnsignedLongSerializer.h" 
#include "xdata/exdr/UnsignedIntegerSerializer.h" 
#include "xdata/exdr/UnsignedInteger64Serializer.h"
#include "xdata/exdr/UnsignedInteger32Serializer.h"    
#include "xdata/exdr/Integer64Serializer.h"     
#include "xdata/exdr/Integer32Serializer.h"    
#include "xdata/exdr/DoubleSerializer.h"  
#include "xdata/exdr/PropertiesSerializer.h"  
#include "xdata/exdr/TableSerializer.h"  
#include "xdata/exdr/MimeSerializer.h"  
#include "xdata/exdr/TimeValSerializer.h" 
#include "xdata/exdr/VectorIntegerSerializer.h"  
#include "xdata/exdr/VectorInteger8Serializer.h"
#include "xdata/exdr/VectorInteger16Serializer.h"
#include "xdata/exdr/VectorInteger32Serializer.h"  
#include "xdata/exdr/VectorInteger64Serializer.h"  
#include "xdata/exdr/VectorUnsignedIntegerSerializer.h"  
#include "xdata/exdr/VectorUnsignedInteger8Serializer.h"
#include "xdata/exdr/VectorUnsignedInteger16Serializer.h"
#include "xdata/exdr/VectorUnsignedInteger32Serializer.h"  
#include "xdata/exdr/VectorUnsignedInteger64Serializer.h"  
#include "xdata/exdr/VectorUnsignedShortSerializer.h"  
#include "xdata/exdr/VectorFloatSerializer.h"  
#include "xdata/exdr/VectorDoubleSerializer.h"  
#include "xdata/exdr/VectorStringSerializer.h"  
#include "xdata/exdr/VectorTimeValSerializer.h"  
#include "xdata/exdr/VectorBooleanSerializer.h"  
#include "xdata/exdr/Integer16Serializer.h"
#include "xdata/exdr/UnsignedInteger16Serializer.h"
#include "xdata/exdr/Integer8Serializer.h"
#include "xdata/exdr/UnsignedInteger8Serializer.h"



#include <sstream>

//
// Serializer * domSerializer = new xdata::exdr::XercesDOMSerializer();
// xdata::Vector<xdata::Integer> myVector; 
// Node * myNode = domSerializer.serialize(myVector,node);
//
xdata::exdr::Serializer::Serializer(bool packed): packed_(packed)
{
	// grabage collection is performed automatically at destruction time

	this->addObjectSerializer(new xdata::exdr::VectorSerializer());
	this->addObjectSerializer(new xdata::exdr::IntegerSerializer());
	this->addObjectSerializer(new xdata::exdr::UnsignedShortSerializer());
	this->addObjectSerializer(new xdata::exdr::UnsignedLongSerializer());
	this->addObjectSerializer(new xdata::exdr::FloatSerializer());
	this->addObjectSerializer(new xdata::exdr::DoubleSerializer());
	this->addObjectSerializer(new xdata::exdr::BooleanSerializer());
	this->addObjectSerializer(new xdata::exdr::StringSerializer());
	this->addObjectSerializer(new xdata::exdr::BagSerializer());
	this->addObjectSerializer(new xdata::exdr::PropertiesSerializer());
	this->addObjectSerializer(new xdata::exdr::TableSerializer());
	this->addObjectSerializer(new xdata::exdr::MimeSerializer());
	this->addObjectSerializer(new xdata::exdr::UnsignedIntegerSerializer());
	this->addObjectSerializer(new xdata::exdr::UnsignedInteger64Serializer());
	this->addObjectSerializer(new xdata::exdr::UnsignedInteger32Serializer());
	this->addObjectSerializer(new xdata::exdr::Integer64Serializer());
	this->addObjectSerializer(new xdata::exdr::Integer32Serializer());
	this->addObjectSerializer(new xdata::exdr::TimeValSerializer());
	this->addObjectSerializer(new xdata::exdr::VectorIntegerSerializer());
	this->addObjectSerializer(new xdata::exdr::VectorInteger8Serializer());
	this->addObjectSerializer(new xdata::exdr::VectorInteger16Serializer());
	this->addObjectSerializer(new xdata::exdr::VectorInteger32Serializer());
	this->addObjectSerializer(new xdata::exdr::VectorInteger64Serializer());
	this->addObjectSerializer(new xdata::exdr::VectorUnsignedIntegerSerializer());
	this->addObjectSerializer(new xdata::exdr::VectorUnsignedInteger8Serializer());
	this->addObjectSerializer(new xdata::exdr::VectorUnsignedInteger16Serializer());
	this->addObjectSerializer(new xdata::exdr::VectorUnsignedInteger32Serializer());
	this->addObjectSerializer(new xdata::exdr::VectorUnsignedInteger64Serializer());
	this->addObjectSerializer(new xdata::exdr::VectorUnsignedShortSerializer());
	this->addObjectSerializer(new xdata::exdr::VectorFloatSerializer());
	this->addObjectSerializer(new xdata::exdr::VectorDoubleSerializer());
	this->addObjectSerializer(new xdata::exdr::VectorBooleanSerializer());
	this->addObjectSerializer(new xdata::exdr::VectorStringSerializer());
	this->addObjectSerializer(new xdata::exdr::VectorTimeValSerializer());
	this->addObjectSerializer(new xdata::exdr::Integer16Serializer());
	this->addObjectSerializer(new xdata::exdr::UnsignedInteger16Serializer());
	this->addObjectSerializer(new xdata::exdr::Integer8Serializer());
	this->addObjectSerializer(new xdata::exdr::UnsignedInteger8Serializer());


}

	
	
void xdata::exdr::Serializer::exportAll
(
	xdata::Serializable * s,
	OutputStreamBuffer * sbuf
) 
	throw (xdata::exception::Exception)
{
 	std::string type = s->type();
        if ( type  == "vector" )
        {
                 type =  "vector " + dynamic_cast<xdata::AbstractVector*>(s)->getElementType();
        }

	xdata::exdr::ObjectSerializer * os = dynamic_cast<xdata::exdr::ObjectSerializer*>(this->getObjectSerializer(type));
	return os->exportAll(this, s, sbuf);
	
}
	
	
void xdata::exdr::Serializer::import
(
	xdata::Serializable * s,
	InputStreamBuffer* sbuf
) 
	throw (xdata::exception::Exception)
{
	std::string type = s->type();
        if ( type  == "vector" )
        {
                 type =  "vector " + dynamic_cast<xdata::AbstractVector*>(s)->getElementType();
        }

	xdata::exdr::ObjectSerializer * os = dynamic_cast<xdata::exdr::ObjectSerializer*>(this->getObjectSerializer(type));
	return os->import(this, s, sbuf);

}


void xdata::exdr::Serializer::encodeTag
(
	DataTypeTag tag, 
	xdata::exdr::OutputStreamBuffer * sbuf
) 
	throw (xdata::exception::Exception)
{
	if ( ! packed_ ) 
	{
		uint32_t tmp = tag;
		sbuf->encodeUInt32(tmp);
	}	
}

void xdata::exdr::Serializer::decodeTag
(
	DataTypeTag  tag,
	xdata::exdr::InputStreamBuffer * sbuf
) 
	throw (xdata::exception::Exception)
{
	if ( ! packed_ ) 
	{
		uint32_t tmp;

		sbuf->decodeUInt32(tmp);

		if ( tag != (DataTypeTag)tmp ) 
		{
			std::ostringstream msg;
			msg << "Expected tag (" << tag << "), found (" << tmp << ")";
			XCEPT_RAISE(xdata::exception::Exception, msg.str());
		}
	}	
}

void xdata::exdr::Serializer::encodeTag
(
	DataTypeTag tag, 
	std::bitset<xdata::NumberOfLimits> & limits,
	xdata::exdr::OutputStreamBuffer * sbuf
) 
	throw (xdata::exception::Exception)
{
	if ( ! packed_ ) 
	{
		uint32_t tmp = tag  | (limits.to_ulong() << 16 );
		sbuf->encodeUInt32(tmp);
	}	
}

void xdata::exdr::Serializer::decodeTag
(
	DataTypeTag  tag,
	std::bitset<xdata::NumberOfLimits> & limits,
	xdata::exdr::InputStreamBuffer * sbuf
	
) 
	throw (xdata::exception::Exception)
{
	if ( ! packed_ ) 
	{
		uint32_t tmp;

		sbuf->decodeUInt32(tmp);
		
		
		if ( tag != (DataTypeTag)(tmp & 0x0000ffff) ) 
		{
			std::ostringstream msg;
			msg << "Expected tag (" << tag << "), found (" << (tmp  & 0x0000ffff) << ")";
			XCEPT_RAISE(xdata::exception::Exception, msg.str());
		}
		
		std::bitset<xdata::NumberOfLimits>  l( (tmp & 0xffff0000) >> 16 );
		limits = l;
		
	}	
}
		


