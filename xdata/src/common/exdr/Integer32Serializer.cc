// $Id: Integer32Serializer.cc,v 1.4 2008/07/18 15:28:13 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xdata/exdr/Integer32Serializer.h"
#include "xdata/exdr/Serializer.h"

xdata::exdr::Integer32Serializer::~Integer32Serializer()
{
}

std::string xdata::exdr::Integer32Serializer::type() const
{
	return "int 32";
}


void xdata::exdr::Integer32Serializer::exportAll
(
	xdata::exdr::Serializer * serializer,
	xdata::Serializable * serializable,
	xdata::exdr::OutputStreamBuffer * sbuf
) 
	throw (xdata::exception::Exception)
{
	// cast serializable to Integer
	xdata::Integer32 *  i = dynamic_cast<xdata::Integer32*>(serializable);
	if ( i == 0 )
        {
                XCEPT_RAISE(xdata::exception::Exception, "failed to cast serializable of xdata::Integerer32 object during export " );
        }

	serializer->encodeTag( xdata::exdr::Serializer::Integer32, i->limits_, sbuf);
	sbuf->encodeInt32(i->value_);
}

void xdata::exdr::Integer32Serializer::import 
(
	xdata::exdr::Serializer * serializer,
	xdata::Serializable * serializable,
	xdata::exdr::InputStreamBuffer * sbuf
) 
	throw (xdata::exception::Exception)
{
	xdata::Integer32 * i = dynamic_cast<xdata::Integer32*>(serializable);
	if ( i == 0 )
        {
                XCEPT_RAISE(xdata::exception::Exception, "failed to cast serializable of xdata::Integer32 object during import" );
        }

	serializer->decodeTag(xdata::exdr::Serializer::Integer32, i->limits_, sbuf);
	sbuf->decodeInt32(i->value_);
}
