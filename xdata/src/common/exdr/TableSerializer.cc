// $Id: TableSerializer.cc,v 1.12 2008/07/18 15:28:13 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include <sstream>
#include "xdata/exdr/TableSerializer.h"
#include "xdata/exdr/Serializer.h"
#include "xdata/exception/Exception.h"
#include "xdata/UnsignedInteger8.h"
#include "xdata/UnsignedInteger16.h"
#include "xdata/Integer32.h"
#include "xdata/Integer8.h"
#include "xdata/Integer16.h"
#include "xdata/Integer32.h"

xdata::exdr::TableSerializer::~TableSerializer()
{
}

std::string xdata::exdr::TableSerializer::type() const
{
	return "table";
}

void xdata::exdr::TableSerializer::exportAll
(
	xdata::exdr::Serializer * serializer,  
	xdata::Serializable * serializable, 
	xdata::exdr::OutputStreamBuffer * sbuf
) 
	throw (xdata::exception::Exception)
{
		serializer->encodeTag(xdata::exdr::Serializer::Table, sbuf);
		
		xdata::AbstractTable * t = dynamic_cast<xdata::AbstractTable*>(serializable);
		xdata::Table * table = dynamic_cast<xdata::Table*>(serializable);
		
		std::vector<std::string> columns = t->getColumns();
		std::vector<std::string>::size_type columnsSize = columns.size();
		
		if (columnsSize >= 0xFFFFFFFFU)
		{
			XCEPT_RAISE (xdata::exception::Exception, "Exceeded maximum number of table columns (2^32)");
		}
		
		serializer->encodeTag(xdata::exdr::Serializer::UnsignedInteger32, sbuf);		
		sbuf->encodeUInt32(static_cast<uint32_t>(columnsSize));
				
		size_t rows = t->getRowCount();
		
		if (rows >= 0xFFFFFFFFU)
		{
			XCEPT_RAISE (xdata::exception::Exception, "Exceeded maximum number of table rows (2^32)");
		}
		
		serializer->encodeTag(xdata::exdr::Serializer::UnsignedInteger32, sbuf);
		sbuf->encodeUInt32(static_cast<uint32_t>(rows));
		
		// encode number of columns	
		std::vector<std::string>::iterator i;	
		for ( i = columns.begin(); i != columns.end(); ++i )
		{
			 std::string colType = t->getColumnType(*i);
			 //std::cout << "serializing:" << columns[i] << " of type: " << colType << std::endl;
			 serializer->encodeTag(xdata::exdr::Serializer::String, sbuf); 
			 sbuf->encode(*i); // column name
			 
			 serializer->encodeTag(xdata::exdr::Serializer::UnsignedInteger32, sbuf);

			 if ( colType == "unsigned long" )
			 {	
				sbuf->encodeUInt32(static_cast<uint32_t>(xdata::exdr::Serializer::UnsignedLong)); // column type
			 }
			 else if ( colType == "unsigned int")
			 {	
				sbuf->encodeUInt32(static_cast<uint32_t>(xdata::exdr::Serializer::UnsignedInteger)); // column type
			 }
			 else if ( colType == "unsigned int 8")
			 {	
				sbuf->encodeUInt32(static_cast<uint32_t>(xdata::exdr::Serializer::UnsignedInteger8)); // column type
			 }
			 else if ( colType == "unsigned int 16")
			 {	
				sbuf->encodeUInt32(static_cast<uint32_t>(xdata::exdr::Serializer::UnsignedInteger16)); // column type
			 }
			 else if ( colType == "unsigned int 32")
			 {	
				sbuf->encodeUInt32(static_cast<uint32_t>(xdata::exdr::Serializer::UnsignedInteger32)); // column type
			 }
			 else if ( colType == "unsigned int 64")
			 {	
				sbuf->encodeUInt32(static_cast<uint32_t>(xdata::exdr::Serializer::UnsignedInteger64)); // column type
			 }
			 else if ( colType == "unsigned short")
			 {	
				sbuf->encodeUInt32(static_cast<uint32_t>(xdata::exdr::Serializer::UnsignedShort)); // column type
			 }
			 else if ( colType == "string")
			 {	
				sbuf->encodeUInt32(static_cast<uint32_t>(xdata::exdr::Serializer::String)); // column type
			 }
			 else if ( colType == "double")
			 {	
				sbuf->encodeUInt32(static_cast<uint32_t>(xdata::exdr::Serializer::Double)); // column type
			 }
			 else if ( colType == "bool")
			 {
				sbuf->encodeUInt32(static_cast<uint32_t>(xdata::exdr::Serializer::Boolean)); // column type
			 }
			 else if ( colType == "float")
			 {
				sbuf->encodeUInt32(static_cast<uint32_t>(xdata::exdr::Serializer::Float)); // column type
			 }
			 else if ( colType == "int")
			 {
			 	sbuf->encodeUInt32(static_cast<uint32_t>(xdata::exdr::Serializer::Integer)); // column type
			 }
			 else if ( colType == "int 8")
			 {
			 	sbuf->encodeUInt32(static_cast<uint32_t>(xdata::exdr::Serializer::Integer8)); // column type
			 }
			 else if ( colType == "int 16")
			 {
			 	sbuf->encodeUInt32(static_cast<uint32_t>(xdata::exdr::Serializer::Integer16)); // column type
			 }
			 else if ( colType == "int 32")
			 {
			 	sbuf->encodeUInt32(static_cast<uint32_t>(xdata::exdr::Serializer::Integer32)); // column type
			 }
			 else if ( colType == "int 64")
			 {
			 	sbuf->encodeUInt32(static_cast<uint32_t>(xdata::exdr::Serializer::Integer64)); // column type
			 }
			 else if ( colType == "time")
			 {
			 	sbuf->encodeUInt32(static_cast<uint32_t>(xdata::exdr::Serializer::TimeVal)); // column type
			 }
			 else if ( colType == "mime")
			 {
			 	sbuf->encodeUInt32(static_cast<uint32_t>(xdata::exdr::Serializer::Mime)); // column type
			 }
			 else if ( colType == "table")
			 {
			 	sbuf->encodeUInt32(static_cast<uint32_t>(xdata::exdr::Serializer::Table)); // column type
			 }
			 else if ( colType == "vector int")
			 {
			 	sbuf->encodeUInt32(static_cast<uint32_t>(xdata::exdr::Serializer::VectorInteger)); // column type
			 }
			 else if ( colType == "vector int 8")
			 {
			 	sbuf->encodeUInt32(static_cast<uint32_t>(xdata::exdr::Serializer::VectorInteger8)); // column type
			 }
			 else if ( colType == "vector int 16")
			 {
			 	sbuf->encodeUInt32(static_cast<uint32_t>(xdata::exdr::Serializer::VectorInteger16)); // column type
			 }
			 else if ( colType == "vector int 32")
			 {
			 	sbuf->encodeUInt32(static_cast<uint32_t>(xdata::exdr::Serializer::VectorInteger32)); // column type
			 }
			 else if ( colType == "vector int 64")
			 {
			 	sbuf->encodeUInt32(static_cast<uint32_t>(xdata::exdr::Serializer::VectorInteger64)); // column type
			 }
			 else if ( colType == "vector unsigned int")
			 {
			 	sbuf->encodeUInt32(static_cast<uint32_t>(xdata::exdr::Serializer::VectorUnsignedInteger)); // column type
			 }
			 else if ( colType == "vector unsigned int 8")
			 {
			 	sbuf->encodeUInt32(static_cast<uint32_t>(xdata::exdr::Serializer::VectorUnsignedInteger8)); // column type
			 }
			 else if ( colType == "vector unsigned int 16")
			 {
			 	sbuf->encodeUInt32(static_cast<uint32_t>(xdata::exdr::Serializer::VectorUnsignedInteger16)); // column type
			 }
			 else if ( colType == "vector unsigned int 32")
			 {
			 	sbuf->encodeUInt32(static_cast<uint32_t>(xdata::exdr::Serializer::VectorUnsignedInteger32)); // column type
			 }
			 else if ( colType == "vector unsigned int 64")
			 {
			 	sbuf->encodeUInt32(static_cast<uint32_t>(xdata::exdr::Serializer::VectorUnsignedInteger64)); // column type
			 }
			  else if ( colType == "vector unsigned short")
			 {
			 	sbuf->encodeUInt32(static_cast<uint32_t>(xdata::exdr::Serializer::VectorUnsignedShort)); // column type
			 }
			  else if ( colType == "vector string")
			 {
			 	sbuf->encodeUInt32(static_cast<uint32_t>(xdata::exdr::Serializer::VectorString)); // column type
			 }
			  else if ( colType == "vector double")
			 {
			 	sbuf->encodeUInt32(static_cast<uint32_t>(xdata::exdr::Serializer::VectorDouble)); // column type
			 }
			  else if ( colType == "vector bool")
			 {
			 	sbuf->encodeUInt32(static_cast<uint32_t>(xdata::exdr::Serializer::VectorBoolean)); // column type
			 }
			  else if ( colType == "vector float")
			 {
			 	sbuf->encodeUInt32(static_cast<uint32_t>(xdata::exdr::Serializer::VectorFloat)); // column type
			 }
			  else if ( colType == "vector time")
			 {
			 	sbuf->encodeUInt32(static_cast<uint32_t>(xdata::exdr::Serializer::VectorTimeVal)); // column type
			 }
			 else
			 {
			 	std::stringstream msg;
				msg << "Serialization for type " << colType << " not supported";
			 	XCEPT_RAISE (xdata::exception::Exception, msg.str());
			 }
			 
			 // Cannot re-use Vector serializer, cause Table has different row count than vectors
			 // have elements: vectors may be larger to accomodate more data -> optimizations		 
			 // serializer->exportAll(dynamic_cast<xdata::Serializable*>(table->columnData_[columns[i]]), sbuf);
			 
			 xdata::Serializable* cv = dynamic_cast<xdata::Serializable*>(table->columnData_[(*i)]);
			 serializer->encodeTag(xdata::exdr::Serializer::Vector, sbuf);
		
			xdata::AbstractVector * v = dynamic_cast<xdata::AbstractVector*>(cv);

			// encode the vector size
			sbuf->encodeUInt32(static_cast<uint32_t>(rows));

			for (size_t i = 0; i < rows; i++)
			{
				serializer->exportAll(v->elementAt(i), sbuf);
			}			
		}
}


void xdata::exdr::TableSerializer::import 
(
	xdata::exdr::Serializer * serializer,  
	xdata::Serializable * serializable, 
	xdata::exdr::InputStreamBuffer * sbuf
) 
	throw (xdata::exception::Exception)
{
	serializer->decodeTag(xdata::exdr::Serializer::Table, sbuf);
	serializer->decodeTag(xdata::exdr::Serializer::UnsignedInteger32, sbuf);
	uint32_t columns;
	sbuf->decodeUInt32(columns);
	serializer->decodeTag(xdata::exdr::Serializer::UnsignedInteger32, sbuf);
	uint32_t rows;
	sbuf->decodeUInt32(rows);
	
	xdata::AbstractTable * v = dynamic_cast<xdata::AbstractTable*>(serializable);
	xdata::Table * table = dynamic_cast<xdata::Table*>(serializable);
	
	//clear table for overriding
	table->clear();
	for ( uint32_t i = 0; i < columns; i++ )
	{
	 		serializer->decodeTag(xdata::exdr::Serializer::String, sbuf); 
			std::string name;
			sbuf->decode(name); // column name
			serializer->decodeTag(xdata::exdr::Serializer::UnsignedInteger32, sbuf);
			uint32_t ctype;
			// std::cout << "decoding:" << name << " of type: " << ctype << std::endl;
			sbuf->decodeUInt32(ctype);
			 if ( ctype == xdata::exdr::Serializer::UnsignedLong )
			 {
				v->addColumn(name, "unsigned long");
			 }
			 else if ( ctype == xdata::exdr::Serializer::UnsignedInteger)
			 {
				v->addColumn(name, "unsigned int");
			 }
			 else if ( ctype == xdata::exdr::Serializer::UnsignedInteger8)
			 {
				v->addColumn(name, "unsigned int 8");
			 }
			 else if ( ctype == xdata::exdr::Serializer::UnsignedInteger16)
			 {
				v->addColumn(name, "unsigned int 16");
			 }
			 else if ( ctype == xdata::exdr::Serializer::UnsignedInteger32)
			 {
				v->addColumn(name, "unsigned int 32");
			 }
			 else if ( ctype == xdata::exdr::Serializer::UnsignedInteger64)
			 {
				v->addColumn(name, "unsigned int 64");
			 }
			 else if ( ctype == xdata::exdr::Serializer::UnsignedShort)
			 {
				v->addColumn(name, "unsigned short");
			 }
			 else if ( ctype == xdata::exdr::Serializer::String)
			 {
				v->addColumn(name, "string");				
			 }
			 else if ( ctype == xdata::exdr::Serializer::Double)
			 {	
				v->addColumn(name, "double");
			 }
			 else if ( ctype == xdata::exdr::Serializer::Boolean)
			 {
			 	v->addColumn(name, "bool");
			 }
			 else if ( ctype == xdata::exdr::Serializer::Float)
			 {
				v->addColumn(name, "float");
			 }
			 else if ( ctype == xdata::exdr::Serializer::Integer8)
			 {
			 	v->addColumn(name, "int 8");
			 }
			 else if ( ctype == xdata::exdr::Serializer::Integer16)
			 {
			 	v->addColumn(name, "int 16");
			 }
			 else if ( ctype == xdata::exdr::Serializer::Integer32)
			 {
			 	v->addColumn(name, "int 32");
			 }
			 else if ( ctype == xdata::exdr::Serializer::Integer64)
			 {
			 	v->addColumn(name, "int 64");
			 }
			 else if ( ctype == xdata::exdr::Serializer::Integer)
			 {
			 	v->addColumn(name, "int");
			 }
			 else if ( ctype == xdata::exdr::Serializer::TimeVal)
			 {
			 	v->addColumn(name, "time");
			 }
			 else if ( ctype == xdata::exdr::Serializer::Mime)
			 {
			 	v->addColumn(name, "mime");
			 }
			 else if ( ctype == xdata::exdr::Serializer::Table)
			 {
			 	v->addColumn(name, "table");
			 }
			 else if ( ctype == xdata::exdr::Serializer::VectorInteger)
			 {
			 	v->addColumn(name, "vector int");
			 }
			 else if ( ctype == xdata::exdr::Serializer::VectorInteger8)
			 {
			 	v->addColumn(name, "vector int 8");
			 }
			 else if ( ctype == xdata::exdr::Serializer::VectorInteger16)
			 {
			 	v->addColumn(name, "vector int 16");
			 }
			 else if ( ctype == xdata::exdr::Serializer::VectorInteger32)
			 {
			 	v->addColumn(name, "vector int 32");
			 }
			 else if ( ctype == xdata::exdr::Serializer::VectorInteger64)
			 {
			 	v->addColumn(name, "vector int 64");
			 }
			 else if ( ctype == xdata::exdr::Serializer::VectorUnsignedInteger)
			 {
			 	v->addColumn(name, "vector unsigned int");
			 }
			 else if ( ctype == xdata::exdr::Serializer::VectorUnsignedInteger8)
			 {
			 	v->addColumn(name, "vector unsigned int 8");
			 }
			 else if ( ctype == xdata::exdr::Serializer::VectorUnsignedInteger16)
			 {
			 	v->addColumn(name, "vector unsigned int 16");
			 }
			 else if ( ctype == xdata::exdr::Serializer::VectorUnsignedInteger32)
			 {
			 	v->addColumn(name, "vector unsigned int 32");
			 }
			 else if ( ctype == xdata::exdr::Serializer::VectorUnsignedInteger64)
			 {
			 	v->addColumn(name, "vector unsigned int 64");
			 }
			 else if ( ctype == xdata::exdr::Serializer::VectorUnsignedShort)
			 {
			 	v->addColumn(name, "vector unsigned short");
			 }
			 else if ( ctype == xdata::exdr::Serializer::VectorString)
			 {
			 	v->addColumn(name, "vector string");
			 }
			 else if ( ctype == xdata::exdr::Serializer::VectorDouble)
			 {
			 	v->addColumn(name, "vector double");
			 }
			 else if ( ctype == xdata::exdr::Serializer::VectorBoolean)
			 {
			 	v->addColumn(name, "vector bool");
			 }
			 else if ( ctype == xdata::exdr::Serializer::VectorFloat)
			 {
			 	v->addColumn(name, "vector float");
			 }
			 else if ( ctype == xdata::exdr::Serializer::VectorTimeVal)
			 {
			 	v->addColumn(name, "vector time");
			 }
			else
			{
				std::stringstream msg;
				msg << "Invalid type " << ctype << ", not supported";
				XCEPT_RAISE (xdata::exception::Exception, msg.str());
			}

  			serializer->decodeTag(xdata::exdr::Serializer::Vector, sbuf);

                        xdata::AbstractVector * v = dynamic_cast<xdata::AbstractVector*>(table->columnData_[name]);

                        // encode the vector size
 			uint32_t size;
                        sbuf->decodeUInt32(size);
			v->setSize(size);


                        for (size_t i = 0; i < size; i++)
                        {
                                serializer->import(v->elementAt(i), sbuf);
                        }
			
	}
	
	table->numberOfRows_ = rows;
	table->memorySize_ = rows;
}
