// $Id: FixedSizeInputStreamBuffer.cc,v 1.4 2008/07/18 15:28:13 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xdata/exdr/FixedSizeInputStreamBuffer.h"
	
xdata::exdr::FixedSizeInputStreamBuffer::FixedSizeInputStreamBuffer(char* buf, unsigned int size): InputStreamBuffer(buf, size)
{
} 

void xdata::exdr::FixedSizeInputStreamBuffer::overflow() throw (xdata::exception::Exception)
{
	// Fixed size does not allow to overflow the buffer
	XCEPT_RAISE(xdata::exception::Exception, "Fixed size buffer overflows");
}
