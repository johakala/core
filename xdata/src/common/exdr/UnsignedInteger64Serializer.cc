// $Id: UnsignedInteger64Serializer.cc,v 1.5 2008/07/18 15:28:13 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xdata/exdr/UnsignedInteger64Serializer.h"
#include "xdata/exdr/Serializer.h"

xdata::exdr::UnsignedInteger64Serializer::~UnsignedInteger64Serializer()
{
}

std::string xdata::exdr::UnsignedInteger64Serializer::type() const
{
	return "unsigned int 64";
}


void xdata::exdr::UnsignedInteger64Serializer::exportAll(xdata::exdr::Serializer * serializer,  xdata::Serializable * serializable, xdata::exdr::OutputStreamBuffer * sbuf) throw (xdata::exception::Exception)
{
	// cast serializable to Integer
	xdata::UnsignedInteger64 *  i = dynamic_cast<xdata::UnsignedInteger64*>(serializable);
	if ( i == 0 )
        {
                XCEPT_RAISE(xdata::exception::Exception, "failed to cast serializable of xdata::UnsignedInteger64  object during export" );
        }

	serializer->encodeTag( xdata::exdr::Serializer::UnsignedInteger64, i->limits_, sbuf);
	sbuf->encodeUInt64(i->value_);
}

void xdata::exdr::UnsignedInteger64Serializer::import (xdata::exdr::Serializer * serializer,  xdata::Serializable * serializable, xdata::exdr::InputStreamBuffer * sbuf) throw (xdata::exception::Exception)
{
	xdata::UnsignedInteger64 * i = dynamic_cast<xdata::UnsignedInteger64*>(serializable);
	if ( i == 0 )
        {
                XCEPT_RAISE(xdata::exception::Exception, "failed to cast serializable of xdata::UnsignedInteger64  object during import" );
        }

	serializer->decodeTag(xdata::exdr::Serializer::UnsignedInteger64, i->limits_, sbuf);
	sbuf->decodeUInt64(i->value_);
}
