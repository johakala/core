// $Id: InputStreamBuffer.cc,v 1.3 2008/07/18 15:28:13 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xdata/exdr/InputStreamBuffer.h"

xdata::exdr::InputStreamBuffer::InputStreamBuffer(char* buf, unsigned int  n)
{
	xdrmem_create(&xdr_, buf, n, XDR_DECODE);
} 

xdata::exdr::InputStreamBuffer::~InputStreamBuffer()
{
        xdr_destroy(&xdr_);
}

