// $Id: BagSerializer.cc,v 1.4 2008/07/18 15:28:13 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xdata/exdr/BagSerializer.h"
#include "xdata/exdr/Serializer.h"
#include "xdata/exception/Exception.h"

xdata::exdr::BagSerializer::~BagSerializer()
{
}

std::string xdata::exdr::BagSerializer::type() const
{
	return "bag";
}


void xdata::exdr::BagSerializer::exportAll(xdata::exdr::Serializer * serializer,  xdata::Serializable * serializable, xdata::exdr::OutputStreamBuffer * sbuf) throw (xdata::exception::Exception)
{
		serializer->encodeTag(xdata::exdr::Serializer::Bag, sbuf);
		
		std::map< std::string, xdata::Serializable*, std::less<std::string> > * b = dynamic_cast<std::map< std::string, xdata::Serializable*, std::less<std::string> >* >(serializable);
		std::map< std::string, xdata::Serializable*, std::less<std::string> >::iterator i;
		for ( i = b->begin(); i != b->end(); i++)
		{
			//xdata::Serializable* var = (*i).second;
			// encode field name
			sbuf->encode((*i).first);
			// recursively encode the field value
			dynamic_cast<xdata::exdr::Serializer*>(serializer)->exportAll((*i).second, sbuf);
		}

}

void xdata::exdr::BagSerializer::import (xdata::exdr::Serializer * serializer,  xdata::Serializable * serializable, xdata::exdr::InputStreamBuffer * sbuf) throw (xdata::exception::Exception)
{
	serializer->decodeTag(xdata::exdr::Serializer::Bag, sbuf);
	
	std::map< std::string, xdata::Serializable*, std::less<std::string> > * b = dynamic_cast<std::map< std::string, xdata::Serializable*, std::less<std::string> >* >(serializable);
	std::map< std::string, xdata::Serializable*, std::less<std::string> >::iterator i;
	for ( i = b->begin(); i != b->end(); i++)
	{	
		std::string fieldname;
		// decode field name
		sbuf->decode(fieldname);
		if (fieldname != (*i).first )
		{
			std::string msg = "Wrong field found in bag. Expected (";
			msg += (*i).first;
			msg += ") found (";
			msg += fieldname;	
			msg += ")";
			XCEPT_RAISE(xdata::exception::Exception, msg);
		
		}
		// recursively decode the field value
		dynamic_cast<xdata::exdr::Serializer*>(serializer)->import((*i).second, sbuf);
	}		
	

}
