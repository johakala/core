// $Id: ItemGroupEvent.cc,v 1.2 2008/07/18 15:28:12 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xdata/ItemGroupEvent.h"

xdata::ItemGroupEvent::ItemGroupEvent(
	const std::string& type, 
	xdata::InfoSpace* is, 
	void* originator):
	xdata::Event(type, originator)
{
	infoSpace_ = is;
}


void xdata::ItemGroupEvent::addItem(const std::string& name, xdata::Serializable* item)
{
	items_[name] = item;
}


std::map<std::string, xdata::Serializable*>& xdata::ItemGroupEvent::getItems()
{
	return items_;
}

xdata::InfoSpace* xdata::ItemGroupEvent::infoSpace()
{
	return infoSpace_;
}


xdata::ItemGroupChangedEvent::ItemGroupChangedEvent(xdata::InfoSpace* is, void* originator):
	xdata::ItemGroupEvent("urn:xdata-event:ItemGroupChangedEvent", is, originator)
{
}

xdata::ItemGroupRetrieveEvent::ItemGroupRetrieveEvent(xdata::InfoSpace* is, void* originator):
	xdata::ItemGroupEvent("urn:xdata-event:ItemGroupRetrieveEvent", is, originator)
{
}
