// $Id: TableIterator.cc,v 1.2 2008/07/18 15:28:12 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xdata/TableIterator.h"
#include "xdata/Table.h"


xdata::TableIterator::TableIterator() : m_Item(*this),  m_pCont(0), m_Pos(0),  m_End(true)
{
}

xdata::TableIterator::TableIterator(xdata::Table* pCont): m_Item(*this), m_pCont(pCont)
{ 

	if ( pCont->getRowCount() == 0 ) 
	{
		m_End = true;
		m_Pos = 0;
	}
	else
	{
		m_End = false;
		m_Pos = 0;

	}	

}

xdata::TableIterator::TableIterator(const TableIterator & i):  m_Item(*this)
{ 
	this->operator=(i);
}


bool  xdata::TableIterator::operator==(const xdata::TableIterator& rhs) const
{ 
	return (m_pCont == rhs.m_pCont && m_Pos == rhs.m_Pos && m_End == rhs.m_End ); 
}

bool  xdata::TableIterator::operator!=(const xdata::TableIterator& rhs) const
{ 
	return !operator==(rhs); 
}

xdata::TableIterator & xdata::TableIterator::operator++()    
{ 
	if ( m_End ) return *this;

	if ((m_Pos + 1 ) == m_pCont->getRowCount() )
	{
		m_End = true;
		m_Pos = 0;
	}
	else
	{
		m_Pos++;
	}
	return *this; 
}

xdata::TableIterator  xdata::TableIterator::operator ++ (int) 
{ 
 	xdata::TableIterator ret(*this);
	++*this;  
	return ret; 
}


xdata::TableIterator & xdata::TableIterator::operator--()    
{ 

	if ( m_End )
	{
		m_End = false;
		m_Pos = m_pCont->getRowCount() -1;
	}
	else 
	{
		if ( m_Pos == 0 ) return *this;

		m_Pos--;
	}
	return *this; 
}

xdata::TableIterator  xdata::TableIterator::operator -- (int) 
{ 
 	xdata::TableIterator ret(*this);
	--*this;  
	return ret; 
}


xdata::Table::Row &  xdata::TableIterator::operator *  ()
{ 
	return this->m_Item; 
}

xdata::Table::Row *  xdata::TableIterator::operator -> ()   
{ 
	return &**this; 
}

xdata::TableIterator & xdata::TableIterator::operator=(const xdata::TableIterator & rhs)
{
	if ( this != &rhs )
	{
		m_End = rhs.m_End;
		m_Pos = rhs.m_Pos;
		m_pCont =  rhs.m_pCont;
		// The field mItem should not be touched , because it point to itself
	}
	return *this;

}
	
 
