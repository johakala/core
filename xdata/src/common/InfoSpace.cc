// $Id: InfoSpace.cc,v 1.21 2008/07/18 15:28:12 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include <cxxabi.h>
#include <algorithm>
#include <sstream>
#include "toolbox/regex.h"

#include "xdata/InfoSpace.h"
#include "xdata/ItemEvent.h"
#include "xdata/ItemGroupEvent.h"
#include "xdata/InfoSpaceFactory.h"



xdata::InfoSpace::InfoSpace(const std::string & name):EventDispatcher(this), lock_(toolbox::BSem::FULL, true)
{
	// We actually do not see why we need the recursion of the infospace itself in itself
	// but it looks interesting, for the time being we comment it out
	//this->fireItemAvailable(name,this,this);
	
	name_ = name;
}

xdata::InfoSpace::~InfoSpace()
{
}

std::string xdata::InfoSpace::toString() const throw (xdata::exception::Exception)
{
	XCEPT_RAISE (xdata::exception::Exception, "InfoSpace toString not implemented");
}

void xdata::InfoSpace::fromString(const std::string& value) throw (xdata::exception::Exception)
{
	XCEPT_RAISE (xdata::exception::Exception, "InfoSpace fromString not implemented");
}


// Comparison operators
int xdata::InfoSpace::equals(const xdata::Serializable & s) const
{
	XCEPT_RAISE( xdata::exception::Exception, "Comparison not implemented" );
}





// static functions
std::map<std::string, xdata::Serializable *, std::less<std::string> > xdata::InfoSpace::match(const std::string & expr) throw (xdata::exception::Exception)
{
	this->lock();
	
	try 
	{
		std::map<std::string, xdata::Serializable *, std::less<std::string> > serializables;
		for ( std::map<std::string, xdata::Serializable *, std::less<std::string> >::iterator i = this->begin(); i != this->end(); i++ )
		{

			if ( toolbox::regx_match((*i).first, expr) )
			{
				serializables[(*i).first] = (*i).second;
			} 

		}
		this->unlock();
		return serializables;
	}
	catch(std::bad_alloc & e )
	{
		this->unlock();
		XCEPT_RAISE( xdata::exception::Exception, e.what() );
	}
	catch (std::invalid_argument & e )
	{
		this->unlock();
		XCEPT_RAISE( xdata::exception::Exception, e.what() );
	}
	
	
}


std::string xdata::InfoSpace::name() const
{
	return name_;
}

std::string xdata::InfoSpace::type() const
{
	return "infospace";
}

void xdata::InfoSpace::setValue(const xdata::Serializable & s) throw (xdata::exception::Exception)
{
        XCEPT_RAISE( xdata::exception::Exception, "setValue() for class InfoSpace is not supported");
}


// InfoSpace methods  


void xdata::InfoSpace::fireItemAvailable(const std::string & name, xdata::Serializable * serializable, void * originator)  throw (xdata::exception::Exception)
{
	this->lock();
	std::map<std::string, EventDispatcher, std::less<std::string> >::iterator i = dataItems_.find(name);
	
	// checck if already existing. Throw exception in this case
	if ( i == dataItems_.end() )
	{		
		dataItems_[name] =  EventDispatcher(serializable);
		// this is a bag.
		(*this)[name] = serializable;
		
		// notify all listeners about new item in space
		ItemAvailableEvent e(name,serializable,this,originator);
		std::list<xdata::ActionListener*>::iterator j;
		for ( j = itemAvailableListenerList_.begin(); j != itemAvailableListenerList_.end(); j++)
		{
			try
			{
				(*j)->actionPerformed(e);
			}
			catch(std::exception & e)
			{
				this->unlock();
				std::stringstream msg;
				msg << "unexpected std::exception from actionPerformed(): " << e.what();
				XCEPT_RAISE(xdata::exception::Exception, msg.str());
			}
			catch(...)
			{
				this->unlock();
				std::type_info * et = abi::__cxa_current_exception_type();
				std::stringstream msg;
				msg << "unexpected exception from actionPerformed() of class: " << et->name();
				XCEPT_RAISE(xdata::exception::Exception, msg.str());
			}
		}
	} 
	else 
	{
		std::string msg = "Item already exists: ";
		msg += name;
		this->unlock();
		XCEPT_RAISE( xdata::exception::Exception, msg );
	}
	this->unlock();
}


void xdata::InfoSpace::fireItemRevoked(const std::string & name, void * originator) throw (xdata::exception::Exception)
{
	this->lock();
	std::map<std::string, EventDispatcher, std::less<std::string> >::iterator i = dataItems_.find(name);
	if ( i != dataItems_.end() )
	{
		// notify all listeners about new item in space
		std::list<xdata::ActionListener*>::iterator j;
		
		ItemRevokedEvent e(name, ((*i).second).getItem(), this, originator);
		for ( j = itemRevokedListenerList_.begin(); j != itemRevokedListenerList_.end(); j++)
		{
			try
			{
				(*j)->actionPerformed(e);
			}
			catch(std::exception & e)
			{
				this->unlock();
				std::stringstream msg;
				msg << "unexpected std::exception from actionPerformed(): " << e.what();
				XCEPT_RAISE(xdata::exception::Exception, msg.str());
			}
			catch(...)
			{
				this->unlock();
				std::type_info * et = abi::__cxa_current_exception_type();
				std::stringstream msg;
				msg << "unexpected exception from actionPerformed() of class: " << et->name();
				XCEPT_RAISE(xdata::exception::Exception, msg.str());
			}
		}

		dataItems_.erase(name);
		// remove from the bag
		this->erase(name);
	} 
	else 
	{
		std::string msg = "Item does not exist: ";
		msg += name;
		this->unlock();
		XCEPT_RAISE( xdata::exception::Exception, msg );
	}
	this->unlock();
}

void xdata::InfoSpace::addItemAvailableListener( xdata::ActionListener * l ) 
{
	this->lock();
	itemAvailableListenerList_.push_back(l);
	this->unlock();
}

void xdata::InfoSpace::addItemRevokedListener( xdata::ActionListener * l )
{
	this->lock();
	itemRevokedListenerList_.push_back(l);
	this->unlock();
}

void xdata::InfoSpace::removeItemAvailableListener( xdata::ActionListener * l ) throw (xdata::exception::Exception)
{
	this->lock();
	std::list<xdata::ActionListener*>::iterator i;
	i = std::find( itemAvailableListenerList_.begin(), itemAvailableListenerList_.end(), l);
	if (i != itemAvailableListenerList_.end())
	{
		itemAvailableListenerList_.erase(i);
	} 
	else
	{
		this->unlock();
		XCEPT_RAISE( xdata::exception::Exception, "Could not remove available listener. Object not found." );
	}
	this->unlock();
}

void xdata::InfoSpace::removeItemRevokedListener(  xdata::ActionListener * l ) throw (xdata::exception::Exception)
{
	this->lock();
	std::list<xdata::ActionListener*>::iterator i;
	i = std::find( itemRevokedListenerList_.begin(), itemRevokedListenerList_.end(), l);
	if (i != itemRevokedListenerList_.end())
	{
		itemRevokedListenerList_.erase(i);
	} 
	else
	{
		this->unlock();
		XCEPT_RAISE( xdata::exception::Exception, "Could not remove revoked listener. Object not found." );
	}
	this->unlock();
}


xdata::Serializable * xdata::InfoSpace::find(const std::string & name) throw (xdata::exception::Exception)
{
	this->lock();
	std::map<std::string, EventDispatcher , std::less<std::string> >::iterator i = dataItems_.find(name);
	if ( i != dataItems_.end() )
	{
		xdata::Serializable* item = ((*i).second).getItem();
		this->unlock();
		return item;
	}
	else
	{
		std::string msg = "Item not found: ";
		msg += name;
		this->unlock();
		XCEPT_RAISE( xdata::exception::Exception, msg );
	}
}

bool xdata::InfoSpace::hasItem(const std::string & name)
{
	this->lock();
	std::map<std::string, EventDispatcher , std::less<std::string> >::iterator i = dataItems_.find(name);
	if ( i != dataItems_.end() )
	{
		this->unlock();
		return true;
	}
	else
	{
		this->unlock();
		return false;
	}
}




// Items Methods
//
void xdata::InfoSpace::fireItemValueChanged(const std::string & name, void * originator ) throw (xdata::exception::Exception)
{
	this->lock();
	std::map<std::string, EventDispatcher, std::less<std::string> >::iterator i = dataItems_.find(name);
	if ( i != dataItems_.end() )
	{
		Serializable * s = ((*i).second).getItem();
		ItemChangedEvent e(name, s, this, originator);
		((*i).second).fireItemChanged(e);
	} 
	else
	{
		std::string msg = "Could not find item ";
		msg += name;
		this->unlock();
		XCEPT_RAISE( xdata::exception::Exception, msg);
	}
	this->unlock();
}

void xdata::InfoSpace::fireItemValueRetrieve(const std::string & name, void * originator) throw (xdata::exception::Exception)
{
	this->lock();
	std::map<std::string, EventDispatcher, std::less<std::string> >::iterator i = dataItems_.find(name);
	if ( i != dataItems_.end() )
	{
		Serializable * s = ((*i).second).getItem();
		ItemRetrieveEvent e(name, s, this, originator);
		((*i).second).fireItemRetrieve(e);
	} 
	else
	{
		std::string msg = "Could not find item ";
		msg += name;
		this->unlock();
		XCEPT_RAISE( xdata::exception::Exception, msg);
	}
	this->unlock();
}

// --
void xdata::InfoSpace::fireItemGroupChanged(std::list<std::string> & names, void * originator ) throw (xdata::exception::Exception)
{
	this->lock();
	try
	{
		xdata::ItemGroupChangedEvent e(this, originator);

		std::list<std::string>::iterator k;
		for (k = names.begin(); k != names.end(); ++k)
		{
			e.addItem( (*k), this->find((*k)) ); // find may raise
		}

		this->fireEvent(e);
	}
	catch (xdata::exception::Exception& e)
	{
		this->unlock();
		XCEPT_RETHROW (xdata::exception::Exception, "Failed to fire group changed event", e);
	}
	this->unlock();
}

void xdata::InfoSpace::fireItemGroupRetrieve(std::list<std::string> & names, void * originator) throw (xdata::exception::Exception)
{
	this->lock();
	try
	{
		xdata::ItemGroupRetrieveEvent e(this, originator);

		std::list<std::string>::iterator k;
		for (k = names.begin(); k != names.end(); ++k)
		{
			e.addItem( (*k), this->find((*k)) ); // find may raise
		}

		this->fireEvent(e);
	}
	catch (xdata::exception::Exception& e)
	{
		this->unlock();
		XCEPT_RETHROW (xdata::exception::Exception, "Failed to fire group retrieve event", e);
	}
	this->unlock();
}
// --

void xdata::InfoSpace::addItemChangedListener( const std::string & name, xdata::ActionListener * l ) throw (xdata::exception::Exception)
{	
	this->lock();
	std::map<std::string, EventDispatcher, std::less<std::string> >::iterator i = dataItems_.find(name);
	if ( i != dataItems_.end() )
	{
		((*i).second).addItemChangedListener(l);
	}
	else 
	{
		std::string msg = "Cannot add item change listener. Could not find item ";
		msg += name;
		this->unlock();
		XCEPT_RAISE(xdata::exception::Exception,msg);
	}
	this->unlock();
}

void xdata::InfoSpace::addItemRetrieveListener( const std::string & name, xdata::ActionListener * l ) throw (xdata::exception::Exception)
{
	this->lock();
	std::map<std::string, EventDispatcher, std::less<std::string> >::iterator i = dataItems_.find(name);
	if ( i != dataItems_.end() )
	{
		((*i).second).addItemRetrieveListener(l);
	}
	else 
	{
		std::string msg = "Cannot add item retrieve listener. Could not find item ";
		msg += name;
		this->unlock();
		XCEPT_RAISE(xdata::exception::Exception,msg);
	}
	this->unlock();
}


void xdata::InfoSpace::removeItemChangedListener( const std::string & name, xdata::ActionListener * l ) throw (xdata::exception::Exception)
{
	this->lock();
	std::map<std::string, EventDispatcher, std::less<std::string> >::iterator i = dataItems_.find(name);
	if ( i != dataItems_.end() )
	{
		// This function will also throw an xdata::exception::Exception if 'l' was not found
		((*i).second).removeItemChangedListener(l);
	} 
	else
	{
		std::string msg = "Cannot remove item changed listener. Could not find item ";
		msg += name;
		this->unlock();
		XCEPT_RAISE(xdata::exception::Exception,msg);
	}
	this->unlock();
}

void xdata::InfoSpace::removeItemRetrieveListener( const std::string & name, xdata::ActionListener * l ) throw (xdata::exception::Exception)
{
	this->lock();
	std::map<std::string, EventDispatcher, std::less<std::string> >::iterator i = dataItems_.find(name);
	if ( i != dataItems_.end() )
	{
		// This function will also throw an xdata::exception::Exception if 'l' was not found
		((*i).second).removeItemRetrieveListener(l);
	}
	else
	{
		std::string msg = "Cannot remove item retrieve listener. Could not find item ";
		msg += name;
		this->unlock();
		XCEPT_RAISE(xdata::exception::Exception,msg);
	}
	this->unlock();
}

void xdata::InfoSpace::lock()
{
	lock_.take();
}

void xdata::InfoSpace::unlock()
{
	lock_.give();
}

// internal, not locked
void xdata::InfoSpace::fireEvent(xdata::Event & event) throw (xdata::exception::Exception)
{
	std::map<std::string, std::list<xdata::ActionListener*>, std::less<std::string> >::iterator i = listeners_.find(event.type()); 
	
	if ( i != listeners_.end() )
	{
		std::list<xdata::ActionListener*>::iterator j;
		for ( j = ((*i).second).begin(); j != ((*i).second).end(); j++)
		{
			try
			{
				(*j)->actionPerformed(event);
			}
			catch(std::exception & e)
			{
				std::stringstream msg;
				msg << "unexpected std::exception from actionPerformed(): " << e.what();
				XCEPT_RAISE(xdata::exception::Exception, msg.str());
			}
			catch(...)
			{
				std::type_info * et = abi::__cxa_current_exception_type();
				std::stringstream msg;
				msg << "unexpected exception from actionPerformed() of class: " << et->name();
				XCEPT_RAISE(xdata::exception::Exception, msg.str());
			}
		}
	} 
}

// internal, not locked
void xdata::InfoSpace::addListener( xdata::ActionListener * l, const std::string & type )
{
	std::map<std::string, std::list<xdata::ActionListener*>, std::less<std::string> >::iterator i = listeners_.find(type); 
	
	if ( i != listeners_.end() )
	{
		((*i).second).push_back(l);
	}
	else
	{
		// create
		listeners_[type].push_back(l);
	}
	

}

// internal, not locked
void xdata::InfoSpace::removeListener(xdata::ActionListener * l, const std::string & type ) throw (xdata::exception::Exception)
{

	std::map<std::string, std::list<xdata::ActionListener*>, std::less<std::string> >::iterator i = listeners_.find(type); 
	if ( i != listeners_.end() )
	{
		std::list<xdata::ActionListener*>::iterator j;
		j = std::find( ((*i).second).begin(), ((*i).second).end(), l);
		if (j != ((*i).second).end())
		{
			((*i).second).erase(j);
		} else
		{
			XCEPT_RAISE( xdata::exception::Exception, "Could not remove listener. Object not found." );
		}
		
	}
	else
	{
		std::string msg = "Cannot remove  listener. Could not find type ";
		msg += type;
		XCEPT_RAISE(xdata::exception::Exception,msg);
	}

}

void xdata::InfoSpace::addGroupChangedListener( xdata::ActionListener * l ) throw (xdata::exception::Exception)
{	
	this->lock();
	try
	{
		this->addListener( l, "urn:xdata-event:ItemGroupChangedEvent" );
	}
	catch (xdata::exception::Exception& e)
	{
		this->unlock();
		XCEPT_RETHROW (xdata::exception::Exception, "Failed to add group changed listener", e);
	}
	this->unlock();
}

void xdata::InfoSpace::addGroupRetrieveListener( xdata::ActionListener * l ) throw (xdata::exception::Exception)
{
	this->lock();	
	try
	{
		this->addListener( l, "urn:xdata-event:ItemGroupRetrieveEvent" );
	}
	catch (xdata::exception::Exception& e)
	{
		this->unlock();
		XCEPT_RETHROW (xdata::exception::Exception, "Failed to add group retrieve listener", e);
	}
	this->unlock();
}

void xdata::InfoSpace::removeGroupChangedListener( xdata::ActionListener * l ) throw (xdata::exception::Exception)
{
	this->lock();	
	try
	{
		this->removeListener( l, "urn:xdata-event:ItemGroupChangedEvent" );
	}
	catch (xdata::exception::Exception& e)
	{
		this->unlock();
		XCEPT_RETHROW (xdata::exception::Exception, "Failed to remove group changed listener", e);
	}
	this->unlock();
}

void xdata::InfoSpace::removeGroupRetrieveListener( xdata::ActionListener * l ) throw (xdata::exception::Exception)
{	
	this->lock();
	try
	{
		this->removeListener( l, "urn:xdata-event:ItemGroupRetrieveEvent" );
	}
	catch (xdata::exception::Exception& e)
	{
		this->unlock();
		XCEPT_RETHROW (xdata::exception::Exception, "Failed to remove group retrieve listener", e);
	}
	this->unlock();
}
