// $Id: EventDispatcher.cc,v 1.6 2008/07/18 15:28:12 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/
#include <algorithm>
#include "xdata/EventDispatcher.h"


xdata::EventDispatcher::EventDispatcher(Serializable * s): serializable_(s)
{

}

xdata::EventDispatcher::EventDispatcher(): serializable_(0)
{

}

void xdata::EventDispatcher::addItemChangedListener(  xdata::ActionListener * l )
{
	itemChangedListenerList_.push_back(l);
}

void xdata::EventDispatcher::addItemRetrieveListener(  xdata::ActionListener * l )
{
	itemRetrieveListenerList_.push_back(l);
}

void xdata::EventDispatcher::removeItemChangedListener( xdata::ActionListener * l ) throw (xdata::exception::Exception)
{
	std::list<xdata::ActionListener*>::iterator i;
	i = find( itemChangedListenerList_.begin(), itemChangedListenerList_.end(), l);
	if (i != itemChangedListenerList_.end())
	{
		itemChangedListenerList_.erase(i);
	} else
	{
		XCEPT_RAISE( xdata::exception::Exception, "Could not remove item changed listener. Object not found." );
	}
}

void xdata::EventDispatcher::removeItemRetrieveListener( xdata::ActionListener * l ) throw (xdata::exception::Exception)
{
	std::list<xdata::ActionListener*>::iterator i;
	i = find( itemRetrieveListenerList_.begin(), itemRetrieveListenerList_.end(), l);
	if (i != itemRetrieveListenerList_.end())
	{
		itemRetrieveListenerList_.erase(i);
	} else
	{
		XCEPT_RAISE( xdata::exception::Exception, "Could not remove item retrieve listener. Object not found." );
	}
}

void xdata::EventDispatcher::fireItemChanged( xdata::Event & e )
{
	std::list<xdata::ActionListener*>::iterator i;
	for ( i = itemChangedListenerList_.begin(); i != itemChangedListenerList_.end(); i++)
	{
		(*i)->actionPerformed(e);
	}
}

void xdata::EventDispatcher::fireItemRetrieve( xdata::Event & e )
{
	std::list<xdata::ActionListener*>::iterator i;
	for ( i = itemRetrieveListenerList_.begin(); i != itemRetrieveListenerList_.end(); i++)
	{
		(*i)->actionPerformed(e);
	}
}

xdata::Serializable * xdata::EventDispatcher::getItem()
{
	return serializable_;
}

void xdata::EventDispatcher::setItem(xdata::Serializable * s)
{
	serializable_ = s;
}


