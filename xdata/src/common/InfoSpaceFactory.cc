// $Id: InfoSpaceFactory.cc,v 1.4 2008/07/18 15:28:12 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and S. Murray					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xdata/InfoSpaceFactory.h"
#include <sstream>

xdata::InfoSpaceFactory* xdata::InfoSpaceFactory::instance_ = 0;


xdata::InfoSpaceFactory* xdata::InfoSpaceFactory::getInstance()
{
	if ( instance_ == 0 )
	{
		instance_ = new xdata::InfoSpaceFactory();
	}
	return instance_;
}

		//! Destroy the manager and destroy all pools.
		//
void xdata::InfoSpaceFactory::destroyInstance()
{
	if ( instance_ != 0 )
	{
		delete instance_;
		instance_ = 0;
	}
	
}
		
xdata::InfoSpace* xdata::InfoSpaceFactory::create (const std::string& name) throw (xdata::exception::Exception)
{
	this->lock();
	if (this->hasItem(name))
	{
		std::stringstream msg;
		msg << "Failed to create infospace, '" << name << "' already existing";
		this->unlock();
		XCEPT_RAISE (xdata::exception::Exception, msg.str());
	}
	else
	{
		xdata::InfoSpace * is = new xdata::InfoSpace(name);
		this->fireItemAvailable(name, is, this);
		this->unlock();
		return is;
	}
}

xdata::InfoSpace* xdata::InfoSpaceFactory::get (const std::string& name) throw (xdata::exception::Exception)
{
	this->lock();
	if (!this->hasItem(name))
	{
		std::stringstream msg;
		msg << "Failed to get infospace, '" << name << "' not existing";
		this->unlock();
		XCEPT_RAISE (xdata::exception::Exception, msg.str());
	}
	else
	{
		xdata::InfoSpace * is = dynamic_cast<xdata::InfoSpace *>(this->find(name));
		this->unlock();
		return is;
	}
}


void xdata::InfoSpaceFactory::destroy (const std::string & name) throw (xdata::exception::Exception)
{
	this->lock();
	try
	{
		xdata::InfoSpace* item = dynamic_cast<xdata::InfoSpace*>(this->find(name));
		this->fireItemRevoked(name, this);
		delete item;
		this->unlock();
	}
	catch (xdata::exception::Exception& e)
	{
		std::stringstream msg;
		msg << "Failed to destroy infspace, '" << name << "' not existing";
		this->unlock();
		XCEPT_RETHROW (xdata::exception::Exception, msg.str(), e);
	}
}


xdata::InfoSpaceFactory::InfoSpaceFactory(): xdata::InfoSpace("urn:xdata-item:factory")
{

}

xdata::InfoSpaceFactory::~InfoSpaceFactory()
{

}

	
xdata::InfoSpaceFactory* xdata::getInfoSpaceFactory()
{
	return xdata::InfoSpaceFactory::getInstance();
}
