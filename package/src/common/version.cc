// $Id: version.cc,v 1.1 2007/03/09 15:38:17 lorsini Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "config/version.h"

GETPACKAGEINFO(config)

void config::checkPackageDependencies() throw (config::PackageInfo::VersionException)
{
	// CHECKDEPENDENCY(toolbox);   
}

std::set<std::string, std::less<std::string> > config::getPackageDependencies()
{
    std::set<std::string, std::less<std::string> > dependencies;

    // ADDDEPENDENCY(dependencies,toolbox); 
  
    return dependencies;
}	
	
