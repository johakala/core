// $Id: PackageInfo.cc,v 1.2 2007/03/22 11:12:30 lorsini Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "config/PackageInfo.h"

config::PackageInfo::PackageInfo(const  std::string & name, const  std::string & vset,
			 const std::string & description, const std::string & link, const std::string & authors, 
			  const std::string & summary, const std::string & date, const std::string & time)
{
	name_ = name;
	vset_ = vset;
	description_ = description;
	link_ = link;
	date_ = date;
	time_ = time;
	authors_ = authors;
	summary_ = summary;
}

std::string config::PackageInfo::getDescription()
{
	return description_;
}

std::string config::PackageInfo::getLink()
{
	return link_;
}

void config::PackageInfo::setDescription (const std::string & description)
{
	 description_ = description;
}

void config::PackageInfo::setLink (const std::string & link)
{
	 link_ = link;
}

std::string config::PackageInfo::getLatestVersion()
{
    std::set<std::string, std::less<std::string> > implemented = stringToVersionSet(vset_);

    std::set<std::string, std::less<std::string> >::iterator lasti;
    lasti = implemented.end();
    lasti--;
    return *lasti;
}

std::set<std::string, std::less<std::string> > config::PackageInfo::getSupportedVersions()
{
	return stringToVersionSet(vset_);

}


std::string config::PackageInfo::getName()
{
	return name_;
}	

std::string config::PackageInfo::getAuthors()
{
	return authors_;
}

std::string config::PackageInfo::getSummary()
{
	return summary_;
}

std::string config::PackageInfo::getDate()
{
	return date_;
}	




std::string config::PackageInfo::getTime()
{
	return time_;
}	


void config::PackageInfo::checkVersions(const std::string& vset) throw (ImplementationTooOldException,ImplementationNewerException)
{
    // make set intersection
    std::set<std::string, std::less<std::string> > expected = stringToVersionSet(vset);
    std::set<std::string, std::less<std::string> > implemented = stringToVersionSet(vset_);

    //cout << "Size of expected:" << expected.size() << endl;
    //cout << "Size of implemented:" << implemented.size() << endl;
    // check if too old or too recent
    std::set<std::string, std::less<std::string> >::iterator lasti;
    lasti = implemented.end();
    lasti--;

    std::set<std::string, std::less<std::string> >::iterator laste;
    laste = expected.end();
    laste--;


    //cout << "The most recent implementation version is:" << *lasti << endl;
    //cout << "The most recent expected version is:" << *laste << endl;
    if (*laste == *lasti ) 
    {
        return; // compatible
    }        
    else if (*laste > *lasti)
    {

        //definition version is newer than implementation version

        if (*(expected.begin()) <= *lasti) 
        {
            return; // compatible
        }    
        else
        { 
            std::string msg = "Found latest implementation for package ";
            msg += name_;
	    msg += " (";
	    msg += *lasti;
	    msg += ") older than oldest expected (";
	    msg += *(expected.begin());
            msg += ")";
            throw ImplementationTooOldException(msg); // Found implementation of package older than expected
        }
    }        
    else
    {

        //definition version is older than implementation version*/

        if ((*implemented.begin()) <= *laste) 
        {
            return; // compatible
        }    
        else
        {
            std::string msg = "Found oldest implementation for package ";
            msg += name_;
	    msg += " (";
	    msg += *(implemented.begin());
	    msg += ") newer than the most recent expected (";
	    msg += *laste;
            msg += ")";
            throw ImplementationNewerException(msg); // Found implementation of package newer that expected"
        }
    }
}


std::set<std::string, std::less<std::string> > config::PackageInfo::stringToVersionSet(const std::string & vset )
{           
	std::set<std::string, std::less<std::string> > versions;
        versions.clear();

	// add versions to set
	std::string::const_iterator curPos = vset.begin();
	std::string::const_iterator end = vset.end();
	while ( curPos < end )
	{
		std::string::const_iterator nextDelim = std::find(curPos,end,',');
		while (isspace(*curPos)) // ignore leading space
			++curPos;
		std::string source(curPos, nextDelim);
		std::istringstream is(source.c_str());
		std::string version;
		is >> version;
		curPos = ++nextDelim;

		//put version in set	
		versions.insert(version);
         }

        return versions;
}
  
