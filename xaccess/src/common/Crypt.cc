#include <unistd.h>

#include "xaccess/Crypt.h"

std::string xaccess::Crypt::crypt(const std::string  & key, const std::string & salt)
{

	std::string crypted = ::crypt(key.c_str(), salt.c_str());
	return crypted;

}
