// $Id: HTAccess.cc,v 1.6 2008/07/18 15:27:56 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xaccess/HTAccess.h"
#include "xoap/HTTPLoader.h"
#include <iostream>
#include <sstream>
#include <fstream>
#include "toolbox/string.h"
#include "toolbox/Runtime.h"
#include "xaccess/Crypt.h"

xaccess::HTAccess::HTAccess(const std::string & url) throw (xaccess::exception::Exception)
{
	std::string htin;
	xoap::HTTPLoader loader(false);
	try 
	{
		loader.get(url,htin);
				
		std::istringstream ishtaccess(htin);
		this->parse(ishtaccess);
	}
	catch (xoap::exception::Exception & e) 
	{
		XCEPT_RETHROW(xaccess::exception::Exception, toolbox::toString("failed to load file from: %s",url.c_str()), e);
	}
}

std::string xaccess::HTAccess::getPolicyName() 
{
	return "urn:xdaq-security:htaccess";
}


std::string xaccess::HTAccess::getName() 
{
	return name_;
}

std::string xaccess::HTAccess::getType() 
{
	return type_;
}

bool xaccess::HTAccess::checkAccess(const std::string & host, const std::string & ip) 
{
	std::string elm;
	bool alp = false;
	bool dep = false;

	// looping for allows
	for (std::vector<std::string>::size_type i=0; i < allowList_.size(); i++) 
	{
		elm = allowList_[i];
		if (elm == "all") 
		{
			alp = true;
			break;
		}
		else 
		{
			char c = elm.at(0);
			if (c >= '0' && c <= '9') 
			{
				// ip
				//if (ip.find(elm) == 0)  // check if the ip starts with elm
				if ( ip == elm )
				{
					alp = true;
					break;
				}
			}
			else 
			{
				// hostname
				//if (host.find(elm) == (host.size() - elm.size() )) // check if host ends with elm
				if ( host == elm )
				{
					alp = true;
					break;
				}
			}
		}
	}
	
	// looping for denies
	for (std::vector<std::string>::size_type i=0; i < denyList_.size(); i++) 
	{
		elm = denyList_[i];
		if (elm == "all") 
		{
			dep = true;
			break;
		}
		else 
		{
			char c = elm.at(0);
			if (c >= '0' && c <= '9') 
			{ // ip
				//if (ip.find(elm) == 0)  // check if starts with elm
				if ( ip == elm )
				{
					dep = true;
					break;
				}
			}
			else 
			{ // hostname
				//if (host.find (elm) == (host.size() - elm.size() )) // check if host ends with elm
				if ( host == elm )
				{
					dep = true;
					break;
				}
			}
		}
	}

	if (order != 0) 
	{
		if (order > 0) 
			return (alp && !dep);
		else 
			return (!dep || alp);
	}
	else 
	{
			return (alp && !dep);
	}
}

bool xaccess::HTAccess::checkAuth(const std::string & userpass) 
{
	std::string::size_type spos = userpass.find(":");
	std::string user = userpass.substr(0,spos);
	std::string pass = userpass.substr(spos+1);
	std::string code;

	if (requireName_ == "user") 
	{
		std::map<std::string, bool, std::less<std::string> >::iterator i = requireEntities_.begin();
		while (i != requireEntities_.end() ) 
		{
			if (user == (*i).first) 
			{
				if ((code = this->getUserCode(user)) != "") 
				{
					if (code == xaccess::Crypt::crypt(pass, code))
					{
						return true;
					}
				}
				break;
			}
			i++;
		}
	}
	else if (requireName_ == "group") 
	{
		std::string gps = this->getUserGroups(user);
		if (gps != "") 
		{
			std::map<std::string, bool, std::less<std::string> >::iterator i = requireEntities_.begin();
			while (i != requireEntities_.end() ) 
			{
			
				if (gps.find((*i).first) >= 0) 
				{
					if ((code = this->getUserCode(user)) != "") 
					{
						if (code ==  xaccess::Crypt::crypt(pass, code)) 
						{
							return true;
						}
					}
					break;
				}
				i++;
			}
		}
	}
	else if (requireName_ == "valid-user") 
	{
		if ((code = this->getUserCode(user)) != "") 
		{
			if (code == xaccess::Crypt::crypt(pass, code))
			{
				return true;
			} 
		}
	}
	return false;
}

bool xaccess::HTAccess::isAccessLimited() 
{
	if ( (allowList_.size() > 0) || (denyList_.size() > 0))
		return true;
	else
		return false;
}

bool xaccess::HTAccess::isAccessLimited(const std::string & method) 
{
	if ((allowList_.size() > 0) || (denyList_.size() > 0)) 
	{	
		// if method was listed, then access is limited for this method
		if ( methods_.find(method) != methods_.end() )
		{
			return true;
		}
		else
		{
			return false;
		}	
	}
	else
	{
		return false;
	}	
}
bool xaccess::HTAccess::isAuthLimited() 
{
	if (requireName_ != "")
		return true;
	else
		return false;
}

bool xaccess::HTAccess::isAuthLimited(const std::string & method) 
{
	if (requireName_ != "")
	{
		// if method was listed, then access is limited for this method
		if ( methods_.find(method) != methods_.end() )
		{
			return true;
		}
		else
		{
			return false;
		}	
	}
	else
	{
		return false;
	}	
}
std::string xaccess::HTAccess::getUserCode(const std::string & user)  throw (xaccess::exception::Exception)
{
	std::string code = "";
	std::ifstream iuserfile;
	try
	{
		iuserfile.open(userFile_.c_str());		
		std::string line;
		while (std::getline(iuserfile, line) )
		{
			std::string::size_type spos = line.find(":");
			if (user == (line.substr(0,spos)))
			{
				code = line.substr(spos+1);
				break;
			}
		}
        }
	catch(...)
	{
		XCEPT_RAISE(xaccess::exception::Exception, toolbox::toString("failed to get user code from file: %s",userFile_.c_str()));
	}
	iuserfile.close();
	return code;
}

std::string xaccess::HTAccess::getUserGroups(const std::string & user)   throw (xaccess::exception::Exception)
{
	
	std::string groups = "";
	std::ifstream gfin;
	try 
	{
		 gfin.open(groupFile_.c_str());
		std::string line;
		while (std::getline(gfin,line)) 
		{
			std::string::size_type spos = line.find(":");
			if (line.substr(spos+1).find(user) > 0) 
			{
				groups += line.substr(0,spos) + " ";
			}
		}
	}
	catch (...) 
	{
		XCEPT_RAISE(xaccess::exception::Exception, toolbox::toString("failed to get user groups from file: %s",userFile_.c_str()));
	}
	gfin.close();
	return groups;
}

void xaccess::HTAccess::parse(std::istringstream & is)
{
	std::string line;
	
	while (std::getline(is,line) )
	{
		if (line.find("#") != std::string::npos) 
		{
			 continue;
		}	 
		else if (line.find("AuthUserFile") == 0)  // starts with
		{
			
			userFile_ = toolbox::trim(line.substr(13));
			
			std::vector<std::string> expandedFile;
			try
			{
				expandedFile = toolbox::getRuntime()->expandPathName(userFile_);
			} 
			catch (toolbox::exception::Exception& tbe)
			{
				// ignore
			}
			userFile_ = expandedFile[0];
			
		}
		else if (line.find("AuthGroupFile") == 0)  // starts with
		{
			groupFile_ = toolbox::trim(line.substr(14));
			std::vector<std::string> expandedFile;
			try
			{
				expandedFile = toolbox::getRuntime()->expandPathName(userFile_);
			} 
			catch (toolbox::exception::Exception& tbe)
			{
				// ignore
			}
			groupFile_ = expandedFile[0];
		}
		else if (line.find("AuthName") == 0)  // starts with
		{
			name_ = toolbox::trim(line.substr(8));
		}
		else if (line.find("AuthType") == 0)  // starts with
		{
			type_ = toolbox::trim(line.substr(8));
		}
		else if (line.find("<Limit") == 0)  // starts with
		{
			std::string::size_type limit = line.size();
			std::string::size_type endp = line.find(">");
			//toolbox::StringTokenizer tkns;

			if (endp == std::string::npos) endp = limit;
			toolbox::StringTokenizer tkns(line.substr(6, endp-6), " ");
			
			while (tkns.hasMoreTokens()) 
			{
				std::string method = tkns.nextToken();
				//std::cout << "Limiting access for method:" << method <<std::endl;
				methods_[method] = true;
			}

			while (getline(is,line )) 
			{
				if (line.find("#") == 0 )// starts with
				{
					continue;
				}	
				else if (line.find("require") == 0) 
				{
					std::string::size_type pos1 = 7;
					limit = line.size();
					while ((pos1 < limit) && (line.at(pos1) <= ' ')) pos1++;
					std::string::size_type pos2 = pos1;
					while ((pos2 < limit) && (line.at(pos2) > ' ')) pos2++;
					requireName_ = line.substr(pos1, pos2 - pos1);
					pos1 = pos2+1;
					if (pos1 < limit) 
					{
						while ((pos1 < limit) && (line.at(pos1) <= ' ')) pos1++;

						toolbox::StringTokenizer tkns(line.substr(pos1), " ");
						while (tkns.hasMoreTokens()) 
						{
							requireEntities_[tkns.nextToken()] = true;
						}
					}

				}
				else if (line.find("order") == 0) 
				{
					if (line.find("allow,deny") != std::string::npos) 
					{
						order = 1;
					}
					else if (line.find("deny,allow") != std::string::npos ) 
					{
						order = -1;
					}
					else if (line.find("mutual-failure") != std::string::npos) 
					{
						order = 0;
					}
					else 
					{
					}
				}
				else if (line.find("allow from") == 0) 
				{
					std::string::size_type pos1 = 10;
					limit = line.size();
					while ((pos1 < limit) && (line.at(pos1) <= ' ')) pos1++;
					toolbox::StringTokenizer tkns(line.substr(pos1), " ");
					while (tkns.hasMoreTokens()) 
					{
						allowList_.push_back(tkns.nextToken());
					}
				}
				else if (line.find("deny from") == 0) 
				{
					std::string::size_type pos1 = 9;
					limit = line.size();
					while ((pos1 < limit) && (line.at(pos1) <= ' ')) pos1++;
					toolbox::StringTokenizer tkns(line.substr(pos1), " ");
					while (tkns.hasMoreTokens()) 
					{
						denyList_.push_back(tkns.nextToken());
					}
				}
				else if (line.find("</Limit>") == 0 ) 
				{
					break;
				}	
			}
		}
	}
}
 

