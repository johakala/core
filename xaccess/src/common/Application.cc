// $Id: $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 			 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber, L. Orsini										 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                        	 *
 * For the list of contributors see CREDITS.   			        		 *
 *************************************************************************/

#include "xaccess/Application.h"
#include "xcept/tools.h"
#include "toolbox/Runtime.h"
#include "toolbox/net/URL.h"
#include "xaccess/HTAccess.h"
#include "pt/SecurityPolicyFactory.h"
#include "xgi/framework/Method.h"

XDAQ_INSTANTIATOR_IMPL(xaccess::Application)

xaccess::Application::Application(xdaq::ApplicationStub * s) throw (xdaq::exception::Exception): xdaq::Application(s), xgi::framework::UIManager(this)
{	
	s->getDescriptor()->setAttribute("icon", "/xaccess/images/xaccess-icon.png");

	xgi::framework::deferredbind(this, this, &xaccess::Application::Default, "Default");
	
	getApplicationInfoSpace()->fireItemAvailable("htaccess",&htaccess_);
	getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");
	
	//std::cout << "Policy Name --->" << policy->getPolicyName() << std::endl;
	//std::cout << "Name --->" << policy->getName() << std::endl;
	//std::cout << "Type --->" << policy->getType() << std::endl;
	//std::cout << "is Access Limited--->" << policy->isAccessLimited() << std::endl;
	//std::cout << "is Auth Limited--->" << policy->isAuthLimited() << std::endl;
}

void xaccess::Application::actionPerformed(xdata::Event& e)
{
	if (e.type() == "urn:xdaq-event:setDefaultValues")
	{
		std::vector<std::string> expandedFile;
                try
                {
                        expandedFile = toolbox::getRuntime()->expandPathName(htaccess_);
                } 
                catch (toolbox::exception::Exception& tbe)
                {
                        std::string msg = "Failed to resolve htaccess url ";
                        msg += htaccess_;
			msg += ", ";
			msg += xcept::stdformat_exception_history(tbe);
			LOG4CPLUS_ERROR (getApplicationLogger(), msg);
			return;
                }

                if (expandedFile.size() != 1)
                {
                        std::string msg = "Failed to expand htaccess url ";
                        msg += htaccess_;
			msg += ", URL is ambiguous";
			LOG4CPLUS_ERROR (getApplicationLogger(), msg);
			return;
                }
	
		// create policy for HTAccess
		xaccess::HTAccess * policy = 0;
		try
		{
                        toolbox::net::URL url( expandedFile[0] ); 
			policy = new xaccess::HTAccess(url.toString());
		}
		catch(xaccess::exception::Exception& xe)
		{
			std::string msg = "Failed to create access policy, ";
			msg += xcept::stdformat_exception_history(xe);
			LOG4CPLUS_ERROR (getApplicationLogger(), msg);
			return;
		}
		catch (toolbox::net::exception::MalformedURL& mfu)
		{
			std::string msg = "Failed to create access policy, ";
			msg += xcept::stdformat_exception_history(mfu);
			LOG4CPLUS_ERROR (getApplicationLogger(), msg);
			return;
		}

		try
		{
			pt::getSecurityPolicyFactory()->addSecurityPolicyImplementation(policy->getPolicyName(), policy);
		}
		catch(pt::exception::Exception & e )
		{
			if (policy != 0)
				delete policy;
			
			std::string msg = "Failed to add access policy, ";
			msg += xcept::stdformat_exception_history(e);
			LOG4CPLUS_ERROR (getApplicationLogger(), msg);
			return;
		}
	}	
}

void xaccess::Application::Default(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception)
{
	*out << cgicc::table().set("class","xdaq-table").set("style","width: 100%;");

	*out << cgicc::thead();
	*out << cgicc::tr();
	*out << cgicc::th("Policy");
	*out << cgicc::tr() << std::endl;
	*out << cgicc::thead();

	*out << cgicc::tbody();
	std::vector<std::string> policies = pt::getSecurityPolicyFactory()->getSecurityPolicyNames();
	for ( std::vector<std::string>::size_type  i = 0; i < policies.size(); i++ )
	{
		*out << cgicc::tr();
		*out << cgicc::td(policies[i]) << std::endl;
		*out << cgicc::tr() << std::endl;
	}
	*out << cgicc::tbody();	

	*out << cgicc::table();
}

