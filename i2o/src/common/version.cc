// $Id: version.cc,v 1.2 2008/07/18 15:26:49 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "config/version.h"
#include "xcept/version.h"
#include "toolbox/version.h"
#include "pt/version.h"
#include "i2o/version.h"

GETPACKAGEINFO(i2o)

void i2o::checkPackageDependencies() throw (config::PackageInfo::VersionException)
{
	CHECKDEPENDENCY(config);
	CHECKDEPENDENCY(xcept);
	CHECKDEPENDENCY(toolbox);
}

std::set<std::string, std::less<std::string> > i2o::getPackageDependencies()
{
	std::set<std::string, std::less<std::string> > dependencies;

	ADDDEPENDENCY(dependencies,config); 
	ADDDEPENDENCY(dependencies,xcept); 
	ADDDEPENDENCY(dependencies,toolbox); 
  
	return dependencies;
}	
	
