// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "i2o/utils/AddressMap.h"
#include "toolbox/mem/Reference.h"
#include "toolbox/string.h"
#include "xdaq/ApplicationContext.h"
#include "xdaq/ApplicationRegistry.h"
#include "i2o/Method.h"


const xdaq::ApplicationDescriptor* i2o::utils::AddressMap::getApplicationDescriptor(I2O_TID tid)
throw (xdaq::exception::ApplicationDescriptorNotFound)
{
	const xdaq::ApplicationDescriptor* descriptor = tidToApplication_[tid];
	if (descriptor == 0)
	{
		std::stringstream msg;
		msg << "No application descriptor mapping found for tid ";
		msg << tid;
		XCEPT_RAISE (xdaq::exception::ApplicationDescriptorNotFound, msg.str());
	}
	else
	{
		return descriptor;
	}
}


I2O_TID i2o::utils::AddressMap::getTid(const xdaq::ApplicationDescriptor* descriptor)
throw (xdaq::exception::ApplicationDescriptorNotFound)
{
	I2O_TID tid = applicationToTid_[descriptor];
	if (tid == 0)
	{
		std::stringstream msg;
		msg << "No tid mapping found for application descriptor ";
		msg << descriptor->getURN() << " in context ";
		msg << descriptor->getContextDescriptor()->getURL();
		XCEPT_RAISE (xdaq::exception::ApplicationDescriptorNotFound, msg.str());
	}
	else
	{
		return tid;
	}
}


void i2o::utils::AddressMap::setApplicationDescriptor (I2O_TID tid, const xdaq::ApplicationDescriptor* descriptor)
{
	tidToApplication_[tid] = descriptor;
	applicationToTid_[descriptor] = tid;
}



void i2o::utils::AddressMap::removeTid (I2O_TID tid) 
throw (xdaq::exception::ApplicationDescriptorNotFound)
{
	
	const xdaq::ApplicationDescriptor* descriptor = tidToApplication_[tid];
	if (descriptor != 0)
	{
		tidToApplication_.erase(tid);
		applicationToTid_.erase(descriptor);
	} else
	{
		std::stringstream msg;
		msg << "No application mapping found for tid ";
		msg << tid;
		XCEPT_RAISE (xdaq::exception::ApplicationDescriptorNotFound, msg.str());
	}
}

void i2o::utils::AddressMap::clear()
{
	tidToApplication_.clear();
	applicationToTid_.clear();
}

// ----
// Factory functions
// ----

i2o::utils::AddressMap* i2o::utils::AddressMap::instance_ = 0;

i2o::utils::AddressMap* i2o::utils::getAddressMap()
{
	return i2o::utils::AddressMap::getInstance();
}

//! Retrieve a pointer to the toolbox::task::WorkLoopFactory singleton
//
i2o::utils::AddressMap* i2o::utils::AddressMap::getInstance()
{
	if (instance_ == 0)
	{
		instance_ = new i2o::utils::AddressMap();
	}
	return instance_;
}

//! Destoy the factory and all associated work loops
//
void i2o::utils::AddressMap::destroyInstance()
{
	if (instance_ != 0)
	{
		delete instance_;
		instance_ = 0;
	}
}
