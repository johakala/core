// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/
 
#include "i2o/utils/Dispatcher.h"
#include "toolbox/mem/Reference.h"
#include "toolbox/string.h"
#include "xdaq/ApplicationContext.h"
#include "xdaq/ApplicationRegistry.h"
#include "i2o/i2o.h"
#include "i2o/i2oObjLib.h"
#include "i2o/i2oDdmLib.h"
#include "i2o/shared/i2oexec.h"
#include "i2o/shared/i2omsg.h"
#include "i2o/Method.h"
static i2o::MethodSignature* tidIndex[4096+1][1024+1];

i2o::utils::Dispatcher::Dispatcher
(
	xdaq::ApplicationContext* context, 
	Logger & logger
)
	:logger_( log4cplus::Logger::getInstance( logger.getName() + ".i2o.utils.Dispatcher"))

{
	this->context_  = context;
	this->registry_ = context->getApplicationRegistry();
	this->addressMap_ = i2o::utils::getAddressMap();

        for ( size_t i=0; i < 4097; i++)
        {
                for ( size_t j = 0; j < 1025; j++ )
                {
                        tidIndex[i][j] = 0;
                }
        }

}

i2o::utils::Dispatcher::~Dispatcher()
{
}

void i2o::utils::Dispatcher::processIncomingMessage 
(
	toolbox::mem::Reference* msg
) 
	throw (pt::exception::Exception)
{
	I2O_PRIVATE_MESSAGE_FRAME * framePtr = (I2O_PRIVATE_MESSAGE_FRAME*) msg->getDataLocation();	
	I2O_TID tid = framePtr->StdMessageFrame.TargetAddress;
	U16 fid = (U16)framePtr->StdMessageFrame.Function;

	if  ( fid == 0x00ff ) // is a private I2O message then convert fid to private entry
	{
		//set entry in private message area
		fid = framePtr->XFunctionCode | framePtr->OrganizationID << XDAQ_XFUNCTION_PLACEMENT; 
	}

	// Multicast support.
	// The TargetId coming from the network contains the
	// target class id. It has to be replaced with the
	// proper tid. If the tid is not found, the message
	// shall be silently discarded.
	//
	// if ((framePtr->StdMessageFrame.MsgFlags & XDAQ_MULTICAST) == XDAQ_MULTICAST)
	// {
		// TBD.
		// loop over all tids ov a local class						
		// #warning "MULTICAST dispatch not implemented"
		// const xdaq::Application* const application = registry_->getApplication(tid);							
		// static_cast<xdaq::I2OMethodSignature*>(application->getMethod(fid))->invoke(ref);						
	// }
	//else 
	//{
		i2o::MethodSignature* method = 0;

		if ( tidIndex[tid][fid] == 0 )
                {
                        try
                        {
                                const xdaq::ApplicationDescriptor* d = this->addressMap_->getApplicationDescriptor(tid);
                                xdaq::Application* application = this->registry_->getApplication(d->getLocalId());
                                method = dynamic_cast<i2o::MethodSignature*>(application->getMethod(fid));
                        }
                        catch (xdaq::exception::ApplicationNotFound& anf)
                        {
                                msg->release();
                                XCEPT_RETHROW(pt::exception::Exception, "Failed to process incoming I2O message", anf);
                        }
                        catch (xdaq::exception::ApplicationDescriptorNotFound& adnf)
                        {
                                msg->release();
                                XCEPT_RETHROW(pt::exception::Exception, "Failed to process incoming I2O message", adnf);
                        }
                        tidIndex[tid][fid] = method;
                }

 		if (tidIndex[tid][fid] == 0)
		{
			msg->release();
			std::string info = toolbox::toString("No callback method found for incoming I2O message, fid(%d), tid(%d)",fid, tid);
			LOG4CPLUS_ERROR (logger_, info);
			XCEPT_RAISE(pt::exception::Exception, info);
		}
		else
		{
			try
			{
				tidIndex[tid][fid]->invoke(msg);
			}
			catch (i2o::exception::Exception& e)
			{
				msg->release();
				std::string info = toolbox::toString ("Exception occured during I2O method dispatch, fid(%d), tid(%d)", fid, tid);
				LOG4CPLUS_ERROR (logger_, info);
				XCEPT_RETHROW (pt::exception::Exception, info, e);
			}
			catch (...)
			{
				msg->release();
				std::string info = toolbox::toString ("Caught unhandled exception raised in user program during I2O method dispatch, fid(%d), tid(%d)", fid, tid);
				LOG4CPLUS_ERROR (logger_, info);
				XCEPT_RAISE (pt::exception::Exception, info);
			}
		}
	// }	
}


