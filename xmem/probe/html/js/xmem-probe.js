var columnOptions = {
    chart: {    	
    	type: 'column',
    	spacingBottom: 0, 
    	spacingRight: 0,   	
    },
    title: {
    	text: '',
    },
    colors: ['#60c068', '#0000ff'],
    legend: {
    	 align: 'right',
    	 layout: 'vertical',
         verticalAlign: 'top',
         floating: false,
         backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
         borderColor: '#CCC',
         borderWidth: 1,
         shadow: false
   	 },
    credits: {
    	enabled: false
    },
    tooltip: {
        formatter: function() {
            return this.series.name + '<br/><b>' + this.x + '</b>: <b>' + this.y + '</b>';
        }
    },
    plotOptions: {
                column: {
                    stacking: 'normal',
                    dataLabels: {
                        enabled: true,
                        //format: 'Memory Used', //this put it in the columns permantly, find the hover one!
                        color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
                        style: {
                            textShadow: '0 0 3px black, 0 0 3px black'
                        }
                    }
                }
    },
    xAxis: {
    	title: {
    		text: 'Size'
    	},
        categories: []
    },
    yAxis: {
    	min: 0,
        title: {
            text: 'Number of Blocks'
        }
    },
    series: [],

};

//pie usage chart for memory

var pieOptions = {
    chart: {   
    		spacingLeft:0,
    		spacingRight: 0,
    		
            },
    title: {
    		text: '',
    },
            legend: {
            	align: 'right',
           	 	layout: 'vertical',
           	 	verticalAlign: 'top',
           	 	itemMarginTop: 5,
    			itemMarginBottom: 5,
    			itemMarginRight: 5,
    			borderRadius: '10px',
            	},
            credits: {
            	enabled: false
            		},
           
            tooltip: {
					pointFormat: '<b>{point.percentage:.1f}%</b>'            
					},
            plotOptions: {
                pie: {
                	size: '75%',
                	shadow: true, 
                	borderWidth: 0,
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                      	distance: 3,
                        format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    },
                   		showInLegend: true
               }
           	},
            series: []
};
    
  //format pie options

FormatOptions = {
    	
 	chart: {
 	    renderTo: 'mem-format-pie',
    	spacingLeft:0,
    	spacingRight: 0,
    },
    		title: {   		
    			text: '',
    		},
            tooltip: {
        	    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            legend: {
            	align: 'right',
           	 	layout: 'vertical',
           	 	verticalAlign: 'top',        	 	
           	 	itemMarginTop: 5,
    			itemMarginBottom: 5
            	
            		},
            credit: {
            	enabled: false,
            		},
            plotOptions: {
                pie: {
                	size: '75%',
                	shadow: true,
                	borderWidth: 0,
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                       	enabled: false
                    },
                    showInLegend: true
                }
            },
            
            series: [{
            	type: 'pie',
            	
            }]
 };




function refreshPoolList(){ 

			$.get($("#mem-pool-refresh").attr("data-url")+"/listPools", function(data) {
	    		    		
	    	var xml = $(data); //makes a jquery object
	    		
	    	$("#xmem-pool-name").empty();
	    		    		
	    	xml.find("pool").each(function(){
	    	    var poolname = ($(this).attr('name'));
	    	    $("#xmem-pool-name").append($("<option/>").text(poolname));
	    	   
	    	refreshCurrent();
	    	
	    	}); 

	    	});
};

function refreshCurrent(){ 

	    	var name = $("#xmem-pool-name").find('option:selected').text();
	    	var plottype = $("#xmem-plot-type").find('option:selected').text();
	    	
	    	displayChart(name,plottype);
	   
};

function resizeCurrent(){ 
 	var plot = $('#xmem-bottom-panel').highcharts();
 	var h = $("#xdaq-main").height()-70;
  	var w = $("#xdaq-main").width()-70;
   	if ( plot != undefined)
   	{
  			$('#xmem-bottom-panel').highcharts().setSize( w,h,false);
	}  
}

function displayChart(name, type){

	//var address = document.location.href + "/displayUsage?name="+ name;
	var address = $("#mem-pool-refresh").attr("data-url") + "/displayUsage?name="+ name;

	$.get(address, function(xml) {
		poolinfo = $(xml);
		
		$('#xmem-bottom-panel').empty();
		
		if ( type == "Memory Format")
		{
			var FormatSeriesOptions = {
				type: 'pie',
		    	animation: true,
				data: []
			}
			
			poolinfo.find('block').each(function(i, point) {
				var newEntry = {
						name: $(point).children("label").text(),
						y: parseInt($(point).children("bytesUsed").text())
				};
				FormatSeriesOptions.data.push(newEntry);
			});	
			FormatOptions.series = [];
			FormatOptions.series.push(FormatSeriesOptions);
		
			$('#xmem-bottom-panel').highcharts(FormatOptions);
			resizeCurrent();
   		}
   		else if ( type ==  "Number of Blocks")
   		{
   			poolinfo.find('block label').each(function(i, category) {
				columnOptions.xAxis.categories.push($(category).text());
			
			});
			var MemoryUsed = {
			    	animation: false,
					data: []
			}
			var MemoryFree = {
			    	animation: false,
					data: []
			}
			
			poolinfo.find('block blocksUsed').each(function(i, point) {
				MemoryUsed.data.push(parseInt($(point).text()));
			});
			
			poolinfo.find('block blocksFree').each(function(i, point) {
				MemoryFree.data.push(parseInt($(point).text()));
			});
			
		
			// add it to the options
			columnOptions.series = [];
			
			columnOptions.series.push(MemoryFree);
			
			columnOptions.series.push(MemoryUsed);
			columnOptions.series[0].name = "Memory Free";
			columnOptions.series[1].name = "Memory Allocated";
			
			$('#xmem-bottom-panel').highcharts(columnOptions);
			resizeCurrent();
   		}
   		else if ( type ==  "Memory Usage")
   		{
   			//Pie chart for usage
			
			var pieSeriesOptions = {
				type: 'pie',
		    	animation: true,
				data: []
			}
			
			poolinfo.find('usage statistic').each(function(i, point) {
				var newEntry = {
						name: $(point).children("label").text(),
						y: parseInt($(point).children("bytes").text())
				};
				pieSeriesOptions.data.push(newEntry);
			});	 
			pieOptions.series = [];
			pieOptions.series.push(pieSeriesOptions);

			$('#xmem-bottom-panel').highcharts(pieOptions);
			resizeCurrent();
   		}
   		else if ( type ==  "Summary")
   		{
   		 	$('#xmem-bottom-panel').empty(); 
   		 	
   			//xml for summary table
			$('<table/>',{'id':'xmem-summary-table' , 'class':'xdaq-table'}).appendTo('#xmem-bottom-panel');
			
			 poolinfo.find('summary statistic').each(function(){
			    var Col1 = $(this).find('label').text();
			    var Col2 = $(this).find('value').text();
			    $('<tr></tr>').html('<td>'+Col1+'</td><td>'+Col2+'</td>').appendTo('#xmem-summary-table');
			 });
		
   		}
   		 
	});
	
	
	
}

$(document).on("xdaq-post-load", function() 
{
	$("#mem-pool-refresh").on("click", function() 
	{
		refreshPoolList();		
	});
	
	$("#refresh-current").on("click", function() 
	{
		refreshCurrent();		
	});
	
	$( window ).resize(function() {
  		//alert($("#xdaq-main").height()+":"+$("#xdaq-main").width());
  		resizeCurrent(); 
	});

	$("#xmem-pool-name").change(function () {
        var name = this.value;
        var plottype = $("#xmem-plot-type").find('option:selected').text();
        
      //   alert(plottype+ "-" + name);
        displayChart(name,plottype);
        
    });
    
    $("#xmem-plot-type").change(function () {
        var plottype = this.value;
        var name = $("#xmem-pool-name").find('option:selected').text();
       //  alert(plottype+ "/" + name);
        displayChart(name, plottype);
       
    });
    
	refreshPoolList();
});








