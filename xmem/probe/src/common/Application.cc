// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.			                 			 *
 * All rights reserved.                                                  *
 * Authors:  P. Roberts				 									 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                             *
 * For the list of contributors see CREDITS.   			                 *
 *************************************************************************/

#include "xmem/probe/Application.h"
#include "xgi/Utils.h"
#include "xgi/Method.h"
#include "xgi/framework/Method.h"

#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"

#include "toolbox/mem/MemoryPoolFactory.h"
#include "toolbox/net/URN.h"
#include "toolbox/mem/SmartBufPool.h"
#include "toolbox/mem/CommittedHeapAllocator.h"

#include <sstream>

XDAQ_INSTANTIATOR_IMPL (xmem::probe::Application)

xmem::probe::Application::Application(xdaq::ApplicationStub * s) throw (xdaq::exception::Exception)
		: xdaq::Application(s), xgi::framework::UIManager(this)
{
	s->getDescriptor()->setAttribute("icon", "/xmem/probe/images/xmem-probe-icon.png");
	xgi::framework::deferredbind(this, this, &xmem::probe::Application::Default, "Default");
	xgi::framework::deferredbind(this, this, &xmem::probe::Application::flexDisplay, "flexDisplay");
	xgi::bind(this, &xmem::probe::Application::displayUsage, "displayUsage");
	xgi::bind(this, &xmem::probe::Application::selfTest, "selfTest");
	xgi::bind(this, &xmem::probe::Application::listPools, "listPools");
}

void xmem::probe::Application::Default(xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	std::stringstream baseurl;
		std::stringstream linkurl;

		baseurl << this->getApplicationDescriptor()->getContextDescriptor()->getURL() << "/" << this->getApplicationDescriptor()->getURN();
		linkurl << this->getApplicationDescriptor()->getContextDescriptor()->getURL() << "/displayUsage?" << this->getApplicationDescriptor()->getURN();
	//includes for js
		*out << "<script src=\"http://code.highcharts.com/highcharts.js\"></script>" <<std::endl;
		*out << "<script type=\"text/javascript\" src=\"/highcharts/js/modules/exporting.js\"></script>" << std::endl;
		*out << "<script type=\"text/javascript\" src=\"/xmem/probe/html/js/xmem-probe.js\"></script> " << std::endl;
	//include the css
		*out << "<link rel=\"stylesheet\" type=\"text/css\" href=\"/xmem/probe/html/css/xmem-probe.css\" />" << std::endl;

		*out << "		<div id=\"xmem-top-panel\">                                                          " << std::endl;

				*out << "				<button id=\"mem-pool-refresh\" data-url=\"" << baseurl.str() << "\" >Reload Memory Pool List</button>      " << std::endl;
				*out << "				<button id=\"refresh-current\" >Refresh Current Selection</button>          " << std::endl;

					*out << " <select id=\"xmem-pool-name\" style=\"width:350px\"> " << std::endl;
					*out << "     </select>" << std::endl;

					*out << " <select id=\"xmem-plot-type\" style=\"width:250px\"> " << std::endl;
					*out << "      <option  selected=\"selected\">Summary</option>" << std::endl;
					*out << "      <option>Memory Format</option>" << std::endl;
					*out << "      <option>Number of Blocks</option>" << std::endl;
					*out << "      <option>Memory Usage</option>" << std::endl;
					*out << "</select>" << std::endl;

				*out << "		</div>                                                                            " << std::endl;

		*out << "		<div id=\"xmem-wrapper\">                                                          " << std::endl;
		*out << "		</div>                                                                            " << std::endl;

		*out << "		<div id=\"xmem-bottom-panel\">                                                             " << std::endl;
		*out << "		</div>                                                        " << std::endl;

}

void xmem::probe::Application::flexDisplay(xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	// embed flash memscan application
	std::stringstream to;
	to << this->getApplicationDescriptor()->getContextDescriptor()->getURL() << "/" << this->getApplicationDescriptor()->getURN();
	std::string memprobeUrl = "/xmem/probe/html/EmbeddedMemscan.swf?service=" + cgicc::form_urlencode(to.str());
	*out << "<object classid=\"clsid:d27cdb6e-ae6d-11cf-96b8-444553540000\" width=\"100%\" height=\"600\">";
	*out << "<param name=\"memscan\" value=\"" << memprobeUrl << "\">";
	*out << "<!--[if !IE]>-->";
	*out << "<object type=\"application/x-shockwave-flash\" data=\"" << memprobeUrl << "\" width=\"100%\" height=\"600\">";
	*out << "<!--<![endif]-->";
	*out << "<p>Error loading memscan flex (version 1.0.0 - Copyright 2010 CERN)  application, contact XDAQ support</p>";
	*out << "<!--[if !IE]>-->";
	*out << "</object>";
	*out << "<!--<![endif]-->";
	*out << "</object>";

}

// Returns a human-readable representation of a given number of bytes
// i.e. an input of 1024 returns "1 KB"
std::string xmem::probe::Application::qualifiedSize(size_t size)
{
	if (size >= 0x100000)
	{
		return toolbox::toString("%.1lf MB", (float) size / 0x100000);
	}
	if (size >= 1024)
	{
		return toolbox::toString("%.1lf KB", size / 1024.0);
	}
	else
	{
		return toolbox::toString("%ld Bytes", size);
	}

}

// Output XML data detailing which pools are available on the host
void xmem::probe::Application::listPools(xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	std::vector < std::string > memoryPoolNames = toolbox::mem::getMemoryPoolFactory()->getMemoryPoolNames();

	*out << "<?xml version=\"1.0\" encoding=\"utf-8\" ?>";
	*out << "<pools>";

	// List each memory pool being used
	for (std::vector<std::string>::size_type i = 0; i < memoryPoolNames.size(); i++)
	{
		// Pool's name
		*out << "<pool name=\"" << memoryPoolNames[i] << "\" />";
	}

	*out << "</pools>";
}

// Outputs XML data describing a specific memory pool
void xmem::probe::Application::displayUsage(xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	*out << "<?xml version=\"1.0\" encoding=\"utf-8\" ?>";
	*out << "<poolData>";

	size_t totalMemoryConsumed = 0;
	std::map<size_t, size_t, std::less<size_t> >::iterator a;
	std::stringstream format, distribution;

	cgicc::Cgicc cgi(in);
	std::string poolName = cgi["name"]->getValue();

	toolbox::net::URN urn(poolName);
	toolbox::mem::Pool* pool = toolbox::mem::getMemoryPoolFactory()->findPool(urn);
	toolbox::mem::Usage& usage = pool->getMemoryUsage();
	std::map<size_t, size_t, std::less<size_t> >& created = usage.getCreatedBlocks();
	std::map<size_t, size_t, std::less<size_t> >& cached = usage.getCachedBlocks();

	size_t totalCreatedFormattedMemory = 0;
	for (std::map<size_t, size_t, std::less<size_t> >::iterator a = created.begin(); a != created.end(); a++)
	{
		totalCreatedFormattedMemory += (*a).first * (*a).second; // size times number of blocks
	}

	// Summary statistics of the pool
	*out << "<summary>";
	*out << "<statistic><label>Name</label><value>" << poolName << "</value></statistic>";
	*out << "<statistic><label>Committed</label><value>" << this->qualifiedSize(usage.getCommitted()) << "</value></statistic>";
	*out << "<statistic><label>Used</label><value>" << this->qualifiedSize(usage.getUsed()) << "</value></statistic>";
	*out << "<statistic><label>Total Free</label><value>" << this->qualifiedSize(usage.getCommitted() - usage.getUsed()) << "</value></statistic>";
	*out << "<statistic><label>Formatted Free</label><value>" << this->qualifiedSize(totalCreatedFormattedMemory) << "</value></statistic>";
	*out << "<statistic><label>Unformatted Free</label><value>" << this->qualifiedSize(usage.getCommitted() - totalCreatedFormattedMemory) << "</value></statistic>";
	*out << "<statistic><label>Low Threshold</label><value>" << this->qualifiedSize(pool->getLowThreshold()) << "</value></statistic>";
	*out << "<statistic><label>High Threshold</label><value>" << this->qualifiedSize(pool->getHighThreshold()) << "</value></statistic>";
	*out << "</summary>";

	// Output data on each block
	*out << "<blocks>";

	for (a = cached.begin(); a != cached.end(); a++)
	{
		size_t totalCreatedBlocks = created[(*a).first];
		size_t blockSize = (*a).first;
		size_t memoryConsumed = blockSize * totalCreatedBlocks;
		totalMemoryConsumed += memoryConsumed;

		if (memoryConsumed > 0)
		{
			*out << "<block>";
			*out << "<label>" << this->qualifiedSize((*a).first) << "</label>";			// Allocation size of this block type (64 bytes, 124 bytes, etc)
			*out << "<bytesUsed>" << memoryConsumed << "</bytesUsed>";				// Total bytes in use by this block type
			*out << "<blocksUsed>" << created[(*a).first] - (*a).second << "</blocksUsed>";		// Total blocks allocated in this block's size
			*out << "<blocksFree>" << (*a).second << "</blocksFree>";				// Number of blocks it'd take to fill the remaining free formatted memory
			*out << "</block>" << std::endl;
		}
	}
	*out << "</blocks>";

	size_t committedMem = usage.getCommitted();
	size_t usedMem = usage.getUsed();
	size_t freeMem = committedMem - usedMem;
	size_t unformattedFreeMem = committedMem - totalMemoryConsumed;

	*out << "<usage>";
	*out << "<statistic><label>Unformatted Free</label><bytes>" << unformattedFreeMem << "</bytes></statistic>";
	*out << "<statistic><label>Formatted Free</label><bytes>" << (freeMem - unformattedFreeMem) << "</bytes></statistic>";
	*out << "<statistic><label>Total Allocated</label><bytes>" << usedMem << "</bytes></statistic>";
	*out << "</usage>";

	*out << "</poolData>";
}

void xmem::probe::Application::allocateBlocks(toolbox::mem::Pool* pool) throw (toolbox::mem::exception::Exception)
{
	// From 64 bytes to 256 KB
	for (size_t j = BUF_LOG2_64; j <= BUF_LOG2_262144; j++)
	{
		std::vector<toolbox::mem::Reference *> remember;
		//std::cout << "allocating " <<  (BUF_LOG2_262144 -j)  <<  " of size " <<  BUF_LOG2_TO_BUF_SIZE(j) << std::endl;
		for (size_t i = 0; i <= (BUF_LOG2_262144 - j); i++)
		{
			//std::cout << "." ;
			toolbox::mem::Reference * ref = toolbox::mem::getMemoryPoolFactory()->getFrame(pool, BUF_LOG2_TO_BUF_SIZE(j));
			remember.push_back(ref);

		}
		for (size_t i = 0; i <= (size_t)((BUF_LOG2_262144 - j) / 3.0); i++)
		//for (unsigned long i = 0; i <= (BUF_LOG2_262144 -j ); i++ ) 
		{
			remember[i]->release();
		}
		remember.clear();
		//std::cout << std::endl;
		//((toolbox::mem::SmartBufPool*)pool)->show();
	}

}

void xmem::probe::Application::allocateBlocks2(toolbox::mem::Pool* pool) throw (toolbox::mem::exception::Exception)
{
	// From 64 bytes to 256 KB
	for (size_t j = BUF_LOG2_64; j <= BUF_LOG2_262144; j++)
	{
		std::vector<toolbox::mem::Reference *> remember;
		//std::cout << "allocating " <<  (BUF_LOG2_262144 -j)  <<  " of size " <<  BUF_LOG2_TO_BUF_SIZE(j) << std::endl;
		for (size_t i = 0; i <= 3; i++)
		{
			//std::cout << "." ;
			toolbox::mem::Reference * ref = toolbox::mem::getMemoryPoolFactory()->getFrame(pool, BUF_LOG2_TO_BUF_SIZE(j));
			remember.push_back(ref);

		}
		for (size_t i = 0; i <= 2; i++)
		//for (unsigned long i = 0; i <= (BUF_LOG2_262144 -j ); i++ )
		{
			remember[i]->release();
		}
		remember.clear();
		//std::cout << std::endl;
		//((toolbox::mem::SmartBufPool*)pool)->show();
	}

}

void xmem::probe::Application::allocateBlocks3(toolbox::mem::Pool* pool) throw (toolbox::mem::exception::Exception)
{
	// From 64 bytes to 256 KB
	for (size_t j = 15; j <= 18; j++)
	{
		std::vector<toolbox::mem::Reference *> remember;
		//std::cout << "allocating " <<  (BUF_LOG2_262144 -j)  <<  " of size " <<  BUF_LOG2_TO_BUF_SIZE(j) << std::endl;
		for (size_t i = 0; i <= 5; i++)
		{
			//std::cout << "." ;
			toolbox::mem::Reference * ref = toolbox::mem::getMemoryPoolFactory()->getFrame(pool, BUF_LOG2_TO_BUF_SIZE(j));
			remember.push_back(ref);

		}
		for (size_t i = 0; i <= 1; i++)
		//for (unsigned long i = 0; i <= (BUF_LOG2_262144 -j ); i++ )
		{
			remember[i]->release();
		}
		remember.clear();
		//std::cout << std::endl;
		//((toolbox::mem::SmartBufPool*)pool)->show();
	}

}

// Allocate some memory pools with varying amounts of allocated memory blocks that can be used for testing purposes
void xmem::probe::Application::selfTest(xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	try
	{
		// Test Pools A (0x100000)
		toolbox::mem::CommittedHeapAllocator* ca = new toolbox::mem::CommittedHeapAllocator(0x200000);
		toolbox::net::URN urna("toolbox-mem-pool", "TestMemoryPoolA");
		toolbox::mem::Pool * pa = toolbox::mem::getMemoryPoolFactory()->createPool(urna, ca);
		// Set high watermark to 90% and low watermark to 70%
		pa->setHighThreshold((size_t)(0x200000 * 0.8));
		pa->setLowThreshold((size_t)(0x200000 * 0.3));
		// Allocate in pool A
		this->allocateBlocks(pa);

		// Test Pools B (0x500000)
		toolbox::mem::CommittedHeapAllocator* cb = new toolbox::mem::CommittedHeapAllocator(0x500000);
		toolbox::net::URN urnb("toolbox-mem-pool", "TestMemoryPoolB");
		toolbox::mem::Pool * pb = toolbox::mem::getMemoryPoolFactory()->createPool(urnb, cb);
		// Set high watermark to 90% and low watermark to 70%
		pb->setHighThreshold((size_t)(0x500000 * 0.9));
		pb->setLowThreshold((size_t)(0x500000 * 0.7));
		this->allocateBlocks2(pb);

		// Test Pools C (0x500000)
		toolbox::mem::CommittedHeapAllocator* cc = new toolbox::mem::CommittedHeapAllocator(0x1000000);
		toolbox::net::URN urnc("toolbox-mem-pool", "TestMemoryPoolC");
		toolbox::mem::Pool * pc = toolbox::mem::getMemoryPoolFactory()->createPool(urnc, cc);
		// Set high watermark to 90% and low watermark to 70%
		pc->setHighThreshold((size_t)(0x1000000 * 0.7));
		pc->setLowThreshold((size_t)(0x1000000 * 0.2));
		this->allocateBlocks3(pc);
	}
	catch (toolbox::mem::exception::Exception & e)
	{
		XCEPT_RETHROW(xgi::exception::Exception, "Could not set up memory pool", e);
	}
}

