// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xdaq/ContextTable.h"
#include "xdaq/ContextDescriptor.h"
#include "toolbox/string.h"
#include "toolbox/net/URL.h"
#include "toolbox/net/Utils.h"
#include "toolbox/net/exception/MalformedURL.h"
#include "toolbox/net/exception/BadURL.h"
#include "xdaq/util/HostDescriptor.h"

// DTOR
xdaq::ContextTable::~ContextTable()
{
	std::map<std::string, xdaq::ContextDescriptor*, std::less<std::string> >::iterator i;
	for ( i = this->contexts_.begin(); i != this->contexts_.end(); i++ )
	{
		delete (*i).second;
	}
}

xdaq::ContextDescriptor* xdaq::ContextTable::createContextDescriptor(const std::string& url ) 
	throw  (xdaq::exception::DuplicateHost, xdaq::exception::CreationFailed )
{
	//std::cerr << "DEPRECATED METHOD, should use createContextDescriptor with HostDescriptor" << std::endl;
	
	// normalize given url to protocol://IPNumber:port, find and store in normalized form
	std::string normalizedURL = "";
	try
	{
		toolbox::net::URL u(url);
		normalizedURL = u.getNormalizedURL();
	}
	catch (toolbox::net::exception::MalformedURL& e)
	{
		XCEPT_RETHROW (xdaq::exception::CreationFailed, "Malformed URL", e);
	}
	catch (toolbox::net::exception::BadURL& e)
	{
		XCEPT_RETHROW (xdaq::exception::CreationFailed, "Bad URL", e);
	}
	
	if ( this->contexts_.find(normalizedURL) == this->contexts_.end() ) 
	{
		this->contexts_[normalizedURL] = new xdaq::ContextDescriptor(url);
		return this->contexts_[normalizedURL];
	}
	else
	{
		std::string msg = "Context url ";
		msg += normalizedURL;
		msg += "' already existing";
		XCEPT_RAISE(xdaq::exception::DuplicateHost, msg);
	}	
}

void xdaq::ContextTable::removeContext(const std::string& url) 
	throw  (xdaq::exception::HostNotFound )
{
	// normalize given url to protocol://IPNumber:port, find and store in normalized form	
	try
	{
		toolbox::net::URL u(url);
		std::string normalizedURL = u.getNormalizedURL();	

		if ( this->contexts_.find(normalizedURL) != this->contexts_.end() ) 
		{
			xdaq::ContextDescriptor* d = this->contexts_[normalizedURL];
			delete d;
			this->contexts_.erase(normalizedURL);
		}
		else
		{
			std::string msg = "Context for url ";
			msg += url;
			msg += " not found";
			XCEPT_RAISE(xdaq::exception::HostNotFound, msg);
		}
	}
	catch (toolbox::net::exception::MalformedURL& e)
	{
		XCEPT_RETHROW (xdaq::exception::HostNotFound, "Malformed URL", e);
	}
	catch (toolbox::net::exception::BadURL& e)
	{
		XCEPT_RETHROW (xdaq::exception::HostNotFound, "Bad URL", e);
	}
}

const xdaq::ContextDescriptor* xdaq::ContextTable::getContextDescriptor(const std::string & url) const
	throw (xdaq::exception::HostNotFound)
{
	// normalize given url to protocol://IPNumber:port, find and store in normalized form
	try
	{
		toolbox::net::URL u(url);
		std::string normalizedURL = u.getNormalizedURL();
		std::map<std::string, xdaq::ContextDescriptor*, std::less<std::string> >::const_iterator i =  this->contexts_.find(normalizedURL);
		if (i != this->contexts_.end() )
		{
			return (*i).second;
		}
		else
		{
			std::string msg = "Context for url ";
			msg += url;
			msg += " not found";
			XCEPT_RAISE(xdaq::exception::HostNotFound, msg);
		}
	}
	catch (toolbox::net::exception::MalformedURL& e)
	{
		XCEPT_RETHROW (xdaq::exception::HostNotFound, "Malformed URL", e);
	}
	catch (toolbox::net::exception::BadURL& e)
	{
		XCEPT_RETHROW (xdaq::exception::HostNotFound, "Bad URL", e);
	}
}

std::vector<xdaq::ContextDescriptor*> xdaq::ContextTable::getContextDescriptors() const
{
	std::vector<xdaq::ContextDescriptor*> contexts;
	
	std::map<std::string, xdaq::ContextDescriptor*, std::less<std::string> >::const_iterator i;
	for ( i =  this->contexts_.begin(); i != this->contexts_.end(); i++ )
	{
		contexts.push_back((*i).second);
	}
	return contexts;
}

bool xdaq::ContextTable::hasContextDescriptor(const std::string & url) const
{
	// normalize given url to protocol://IPNumber:port, find and store in normalized form
	try
	{
		toolbox::net::URL u(url);
		std::string normalizedURL = u.getNormalizedURL();

		if ( this->contexts_.find(normalizedURL) != this->contexts_.end() ) 
		{
			return true;
		}
	}
	catch (toolbox::net::exception::MalformedURL& e)
	{
		return false;
	}
	catch (toolbox::net::exception::BadURL& e)
	{
		return false;
	}
	return false;
}


