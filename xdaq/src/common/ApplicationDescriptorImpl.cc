// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xdaq/ApplicationDescriptorImpl.h"
#include <sstream>
#include <string>
#include "toolbox/string.h"
#include "xdata/UnsignedInteger.h"

xdaq::ApplicationDescriptorImpl::ApplicationDescriptorImpl
( 
	const xdaq::ContextDescriptor* context,
	const std::string& className,
	xdata::UnsignedIntegerT localId,
	const std::string & groups
)
{
	this->instance_ = 0; // instance is not set initially	
	this->hasInstance_ = false;
	
	this->id_ = localId; // here we only store the local numeric identifier that is unique within the context
	this->className_ = className;
	this->context_ = context;
	
	// Default properties for discovery services
	this->setProperty("group", groups);

	// the default value of the service must be empty
	// this->setProperty("service", className);
	
	this->setProperty("id", this->id_.toString());	
	this->setProperty("className", className);
	this->setProperty("context", context->getURL());
	this->setProperty("instance", this->instance_.toString());
	this->setProperty("hasInstance", this->hasInstance_.toString());
	this->setProperty("uuid", this->uuid_.toString());
}

bool xdaq::ApplicationDescriptorImpl::equals (const ApplicationDescriptor& descriptor) const
{
	return (this->uuid_ == descriptor.getUUID());
}

xdaq::ApplicationDescriptorImpl::ApplicationDescriptorImpl
( 
	const xdaq::ContextDescriptor* context,
	const std::string& className,
	xdata::UnsignedIntegerT localId,
	const std::string & groups,
	toolbox::net::UUID & uuid
)
{
	uuid_ = uuid;
	
	this->instance_ = 0; // instance is not set initially	
	this->hasInstance_ = false;
	
	this->id_ = localId; // here we only store the local numeric identifier that is unique within the context
	this->className_ = className;
	this->context_ = context;
	
	// Default properties for discovery services
	this->setProperty("group", groups);

	// the default value of the service must be empty
	// this->setProperty("service", className);
	
	this->setProperty("id", this->id_.toString());	
	this->setProperty("className", className);
	this->setProperty("context", context->getURL());
	this->setProperty("instance", this->instance_.toString());
	this->setProperty("hasInstance", this->hasInstance_.toString());
	this->setProperty("uuid", this->uuid_.toString());
}

void xdaq::ApplicationDescriptorImpl::setAttributes(toolbox::Properties& attributes)
	throw (xdaq::exception::Exception)
{
	try
	{
		std::string instance = attributes.getProperty("instance");
		if (instance != "")
		{
			this->instance_.fromString(instance);
			this->hasInstance_ = true;
		}
		else
		{
			this->hasInstance_ = false;
		}

		this->id_.fromString (attributes.getProperty("id"));
		this->className_.fromString (attributes.getProperty("className"));
		
		try
		{
			uuid_ = toolbox::net::UUID(attributes.getProperty( "uuid" ));
		}
		catch (toolbox::net::exception::Exception& e)
		{
			std::stringstream msg;
			msg << "Failed to set application descriptor property 'uuid' value '";
			msg << attributes.getProperty( "uuid" ) << "'" << std::endl;
			XCEPT_RETHROW (xdaq::exception::Exception, msg.str(), e);
		}
		
		std::vector<std::string> alist = attributes.propertyNames();
		for (std::vector<std::string>::iterator i = alist.begin(); i != alist.end(); ++i)
		{
			
			this->setProperty ( *i, attributes.getProperty( *i ) );
		}
		
	}
	catch (xdata::exception::Exception& e)
	{
		std::stringstream msg;
		msg << "Failed to set application descriptor attributes" << std::endl;
		XCEPT_RETHROW (xdaq::exception::Exception, msg.str(), e);
	}
}

std::string xdaq::ApplicationDescriptorImpl::getURN() const
{
	/*
	urn += contextId_.toString();
	urn += ",class=";
	urn += className_;
	urn += ",instance=";
	urn += instance_.toString();
	*/

	std::string urn = "urn:xdaq-application:";
	/*
        if (this->hasProperty("service"))
        {
                urn += "service=";
                urn += this->getProperty("service");
        }
        else
        {
	*/
                urn += "lid=";
                urn += id_.toString();
        // }

	return urn;
}


std::string xdaq::ApplicationDescriptorImpl::getClassName() const
{
	return this->className_;
}

xdata::UnsignedIntegerT xdaq::ApplicationDescriptorImpl::getInstance() const
	throw (xdaq::exception::Exception)
{
	// Compare always agains a 32 bit number: unsigned int
	if (this->hasInstanceNumber())
	{
		return this->instance_;
	}
	else
	{
		std::string msg = "Instance number for application ";
		msg += this->className_.toString();
		msg += " in context ";
		msg += this->context_->getURL();
		msg += " not set";
		XCEPT_RAISE (xdaq::exception::Exception, msg);
	}
}

xdata::UnsignedIntegerT xdaq::ApplicationDescriptorImpl::getLocalId() const
{
	return this->id_;
}

const xdaq::ContextDescriptor* xdaq::ApplicationDescriptorImpl::getContextDescriptor() const
{
	return this->context_;
}

void xdaq::ApplicationDescriptorImpl::setInstance(xdata::UnsignedIntegerT instance) 
{
	this->instance_ = instance;
	this->hasInstance_ = true;
	this->setProperty("instance", this->instance_.toString());
	this->setProperty("hasInstance", this->hasInstance_.toString());
}

bool xdaq::ApplicationDescriptorImpl::hasInstanceNumber() const
{
	return this->hasInstance_;
}

std::string xdaq::ApplicationDescriptorImpl::getAttribute (const std::string& name) const
{
	return this->getProperty(name);
}

void xdaq::ApplicationDescriptorImpl::setAttribute(const std::string& name, const std::string& value)
{
	this->setProperty(name, value);
}

toolbox::net::UUID xdaq::ApplicationDescriptorImpl::getUUID() const
{
	return uuid_;
}
