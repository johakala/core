// $Id: XceptSerializer.cc,v 1.6 2008/10/14 12:56:31 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/
 
#include "xdaq/XceptSerializer.h"
#include <string>
#include "xoap/domutils.h"
#include <map>
#include <stack>
#include "toolbox/net/URN.h"
#include "toolbox/string.h"

#include "xdata/exdr/Serializer.h"
#include "xdata/exdr/VectorSerializer.h"
#include "xdata/exdr/PropertiesSerializer.h"

static void importRecursiveFrom (DOMNode* dom, xcept::Exception& e) throw (xdaq::exception::SerializationFailed);

void xdaq::XceptSerializer::writeTo (xcept::Exception& e, DOMNode* node) throw (xdaq::exception::SerializationFailed)
{
	try
	{

		std::string xen = "http://xdaq.web.cern.ch/xdaq/xsd/2005/ErrorNotification-11.xsd";
		DOMDocument* d = node->getOwnerDocument();
		
		DOMNode* currentErrorNode = node;
	
		std::map<std::string,std::string> uprefixes;	
		size_t nscounter = 0;
		
		// recursive (history)
		std::vector<xcept::ExceptionInformation> & history = e.getHistory();
		std::vector<xcept::ExceptionInformation>::reverse_iterator i = history.rbegin();
		while ( i != history.rend() )
		{
			//xcept::ExceptionInformation& info = history.back();
			// get the qualified namespace
			std::string qns = (*i).getProperty("qualifiedErrorSchemaURI");
			std::string prefix = xoap::XMLCh2String(node->getPrefix());

			if (prefix != "")
			{
				prefix += ":";
			}

			DOMElement* error = d->createElementNS ( node->getNamespaceURI(), xoap::XStr(prefix+"error") );
			error->setAttribute ( xoap::XStr("xmlns:qns"), xoap::XStr(qns) );	
			error->setAttribute ( xoap::XStr("xmlns:xen"), xoap::XStr(xen) ); 
			error->setAttribute ( xoap::XStr("sessionID"), xoap::XStr( (*i).getProperty("sessionID") ) );	
			error->setAttribute ( xoap::XStr("qualifiedErrorSchemaURI"), xoap::XStr(qns) );
			currentErrorNode->appendChild (error);
				
			// get the qualified namespace
			std::map<std::string, std::string, std::less<std::string> >& p = (*i).getProperties();
			std::map<std::string, std::string, std::less<std::string> >::iterator j;
			for (j = p.begin(); j != p.end(); j++)
			{
				std::string name = (*j).first;
				if ((name == "identifier") || (name == "notifier") || (name == "dateTime") || (name == "severity"))
				{
					DOMElement* pNode = d->createElementNS ( xoap::XStr(xen), xoap::XStr( "xen:" + (*j).first ) ); 
					DOMText* pText = d->createTextNode ( xoap::XStr( (*j).second ) );
					pNode->appendChild(pText);
					error->appendChild(pNode);
				} 
				else if ((name == "qualifiedErrorSchemaURI") || (name == "sessionID"))
				{
					// do nothing, already given as attributes
				} 
				else if ( name.find("urn:") == 0 )
				{
					toolbox::net::URN urn(name);
					std::string nid = urn.getNamespace();
					std::string nss = urn.getNSS();
					if ( uprefixes.find(nid) ==  uprefixes.end() )
					{
						uprefixes[nid] = toolbox::toString("un%d",nscounter++);
						error->setAttribute ( xoap::XStr("xmlns:"+uprefixes[nid]), xoap::XStr(nid) );
					}
	
 					DOMElement* pNode = d->createElementNS ( xoap::XStr(nid), xoap::XStr( uprefixes[nid]+":" + nss ) );
                                	DOMText* pText = d->createTextNode ( xoap::XStr( (*j).second ) );
                                	pNode->appendChild(pText);
                                	error->appendChild(pNode);
	
	
				}
				else 
				{
					DOMElement* pNode = d->createElementNS ( xoap::XStr(qns), xoap::XStr( "qns:" + (*j).first ) );
					DOMText* pText = d->createTextNode ( xoap::XStr( (*j).second ) );
					pNode->appendChild(pText);
					error->appendChild(pNode);
				}
			}
			
			i++;
			currentErrorNode = error; // stack next node
		}
	}
	catch(DOMException & de)
	{
		XCEPT_RAISE(xdaq::exception::SerializationFailed, xoap::XMLCh2String(de.msg));
	}
}


void xdaq::XceptSerializer::importFrom (DOMNode* dom, xcept::Exception& e) throw (xdaq::exception::SerializationFailed)
{
	try
	{
		// clear current exception
		std::vector<xcept::ExceptionInformation>& h = e.getHistory();
		while (!h.empty())
		{
			h.pop_back();
		}
		
		// Extract first error node
		DOMNodeList* children = dom->getChildNodes();
		for (XMLSize_t i = 0; i < children->getLength(); i++)
		{
			DOMNode* currentChild = children->item(i);
			if (currentChild->getNodeType() == DOMNode::ELEMENT_NODE)
			{
				std::string tag = xoap::XMLCh2String(currentChild->getLocalName());
				if (tag == "error")
				{
					importRecursiveFrom (currentChild, e);
					return;
				}
			}
		}
	}
	catch(DOMException & de)
	{
		XCEPT_RAISE(xdaq::exception::SerializationFailed, xoap::XMLCh2String(de.msg));
	}
}

/*! Read a serialized exception into the provided exception object */
void importRecursiveFrom (DOMNode* dom, xcept::Exception& e) throw(xdaq::exception::SerializationFailed)
{
	try
	{
		DOMNodeList* children = dom->getChildNodes();
	
		// for finding the <error> tag for the recursion
		for (XMLSize_t i = 0; i < children->getLength(); i++)
		{
			DOMNode* currentChild = children->item(i);
			if (currentChild->getNodeType() == DOMNode::ELEMENT_NODE)
			{
				if (xoap::XMLCh2String(currentChild->getLocalName()) == "error")
				{				
					importRecursiveFrom( currentChild, e); // recursion
					break; // unwind recursion
				}
			}
		}
		
		xcept::ExceptionInformation info;
		DOMNamedNodeMap * attributes = dom->getAttributes();
		for (XMLSize_t i = 0; i < attributes->getLength(); i++)
		{
			std::string valueStr  = xoap::XMLCh2String( attributes->item(i)->getNodeValue() );
			std::string nameStr  = xoap::XMLCh2String( attributes->item(i)->getNodeName() );
			if ( nameStr.find("xmlns:") == std::string::npos )
			{
				info.setProperty(nameStr, valueStr);
			}
				
		}
		
		info.setProperty("sessionID", xoap::getNodeAttribute(dom, "sessionID")); 
 	        info.setProperty("qualifiedErrorSchemaURI", xoap::getNodeAttribute(dom, "qualifiedErrorSchemaURI"));
	
        	std::string qnsURI = info.getProperty("qualifiedErrorSchemaURI");
		for (XMLSize_t j = 0; j < children->getLength(); j++)
		{
			DOMNode* currentChild = children->item(j);
			if (currentChild->getNodeType() == DOMNode::ELEMENT_NODE)
			{
				std::string tag = xoap::XMLCh2String(currentChild->getLocalName());
				if (tag != "error")
				{
					std::string nsURI = xoap::XMLCh2String(currentChild->getNamespaceURI());
	
					if (nsURI == "http://xdaq.web.cern.ch/xdaq/xsd/2005/ErrorNotification-11.xsd")
					{
						info.setProperty(tag, xoap::XMLCh2String(currentChild->getTextContent()));
					}
                        		else if ( nsURI == qnsURI )
                        		{
						info.setProperty(tag, xoap::XMLCh2String(currentChild->getTextContent()));
					}
					else
					{
                                		info.setProperty("urn:" + nsURI + ":" +  tag, xoap::XMLCh2String(currentChild->getTextContent()));
                        		}
				}
			}
		}
	
		e.getHistory().push_back(info);
	}
	catch(DOMException & de)
	{
		XCEPT_RAISE(xdaq::exception::SerializationFailed, xoap::XMLCh2String(de.msg));
	}
}

void xdaq::XceptSerializer::writeTo (xcept::Exception& e, xdata::exdr::OutputStreamBuffer *sbuf ) throw(xdata::exception::Exception)
{
	xdata::exdr::Serializer serializer;

	std::vector<xcept::ExceptionInformation> & history = e.getHistory();
	size_t size = history.size();
	if( size >= 0xFFFFFFFFU )
	{
		XCEPT_RAISE( xdata::exception::Exception, "Exceeded maximum vector size (> 2^32 elements)");
	}

	serializer.encodeTag(xdata::exdr::Serializer::Vector, sbuf);
	sbuf->encodeUInt32(static_cast<uint32_t>(size));

	// history is stored in reverse order
	for(std::vector<xcept::ExceptionInformation>::iterator i = history.begin() ; i != history.end() ; i++)
	{
		std::map<std::string, std::string, std::less<std::string> >& p = (*i).getProperties();
		size_t psize = p.size();
		if( psize >= 0xFFFFFFFFU )
		{
			XCEPT_RAISE( xdata::exception::Exception, "Exceeded number of properties (> 2^32 elements)");
		}

		serializer.encodeTag(xdata::exdr::Serializer::Properties, sbuf);
		sbuf->encodeUInt32(static_cast<uint32_t>(psize));

		std::map<std::string, std::string, std::less<std::string> >::iterator j;
		for (j = p.begin(); j != p.end(); j++)
		{
			serializer.encodeTag(xdata::exdr::Serializer::String, sbuf);
			sbuf->encode((*j).first);
			serializer.encodeTag(xdata::exdr::Serializer::String, sbuf);
			sbuf->encode((*j).second);
		}
	}
}

void xdaq::XceptSerializer::importFrom (xdata::exdr::InputStreamBuffer *sbuf, xcept::Exception& e) throw(xdata::exception::Exception)
{
	// clear current exception
	std::vector<xcept::ExceptionInformation>& h = e.getHistory();
	while (!h.empty())
	{
		h.pop_back();
	}
	
	xdata::exdr::Serializer serializer;

	uint32_t size;
	serializer.decodeTag(xdata::exdr::Serializer::Vector, sbuf);
	sbuf->decodeUInt32(size);

	for( uint32_t i = 0 ; i < size ; i ++)
	{
		uint32_t psize;
		serializer.decodeTag(xdata::exdr::Serializer::Properties, sbuf);
		sbuf->decodeUInt32(psize);

		std::string name;
		std::string value;

		xcept::ExceptionInformation info;
		for( uint32_t j = 0 ; j < psize ; j++ )
		{
			serializer.decodeTag(xdata::exdr::Serializer::String, sbuf);
			sbuf->decode(name);
			serializer.decodeTag(xdata::exdr::Serializer::String, sbuf);
			sbuf->decode(value);
			info.setProperty(name, value);
		}

		e.getHistory().push_back(info);
	}
}

