// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include <sstream>
#include "xdaq/ApplicationGroupImpl.h"
#include "toolbox/string.h"

xdaq::ApplicationGroupImpl::ApplicationGroupImpl
(
	const std::string &  name
)
	: name_(name), lock_(toolbox::BSem::FULL, true)
{
}

xdaq::ApplicationGroupImpl::~ApplicationGroupImpl()
{
	
}


void xdaq::ApplicationGroupImpl::addApplicationDescriptor (const xdaq::ApplicationDescriptor* descriptor )
{
	lock_.take();
	this->applicationDescriptors_.insert(descriptor);
	lock_.give();
}


std::string xdaq::ApplicationGroupImpl::getName() const
{
	return this->name_;
}
bool xdaq::ApplicationGroupImpl::hasApplicationDescriptor
(
	const std::string & className, 
	xdata::UnsignedIntegerT instance
) const
{
	lock_.take();
	for (
		std::set<const xdaq::ApplicationDescriptor*>::const_iterator i = this->applicationDescriptors_.begin();
		i != this->applicationDescriptors_.end(); 
		i++ 
	)
	{
		if ((*i)->hasInstanceNumber())
		{
			if (( (*i)->getClassName() == className )
		   	&& ( (*i)->getInstance() == instance ))
			{
				
				lock_.give();
				return true;
			}
		}
	}
	
	
	lock_.give();
	return false;	
}

bool xdaq::ApplicationGroupImpl::hasApplicationDescriptor
(
	const xdaq::ContextDescriptor * context,
	xdata::UnsignedIntegerT localId
) const
{
	lock_.take();

	for (
		std::set<const xdaq::ApplicationDescriptor*>::const_iterator i = this->applicationDescriptors_.begin();
		i != this->applicationDescriptors_.end(); 
		i++ 
	)
	{
		if (( (*i)->getContextDescriptor() == context ) && ( localId == (*i)->getLocalId() ) )
		{
			lock_.give();			
			return true;			
		}
	}	
	
	lock_.give();
	return false;		
}

bool xdaq::ApplicationGroupImpl::hasApplicationDescriptor (const xdaq::ApplicationDescriptor* descriptor) const
{
	lock_.take();
	for (
		std::set<const xdaq::ApplicationDescriptor*>::const_iterator i = this->applicationDescriptors_.begin();
		i != applicationDescriptors_.end(); 
		i++ 
	)
	{
		if ( (*i) == descriptor )
		{
			lock_.give();			
			return true;			
		}
	}	
	lock_.give();
	return false;		
}

const xdaq::ApplicationDescriptor* xdaq::ApplicationGroupImpl::getApplicationDescriptor
(
	const xdaq::ContextDescriptor * context,
	xdata::UnsignedIntegerT localId
) const
	throw (xdaq::exception::ApplicationDescriptorNotFound)
{
	lock_.take();
	for (
		std::set<const xdaq::ApplicationDescriptor*>::const_iterator i = this->applicationDescriptors_.begin();
		i != this->applicationDescriptors_.end(); 
		i++ 
	)
	{
		if (( (*i)->getContextDescriptor() == context ) && ( localId == (*i)->getLocalId() ) )
		{	
			lock_.give();		
			return (*i);			
		}
	}
	
	std::stringstream msg;
	msg << "No descriptor for local id '" << localId << "' found in context '" << context->getURL() << "'";
	lock_.give();
	XCEPT_RAISE(xdaq::exception::ApplicationDescriptorNotFound, msg.str());
	
	return 0; // compiler happy	
}


std::set<const xdaq::ApplicationDescriptor*> xdaq::ApplicationGroupImpl::getApplicationDescriptors
(
	const xdaq::ContextDescriptor * context
) const
{
	lock_.take();
	std::set<const xdaq::ApplicationDescriptor*> selection;
	
	for (
		std::set<const xdaq::ApplicationDescriptor*>::const_iterator i = this->applicationDescriptors_.begin();
		i != applicationDescriptors_.end(); 
		i++ 
	)
	{
		if ( (*i)->getContextDescriptor() == context )
		{
			selection.insert((*i));
		}	
	}
	lock_.give();
	return selection;
}



const xdaq::ApplicationDescriptor* xdaq::ApplicationGroupImpl::getApplicationDescriptor
(
	const std::string & className, 
	xdata::UnsignedIntegerT instance
) const
	throw (xdaq::exception::ApplicationDescriptorNotFound)
{
	lock_.take();
	for (
		std::set<const xdaq::ApplicationDescriptor*>::const_iterator i = this->applicationDescriptors_.begin();
		i != this->applicationDescriptors_.end(); 
		i++ 
	)
	{
		if ((*i)->hasInstanceNumber())
		{
			if (( (*i)->getClassName() == className )
		   	&& ( (*i)->getInstance() == instance ))
			{
				const xdaq::ApplicationDescriptor* d = (*i);
				lock_.give();
				return d;
			}
		}
	}
	
	std::stringstream msg;
	msg << "No descriptor for class '" << className << "', instance '" << instance << "' found";
	lock_.give();
	XCEPT_RAISE(xdaq::exception::ApplicationDescriptorNotFound, msg.str());
	
	return 0;	
}


std::set<const xdaq::ApplicationDescriptor*> xdaq::ApplicationGroupImpl::getApplicationDescriptors
(
	const std::string & className
) const
{
	lock_.take();
	std::set<const xdaq::ApplicationDescriptor*> descriptors;
	
	descriptors.clear();
	
	for (
		std::set<const xdaq::ApplicationDescriptor*>::const_iterator i = this->applicationDescriptors_.begin();
		i != applicationDescriptors_.end(); 
		i++ 
	)
	{
		if ( (*i)->getClassName() == className )
		{
			descriptors.insert((*i));
		}	
	}
	
	lock_.give();
	return descriptors;	
}

std::set<const xdaq::ApplicationDescriptor*> xdaq::ApplicationGroupImpl::getApplicationDescriptors
(
	const std::string & attribute,
	const std::string& value
)  const
{
	lock_.take();
	std::set<const xdaq::ApplicationDescriptor*> descriptors;
	
	descriptors.clear();
	
	for (
		std::set<const xdaq::ApplicationDescriptor*>::const_iterator i = this->applicationDescriptors_.begin();
		i != applicationDescriptors_.end(); 
		i++ 
	)
	{
		if ( (*i)->getAttribute(attribute) == value )
		{
			descriptors.insert((*i));
		}	
	}
	
	lock_.give();
	return descriptors;	
}

std::set<const xdaq::ApplicationDescriptor*> xdaq::ApplicationGroupImpl::getApplicationDescriptors()  const
{
	lock_.take();
	std::set<const xdaq::ApplicationDescriptor*> descriptors;
	
	descriptors.clear();
	
	for (
		std::set<const xdaq::ApplicationDescriptor*>::const_iterator i = this->applicationDescriptors_.begin();
		i != applicationDescriptors_.end(); 
		i++ 
	)
	{
		descriptors.insert((*i));	
	}
	lock_.give();
	return descriptors;	
}


