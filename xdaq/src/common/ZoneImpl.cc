// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/
#include <sstream>

#include "xdaq/ZoneImpl.h"
#include "xdaq/ApplicationGroupImpl.h"

#include "toolbox/string.h"

xdaq::ZoneImpl::ZoneImpl(): mutex_(toolbox::BSem::FULL, true)
{
}

xdaq::ZoneImpl::~ZoneImpl()
{
}

xdaq::ApplicationGroup*  xdaq::ZoneImpl::createApplicationGroup(const std::string & group) 
	throw (xdaq::exception::Exception)
{
	mutex_.take();

	std::map<std::string, xdaq::ApplicationGroup *>::iterator j = groups_.find(group);
	if ( j == groups_.end() )
	{
		this->groups_[group] = new xdaq::ApplicationGroupImpl(group);
	}
	else
	{
		std::string msg = "Group '";
		msg += group;
		msg += "' already existing";
		mutex_.give();
		XCEPT_RAISE(xdaq::exception::Exception, msg );
	}	
	
	xdaq::ApplicationGroup* g = this->groups_[group];
	mutex_.give();
	return g;	
}

const xdaq::ApplicationGroup* xdaq::ZoneImpl::getApplicationGroup(const std::string & group) const
	throw (xdaq::exception::Exception)
{	
	mutex_.take();
	
	std::map<std::string, xdaq::ApplicationGroup *>::const_iterator j = groups_.find(group);
	if ( j != groups_.end() )
	{
		xdaq::ApplicationGroup* g = (*j).second;
		mutex_.give();
		return g;
	}
	else
	{
		std::string msg = "Group '";
		msg += group;
		msg += "' not found";
		mutex_.give();
		XCEPT_RAISE(xdaq::exception::Exception, msg );
	}

	mutex_.give();
}

std::set<std::string> xdaq::ZoneImpl::getGroupNames() const
{
	std::set<std::string> groups;
	mutex_.take();
	
	std::map<std::string, xdaq::ApplicationGroup *>::const_iterator j;
	for (j = groups_.begin(); j != groups_.end(); ++j)
	{
		groups.insert ( (*j).first );	
	}
	
	mutex_.give();
	return groups;	
}

bool xdaq::ZoneImpl::hasApplicationGroup(const std::string & group) const
{
	mutex_.take();
	std::map<std::string, xdaq::ApplicationGroup *>::const_iterator j = groups_.find(group);
	if ( j != groups_.end() )
	{
		mutex_.give();
		return true;
	}
	
	mutex_.give();
	return false;	
}

std::set<const xdaq::ApplicationGroup *> xdaq::ZoneImpl::getGroups() const
	throw (xdaq::exception::Exception)
{
	mutex_.take();

	std::set<const xdaq::ApplicationGroup *> m;

	for ( std::map<std::string, xdaq::ApplicationGroup *>::const_iterator j = groups_.begin(); j != groups_.end(); j++ )
	{
		m.insert((*j).second);
	}	
	mutex_.give();
	return m;
}



std::set<const xdaq::ApplicationDescriptor*> xdaq::ZoneImpl::getApplicationDescriptors (const std::string & className) const
{
	mutex_.take();
	std::set<const xdaq::ApplicationDescriptor*> descriptors;

	for ( std::map<std::string, xdaq::ApplicationGroup *>::const_iterator i = groups_.begin(); i != groups_.end(); i++ )
	{
		std::set<const xdaq::ApplicationDescriptor*>  tmp = (*i).second->getApplicationDescriptors(className);
		descriptors.insert( tmp.begin(), tmp.end() );
	}
	
	mutex_.give();	
	return descriptors;
}		

const xdaq::ApplicationDescriptor* xdaq::ZoneImpl::getApplicationDescriptor( const xdaq::ContextDescriptor * context, xdata::UnsignedIntegerT localId) const
	throw (xdaq::exception::ApplicationDescriptorNotFound)
{
	mutex_.take();
	const xdaq::ApplicationDescriptor* d;
	
	for ( std::map<std::string, xdaq::ApplicationGroup *>::const_iterator i = groups_.begin(); i != groups_.end(); i++ )
	{
		if ((*i).second->hasApplicationDescriptor(context, localId) )
		{
			d = (*i).second->getApplicationDescriptor(context, localId);
			mutex_.give();
			return d;
		}
	}
		
	std::stringstream msg;
	msg << "No descriptor for local id '" << localId << "' found in context '" << context->getURL() << "'";
	mutex_.give();
	XCEPT_RAISE(xdaq::exception::ApplicationDescriptorNotFound, msg.str());
}

const xdaq::ApplicationDescriptor*  xdaq::ZoneImpl::getApplicationDescriptor (const std::string & className, xdata::UnsignedIntegerT instance) const
		throw (xdaq::exception::ApplicationDescriptorNotFound)
{
	mutex_.take();
	const xdaq::ApplicationDescriptor* d;
	
	for ( std::map<std::string, xdaq::ApplicationGroup *>::const_iterator i = groups_.begin(); i != groups_.end(); i++ )
	{
		if ((*i).second->hasApplicationDescriptor(className, instance) )
		{
			d = (*i).second->getApplicationDescriptor(className, instance);
			mutex_.give();
			return d;
		}
	}
	
	std::stringstream msg;
	msg << "No descriptor for className '" << className << "' instance '" << instance << "'";
	mutex_.give();
	XCEPT_RAISE(xdaq::exception::ApplicationDescriptorNotFound, msg.str());
}

std::set<const xdaq::ApplicationDescriptor*> xdaq::ZoneImpl::getApplicationDescriptors (const xdaq::ContextDescriptor * context) const
{
	mutex_.take();
	std::set<const xdaq::ApplicationDescriptor*>  descriptors;
	
	for ( std::map<std::string, xdaq::ApplicationGroup *>::const_iterator i = groups_.begin(); i != groups_.end(); i++ )
	{
		std::set<const xdaq::ApplicationDescriptor*>  tmp = (*i).second->getApplicationDescriptors(context);
		descriptors.insert( tmp.begin(), tmp.end() );	
	}
	
	mutex_.give();	
	return descriptors;

}

std::set<std::string> xdaq::ZoneImpl::getGroupNames(const xdaq::ApplicationDescriptor* descriptor) const
{
	mutex_.take();
	std::set<std::string> names;
	for ( std::map<std::string, xdaq::ApplicationGroup *>::const_iterator i = groups_.begin(); i != groups_.end(); i++ )
	{
		if ((*i).second->hasApplicationDescriptor(descriptor) )
		{
			names.insert( (*i).first );
		}
	}
	
	mutex_.give();
	return names;
}

