// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include <xdaq/NetGroup.h>
#include "toolbox/string.h"

void xdaq::NetGroup::addNetwork
(
	const std::string & name, 
	const std::string & protocol
) 
	throw (xdaq::exception::DuplicateNetwork)
{
	if ( networks_.find(name) == networks_.end() )
	{
		// add network to group
		xdaq::Network* network = new xdaq::Network(name, protocol);
		networks_[name] = network;
	}
	else
	{
		std::string msg = "Network already existing: ";
		msg += name;
		XCEPT_RAISE(xdaq::exception::DuplicateNetwork, msg);
	}
}

bool xdaq::NetGroup::isNetworkExisting (const std::string & name) const
{
	if (networks_.find(name) != networks_.end())
	{
		return true;
	} else
	{
		return false;
	}
}

const xdaq::Network * xdaq::NetGroup::getNetwork(const std::string & name)  const
	throw (xdaq::exception::NoNetwork)
{
	std::map<std::string, xdaq::Network*, std::less<std::string> >::const_iterator i = networks_.find(name);
	if ( i != networks_.end() )
	{
		return (*i).second;
	}
	else
	{
		std::string msg = "Cannot find network: ";
		msg += name;
		XCEPT_RAISE(xdaq::exception::NoNetwork, msg);
	}
}

std::vector<const xdaq::Network*> xdaq::NetGroup::getNetworks() const
{
	std::vector<const xdaq::Network*> retVal;
	std::map<std::string, xdaq::Network*, std::less<std::string> >::const_iterator i;
	for (i = networks_.begin(); i != networks_.end(); i++)
	{
		retVal.push_back( (*i).second );
	}
	return retVal;	
}

xdaq::NetGroup::~NetGroup()
{
	std::map<std::string, xdaq::Network*, std::less<std::string> >::iterator i;
	for (i = networks_.begin(); i != networks_.end(); i++)
	{
		delete (*i).second;
	}	
}

