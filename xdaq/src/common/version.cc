// $Id: version.cc,v 1.3 2008/07/18 15:28:05 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "config/version.h"
#include "xcept/version.h"
#include "toolbox/version.h"
#include "pt/version.h"
#include "xoap/version.h"
#include "log/udpappender/version.h"
#include "log/xmlappender/version.h"
#include "xdata/version.h"
#include "xdaq/version.h"

GETPACKAGEINFO(xdaq)

void xdaq::checkPackageDependencies() throw (config::PackageInfo::VersionException)
{
	CHECKDEPENDENCY(config);
	CHECKDEPENDENCY(xcept);
	CHECKDEPENDENCY(toolbox); 
	CHECKDEPENDENCY(pt); 
	CHECKDEPENDENCY(xoap); 
	CHECKDEPENDENCY(logudpappender); 
	CHECKDEPENDENCY(logxmlappender); 
	CHECKDEPENDENCY(xdata); 
}

std::set<std::string, std::less<std::string> > xdaq::getPackageDependencies()
{
	std::set<std::string, std::less<std::string> > dependencies;

	ADDDEPENDENCY(dependencies,config); 
	ADDDEPENDENCY(dependencies,xcept); 
	ADDDEPENDENCY(dependencies,toolbox); 
	ADDDEPENDENCY(dependencies,pt); 
	ADDDEPENDENCY(dependencies,xoap); 
	ADDDEPENDENCY(dependencies,logudpappender); 
	ADDDEPENDENCY(dependencies,logxmlappender); 
	ADDDEPENDENCY(dependencies,xdata); 
  
	return dependencies;
}	
	
