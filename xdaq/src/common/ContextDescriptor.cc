// $Id: ContextDescriptor.cc,v 1.6 2008/07/18 15:28:05 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xdaq/ContextDescriptor.h"
#include "toolbox/net/URL.h"
#include "toolbox/net/Utils.h"
#include <string>

xdaq::ContextDescriptor::ContextDescriptor(const std::string& url)
{
	url_ = url;
}

std::string xdaq::ContextDescriptor::getURL() const
{
	return url_;
}


bool xdaq::ContextDescriptor::matchURL(const std::string& url) const
	throw (xdaq::exception::InvalidURL)
{
	try
	{
		toolbox::net::URL u1(url);
		toolbox::net::URL u2(url_);
	
		if (u1.getNormalizedURL() == u2.getNormalizedURL())
			return true;
		else
			return false;
	}
	catch (toolbox::net::exception::MalformedURL& e)
	{
		XCEPT_RETHROW (xdaq::exception::InvalidURL, "Failed to compare URLs", e);
	}
	catch (toolbox::net::exception::BadURL& e)
	{
		XCEPT_RETHROW (xdaq::exception::InvalidURL, "Failed to compare URLs", e);
	}
}
