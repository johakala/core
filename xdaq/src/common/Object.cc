// $Id: Object.cc,v 1.2 2008/07/18 15:28:05 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/


#include "xdaq/Object.h"

xdaq::Object::Object(xdaq::Application * owner): owner_(owner)
{

}

xdaq::Object::~Object()
{

}

xdaq::Application * xdaq::Object::getOwnerApplication() const
{
	return owner_;
}
