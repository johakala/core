// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xdaq/MessengerCache.h"
#include <sstream>

xdaq::MessengerCache::MessengerCache (xdaq::ApplicationContext* context): lock_(toolbox::BSem::FULL)
{
	context_ = context;
}

//!
//
void xdaq::MessengerCache::invalidate 
(
	const xdaq::ApplicationDescriptor * from,
	const xdaq::ApplicationDescriptor * to
)
	throw (xdaq::exception::Exception)
{
	lock_.take();
	pt::Messenger* m = messengers_[from][to];
	std::vector<pt::Messenger::Reference>::iterator i = messengerReferences_.begin();
	while (i != messengerReferences_.end())
	{
		if (m == &(*(*i)))
		{
			messengers_[from][to] = 0;
			messengerReferences_.erase(i);		
			lock_.give();
			return;
		}
		i++;
	}
	lock_.give();
	XCEPT_RAISE (xdaq::exception::Exception, "No messenger for invalidation found");
}

pt::Messenger* xdaq::MessengerCache::getMessenger
(
	const xdaq::ApplicationDescriptor * from,
	const xdaq::ApplicationDescriptor * to
)
	throw ( xdaq::exception::MessengerCreationFailed)
{
	
	lock_.take();
	pt::Messenger* m = messengers_[from][to];
	lock_.give();

	if (m == 0)
	{
		lock_.take();
		
		// Now exclusively check if somebody else has created it
		// in the meanwhile
		m = messengers_[from][to];
		if (m == 0)
		{	
			//std::string errorMessage = toolbox::toString("Cannot create messenger from: %d to: %d",from, to);
			// Create a messenger
			std::stringstream errorMessage;
			errorMessage << "Canot create messenger from application class '" << from->getClassName();
			errorMessage << "' in context '" << from->getContextDescriptor()->getURL();
			errorMessage << "' to application class '" << to->getClassName();
			errorMessage << "' in context '" << to->getContextDescriptor()->getURL() << "'";
			
			try 
			{	
				std::string networkName = context_->getRoutingTable()->getNetworkPath(from, to);
			
				// default configured network at which the remote application is reachable			
			
				const xdaq::Network * network =  context_->getNetGroup()->getNetwork(networkName);
			
				pt::Address::Reference localAddress = network->getAddress(from->getContextDescriptor());
			
				//xdaq::ApplicationDescriptor* remoteConfig = context_->getApplicationGroup()->getApplicationDescriptor(to);
				
				pt::Address::Reference remoteAddress = network->getAddress( to->getContextDescriptor() );	
				
				pt::Messenger::Reference mr = pt::getPeerTransportAgent()->getMessenger ( remoteAddress, localAddress );
				messengerReferences_.push_back(mr);
				messengers_[from][to] = &(*mr);
				lock_.give();
				return &(*mr);				
			}
			catch (xdaq::exception::NoRoute & e)
			{
				lock_.give();
				XCEPT_RETHROW(xdaq::exception::MessengerCreationFailed,errorMessage.str(), e);
			}
			catch (xdaq::exception::NoNetwork & e)
			{
				lock_.give();
				XCEPT_RETHROW(xdaq::exception::MessengerCreationFailed, errorMessage.str(), e);
			}
			catch(xdaq::exception::ApplicationDescriptorNotFound & e)
			{
				lock_.give();
				XCEPT_RETHROW(xdaq::exception::MessengerCreationFailed,errorMessage.str(), e);
			}
			catch (xdaq::exception::NoEndpoint & e)
			{
				lock_.give();
				XCEPT_RETHROW(xdaq::exception::MessengerCreationFailed, errorMessage.str(), e);
			}
			catch (pt::exception::NoMessenger & e)
			{
				lock_.give();
				XCEPT_RETHROW(xdaq::exception::MessengerCreationFailed, errorMessage.str(), e);
			}
		}

		lock_.give();

	}

	return m;
}





