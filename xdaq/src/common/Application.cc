// $Id lorsini Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include <string>
#include <typeinfo>

#include "xoap/MessageFactory.h"
#include "xoap/MessageReference.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/domutils.h"

#include "xdata/xdata.h"
#include "xdata/soap/Serializer.h"
#include "xdata/XMLDOM.h"
#include "xdata/InfoSpaceFactory.h"

#include "xdaq/Application.h"
#include "xdaq/ApplicationContext.h"
#include "xdaq/ApplicationStubImpl.h"
#include "xdaq/exception/Exception.h"
#include "xdaq/exception/ParameterSetFailed.h"
#include "xdaq/exception/CommandStateMismatch.h"
#include "xdaq/exception/MultipleCommandsInSOAP.h"
#include "xdaq/exception/MissingCommandInSOAP.h"
#include "xoap/Method.h"
#include "xdaq/ApplicationDescriptorImpl.h"
#include "xdaq/NamespaceURI.h"

#include "xgi/Method.h"
#include "xgi/Utils.h"

#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"

#include "toolbox/utils.h"
#include "toolbox/exception/Processor.h"


xdaq::Application::~Application()
{
}

xdaq::Application::Application(xdaq::ApplicationStub * stub) 
	throw (xdaq::exception::Exception)
	:stub_(stub)
{
	xoap::bind(this, &Application::ParameterSet, "ParameterSet", XDAQ_NS_URI );
	xoap::bind(this, &Application::ParameterGet, "ParameterGet", XDAQ_NS_URI);
	xoap::bind(this, &Application::ParameterQuery, "ParameterQuery", XDAQ_NS_URI);
	xoap::bind(this, &Application::InterfaceQuery, "InterfaceQuery", XDAQ_NS_URI);
	
	context_ = this->stub_->getContext();
}


xdata::InfoSpace* xdaq::Application::getApplicationInfoSpace() const
{
	return this->stub_->getInfoSpace();
}

const xdaq::ApplicationDescriptor * xdaq::Application::getApplicationDescriptor() const
{
	return this->stub_->getDescriptor();
}

xdaq::ApplicationDescriptor * xdaq::Application::editApplicationDescriptor() const
{
	return this->stub_->getDescriptor();
}

Logger& xdaq::Application::getApplicationLogger() const
{
	return static_cast<xdaq::ApplicationStubImpl*>(this->stub_.get())->getLogger();
}

xdaq::ApplicationContext* xdaq::Application::getApplicationContext() const
{
	return this->context_;
}

void xdaq::Application::setDefaultValues (DOMNode* funcNode) 
	throw (xdaq::exception::ParameterSetFailed)
{
	try
	{
		xdata::soap::Serializer serializer;
		serializer.import(getApplicationInfoSpace(),funcNode);
		xdata::Event e("urn:xdaq-event:setDefaultValues", this);

		this->getApplicationInfoSpace()->fireEvent(e);
	} 
	catch (xdata::exception::Exception& imf)
	{
		XCEPT_RETHROW (xdaq::exception::ParameterSetFailed, "set default values failed", imf);
	}
}


xoap::MessageReference xdaq::Application::ParameterQuery 
(
	xoap::MessageReference message
) 
	throw (xoap::exception::Exception)
{	
	xoap::MessageReference reply = xoap::createMessage();
	xoap::SOAPEnvelope envelope = reply->getSOAPPart().getEnvelope();
	xoap::SOAPBody b = envelope.getBody();
	xoap::SOAPName responseName = envelope.createName("ParameterQueryResponse", "xdaq", XDAQ_NS_URI);
	xoap::SOAPBodyElement e = b.addBodyElement ( responseName );
	
	xdata::soap::Serializer serializer;
	
	const xdaq::ApplicationDescriptorImpl * d =  dynamic_cast<const xdaq::ApplicationDescriptorImpl*>(getApplicationDescriptor());
	std::string applicationNamespaceURI = toolbox::toString("urn:xdaq-application:%s",d->getClassName().c_str());
	
	DOMElement * properties =
		e.getDOMNode()->getOwnerDocument()->createElementNS(xoap::XStr(applicationNamespaceURI),xoap::XStr("properties"));

	// Cannot use classname since it may contain namespaces
	//properties->setPrefix (xoap::XStr(d->getClassName()));
	properties->setPrefix (xoap::XStr("p"));
	
	std::string nsprefix = "xmlns:p";
	//nsprefix += d->getClassName(); cannot work with scope in class name

	properties->setAttributeNS(xoap::XStr("http://www.w3.org/2000/xmlns/"), xoap::XStr(nsprefix), xoap::XStr(applicationNamespaceURI));
	
	try
	{
		xdata::Event e("urn:xdaq-event:ParameterQuery", this);
		this->getApplicationInfoSpace()->fireEvent(e);
		serializer.exportAll (this->getApplicationInfoSpace(), properties , true );
	}
	catch (xdata::exception::Exception & xde)
	{
		XCEPT_RETHROW (xoap::exception::Exception, "Parameter export failed in ParameterQuery", xde);
	}
	
	e.getDOMNode()->appendChild(properties);
	return reply;
}

xoap::MessageReference xdaq::Application::ParameterSet 
(
	xoap::MessageReference message
) 
	throw (xoap::exception::Exception)
{	
	const xdaq::ApplicationDescriptorImpl * d =  dynamic_cast<const xdaq::ApplicationDescriptorImpl*>(this->getApplicationDescriptor());
	xoap::MessageReference reply = xoap::createMessage();
	xoap::SOAPEnvelope envelope = reply->getSOAPPart().getEnvelope();
	xoap::SOAPBody b = envelope.getBody();
	xoap::SOAPName responseName = envelope.createName("ParameterSetResponse", "xdaq", XDAQ_NS_URI);
	b.addBodyElement ( responseName );
	
	//extract command <ParameterSet> from SOAPMessage
	DOMNode* node  = message->getSOAPPart().getEnvelope().getBody().getDOMNode();
	DOMNodeList* bodyList = node->getOwnerDocument()->getElementsByTagNameNS(xoap::XStr("*"), xoap::XStr("ParameterSet"));
	
	// Only check for > 1. One <ParameterSet> must at least be present,
	// otherwise this function would not have been dispatched.
	if ( bodyList->getLength() > 1 ) 
	{
		XCEPT_RAISE (xoap::exception::Exception, "Multiple '<ParameterSet>' command tags are not supported.");
	}
	
	DOMNode* funcNode = bodyList->item(0); // this is the ParameterSet node
	DOMNode* newNode = (DOMNode*) 0;

	DOMNodeList* childList = funcNode->getChildNodes();
	size_t i;
	
	// Only one child allowed. Return after the first processed child.
	//
	for ( i = 0; i < childList->getLength(); i++)
	{	
		newNode = childList->item(i);	
		if ( newNode->getNodeType() == DOMNode::ELEMENT_NODE) 
		{
			std::string applicationNamespaceURI = toolbox::toString("urn:xdaq-application:%s",d->getClassName().c_str());
			std::string receivedNamespaceURI = xoap::XMLCh2String(newNode->getNamespaceURI());
			
			if ( applicationNamespaceURI != receivedNamespaceURI )
			{
				std::string msg = toolbox::toString("Invalid or missing namespace '%s' in properties section, expected [%s]",receivedNamespaceURI.c_str(), applicationNamespaceURI.c_str());
				XCEPT_RAISE (xoap::exception::Exception, msg);			
			}
			
			xdata::soap::Serializer serializer;			
			try
			{
				serializer.import(getApplicationInfoSpace(),newNode);

				xdata::Event e("urn:xdaq-event:ParameterSet", this);
				this->getApplicationInfoSpace()->fireEvent(e);
			}
			catch (xdata::exception::Exception & xde)
			{
				XCEPT_RETHROW (xoap::exception::Exception, "Parameter import failed in ParameterSet", xde);
			}
			return reply;
		}	
	        	
	} 
	
	XCEPT_RAISE (xoap::exception::Exception, "Missing data arguments in ParameterSet");
}


xoap::MessageReference xdaq::Application::InterfaceQuery 
(
	xoap::MessageReference message

) 
	throw (xoap::exception::Exception)
{
	LOG4CPLUS_DEBUG (getApplicationLogger(), "InterfaceQuery");

	xoap::MessageReference reply = xoap::createMessage();
	xoap::SOAPEnvelope envelope = reply->getSOAPPart().getEnvelope();
	xoap::SOAPBody b = envelope.getBody();
	xoap::SOAPName responseName = envelope.createName("InterfaceQueryResponse", "xdaq", XDAQ_NS_URI);
	xoap::SOAPBodyElement e = b.addBodyElement ( responseName );
	xoap::SOAPName interfaceElementName = envelope.createName("Interface", "xdaq", XDAQ_NS_URI);
	xoap::SOAPElement interfaceElement =  e.addChildElement(interfaceElementName);
			
	std::vector<toolbox::lang::Method*> methods = this->getMethods();
	for (size_t i = 0; i < methods.size(); i++)
	{
		// Add only those commands that can be called via the soap protocol
		if (methods[i]->type() == "soap")
		{
			std::string methodName = dynamic_cast<xoap::MethodSignature*>( methods[i] )->name();
			xoap::SOAPName methodElementName = envelope.createName("Method", "xdaq", XDAQ_NS_URI);
			xoap::SOAPElement methodElement = interfaceElement.addChildElement (methodElementName);			
			methodElement.addTextNode (methodName);
		}
	}

	return reply;
}


xoap::MessageReference xdaq::Application::ParameterGet 
(
	xoap::MessageReference message
) 	
	throw (xoap::exception::Exception)
{
	const xdaq::ApplicationDescriptorImpl * d =  dynamic_cast<const xdaq::ApplicationDescriptorImpl*>(this->getApplicationDescriptor());
	xoap::MessageReference reply = xoap::createMessage();
	xoap::SOAPEnvelope envelope = reply->getSOAPPart().getEnvelope();
	xoap::SOAPBody b = envelope.getBody();
	xoap::SOAPName responseName = envelope.createName("ParameterGetResponse", "xdaq", XDAQ_NS_URI);
	xoap::SOAPBodyElement e = b.addBodyElement ( responseName );
	
	//extract command from SOAPMessage
	DOMNode* node  = message->getSOAPPart().getEnvelope().getBody().getDOMNode();
	DOMNodeList* bodyList = node->getOwnerDocument()->getElementsByTagNameNS (xoap::XStr("*"), xoap::XStr("ParameterGet"));
	
	// Create result node
	std::string applicationNamespaceURI = toolbox::toString("urn:xdaq-application:%s",d->getClassName().c_str());
	
	DOMElement * resultNode = e.getDOMNode()->getOwnerDocument()->createElementNS(xoap::XStr(applicationNamespaceURI),xoap::XStr("properties"));
	
	// Use a safe prefix, namespace may be NS1::NS2::Class
	// resultNode->setPrefix (xoap::XStr(d->getClassName()));
	resultNode->setPrefix (xoap::XStr("p"));
	
	std::string nsprefix = "xmlns:p";
	//nsprefix += d->getClassName(); cannot work with c++ namespaces
	resultNode->setAttributeNS(xoap::XStr("http://www.w3.org/2000/xmlns/"), xoap::XStr(nsprefix), xoap::XStr(applicationNamespaceURI));


	e.getDOMNode()->appendChild(resultNode);
	
	// Only check if <ParameterGet> tag is there more than once.
	// If it was not present, this function would not even have been dispatched.
	if ( bodyList->getLength() > 1 ) 
	{
		XCEPT_RAISE (xoap::exception::Exception, "Multiple <ParameterGet> command tags are not supported.");
	}
	
	DOMNode * queryNode = 0;
	DOMNode * funcNode = bodyList->item(0);
	DOMNodeList * childList = funcNode->getChildNodes();
	
	// Childlist is expected to contain only one element,
	// the root parameter bag.
	//
	for (size_t i = 0; i < childList->getLength(); i++)
	{
		queryNode = childList->item(i);
		if ( queryNode->getNodeType() == DOMNode::ELEMENT_NODE) 
		{
			std::string applicationNamespaceURI = toolbox::toString("urn:xdaq-application:%s",d->getClassName().c_str());
			std::string receivedNamespaceURI = xoap::XMLCh2String(queryNode->getNamespaceURI());
			
			if ( applicationNamespaceURI != receivedNamespaceURI )
			{
				std::string msg = toolbox::toString("Missing or invalid namespace '%s' in properties section, expected [%s]",receivedNamespaceURI.c_str(), applicationNamespaceURI.c_str());
				XCEPT_RAISE (xoap::exception::Exception, msg);
			
			}
			//GUTLEBER:
			// Not clear to me, if with new API we still need a deep copy or
			// cannot maybe re-used the received DOM tree for filling in the
			// parameter values.
			//
			//DOMNode* n = newNode->cloneNode(true); // true means deep clone
				
			xdata::soap::Serializer serializer;
			
			try
			{
				xdata::Event e("urn:xdaq-event:ParameterGet", this);
				this->getApplicationInfoSpace()->fireEvent(e);
				//serializer.exportQualified(getApplicationInfoSpace(),n);
				serializer.exportQualified (getApplicationInfoSpace(), queryNode, resultNode, true);
			} 
			catch (xdata::exception::Exception & xde)
			{
				XCEPT_RETHROW (xoap::exception::Exception, "Failed to serialize application parameters", xde);
			}
			
			// e.addChildElement(n);			
			return reply;
		}	
	}
	
	XCEPT_RAISE (xoap::exception::Exception, "Missing properties arguments in ParameterGet command");
}


toolbox::net::URN xdaq::Application::createQualifiedInfoSpace(const std::string & nid ) throw (xdaq::exception::Exception )
{
	//
	try
	{
		std::stringstream id;
		toolbox::net::UUID uuid;
		id << "uuid=" << uuid.toString();  
		toolbox::net::URN urn (nid,id.str());
		
		xdata::InfoSpace * is =  xdata::getInfoSpaceFactory()->create(urn.toString());
		//is->fireItemAvailable("_descriptor", dynamic_cast<xdaq::ApplicationDescriptorImpl*>(this->getApplicationDescriptor()) );
		is->fireItemAvailable("_descriptor", dynamic_cast<xdaq::ApplicationDescriptorImpl*>(this->stub_->getDescriptor()) );
		return urn;
	}
	catch (toolbox::net::exception::MalformedURN & e )
	{
			XCEPT_RETHROW(xdaq::exception::Exception, "failed to create infospace", e );
	}	
	catch (xdata::exception::Exception & e )
	{
			XCEPT_RETHROW(xdaq::exception::Exception, "failed to create infospace", e );
	}

}

void xdaq::Application::notifyQualified( std::string severity, xcept::Exception & e  )
{
	this->qualifyException(severity,e);
	
	toolbox::exception::getProcessor()->push(e,0,0);

} 
		
void xdaq::Application::qualifyException( std::string severity, xcept::Exception & e  )
{
	std::stringstream  url;
	url << this->getApplicationDescriptor()->getContextDescriptor()->getURL() << "/" << this->getApplicationDescriptor()->getURN();

	// Mandatory fields	
	e.setProperty("notifier", url.str());
	e.setProperty("sessionID", this->getApplicationContext()->getSessionId() );
	e.setProperty("qualifiedErrorSchemaURI","http://xdaq.web.cern.ch/xdaq/xsd/2005/QualifiedSoftwareErrorRecord-10.xsd" );
	e.setProperty("severity", severity);

	// Optional fields
	e.setProperty("urn:xdaq-application:service",this->getApplicationDescriptor()->getAttribute("service"));
	std::set<std::string> zones = this->getApplicationContext()->getZoneNames();
	e.setProperty("urn:xdaq-application:zone", toolbox::printTokenSet(zones,"," ));
	e.setProperty("urn:xdaq-application:group", this->getApplicationDescriptor()->getAttribute("group"));
	e.setProperty("urn:xdaq-application:uuid", this->getApplicationDescriptor()->getAttribute("uuid"));
	e.setProperty("urn:xdaq-application:class",this->getApplicationDescriptor()->getClassName());
	e.setProperty("urn:xdaq-application:id",this->getApplicationDescriptor()->getAttribute("id"));
	e.setProperty("urn:xdaq-application:context",this->getApplicationDescriptor()->getContextDescriptor()->getURL());

	if (this->getApplicationDescriptor()->hasInstanceNumber())
 	{	
		e.setProperty("urn:xdaq-application:instance", this->getApplicationDescriptor()->getAttribute("instance"));
	}
	else
	{
		e.setProperty("urn:xdaq-application:instance", "");
	}
	
} 
