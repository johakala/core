// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include <map>
#include <string>
#include <sstream>

#include "xdaq/Zone.h"
#include "xdaq/ApplicationDescriptorFactory.h"
#include "xdaq/ApplicationGroupImpl.h"
#include "xdaq/ApplicationDescriptorImpl.h"

xdaq::ApplicationDescriptorFactory * xdaq::ApplicationDescriptorFactory::instance_ = 0;

xdaq::ApplicationDescriptorFactory::ApplicationDescriptorFactory(): mutex_(toolbox::BSem::FULL,true)
{
	
}

xdaq::ApplicationDescriptorFactory* xdaq::ApplicationDescriptorFactory::getInstance()
{
	if (instance_ == 0)
	{
		instance_ = new xdaq::ApplicationDescriptorFactory();	
	}
	return instance_;
}


bool xdaq::ApplicationDescriptorFactory::hasDescriptor(xdaq::ApplicationDescriptor* descriptor)
{
	return (descriptors_.find(static_cast<xdaq::ApplicationDescriptorImpl*>(descriptor)) != descriptors_.end());
}
	
void xdaq::ApplicationDescriptorFactory::lock()
{
	mutex_.take();	
}

void xdaq::ApplicationDescriptorFactory::unlock()
{
	mutex_.give();
}



xdaq::ApplicationDescriptorImpl* xdaq::ApplicationDescriptorFactory::createApplicationDescriptor 
(
	const xdaq::ContextDescriptor* context,
	const std::string& className, 
	xdata::UnsignedIntegerT localId,
	std::set<std::string>& zones,
	std::set<std::string>& groups
)
	throw (xdaq::exception::DuplicateApplicationDescriptor, xdaq::exception::InvalidZone)
{
	

	mutex_.take();
	
	std::set<std::string> userZones;
	if (zones.empty())
	{
		mutex_.give();
		XCEPT_RAISE (xdaq::exception::InvalidZone,"Empty zone set specifier");
	}
	else
	{
		userZones = zones;	
	}
	
	// zones_ has all "allowed" zones	
	// Attempt to add descriptor to requested zones. If a zone doesn't match configured zoneNames -> throw
	for (std::set<std::string>::iterator zi = zones.begin(); zi != zones.end(); ++zi)
	{
		if (zones_.find( *zi ) == zones_.end())
		{
			std::stringstream msg;
			msg << "Invalid zone '" << *zi << "'";
			mutex_.give();
			XCEPT_RAISE (xdaq::exception::InvalidZone, msg.str());			
		}
	}
	
	// Re-check if the descriptor exists already in ANY of the local zones or groups
	std::set<std::string> allowedZones;
	for ( std::map<std::string, xdaq::ZoneImpl*>::iterator j = zones_.begin(); j != zones_.end(); j++ )
	{
		allowedZones.insert( (*j).first );
		std::set<const xdaq::ApplicationGroup *>  groupsSet = (*j).second->getGroups();
		
		for (std::set< const xdaq::ApplicationGroup *>::iterator i =  groupsSet.begin(); i != groupsSet.end(); i++ )
		{
			
			const xdaq::ApplicationGroupImpl* g = dynamic_cast<const xdaq::ApplicationGroupImpl*>(*i);
			if ( g->hasApplicationDescriptor(context, localId ) )
			{
				std::stringstream msg;
				msg << "Application descriptor already existing in context '" << context->getURL() << "', className '";
				msg << className << "', local id '" << localId << "'";
				mutex_.give();
				XCEPT_RAISE (xdaq::exception::DuplicateApplicationDescriptor, msg.str());
			}	
		}			
	}
	
	std::set<std::string> userGroups;
	if (groups.empty())
	{
		// if no group provided by user, put in the "default" group only
		userGroups.insert("default");
	}
	else
	{
		userGroups = groups;	
	}
	
	
	std::string groupString = "";
	std::set<std::string>::iterator i =  userGroups.begin();
	do 
	{
		groupString += (*i);
		++i;
		if ( i != userGroups.end() ) groupString += ",";	
	} while ( i != 	userGroups.end() );
	
	// createDescriptor locks recursively the descriptorFactory lock
	xdaq::ApplicationDescriptorImpl* d = new xdaq::ApplicationDescriptorImpl(context, className, localId,groupString);
	descriptors_.insert(d);
		
	// Create descriptor in all zones and all groups		
	for ( std::set<std::string>::iterator j = userZones.begin(); j != userZones.end(); j++ )
	{
		for (std::set<std::string>::iterator k = userGroups.begin(); k != userGroups.end(); k++ )
		{		
			xdaq::ZoneImpl* z = zones_[(*j)];
			
			if ( z->hasApplicationGroup(*k) )
			{	
				xdaq::ApplicationGroup * g = const_cast<xdaq::ApplicationGroup*>(z->getApplicationGroup(*k)); // editable group
				dynamic_cast<xdaq::ApplicationGroupImpl*>(g)->addApplicationDescriptor(d);
			}
			else
			{
				dynamic_cast<xdaq::ApplicationGroupImpl*>(z->createApplicationGroup(*k))->addApplicationDescriptor(d);
			}
		}
	}
	mutex_.give();	
	return d;		
}


	
void xdaq::ApplicationDescriptorFactory::destroyDescriptor (xdaq::ApplicationDescriptor* descriptor)
	throw (xdaq::exception::ApplicationDescriptorNotFound)
{
	mutex_.take();
	// Look if the descriptor is valid, then destroy
	std::set<xdaq::ApplicationDescriptorImpl*>::iterator i = descriptors_.find(static_cast<xdaq::ApplicationDescriptorImpl*>(descriptor));
	if (i != descriptors_.end())
	{
		delete (*i);
		descriptors_.erase(i);
		mutex_.give();
	}
	else
	{
		mutex_.give();
		XCEPT_RAISE (xdaq::exception::ApplicationDescriptorNotFound, "Invalid descriptor object");	
	}
}

xdaq::ZoneImpl * xdaq::ApplicationDescriptorFactory::createZone(const std::string & name ) 
			throw ( xdaq::exception::InvalidZone)
{
	mutex_.take();
	std::map<std::string, xdaq::ZoneImpl*>::iterator i = zones_.find(name);
	
	if ( i != zones_.end() )
	{
		mutex_.give();
		XCEPT_RAISE (xdaq::exception::InvalidZone, "already existing zone");	
	
	}
	else
	{
		xdaq::ZoneImpl * zone = new ZoneImpl();
		zones_[name] = zone;
		mutex_.give();
		return zone;
	}
	
		
}

std::set<std::string> xdaq::ApplicationDescriptorFactory::getZoneNames ()
{
	std::set<std::string> zoneNames;
	mutex_.take();
	for (std::map<std::string, xdaq::ZoneImpl*>::iterator i = zones_.begin(); i != zones_.end(); i++ )
	{
		zoneNames.insert((*i).first);
	}
	mutex_.give();
	return zoneNames; 
}			

xdaq::Zone * xdaq::ApplicationDescriptorFactory::getZone(const std::string & name ) 
			throw ( xdaq::exception::InvalidZone)
{
	mutex_.take();
	std::map<std::string, xdaq::ZoneImpl*>::iterator i = zones_.find(name);
	
	if ( i == zones_.end() )
	{
		mutex_.give();
		XCEPT_RAISE (xdaq::exception::InvalidZone, "zone not found");	
	
	}
	else
	{
		xdaq::Zone * zone = zones_[name];
		mutex_.give();
		return zone;
	}

}			
