// $Id: WebApplication.cc,v 1.15 2008/07/18 15:28:05 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xdaq/WebApplication.h"

#include "xdata/xdata.h"
#include "xdata/soap/Serializer.h"
#include "xdata/XMLDOM.h"

#include "xgi/Method.h"

xdaq::WebApplication::~WebApplication()
{
}

xdaq::WebApplication::WebApplication (xdaq::ApplicationStub * stub) throw (xdaq::exception::Exception)
        : xdaq::Application(stub), xgi::framework::UIManager(this)
{
	// Bind a default HTTP callback
	xgi::bind(this, &WebApplication::ParameterQuery, "ParameterQuery");
	xgi::bind(this, &WebApplication::Empty, "null");
}

void xdaq::WebApplication::Empty (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{

}

void xdaq::WebApplication::ParameterQuery (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	out->getHTTPResponseHeader().addHeader("Content-Type", "text/xml");

	DOMImplementation* impl = DOMImplementationRegistry::getDOMImplementation(xdata::XStr("Core"));
	DOMDocument* exportDocument = impl->createDocument(xdata::XStr(""), xdata::XStr("root"), 0);
	DOMElement* node = exportDocument->createElementNS(xdata::XStr("any"), xdata::XStr("Properties"));
	node->setAttribute(xdata::XStr("xmlns"), xdata::XStr("any"));
	xdata::soap::Serializer serializer;
	serializer.exportAll(getApplicationInfoSpace(), node, true);
	std::string xml = "";
	xdata::XMLDOMSerializer s2(xml);
	s2.serialize(node);
	*out << xml << std::endl;
}
