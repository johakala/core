#include "xdaq/soap/utils.h"

#include "xcept/tools.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPHeader.h"
#include "xoap/domutils.h"
#include "pt/PeerTransportAgent.h"
#include "pt/SOAPMessenger.h"

xoap::MessageReference xdaq::soap::sendMessage(xoap::MessageReference msg,  const std::string & url, const std::string & urn) 
			throw (xdaq::exception::Exception)
{
	pt::PeerTransportAgent* pta = pt::getPeerTransportAgent();
	
	try
	{
		// Currently the addresses are created, but not deleted.
		// Either the messenger has to take care of that or the addresses need to be reference counted
		pt::Address::Reference destAddress = pta->createAddress(url, "soap");
		pt::Address::Reference localAddress = pt::getPeerTransportAgent()->createAddress("http://localhost", "soap");

		// These two lines cannot be merges, since a reference that is a temporary object
		// would delete the contained object pointer immediately after use.
		//
		pt::Messenger::Reference mr = pta->getMessenger(destAddress,localAddress);
		pt::SOAPMessenger& m = dynamic_cast<pt::SOAPMessenger&>(*mr);

		// fill the SOAPAction field
		msg->getMimeHeaders()->setHeader("SOAPAction", urn);
		
		return m.send (msg);
		
		
	}
	catch (pt::exception::Exception& pte)
	{	
		XCEPT_RETHROW(xdaq::exception::Exception, "failed to relay message", pte);
	}	

}
