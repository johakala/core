// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xdaq/soap/MessengerCache.h"
#include <sstream>

xdaq::soap::MessengerCache::MessengerCache (): lock_(toolbox::BSem::FULL)
{
}

//!
//
void xdaq::soap::MessengerCache::invalidate 
(
	const xdaq::ApplicationDescriptor & from,
	const xdaq::ApplicationDescriptor & to
)
	throw (xdaq::exception::Exception)
{

	std::stringstream errorMessage;
	errorMessage << "Canot invalidate messenger from application class '" << from.getURN();
	errorMessage << "' in context '" << from.getContextDescriptor()->getURL();
	errorMessage << "' to application class '" << to.getURN();
	errorMessage << "' in context '" << to.getContextDescriptor()->getURL() << "'";
	
	lock_.take();
	std::map<toolbox::net::UUID, std::map<toolbox::net::UUID, pt::Messenger::Reference> >::iterator i;
	i = messengers_.find(from.getUUID());
	if ( i != messengers_.end() )
	{
		std::map<toolbox::net::UUID, pt::Messenger::Reference>::iterator j;
		j = (*i).second.find(to.getUUID());
		if ( j !=  (*i).second.end() )
		{
			(*i).second.erase(j);
			lock_.give();
			return;
		} 
		else
		{
			lock_.give();
			XCEPT_RAISE (xdaq::exception::Exception, errorMessage.str());
		}
	
	}
	else
	{
		lock_.give();
		XCEPT_RAISE (xdaq::exception::Exception,  errorMessage.str());
	
	}
	
}

pt::Messenger::Reference xdaq::soap::MessengerCache::getMessenger
(
	const xdaq::ApplicationDescriptor & from,
	const xdaq::ApplicationDescriptor & to
) 
	throw ( xdaq::exception::MessengerCreationFailed)
{
	
	lock_.take();
	
	std::map<toolbox::net::UUID, std::map<toolbox::net::UUID, pt::Messenger::Reference> >::iterator i;
	i = messengers_.find(from.getUUID());
	if ( i != messengers_.end() )
	{
		std::map<toolbox::net::UUID, pt::Messenger::Reference>::iterator j;
		j = (*i).second.find(to.getUUID());
		if ( j !=  (*i).second.end() )
		{
			lock_.give();
			return (*j).second;
		}
	}
		
	
	//std::string errorMessage = toolbox::toString("Cannot create messenger from: %d to: %d",from, to);
	// Create a messenger
	std::stringstream errorMessage;
	errorMessage << "Canot create messenger from application class '" << from.getURN();
	errorMessage << "' in context '" << from.getContextDescriptor()->getURL();
	errorMessage << "' to application class '" << to.getURN();
	errorMessage << "' in context '" << to.getContextDescriptor()->getURL() << "'";

	try 
	{	

		std::stringstream path;
		path << to.getContextDescriptor()->getURL(); 
		if (to.getAttribute("path") != "" )
		{
			path << to.getAttribute("path"); 
		}
		else
		{
			path << "/" <<  to.getURN();
		}
		
		pt::Address::Reference remoteAddress = 
			pt::getPeerTransportAgent()->createAddress(path.str(),"soap");

		pt::Address::Reference localAddress = 
			pt::getPeerTransportAgent()->createAddress(from.getContextDescriptor()->getURL(),"soap");


		pt::Messenger::Reference mr = pt::getPeerTransportAgent()->getMessenger ( remoteAddress, localAddress );
		messengers_[from.getUUID()][to.getUUID()] = mr;
		lock_.give();
		return mr;				
	}
	catch (pt::exception::InvalidAddress & e)
	{
		lock_.give();

		XCEPT_RETHROW(xdaq::exception::MessengerCreationFailed, errorMessage.str(), e);
	}
	catch (pt::exception::NoMessenger & e)
	{
		lock_.give();

		XCEPT_RETHROW(xdaq::exception::MessengerCreationFailed, errorMessage.str(), e);
	}

}





