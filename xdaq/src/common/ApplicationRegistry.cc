// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include <sstream>
#include <memory>
#include "xdaq/ApplicationRegistry.h"
#include "xdaq/ApplicationGroup.h"
#include "xdaq/ApplicationStubImpl.h"
#include "toolbox/string.h"
#include "toolbox/lang/RTTI.h"
#include "xdata/UnsignedInteger.h"

#include "log4cplus/logger.h"
#include "log4cplus/loglevel.h"
#include "log4cplus/ndc.h"
#include "log4cplus/consoleappender.h"
#include "log4cplus/nullappender.h"
#include "log4cplus/fileappender.h"
#include "log4cplus/helpers/appenderattachableimpl.h"
#include "log4cplus/helpers/loglog.h"
#include "log4cplus/helpers/pointer.h"
#include "log4cplus/spi/loggingevent.h"
#include "log4cplus/layout.h"
#include "log/udpappender/UDPAppender.h"
#include "log/xmlappender/XMLAppender.h"

using namespace log4cplus;
using namespace log4cplus::helpers;
using namespace log4cplus::spi;


xdaq::ApplicationRegistry::ApplicationRegistry (xdaq::ApplicationContext* context)
{
	this->context_ = context;
}

xdaq::Application* xdaq::ApplicationRegistry::instantiateApplication (xdaq::ApplicationDescriptor* descriptor)
	throw (xdaq::exception::ApplicationInstantiationFailed )
{
	std::string pattern = "%d{%d %b %Y %H:%M:%S.%q} [%t] %-5p %c <%x";

        
	if (this->context_->getSessionId()  != "")
	{
		pattern += "<sid>";
		pattern += this->context_->getSessionId();
		pattern += "</sid>";
	}
	pattern += "> - %m%n";

	// Define a function pointer for the "instantiate_className" functions that are found in loaded modules
	//
	//typedef xdaq::Application* (*ApplicationInstantiatorFunctionType)(xdaq::ApplicationStub*);	
	//typedef xdaq::Application* (*ApplicationInstantiatorFunctionTypeWithThrow)(xdaq::ApplicationStub*) throw (xdaq::exception::Exception);
	
	xdata::UnsignedIntegerT lid = descriptor->getLocalId();
			
	if (this->applications_.find(lid) != this->applications_.end())
	{		
		std::stringstream msg;
		msg << "Internal error. Application class ";
		msg << descriptor->getClassName();
		msg << " already instantiated with local id " << lid;
		XCEPT_RAISE(xdaq::exception::ApplicationInstantiationFailed, msg.str());
	}	
		
	std::string applicationInstantiatorFunction = "";
	// first check simple instantiator
	try
	{
		//ApplicationInstantiatorFunctionType instantiator;
		xdaq::Application* (*instantiator)(xdaq::ApplicationStub*);
		applicationInstantiatorFunction = "instantiate_";
		applicationInstantiatorFunction += descriptor->getClassName();

		//instantiator = (ApplicationInstantiatorFunctionType) context_->getSharedObjectRegistry()->lookup ( applicationInstantiatorFunction );
		instantiator = (xdaq::Application* (*)(xdaq::ApplicationStub*))context_->getSharedObjectRegistry()->lookup ( applicationInstantiatorFunction );
		try 
		{
			// See Support Request [1246650]
   			// Logger l = Logger::getInstance( descriptor->getURN() );
			std::string appId;
			if (descriptor->hasInstanceNumber())
			{
				appId = "instance(";
				appId += descriptor->getAttribute("instance");
				appId += ")";
			}
			else
			{
				appId = "lid(";
				appId += descriptor->getAttribute("id");
				appId += ")";
			}
			

			Logger l = Logger::getInstance(context_->getLogger().getName() + "." + descriptor->getClassName() + "." + appId);

			if ( descriptor->getAttribute("logpolicy") != "inherit")
			{
				l.removeAllAppenders();
				//Appender* newAppender = new ConsoleAppender(true, true);
				SharedAppenderPtr newAppender ( new ConsoleAppender(true, true));
				newAppender->setName ("console");
 				newAppender->setLayout(std::auto_ptr<Layout>(new PatternLayout(pattern)));
				l.setAdditivity(false);
				l.addAppender(newAppender);
			}

			std::string loglevel = descriptor->getAttribute("loglevel");
			if ( loglevel != "")
			{
				LogLevelManager& llm = getLogLevelManager();

				LogLevel level = llm.fromString (loglevel);
				if (level != NOT_SET_LOG_LEVEL)
				{
					l.setLogLevel (level);
				}
				else
				{
					std::string msg = "Log level ";
					msg += loglevel;
					msg += " is invalid";
					XCEPT_RAISE (xdaq::exception::Exception, msg);
				}
			}
			
			xdaq::ApplicationStub* stub = new xdaq::ApplicationStubImpl(context_,descriptor,l);
			xdaq::Application* application = (*instantiator)(stub);
			applications_[lid] = application;
			return application;
		}
		catch (xdaq::exception::Exception & e)
		{
			std::stringstream msg;
			msg << "Failed to instantiate object for class ";
			msg << descriptor->getClassName() << " in context ";
			msg << descriptor->getContextDescriptor()->getURL();
			
			XCEPT_RETHROW(xdaq::exception::ApplicationInstantiationFailed, msg.str(), e);
		}
		catch (std::exception & e )
		{
			std::stringstream msg;
			msg << "Failed to instantiate object for class ";
			msg << descriptor->getClassName() << " in context ";
			msg << descriptor->getContextDescriptor()->getURL();
			msg << ", std exception in user function: ";
			msg << e.what();
			
			XCEPT_RAISE(xdaq::exception::ApplicationInstantiationFailed, msg.str());
		}
		catch (...)
		{
			std::stringstream msg;
			msg << "Failed to instantiate object for class ";
			msg << descriptor->getClassName() << " in context ";
			msg << descriptor->getContextDescriptor()->getURL();
			msg << ", unknown exception in user function";
			
			XCEPT_RAISE(xdaq::exception::ApplicationInstantiationFailed, msg.str());
		}
	}
	catch (xdaq::exception::SymbolLookupFailed & sluf)
	{
		// check for namespace instantiator
		try
		{
			//ApplicationInstantiatorFunctionTypeWithThrow instantiator;
			xdaq::Application* (*instantiator)(xdaq::ApplicationStub*) throw (xdaq::exception::Exception);
			applicationInstantiatorFunction = "_ZN";
			applicationInstantiatorFunction += toolbox::lang::scopemangle(descriptor->getClassName());
			applicationInstantiatorFunction += "11instantiateEPN4xdaq15ApplicationStubE";
			
			//instantiator = (ApplicationInstantiatorFunctionType) context_->getSharedObjectRegistry()->lookup ( applicationInstantiatorFunction );
			instantiator = (xdaq::Application* (*)(xdaq::ApplicationStub*) throw (xdaq::exception::Exception))context_->getSharedObjectRegistry()->lookup ( applicationInstantiatorFunction );
			try 
			{
				// See Support Request [1246650]
   				// Logger l = Logger::getInstance( descriptor->getURN() );
				std::string appId;
				if (descriptor->hasInstanceNumber())
				{
					appId = "instance(";
					appId += descriptor->getAttribute("instance");
					appId += ")";
				}
				else
				{
					appId = "lid(";
					appId += descriptor->getAttribute("id");
					appId += ")";
				}

				Logger l = Logger::getInstance(context_->getLogger().getName() + "." + descriptor->getClassName() + "." + appId);

				if ( descriptor->getAttribute("logpolicy") != "inherit")
				{
					l.removeAllAppenders();
					//Appender* newAppender = new ConsoleAppender(true, true);
					SharedAppenderPtr newAppender (new ConsoleAppender(true, true));
					newAppender->setName ("console");
					newAppender->setLayout( std::auto_ptr<Layout>(new PatternLayout(pattern)) );
					l.setAdditivity(false);
					l.addAppender(newAppender);
				}

				std::string loglevel = descriptor->getAttribute("loglevel");
				if ( loglevel != "")
				{
					LogLevelManager& llm = getLogLevelManager();

					LogLevel level = llm.fromString (loglevel);
					if (level != NOT_SET_LOG_LEVEL)
					{
						l.setLogLevel (level);
					}
					else
					{
						std::string msg = "Log level ";
						msg += loglevel;
						msg += " is invalid";
						XCEPT_RAISE (xdaq::exception::Exception, msg);
					}
				}

			
				xdaq::ApplicationStub* stub = new xdaq::ApplicationStubImpl(context_,descriptor,l);
				xdaq::Application* application = (*instantiator)(stub);
				applications_[lid] = application;
				return application;
			}
			catch (xdaq::exception::Exception & e)
			{
				std::stringstream msg;
				msg << "Failed to instantiate object for class ";
				msg << descriptor->getClassName() << " in context ";
				msg << descriptor->getContextDescriptor()->getURL();
				
				XCEPT_RETHROW(xdaq::exception::ApplicationInstantiationFailed, msg.str(), e);
			}
			catch (std::exception & e )
			{
				std::stringstream msg;
				msg << "Failed to instantiate object for class ";
				msg << descriptor->getClassName() << " in context ";
				msg << descriptor->getContextDescriptor()->getURL();
				msg << ", std exception in user function: ";
				msg << e.what();
				XCEPT_RAISE(xdaq::exception::ApplicationInstantiationFailed, msg.str());
			}
			catch (...)
			{
				std::stringstream msg;
				msg << "Failed to instantiate object for class ";
				msg << descriptor->getClassName() << " in context ";
				msg << descriptor->getContextDescriptor()->getURL();
				msg << ", unknown exception in user function";
				XCEPT_RAISE(xdaq::exception::ApplicationInstantiationFailed, msg.str());
			}
		}
		catch (xdaq::exception::SymbolLookupFailed & sluf)
		{
			std::stringstream msg;
			msg << "Failed to instantiate object for class ";
			msg << descriptor->getClassName() << " in context ";
			msg << descriptor->getContextDescriptor()->getURL();
			msg << ", cannot find instantiator function";
			
			XCEPT_RETHROW(xdaq::exception::ApplicationInstantiationFailed, msg.str(), sluf);
		}
	}				
}

xdaq::Application* xdaq::ApplicationRegistry::getFirstApplication(const std::string & className) const
	throw (xdaq::exception::ApplicationNotFound)
{
	for ( std::map<xdata::UnsignedIntegerT, xdaq::Application*, std::less <xdata::UnsignedIntegerT> >::const_iterator i = applications_.begin(); i != applications_.end(); i++ )
	{
		if ( (*i).second->getApplicationDescriptor()->getClassName() == className )
		{
			return (*i).second;
		}	
	}
	std::string msg = "Application class '";
	msg += className;
	msg += "' not found";
	XCEPT_RAISE(xdaq::exception::ApplicationNotFound, msg);
}

xdaq::Application* xdaq::ApplicationRegistry::getApplication(const std::string & className, xdata::UnsignedIntegerT instance) const
	throw (xdaq::exception::ApplicationNotFound)
{
	for ( std::map<xdata::UnsignedIntegerT, xdaq::Application*, std::less <xdata::UnsignedIntegerT> >::const_iterator i = applications_.begin();
		i != applications_.end(); i++ )
	{
		const xdaq::ApplicationDescriptor* d = (*i).second->getApplicationDescriptor();
		if (d->hasInstanceNumber())
		{
			if ((d->getClassName() == className) && (d->getInstance() == instance))	
			{
				return (*i).second;
			}
		}	
	}
	std::stringstream msg;
	msg << "No application for class '" << className << "' instance '" << instance << "' found";
	XCEPT_RAISE(xdaq::exception::ApplicationNotFound, msg.str());
}

std::vector<xdaq::Application*> xdaq::ApplicationRegistry::getApplications
(
	const std::string& attribute, 
	const std::string & value
) const
{
	std::vector<xdaq::Application*> apps_;
	
	for ( std::map<xdata::UnsignedIntegerT, xdaq::Application*, std::less <xdata::UnsignedIntegerT> >::const_iterator i = applications_.begin(); i != applications_.end(); i++ )
	{
		if ( (*i).second->getApplicationDescriptor()->getAttribute(attribute) == value )
		{
			apps_.push_back( (*i).second );
		}	
	}
	return apps_;
}

xdaq::Application* xdaq::ApplicationRegistry::getApplication (xdata::UnsignedIntegerT localId) const
	throw (xdaq::exception::ApplicationNotFound)
{	

	std::map<xdata::UnsignedIntegerT, xdaq::Application*, std::less <xdata::UnsignedIntegerT> >::const_iterator i = applications_.find(localId);
	if (i != applications_.end())
	{
		return (*i).second;
	} 
	else
	{
		std::stringstream msg;
		msg << "No application instance for local id " << localId << " found";
		XCEPT_RAISE(xdaq::exception::ApplicationNotFound, msg.str());
	}
}

//! Remove a registered XDAQ application from the run-time environment
//
void xdaq::ApplicationRegistry::destroyApplication (xdata::UnsignedIntegerT localId) 
	throw (xdaq::exception::ApplicationNotFound)
{
	std::map <xdata::UnsignedIntegerT, xdaq::Application*, std::less <xdata::UnsignedIntegerT> >::iterator i;	
	i = applications_.find(localId);
	
	if (i != applications_.end())
	{
		// delete the application pointer
		delete (*i).second;
		
		// remove from the map
		applications_.erase (i);
	} 
	else
	{
		std::stringstream msg;
		msg << "No application instance for local id " << localId << " found";
		XCEPT_RAISE(xdaq::exception::ApplicationNotFound, msg.str());
	}	
}

std::list<xdaq::Application*>  xdaq::ApplicationRegistry::getApplications() const
{
	std::list<xdaq::Application*> apps;
	
	for ( std::map<xdata::UnsignedIntegerT, xdaq::Application*, std::less <xdata::UnsignedIntegerT> >::const_iterator i = applications_.begin(); i != applications_.end(); i++ )
	{
		apps.push_back( (*i).second );	
	}
	return apps;
}
