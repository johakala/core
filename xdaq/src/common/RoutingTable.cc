// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "toolbox/string.h"
#include "xdaq/RoutingTable.h"
#include <sstream>

std::string xdaq::RoutingTable::getNetworkPath
(
		const xdaq::ApplicationDescriptor * from,
		const xdaq::ApplicationDescriptor * to
) const
throw (xdaq::exception::NoRoute)
{
	std::map<const xdaq::ApplicationDescriptor*, std::map<const xdaq::ApplicationDescriptor*, std::string> >::const_iterator f = routes_.find(from);

	if ( f != routes_.end())
	{
		std::map<const xdaq::ApplicationDescriptor*, std::string>::const_iterator t = (*f).second.find(to);
		if ( t != (*f).second.end() )
		{
			return (*t).second;
		}
	}

	std::stringstream s;
	s << "No route from application class '" << from->getClassName() << "' in context '";
	s << from->getContextDescriptor()->getURL();
	s << "' to application class '" << to->getClassName();
	s << "' in context '" << to->getContextDescriptor()->getURL() << "'";
	XCEPT_RAISE(xdaq::exception::NoRoute, s.str());

}

//! add a network path for a source/destination tid combination
//
void xdaq::RoutingTable::addNetworkPath
(
	const xdaq::ApplicationDescriptor * from,
	const xdaq::ApplicationDescriptor * to,
	const std::string & networkName 
) 
throw (xdaq::exception::DuplicateRoute)
{
	std::map<const xdaq::ApplicationDescriptor*, std::map<const xdaq::ApplicationDescriptor*, std::string> >::const_iterator f = routes_.find(from);
	if ( f != routes_.end())
	{
		std::map<const xdaq::ApplicationDescriptor*, std::string>::const_iterator t = (*f).second.find(to);
		if ( t != (*f).second.end() )
		{
			std::stringstream s;
			s << "Route already existing from application class '" << from->getClassName() << "' in context '";
			s << from->getContextDescriptor()->getURL() << "' to application class '";
			s << to->getClassName() << "' in context '" << to->getContextDescriptor()->getURL() << "'";
			XCEPT_RAISE(xdaq::exception::DuplicateRoute, s.str());
		}

	}
	routes_[from][to] = networkName;
}

//! add a network path for a source/destination tid combination
//

void xdaq::RoutingTable::setNetworkPath
(
	const xdaq::ApplicationDescriptor * from,
	const xdaq::ApplicationDescriptor * to,
	const std::string & networkName 
) 
{
	routes_[from][to] = networkName;
}

