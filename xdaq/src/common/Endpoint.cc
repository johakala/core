// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xdaq/Endpoint.h"

xdaq::Endpoint::Endpoint
(
	pt::Address::Reference address, 
	const xdaq::ContextDescriptor* context,
	bool alias
)
{
	this->address_ = address;
	this->context_ = context;
	this->alias_ = alias;
	this->publish_ = false;
	this->active_ = false;
}

xdaq::Endpoint::Endpoint
(
	pt::Address::Reference address, 
	const xdaq::ContextDescriptor* context,
	bool alias,
	bool publish
)
{
	this->address_ = address;
	this->context_ = context;
	this->alias_ = alias;
	this->publish_ = publish;
	this->active_ = false;
}

//! Retrieve the context to which this endpoint is associated
//
const xdaq::ContextDescriptor* xdaq::Endpoint::getContextDescriptor() const
{
	return this->context_;
}


//! Retrieve the address that contains the specification of the endpoint
pt::Address::Reference xdaq::Endpoint::getAddress() const
{
	return this->address_;
}

//! If the endpoint is an alias to another endpoint that contains the same address, return true
//
bool xdaq::Endpoint::isAlias() const
{
	return this->alias_;
}

bool xdaq::Endpoint::publish() const
{
	return this->publish_;
}

bool xdaq::Endpoint::isActive() const
{
	return this->active_;
}

void xdaq::Endpoint::setActive(bool active) 
{
	 this->active_ = active;
}






