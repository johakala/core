// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xdaq/SharedObjectRegistry.h"
#include "toolbox/utils.h"
#include "xdaq/exception/LoadFailed.h"
#include "xdaq/exception/SymbolLookupFailed.h"
#include <link.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdexcept>

static int callback(struct dl_phdr_info *info, size_t size, void *data)
{
	//xdaq::SharedObjectRegistry * so = (xdaq::SharedObjectRegistry *)data;
	std::map <std::string, void*, std::less<std::string> > * so = (std::map <std::string, void*, std::less<std::string> > *)data;
   
   	//printf("name=%s (%d segments)\n", info->dlpi_name, info->dlpi_phnum);
	
   	std::string  pathname = info->dlpi_name;
   	if ( pathname == "" )
		return 0;
	
   	//void* handle = ::dlopen (pathname.c_str(), RTLD_NOLOAD| RTLD_GLOBAL);
	//if ( handle == (void*)NULL)
	//{
//
//	std::cout << "handle ==>" << std::hex << (unsigned long)handle << "-> " << dlerror() << std::endl;
//	}
	
	
	//so->sharedObjects_[pathname] = handle;
	
	//(*so)[pathname] = handle;
	(*so)[pathname] = RTLD_DEFAULT;

	return 0;
}
xdaq::SharedObjectRegistry::SharedObjectRegistry()
{
	// trigger loading info of directly linked libraries ( gcc )
        ::dl_iterate_phdr(callback, &(this->sharedObjects_));

	//std::map <std::string, void*, std::less<std::string> >::iterator i;
        //for (i = sharedObjects_.begin(); i != sharedObjects_.end(); i++)
        //{
//		std::cout << "found ==>" << (*i).first << " " << std::hex << (unsigned long)(*i).second << std::endl;
//	}
}

xdaq::SharedObjectRegistry::~SharedObjectRegistry()
{
	// close all loaded files
	std::map <std::string, void*, std::less<std::string> >::iterator i;
	for (i = sharedObjects_.begin(); i != sharedObjects_.end(); i++)
	{
		if ( (*i).second != RTLD_DEFAULT )
		{
			if (::dlclose( (*i).second ) != 0)
			{
			// no error handling here
			}
		}
	}
}

void xdaq::SharedObjectRegistry::checkdependency(const std::string & pathname) 
	throw (xdaq::exception::SymbolLookupFailed, config::PackageInfo::VersionException)
{	
	// char functionStr[255];
	std::stringstream functionStr;

	std::string library = toolbox::chop_path(pathname);
	std::string extension = ".so";
#ifdef macosx
	extension = ".dylib";
#endif
	// libXXX.so, lib counts 3
	std::string modulename = "";
	try
	{
		modulename = library.substr(3, library.size() - (extension.size() + 3));
	}
	catch (const std::out_of_range&)
	{
	}

	//
	// System dependent symbol creation!!! Move into linux specific directory.
	//
#if defined ( __GNUC__ ) && ( __GNUC__ == 2 )
	functionStr << "checkPackageDependencies__" << modulename.size() << modulename << "v";
	//snprintf (functionStr, 255, "checkPackageDependencies__%d%sv", modulename.size(), modulename.c_str());
#endif
#if defined ( __GNUC__ ) && ( __GNUC__ >= 3 )
	functionStr << "_ZN" << modulename.size() << modulename << "24checkPackageDependenciesEv";
	// snprintf (functionStr, 255, "_ZN%ld%s24checkPackageDependenciesEv", modulename.size(), modulename.c_str());
#endif

	symbolVFPtrT symPtr = (symbolVFPtrT) this->lookup ( functionStr.str() );

	if (symPtr == (symbolVFPtrT) 0) 
	{
		XCEPT_RAISE ( xdaq::exception::SymbolLookupFailed, ::dlerror() );
	}
	else 
	{
		// Call the check dependencies function
		(*symPtr)();
	}

}


config::PackageInfo xdaq::SharedObjectRegistry::getPackageInfo (const std::string & pathname) const
	throw (xdaq::exception::SymbolLookupFailed, config::PackageInfo::VersionException)
{
	typedef config::PackageInfo (*symbolSFPtrT)();

	// char functionStr[255];
	std::stringstream functionStr;

	std::string library = toolbox::chop_path(pathname);

	std::string extension = ".so";
#ifdef macosx
	extension = ".dylib";
#endif
	// libXXX.so, lib counts 3
	std::string modulename = "";
	try
	{
		modulename = library.substr(3, library.size() - (extension.size() + 3));
	}
	catch (const std::out_of_range&)
	{
	}

	//
	// System dependent symbol creation!!! Move into linux specific directory.
	//
#if defined ( __GNUC__ ) && ( __GNUC__ == 2 )
	functionStr << "getPackageInfo__" << modulename.size() << modulename << "v";
	// ::snprintf (functionStr, 255, "getPackageInfo__%d%sv", modulename.size(), modulename.c_str());
#endif
#if defined ( __GNUC__ ) && ( __GNUC__ >= 3 )
	functionStr << "_ZN" << modulename.size() << modulename << "14getPackageInfoEv";
	// ::snprintf (functionStr, 255, "_ZN%ld%s14getPackageInfoEv", modulename.size(), modulename.c_str());
#endif

	symbolSFPtrT symPtr = (symbolSFPtrT) this->lookup ( functionStr.str() );

	if (symPtr == (symbolSFPtrT) 0)
	{
		XCEPT_RAISE ( xdaq::exception::SymbolLookupFailed, ::dlerror() );
	}
	else
	{
		// Call the check dependencies function
		return (*symPtr)();
	}
}

// Returns file handle
void xdaq::SharedObjectRegistry::load (const std::string & pathname) 
	throw (xdaq::exception::LoadFailed)
{
	if (pathname == "")
	{
		XCEPT_RAISE ( xdaq::exception::LoadFailed, "Missing filename" );
	}

	// Check if already loaded: ignore if loaded
	if (sharedObjects_.find(pathname) == sharedObjects_.end())
	{
		// load
		//string filename = chop_path((char*)pathname.c_str());
		
		// libXXX.so, lib counts 3, remains XXX
		//string modulename = filename.substr(3,library.size() - (extension.size()+3));
		
		// open the file here
		void* handle = ::dlopen (pathname.c_str(), RTLD_LAZY | RTLD_GLOBAL);
		
		if (handle == 0) 
		{
			XCEPT_RAISE ( xdaq::exception::LoadFailed, ::dlerror() );
		}
		
		// remember handle
		sharedObjects_[pathname] = handle;
	}
}

void xdaq::SharedObjectRegistry::load (const std::string & pathname, bool checkDependencies) 
	throw (xdaq::exception::LoadFailed, xdaq::exception::SymbolLookupFailed, config::PackageInfo::VersionException)
{
	this->load (pathname);				
	
	if (checkDependencies)
	{
		this->checkdependency(pathname);
	} 
}

void xdaq::SharedObjectRegistry::unload(const std::string & pathname) 
	throw (xdaq::exception::UnloadFailed)
{
	// Check if already loaded: ignore if loaded
	std::map <std::string, void*, std::less<std::string> >::iterator i = sharedObjects_.find(pathname);
	if (i != sharedObjects_.end())
	{
		if (::dlclose( (*i).second ) != 0)
		{
			XCEPT_RAISE ( xdaq::exception::UnloadFailed, dlerror() );
		}
		
		sharedObjects_.erase(i);
	} 
	else 
	{
		std::string msg = "Cannot close shared library (not unloaded): ";
		msg += pathname;
		XCEPT_RAISE ( xdaq::exception::UnloadFailed, msg );
	}
}

void* xdaq::SharedObjectRegistry::lookup (const std::string & name) const
	throw (xdaq::exception::SymbolLookupFailed)
{
	// lookup symbol
	void* symbol = 0;
	std::map <std::string, void*, std::less<std::string> >::const_iterator i;
	for (i = sharedObjects_.begin(); i != sharedObjects_.end(); i++)
	{
		//std::cout << "going to lookup handle :" << std::hex << (unsigned long)(*i).second << " on path " << (*i).first << " symbol:" << name << std::endl;
		symbol = dlsym ( (*i).second, name.c_str());
		if (symbol != 0)
		{
			// std::cout << " found " << std::hex << (unsigned long)symbol << std::endl;
			return symbol;
		}
	}
	
	std::string msg = "Symbol not found: ";
	msg += name;
	XCEPT_RAISE ( xdaq::exception::SymbolLookupFailed, msg );		
}

std::vector<std::string> 
xdaq::SharedObjectRegistry::getObjectNames() const
{
	std::vector<std::string> v;
	std::map <std::string, void*, std::less<std::string> >::const_iterator i;
	for (i = sharedObjects_.begin(); i != sharedObjects_.end(); i++)
	{
		v.push_back((*i).first);
	}
	return v;
}
