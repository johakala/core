// $Id: ServicesMap.cc,v 1.2 2008/07/18 15:28:05 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xdaq/ServicesMap.h"
#include "toolbox/string.h"

void xdaq::ServicesMap::import(DOMDocument* document) throw (xdaq::exception::Exception)
{	
	hasDefault_ = false;
	services_.clear();
	
	DOMNodeList* lists = document->getElementsByTagNameNS(xoap::XStr(xdaq::ServicesMapNamespaceUri), xoap::XStr("Service"));
			
	for (XMLSize_t i = 0; i < lists->getLength(); i++)
	{
		DOMNode* serviceNode = lists->item(i);

		std::string profile = xoap::getNodeAttribute(serviceNode, "profile");
		std::string configuration = xoap::getNodeAttribute(serviceNode, "configuration");

		try
		{
			size_t from =  toolbox::toLong(xoap::getNodeAttribute(serviceNode, "from"));
			size_t to  =   toolbox::toLong(xoap::getNodeAttribute(serviceNode, "to"));

			if ( from <= to )
			{

				for ( size_t j = from; j <= to; j++ )
				{

					std::map<size_t, std::pair<std::string,std::string> >::iterator i = services_.find(j);
					if ( i != services_.end() )
					{
						std::stringstream msg;
						msg << "Service in range from: " << from << " to:" << to << ", already existing";
						XCEPT_RAISE (xdaq::exception::Exception, msg.str());
					}
					else
					{
						services_[j] = std::pair<std::string,std::string>(profile,configuration);
					}

				}
			}
			else
			{
				std::stringstream msg;
				msg << "Invalid range for service from: " << from << " to:" << to;
				XCEPT_RAISE (xdaq::exception::Exception, msg.str());
			}	
		}
		catch(toolbox::exception::Exception & e)
		{
			std::stringstream msg;
			msg << "Failed to parse port number";
			XCEPT_RETHROW (xdaq::exception::Exception, msg.str(), e);
		}

	}
	
	lists = document->getElementsByTagNameNS(xoap::XStr(xdaq::ServicesMapNamespaceUri), xoap::XStr("Default"));
	if ( lists->getLength() > 0 )
	{
		hasDefault_ = true;
		std::string profile = xoap::getNodeAttribute(lists->item(0), "profile");
		std::string configuration = xoap::getNodeAttribute(lists->item(0), "configuration");
		default_ = std::pair<std::string,std::string>(profile,configuration);
	}
	
}

std::string xdaq::ServicesMap::getProfile(size_t port) throw (xdaq::exception::Exception)
{
	std::map<size_t, std::pair<std::string,std::string> >::iterator i = services_.find(port);
	if ( i != services_.end() )
	{
		return (*i).second.first;
	}
	else
	{
	
		if ( hasDefault_ )
		{
			return default_.first;
		
		}
		std::stringstream msg;
		msg << "Cannot find profile for port: " << port;
		XCEPT_RAISE (xdaq::exception::Exception, msg.str());
	}

}

		
std::string xdaq::ServicesMap::getConfiguration(size_t port) throw (xdaq::exception::Exception)
{
	std::map<size_t, std::pair<std::string,std::string> >::iterator i = services_.find(port);
	if ( i != services_.end() )
	{
		return (*i).second.second;
	}
	else
	{
		if ( hasDefault_ )
		{
			return default_.second;
		
		}
		std::stringstream msg;
		msg << "Cannot find configuration for port: " << port;
		XCEPT_RAISE (xdaq::exception::Exception, msg.str());
	}

}

