// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "toolbox/string.h"
#include "xdaq/Network.h"
#include "xdaq/Endpoint.h"
#include "xdaq/exception/NoEndpoint.h"
#include "xdaq/exception/DuplicateEndpoint.h"
#include "xdaq/exception/AddressMismatch.h"

xdaq::Network::Network
(
	const std::string& name, 
	const std::string& protocol
)
{
	name_ = name;
	protocol_ = protocol;
}


xdaq::Network::~Network()
{
	std::map<const xdaq::ContextDescriptor*, xdaq::Endpoint*, std::less<xdaq::ContextDescriptor*> >::iterator i;
	for (i = endpoints_.begin(); i != endpoints_.end(); i++)
	{
		delete (*i).second;
	}
}	


// Only one address for the same network on a given host
//! Create an endpoint from an address, hostId and alias attribute
//
void xdaq::Network::addEndpoint 
(
	pt::Address::Reference address, 
	const xdaq::ContextDescriptor *  context,
	bool alias
)
	throw (xdaq::exception::DuplicateEndpoint, xdaq::exception::AddressMismatch)
{
	// the attribute network is XDAQ only and should actually be checked outside
	// when parsing the XML 
	if (address->getProtocol() == protocol_)
	{

		if ( endpoints_.find(context) == endpoints_.end() )
		{
			//add endpoint to list
			xdaq::Endpoint* endpoint = new xdaq::Endpoint(address, context, alias);
			endpoints_[context] = endpoint;
		}
		else
		{			
			std::string msg = "Endpoint already existing for protocol ";
			msg += protocol_;
			XCEPT_RAISE (xdaq::exception::DuplicateEndpoint, msg );
		}	
	}
	else
	{
		std::string msg = "Address definition for protocol ";
		msg += address->getProtocol();
		msg += " does not match network protocol ";
		msg += protocol_;
		XCEPT_RAISE (xdaq::exception::AddressMismatch, msg );	
	}
}

void xdaq::Network::addEndpoint 
(
	pt::Address::Reference address, 
	const xdaq::ContextDescriptor *  context,
	bool alias,
	bool publish
)
	throw (xdaq::exception::DuplicateEndpoint, xdaq::exception::AddressMismatch)
{
	// the attribute network is XDAQ only and should actually be checked outside
	// when parsing the XML 
	if (address->getProtocol() == protocol_)
	{

		if ( endpoints_.find(context) == endpoints_.end() )
		{
			//add endpoint to list
			xdaq::Endpoint* endpoint = new xdaq::Endpoint(address, context, alias, publish);
			endpoints_[context] = endpoint;
		}
		else
		{			
			std::string msg = "Endpoint already existing for protocol ";
			msg += protocol_;
			XCEPT_RAISE (xdaq::exception::DuplicateEndpoint, msg );
		}	
	}
	else
	{
		std::string msg = "Address definition for protocol ";
		msg += address->getProtocol();
		msg += " does not match network protocol ";
		msg += protocol_;
		XCEPT_RAISE (xdaq::exception::AddressMismatch, msg );	
	}
}

pt::Address::Reference xdaq::Network::getAddress 
(
	const xdaq::ContextDescriptor* context
) const
	throw (xdaq::exception::NoEndpoint)
{
	const xdaq::Endpoint* const e = this->getEndpoint(context);
	return e->getAddress();
}

std::string xdaq::Network::getName() const
{
	return name_;
}

std::string xdaq::Network::getProtocol() const
{
	return protocol_;
}

const xdaq::Endpoint* xdaq::Network::getEndpoint
(
	const xdaq::ContextDescriptor* context
)  const
	throw (xdaq::exception::NoEndpoint)
{
	std::map<const xdaq::ContextDescriptor*, xdaq::Endpoint*, std::less<const xdaq::ContextDescriptor*> >::const_iterator i = endpoints_.find(context);
	if ( i != endpoints_.end() )
	{
		return (*i).second;
	}
	else
	{
		std::string msg = "Cannot find endpoint for context '";
		msg += context->getURL();
		msg += "' on network '";
		msg += this->getName();
		msg += "'";
		
		XCEPT_RAISE(xdaq::exception::NoEndpoint,msg);
	}
}

bool xdaq::Network::isEndpointExisting (const xdaq::ContextDescriptor*  context) const
{
	if (endpoints_.find(context) != endpoints_.end() )
	{
		return true;
	}
	else
	{
		return false;
	}
}
