// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include <unistd.h>
#include <signal.h>

#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <limits.h>
#include <sstream>
#include <stack>
#include <netinet/in.h>
#include <arpa/nameser.h>

#include "xoap/SOAPBody.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/domutils.h"
#include "xoap/HTTPLoader.h"

#include "config/PackageInfo.h"
#include "toolbox/utils.h"
#include "toolbox/Runtime.h"
#include "toolbox/mem/Reference.h"
#include "toolbox/net/Utils.h"
#include "toolbox/net/URL.h"
#include "toolbox/string.h"
#include "toolbox/PolicyFactory.h"
#include "toolbox/Policy.h"

#include "pt/PeerTransportReceiver.h"
#include "pt/PeerTransportSender.h"
#include "pt/BinaryMessenger.h"
#include "pt/SOAPMessenger.h"

#include "xdaq/version.h"
#include "xdaq/ApplicationContextImpl.h"
#include "xdaq/exception/SymbolLookupFailed.h"
#include "xdaq/exception/DuplicateTid.h"
#include "xdaq/exception/LoadFailed.h"
#include "xdaq/exception/Exception.h"

#include "xdaq/InstantiateApplicationEvent.h"
#include "xdaq/EndpointAvailableEvent.h"

//
// Log4CPLUS
//
#include "log4cplus/logger.h"
#include "log4cplus/consoleappender.h"
#include "log4cplus/socketappender.h"
#include "log4cplus/nullappender.h"
#include "log4cplus/fileappender.h"
#include "log4cplus/helpers/appenderattachableimpl.h"
#include "log4cplus/helpers/loglog.h"
#include "log4cplus/helpers/pointer.h"
#include "log4cplus/spi/loggingevent.h"
#include "log4cplus/layout.h"
#include "log/udpappender/UDPAppender.h"
#include "log/xmlappender/XMLAppender.h"
// #include "log4cplus/socketSOAPappender.h"

#include <xercesc/util/XMLURL.hpp>
#include "xoap/DOMParserFactory.h"
#include "xoap/DOMParser.h"
#include "xoap/MessageFactory.h"

#include "xcept/tools.h"

using namespace log4cplus;
using namespace log4cplus::helpers;
using namespace log4cplus::spi;

 void xdaq::ApplicationContextImpl::configureTransportReceivers( DOMDocument * doc, Logger logger)
throw (xdaq::exception::ConfigurationError)
{
	const xdaq::ContextDescriptor* localContext = this->getContextDescriptor();
	
	// additional receiver endpoints
	DOMNodeList* addressList = doc->getElementsByTagNameNS (xoap::XStr("http://xdaq.web.cern.ch/xdaq/xsd/2005/XMLProfile-11"), xoap::XStr("Endpoint"));
	for ( XMLSize_t j = 0 ; j < addressList->getLength(); j++ )
	{
		DOMNode * addressNode = addressList->item(j);
			
		std::string network  = xoap::getNodeAttribute (addressNode, "network");
		if (network == "")
		{
			XCEPT_RAISE (xdaq::exception::ConfigurationError, "Missing 'network' attribute in 'Endpoint' tag");
		}
		
		std::string protocol = xoap::getNodeAttribute (addressNode, "protocol");
		if (network == "")
		{
			XCEPT_RAISE (xdaq::exception::ConfigurationError, "Missing 'protocol' attribute in 'Endpoint' tag");
		}
		
		std::string service = xoap::getNodeAttribute (addressNode, "service");
		if (service == "")
		{
			XCEPT_RAISE (xdaq::exception::ConfigurationError, "Missing 'service' attribute in 'Endpoint' tag");
		}
		
		std::string publish = xoap::getNodeAttribute (addressNode, "publish");
		if (publish == "")
		{
			publish="false";
		}
		
		// First ensure that a network can be created or is already existing
		if ( !netGroup_.isNetworkExisting(network) )
		{
			try
			{
				pt::getPeerTransportAgent()->getPeerTransport (protocol, service, pt::Sender);
				netGroup_.addNetwork(network, protocol);
			} 
			catch (pt::exception::PeerTransportNotFound& pte)
			{
				std::stringstream msg;
				msg << "Failed to configure network '" << network << "' on protocol '" << protocol << "', service '";
				msg << service << ", missing a compatible peer transport";
				XCEPT_RETHROW (xdaq::exception::ConfigurationError, msg.str(), pte);
			}
		}
		xdaq::Network * net  = const_cast<xdaq::Network *>(netGroup_.getNetwork(network)); // editable network object
		
		// create address
		std::map<std::string, std::string, std::less<std::string> > addressProperties;
		DOMNamedNodeMap * attributes = addressNode->getAttributes();
		for (XMLSize_t k = 0; k < attributes->getLength(); k++)
		{
			addressProperties[xoap::XMLCh2String(attributes->item(k)->getNodeName())] = xoap::XMLCh2String(attributes->item(k)->getNodeValue());
		}
		
		try
		{
			pt::Address::Reference  address = pt::getPeerTransportAgent()->createAddress(addressProperties);			
			if (publish == "true")
			{
				net->addEndpoint(address,  localContext, false, true);
			}
			else
			{
				net->addEndpoint(address,  localContext, false, false);
			}
			pt::PeerTransportReceiver* transport = dynamic_cast<pt::PeerTransportReceiver*>(pt::getPeerTransportAgent()->getPeerTransport (protocol, service, pt::Receiver));			
			xdaq::Endpoint* endpoint = const_cast<xdaq::Endpoint*>(net->getEndpoint(localContext)); // editable endpoint
	
			transport->config(address);
			
			 // Fire availability of endpoint (now reachable) - to be published by discovery service
			 // Format of published endpoint URL will be
			// service:endpoint:b2in.tcp://137.138.236.114:2000#lxcmd112.cern.ch:1972
			//
			
			endpoint->setActive(true);
			xdaq::EndpointAvailableEvent endpointAvailableEvent(endpoint, net, 0);
			this->fireEvent(endpointAvailableEvent);
			
			LOG4CPLUS_DEBUG(logger, "configured transport receiver on " << address->toString());
		}
		catch (pt::exception::InvalidAddress & iad)
		{
			XCEPT_RETHROW (xdaq::exception::ConfigurationError, "failed configuring networks", iad );
		}
		catch (xdaq::exception::AddressMismatch& am)
		{
			XCEPT_RETHROW (xdaq::exception::ConfigurationError, "failed configuring networks",am );
		}
		catch (pt::exception::PeerTransportNotFound& ptenf)
                {
                        XCEPT_RETHROW (xdaq::exception::ConfigurationError, "failed configuring networks, missing pt for endpoint", ptenf);
                }
                catch (pt::exception::Exception& pte)
                {
                        XCEPT_RETHROW (xdaq::exception::ConfigurationError, "failed configuring networks", pte);
                }
	}
}	

void xdaq::ApplicationContextImpl::configurePolicies (DOMElement* doc) throw (xdaq::exception::Exception)
{
	// Extract all context ids
	DOMNodeList* contextList = doc->getElementsByTagNameNS( xoap::XStr("*"), xoap::XStr("policy") );

	// loop over all context ids
	for (XMLSize_t i = 0; i < contextList->getLength(); i++)
	{
		DOMNode* protocolNode = contextList->item(i);
		std::string nsURI = xoap::XMLCh2String(protocolNode->getNamespaceURI());
		if ( nsURI == "http://xdaq.web.cern.ch/xdaq/xsd/2013/XDAQPolicy-10" )
		{
			//i2o::utils::AddressMap * addressMap = i2o::utils::getAddressMap();
			toolbox::PolicyFactory * factory = toolbox::getPolicyFactory();

			DOMNodeList* targetList = protocolNode->getChildNodes();
			for (XMLSize_t j = 0; j < targetList->getLength(); j++)
			{
				DOMNode* targetNode = targetList->item(j);
				if ( xoap::XMLCh2String(targetNode->getLocalName()) == "element")
				{
					std::string pattern = xoap::getNodeAttribute ( targetNode, "pattern");
					std::string type = xoap::getNodeAttribute ( targetNode, "type");
					std::string package = xoap::getNodeAttribute ( targetNode, "package");
					std::string targetIdentifier = "";

					//toolbox::net::URN urn(spec);
					toolbox::Policy * policy = 0;
					try
					{
						policy = factory->createPolicy(pattern, type, package);
					}
					catch (toolbox::exception::Exception & e)
					{
						XCEPT_RETHROW (xdaq::exception::Exception, "Policy not supported", e);
					}

					DOMNamedNodeMap * attributes = targetNode->getAttributes();

					for (XMLSize_t k = 0; k < attributes->getLength(); k++)
					{
						policy->setProperty(xoap::XMLCh2String(attributes->item(k)->getNodeName()), xoap::XMLCh2String(attributes->item(k)->getNodeValue()));
					}

				}
			}
			// no other protocol tags are supported
			return;
		}
		else
		{
			XCEPT_RAISE (xdaq::exception::Exception, toolbox::toString("Policy specification: %s not supported", nsURI.c_str()));
		}
	}
}

void xdaq::ApplicationContextImpl::init
(
	int argc, 
	char* argv[]
) 
	throw (xdaq::exception::Exception)
{
	char * envptr; // temporary var to extract environment variables
	
	std::string xdaq_root;
	if ( (envptr = (char*)getenv("XDAQ_ROOT")) != (char*)0 )
	{
		xdaq_root = envptr;
	}

	if ( (envptr = (char*)getenv("XDAQ_RESOURCE_ROOT")) == (char*)0 )
	{
		resources_.setProperty ("xdaq.resource.root", xdaq_root + "/daq");
	}
	else
	{
		resources_.setProperty ("xdaq.resource.root", (char*)getenv("XDAQ_RESOURCE_ROOT"));
	}
	
	// Set process ID in environment variable XDAQ_PID
	std::stringstream pid;
	pid << ::getpid();
	
	if (setenv("XDAQ_PID",pid.str().c_str(), 0) != 0)
	{
		XCEPT_RAISE(xdaq::exception::Exception, "Failed to set XDAQ_PID environemt variable");
	}
	
	// Initialize log system
	this->lastLogLevel_ = ""; // initialize log level empty
	this->defaultAppender_ = new NullAppender();
	this->defaultAppender_->setName ("null");		
	Logger::getRoot().addAppender(this->defaultAppender_);
	this->setAppender("console", "");

	int c;
	std::string hostName = toolbox::net::getHostName();
	std::string portNumber = "1972";
	std::string logLevel = ""; // DEBUG, INFO, WARN, FATAL, ERROR, TRACE
	std::string logUrl = "console";
	std::string configureURL = "";
	bool autoconfigure = false;
	
	this->sessionId_ = ""; // default is empty session id

	// Load from multiple locations:
	// 1) /etc/default.profile
	// 2) $(HOME)/etc/default.profile
	// 3) $(XDAQ_ROOT)/shared/$(XDAQ_ZONE)/profile/default.profile
	// 4) $(XDAQ_ROOT)/etc/default.profile
	//
	std::list<std::string> profile;
	profile.push_back("/etc/default.profile");
	profile.push_back("${HOME}/etc/default.profile");
	
	std::string zoneList = "default";
	if ( ( envptr = (char*)getenv("XDAQ_ZONE")) != (char*)0 )
	{
		zoneList = envptr;
	}

	// host specific
	std::string hostSpecificProfile = "${XDAQ_ROOT}/share/${XDAQ_ZONE}/profile/default." + toolbox::tolower(hostName) + ".profile";
	profile.push_back(hostSpecificProfile);
	
	// non-host specific profile
	profile.push_back("${XDAQ_ROOT}/share/${XDAQ_ZONE}/profile/default.profile");
	
	// general, minimum profile
	profile.push_back("${XDAQ_ROOT}/etc/default.profile");

	/* For an example of getopt_long see
	 * http://www.gnu.org/software/libc/manual/html_node/Getopt-Long-Option-Example.html
	 */	
	struct option long_options[] =
	{		
		{"help",		no_argument,       0, 255},
		{"daemon",		no_argument,       0, 254},
		{"stdout",		no_argument,       0, 254},
		{"stderr",		no_argument,       0, 254},
		{"pidfile",		required_argument, 0, 253},
		{"autoconfigure",	no_argument, 0, 252},
		{0, 0, 0, 0}
	};
	
	//extern char *optarg;
	//extern int optind, optopt, opterr;
	//extern int optopt;
	int option_index = -1; // index of long option
	while ((c = getopt_long(argc, argv, ":z:h:p:l:u:c:e:s:d",long_options, &option_index)) != -1) 
	{
    		switch(c) 
		{
			case 255:
				std::cout << "No help available" << std::endl;
				break;
			case 254:
				break;
			case 253:
				try
				{
					toolbox::getRuntime()->createPidFile(optarg);
				}
				catch (toolbox::exception::Exception& e)
				{
					XCEPT_RETHROW (xdaq::exception::Exception, "Failed to create pidfile", e);
				}
				break;
			case 252:
				autoconfigure = true;
				break;
    			case 'h':
				hostName = optarg;
        			break;
			case 'z':
				zoneList = optarg;
        			break;
		    	case 'p':
        			portNumber = optarg;
        			break;
		    	case 'l':
        			logLevel = optarg;
        			break;
			case 'u':
        			logUrl = optarg;
        			break;
			case 'c':
				configureURL = optarg;
				break;
			case 'e':
				profile.clear();
				profile.push_back(optarg);
				break;
			case 's':
				this->sessionId_ = optarg;
				break;
		    	case ':':
				{	
					XCEPT_RAISE (xdaq::exception::Exception, toolbox::toString("Missing argument for option -%c", optopt));
				}
        			break;
		    	case '?':
				{
					XCEPT_RAISE (xdaq::exception::Exception, toolbox::toString("unknown commandline argument '-%c'", optopt));
        			}
        			break;
		    }
	}
	
	this->argv_ = argv;
	this->argc_ = argc;

	try
	{	
		this->setAppender(logUrl, hostName);
	}
	catch (xdaq::exception::Exception& xe)
	{
		LOG4CPLUS_WARN(logger_, xcept::stdformat_exception_history(xe));
	}
	
	if (logLevel != "")
	{
		try
		{
       			this->setLogLevel(logLevel);		
		} 
		catch (xdaq::exception::Exception& xe)
		{	
			LOG4CPLUS_WARN(logger_, xcept::stdformat_exception_history(xe));
		}
	}
	// Prepare Application groups and zones
	
	toolbox::StringTokenizer strtok(zoneList,",");

	defaultZoneName_ = toolbox::trim(strtok.nextToken());
	
	try
	{
		defaultZone_ =  xdaq::ApplicationDescriptorFactory::getInstance()->createZone(defaultZoneName_);	
		while (strtok.hasMoreTokens())
		{
			std::string s = toolbox::trim(strtok.nextToken());
			xdaq::ApplicationDescriptorFactory::getInstance()->createZone(s);
		}
	}
	catch (xdaq::exception::DuplicateZone& e)
	{
		XCEPT_RETHROW (xdaq::exception::Exception, "Invalid zone list", e);
	}
	
	// Export XDAQ_ZONE
	std::string * envzone = new std::string("XDAQ_ZONE="+defaultZoneName_);
	if ( ::putenv((char*)envzone->c_str()) != 0 ) 
	{
		XCEPT_RAISE (xdaq::exception::Exception, "Failed to set XDAQ_ZONE environemt variable");
	}
	
	// Default logger is domainname.hostname.port or the IP address or domain.host.port
	
	std::string defaultLoggerName = this->getDefaultZoneName();
	
	if (defaultLoggerName != "default")
	{
		defaultLoggerName += ".";
	}
	else
	{
		defaultLoggerName = "";
	}
	
	if (std::isdigit(hostName[0]))
	{				
		this->logger_ = Logger::getInstance(defaultLoggerName + hostName + ".p:" + portNumber);
	}
	else
	{
		try
		{
			// This function can return empty string, in this case just use hostname
			std::string dnsHostname = toolbox::net::getDNSHostName(hostName);
			// Reassign resolved hostname
			if ( dnsHostname != "")
				hostName = dnsHostname;
		}
		catch (toolbox::net::exception::Exception& ne)
		{
			LOG4CPLUS_WARN(Logger::getRoot(), "Not able to resolve hostnames via DNS on host " << hostName);
		}
		
		toolbox::StringTokenizer strtok = toolbox::StringTokenizer(hostName,".");

		std::stack<std::string> s;
		while (strtok.hasMoreTokens())
		{
			s.push(strtok.nextToken());
		}

		std::string lname = "";
		while (!s.empty())
		{
			if ( lname != "") lname += ".";
			lname += s.top();
			s.pop();
		}

		// remove last "." buggy if lname empty
	        //lname.erase(lname.end()-1);

		this->logger_ = Logger::getInstance(defaultLoggerName + lname + ".p:" + portNumber);			
	}
	
	// Create a URL from the commandline IP address and port number
	//
	std::string localUrl = "http://";
	localUrl += hostName;
	localUrl += ":";
	localUrl += portNumber;
	localUrl = toolbox::tolower(localUrl);
	
	/*
	if (this->sessionId_ != "")
	{
		getNDC().push("<sid>"+this->sessionId_+"</sid>");
	}
	*/
             
	// end log preparation
	config::PackageInfo info = xdaq::getPackageInfo();
	LOG4CPLUS_INFO(this->logger_, info.getName() << " version: " << info.getLatestVersion() << ", compiled on " << info.getDate() << " at " << info.getTime() << ", " << __WORDSIZE << "bit");
		
	// Creation of unique context identifier
	try
	{
		this->context_ = this->contextTable_.createContextDescriptor(localUrl);	
	}
	catch(xdaq::exception::DuplicateHost & e)
	{
		XCEPT_RETHROW(xdaq::exception::Exception, "Failed to create local context descriptor",e); 
	}
	catch(xdaq::exception::CreationFailed & e)
	{
		XCEPT_RETHROW(xdaq::exception::Exception, "Failed to create local context descriptor",e); 
	}
		
	try 
	{		
		xdaq::checkPackageDependencies();		
	}
	catch(config::PackageInfo::VersionException & e )
	{
		LOG4CPLUS_ERROR (logger_, "Version mismatch: " << e.what() );
		
		// Version exception is not compatible with xcept exceptions
		std::string msg = "Version mismatch: ";
		msg += e.what();
		XCEPT_RAISE(xdaq::exception::Exception, msg);
	}
        
        //get XDAQ environment where to locate pt libraries
	std::string xdaqLibraryPath = "";
	if ( ( envptr = (char*)getenv("XDAQ_LIBRARY_PATH")) != (char*)NULL )
	{
		xdaqLibraryPath = envptr;
		xdaqLibraryPath += "/";
	}


	// Load from the services.map in $XDAQ_ZONE/profile/services.map
	// This operation is a consequence of the commandline option --autoconfigure	
	//
	if (autoconfigure)
	{
		DOMDocument* servicesDoc = 0;

		try
		{
			servicesDoc = this->loadDOM("${XDAQ_ROOT}/share/${XDAQ_ZONE}/profile/services.map");
		} 
		catch (xdaq::exception::Exception& xe)
		{
			std::stringstream msg;
			msg << "Failed to load services services.map file for zone '" << defaultZoneName_ << "'";
			XCEPT_RETHROW (xdaq::exception::Exception, msg.str(), xe);
		}

		try
		{
			servicesMap_.import ( servicesDoc );
			servicesDoc->release();
			servicesDoc = 0;
		}
		catch (xdaq::exception::Exception& xe)
		{
			std::stringstream msg;
			msg << "Failed to import service definitions from services.map file for zone '" << defaultZoneName_ << "'";
			XCEPT_RETHROW (xdaq::exception::Exception, msg.str(), xe);
		}
	}
	
	// If a profile is given with the -e option the --autoconfigure option is ignored
	//
	if ((profile.size() != 1) && autoconfigure)
	{
		try
		{
			profile.clear();
			profile.push_back( servicesMap_.getProfile(toolbox::toLong(portNumber)) );
			
		} 
		catch (toolbox::exception::Exception& e)
		{
			std::stringstream msg;
			msg << "Failed to autoconfigure executive for zone '" << defaultZoneName_ << "'";
			XCEPT_RETHROW (xdaq::exception::Exception, msg.str(), e);
		}
		catch (xdaq::exception::Exception& e)
		{			
			std::stringstream msg;
			msg << "Failed to autoconfigure executive for zone '" << defaultZoneName_ << "'";
			XCEPT_RETHROW (xdaq::exception::Exception, msg.str(), e);
		}
	}

	// Configure from the profile
	std::list<std::string>::iterator i = profile.begin();
	do
	{
		try
		{
			this->applyProfile( *i );
			break; // once a profile is loaded successfully go on
		} 
		catch (xdaq::exception::Exception& xe)
		{
			++i;
			if (i != profile.end())
			{
				LOG4CPLUS_WARN (logger_, xe.what() << ", trying next");				
			}
			else
			{
				XCEPT_RETHROW (xdaq::exception::Exception, "Failed to apply profile", xe);
			}
		}
	} while (i != profile.end());
	
        
	// Call config on the two peer transport HTTP
	pt::PeerTransportReceiver* httpPt = 
		dynamic_cast<pt::PeerTransportReceiver*>(pt::getPeerTransportAgent()->getPeerTransport ("http", "soap", pt::Receiver));
	try
	{
		pt::Address::Reference a = pt::getPeerTransportAgent()->createAddress(localUrl, "soap");
		httpPt->config(a);
	} 
	catch (pt::exception::Exception& httpe)
	{
		XCEPT_RETHROW (xdaq::exception::Exception, "Cannot setup HTTP transport.", httpe);
	}

	// fire profile loaded event
	xdaq::Event e("urn:xdaq-event:profile-loaded", 0);
	e.setProperty("urn:xdaq-event:baseurl", localUrl);
	dispatcher_.fireEvent(e);

	// Load from the configurationURL if it is given with -c <url> on the commandline or
	// the ServicesMap if started with --autoconfigure
	//	
	if ((configureURL == "") && autoconfigure)
	{
		try
		{
			configureURL = servicesMap_.getConfiguration( toolbox::toLong(portNumber) );
		}
		catch (xdaq::exception::Exception& e)
		{
			LOG4CPLUS_INFO(this->logger_, "No configuration file available to autoconfigure executive on port '" << portNumber << "'");
		}
	}
	
	if (configureURL != "")
	{
		try
		{
			DOMDocument* doc = this->loadDOM(configureURL);
			
			// Create SOAP Configure command and send it to the executive
			xoap::MessageReference msg = xoap::createMessage();
			xoap::SOAPEnvelope envelope = msg->getSOAPPart().getEnvelope();
			xoap::SOAPBody body = envelope.getBody();
			xoap::SOAPName commandName = envelope.createName ("Configure", "xdaq", "urn:xdaq-soap:3.0");
			xoap::SOAPElement command = body.addBodyElement(commandName);
			DOMNode* commandDOMNode = command.getDOM();
			DOMNode* importedConfiguration = commandDOMNode->getOwnerDocument()->importNode(doc->getDocumentElement(), true);
			commandDOMNode->appendChild(importedConfiguration);
			
			doc->release();
			try
			{
				// Send configure message to local executive: local id = 0
				const xdaq::ApplicationDescriptor* executiveDescriptor = this->getApplicationRegistry()->getApplication(0)->getApplicationDescriptor();
				this->postSOAP( msg, *executiveDescriptor, *executiveDescriptor );				
			}
			catch (xdaq::exception::ApplicationDescriptorNotFound& adnf)
			{
				XCEPT_RETHROW (xdaq::exception::Exception, "Failed to configure from file " + configureURL, adnf);
			}
		} 
		catch (xoap::exception::Exception& xe)
		{
			XCEPT_RETHROW (xdaq::exception::Exception, "Failed to configure from file " + configureURL, xe);
		}
		catch (xdaq::exception::Exception& xde)
		{
			XCEPT_RETHROW (xdaq::exception::Exception, "Failed to configure from file " + configureURL, xde);
		}		
	}
	
	LOG4CPLUS_INFO (this->logger_, "Ready.");
}



void xdaq::ApplicationContextImpl::reset() 
	throw (xdaq::exception::Exception)
{
	XCEPT_RAISE (xdaq::exception::Exception, "Reset not implemented");
}

void xdaq::ApplicationContextImpl::setAppender(const std::string& logUrl, const std::string& hostname)
	throw (xdaq::exception::Exception)
{	
	std::string msg = "Trying to set logger appender to ";
	msg += logUrl;
	
	// LOG4CPLUS_INFO (this->config()->logger(), msg);
	// Default error message for this function (can be overridden)
	
	//Appender* newAppender = 0;
        SharedAppenderPtr  newAppender;	
	try 
	{
		std::string pattern = "%d{%d %b %Y %H:%M:%S.%q} [%t] %-5p %c <%x";
			
		if (this->sessionId_ != "")
		{
			pattern += "<sid>";
			pattern += this->sessionId_;
			pattern += "</sid>";
		}
		pattern += "> - %m%n";
	
		if (logUrl == "null")
		{
			newAppender = new NullAppender();
			newAppender->setName ("null");
			Logger::getRoot().addAppender(newAppender);
		} 
		else if (logUrl == "console")
		{
			newAppender = new ConsoleAppender(true, true);
			newAppender->setName ("console");
			// newAppender->setLayout( std::auto_ptr<Layout>(new TTCCLayout()) );						
			
			newAppender->setLayout( std::auto_ptr<Layout>(new PatternLayout(pattern)) );
			Logger::getRoot().addAppender(newAppender);
		} 
		else 
		{
			std::vector<std::string> expandedFile;
			try
			{
				expandedFile = toolbox::getRuntime()->expandPathName(logUrl);
			} 
			catch (toolbox::exception::Exception& tbe)
			{
				std::string msg = "Cannot expand url ";
				msg += logUrl;
				XCEPT_RETHROW (xdaq::exception::Exception, msg, tbe);
			}

			if (expandedFile.size() != 1)
			{
				std::string msg = "Cannot expand url ";
				msg += logUrl;
				XCEPT_RAISE (xdaq::exception::Exception, msg);
			}
		
			// Parse the URL, format: PROTOCOL://HOSTNAME:PORT/... or PROTOCOL:/PATHNAME/...
			toolbox::net::URL url( expandedFile[0] );		
			std::string protocol = url.getProtocol();

			if (protocol == "tcp")
			{
				std::string host = url.getHost();
				unsigned int port = url.getPort(); // always 32 bit number

				newAppender = new SocketAppender (host, port, hostname);
				newAppender->setName (logUrl);
				// newAppender->setLayout( std::auto_ptr<Layout>(new TTCCLayout()) );
				newAppender->setLayout( std::auto_ptr<Layout>(new PatternLayout(pattern)) );
				Logger::getRoot().addAppender(newAppender);

				LOG4CPLUS_INFO (this->logger_, "Logging to tcp socket server " << host << ", port " << port);

			} 
			else if (protocol == "file")
			{				
				std::string path = url.getPath();
				newAppender = new FileAppender (path);
				newAppender->setName ("file:" + path);
				// newAppender->setLayout( std::auto_ptr<Layout>(new TTCCLayout()) );
				newAppender->setLayout( std::auto_ptr<Layout>(new PatternLayout(pattern)) );
				Logger::getRoot().addAppender(newAppender);

				LOG4CPLUS_INFO (this->logger_, "Logging to file (rewind) " << path);
			}
			else if (protocol == "file.append")
			{				
				std::string path = url.getPath();
				newAppender = new FileAppender (path, std::ios::app);
				newAppender->setName ("file.append:" + path);
				newAppender->setLayout( std::auto_ptr<Layout>(new PatternLayout(pattern)) );
				Logger::getRoot().addAppender(newAppender);

				LOG4CPLUS_INFO (this->logger_, "Logging to file (append) " << path);
			}  
			else if (protocol == "udp")
			{
				std::string host = url.getHost();
				unsigned int port = url.getPort(); // always 32 bit number
				
    				newAppender = new log4cplus::UDPAppender(host, port, hostname, this->getSessionId());
    				newAppender->setName(logUrl);
    				Logger::getRoot().addAppender(newAppender);
				
				LOG4CPLUS_INFO (this->logger_, "Logging to udp socket server " << host << ", port " << port);
			} 
			else if (protocol == "xml")
			{
				std::string host = url.getHost();
				unsigned int port = url.getPort(); // always 32 bit number
				
    				newAppender = new log4cplus::XMLAppender(host, 
									port, 
									hostname, 
									this->getSessionId());
    				newAppender->setName(logUrl);
    				Logger::getRoot().addAppender(newAppender);
				
				LOG4CPLUS_INFO (this->logger_, "Logging to XML socket server " << host << ", port " << port);
			} 
			else 
			{
				XCEPT_RAISE(xdaq::exception::Exception, "Invalid or unsupported log appender " + logUrl);
			}
		}
	} 
	catch (toolbox::exception::Exception& te)
	{
		// restore original logger
		// if we come here, protocol unknown, restore original url.
		if (newAppender != 0)
		{
			Logger::getRoot().removeAppender(newAppender);
			//delete newAppender;
		}
		std::string msg = "Could not set appender to ";
		msg += logUrl;
		XCEPT_RETHROW (xdaq::exception::Exception, msg, te);
	}
	catch (xdaq::exception::Exception& ex)
	{
		// restore original logger
		// if we come here, protocol unknown, restore original url.
		if (newAppender != 0)
		{
			Logger::getRoot().removeAppender(newAppender);
			//delete newAppender;
		}
		std::string msg = "Could not set appender to ";
		msg += logUrl;
		XCEPT_RETHROW (xdaq::exception::Exception, msg, ex);
	} 
	catch (...)
	{
		std::string msg = "Unknown exception occured, could not set appender to ";
		msg += logUrl;
		XCEPT_RAISE (xdaq::exception::Exception, msg);
	} 
	
	// Everything ok. Remove defaultAppender and replace with new appender
	// delete is not necessary. RemoveAppender does this
	this->defaultAppender_->close();
	Logger::getRoot().removeAppender(this->defaultAppender_);
	this->defaultAppender_ = newAppender;
}



const xdaq::ContextDescriptor* xdaq::ApplicationContextImpl::getContextDescriptor() const
{
	return this->context_;
}		

xdaq::ApplicationRegistry*  xdaq::ApplicationContextImpl::getApplicationRegistry()
{
	return &(this->applicationRegistry_);
}

const xdaq::NetGroup*  xdaq::ApplicationContextImpl::getNetGroup()
{
	return &(this->netGroup_);
}

xdaq::SharedObjectRegistry*  xdaq::ApplicationContextImpl::getSharedObjectRegistry()
{
	return &(this->sharedObjectRegistry_);
}

xdaq::RoutingTable* xdaq::ApplicationContextImpl::getRoutingTable()
{
	return &(this->routingTable_);
}

const xdaq::ContextTable*  xdaq::ApplicationContextImpl::getContextTable()
{
	return &(this->contextTable_);
}

Logger xdaq::ApplicationContextImpl::getLogger() const
{
	return this->logger_;
}

int xdaq::ApplicationContextImpl::argc()
{
	return this->argc_;
}

char** xdaq::ApplicationContextImpl::argv()
{
	return this->argv_;
}

std::string xdaq::ApplicationContextImpl::getPath() const
{
	return "unknown";
}

std::string xdaq::ApplicationContextImpl::getCommand() const
{
	return "unknown";
}

	
//The user has to set the length and the content
// of the incoming frame before ;ing
//
/*
void xdaqExecutive::frameReply(Reference* ref) 
{
		I2O_TID tmpTid = ((PI2O_MESSAGE_FRAME)ref->data())->InitiatorAddress;
		((PI2O_MESSAGE_FRAME)ref->data())->InitiatorAddress =  ((PI2O_MESSAGE_FRAME)ref->data())->TargetAddress;
		((PI2O_MESSAGE_FRAME)ref->data())->TargetAddress = tmpTid;
		
		// For the moment all replys are done on TCP
		//
		// Set BIT to flags that is a reply message 
		//
		((PI2O_MESSAGE_FRAME)ref->data())->MsgFlags |= I2O_MESSAGE_FLAGS_REPLY;
		
		if (tmpTid == XDAQ_HOST_TID) 
		{
			ptap_->reply(ref,"host:tcp");
			return;
		}
}
*/

/*
void xdaq::ApplicationContextImpl::emulateMulticast ( U16 classId, U32 networkCode, toolbox::mem::Reference* ref)
{
	for (unsigned int i=0; i < tidMap_.size(); i++)
	{
		XDAQ_Tid_Entry* entry = tidMap_[i];
	
		if (( entry->ClassId == classId) && (networkCode == entry->addressCode) && ( entry->IOPId != hostId_ ))
		{	
			ptap_->send(entry->IOPId, networkCode, frameRefDuplicate(ref));
		}
	}
	
	this->frameFree(ref);
	return;
}
*/


//
// Remember dead code for multicast implementation
/*
void xdaq::ApplicationContextImpl::frameSend (toolbox::mem::Reference * ref) 
{
	PI2O_MESSAGE_FRAME framePtr = (PI2O_MESSAGE_FRAME)ref->data();
	I2O_TID target = framePtr->TargetAddress;
	//
	// IN CASE OF MULTICAST SEND BY NETWORK
	//
	if ( (framePtr->MsgFlags & XDAQ_MULTICAST) == XDAQ_MULTICAST) 
	{
		// target is the classId in this case
		map < U16, vector<MulticastEntry>, less<U16> >::iterator i = multicastNetworkMap_.find ( target );
		if (i != multicastNetworkMap_.end())
		{
			for (unsigned int j = 0; j < i->second.size(); j++)
			{
				MulticastEntry e = i->second[j];
				ptap_->multicastSend ( e.networkCode, frameRefDuplicate(ref) );
			}
			
			// Always do a local send, too. If the class is not
			// locally configured, the message will be discarded 
			// by the dispatcher.
			ptap_->send(hostId_ ,fifoAddressCode_, frameRefDuplicate(ref));
			
			// force free of the reference
			this->frameFree(ref);
			return;
		} 
		else 
		{
			string msg = toolbox::toString ("frameSend(I2O, multicast) error: Invalid class %d", target);
			XDAQ_LOG_AND_RAISE (xdaq::exception::Exception, logger_, msg.c_str());
		}
		return;
	}	

	XDAQ_Tid_Entry* entry = tidMapSorted_[target];
	if (entry != 0) 
	{
		U32 addressCode = tidMapSorted_[target]->addressCode;
		if ( entry->IOPId == hostId_ )  
		{   
			// local then force transport 0
			ptap_->send(entry->IOPId, fifoAddressCode_, ref);
		}
		 else 
		{
			ptap_->send(entry->IOPId, addressCode, ref);
		}
	} else 
	{
		string msg = toolbox::toString ("frameSend (I2O) error: Invalid tid %d", target);
		XDAQ_LOG_AND_RAISE (xdaq::exception::Exception, logger_, msg.c_str());
	}	
}
*/
	
	
	

/*
void xdaq::ApplicationContextImpl::clearMulticastConfiguration ( U16 classId )
{
	map < U16, vector<MulticastEntry>, less<U16> >::iterator i = multicastNetworkMap_.find ( classId );
	if (i != multicastNetworkMap_.end())
	{
		(i->second).clear();
	}
}


void xdaq::ApplicationContextImpl::addMulticastNetwork ( U16 classId, U32 networkCode, U8 transportType )
{
	map < U16, vector<MulticastEntry>, less<U16> >::iterator i = multicastNetworkMap_.find ( classId );
	if (i != multicastNetworkMap_.end())
	{
		for (unsigned int j = 0; j < i->second.size(); j++)
		{
			if ( i->second[j].networkCode == networkCode) return;
		}
	}
	// didn't find classId entry, so add it, or
	// network code has not been recorded yet
	MulticastEntry info;
	info.multicast = ptap_->hasMulticast ( transportType );
	info.networkCode = networkCode;
	multicastNetworkMap_[classId].push_back (info);
	
	LOG4CPLUS_DEBUG(logger_, toolbox::toString("added multicast entry for network: %d transport: %d actual multicast: %d",info.networkCode,transportType,info.multicast));
}
*/

	

/*	
NOT NEEDED ANYMORE SINCe CONTEXTDESCRIPTOR CLASS IS USED
void xdaq::ApplicationContextImpl::setContextId(unsigned int contextId) 
{ 
	// Get all local applications
	vector<xdaq::ApplicationDescriptor*> descriptors = applicationGroup_.getApplicationDescriptors(contextId_);
	
	// Set new context
	contextId_ = contextId; 
	
	// reset all local applications descriptors with new context
	for (int i= 0; i < descriptors.size(); i++ )
	{
		((xdaq::ApplicationDescriptorImpl*)descriptors[i])->setContextId(contextId);
	}
	
	
}
*/

void xdaq::ApplicationContextImpl::setLogLevel 
(
	const std::string& level
) 
	throw (xdaq::exception::Exception)
{
	Logger root = Logger::getRoot();	

	// DO NOT FORGET REFERENCE, otherwise program core dumps
	LogLevelManager& m = getLogLevelManager();

	LogLevel l = m.fromString (level);
	if (l != NOT_SET_LOG_LEVEL)
	{
		Logger::getRoot().setLogLevel (l);
		this->lastLogLevel_ = level;
	} 
	else 
	{
		std::string msg = "Log level ";
		msg += level;
		msg += " invalid";
		XCEPT_RAISE (xdaq::exception::Exception, msg);
	}
}

std::string xdaq::ApplicationContextImpl::getLogLevel () const
{
	return this->lastLogLevel_;
}


void xdaq::ApplicationContextImpl::postFrame 
(
	toolbox::mem::Reference* ref, 
	const xdaq::ApplicationDescriptor *  originator,
	const xdaq::ApplicationDescriptor *  destination
) 
	throw (xdaq::exception::Exception)
{
	this->postFrame(ref,originator, destination, 0 , 0);
}


void xdaq::ApplicationContextImpl::postFrame 
(
	toolbox::mem::Reference * ref, 
	const xdaq::ApplicationDescriptor * originator,
	const xdaq::ApplicationDescriptor * destination,
	toolbox::exception::HandlerSignature* handler, 
	void* context
)  
	throw (xdaq::exception::Exception)		
{
	// First thing: check if descriptor is valid
	/*
	applicationDescriptorFactory_.lock();
	
	if (!applicationDescriptorFactory_.hasDescriptor(destination))
	{
		applicationDescriptorFactory_.unlock();
		XCEPT_RAISE (xdaq::exception::Exception, "Invalid destination descriptor object");
	}
	
	if (!applicationDescriptorFactory_.hasDescriptor(originator))
	{
		applicationDescriptorFactory_.unlock();
		XCEPT_RAISE (xdaq::exception::Exception, "Invalid originator descriptor object");
	}
	*/
	
	// There may be a check for the integrity of the i2o message.
	// This check can only be done, if dependency on the i2o::utils::AddressMap is introduced
	//
	// I2O_PRIVATE_MESSAGE_FRAME * framePtr = (I2O_PRIVATE_MESSAGE_FRAME*) ref->getDataLocation();		
	// framePtr->StdMessageFrame.InitiatorAddress
	// framePtr->StdMessageFrame.TargetAddress
	
	try
	{
		pt::BinaryMessenger* m = dynamic_cast<pt::BinaryMessenger*>(this->messengerCache_.getMessenger(originator, destination));
		m->send(ref, handler, context);
	} 
	catch (pt::exception::Exception& pte)
	{
		/*applicationDescriptorFactory_.unlock();
		*/
		XCEPT_RETHROW (xdaq::exception::Exception, "Failed to post frame", pte);
	}
	catch (xdaq::exception::MessengerCreationFailed& mcfe)
	{
		/*applicationDescriptorFactory_.unlock();
		*/
		XCEPT_RETHROW (xdaq::exception::Exception, "Failed to post frame", mcfe);
	}
	
	/*
	applicationDescriptorFactory_.unlock();
	*/
}
xoap::MessageReference xdaq::ApplicationContextImpl::postSOAP 
(
	xoap::MessageReference & message, 
	const xdaq::ApplicationDescriptor & originator,
	const xdaq::ApplicationDescriptor & destination
) 
	throw (xdaq::exception::Exception)
{
	bool setContentLocation = false;
	if ( message->getMimeHeaders()->getHeader("SOAPAction").size() == 0 &&
	     message->getMimeHeaders()->getHeader("Content-Location").size() == 0 )
	{
		// Temporarily also emit SOAP 1.1 format with SOAPAction
		message->getMimeHeaders()->setHeader("SOAPAction", destination.getURN());	
		message->getMimeHeaders()->setHeader("Content-Location", destination.getURN());		
		setContentLocation = true;
	}
	
	xoap::SOAPBody b = message->getSOAPPart().getEnvelope().getBody();
	DOMNode* node = b.getDOMNode();
	
	DOMNodeList* bodyList = node->getChildNodes();
	DOMNode* command = bodyList->item(0);
	
	if (command->getNodeType() == DOMNode::ELEMENT_NODE) 
	{                
		try
		{	
			pt::Messenger::Reference reference = this->soapMessengerCache_.getMessenger(originator, destination);
			pt::SOAPMessenger& m  = dynamic_cast<pt::SOAPMessenger&>(*reference);
			xoap::MessageReference reply = m.send(message);	 
			
			if (setContentLocation)
			{
				// Temporarily also emit SOAP 1.1 format with SOAPAction
				message->getMimeHeaders()->removeHeader("SOAPAction");
				message->getMimeHeaders()->removeHeader("Content-Location");
			}
			return reply;
		}
		catch (xdaq::exception::HostNotFound& e)
		{
			
			XCEPT_RETHROW (xdaq::exception::Exception, "Failed to post SOAP message", e);
		} 
		catch (xdaq::exception::MessengerCreationFailed &e)
		{
			
			XCEPT_RETHROW (xdaq::exception::Exception, "Failed to post SOAP message", e);
		}
		catch (pt::exception::Exception& e)
		{
			
			XCEPT_RETHROW (xdaq::exception::Exception, "Failed to post SOAP message", e);
		}
		catch(std::exception& e)
		{
			
			XCEPT_RAISE (xdaq::exception::Exception, e.what());
		}
		catch(...)
		{
			
			XCEPT_RAISE (xdaq::exception::Exception, "Failed to post SOAP message, unknown exception");
		}
	} 
	else
	{
		XCEPT_RAISE (xdaq::exception::Exception, "Bad SOAP message. Cannot find command tag");
	}
	
}
		

xdaq::Application* xdaq::ApplicationContextImpl::getFirstApplication
(
	const std::string & className
)  const
	throw (xdaq::exception::Exception)
{
	try
	{
		return this->applicationRegistry_.getFirstApplication(className);
	}
	catch (xdaq::exception::ApplicationNotFound& e)
	{
		std::stringstream msg;
		msg << "Failed to retrieve application class '" << className << "' from registry";
		XCEPT_RETHROW (xdaq::exception::Exception, msg.str(), e);
	}
}


void xdaq::ApplicationContextImpl::applyProfile
(
	const std::string& filename
)
	throw (xdaq::exception::Exception)
{
	// Load from the configurationURL if it is given with -c <url> on the commandline
	DOMDocument* doc = 0;
	
	try
	{
		doc = this->loadDOM(filename);
		LOG4CPLUS_INFO (logger_, "Loaded profile: " << filename);	
		//std::string tmp;
		//xoap::dumpTree(doc->getDocumentElement(),tmp);
		//std::cout << "myDOM[" << tmp << "]myDOM" << std::endl;
		
	} 
	catch (xdaq::exception::Exception& xe)
	{
		std::stringstream msg;
		msg << "Failed to load profile '" << filename << "'";
		XCEPT_RETHROW (xdaq::exception::Exception, msg.str(), xe);
	}
	
	std::string nsURI = xoap::XMLCh2String(doc->getDocumentElement()->getNamespaceURI());
	if ( nsURI != "http://xdaq.web.cern.ch/xdaq/xsd/2005/XMLProfile-11" )
	{
		XCEPT_RAISE(xdaq::exception::Exception, "Invalid namespace URI: '" + nsURI + "' for profile, expected 'http://xdaq.web.cern.ch/xdaq/xsd/2005/XMLProfile-11'");
	}
	// Apply Policies
	this->configurePolicies(doc->getDocumentElement());

	// Loop over all <Environment> tags to be able to set environment variables
	DOMNodeList* environmentList = doc->getElementsByTagNameNS (xoap::XStr("http://xdaq.web.cern.ch/xdaq/xsd/2005/XMLProfile-11"), 
								xoap::XStr("Environment"));

	for (XMLSize_t i = 0; i < environmentList->getLength(); i++)
	{
		std::string * envString = new std::string( xoap::XMLCh2String(environmentList->item(i)->getTextContent()) );
        	if ( ::putenv((char*)envString->c_str()) != 0 )
        	{
				std::stringstream msg;
				msg << "Failed to set environment '" << *envString << "'";
        	    delete envString;
				XCEPT_RAISE (xdaq::exception::Exception, msg.str());
        	}
	}

	// Load the modules
	DOMNodeList* moduleList = doc->getElementsByTagNameNS (xoap::XStr("http://xdaq.web.cern.ch/xdaq/xsd/2005/XMLProfile-11"), xoap::XStr("Module"));
	if (moduleList->getLength() == 0)
	{
	    moduleList = doc->getElementsByTagNameNS (xoap::XStr("http://xdaq.web.cern.ch/xdaq/xsd/2005/XMLProfile-11"), xoap::XStr("Module"));
	}

	for (XMLSize_t i = 0; i < moduleList->getLength(); i++)
	{
		std::string module = xoap::XMLCh2String(moduleList->item(i)->getTextContent());

		try
		{
			// future: configuration_->loadModule(module);
			this->loadModule(module); // if module contains variables, loadModule xpands the path
		} 
		catch (xdaq::exception::Exception& xe)
		{
			LOG4CPLUS_ERROR (logger_, "Failed to load module: " << xcept::stdformat_exception_history(xe));	
		}			
	}

	// Create the applications
	DOMNodeList* applicationList = doc->getElementsByTagNameNS (xoap::XStr("http://xdaq.web.cern.ch/xdaq/xsd/2005/XMLProfile-11"), xoap::XStr("Application"));
	if (applicationList->getLength() == 0)
	{
	    applicationList = doc->getElementsByTagNameNS (xoap::XStr("http://xdaq.web.cern.ch/xdaq/xsd/2005/XMLProfile-11"), xoap::XStr("Application"));
	}
	for (XMLSize_t i = 0; i < applicationList->getLength(); i++)
	{
		//DOMNamedNodeMap* applicationAttributes = applicationList->item(i)->getAttributes();				
		DOMNode * applicationNode = applicationList->item(i);
		std::string className = xoap::getNodeAttribute (applicationNode, "class");
		XCEPT_ASSERT (className != "", xdaq::exception::Exception, "Missing 'class' attribute in 'Application' tag");
					
		std::string idString = xoap::getNodeAttribute (applicationNode, "id");					
		XCEPT_ASSERT (idString != "", xdaq::exception::Exception, "Missing 'id' attribute in 'Application' tag");
					
		std::string instanceString = xoap::getNodeAttribute (applicationNode, "instance");					
		std::string  groupString = xoap::getNodeAttribute (applicationNode, "group");
		
		try
		{
			xdata::UnsignedInteger localId(idString);
			std::set<std::string> groups;
			toolbox::StringTokenizer strtok(groupString,",");
			while (strtok.hasMoreTokens())
			{
				std::string g = toolbox::trim(strtok.nextToken());
				groups.insert(g);
			}
			//all allowed zones
			std::set<std::string> allowedZones = xdaq::ApplicationDescriptorFactory::getInstance()->getZoneNames(); 

			xdaq::ApplicationDescriptor * d = xdaq::ApplicationDescriptorFactory::getInstance()->createApplicationDescriptor(context_, className,
				static_cast<xdata::UnsignedIntegerT>(localId), allowedZones, groups); 


			LOG4CPLUS_DEBUG(logger_,"Create application descriptor for class: " << className << " id: " << idString); 

			// reset instance  number
			if ( instanceString != "")
			{
				static_cast<xdaq::ApplicationDescriptorImpl*>(d)->setInstance(std::atoi (instanceString.c_str()));
				LOG4CPLUS_DEBUG(logger_,"set application instance: "  <<  instanceString << " for class: " <<	className  << " id: " << idString);
			}

			// Set all application descriptor attributes: class, instance, ...
			DOMNamedNodeMap* attributeList = applicationNode->getAttributes();
			for (XMLSize_t j = 0; j < attributeList->getLength(); j++)
			{
				DOMNode* currentAttribute = attributeList->item(j);
				d->setAttribute(xoap::XMLCh2String(currentAttribute->getNodeName()),
						xoap::XMLCh2String(currentAttribute->getNodeValue()));
			}					

			// Instantiate application
			xdaq::Application* application = applicationRegistry_.instantiateApplication (d);

			// Set default properties
			DOMNodeList* applicationProperties = applicationNode->getChildNodes();
			for ( size_t k = 0; k < applicationProperties->getLength(); k++) 
			{
				if (  xoap::XMLCh2String(applicationProperties->item(k)->getLocalName()) == "properties") 
				{
					try
					{
						// if the log level is already set, to not apply the log level property
						if (this->lastLogLevel_ != "")
						{
							// Remove logLevel node
							DOMNode* cl = applicationProperties->item(k)->getFirstChild();
							while (cl != 0)
							{
								if ((cl->getNodeType() == DOMNode::ELEMENT_NODE) && (xoap::XMLCh2String(cl->getLocalName()) == "logLevel"))
								{
									LOG4CPLUS_INFO(this->logger_, "profile loglevel is overridden by commandline");
									applicationProperties->item(k)->removeChild(cl);
									break;
								}
								cl = cl->getNextSibling();
							}
						}
						application->setDefaultValues(applicationProperties->item(k));
					} 
					catch (xdaq::exception::ParameterSetFailed& spf)
					{
						LOG4CPLUS_ERROR (this->logger_, "Parameter set failed: " << xcept::stdformat_exception_history(spf));
					}
					break;
				}
			}
			
			// Descriptor always valid at this point cause it is local (doesn't go away)
			xdaq::InstantiateApplicationEvent instantiateEvent(d,0);
			dispatcher_.fireEvent(instantiateEvent);
		}
		catch (xdaq::exception::ApplicationInstantiationFailed& aif)
		{
			LOG4CPLUS_ERROR (logger_, xcept::stdformat_exception_history(aif));
		}
		catch (xdaq::exception::Exception& e)
		{
			LOG4CPLUS_ERROR (logger_, xcept::stdformat_exception_history(e));
		}			
	}
	
	// configure peer transports locally 
	try
	{
		this->configureTransportReceivers(doc,logger_);
	}
	catch (xdaq::exception::ConfigurationError & e )
	{
		LOG4CPLUS_ERROR (logger_, xcept::stdformat_exception_history(e));
	}
}

void xdaq::ApplicationContextImpl::loadModule
(
	const std::string& pathname
) 
	throw (xdaq::exception::Exception)
{

	char * envptr; // temporary var to extract environment variables
	std::vector<std::string> expandedFile;
	try
	{
		expandedFile = toolbox::getRuntime()->expandPathName(pathname);
	} 
	catch (toolbox::exception::Exception& tbe)
	{
		std::string msg = "Cannot expand filename '";
		msg += pathname;
		msg += "'";
		XCEPT_RETHROW (xdaq::exception::Exception, msg, tbe);
	}
	
	if (expandedFile.size() == 1)
	{
		std::string filename = expandedFile[0];
		if ((filename.find("file:/") == std::string::npos) && (filename.find("http://") == std::string::npos ))
		{
			// MUST have always the "/" in the beginning
			filename.insert(0, "file:/");
		}
		
		// ------------------------
		
		std::string tmp = "";
		// if it starts with file: or /
		if ( (filename.find("file:",0) == 0) || (filename.find("/",0) == 0) )
		{
			std::string::size_type delimiter = filename.find("/");
			tmp = filename.substr ( delimiter );
		} 
		// if it starts with http:
		else if ( (filename.find ("http:",0) == 0) )
		{
			xoap::HTTPLoader loader(false);
			// always load to /tmp
			std::string tmpDir = "/tmp";
			if ( (envptr = (char*)getenv("XDAQ_TEMP")) != (char*)0 )
			{
				tmpDir = envptr;
			}
		
			if (loader.store (filename, tmpDir) != -1)
			{					
				std::string::size_type delimiter = filename.rfind("/");
				tmp =  tmpDir;
				tmp += filename.substr ( delimiter, filename.size() - delimiter );					
			}
			else
			{
				std::string msg = "Load failed. Cannot create temporary file for URL ";
				msg += filename;
				msg += " in directory ";
				msg += tmpDir;
				XCEPT_RAISE (xdaq::exception::Exception, msg);
			}
		}
		else 
		{
			std::string msg = "Load failed. Unsupported protocol in URL ";
			msg += filename;
			XCEPT_RAISE (xdaq::exception::Exception, msg);
		}

		// load library from local file, check dependencies (true) that outputs warnings if not resolvable
		try
		{
			this->sharedObjectRegistry_.load(tmp, true);
			config::PackageInfo info = this->sharedObjectRegistry_.getPackageInfo( tmp );
			LOG4CPLUS_INFO(logger_, info.getName() << " version: " << info.getLatestVersion() << ", compiled on " << info.getDate() << " at " << info.getTime());
		}
		catch (xdaq::exception::LoadFailed& lf)
		{
			XCEPT_RETHROW (xdaq::exception::Exception, "Cannot load module " + tmp, lf);
		} 
		catch (xdaq::exception::SymbolLookupFailed& slf)
		{
			std::string msg = "Could not perform version checking on module ";
			msg += filename;
			msg += ", version declaration missing.";
			LOG4CPLUS_WARN (logger_, msg);
			// XCEPT_RAISE (xdaq::exception::ConfigurationError, slf.what());
		} 
		catch (config::PackageInfo::VersionException& ve)
		{
			// toolbox::VersionException is not compatbile yet with xcept exception
			XCEPT_RAISE (xdaq::exception::Exception, ve.what());
		}
	} 
	else
	{
		std::string msg = "Pathname '";
		msg += pathname;
		msg += "' is ambiguous";
		XCEPT_RAISE (xdaq::exception::Exception, msg);
	}

}
	


void xdaq::ApplicationContextImpl::setSessionId(const std::string& id)
{
	this->sessionId_ = id;
}

std::string xdaq::ApplicationContextImpl::getSessionId() const
{
	return this->sessionId_;
}

DOMDocument* xdaq::ApplicationContextImpl::loadDOM ( const std::string& pathname) throw (xdaq::exception::Exception)
{
	// Load from the configurationURL if it is given with -c <url> on the commandline
	std::vector<std::string> files;
	try
	{
		files = toolbox::getRuntime()->expandPathName(pathname);
	} 
	catch (toolbox::exception::Exception& tbe)
	{
		XCEPT_RETHROW (xdaq::exception::Exception, "Cannot parse pathname " + pathname, tbe);
	}
	
	if (files.size() == 1)
	{
		try
                {
                       DOMDocument* doc = xoap::getDOMParserFactory()->get("configure")->loadXML(files[0]);
                       return doc;

                }
                catch (xoap::exception::Exception& e)
                {
                        std::stringstream msg;
                        msg << "Failed to load  file from '" << files[0] << "'";
                        XCEPT_RETHROW(xdaq::exception::Exception, msg.str(), e);
                }

	}
 	else
        {
                XCEPT_RAISE (xdaq::exception::Exception, "Pathname " + pathname + " is ambiguous");
        }
	
}

void xdaq::ApplicationContextImpl::addActionListener( toolbox::ActionListener * l )
{
	dispatcher_.addActionListener(l);
}

void xdaq::ApplicationContextImpl::removeActionListener( toolbox::ActionListener * l ) throw (xdaq::exception::Exception)
{
	try
	{
		dispatcher_.removeActionListener(l);
	}
	catch (toolbox::exception::Exception& e)
	{
		XCEPT_RETHROW (xdaq::exception::Exception, "Failed to remove listener", e);
	}
}

std::string xdaq::ApplicationContextImpl::getDefaultZoneName() const
{
	return defaultZoneName_;
}

const xdaq::Zone * xdaq::ApplicationContextImpl::getDefaultZone() const
{
	return defaultZone_;
}


const xdaq::Zone * xdaq::ApplicationContextImpl::getZone(const std::string & name) const throw (xdaq::exception::InvalidZone)
{
	return xdaq::ApplicationDescriptorFactory::getInstance()->getZone(name);
}

std::set<std::string>  xdaq::ApplicationContextImpl::getZoneNames () const
{
	return xdaq::ApplicationDescriptorFactory::getInstance()->getZoneNames();
}

void xdaq::ApplicationContextImpl::fireEvent ( toolbox::Event & e )
{
	dispatcher_.fireEvent(e);
}

toolbox::Properties& xdaq::ApplicationContextImpl::getResources()
{
	return resources_;
}


