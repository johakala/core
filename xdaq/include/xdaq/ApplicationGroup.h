// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _xdaq_ApplicationGroup_h_
#define _xdaq_ApplicationGroup_h_

#include <set>
#include "xdaq/ApplicationDescriptor.h"
#include "xdaq/ContextDescriptor.h"
#include "xdata/UnsignedInteger.h"

// Includes for exceptions
#include "xdaq/exception/DuplicateTid.h"
#include "xdaq/exception/ApplicationDescriptorNotFound.h"
#include "xdaq/exception/DuplicateApplicationDescriptor.h"

namespace xdaq
{

class ApplicationGroup
{
	public:
	
	virtual ~ApplicationGroup() {}

					
	//! Retrieve list of configurations that belong to host
	//
	virtual std::set<const xdaq::ApplicationDescriptor*> getApplicationDescriptors
	(
		const xdaq::ContextDescriptor * context
	) const = 0;
	
	//! Retrieve a single application descriptor according to its class name and instance number
	//
	virtual const xdaq::ApplicationDescriptor* getApplicationDescriptor
	(
		const std::string & className, 
		xdata::UnsignedIntegerT instance
	) const
	throw (xdaq::exception::ApplicationDescriptorNotFound) = 0;
	
	//! Retrieve an application descriptor according to its local identifier within a given context
	//
	virtual const xdaq::ApplicationDescriptor* getApplicationDescriptor
	(
		const xdaq::ContextDescriptor * context,
		xdata::UnsignedIntegerT localId
	)  const
	throw (xdaq::exception::ApplicationDescriptorNotFound) = 0;
		
	//! Retrieve an application descriptor according to its local identifier within a given context
	//
	virtual bool hasApplicationDescriptor 
	(
		const xdaq::ContextDescriptor * context,
		xdata::UnsignedIntegerT localId
	) const = 0;
	
	virtual bool hasApplicationDescriptor 
	(
		const xdaq::ApplicationDescriptor* descriptor
	) const = 0;
	
	virtual bool hasApplicationDescriptor 
	(
		const std::string & className, 
		xdata::UnsignedIntegerT instance
	) const  = 0;
	
	//! Retrieve list of configurations that belong to a class 
	//
	virtual std::set<const xdaq::ApplicationDescriptor*> getApplicationDescriptors
	(
		const std::string & className
	) const = 0;
	
	//! Retrieve list of configurations that match an attribute name/value pair 
	//
	virtual std::set<const xdaq::ApplicationDescriptor*> getApplicationDescriptors
	(
		const std::string & attribute,
		const std::string& value
	) const = 0;
	
	//! Retrieve set of all configurations in group
	//
	virtual std::set<const xdaq::ApplicationDescriptor*> getApplicationDescriptors () const = 0;
	
	
	//! Return the name of this group 
	virtual std::string getName() const = 0;
};

}

#endif
