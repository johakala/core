// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _xdaq_Zone_h_
#define _xdaq_Zone_h_

#include <list>
#include "xdaq/ApplicationGroup.h"

namespace xdaq
{

	class Zone
	{
		public:
		
		virtual ~Zone() {};
			
		virtual const xdaq::ApplicationGroup*  getApplicationGroup
		(
			const std::string & name
		) const
		throw (xdaq::exception::Exception) = 0;
	
		virtual std::set<const xdaq::ApplicationGroup *>  getGroups
		(
		) const
		throw (xdaq::exception::Exception) = 0;
			
		virtual bool hasApplicationGroup
		(
			const std::string & name
		) const = 0;
		
		virtual  const xdaq::ApplicationDescriptor* getApplicationDescriptor
		(
			const xdaq::ContextDescriptor * context,
			xdata::UnsignedIntegerT localId
		) const
		throw (xdaq::exception::ApplicationDescriptorNotFound) = 0;
		
		virtual std::set<const xdaq::ApplicationDescriptor*> getApplicationDescriptors
		(
			const std::string & className
		) const = 0;
			
		virtual const xdaq::ApplicationDescriptor* getApplicationDescriptor
		(
			const std::string & className, 
			xdata::UnsignedIntegerT instance
		) const
		throw (xdaq::exception::ApplicationDescriptorNotFound) = 0;
			
		virtual std::set<const xdaq::ApplicationDescriptor*> getApplicationDescriptors
		(
			const xdaq::ContextDescriptor * context
		) const = 0;
				
		virtual std::set<std::string> getGroupNames() const = 0;
		
		//! Returns the set of all groups the application descriptor belongs to
		//
		virtual std::set<std::string> getGroupNames(const xdaq::ApplicationDescriptor* descriptor) const  = 0;
	};

}

#endif
