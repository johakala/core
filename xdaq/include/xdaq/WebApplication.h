// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _xdaq_WebApplication_h_
#define _xdaq_WebApplication_h_

#include "xdaq/Application.h"
#include "xgi/framework/UIManager.h"

namespace xdaq
{

class WebApplication: public xdaq::Application, public xgi::framework::UIManager
{
	public:
	
	WebApplication(xdaq::ApplicationStub * c) throw (xdaq::exception::Exception);
	
	virtual ~WebApplication();

	virtual void ParameterQuery (xgi::Input* in, xgi::Output* out) throw (xgi::exception::Exception);
	virtual void Empty (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception);

};

}

#endif


