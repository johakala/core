// $Id: InstantiateApplicationEvent.h,v 1.3 2008/07/18 15:28:02 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _xdaq_Event_h_
#define _xdaq_Event_h_

#include <string>
#include "toolbox/Event.h"
#include "toolbox/mem/CountingPtr.h"
#include "toolbox/mem/ThreadSafeReferenceCount.h"
#include "toolbox/mem/StandardObjectPolicy.h"
#include "xdaq/ApplicationDescriptor.h"
#include "xdaq/Object.h"
#include "xdata/Properties.h"

namespace xdaq 
{

class Event: public toolbox::Event, public xdaq::Object, public xdata::Properties
{
	public:
	
	// typedef toolbox::mem::CountingPtr<InstantiateApplicationEvent, toolbox::mem::ThreadSafeReferenceCount, toolbox::mem::StandardObjectPolicy> Reference;
	
	Event(const std::string & type, xdaq::Application* originator)
		: toolbox::Event(type, originator), xdaq::Object(originator)
	{
	}

};

}

#endif
