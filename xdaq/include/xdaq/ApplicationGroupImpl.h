// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _xdaq_ApplicationGroupImpl_h_
#define _xdaq_ApplicationGroupImpl_h_

#include <set>

#include "toolbox/BSem.h"
#include "xdaq/ApplicationDescriptor.h"
#include "xdaq/ContextDescriptor.h"
#include "xdaq/ApplicationGroup.h"

// Includes for exceptions
#include "xdaq/exception/DuplicateTid.h"
#include "xdaq/exception/ApplicationDescriptorNotFound.h"
#include "xdaq/exception/DuplicateApplicationDescriptor.h"
#include "xdata/UnsignedInteger.h"

namespace xdaq
{

class ApplicationGroupImpl: public xdaq::ApplicationGroup
{
	public:
	
	ApplicationGroupImpl(const std::string & name);
	~ApplicationGroupImpl();

					
	std::set<const xdaq::ApplicationDescriptor*> getApplicationDescriptors
	(
		const xdaq::ContextDescriptor * context
	) const;
	
	//! Retrieve a single application descriptor according to its class name and instance number
	//
	const xdaq::ApplicationDescriptor* getApplicationDescriptor
	(
		const std::string & className, 
		xdata::UnsignedIntegerT instance
	) const
	throw (xdaq::exception::ApplicationDescriptorNotFound);
	
	//! Retrieve an application descriptor according to its local identifier within a given context
	//
	const xdaq::ApplicationDescriptor* getApplicationDescriptor
	(
		const xdaq::ContextDescriptor * context,
		xdata::UnsignedIntegerT localId
	) const
	throw (xdaq::exception::ApplicationDescriptorNotFound);
	
	//! Retrieve set of configurations that belong to a class 
	//
	std::set<const xdaq::ApplicationDescriptor*> getApplicationDescriptors
	(
		const std::string & className
	) const ;
	
	//! Retrieve list of configurations that match an attribute name/value pair 
	//
	std::set<const xdaq::ApplicationDescriptor*> getApplicationDescriptors
	(
		const std::string & attribute,
		const std::string& value
	) const ;
	
	//! Retrieve an application descriptor according to its local identifier within a given context
	//
	bool hasApplicationDescriptor (const xdaq::ContextDescriptor * context, xdata::UnsignedIntegerT localId) const;
	
	bool hasApplicationDescriptor 
	(
		const xdaq::ApplicationDescriptor* descriptor
	) const;
	
	bool hasApplicationDescriptor 
	(
		const std::string & className, 
		xdata::UnsignedIntegerT instance
	) const;
	
	//! Retrieve list of configurations that belong to a class 
	//
	std::set<const xdaq::ApplicationDescriptor*> getApplicationDescriptors () const;
		
	//! Add an application config
	//
	void addApplicationDescriptor (const xdaq::ApplicationDescriptor* descriptor);
	
	//! Return the name of this group 
	std::string getName() const;
	
	protected:
	
	std::string name_;
	std::set<const xdaq::ApplicationDescriptor*> applicationDescriptors_;
	mutable toolbox::BSem lock_;
};

}

#endif
