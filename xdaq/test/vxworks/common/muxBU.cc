#include "strstream.h"

#include "XRC.h"
#include "XRU.h"
#include "XIO.h"
#include "vxioslib.h"
void etherAsciiToBinary(char * etherAscii, char * etherAddress);


extern "C" int Main(char * dest, char * ifname)
{
	// Preapare Mux channel
	sensAddr sap_;
	sap_.sap = 0x1921;
	etherAsciiToBinary(dest,sap_.host);
	vxhandle * handleCln_ = new vxsenshandle(ifname,0,"dll",0x1921);
	vxiostream * streamCln_ = new vxnetiostream (static_cast<vxsenshandle*>(handleCln_));
	streamCln_->open(&sap_,vxios::write);
	
	XIO  xio(streamCln_);
	
	// Prepare interfaces on channel
	
	XRU xru(&xio);
	XRC xrc(&xio);
	
	EventIdentifier eid;
	xru.readout(&eid);
	xrc.config("Ciao");
	xrc.start("Ciao");	
	delete handleCln_;
	delete streamCln_;
	
	
	
}

void etherAsciiToBinary(char * etherAscii, char * etherAddress)
{
	strstream  etherAsciiStream;
	etherAsciiStream << etherAscii;
	char valstr[256];
	int val;
	for ( int i=0;i<6;i++)  {
		etherAsciiStream.getline(valstr,10,':');
		sscanf(valstr,"%x",&val);
		etherAddress[i] = val;
	}
	

} 

//test
vxiostream * streamCln_;
vxhandle * handleCln_;
char buf[256];
extern "C" int  main(char * dest, char * ifname) {
	sensAddr sap_;
	sap_.sap = 0x1921;
	etherAsciiToBinary(dest,sap_.host);
	handleCln_ = new vxsenshandle(ifname,0,"dll",0x1921);
	streamCln_ = new vxnetiostream (static_cast<vxsenshandle*>(handleCln_));
	streamCln_->open(&sap_,vxios::write);
	buf[4] = 4;
	*streamCln_ >> setl(sizeof(unsigned long)) >> buf;
}

extern "C" int clear() {
	delete handleCln_;
	delete streamCln_;
}
