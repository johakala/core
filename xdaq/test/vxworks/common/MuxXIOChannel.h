// $Id: MuxXIOChannel.h,v 1.3 2008/07/18 15:28:07 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef MuxXIOChannle_h
#define MuxXIOChannel_h


#include "strstream.h"

#include "XRC.h"
#include "XRU.h"
#include "XIO.h"
#include "vxioslib.h"

class MuxXIOChannel: public XIO
{
	public:
	
	MuxXIOChannel( char* interface,
                      const int unit,
                      const int type,
		      char * dest )
        {
	
		sap_.sap = type;
		etherAsciiToBinary(dest,sap_.host);
		handleCln_ = new vxsenshandle(interface,unit,"XIO",type);
		iostreamCln_ = new vxnetiostream (static_cast<vxsenshandle*>(handleCln_));
		iostreamCln_->open(&sap_,vxios::write);
		this->setStream(iostreamCln_);
	}
	
	virtual ~MuxXIOChannel() {
		cout << "MuxXIOChannel::~MuxXIOChannel()" << endl;
		delete handleCln_ ;
		delete iostreamCln_ ;
	}
	
	void etherAsciiToBinary(char * etherAscii, char * etherAddress)
	{
		strstream  etherAsciiStream;
		etherAsciiStream << etherAscii;
		char valstr[256];
		int val;
		for ( int i=0;i<6;i++)  {
			etherAsciiStream.getline(valstr,10,':');
			sscanf(valstr,"%x",&val);
			etherAddress[i] = val;
		}
	

	} 
	
	protected:
	vxiostream *iostreamCln_;
	vxhandle * handleCln_;
	sensAddr sap_;
	
};



#endif
