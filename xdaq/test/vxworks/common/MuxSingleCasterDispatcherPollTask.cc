
#include "vxNet.h"
#include "MuxSingleCasterDispatcherPollTask.h"

extern int fei82557Debug;

MuxSingleCasterDispatcherPollTask::MuxSingleCasterDispatcherPollTask( const char* interface,
			  const int unit,
			  const char* protoName,
			  const int type )

{
	status_    = OK;
	cout << "Set FEI DEBUGGER" << endl;
	//fei82557Debug = 0xff;

	::strcpy ( interface_, interface );
	unit_ = unit;
	::strcpy ( protoName_, protoName );
	type_ = type;
	
	

	muxCookie_ = ::muxBind ( interface_,
			         unit_,
			         ( FUNCPTR ) MuxSingleCasterDispatcherPollTask::wrapRcvRtn,
			         ( FUNCPTR ) MuxSingleCasterDispatcherPollTask::wrapShutdownRtn,
			         ( FUNCPTR ) MuxSingleCasterDispatcherPollTask::wrapTxRestartRtn,
			         MuxSingleCasterDispatcherPollTask::wrapErrorRtn,
			         type_,
			         protoName_,
			         ( void* ) this );

	if ( muxCookie_ == NULL ) {
		status_ = ERROR;
		cout << "vxsenshandle::vxsenshandle:: Error muxCookie_ " << endl;
		return; // THROW
	}
	// Polling mode
	status_ = ::muxIoctl ( muxCookie_,
			       EIOCPOLLSTART,
			       0 );

	if ( status_ != OK ) {
		status_ = ERROR;
		cout << "vxsenshandle::vxsenshandle:: Error muxIoctl " << endl;
		return; // THROW
	}
	
	//Prepare MBLK for reception
	M_BLK_ID pMblk = netMblkGet (((END_OBJ*)muxCookie_)->pNetPool, M_DONTWAIT, MT_DATA );
	CL_BLK_ID pClBlk = netClBlkGet (((END_OBJ*)muxCookie_)->pNetPool,M_DONTWAIT );
	netClBlkJoin ( pClBlk, buf_, 2048, NULL, 0, 0, 0 );
        pNetBuff_ = netMblkClJoin ( pMblk, pClBlk );
	pNetBuff_->mBlkPktHdr.len = 2048;
	pNetBuff_->mBlkHdr.mLen = 2048;
	//cout << "MDATA before" << hex << (int)pMblk->mBlkHdr.mData << dec << endl;
	//pNetBuff_->mBlkHdr.mData = buf_;
	//cout << "MDATA after" << hex << (int)pMblk->mBlkHdr.mData << dec << endl;
	
	

}



MuxSingleCasterDispatcherPollTask::~MuxSingleCasterDispatcherPollTask ( )
{
	STATUS s;
	cout <<
	"MuxSingleCasterDispatcherPollTask::~MuxSingleCasterDispatcherPollTask" << endl;
	status_ = ::muxIoctl ( muxCookie_,
			       EIOCPOLLSTOP,
			       0 );

	if ( status_ != OK ) {
		status_ = ERROR;
		cout << "vxsenshandle::vxsenshandle:: Error muxIoctl " << endl;
		return; // THROW
	}
	if ( muxCookie_ != NULL ) {
		s = ::muxUnbind(muxCookie_, type_, (FUNCPTR)MuxSingleCasterDispatcherPollTask::wrapRcvRtn);
	}

}
BOOL MuxSingleCasterDispatcherPollTask::stackRcvRtn ( long type, M_BLK_ID pNetBuff, LL_HDR_INFO* pLinkHdr)
{
	::logMsg("stackRcvRtn dump pNetBuff %d\n", (int)pNetBuff, 0, 0, 0, 0, 0);
	
}


int MuxSingleCasterDispatcherPollTask::svc()
{
	int count = 0;
	
	for(;;) {
		retry:
		
		int status = ::muxPollReceive(muxCookie_,pNetBuff_);
		if (status == EAGAIN ) {
			goto retry;
		}

		count = 0;
		char* buf = pNetBuff_->mBlkHdr.mData + 14 + 2;
		short sap = ntohs(*(short*)&(pNetBuff_->mBlkHdr.mData[12]));
		//::logMsg("**** POLLING RECEIVED ***** pNetBuff %d\n", (int)sap, 0, 0, 0, 0, 0);
		if ( sap == type_ ) {
			DAQProtocolHeader* proto;
			proto = (DAQProtocolHeader*) buf;
		
			this->dispatch(ntohs(proto->protocol),buf);
		}
				
	}

}
void MuxSingleCasterDispatcherPollTask::stackShutdownRtn ( )
{
}

void MuxSingleCasterDispatcherPollTask::stackTxRestartRtn ( )
{

}

void MuxSingleCasterDispatcherPollTask::stackErrorRtn ( END_OBJ* pEnd, END_ERR* pError )
{

}


//
// wrapper members
//

BOOL MuxSingleCasterDispatcherPollTask::wrapRcvRtn ( void* muxCookie, long type, M_BLK_ID pNetBuff, LL_HDR_INFO* pLinkHdr,
			        void* pObj )
{
	//::logMsg("MuxSingleCasterDispatcherPollTask::wrapRcvRtn\n", 0, 0, 0, 0, 0, 0);

	( ( MuxSingleCasterDispatcherPollTask* ) pObj )->stackRcvRtn ( type, pNetBuff, pLinkHdr );
	//Hold packet, therefore free MBLK chain after use
	
	return(TRUE);
}

void MuxSingleCasterDispatcherPollTask::wrapShutdownRtn ( void* muxCookie, void* pObj )
{
	( ( MuxSingleCasterDispatcherPollTask* ) pObj )->stackShutdownRtn ( );
}

void MuxSingleCasterDispatcherPollTask::wrapTxRestartRtn ( void* muxCookie, void* pObj )
{
	( ( MuxSingleCasterDispatcherPollTask* ) pObj )->stackTxRestartRtn ( );
}

void MuxSingleCasterDispatcherPollTask::wrapErrorRtn ( END_OBJ* pEnd, END_ERR* pError, void* pObj )
{
	( ( MuxSingleCasterDispatcherPollTask* ) pObj )->stackErrorRtn( pEnd, pError );
}



