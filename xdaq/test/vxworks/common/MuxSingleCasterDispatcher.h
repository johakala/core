// $Id: MuxSingleCasterDispatcher.h,v 1.4 2008/07/18 15:28:07 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/


#ifndef MuxSingleCasterDispatcher_h
#define MuxSingleCasterDispatcher_h
#include <vxWorks.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <hostLib.h>
#include <usrLib.h>
#include <ioLib.h>
#include <errnoLib.h>
#include <iostream.h>
#include <iosLib.h>
#include <intLib.h>
#include <vxLib.h>
#include <wdLib.h>
#include <semLib.h>
#include <logLib.h>
#include <taskLib.h>
#include <tickLib.h>
#include <cacheLib.h>
#include <rngLib.h>
#include <netBufLib.h>
#include <muxLib.h>
#include "XSingleCasterDispatcher.h"

class MuxSingleCasterDispatcher: public XSingleCasterDispatcher
{


   public:

    
    MuxSingleCasterDispatcher ( const char* interface,
	 	   const int unit,
		   const char* protoName,
		   const int type);
    
     virtual ~MuxSingleCasterDispatcher ();
        
  
     BOOL stackRcvRtn ( long type, M_BLK_ID pNetBuff, LL_HDR_INFO* pLinkHdr );
     void stackShutdownRtn ( );
     void stackTxRestartRtn ( );
     void stackErrorRtn ( END_OBJ* pEnd, END_ERR* pError );


  
  private:
  	static BOOL wrapRcvRtn ( void* muxCookie, long type, M_BLK_ID pNetBuff, 
	                         LL_HDR_INFO* pLinkHdr, void* pObj );
	static void wrapShutdownRtn ( void* muxCookie, void* pObj );
	static void wrapTxRestartRtn ( void* muxCookie, void* pObj );
	static void wrapErrorRtn ( END_OBJ* pEnd, END_ERR* pError, void* pObj );
	static void wrapFreeRtn ( int pObj, int freeRtnArg1, int freeRtnArg2 );

  public:
	STATUS status_;

  protected:
	char interface_[32];
	int unit_;
	int type_;
	char protoName_[32];
	void* muxCookie_;


};



#endif /* ifndef  __vxsenshandle_h__ */


