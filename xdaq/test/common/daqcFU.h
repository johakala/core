// $Id: daqcFU.h,v 1.7 2008/07/18 15:28:06 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "XRUAdapter.h"
#include "XRCAdapter.h"
#include "XEVMAdapter.h"
#include "XFUAdapter.h"
#include "XRU.h"
#include "XEVM.h"
#include "XMDispatcherLoop.h"
#include "XSDispatcherLoop.h"
#include "XIO.h"
#include "vxtExceptionHandler.h"
#include "Task.h"
#include "TaskGroup.h"
#include "TcpXIOConnectorM.h"
#include "BSem.h"
#include "Loader.h"
#include "TcpDispatcherSelectorTask.h"
#include "TcpXIOConnectorS.h"
#include "vxTiming.h"
#ifdef vxworks
#include "MuxXIOChannel.h"
#endif	

extern int counter_;

vxChron chrono;	
unsigned long loopCounter = 0;
inline void Benchmarks2(int size)
{
	
	
	if ( loopCounter == 1000 )
	{
		chrono.start(0);
	}
	else
	if ( loopCounter == 101000 )
	{
		loopCounter = 0;
		chrono.stop(0);
                printf("Delta Time : %d usecs \n", chrono.dusecs());
		printf("Rate: %f Hz  \n", (float)100000/(float)(chrono.dusecs()/(float)1000000));
		printf("Bandwitdth: %f Mb\n",
		(((float)100000/(float)(chrono.dusecs()/(float)1000000)) * (float)size)/(float)0x100000);
		printf("Time to process one Event: %f usecs\n", (float)(chrono.dusecs()/(float)100000));
	}
	loopCounter++;
}



class daqcFU: public Loader, public XFUAdapter, public XRCAdapter {
	public:
	
	daqcFU() {
		
		stop_ = 0;
		counter_ = 0;
		mutex_ = new BSem(BSem::FULL);
		initialized = 0;
	}
	

       void start (char * str) {
       		sleep(1);
       		cout << "start:" << str << " on:" << DaqCfgSysUAddress_<< endl;
		EventIdentifier eid = 10;
		DestinationCookie dcookie;
		dcookie.addr = DaqCfgSysUAddress_;
		dcookie.cookie = DaqCfgSysUAddress_;
		//Request first fragment
		EventProfile eprofile = 0xff;
		mutex_->take();
		for (int j=0; j< 50; j++ ) {
				xevm_->allocate(&eprofile,&dcookie);
		}
		mutex_->give();
       }
       
       void stop (char * str) {
       		cout << "stop:" << str << endl;
		stop_ = 1;
       }        
       
       void confirm (EventIdentifier* eid) {
       		mutex_->take();
       		//cout << "confirm:" << *eid << endl;
		DestinationCookie dcookie;
		dcookie.addr = DaqCfgSysUAddress_;
		dcookie.cookie = DaqCfgSysUAddress_;
		//fragmentv = 0;
		EventProfile eprofile = 0xff;
		
/* testing BM only
		xevm_->allocate(&eprofile,&dcookie);
		xevm_->clear(eid);
		Benchmarks2(1024);
		counter_++;
		if ((counter_ % 1000 ) == 0 )
				//::logMsg("Got trip 10000\n", 0, 0, 0, 0, 0, 0);
				cout << "Got 1000 events" << endl;
*/

		if (*eid != -1 ) { //valid ID
			fragmentv[*eid] = xruv_.size();
			for (int i =0; i < xruv_.size(); i++ )
				xruv_[i]->send(eid,&dcookie);
		}
		else
		{
			cout << "*** Warning: Invalid ID: retry" << endl;
			//force handshake with cache if any
			xevm_->allocate(&eprofile,&dcookie);
		}
		
		mutex_->give();
       }
       
       
       void cache (FragmentIdentifier* fid, EventIdentifier* eid,  DestinationCookie* dcookie, FragmentData* fdata) {
       		mutex_->take();
       		//cout << "cache: got fragment:<" << *fid << "," << *eid << " from:" << dcookie->addr << " on:" << dcookie->cookie << ">"  << endl;
		//request next
		EventProfile eprofile = 0xff;
		if ( stop_ ) return;
		//fragmentv++;
		fragmentv[*eid]--;
		if ( fragmentv[*eid] ==  0) {
			//cout << "Clear  Fragment" << endl;
			//cout << "Allocate next Fragment" << endl;
			DestinationCookie dc;
			dc.addr = DaqCfgSysUAddress_;
			dc.cookie = DaqCfgSysUAddress_;
			xevm_->allocate(&eprofile,&dc);
			xevm_->clear(eid);
			Benchmarks2(1024);
			counter_++;
			if ((counter_ % 1000 ) == 0 )
				//::logMsg("Got trip 10000\n", 0, 0, 0, 0, 0, 0);
				cout << "Got 1000 events" << endl;
		}
		
		
       		mutex_->give();
       }  
       
       
        void init(char * str) {
       		if ( ! initialized )
		{	
			this->loadEnvironment();
			int i;
#ifdef vxworks
  			muxioevm_ = new MuxXIOChannel("fei",0,0x1922,"08:00:3e:28:74:b0");
			muxioru_ = new MuxXIOChannel("fei",0,0x1921,"00:90:27:5d:7b:63"); //vxwcmsru11
#endif
			//
			// Prepare interface to Readout Units according socket configuration
			//
			cout << "Prepare interface to Readout Units" << endl;
			s = new TcpDispatcherSelectorTask();
			xruv_.resize(DaqCfgNoRUs_);
			connector = new TcpXIOConnectorM(&DaqCfgRUGenericAddrList_);
			connector->connect();
			for (  i= 0; i< DaqCfgNoRUs_; i++ ) {
				s->putXIO((*connector->getXIO())[i]);
				xruv_[i] = new XRU((*connector->getXIO())[i]);
				//xruv_[i] = new XRU(muxioru_);
			}
			//
			// Prepare interface to EVM
			//
			cout << "Prepare interface to EVM" << endl;
			 cevm = new TcpXIOConnectorS(DaqCfgEVMIpAddr_,DaqCfgEVMPort_);
			cevm->connect();
			s->putXIO(cevm->getXIO());
			xevm_ = new XEVM(cevm->getXIO());
			//xevm_ = new XEVM(muxioevm_);
			//
			// Run trigger loop
			//
			s->addListener(this);
			s->activate();
				
			initialized = 1;
			
			cout << "Done init" << endl;
		}
		else
		{
			cout << "***Warning: RU system already initialized" << endl;
		
		}

       }
    
	protected:
#ifdef vxworks	
	MuxXIOChannel * muxioru_;
	MuxXIOChannel * muxioevm_;
#endif
	int stop_;
	vector<RUInterface *> xruv_;
	EVMInterface * xevm_;
	BSem * mutex_;
	//int fragmentv;
	int fragmentv[1024];
	int initialized;			
	TcpDispatcherSelectorTask * s;
	TcpXIOConnectorS * cevm;
	TcpXIOConnectorM * connector;

};


