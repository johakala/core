#include "XIO.h"
#include "XRU.h"
#include "XRC.h"
#include "XFU.h"
#include "XEVM.h"
#include "vxtExceptionHandler.h"


void printhelp (const char *sf, const char *da)
{
	cout << "Help Screen:" << endl << endl;
	cout << "    Current script file: " << sf << endl;
	cout << "    Current dynamic arguments: " << da << endl;
	cout << endl;
	cout << "    h - Shows this help screen" << endl;
	cout << "    f - Select script file" << endl;
	cout << "    a - Enter dynamic arguments" << endl;
	cout << "    l - Reload script file" << endl;
	cout << "    c - Clear dynamic arguments" << endl;
	cout << "    i - Execute script from <Init> entry point" << endl;
	cout << "    s - Execute script from <Start> entry point" << endl;
	cout << "    e - Execute script from <Stop> entry point" << endl;
	cout << "    d - Execute script from <Shutdown> entry point" << endl;
	cout << "    w - Execute script from <Write> entry point" << endl;
	cout << "    r - Execute script from <Read> entry point" << endl;
	cout << "    x - Exit" << endl;
	cout << endl;
}



// str = IP NUmber of server
extern "C" void Main(char * str)
{
  
  
  
  //
  // Open a UDP output stream
  //
 
  tcpAddr      addr;
  XIOStreamVector v(1);
  addr.port = 20200;
  strcpy(addr.host,str);
  vxtcphandle * handle = new vxtcphandle();
  v[0] = new vxtcpiostream(handle);
  handle->connect(&addr);

 
  cout << "Create XIO" << endl;
  try
  {
  	//
  	// Create virtual I/O channel
  	//
	XIO * xio = new XIO(v);
  
   	
  	XRU * xru = new XRU(xio,0);
    XRC * xrc = new XRC(xio,0);
	
  	
	FragmentIdentifier fid;
  	EventIdentifier eid;
	EventProfile eprofile;
	DestinationCookie dcookie;
	FragmentSet fset;
	FragmentData fdata;
	
/*	
 	xru->readout(&eid);
  	xru->send(&eid,&dcookie);
	xrc->config("config");
	xrc->start("start");
	xrc->stop("stop");
	xrc->shutdown("shutdown");
	xrc->action("action");
	xrc->suspend("suspend");
	xrc->resume("resume");
	xrc->load("load");
*/

	char scriptfile[150];
	char args[280];
	char param[300];
	char opt = '\0';
	scriptfile[0] = '\0';
	args[0] = '\0';
	param[0] = '\0';
	printhelp(scriptfile,args);
	
	do {
		cout << "Option->> ";
		cin >> opt;
		cout << endl;
		switch (opt) {
		case 'h':
			printhelp(scriptfile,args);
			break;
		case 'f':
			cout << endl << "File path and name:" << endl << ">> ";
			cin >> scriptfile;
			cout << endl;
			xrc->load(scriptfile);
			break;
		case 'a':
			cout << endl << "Dynamic Arguments:" << endl;
			cout << "Separate with commas (eg: 0x5000000,0x7500000)" << endl;
			cout << ">> ";
			cin >> args;
			cout << endl;
			break;
		case 'l':
			cout << endl;
			xrc->load(scriptfile);
			break;
		case 'c':
			cout << endl;
			args[0] = '\0';
			break;
		case 'i':
			xrc->config(args);
			break;
		case 's':
			xrc->start(args);
			break;
		case 'e':
			xrc->stop(args);
			break;
		case 'd':
			xrc->shutdown(args);
			break;
		case 'w':
			strcpy(param,"Write");
			if (args[0] != '\0') {
				strcat(param,",");
				strcat(param,args);
			}
			xrc->action(args);
			break;
		case 'r':
		 	xru->readout(&eid);
			break;
		case 'x':
			cout << "Bye" << endl;
			break;
		default:
			cout << "Command does not exist, select 'h' for help screen." << endl;
			break; 
		}
	
	} while (opt != 'x');
	

  	delete xio;
  	delete xru;
  	delete xrc;
	delete handle;
	delete v[0];
	
  }
  catch(vxException e)
  {
  	cout << e;
  };
  
}


//
// The main program
//
// Usage:
// main load /tmp/script
//
#ifndef vxworks
int main(int argc, char* argv[])
{
	Main(argv[1]);
 	
}
#endif
