// $Id: sdevmM.h,v 1.4 2008/07/18 15:28:06 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef __sdevm_h__
#define __sdevm_h__



#include "XRCAdapter.h"
#include "XEVMAdapter.h"
#include "RUInterface.h"
#include "FUInterface.h"
#include "BSem.h"
#include "vxSys.h"
#include "TcpDispatcherAcceptorTaskS.h"
#include "TcpXIOConnectorS.h"
#include "XFU.h"
#include "XRU.h"

class RingList {
	
	public:

	RingList(int size) {
		size_ = size;
		first_ = 0;
		last_ = 0;
		v_ = new int [size_];
		currentsize_ = 0;
	}
	
	~RingList() {
		delete [] v_;
	}
	inline int size() {
		return(currentsize_);
	}
	inline void pop_front() {
		if ( first_ != last_ ) {
			first_ = (first_ +1 ) % size_;
			currentsize_--;
			return;
		}
		else
		{
			cerr << "*** Error: RingList is empty!" << endl;
			return;
		}
	}
	
	inline int front() {
		return(v_[first_]);
	}
	
	inline void push_back(int i) {
		if ((( last_ + 1) % size_ ) != first_ ) {
			v_[last_] = i;
			last_ = (last_ + 1) % size_;
			currentsize_++;
			return;
		}
		else
		{
			cerr << "*** Error: RingList is full!" << endl;

		}
	
	}
	
	protected:
	
	int size_;
	int first_;
	int last_;
	int * v_;
	int currentsize_;

};

class SDEVM: public XEVMAdapter, public Task {

//
//  Member Functions:
//

//
//      This class defines the following public member functions:
//
//
	public:
	SDEVM( vector<FUInterface *> * fuv, RUInterface * ru, int maxeventids) {
		fuv_ = fuv;
		ru_ = ru;
		stop_ = 0;
		maxeventids_ = maxeventids;
		mutex_ = new BSem(BSem::FULL);
		triggersq_ = new RingList(maxeventids_+1);
		identifiersq_ = new RingList(maxeventids_+1);
		for (int i =0; i< maxeventids_; i++ )
			idput(i);
	} 
	
	
	void allocate (EventProfile* eprofile, DestinationCookie* dcookie) {
		EventIdentifier eid;
		if ( triggernotempty() ) {
	 		//TRIGGERs available
			eid = triggerget();
			//cout << "Allocating fragment: " << eid << " to:" << dcookie->addr << endl;
			(*fuv_)[dcookie->addr]->confirm(&eid);
		}
		else {
			//NO TRIGGER availables
			eid = -1;
			//cout << "No trigger available: " << eid << endl;
			(*fuv_)[dcookie->addr]->confirm(&eid);
		}
	}
	 
	void clear (EventIdentifier* eid) {
	 	idput(*eid);
	}
	
	int svc() {
	      //new trigger
	      for (;;) {
	      
	      if ( idnotempty() ) {
	              // IDs availables
	              EventIdentifier eid = idget();
		      //cout << "Read out fragment" << eid << endl;
	              ru_->readout(&eid);
	              triggerput(eid);

	      }
	      else
	      {
	              // TRIGGER LOST
	              //cout << "*** Warning: Trigger LOST, apply back pressure" << endl;
	              sleep(1);
	
	      }
	      }
	      return(0);
	}
	
	// ID Queue
	inline int idnotempty()
	{
		mutex_->take();
		int size = identifiersq_->size();
		mutex_->give();
		return(size); 
	
	}
	inline void idput(int id)
	{	
		mutex_->take();
		identifiersq_->push_back(id);
		mutex_->give();
	}

	inline int idget()
	{	
		mutex_->take();
		int tmpId = identifiersq_->front();
		identifiersq_->pop_front();
		mutex_->give();
	
		return(tmpId);
	}
	// Trigger Queue
	inline int triggernotempty()
	{
		mutex_->take();
		int size = triggersq_->size();
		mutex_->give();
		return(size); 
	
	}
	inline void triggerput(int id)
	{	
		mutex_->take();
		triggersq_->push_back(id);
		mutex_->give();
	}

	inline int triggerget()
	{	
		mutex_->take();
		int tmpId = triggersq_->front();
		triggersq_->pop_front();
		mutex_->give();
	
		return(tmpId);
	}
	
	protected:
	
	int stop_;
	int maxeventids_;
	vector<FUInterface *> * fuv_;
	RUInterface * ru_;
	RingList * triggersq_;
	RingList * identifiersq_;
	BSem * mutex_;
};

#endif
