// $Id: TriggerGenerator.h,v 1.3 2008/07/18 15:28:06 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef __TriggerGenerator_h__
#define __TriggerGenerator_h__

#include "Task.h"

class daqcEVM;

class TriggerGenerator:  public Task {
	public:
	
	void addListener(daqcEVM * evm);
	
	int svc();
	
	protected:
	
	daqcEVM * evm_;
};

#endif
