// $Id: sdfu.h,v 1.4 2008/07/18 15:28:06 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "XRUAdapter.h"
#include "XRCAdapter.h"
#include "XEVMAdapter.h"
#include "XFUAdapter.h"
#include "XRU.h"
#include "XEVM.h"
#include "XMDispatcherLoop.h"
#include "XSDispatcherLoop.h"
#include "XIO.h"
#include "vxtExceptionHandler.h"
#include "Task.h"
#include "TaskGroup.h"
#include "TcpXIOConnectorS.h"

class SDispatcherTask: public Task, public XSDispatcherLoop {

	public:
	
	SDispatcherTask(XIO* xio):XSDispatcherLoop(xio) {
	
	}
	
	int svc() {
	 	Try
  		{
  			this->foreverloop();
  		}
  		Catch(vxException e)
  		{
  			cout << e;
  		}
		return(0);
	
	}


};
class SDFU: public XFUAdapter, public XRCAdapter {
	public:
	
	SDFU(RUInterface * ru, EVMInterface * evm) {
		ru_ = ru;
		evm_ = evm;
		stop_ = 0;
		counter_ = 0;
	}
	

       void start (char * str) {
       		cout << "start:" << str << endl;
		EventIdentifier eid = 10;
		DestinationCookie dcookie;
		dcookie.addr = 0x5060;
		dcookie.cookie = 0x7080;
		//Request first fragment
		EventProfile eprofile = 0xff;
		evm_->allocate(&eid,&dcookie);
       }
       
       void stop (char * str) {
       		cout << "stop:" << str << endl;
		stop_ = 1;
       }        
       
       void confirm (EventIdentifier* eid) {
       		//cout << "confirm:" << *eid << endl;
		DestinationCookie dcookie;
		dcookie.addr = 0x5060;
		dcookie.cookie = 0x7080;
		ru_->send(eid,&dcookie);
       }
       
       
       void cache (FragmentIdentifier* fid, EventIdentifier* eid,  DestinationCookie* dcookie, FragmentData* fdata) {
       		//cout << "cache: got fragment:<" << *fid << "," << *eid << "," << dcookie->addr << "," << dcookie->cookie << ">"  << endl;
		//request next
		EventProfile eprofile = 0xff;
		if ( stop_ ) return;
		//cout << "Clear previous Fragment" << endl;
		evm_->clear(eid);
		//cout << "Allocate next Fragment" << endl;
		evm_->allocate(&eprofile,dcookie);
		
		counter_++;
		if ((counter_ % 100 ) == 0 )
			cout << "Got 100 events" << endl;
       
       }       
	protected:
	int counter_;
	int stop_;
	RUInterface * ru_;
	EVMInterface * evm_;

};


