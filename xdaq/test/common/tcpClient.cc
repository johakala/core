#include "sdfu.h"

extern "C" int Main(char * server)
{

	
	TcpXIOConnectorS * c1 = new TcpXIOConnectorS(server, 20200);
	
  	SDispatcherTask * d1 = new SDispatcherTask(c1->getXIO());
	
	XRU * xru = new XRU(c1->getXIO());
	XEVM * xevm = new XEVM(c1->getXIO());
	SDFU * sdfu = new SDFU(xru,xevm);
	
	c1->connect();
	

  	TaskGroup g;
  	d1->initTaskGroup(&g);
  	d1->setListener(sdfu);
  	d1->activate();
	
	sleep(3);
	sdfu->start("Go!");
	
 	g.join();
	delete d1;
	delete c1;
	delete xru;
	delete xevm;
	delete sdfu;
	return(0);	
}
#ifndef vxworks
int main(int argc, char* argv[])
{

	return Main(argv[1]);
}
#endif
