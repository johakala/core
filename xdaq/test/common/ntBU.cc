#include "XRC.h"
#include "XRU.h"
#include "TcpXIOConnectorS.h"

int main(int argc, char* argv[])
{
	TcpXIOConnectorS  c1(argv[1], 20200);
	XRU xru(c1.getXIO());
	XRC xrc(c1.getXIO());
	c1.connect();
	
	EventIdentifier eid;
	xru.readout(&eid);
	xrc.config("Ciao");
	xrc.start(argv[1]);	
	pause();
	
}
