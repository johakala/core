// $Id: Loader.h,v 1.5 2008/07/18 15:28:06 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef __Loader_h__
#define __Loader_h__

#include "DAQConfig.h"
#include "Environment.h"



class Loader {
	public:
	
	void loadEnvironment() {
	
	     //
	     // Load Configuration from environment
	     //
	     sscanf(Environment::get(DaqCfgSafeMargin),"%f",&DaqCfgSafeMargin_);
	     sscanf(Environment::get(DaqCfgRDPMBlockSize),"%x",&DaqCfgRDPMBlockSize_);
	     sscanf(Environment::get(DaqCfgSysUAddress),"%d",&DaqCfgSysUAddress_);
	     sscanf(Environment::get(DaqCfgNoEIDs),"%d",&DaqCfgNoEIDs_);
	     sscanf(Environment::get(DaqCfgEVMPort),"%d",&DaqCfgEVMPort_);
	     strcpy(DaqCfgEVMIpAddr_,(char *)Environment::get(DaqCfgEVMIpAddr));
	    
	     
	     ValList list;
	     list = Environment::getList(DaqCfgBUIpAddrList);
	     int i; 
	     DaqCfgNoBUs_ = list.size();
	     DaqCfgBUGenericAddrList_.resize(DaqCfgNoBUs_);
	     DaqCfgBUIpAddrList_.resize(DaqCfgNoBUs_);
	     
	     for ( i= 0; i<DaqCfgNoBUs_; i++ ) {
	     	     strcpy(DaqCfgBUIpAddrList_[i].host,list[i]);
	     	     DaqCfgBUGenericAddrList_[i] = &DaqCfgBUIpAddrList_[i];
	     }
	     list = Environment::getList(DaqCfgBUIpPortList);
	     for ( i= 0; i<DaqCfgNoBUs_; i++ ) {
		     sscanf(list[i],"%d",&(DaqCfgBUIpAddrList_[i].port));
	     }
	     list = Environment::getList(DaqCfgRUIpAddrList);
	     DaqCfgNoRUs_ = list.size();
	     DaqCfgRUGenericAddrList_.resize(DaqCfgNoRUs_);
	     DaqCfgRUIpAddrList_.resize(DaqCfgNoRUs_);
	     for ( i= 0; i<DaqCfgNoRUs_; i++ ) {
	     	     strcpy(DaqCfgRUIpAddrList_[i].host,list[i]);		     
	     	     DaqCfgRUGenericAddrList_[i] = &DaqCfgRUIpAddrList_[i];
	     }
	     list = Environment::getList(DaqCfgRUIpPortList);
	     for ( i= 0; i<DaqCfgNoRUs_; i++ ) {
	     	     sscanf(list[i],"%d",&(DaqCfgRUIpAddrList_[i].port));

	     }
	     
	     
	     	     
	}
	
	
	
	UAddress DaqCfgSysUAddress_;
	unsigned long DaqCfgRDPMBlockSize_;
	float DaqCfgSafeMargin_;
	int DaqCfgNoEIDs_;

	int DaqCfgNoBUs_;
	int DaqCfgNoRUs_;
	
	vector<tcpAddr>  DaqCfgBUIpAddrList_;
	vector<Addr*>	 DaqCfgBUGenericAddrList_;
	vector<tcpAddr>  DaqCfgRUIpAddrList_;
	vector<Addr*>	 DaqCfgRUGenericAddrList_;
	
	char DaqCfgEVMIpAddr_[256];
	int DaqCfgEVMPort_;
	

};


#endif
