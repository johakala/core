// $Id: daqcEVM.h,v 1.6 2008/07/18 15:28:06 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef __daqcEVM_h__
#define __daqcEVM_h__



#include "XRCAdapter.h"
#include "XEVMAdapter.h"
#include "RUInterface.h"
#include "FUInterface.h"
#include "BSem.h"
#include "vxSys.h"
#include "TcpDispatcherAutoAcceptorTask.h"
#include "TcpXIOConnectorM.h"
#include "XFU.h"
#include "XRU.h"
#include "Loader.h"
#include "TriggerGenerator.h"
#ifdef vxworks
#include "MuxXIOChannel.h"
#endif
#include "TaskAttributes.h"
class RingList {
	
	public:

	RingList(int size) {
		size_ = size;
		first_ = 0;
		last_ = 0;
		v_ = new int [size_];
		currentsize_ = 0;
	}
	
	~RingList() {
		delete [] v_;
	}
	inline int size() {
		return(currentsize_);
	}
	inline void pop_front() {
		if ( first_ != last_ ) {
			first_ = (first_ +1 ) % size_;
			currentsize_--;
			return;
		}
		else
		{
			cerr << "*** Error: RingList is empty!" << endl;
			return;
		}
	}
	
	inline int front() {
		return(v_[first_]);
	}
	
	inline void push_back(int i) {
		if ((( last_ + 1) % size_ ) != first_ ) {
			v_[last_] = i;
			last_ = (last_ + 1) % size_;
			currentsize_++;
			return;
		}
		else
		{
			cerr << "*** Error: RingList is full!" << endl;

		}
	
	}
	
	protected:
	
	int size_;
	int first_;
	int last_;
	int * v_;
	int currentsize_;

};

extern int G_allocated ;
extern int G_cleared ;
class daqcEVM: public Loader, public XEVMAdapter, public XRCAdapter {

//
//  Member Functions:
//

//
//      This class defines the following public member functions:
//
//
	public:
	
	daqcEVM(TcpDispatcherAutoAcceptorTask * dispatcher) {
		dispatcher_ = dispatcher;
		mutex_ = new BSem(BSem::FULL);
		initialized = 0;
	}
	
// Error handling
/*	void allocate (EventProfile* eprofile, DestinationCookie* dcookie) {
		EventIdentifier eid;
		while ( triggerempty() ) { 
			cout << "No triggers available!"<< endl;
			sleep(1); 
		};
		
	 	//TRIGGERs available
		eid = triggerget();
		//cout << "Allocating fragment: " << eid << "to:" << dcookie->addr  << endl;
		

		 
		xfuv_[dcookie->addr & 0x0000ffff]->confirm(&eid);
		
	}
*/
	void allocate (EventProfile* eprofile, DestinationCookie* dcookie) {
		//sleep(1);
		EventIdentifier eid;
		if ( triggernotempty() ) {
	 		//TRIGGERs available
			eid = triggerget();
			//cout << "Allocating fragment: " << eid << "to:" << dcookie->addr  << endl;
			xfuv_[dcookie->addr & 0x0000ffff]->confirm(&eid);
		}
		else {
			//NO TRIGGER availables
			eid = -1;
			cout << "No trigger available: " << eid << endl;
			//sleep(1);
			xfuv_[dcookie->addr & 0x0000ffff]->confirm(&eid);
		}
		G_allocated++;
	}
	 
	void clear (EventIdentifier* eid) {
		//cout << "Clear:" << (int)*eid << endl;
		
		
	 	// DISABLE TRIGGER idput(*eid);
		triggerput(*eid);
		G_cleared++;
	
	}
	
	void trigger() {
	      //new trigger
	     
	      
	      if ( idnotempty() ) {
	              // IDs availables
	              EventIdentifier eid = idget();
		      //cout << "Read out fragment" << eid << endl;
		      //for (int i=0; i< xruv_.size(); i++ )
	              //		xruv_[i]->readout(&eid);
	              triggerput(eid);

	      }
	      else
	      {		      
		      // TRIGGER LOST
	              //cout << "*** Warning: Trigger LOST, apply back pressure" << endl;
#ifdef vxworks
	              //sleep(1);
		      ::taskDelay(0);
#endif
		      
	
	      }
	      
	      return;
	}
	
	
	void init(char * str) {
       		if ( ! initialized )
		{	
			this->loadEnvironment();
			int i;			
			
#ifdef vxworks			
			muxiofu_ = new MuxXIOChannel("dc",0,0x1923,"00:90:27:5d:86:2e"); 
			muxioru_ = new MuxXIOChannel("dc",0,0x1921,"00:90:27:5d:7b:63"); 
#endif
			//
			// Configure event protocol
			//
			triggersq_ = new RingList(DaqCfgNoEIDs_+1);
			identifiersq_ = new RingList(DaqCfgNoEIDs_+1);
			for ( i =0; i< DaqCfgNoEIDs_; i++ )
				//DISABLE TRIGGER idput(i);
				triggerput(i); //No trigger		
			
			
			//
			// Prepare interface to Builder Units according socket configuration
			//
			
			xfuv_.resize(DaqCfgNoBUs_);
			for (  i= 0; i< DaqCfgNoBUs_; i++ ) {
				//dispatcher_->acceptAddress(&DaqCfgBUIpAddrList_[i]); // create a new XIO channel for address
				//xfuv_[i] = new XFU( dispatcher_->getXIO(&DaqCfgBUIpAddrList_[i])); // assign channel to FUi
				xfuv_[i] = new XFU( dispatcher_->getXIO());
				//xfuv_[i] = new XFU(muxiofu_);
			}
  			
			//
			// Prepare interface to Readout Units according socket configuration
			//
			
			xruv_.resize(DaqCfgNoRUs_);
			TcpXIOConnectorM * connector;
			connector = new TcpXIOConnectorM(&DaqCfgRUGenericAddrList_);
			connector->connect();
			for (  i= 0; i< DaqCfgNoRUs_; i++ ) {
				xruv_[i] = new XRU((*connector->getXIO())[i]);
				//xruv_[i] = new XRU(muxioru_);
			}
			
			//
			// Run Trigger generator
			//
			tg_ = new TriggerGenerator();
			tg_->addListener(this);
			TaskAttributes attrib;
			attrib.stacksize(40000);
			attrib.priority(100);
			attrib.name("MuxDisp");
			//tg_->set(&attrib);
			//DISABLE TRIGGER tg_->activate();
			cout << "Trigger generator is activated" << endl;
				
			initialized = 1;
		}
		else
		{
			cout << "***Warning: RU system already initialized" << endl;
		
		}

       }
       
        void shutdown(char * str ) {
       		if ( initialized ) {
       			// TBD should kill everything
			initialized = 0;
		}
        }
	// ID Queue
	inline int idnotempty()
	{
		mutex_->take();
		int size = identifiersq_->size();
		mutex_->give();
		return(size); 
	
	}
	inline void idput(int id)
	{	
		mutex_->take();
		identifiersq_->push_back(id);
		mutex_->give();
	}

	inline int idget()
	{	
		mutex_->take();
		int tmpId = identifiersq_->front();
		identifiersq_->pop_front();
		mutex_->give();
	
		return(tmpId);
	}
	
	// Trigger Queue
	inline int triggernotempty()
	{
		mutex_->take();
		int size = triggersq_->size();
		mutex_->give();
		return(size); 
	
	}
	inline int triggerempty()
	{
		mutex_->take();
		int size = triggersq_->size();
		mutex_->give();
		return(size == 0 ? 1:0); 
	
	}
	inline void triggerput(int id)
	{	
		mutex_->take();
		triggersq_->push_back(id);
		mutex_->give();
	}

	inline int triggerget()
	{	
		mutex_->take();
		int tmpId = triggersq_->front();
		triggersq_->pop_front();
		mutex_->give();
	
		return(tmpId);
	}
	
	protected:
#ifdef vxworks	
	MuxXIOChannel * muxiofu_;
	MuxXIOChannel * muxioru_;
#endif	
	RingList * triggersq_;
	RingList * identifiersq_;
	BSem * mutex_;
	int initialized;
	vector<FUInterface *> xfuv_;
	vector<RUInterface *> xruv_;
	TcpDispatcherAutoAcceptorTask * dispatcher_;
	TriggerGenerator * tg_;
};


#endif
