// $Id: daqcRU.h,v 1.5 2008/07/18 15:28:06 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef __daqcRU_h__
#define __daqcRU_h__


#include "XRUAdapter.h"
#include "XRCAdapter.h"
#include "XEVMAdapter.h"
#include "XFUAdapter.h"
#include "XFU.h"
#include "XMDispatcherLoop.h"
#include "XSDispatcherLoop.h"
#include "XIO.h"
#include "vxtExceptionHandler.h"
#include "Task.h"
#include "TaskGroup.h"
#include "vxioslib.h"
#include "Environment.h"
#include "DAQConfig.h"
#include "XRCAdapter.h"
#include "TcpDispatcherAutoAcceptorTask.h"
#include "Loader.h"

#ifdef vxworks
#include "MuxXIOChannel.h"
#endif

extern int G_sent;

class daqcRU: public Loader, public XRUAdapter, public XRCAdapter{
	public:
	
	daqcRU(TcpDispatcherAutoAcceptorTask * dispatcher) {
		dispatcher_ = dispatcher;
		initialized = 0;
	}
	
	~daqcRU() {
		delete rdpm_;
		delete rdpmOutputHandle_;
		delete outputStream_;
	}
	
        void readout (EventIdentifier* eid) {
       		//cout << "readout:" << hex << *eid << endl;
		rdpmAddr event;
	        event.id = (int)*eid;
		outputStream_->open(&event,vxios::write);
      		*outputStream_ << setl(28) << buf_;   // reserved header fro ZERO copy CACHE 
		*outputStream_ << setl(4) << buf_; 
       		outputStream_->close();
        }

       
       
        void send (EventIdentifier* eid, DestinationCookie* dcookie) {
        	//cout << "send:" << *eid << " to:" << dcookie->addr <<  " on" << dcookie->cookie   << endl; 
		int size;
		FragmentIdentifier fid;
		fid = DaqCfgSysUAddress_ & 0x0000ffff;
		G_sent++;
#ifdef CUT		
		rdpm_->lock(*eid);	
		//rdpm_->ShowTable(*eid);	
		fdata_.data = (rdpm_->open(*eid, &size))->getData();
		//cout << "Got event of size:" << size << "," << hex << (int)fdata_.data << dec << endl;
		fdata_.length = size;
#endif
		//test
		fdata_.data = buf_;
		fdata_.length = 4;
		//test
		//PERF
		//sleep(1);
		//
		// Optimized cache (no memcpy)
		//
		//XMethodStub opti;
		//opti.init(size,(rdpm_->open(*eid, &size))->getData());
		//opti.setMethod(FU_CACHE);
		//opti.addArg(fid);
		//opti.addArg(*eid);
		//opti.addArg(dcookie->addr);
		//opti.addArg(dcookie->cookie);
		//opti.addArg((unsigned long)(size - opti.length()));
		//opti.addZArg(size - opti.length() );
		//XIO * xio = dispatcher_->getXIO();
		//xio->send((char*)opti.bufPtr(),opti.length()) ;
		
		xfuv_[dcookie->addr & 0x0000ffff ]->cache( &fid, eid, dcookie, &fdata_); 
#ifdef CUT			
		rdpm_->unlock(*eid);
#endif
       }  
         
	 
 	void init(char * str) {
       		if ( ! initialized )
		{	
			this->loadEnvironment();
#ifdef vxworks
			muxiofu_ = new MuxXIOChannel("fei",0,0x1923,"00:90:27:5d:86:2e"); // vxwcmsru10
#endif
			//
			// Prepare interface to RDPM
			//	
			
			rdpm_ = new RDPM((char*)0x0, DaqCfgRDPMBlockSize_, DaqCfgNoEIDs_, DaqCfgSafeMargin_);
			rdpmOutputHandle_ = new vxrdpmhandle (rdpm_);
			outputStream_ =  new vxrdpmiostream(static_cast<vxrdpmhandle*>(rdpmOutputHandle_));
			
			//
			// Prepare interface to Builder Units according socket configuration
			//
			
			xfuv_.resize(DaqCfgNoBUs_);
			for ( int i= 0; i< DaqCfgNoBUs_; i++ ) {
				
				xfuv_[i] = new XFU(dispatcher_->getXIO() ); 
				//xfuv_[i] = new XFU(muxiofu_); 
			}
  				
			initialized = 1;
		}
		else
		{
			cout << "***Warning: RU system already initialized" << endl;
		
		}

       }
       
       
       
       void shutdown(char * str ) {
       		if ( initialized ) {
       			// TBD should kill everything
			initialized = 0;
		}
       }

	protected:
	

#ifdef vxworks
	MuxXIOChannel * muxiofu_;
#endif
	vector<FUInterface *> xfuv_;
	char buf_[8192];
	FragmentData  fdata_;
	vxhandle * rdpmOutputHandle_;	
	vxiostream * outputStream_;
	RDPM *rdpm_;
	TcpDispatcherAutoAcceptorTask * dispatcher_;
	int initialized; 

};

#endif
