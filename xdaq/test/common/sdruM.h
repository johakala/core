// $Id: sdruM.h,v 1.5 2008/07/18 15:28:06 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef __sdru_h__
#define __sdru_h__

#include "XRUAdapter.h"
#include "XRCAdapter.h"
#include "XEVMAdapter.h"
#include "XFUAdapter.h"
#include "XFU.h"
#include "XMDispatcherLoop.h"
#include "XSDispatcherLoop.h"
#include "XIO.h"
#include "vxtExceptionHandler.h"
#include "Task.h"
#include "TaskGroup.h"
#include "vxioslib.h"
#include "Environment.h"
#include "DAQConfig.h"


class SDRU: public XRUAdapter{
	public:
	
	SDRU(vector<FUInterface *> * xfuv, int nevents,  UAddress uaddr) {
		uaddr_ = uaddr;
		xfuv_ = xfuv;
		fid_ = 0;
		
		sscanf(Environment::get(DaqCfgSafeMargin),"%f",&DaqCfgSafeMargin_);
		sscanf(Environment::get(DaqCfgRDPMBlockSize),"%x",&DaqCfgRDPMBlockSize_);
		
		rdpm_ = new RDPM((char*)0x0, DaqCfgRDPMBlockSize_, nevents, DaqCfgSafeMargin_);
		rdpmOutputHandle_ = new vxrdpmhandle (rdpm_);
		outputStream_ =  new vxrdpmiostream(static_cast<vxrdpmhandle*>(rdpmOutputHandle_));
	}
	~SDRU() {
		delete rdpm_;
		delete rdpmOutputHandle_;
		delete outputStream_;
	
	}
       void readout (EventIdentifier* eid) {
       		//cout << "readout:" << hex << *eid << endl;
		rdpmAddr event;
	        event.id = (int)*eid;
		outputStream_->open(&event,vxios::write);
      		*outputStream_ << setl(1024) << buf_; 
       		outputStream_->close();
       }

       
       
       void send (EventIdentifier* eid, DestinationCookie* dcookie) {
       		
        	//cout << "send:" << *eid << " to:" << dcookie->addr <<  " on" << dcookie->cookie   << endl; 
		int size;
		
		fid_ = uaddr_;

		rdpm_->lock(*eid);		
	
		fdata_.data = (rdpm_->open(*eid, &size))->getData();
		fdata_.length = size;
		(*xfuv_)[dcookie->addr & 0x0000ffff ]->cache( &fid_, eid, dcookie, &fdata_); 
		
		rdpm_->unlock(*eid);
       }  
         
	 

	protected:
	
	FragmentIdentifier fid_;
	vector<FUInterface *> * xfuv_;
	char buf_[8192];
	FragmentData  fdata_;
	vxhandle * rdpmOutputHandle_;	
	vxiostream * outputStream_;
	RDPM *rdpm_;
	UAddress uaddr_;
	float DaqCfgSafeMargin_;
	unsigned long DaqCfgRDPMBlockSize_;	
};

#endif
