// $Id: XConfigurator.h,v 1.3 2008/07/18 15:28:06 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

//
//  File Name:
//
//      XConfigurator.h
//
//  File Type:
//
//      C++ Header file
//
//  Creative Authors:
//
//      Luciano B. ORSINI
//      CERN, EP Division
//      CMD group
//      CMS TriDAS
//      Compact Muon Solenoid - Trigger and Data Acquisition
//      Geneva, Switzerland
//      Tel: (+41) 22 76 71615 
//      E-mail:Luciano.Orsini@cern.ch
//      
//
//  Change History:
//
//      $Log: XConfigurator.h,v $
//      Revision 1.3  2008/07/18 15:28:06  gutleber
//      Changed Copyright date 2000-2009
//
//      Revision 1.2  2004/09/02 09:57:34  xdaq
//      File header
//
//      Revision 1.1  1999/10/05 13:38:10  lorsini
//      Reference: LO
//      Software Change:
//      	Source created.
//
//
//
//  Version Number
//
//      @(#)$Id: XConfigurator.h,v 1.3 2008/07/18 15:28:06 gutleber Exp $
//
//

//  ------------------------------------------------------------------------
//  ------------------  Pre-processor Control Directives  ------------------
//  ------------------------------------------------------------------------
 
// The following pre-processor directives ensure that spurious errors will not
// be generated in the EventIdentifier that this header file is included several times.
 
#ifndef __XConfigurator_h__
#define __XConfigurator_h__

//  ------------------------------------------------------------------------ 
//  ------------------------  Include Header Files  ------------------------
//  ------------------------------------------------------------------------
 
// Include the following system header files:

#include "iostream.h"
#include "strstream.h"
#include "stdlib.h"
  
// Include the following vxRU  header files:
#include "vxSys.h"
#include "XRCAdapter.h"

//  ------------------------------------------------------------------------ 
//  ----------------------  Public Class Declarations  --------------------- 
//  ------------------------------------------------------------------------ 
//

//
//  Class: 
//
//      
//
//  Superclasses:
//
//     NONE 
//
//
// Class Arguments
//
//
//  Description:
//
//
//    This is a listener that can be added to the used dispatcher to
//    set the proces environment configuration.
//    It can be used as follows:
//
//    XConfigurator c;
// 
//    d.addListener(&c);
//
//    where d is an event dispatcher.
//

 
class XConfigurator: virtual public XRCAdapter {
	public:
       void config (char * str) {
       
		strstream envs(str,strlen(str),ios::in);
		char buf[8192];
		cout << "Reset configuration..." << endl; 
		while (! envs.eof())
		{
			envs.getline(buf,8192);
			cout << "Set var:" << endl;
			putenv(buf);
		}
       } 
       
};

//  ------------------------------------------------------------------------ 
//  ----------------------  Public Class Declarations  --------------------- 
//  ------------------------------------------------------------------------ 
//
//  ------------------------------------------------------------------------
//  ------------------  Pre-processor Control Directives  ------------------
//  ------------------------------------------------------------------------
 
// Terminate the #if ... #endif pre-processor control directive located at the
// start of this header file.
 
#endif /* ifndef  __XConfigurator_h__ */

