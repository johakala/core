// $Id: DAQConfig.h,v 1.5 2008/07/18 15:28:06 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef __DAQConfig_h__
#define __DAQConfig_h__

//-----------------------------------------------------------------------------
// General
//-----------------------------------------------------------------------------


#define DaqCfgSysUAddress   "DAQSYSUADDRESS"
#define DaqCfgNoEIDs        "DAQNOEIDS"

//-----------------------------------------------------------------------------
// RU
//-----------------------------------------------------------------------------

#define DaqCfgRUIpAddrList     "DAQRUADDR"
#define DaqCfgRUIpPortList     "DAQRUPORT"
#define DaqCfgRDPMBlockSize    "DAQRUMBLOCKSIZE"
#define DaqCfgSafeMargin       "DAQRUMSAFEMARGIN"

//-----------------------------------------------------------------------------
// BU
//-----------------------------------------------------------------------------

#define DaqCfgBUIpAddrList     "DAQBUADDR"
#define DaqCfgBUIpPortList     "DAQBUPORT"

//-----------------------------------------------------------------------------
// FU
//-----------------------------------------------------------------------------

#define DaqCfgFUIpAddrList      "DAQFUADDR"
#define DaqCfgFUIpPortList      "DAQFUPORT"

//-----------------------------------------------------------------------------
// EVM
//-----------------------------------------------------------------------------

#define DaqCfgEVMIpAddr       "DAQEVMADDR"
#define DaqCfgEVMPort         "DAQEVMPORT"

//-----------------------------------------------------------------------------
// RC
//-----------------------------------------------------------------------------

#define DaqCfgRCIpAddr         "DAQRCADDR"
#define DaqCfgRCPort           "DAQRCPORT"



#endif
