// --  DOMAIN:
// --    XDAQ
// --
// --  FACILITY:
// --    An example server that demonstrates how to instantiate a
// --    XMI dispatcher driven server in the DAQ domain. For communication
// --    UDP vxiostreams are used.
// --    This file may serve as a programming example for DAQ developers.
// --
// --  ABSTRACT:
// --    TBD.
// --
// --  VERSION:
// --    1.0
// --
// --  MODIFICATION:
// --    Date           Purpose                               Author
// --    26-Jul-1999    Creation.                             JG
// --

//
// Include the client interface
//
#include "XFU.h"
#include "XRU.h"
//
// Include the communication library 
//
#ifdef vxtoolbox
#include "vxSys.h"
#include "vxioslib.h"
#endif

//
// Include other useful stuff
//
#include "iostream.h"
#ifndef vxworks
#include "strings.h"
#endif
#include "XIO.h"
#include "XListener.h"

#ifdef vxworks
int Main(char * str)
#else
int main(int argc, char* argv[])
#endif
{
#ifndef vxworks
  if (argc < 2) {
    cout << argv[0] << ": Please supply destination IP address" << endl;
    return 1;
  }
#endif
  //
  // Open a UDP stream
  //
  vxhandle*   client = new vxudphandle();
  vxiostream* stream = new vxdgramiostream(static_cast<vxudphandle*>(client));

  char buf[8192];
  udpAddr destination;
  destination.port = 20200; // send on port 20200
#ifndef vxworks  
  strcpy(destination.host, argv[1]);
#else
  strcpy(destination.host, str);
#endif
  XIO  *xio = new XIO(stream,&destination);
  //
  // Create the XFU stub. The stream is connected inside
  //
  XFU fu(xio,0);
  XRU ru(xio,0);

  EventIdentifier eid = 3000;

  //
  // Make an XMI call
  //
  fu.confirm(&eid);

  eid++;

  FragmentIdentifier frag = 4000;

  void* p = new char[100];
  FragmentData* fragment = (FragmentData*) p;
  strcpy (&(fragment->data[0]), "Hallo Hannes");
  fragment->length = strlen(&(fragment->data[0]));
  
  DestinationCookie toAddr;
  toAddr.addr   = 100;
  toAddr.cookie = 200;

  //
  // Another XMI call
  //
  fu.cache ( &frag, &eid, &toAddr, fragment );
  ru.readout ( &eid );

  delete client;
  delete stream;
}

