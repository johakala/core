// $Id: sdru.h,v 1.6 2008/07/18 15:28:06 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef __sdru_h__
#define __sdru_h__

#include "XRUAdapter.h"
#include "XRCAdapter.h"
#include "XEVMAdapter.h"
#include "XFUAdapter.h"
#include "XFU.h"
#include "XMDispatcherLoop.h"
#include "XSDispatcherLoop.h"
#include "XIO.h"
#include "vxtExceptionHandler.h"
#include "Task.h"
#include "TaskGroup.h"
#include "TcpDispatcherAcceptorTaskS.h"
#include "vxioslib.h"
class SDRU: public XRUAdapter {
	public:
	
	SDRU(XFU * xfu, int nevents) {
		xfu_ = xfu;
		fid_ = 0;
		rdpm_ = new RDPM((char*)0x0, 0x8000, nevents, 0.0);
		rdpmOutputHandle_ = new vxrdpmhandle (rdpm_);
		outputStream_ =  new vxrdpmiostream(static_cast<vxrdpmhandle*>(rdpmOutputHandle_));
	}
	~SDRU() {
		delete rdpm_;
		delete rdpmOutputHandle_;
		delete outputStream_;
	
	}
       void readout (EventIdentifier* eid) {
       		cout << "readout:" << hex << *eid << endl;
		rdpmAddr event;
	        event.id = (int)*eid;
		outputStream_->open(&event,vxios::write);
      		*outputStream_ << setl(1024) << buf_; 
       		outputStream_->close();
       }

     
       
       void send (EventIdentifier* eid, DestinationCookie* dcookie) {
       		
        	cout << "send:<" << *eid << "," << dcookie->addr <<  "," << dcookie->cookie << ">"  << endl; 
		char buf[8];
		fdata_.length = 0x4;
		fdata_.data = buf_;
		fdata_.data[0] = 0xf;
		xfu_->cache(&fid_,eid,dcookie,&fdata_);
		
       }    
	protected:
	FragmentIdentifier fid_;
	XFU * xfu_;
	char buf_[8192];
	FragmentData  fdata_;
	vxhandle * rdpmOutputHandle_;	
	vxiostream * outputStream_;
	RDPM *rdpm_;
};

#endif
