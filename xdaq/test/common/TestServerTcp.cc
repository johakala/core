#include "XRUAdapter.h"
#include "XRCAdapter.h"
#include "XEVMAdapter.h"
#include "XFUAdapter.h"
#include "XMDispatcherLoop.h"
#include "XSDispatcherLoop.h"
#include "XIO.h"
#include "vxtExceptionHandler.h"
#include "Task.h"
#include "TaskGroup.h"
#include "TcpDispatcherAcceptorTaskS.h"




class RU: public XRUAdapter, public XRCAdapter
{	 
       void readout (EventIdentifier* eid) {
       	cout << "readout:" << hex << *eid << endl;
       
       }

       void start (char * str) {
       	cout << "config:" << str << endl;
       }
       
       void stop (char * str) {
       	cout << "stop:" << str << endl;
       }     

       

};

class FU: public XRCAdapter, public XFUAdapter
{
       void cache (FragmentIdentifier* fid, EventIdentifier* eid,  DestinationCookie* dcookie, FragmentData* fdata)
       {
       	cout << "cache:" << hex << *fid << *eid << dcookie->addr << dcookie->cookie <<  fdata->length <<
	 (int)fdata->data[0] << endl;
       }
       
       void start (char * str) {
       	cout << "config:" << str << endl;
       }
       
       void stop (char * str) {
       	cout << "stop:" << str << endl;
       }     
};

extern "C" void Main(char * str)
{
  
  RU * ru = new RU();
  FU * fu = new FU();
  
  TcpDispatcherAcceptorTaskS * d = new TcpDispatcherAcceptorTaskS(str,20200);
  TaskGroup g;
  d->initTaskGroup(&g);
  
  d->addListener(ru);
  d->addListener(fu);
  
  
  d->activate();
  g.join();
  
}


//
// The main program
//
#ifndef vxworks
int main(int argc, char* argv[])
{
	Main(argv[1]);
 	
}
#endif
