// $Id: sdfuM.h,v 1.4 2008/07/18 15:28:06 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "XRUAdapter.h"
#include "XRCAdapter.h"
#include "XEVMAdapter.h"
#include "XFUAdapter.h"
#include "XRU.h"
#include "XEVM.h"
#include "XMDispatcherLoop.h"
#include "XSDispatcherLoop.h"
#include "XIO.h"
#include "vxtExceptionHandler.h"
#include "Task.h"
#include "TaskGroup.h"
#include "TcpXIOConnectorM.h"
#include "BSem.h"

class SDispatcherTask: public Task, public XSDispatcherLoop {

	public:
	
	SDispatcherTask(XIO* xio):XSDispatcherLoop(xio) {
	
	}
	
	int svc() {
	 	Try
  		{
  			this->foreverloop();
  		}
  		Catch(vxException e)
  		{
  			cout << e;
  		}
		return(0);
	
	}


};
class SDFU: public XFUAdapter, public XRCAdapter {
	public:
	
	SDFU(vector<RUInterface *> * ruv, EVMInterface * evm, UAddress uaddr) {
		ruv_ = ruv;
		evm_ = evm;
		stop_ = 0;
		counter_ = 0;
		uaddr_ = uaddr;
		mutex_ = new BSem(BSem::FULL);
		
	}
	

       void start (char * str) {
       		mutex_->take();
       		cout << "start:" << str << " on:" << uaddr_<< endl;
		EventIdentifier eid = 10;
		DestinationCookie dcookie;
		dcookie.addr = uaddr_;
		dcookie.cookie = uaddr_;
		//Request first fragment
		EventProfile eprofile = 0xff;
		evm_->allocate(&eid,&dcookie);
		mutex_->give();
       }
       
       void stop (char * str) {
       		cout << "stop:" << str << endl;
		stop_ = 1;
       }        
       
       void confirm (EventIdentifier* eid) {
       		mutex_->take();
       		//cout << "confirm:" << *eid << endl;
		DestinationCookie dcookie;
		dcookie.addr = uaddr_;
		dcookie.cookie = uaddr_;
		fragmentv = 0;

		for (int i =0; i < ruv_->size(); i++ )
			(*ruv_)[i]->send(eid,&dcookie);	
		mutex_->give();
       }
       
       
       void cache (FragmentIdentifier* fid, EventIdentifier* eid,  DestinationCookie* dcookie, FragmentData* fdata) {
       		mutex_->take();
       		//cout << "cache: got fragment:<" << *fid << "," << *eid << " from:" << dcookie->addr << " on:" << dcookie->cookie << ">"  << endl;
		//request next
		EventProfile eprofile = 0xff;
		if ( stop_ ) return;
		fragmentv++;
		//cout << "Clear previous Fragment" << endl;
		if ( fragmentv == 2 ) {
			fragmentv = 0;
			evm_->clear(eid);
			//cout << "Allocate next Fragment" << endl;
			evm_->allocate(&eprofile,dcookie);
		}
		
		counter_++;
		if ((counter_ % 100 ) == 0 )
			cout << "Got 100 events" << endl;
		
       		mutex_->give();
       }       
	protected:
	int counter_;
	int stop_;
	vector<RUInterface *> * ruv_;
	EVMInterface * evm_;
	BSem * mutex_;
	UAddress uaddr_;
	int fragmentv;
};


