// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.			                			 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, A. Forrest										 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                        	 *
 * For the list of contributors see CREDITS.   			        		 *
 *************************************************************************/

#ifndef _eventing_api_Publisher_h_
#define _eventing_api_Publisher_h_

#include "xdaq/Object.h"

#include "toolbox/mem/Reference.h"

#include "xdata/Properties.h"

#include "eventing/api/exception/Exception.h"

namespace eventing
{
	namespace core
	{

		class PublisherInterface;

	}
	namespace api
	{

		class Publisher : public  xdaq::Object
		{
			public:

				Publisher (xdaq::Application * owner) throw (xdaq::exception::Exception);
				Publisher (xdaq::Application * owner, eventing::core::PublisherInterface * p) throw (xdaq::exception::Exception);

				void publish (const std::string & topic, toolbox::mem::Reference * ref, xdata::Properties & plist) throw (eventing::api::exception::Exception);

			protected:

				Publisher (const Publisher& other) {};
				Publisher() {};

				eventing::core::PublisherInterface * publisher_;
		};
	}
}

#endif
