// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

//
// Version definition for MStreamIO
//
#ifndef _eventing_api_Version_h_
#define _eventing_api_Version_h_

#include "config/PackageInfo.h"
// !!! Edit this line to reflect the latest package version !!!

#define EVENTINGAPI_VERSION_MAJOR 4
#define EVENTINGAPI_VERSION_MINOR 0
#define EVENTINGAPI_VERSION_PATCH 0
// If any previous versions available E.g. #define EVENTINGAPI_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#undef EVENTINGAPI_PREVIOUS_VERSIONS


//
// Template macros
//
#define EVENTINGAPI_VERSION_CODE PACKAGE_VERSION_CODE(EVENTINGAPI_VERSION_MAJOR,EVENTINGAPI_VERSION_MINOR,EVENTINGAPI_VERSION_PATCH)
#ifndef EVENTINGAPI_PREVIOUS_VERSIONS
#define EVENTINGAPI_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(EVENTINGAPI_VERSION_MAJOR,EVENTINGAPI_VERSION_MINOR,EVENTINGAPI_VERSION_PATCH)
#else 
#define EVENTINGAPI_FULL_VERSION_LIST  EVENTINGAPI_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(EVENTINGAPI_VERSION_MAJOR,EVENTINGAPI_VERSION_MINOR,EVENTINGAPI_VERSION_PATCH)
#endif 

namespace eventingapi
{
	const std::string package  =  "eventingapi";
	const std::string versions =  EVENTINGAPI_FULL_VERSION_LIST;
	const std::string summary = "Publisher/Subscriber XDAQ interface";
	const std::string description = "";
	const std::string authors = "Luciano Orsini, Andrea Petrucci, Andy Forrest";
	const std::string link = "http://xdaqwiki.cern.ch/index.php/Power_Pack";
	config::PackageInfo getPackageInfo();
	void checkPackageDependencies() throw (config::PackageInfo::VersionException);
	std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
