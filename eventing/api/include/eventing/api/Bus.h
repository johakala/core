// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.			                			 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, A. Forrest										 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                        	 *
 * For the list of contributors see CREDITS.   			        		 *
 *************************************************************************/

#ifndef _eventing_api_EventingBus_h_
#define _eventing_api_EventingBus_h_

#include "xdaq/Object.h"

#include "eventing/api/exception/Exception.h"
#include "eventing/api/Publisher.h"
#include "eventing/api/Subscriber.h"

#include "toolbox/ActionListener.h"
#include "toolbox/EventDispatcher.h"

namespace eventing
{
	namespace api
	{
		class Member;

		class Bus : public eventing::api::Publisher, public eventing::api::Subscriber, public toolbox::ActionListener, public toolbox::EventDispatcher
		{
			friend class Member;

			public:

				std::string getBusName () const;

				// These functions return true if there is an associated publisher or subscriber. They do NOT know about the status of the associated p/s
				bool canPublish() const;
				bool canSubscribe() const;


				//const Bus& operator= (const Bus& other) const;

			 protected:

					void actionPerformed(toolbox::Event& e);


					Bus (xdaq::Application * owner, std::string busName, eventing::core::PublisherInterface* p, eventing::core::SubscriberInterface* s) throw (xdaq::exception::Exception);
			        Bus (const Bus& other);
			        Bus& operator=(const Bus& other);
			        Bus();


			private:

				std::string name_;

				bool publisherReady_;

		};
	}
}

#endif
