// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.			                			 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, A. Forrest										 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                        	 *
 * For the list of contributors see CREDITS.   			        		 *
 *************************************************************************/

#ifndef _eventing_api_Member_h_
#define _eventing_api_Member_h_

#include "xdaq/Application.h"

#include "eventing/api/exception/Exception.h"
#include "eventing/api/Bus.h"

#include "toolbox/BSem.h"

namespace eventing
{
	namespace api
	{
		class Member : public xdaq::Object
		{
			public:

				Member (xdaq::Application * owner) throw (xdaq::exception::Exception);

				Bus& getEventingBus (const std::string & name) const throw (eventing::api::exception::Exception) ;

				std::set<std::string> getAvailableBuses() const;

				// standardized HTML output for this eventing::member, can be embedded in an application hyperdaq page (e.g. in a tab)
				std::string busesToHTML ();

			protected:

				std::map<std::string, eventing::api::Bus*> eventingBuses_;

			private:

				mutable toolbox::BSem mutex_;
		};
	}
}

#endif
