// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.			                			 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, A. Forrest										 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                        	 *
 * For the list of contributors see CREDITS.   			        		 *
 *************************************************************************/

#include "eventing/api/Publisher.h"
#include "eventing/core/PublisherInterface.h"
#include "eventing/core/exception/Exception.h"

#include "xdaq/ApplicationRegistry.h"

eventing::api::Publisher::Publisher (xdaq::Application * owner) throw (xdaq::exception::Exception)
	: xdaq::Object(owner)
{
	std::vector<xdaq::Application*> apps = owner->getApplicationContext()->getApplicationRegistry()->getApplications("service", eventing::core::PublisherService);

	if (apps.size() == 0)
	{
		std::stringstream msg;
		msg << eventing::core::PublisherService << " application not found in this executive";
		XCEPT_RAISE(xdaq::exception::Exception, msg.str());
	}

	publisher_ = dynamic_cast<eventing::core::PublisherInterface*>(apps[0]);
}

eventing::api::Publisher::Publisher (xdaq::Application * owner, eventing::core::PublisherInterface * publisher) throw (xdaq::exception::Exception)
	: xdaq::Object(owner), publisher_(publisher)
{
}

void eventing::api::Publisher::publish (const std::string & topic, toolbox::mem::Reference * ref, xdata::Properties & plist) throw (eventing::api::exception::Exception)
{
	if (publisher_ != 0)
	{
		try
		{
			publisher_->publish(topic, ref, plist);
		}
		catch (eventing::core::exception::Exception & e)
		{
			XCEPT_RETHROW(eventing::api::exception::Exception, "Failed to publish", e);
		}
	}
	else
	{
		XCEPT_RAISE(eventing::api::exception::Exception, "Cannot find publisher core support ( not installed )");
	}
}
