// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.			                			 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, A. Forrest										 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                        	 *
 * For the list of contributors see CREDITS.   			        		 *
 *************************************************************************/

#include "eventing/api/Subscriber.h"

#include "eventing/core/SubscriberInterface.h"
#include "eventing/core/exception/Exception.h"

#include "xdaq/ApplicationRegistry.h"

eventing::api::Subscriber::Subscriber (xdaq::Application * owner) throw (xdaq::exception::Exception)
	: xdaq::Object(owner)
{
	std::vector<xdaq::Application*> apps = owner->getApplicationContext()->getApplicationRegistry()->getApplications("service", eventing::core::SubscriberService);

	if (apps.size() == 0)
	{
		std::stringstream msg;
		msg << eventing::core::SubscriberService << " application not found in this executive";
		XCEPT_RAISE(xdaq::exception::Exception, msg.str());
	}

	subscriber_ = dynamic_cast<eventing::core::SubscriberInterface*>(apps[0]);
}

eventing::api::Subscriber::Subscriber (xdaq::Application * owner, eventing::core::SubscriberInterface * subscriber) throw (xdaq::exception::Exception)
	: xdaq::Object(owner), subscriber_(subscriber)
{
}

void eventing::api::Subscriber::subscribe (const std::string & topic) throw (eventing::api::exception::Exception)
{
	if (subscriber_ != 0)
	{
		try
		{
			subscriber_->subscribe(topic, toolbox::toString("%d", this->getOwnerApplication()->getApplicationDescriptor()->getLocalId()));
		}
		catch (eventing::core::exception::Exception & e)
		{
			XCEPT_RETHROW(eventing::api::exception::Exception, "Failed to subscribe", e);
		}
	}
	else
	{
		XCEPT_RAISE(eventing::api::exception::Exception, "Cannot find subscriber core support ( not installed )");
	}
}

void eventing::api::Subscriber::unsubscribe (const std::string & topic) throw (eventing::api::exception::Exception)
{
	if (subscriber_ != 0)
	{
		try
		{
			subscriber_->unsubscribe(topic, toolbox::toString("%d", this->getOwnerApplication()->getApplicationDescriptor()->getLocalId()));
		}
		catch (eventing::core::exception::Exception & e)
		{
			XCEPT_RETHROW(eventing::api::exception::Exception, "Failed to unsubscribe", e);
		}
	}
	else
	{
		XCEPT_RAISE(eventing::api::exception::Exception, "Cannot find subscriber core support ( not installed )");
	}
}
