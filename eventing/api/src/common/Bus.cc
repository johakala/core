// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.	               			 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, A. Forrest					 *
 *                                                                       *
 * For the licensing terms see LICENSE.	                        	 *
 * For the list of contributors see CREDITS.  	        		 *
 *************************************************************************/

#include "eventing/api/Bus.h"
#include "eventing/core/PublisherInterface.h"
#include "eventing/core/SubscriberInterface.h"

eventing::api::Bus::Bus (xdaq::Application * owner, std::string busName, eventing::core::PublisherInterface * publisher, eventing::core::SubscriberInterface * subscriber) throw (xdaq::exception::Exception)
	: eventing::api::Publisher(owner, publisher), eventing::api::Subscriber(owner, subscriber), name_(busName), publisherReady_(false)
{
	if (publisher != 0)
	{
		publisher->addActionListener(this);
	}
}

std::string eventing::api::Bus::getBusName () const
{
	return name_;
}

bool eventing::api::Bus::canPublish() const
{
	return (publisher_ != 0 && publisherReady_);
}

bool eventing::api::Bus::canSubscribe() const
{
	return (subscriber_ != 0);
}

void eventing::api::Bus::actionPerformed(toolbox::Event& e)
{
	if (e.type() == "eventing::core::PublisherReadyEvent")
	{
		publisherReady_ = true;

		// dispatch to listening application
		toolbox::Event event ("eventing::api::BusReadyToPublish", this);
		this->fireEvent(event);
		// user code
		// std::string busname = dynamic_cast<eventing::api::Bus*>(event.originator())->getBusName();
	}
}
