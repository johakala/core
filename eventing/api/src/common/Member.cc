// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.			                			 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, A. Forrest										 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                        	 *
 * For the list of contributors see CREDITS.   			        		 *
 *************************************************************************/

#include "eventing/api/Member.h"
#include "eventing/api/Bus.h"

#include "eventing/core/PublisherInterface.h"
#include "eventing/core/SubscriberInterface.h"

#include "toolbox/task/Guard.h"

#include "xdaq/ApplicationRegistry.h"

#include "cgicc/Cgicc.h"
#include "cgicc/HTMLClasses.h"

eventing::api::Member::Member (xdaq::Application * owner) throw (xdaq::exception::Exception)
	: xdaq::Object(owner), mutex_(toolbox::BSem::FULL, false)
{
	std::map<std::string, eventing::core::PublisherInterface*> publishers;
	std::map<std::string, eventing::core::SubscriberInterface*> subscribers;

	std::set < std::string > groups;

	try
	{
		//std::vector<xdaq::Application*> getApplications
		std::vector<xdaq::Application*> pubApps = this->getOwnerApplication()->getApplicationContext()->getApplicationRegistry()->getApplications("service", eventing::core::PublisherService);
		std::vector<xdaq::Application*> subApps = this->getOwnerApplication()->getApplicationContext()->getApplicationRegistry()->getApplications("service", eventing::core::SubscriberService);

		for (std::vector<xdaq::Application*>::iterator i = pubApps.begin(); i != pubApps.end(); i++)
		{
			const xdaq::ApplicationDescriptor* desc = (*i)->getApplicationDescriptor();
			std::string bus = desc->getAttribute("bus");

			if (bus != "")
			{
				publishers[bus] = dynamic_cast<eventing::core::PublisherInterface*>(*i);
				groups.insert(bus);
			}
		}
		for (std::vector<xdaq::Application*>::iterator i = subApps.begin(); i != subApps.end(); i++)
		{
			const xdaq::ApplicationDescriptor* desc = (*i)->getApplicationDescriptor();
			std::string bus = desc->getAttribute("bus");

			if (bus != "")
			{
				subscribers[bus] = dynamic_cast<eventing::core::SubscriberInterface*>(*i);
				groups.insert(bus);
			}
		}

		for (std::set<std::string>::iterator i = groups.begin(); i != groups.end(); i++)
		{
			eventing::core::PublisherInterface* publisher = 0;
			eventing::core::SubscriberInterface* subscriber = 0;

			if (publishers.find(*i) != publishers.end())
			{
				publisher = publishers[*i];
			}

			if (subscribers.find(*i) != subscribers.end())
			{
				subscriber = subscribers[*i];
			}

			eventing::api::Bus* bus = new eventing::api::Bus(owner, *i, publisher, subscriber);
			eventingBuses_[*i] = bus;

			std::stringstream msg;
			msg << "Creating eventing Bus - " << *i;

			// if it can't publish, it must be able to subscribe, and vice versa
			if (publisher == 0)
			{
				msg << " - subscribe only";
			}
			else if (subscriber == 0)
			{
				msg << " - publish only";
			}

			LOG4CPLUS_DEBUG(owner->getApplicationLogger(), msg.str());
		}
	}
	catch (xdaq::exception::Exception & e)
	{
		std::stringstream msg;
		msg << "Failed to setup eventing Buses";
		XCEPT_RETHROW(xdaq::exception::Exception, msg.str(), e);
	}
}

eventing::api::Bus& eventing::api::Member::getEventingBus (const std::string & name) const throw (eventing::api::exception::Exception)
{
	toolbox::task::Guard < toolbox::BSem > guard(mutex_);

	std::map<std::string, eventing::api::Bus*>::const_iterator i = eventingBuses_.find(name);

	if (i != eventingBuses_.end())
	{
		return *((*i).second);
	}

	std::stringstream msg;
	msg << "Cannot find eventing Bus for name '" << name << "'";
	XCEPT_RAISE(eventing::api::exception::Exception, msg.str());
}

std::set<std::string> eventing::api::Member::getAvailableBuses () const
{
	toolbox::task::Guard < toolbox::BSem > guard(mutex_);

	std::set < std::string > buses;
	for (std::map<std::string, eventing::api::Bus*>::const_iterator i = eventingBuses_.begin(); i != eventingBuses_.end(); i++)
	{
		buses.insert((*i).first);
	}
	return buses;
}

std::string eventing::api::Member::busesToHTML ()
{
	std::stringstream out;

	out << cgicc::table().set("class", "xdaq-table");

	out << cgicc::thead();

	out << cgicc::tr();
	out << cgicc::th("Name");
	out << cgicc::th("Publisher");
	out << cgicc::th("Subscriber");
	out << cgicc::tr() << std::endl;

	out << cgicc::thead();
	out << cgicc::tbody();

	for (std::map<std::string, eventing::api::Bus*>::iterator i = eventingBuses_.begin(); i != eventingBuses_.end(); i++)
	{
		eventing::api::Bus* bus = (*i).second;

		std::vector<xdaq::Application*> apps = this->getOwnerApplication()->getApplicationContext()->getApplicationRegistry()->getApplications("bus", (*i).first);

		std::string pubURL;
		std::string subURL;

		out << cgicc::tr();
		out << cgicc::td((*i).first);
		if (bus->canPublish())
		{
			std::stringstream url;
			for (std::vector<xdaq::Application*>::iterator i = apps.begin(); i != apps.end(); i++)
			{
				if ((*i)->getApplicationDescriptor()->getAttribute("service") == eventing::core::PublisherService)
				{
					url << (*i)->getApplicationDescriptor()->getContextDescriptor()->getURL() << "/" << (*i)->getApplicationDescriptor()->getURN();
					break;
				}
			}
			std::stringstream text;
			text << "<a href=\"" << url.str() << "\">Active</a>";
			out << cgicc::td(text.str());
		}
		else
		{
			out << cgicc::td("Inactive");
		}
		if (bus->canSubscribe())
		{
			std::stringstream url;
			for (std::vector<xdaq::Application*>::iterator i = apps.begin(); i != apps.end(); i++)
			{
				if ((*i)->getApplicationDescriptor()->getAttribute("service") == eventing::core::SubscriberService)
				{
					url << (*i)->getApplicationDescriptor()->getContextDescriptor()->getURL() << "/" << (*i)->getApplicationDescriptor()->getURN();
					break;
				}
			}
			std::stringstream text;
			text << "<a href=\"" << url.str() << "\">Active</a>";
			out << cgicc::td(text.str());
		}
		else
		{
			out << cgicc::td("Inactive");
		}
		out << cgicc::tr() << std::endl;
	}

	out << cgicc::tbody();
	out << cgicc::table();

	return out.str();
}
