// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.			                			 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, A. Forrest										 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                        	 *
 * For the list of contributors see CREDITS.   			        		 *
 *************************************************************************/

#ifndef _eventing_core_Publisher_h_
#define _eventing_core_Publisher_h_

#include "xdaq/Application.h"
#include "xgi/framework/UIManager.h"

#include "xdaq/ApplicationDescriptor.h"

#include "eventing/core/PublisherInterface.h"

#include "toolbox/mem/Reference.h"
#include "toolbox/EventDispatcher.h"

#include "xdata/Properties.h"

#include <string>
#include "xdata/String.h"
#include "xdata/Vector.h"

//#include "b2in/utils/MessengerCacheListener.h"
//#include "b2in/utils/MessengerCache.h"
#include "pt/Messenger.h"

#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"
#include "xgi/Utils.h"
#include "xgi/Input.h"
#include "xgi/Output.h"

namespace eventing
{
	namespace core
	{
		class Publisher : public xdaq::Application, public xgi::framework::UIManager, public eventing::core::PublisherInterface, public toolbox::ActionListener
		{
			public:

				XDAQ_INSTANTIATOR();

				Publisher (xdaq::ApplicationStub* c) throw (xdaq::exception::Exception);
				~Publisher ();

				void publish (const std::string & topic, toolbox::mem::Reference * ref, xdata::Properties & plist) throw (eventing::core::exception::Exception);

				void actionPerformed (toolbox::Event& e);

				void Default (xgi::Input *in, xgi::Output *out) throw (xgi::exception::Exception);

				void addActionListener (toolbox::ActionListener * l);

			protected:

				bool publishFailed (xcept::Exception& e, void * context);

				toolbox::BSem mutex_;

				xdata::Vector<xdata::String> eventings_;

				size_t currentIndex_;

				std::vector<pt::Messenger::Reference> messengers_;

				toolbox::exception::HandlerSignature * publishFailedHandler_;

				toolbox::EventDispatcher dispatcher_;

		};
	}
}

#endif
