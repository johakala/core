// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.			                			 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, A. Forrest										 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                        	 *
 * For the list of contributors see CREDITS.   			        		 *
 *************************************************************************/

#ifndef _eventing_core_Subscriber_h_
#define _eventing_core_Subscriber_h_

#include "xdaq/Application.h"
#include "xgi/framework/UIManager.h"

#include "eventing/core/SubscriberInterface.h"

#include <string>
#include <map>
#include <set>

#include "toolbox/task/Timer.h"
#include "toolbox/task/TimerFactory.h"

#include "xdata/Event.h"
#include "xdata/ActionListener.h"
#include "xdata/Vector.h"
#include "xdata/String.h"

#include "b2in/utils/MessengerCacheListener.h"
#include "b2in/utils/MessengerCache.h"

#include "b2in/nub/exception/Exception.h"

#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"
#include "xgi/Utils.h"
#include "xgi/Input.h"
#include "xgi/Output.h"

namespace eventing
{
	namespace core
	{
		class Subscriber : public xdaq::Application, public xgi::framework::UIManager, public eventing::core::SubscriberInterface, public toolbox::ActionListener, public xdata::ActionListener, public b2in::utils::MessengerCacheListener, public toolbox::task::TimerListener
		{
			public:

				XDAQ_INSTANTIATOR();

				Subscriber (xdaq::ApplicationStub* c) throw (xdaq::exception::Exception);
				~Subscriber();

				void subscribe (const std::string & topic, const std::string & lid) throw (eventing::core::exception::Exception);
				void unsubscribe (const std::string & topic, const std::string & lid) throw (eventing::core::exception::Exception);

				void Default(xgi::Input *in, xgi::Output *out) throw (xgi::exception::Exception);

				void onMessage (toolbox::mem::Reference * ref, xdata::Properties & plist) throw (b2in::nub::exception::Exception);

				protected:

				typedef std::pair<std::string, std::string> Key;

				void refreshSubscriptionsToEventing () throw (b2in::utils::exception::Exception);

				void timeExpired (toolbox::task::TimerEvent& e);

				void actionPerformed (xdata::Event& event);
				void actionPerformed (toolbox::Event& e);

				toolbox::BSem mutex_;

				xdata::Vector<xdata::String> eventings_;

				xdata::String subscriptionExpire_;
				xdata::String renewRate_;
				xdata::String connectionRetryNum_;

				std::map<Key, xdata::Properties> subscriptions_;
				std::set<Key> topics_;

				void asynchronousExceptionNotification(xcept::Exception& e);

				b2in::utils::MessengerCache* messengerCache_;

			};}
	}

#endif
