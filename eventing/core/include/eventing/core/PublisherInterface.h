// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.			                			 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, A. Forrest										 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                        	 *
 * For the list of contributors see CREDITS.   			        		 *
 *************************************************************************/

#ifndef _eventing_core_PublisherInterface_h_
#define _eventing_core_PublisherInterface_h_

#include "xdaq/Object.h"

#include "toolbox/mem/Reference.h"

#include "xdata/Properties.h"

#include "eventing/core/exception/Exception.h"

namespace eventing
{
	namespace core
	{

		const std::string PublisherService = "eventing-publisher";

		class PublisherInterface
		{
			public:

				virtual void publish (const std::string & topic, toolbox::mem::Reference * ref, xdata::Properties & plist) throw (eventing::core::exception::Exception) = 0;

				virtual void addActionListener (toolbox::ActionListener * l) = 0;
		};
	}
}

#endif
