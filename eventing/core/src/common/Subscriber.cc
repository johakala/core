// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.			                			 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, A. Forrest										 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                        	 *
 * For the list of contributors see CREDITS.   			        		 *
 *************************************************************************/

#include "eventing/core/Subscriber.h"

#include "xdaq/EndpointAvailableEvent.h"
#include "xdaq/ApplicationRegistry.h"

#include "xcept/tools.h"
#include "xgi/framework/Method.h"

#include "b2in/nub/Method.h"
#include "b2in/nub/Listener.h"

#include "pt/PeerTransportAgent.h"

XDAQ_INSTANTIATOR_IMPL (eventing::core::Subscriber)

eventing::core::Subscriber::Subscriber (xdaq::ApplicationStub* c) throw (xdaq::exception::Exception)
	: xdaq::Application(c), xgi::framework::UIManager(this), mutex_(toolbox::BSem::FULL)
{
	this->getApplicationContext()->addActionListener(this);

	c->getDescriptor()->setAttribute("icon", "/eventing/core/images/subscriber.png");
	c->getDescriptor()->setAttribute("icon16x16", "/eventing/core/images/subscriber.png");

	this->subscriptionExpire_ = "PT60S";
	this->renewRate_ = "PT30S";
	this->connectionRetryNum_ = "15"; // 15 secs

	this->messengerCache_ = 0;

	this->getApplicationInfoSpace()->fireItemAvailable("eventings", &eventings_);
	this->getApplicationInfoSpace()->fireItemAvailable("subscriptionExpire", &subscriptionExpire_);
	this->getApplicationInfoSpace()->fireItemAvailable("renewRate", &renewRate_);
	this->getApplicationInfoSpace()->fireItemAvailable("connectionRetryNum", &connectionRetryNum_);

	this->getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");

	xgi::framework::deferredbind(this, this, &eventing::core::Subscriber::Default, "Default");

	b2in::nub::bind(this, &eventing::core::Subscriber::onMessage);
}

eventing::core::Subscriber::~Subscriber ()
{
}

void eventing::core::Subscriber::subscribe (const std::string & topic, const std::string & lid) throw (eventing::core::exception::Exception)
{
	mutex_.take();

	Key k = std::make_pair(topic, lid);

	if (topics_.find(k) != topics_.end())
	{
		mutex_.give();
		XCEPT_RAISE(eventing::core::exception::Exception, "Topic/Service already subscribed");
	}

	topics_.insert(k);

	mutex_.give();

	this->refreshSubscriptionsToEventing();
}

void eventing::core::Subscriber::unsubscribe (const std::string & topic, const std::string & lid) throw (eventing::core::exception::Exception)
{
	mutex_.take();

	Key k = std::make_pair(topic, lid);

	// remove entry and let expire
	std::set<Key>::iterator i = topics_.find(k);
	if (i == topics_.end())
	{
		mutex_.give();
		XCEPT_RAISE(eventing::core::exception::Exception, "Could not find topic to unsubscribe");
	}
	topics_.erase(i);

	//remove from subscriptions
	std::map<Key, xdata::Properties>::iterator s = subscriptions_.find(k);
	if (s == subscriptions_.end())
	{
		mutex_.give();
		XCEPT_RAISE(eventing::core::exception::Exception, "Could not find subscription to unsubscribe");
	}
	subscriptions_.erase(s);

	mutex_.give();
}

void eventing::core::Subscriber::actionPerformed (xdata::Event& event)
{
	if (event.type() == "urn:xdaq-event:setDefaultValues")
	{
		try
		{
			std::stringstream timerName;
			timerName << "eventing-core-subscriber-timer-" << this->getApplicationDescriptor()->getLocalId();

			toolbox::task::Timer * timer = 0;
			if (!toolbox::task::getTimerFactory()->hasTimer(timerName.str()))
			{
				timer = toolbox::task::getTimerFactory()->createTimer(timerName.str());
			}
			else
			{
				timer = toolbox::task::getTimerFactory()->getTimer(timerName.str());
			}

			toolbox::TimeVal startTime;
			startTime = toolbox::TimeVal::gettimeofday();
			toolbox::TimeInterval interval;
			interval.fromString(renewRate_); // in seconds

			timer->scheduleAtFixedRate(startTime, this, interval, 0, "eventing-core-subscribe");
		}
		catch (toolbox::task::exception::Exception& te)
		{
			std::cout << "Cannot run timer" << xcept::stdformat_exception_history(te) << std::endl;
		}
	}
}

void eventing::core::Subscriber::actionPerformed (toolbox::Event& e)
{
	LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Received event " << e.type());

	if (e.type() == "xdaq::EndpointAvailableEvent")
	{
		std::string networkName = this->getApplicationDescriptor()->getAttribute("network");

		xdaq::EndpointAvailableEvent& ie = dynamic_cast<xdaq::EndpointAvailableEvent&>(e);
		const xdaq::Network* network = ie.getNetwork();

		LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Received endpoint available event for network " << network->getName());

		if (network->getName() == networkName)
		{
			mutex_.take();
			try
			{
				messengerCache_ = new b2in::utils::MessengerCache(this->getApplicationContext(), this->getApplicationDescriptor()->getAttribute("network"), this);
			}
			catch (b2in::utils::exception::Exception& e)
			{
				this->notifyQualified("error", e);
				return;
			}

			mutex_.give();

			this->refreshSubscriptionsToEventing();
		}
	}
}

void eventing::core::Subscriber::onMessage (toolbox::mem::Reference * ref, xdata::Properties & plist) throw (b2in::nub::exception::Exception)
{
	std::string action = plist.getProperty("urn:b2in-eventing:action");
	if (action != "notify")
	{
		//std::cout << "received control message << std::endl;
		if (ref != 0)
		{
			ref->release();
		}
		return;
	}

	std::string lid = plist.getProperty("urn:b2in-eventing:context");
	if (lid == "")
	{
		LOG4CPLUS_WARN(this->getApplicationLogger(), "Invalid notification, missing context (forwarding lid)");
		if (ref != 0)
		{
			ref->release();
		}
		return;
	}

	std::string subscriptionID = plist.getProperty("urn:b2in-eventing:id");

	std::string topic = plist.getProperty("urn:b2in-eventing:topic");

	Key k = std::make_pair(topic, lid);

	std::map<Key, xdata::Properties>::iterator s = subscriptions_.find(k);
	if (s == subscriptions_.end())
	{
		// could not find subscription
		LOG4CPLUS_WARN(this->getApplicationLogger(), "Could not find the local subscription record, discarding message");
		if (ref != 0)
		{
			ref->release();
		}
		return;
	}
	else
	{
		// found subscription, check UUID
		if (subscriptionID != (*s).second.getProperty("urn:b2in-eventing:id"))
		{
			LOG4CPLUS_WARN(this->getApplicationLogger(), "Mismatch UUID from subscription message, assuming duplicated subscription, discarding message");
			if (ref != 0)
			{
				ref->release();
			}
			return;
		}

		// found valid message
		try
		{
			xdaq::Application * app = this->getApplicationContext()->getApplicationRegistry()->getApplication(toolbox::toLong(lid));
			b2in::nub::MethodSignature* method = dynamic_cast<b2in::nub::MethodSignature*>(app->getMethod(0));
			method->invoke(ref, plist);
		}
		catch (xdaq::exception::ApplicationNotFound& e)
		{
			std::stringstream msg;
			msg << "Failed to find application by lid to dispatch b2in message in eventing subscriber";
			msg << plist.getProperty("urn:b2in-protocol:service") << "'";
			// Add all properties to the error message
			std::map<std::string, std::string, std::less<std::string> >::iterator pi = plist.begin();
			while (pi != plist.end())
			{
				msg << "(" << (*pi).first << "=" << (*pi).second << ")";
				++pi;
				if (pi != plist.end())
				{
					msg << ", ";
				}
			}

			XCEPT_RAISE (b2in::nub::exception::Exception, msg.str());
		}
	}
}

void eventing::core::Subscriber::refreshSubscriptionsToEventing () throw (b2in::utils::exception::Exception)
{
	LOG4CPLUS_DEBUG(this->getApplicationLogger(), "refresh subscriptions");
	if (messengerCache_ == 0)
	{
		LOG4CPLUS_DEBUG(this->getApplicationLogger(), "No messenger cache");
		return;
	}

	mutex_.take();

	if (topics_.empty())
	{
		mutex_.give();
		LOG4CPLUS_DEBUG(this->getApplicationLogger(), "no topics to subscribe for");
		return;
	}

	// This lock is available all the time excepted when an asynchronous send will fail and a cache invalidate is performed
	// Therefore there is no loss of efficiency.

	LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Preparing subscription records");
	const xdaq::Network * network = 0;
	std::string networkName = this->getApplicationDescriptor()->getAttribute("network");
	try
	{
		network = this->getApplicationContext()->getNetGroup()->getNetwork(networkName);
	}
	catch (xdaq::exception::NoNetwork& nn)
	{
		mutex_.give();
		std::stringstream msg;
		msg << "Failed to access b2in network " << networkName << ", not configured";
		XCEPT_RETHROW(b2in::utils::exception::Exception, msg.str(), nn);

	}
	pt::Address::Reference localAddress = network->getAddress(this->getApplicationContext()->getContextDescriptor());

	for (std::set<Key>::iterator t = topics_.begin(); t != topics_.end(); t++)
	{
		if (subscriptions_.find((*t)) == subscriptions_.end())
		{
			xdata::Properties plist;
			toolbox::net::UUID identifier;
			plist.setProperty("urn:b2in-protocol:service", "b2in-eventing"); // for remote dispatching

			plist.setProperty("urn:b2in-eventing:action", "subscribe");
			plist.setProperty("urn:b2in-eventing:id", identifier.toString());
			plist.setProperty("urn:b2in-eventing:topic", (*t).first); // Subscribe to collected data
			plist.setProperty("urn:b2in-eventing:subscriberurl", localAddress->toString());
			//plist.setProperty("urn:b2in-eventing:subscriberservice", this->getApplicationDescriptor()->getAttribute("service"));
			plist.setProperty("urn:b2in-eventing:subscriberlid", toolbox::toString("%d", this->getApplicationDescriptor()->getLocalId()));
			plist.setProperty("urn:b2in-eventing:context", (*t).second); // user application service
			plist.setProperty("urn:b2in-eventing:expires", subscriptionExpire_);

			// For future use
			std::stringstream myURL;
			myURL << this->getApplicationDescriptor()->getContextDescriptor()->getURL() << "/" << this->getApplicationDescriptor()->getURN();
			plist.setProperty("urn:b2in-eventing:originator", myURL.str());

			subscriptions_[(*t)] = plist;
		}
	}

	if (eventings_.elements() != 0)
	{
		for (xdata::Vector<xdata::String>::iterator i = eventings_.begin(); i != eventings_.end(); i++)
		{
			try
			{
				if (!messengerCache_->hasMessenger((*i)))
				{
					messengerCache_->createMessenger((*i));
				}
			}
			catch (b2in::utils::exception::Exception& e)
			{
				mutex_.give();
				LOG4CPLUS_FATAL(this->getApplicationLogger(), "Failed to create messenger on eventing at " << (*i).toString() << ", " << e.what());
				this->notifyQualified("error", e);
				return;
			}
		}
	}

	std::list < std::string > destinations = messengerCache_->getDestinations();

	size_t messengerIndex = 0;

	for (std::list<std::string>::iterator j = destinations.begin(); j != destinations.end(); j++)
	{
		messengerIndex++;
		//
		// Subscribe to OR Renew all existing subscriptions
		//
		LOG4CPLUS_INFO(this->getApplicationLogger(), "Subscribing for topics on url " << (*j) << " : eventing #" << messengerIndex);
		for (std::map<Key, xdata::Properties>::iterator i = subscriptions_.begin(); i != subscriptions_.end(); ++i)
		{
			try
			{
				// plist is already prepared for a subscribe/resubscribe message
				//
				(*i).second.setProperty("urn:b2in-protocol-tcp:connection-timeout",connectionRetryNum_);

				LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Subscribe to topic " << (*i).first.first << " on url " << (*j) << " : eventing #" << messengerIndex);
				messengerCache_->send((*j), 0, (*i).second);
			}
			catch (b2in::nub::exception::InternalError & e)
			{
				messengerCache_->invalidate((*j));
				mutex_.give();
				std::stringstream msg;
				msg << "failed to send subscription for topic (internal error) " << (*i).first.first << " to url " << (*j) << " : eventing #" << messengerIndex;

				//LOG4CPLUS_ERROR(this->getApplicationLogger(), msg.str());

				XCEPT_DECLARE_NESTED(b2in::utils::exception::Exception, ex, msg.str(), e);
				this->notifyQualified("fatal", ex);
				break;
			}
			catch (b2in::nub::exception::QueueFull & e)
			{
				mutex_.give();
				std::stringstream msg;
				msg << "failed to send subscription for topic (queue full) " << (*i).first.first << " to url " << (*j) << " : eventing #" << messengerIndex;

				//LOG4CPLUS_ERROR(this->getApplicationLogger(), msg.str());

				XCEPT_DECLARE_NESTED(b2in::utils::exception::Exception, ex, msg.str(), e);
				this->notifyQualified("error", ex);
				break;
			}
			catch (b2in::nub::exception::OverThreshold & e)
			{
				mutex_.give();
				// ignore just count to avoid verbosity
				std::stringstream msg;
				msg << "failed to send subscription for topic (over threshold) " << (*i).first.first << " to url " << (*j) << " : eventing #" << messengerIndex;

				//LOG4CPLUS_ERROR(this->getApplicationLogger(), msg.str());

				XCEPT_DECLARE_NESTED(b2in::utils::exception::Exception, ex, msg.str(), e);
				this->notifyQualified("error", ex);
				break;
			}
		}
		LOG4CPLUS_DEBUG(this->getApplicationLogger(), "done with subscription");
	}

	mutex_.give();
}

void eventing::core::Subscriber::timeExpired (toolbox::task::TimerEvent& e)
{
	std::string name = e.getTimerTask()->name;

	if (name == "eventing-core-subscribe")
	{
		try
		{
			this->refreshSubscriptionsToEventing();
		}
		catch (b2in::utils::exception::Exception& e)
		{
			LOG4CPLUS_FATAL(this->getApplicationLogger(), xcept::stdformat_exception_history(e));
		}

	}
}

void eventing::core::Subscriber::asynchronousExceptionNotification (xcept::Exception& e)
{
	LOG4CPLUS_DEBUG(this->getApplicationLogger(), e.what());
}

void eventing::core::Subscriber::Default (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	*out << cgicc::table().set("class","xdaq-table-vertical");
	*out << cgicc::caption("Settings");
	*out << cgicc::tbody();

	const xdaq::ApplicationDescriptor * d = this->getApplicationDescriptor();

	*out << cgicc::tr();
	*out << cgicc::th("Bus Name");
	*out << cgicc::td(d->getAttribute("bus"));
	*out << cgicc::tr();

	*out << cgicc::tr();
	*out << cgicc::th("Service");
	*out << cgicc::td(d->getAttribute("service"));
	*out << cgicc::tr();

	*out << cgicc::tbody();
	*out << cgicc::table();

	*out << cgicc::br();

	// Subscription information

	*out << cgicc::table().set("class", "xdaq-table").set("style", "width: 100%;");
	*out << cgicc::caption("Subscriptions");
	*out << cgicc::thead();
	*out << cgicc::tr();
	*out << cgicc::th("Topic");
	*out << cgicc::th("Owner LID");
	*out << cgicc::th("Owner Application");
	*out << cgicc::th("ID");
	*out << cgicc::tr() << std::endl;
	*out << cgicc::thead();
	*out << cgicc::tbody();

	mutex_.take();

	for (std::map<Key, xdata::Properties>::iterator i = subscriptions_.begin(); i != subscriptions_.end(); ++i)
	{
		*out << cgicc::tr();
		*out << cgicc::td() << (*i).second.getProperty("urn:b2in-eventing:topic") << cgicc::td();

		std::string lid = (*i).second.getProperty("urn:b2in-eventing:context");
		*out << cgicc::td() << lid << cgicc::td();

		try
		{
			xdaq::Application * app = this->getApplicationContext()->getApplicationRegistry()->getApplication(toolbox::toLong(lid));

			const xdaq::ApplicationDescriptor* desc = app->getApplicationDescriptor();

			std::stringstream url;
			url << app->getApplicationDescriptor()->getContextDescriptor()->getURL() << "/" << app->getApplicationDescriptor()->getURN();

			*out << cgicc::td() << "<a href=\"" << url.str() << "\">" << desc->getClassName() << " - instance " << desc->getInstance() << "</a>" << cgicc::td();
		}
		catch (xdaq::exception::ApplicationNotFound& e)
		{
			XCEPT_RETHROW(xgi::exception::Exception, "Error parsing subscription", e);
		}
		catch (xdaq::exception::Exception& e)
		{
			XCEPT_RETHROW(xgi::exception::Exception, "Error displaying subscription", e);
		}


		*out << cgicc::td() << (*i).second.getProperty("urn:b2in-eventing:id") << cgicc::td();

		*out << cgicc::tr();
	}

	mutex_.give();

	*out << cgicc::tbody();
	*out << cgicc::table();
}

