// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2019, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, A. Forrest and D. Simelevicius                    *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include "eventing/core/Publisher.h"
#include "eventing/core/PublisherReadyEvent.h"

#include "xdaq/EndpointAvailableEvent.h"

#include "xgi/framework/Method.h"
#include "xcept/tools.h"

#include "pt/PeerTransportAgent.h"

#include "b2in/nub/Messenger.h"
#include "pt/utcp/B2INMessenger.h"

XDAQ_INSTANTIATOR_IMPL (eventing::core::Publisher)

eventing::core::Publisher::Publisher (xdaq::ApplicationStub* c) throw (xdaq::exception::Exception)
	: xdaq::Application(c), xgi::framework::UIManager(this), mutex_(toolbox::BSem::FULL,true)
{
	this->getApplicationContext()->addActionListener(this);

	c->getDescriptor()->setAttribute("icon", "/eventing/core/images/publisher.png");
	c->getDescriptor()->setAttribute("icon16x16", "/eventing/core/images/publisher.png");

	this->currentIndex_ = 0;

	this->getApplicationInfoSpace()->fireItemAvailable("eventings", &eventings_);

	publishFailedHandler_ = toolbox::exception::bind(this, &eventing::core::Publisher::publishFailed, "publishFailed");

	xgi::framework::deferredbind(this, this,  &eventing::core::Publisher::Default, "Default");
}

eventing::core::Publisher::~Publisher ()
{
}

void eventing::core::Publisher::actionPerformed (toolbox::Event& e)
{
	LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Received event " << e.type());

	mutex_.take();

	if (e.type() == "xdaq::EndpointAvailableEvent")
	{
		// create the messengers
		//xdaq::Network * network = 0;
		pt::Address::Reference localAddress;

		std::string networkName = this->getApplicationDescriptor()->getAttribute("network");
		xdaq::EndpointAvailableEvent& ie = dynamic_cast<xdaq::EndpointAvailableEvent&>(e);
		const xdaq::Network* network = ie.getNetwork();
		LOG4CPLUS_INFO(this->getApplicationLogger(), "Received endpoint available event for network " << network->getName());

		if (network->getName() == networkName)
		{
		/*try
		{
			network = this->getApplicationContext()->getNetGroup()->getNetwork(networkName);
		}
		catch (xdaq::exception::NoNetwork& nn)
		{
			mutex_.give();
			std::stringstream msg;
			msg << "Failed to access b2in network " << networkName << ", not configured";
			//XCEPT_RETHROW(b2in::utils::exception::Exception, msg.str(), nn);
		}
		*/


			try
			{
				localAddress = network->getAddress(this->getApplicationContext()->getContextDescriptor());
			}
			catch (xdaq::exception::NoEndpoint& nn)
			{
				mutex_.give();
				std::stringstream msg;
				msg << "Failed to find local address for network " << networkName;
				//XCEPT_RETHROW(b2in::utils::exception::Exception, msg.str(), nn);
				XCEPT_DECLARE_NESTED(eventing::core::exception::Exception, e, msg.str(), nn);
				this->notifyQualified("fatal", e);
				return;
			}

			for (xdata::Vector<xdata::String>::iterator i = eventings_.begin(); i != eventings_.end(); i++)
			{
				try
				{
					pt::Address::Reference eventingAddress = pt::getPeerTransportAgent()->createAddress((*i), "b2in");

					if (localAddress->equals(eventingAddress))
					{
						pt::Address::Reference loopbackAddress = pt::getPeerTransportAgent()->createAddress("loopback://localhost", "b2in");
						messengers_.push_back(pt::getPeerTransportAgent()->getMessenger(loopbackAddress, loopbackAddress));
					}
					else
					{
						LOG4CPLUS_INFO(this->getApplicationLogger(), "create messenger for network " << networkName << " url: " << (*i).toString() << " messenger size is " << messengers_.size());

						messengers_.push_back(pt::getPeerTransportAgent()->getMessenger(eventingAddress, localAddress));
					}
				}
				catch (xdaq::exception::NoRoute & e)
				{
					std::stringstream msg;
					msg << "Failed to create a messenger";
					mutex_.give();
					//XCEPT_RETHROW(b2in::utils::exception::Exception, msg.str(), e);
					XCEPT_DECLARE_NESTED(eventing::core::exception::Exception, ex, msg.str(), e);
					this->notifyQualified("fatal", ex);
					continue;
				}
				catch (xdaq::exception::NoNetwork & e)
				{
					std::stringstream msg;
					msg << "Failed to create a messenger";
					mutex_.give();
					//XCEPT_RETHROW(b2in::utils::exception::Exception, msg.str(), e);
					XCEPT_DECLARE_NESTED(eventing::core::exception::Exception, ex, msg.str(), e);
					this->notifyQualified("fatal", ex);
					continue;
				}
				catch (xdaq::exception::NoEndpoint & e)
				{
					std::stringstream msg;
					msg << "Failed to create a messenger";
					mutex_.give();
					//XCEPT_RETHROW(b2in::utils::exception::Exception, msg.str(), e);
					XCEPT_DECLARE_NESTED(eventing::core::exception::Exception, ex, msg.str(), e);
					this->notifyQualified("fatal", ex);
					continue;
				}
				catch (pt::exception::InvalidAddress& e)
				{
					std::stringstream msg;
					msg << "Failed to create a messenger";
					mutex_.give();
					//XCEPT_RETHROW(b2in::utils::exception::Exception, msg.str(), e);
					XCEPT_DECLARE_NESTED(eventing::core::exception::Exception, ex, msg.str(), e);
					this->notifyQualified("fatal", ex);
					continue;
				}
				catch (pt::exception::NoMessenger & e)
				{
					std::stringstream msg;
					msg << "Failed to create a messenger";
					mutex_.give();
					//XCEPT_RETHROW(b2in::utils::exception::Exception, msg.str(), e);
					XCEPT_DECLARE_NESTED(eventing::core::exception::Exception, ex, msg.str(), e);
					this->notifyQualified("fatal", ex);
					continue;
				}
			}

			// notify the listening applications
			eventing::core::PublisherReadyEvent e(this, this->getApplicationDescriptor()->getAttribute("bus"));
			dispatcher_.fireEvent(e);
		}
	}
	mutex_.give();
}

void eventing::core::Publisher::publish (const std::string & topic, toolbox::mem::Reference * ref, xdata::Properties & plist) throw (eventing::core::exception::Exception)
{
	if (messengers_.empty())
	{
		XCEPT_RAISE(eventing::core::exception::Exception, "Cannot publish, messengers not created");
	}

	plist.setProperty("urn:b2in-protocol:service", "b2in-eventing");
	plist.setProperty("urn:b2in-eventing:topic", topic);
	plist.setProperty("urn:b2in-eventing:action", "notify");

	mutex_.take();

	size_t attempts = 0;

	while (attempts < messengers_.size())
	{
		LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Attempting to send to eventing #" << currentIndex_);
		b2in::nub::Messenger * m = dynamic_cast<b2in::nub::Messenger*>(&(*(messengers_[currentIndex_])));
		std::string url = m->getDestinationAddress()->toString();

		try
		{
			plist.setProperty("urn:b2in-protocol:originator", this->getApplicationContext()->getContextDescriptor()->getURL());
			m->send(ref, plist, publishFailedHandler_, (void*)currentIndex_);
		}
		catch (b2in::nub::exception::InternalError & e)
		{
			LOG4CPLUS_WARN(this->getApplicationLogger(), "blaf  1: " << xcept::stdformat_exception_history(e));

			/*
			mutex_.give();
			std::stringstream msg;
			msg << "Internal error, failed to send to url '" << url << "'";
			XCEPT_RETHROW(eventing::core::exception::Exception, msg.str(), e);
			*/
			this->publishFailed (e, (void *) currentIndex_);
			attempts++;
			continue;
		}
		catch (b2in::nub::exception::QueueFull & e)
		{
			LOG4CPLUS_WARN(this->getApplicationLogger(), "blaf 2  : " << xcept::stdformat_exception_history(e));

			/*
			mutex_.give();
			std::stringstream msg;
			msg << "Failed to send to url '" << url << "'";
			XCEPT_RETHROW(eventing::core::exception::Exception, msg.str(), e);
			*/
			this->publishFailed (e, (void *) currentIndex_);
			attempts++;
			continue;
		}
		catch (b2in::nub::exception::OverThreshold & e)
		{
			LOG4CPLUS_WARN(this->getApplicationLogger(), "blaf 3  : " << xcept::stdformat_exception_history(e));

			/*
			mutex_.give();
			std::stringstream msg;
			msg << "Failed to send to url '" << url << "'";
			XCEPT_RETHROW(eventing::core::exception::Exception, msg.str(), e);
			*/
			this->publishFailed (e, (void *) currentIndex_);
			attempts++;
			continue;
		}
		break;
	}

	if (attempts == messengers_.size())
	{
		// could not publish to any eventings
		std::stringstream msg;
		msg << "Failed to send to all eventings";
		mutex_.give();
		XCEPT_RAISE(eventing::core::exception::Exception, msg.str());
	}

	mutex_.give();
}

bool eventing::core::Publisher::publishFailed (xcept::Exception& e, void * context)
{
	mutex_.take();

	size_t index = (size_t) context;

	if (index != currentIndex_)
	{
		LOG4CPLUS_WARN(this->getApplicationLogger(), "Failed to publish on index: " << index << " active index is: "<< currentIndex_ << xcept::stdformat_exception_history(e));
	}
	else
	{
		LOG4CPLUS_ERROR(this->getApplicationLogger(), "Failed to publish on current eventing, reset messenger on index  : " << currentIndex_ << " exception history: " << xcept::stdformat_exception_history(e));
		pt::utcp::B2INMessenger * m = dynamic_cast<pt::utcp::B2INMessenger*>(&(*(messengers_[currentIndex_])));
		m->reset();
		currentIndex_ = (index + 1) % messengers_.size();
		LOG4CPLUS_INFO(this->getApplicationLogger(), "Moving from eventing on index: " << index << " to a redundant eventing on index : " << currentIndex_  << " messenger size is " << messengers_.size());

	}

	mutex_.give();
	return false;
}

void eventing::core::Publisher::addActionListener (toolbox::ActionListener * l)
{
	mutex_.take();
	dispatcher_.addActionListener(l);
	mutex_.give();
}

void eventing::core::Publisher::Default (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	*out << cgicc::table().set("class","xdaq-table-vertical");
	*out << cgicc::caption("Settings");
	*out << cgicc::tbody();

	const xdaq::ApplicationDescriptor * d = this->getApplicationDescriptor();

	*out << cgicc::tr();
	*out << cgicc::th("Bus Name");
	*out << cgicc::td(d->getAttribute("bus"));
	*out << cgicc::tr();

	*out << cgicc::tr();
	*out << cgicc::th("Service");
	*out << cgicc::td(d->getAttribute("service"));
	*out << cgicc::tr();

	*out << cgicc::tbody();
	*out << cgicc::table();

	*out << cgicc::br();

	// Eventing URLS

	*out << cgicc::table().set("style", "width: 100%;").set("class","xdaq-table");
	*out << cgicc::thead();
	*out << cgicc::tr();
	*out << cgicc::th("Eventing URLs");
	*out << cgicc::tr() << std::endl;
	*out << cgicc::thead();
	*out << cgicc::tbody();
	size_t cur = 0;
	for (xdata::Vector<xdata::String>::iterator i = eventings_.begin(); i != eventings_.end(); i++)
	{
		if (cur == currentIndex_)
		{
			*out << cgicc::tr();
			*out << cgicc::td().set("class", "xdaq-green") << (*i).toString() << cgicc::td();
			*out << cgicc::tr();
		}
		else
		{
			*out << cgicc::tr();
			*out << cgicc::td() << (*i).toString() << cgicc::td();
			*out << cgicc::tr();
		}

		cur++;
	}

	*out << cgicc::tbody();
	*out << cgicc::table();
}

