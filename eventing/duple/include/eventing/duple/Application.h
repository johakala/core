// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.			                			 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, D. Simelevicious								     *
 *                                                                       *
 * For the licensing terms see LICENSE.		                        	 *
 * For the list of contributors see CREDITS.   			        		 *
 *************************************************************************/

#ifndef _eventing_duple_Application_h_
#define _eventing_duple_Application_h_

#include "xdaq/Application.h"
#include "xdata/ActionListener.h"

#include "xdata/UnsignedLong.h"
#include "xdata/String.h"

#include "xgi/Input.h"
#include "xgi/Output.h"

#include "xgi/framework/UIManager.h"

#include "eventing/api/Member.h"
#include "b2in/nub/exception/Exception.h"
namespace eventing
{
	namespace duple
	{
		class Application : public xdaq::Application, public xgi::framework::UIManager, public xdata::ActionListener
		{
			public:
				XDAQ_INSTANTIATOR();

				Application (xdaq::ApplicationStub* c) throw (xdaq::exception::Exception);

				//void onMessage (toolbox::mem::Reference * ref, xdata::Properties & plist) throw (b2in::nub::exception::Exception);

				void Default (xgi::Input *in, xgi::Output *out) throw (xgi::exception::Exception);

			protected:

				void actionPerformed (xdata::Event& event);

				//toolbox::BSem subMutex_;

				//xdata::UnsignedLong counter_;
				xdata::String eventingBusName_;
				xdata::String mirrorTopics_;

				eventing::api::Member * member_;
		};

	}
}

#endif
