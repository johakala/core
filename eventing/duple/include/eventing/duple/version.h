// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

//
// Version definition for MStreamIO
//
#ifndef _eventingduple_Version_h_
#define _eventingduple_Version_h_

#include "config/PackageInfo.h"
// !!! Edit this line to reflect the latest package version !!!

#define EVENTINGDUPLE_VERSION_MAJOR 1
#define EVENTINGDUPLE_VERSION_MINOR 1
#define EVENTINGDUPLE_VERSION_PATCH 0
// If any previous versions available E.g. #define EVENTINGDUPLE_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#undef EVENTINGDUPLE_PREVIOUS_VERSIONS


//
// Template macros
//
#define EVENTINGDUPLE_VERSION_CODE PACKAGE_VERSION_CODE(EVENTINGDUPLE_VERSION_MAJOR,EVENTINGDUPLE_VERSION_MINOR,EVENTINGDUPLE_VERSION_PATCH)
#ifndef EVENTINGDUPLE_PREVIOUS_VERSIONS
#define EVENTINGDUPLE_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(EVENTINGDUPLE_VERSION_MAJOR,EVENTINGDUPLE_VERSION_MINOR,EVENTINGDUPLE_VERSION_PATCH)
#else 
#define EVENTINGDUPLE_FULL_VERSION_LIST  EVENTINGDUPLE_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(EVENTINGDUPLE_VERSION_MAJOR,EVENTINGDUPLE_VERSION_MINOR,EVENTINGDUPLE_VERSION_PATCH)
#endif 

namespace eventingduple
{
	const std::string package  =  "eventingduple";
	const std::string versions =  EVENTINGDUPLE_FULL_VERSION_LIST;
	const std::string summary = "Enable an aventing to  a mirror topics";
	const std::string description = "";
	const std::string authors = "Luciano Orsini, Dainius Simelevicius";
	const std::string link = "http://xdaqwiki.cern.ch/index.php/Power_Pack";
	config::PackageInfo getPackageInfo();
	void checkPackageDependencies() throw (config::PackageInfo::VersionException);
	std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
