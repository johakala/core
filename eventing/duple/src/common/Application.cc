// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                             *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, D. Simelevicious				                     *
 *                                                                       *
 * For the licensing terms see LICENSE.		                             *
 * For the list of contributors see CREDITS.   			                 *
 *************************************************************************/



#include "eventing/duple/Application.h"

#include "xdaq/Application.h"
#include "xdaq/ApplicationContext.h"
#include "xdaq/ApplicationGroup.h"
#include "toolbox/PerformanceMeter.h"
#include "xdaq/NamespaceURI.h"

#include "toolbox/BSem.h"

#include "toolbox/rlist.h"
#include "toolbox/string.h"
#include "toolbox/mem/Reference.h"
#include "toolbox/mem/Pool.h"
#include "toolbox/task/TimerTask.h"
#include "toolbox/task/Timer.h"
#include "toolbox/task/TimerFactory.h"

#include "xdata/UnsignedLong.h"
#include "xdata/Double.h"
#include "xdata/Boolean.h"
#include "xdata/ActionListener.h"

#include "xoap/MessageFactory.h"
#include "xoap/MessageReference.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPName.h"
#include "xoap/SOAPBody.h"
#include "xoap/Method.h"

#include "xcept/Exception.h"
#include "xcept/tools.h"

#include "xgi/framework/Method.h"
#include "xdaq/ApplicationRegistry.h"

//#include "b2in/nub/Method.h"

XDAQ_INSTANTIATOR_IMPL (eventing::duple::Application);

eventing::duple::Application::Application (xdaq::ApplicationStub* c) throw (xdaq::exception::Exception)
	: xdaq::Application(c), xgi::framework::UIManager(this)
{
	member_  = 0;
	eventingBusName_ = "duple";
	mirrorTopics_ = "";

	c->getDescriptor()->setAttribute("icon", "/eventing/duple/images/eventingduple.png");
	c->getDescriptor()->setAttribute("icon16x16", "/eventing/duple/images/eventingduple.png");

	getApplicationInfoSpace()->fireItemAvailable("eventingBusName", &eventingBusName_);
	getApplicationInfoSpace()->fireItemAvailable("mirrorTopics", &mirrorTopics_);

	//b2in::nub::bind(this, &eventing::duple::onMessage);

	xgi::framework::deferredbind(this, this, &eventing::duple::Application::Default, "Default");

	this->getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");
}

void eventing::duple::Application::actionPerformed (xdata::Event& event)
{
	if (event.type() == "urn:xdaq-event:setDefaultValues")
	{

		try
		{
			//std::vector<xdaq::Application*> getApplications
			std::vector<xdaq::Application*> eventings = this->getApplicationContext()->getApplicationRegistry()->getApplications("service", "b2in-eventing");

			for (std::vector<xdaq::Application*>::iterator i = eventings.begin(); i != eventings.end(); i++)
			{

				member_ = new eventing::api::Member(*i);

				try
				{
					eventing::api::Bus& bus = member_->getEventingBus(eventingBusName_);
					if (bus.canSubscribe())
					{
						std::set<std::string> topics = toolbox::parseTokenSet(mirrorTopics_,",");
						for (std::set<std::string>::iterator j = topics.begin(); j != topics.end(); j++ )
						{
							bus.subscribe(*j);
						}
					}

				}
				catch (eventing::api::exception::Exception & e)
				{
					std::cout << "Failed duple subscribe - " << xcept::stdformat_exception(e) << std::endl;
				}

				return; // just the first found

			}
		}
		catch (xdaq::exception::Exception & e)
			{
				std::cout << "Failed duple init - " << xcept::stdformat_exception(e) << std::endl;
			}
	}
}



//
void eventing::duple::Application::Default (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	*out << "<div class=\"xdaq-tab-wrapper\">";

        *out << "<div class=\"xdaq-tab\" title=\"Eventing\">";
        
        *out << "<h3>Buses</h3>";
        if ( member_ != 0 )
        {
        	*out << member_->busesToHTML() << std::endl;
        }
        *out << "</div>";
        *out << "</div>";
}
