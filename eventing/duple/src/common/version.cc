// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                             *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, D. Simelevicius              					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                             *
 * For the list of contributors see CREDITS.   			                 *
 *************************************************************************/

#include "eventing/duple/version.h"
#include "config/version.h"
#include "xcept/version.h"
#include "xdata/version.h"
#include "xdaq/version.h"
#include "toolbox/version.h"
#include "xgi/version.h"
#include "pt/version.h"
#include "xoap/version.h"

GETPACKAGEINFO(eventingduple)

void eventingduple::checkPackageDependencies() throw (config::PackageInfo::VersionException)
{
	CHECKDEPENDENCY(config)
    CHECKDEPENDENCY(xcept)
	CHECKDEPENDENCY(xdata)
	CHECKDEPENDENCY(xdaq)
	CHECKDEPENDENCY(toolbox)
	CHECKDEPENDENCY(xgi)
	CHECKDEPENDENCY(pt)
	CHECKDEPENDENCY(xoap)
}

std::set<std::string, std::less<std::string> > eventingduple::getPackageDependencies()
{
	std::set<std::string, std::less<std::string> > dependencies;
	ADDDEPENDENCY(dependencies,config);
	ADDDEPENDENCY(dependencies,xcept);
	ADDDEPENDENCY(dependencies,xdata);
	ADDDEPENDENCY(dependencies,xdaq);
	ADDDEPENDENCY(dependencies,toolbox);
	ADDDEPENDENCY(dependencies,xgi);
	ADDDEPENDENCY(dependencies,pt);
	ADDDEPENDENCY(dependencies,xoap);
	return dependencies;
}	
