// $Id: xupnpV.h,v 1.5 2008/07/18 15:28:47 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _xupnpV_h_
#define _xupnpV_h_

#include "PackageInfo.h"

namespace xupnp {
    const string package  =  "xupnp";
    const string versions =  "1.4.1,1.4.2";
    const string description = "xupnp discovery service";
	const string link = "http://xdaqwiki.cern.ch/index.php/Power_Pack";
    toolbox::PackageInfo getPackageInfo();
    void checkPackageDependencies() throw (toolbox::PackageInfo::VersionException);
    set<string, less<string> > getPackageDependencies();
}

#endif
