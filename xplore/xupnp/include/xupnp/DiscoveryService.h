// $Id: DiscoveryService.h,v 1.3 2008/07/18 15:28:47 gutleber Exp $
/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber, G. Lo Presti and L. Orsini                      *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _xplore_xupnp_DiscoveryService_h
#define _xplore_xupnp_DiscoveryService_h

#include "xplore/exception/Exception.h"
#include "xplore/Advertisement.h"
#include "xplore/DiscoveryListener.h"
#include "xplore/DiscoveryService.h"

#include "xupnp/Notifier.h"
#include "xupnp/Device.h"

#define HAVE_SOCKLEN_T
#include <cybergarage/upnp/CyberLink.h>
#undef HAVE_SOCKLEN_T

#include "BSem.h"

namespace xplore {
namespace xupnp {

//! This class wraps the xplore Discovery Service.
//! This service is responsible for all discovery queries over the network.
class DiscoveryService: public xplore::DiscoveryService,
	public Device
{
	public:

	DiscoveryService(const std::string& groupName, std::string id, xplore::xupnp::Notifier *notifier)
		throw (xplore::exception::Exception);

	~DiscoveryService();

	//! Queries the local cache getting all known advertisements
	/*! @param std::string advType the requested advertisement type; This parameter is technology dependent.
	    To get a Service Location Protocol advertisement use "slp", for JXTA use
	    one of DiscoveryService::PEER, DiscoveryService::GROUP, DiscoveryService::ADV
	    @return a list of advertisements, or an empty list if nothing is present in the local cache.
	    */
	std::vector<xplore::Advertisement::Reference>
		getKnownAdvertisements()
		throw (xplore::exception::Exception);

	std::vector<xplore::Advertisement::Reference>
		getKnownAdvertisements(const std::string& advType)
		throw (xplore::exception::Exception);

	std::vector<xplore::Advertisement::Reference>
		getKnownAdvertisements(const std::string& advType, const std::string& property, const std::string& propertyValue)
		throw (xplore::exception::Exception);

	//! Remotely and asynchronously queries the network discovering advertisements.
	/*! To get the discovered advertisements use a DiscoveryListener or call getKnownAdvertisements()
	   @param std::string advType the requested advertisement type; This parameter is technology dependent.
	    To get a Service Location Protocol advertisement use "slp", for JXTA use
	    one of DiscoveryService::PEER, DiscoveryService::GROUP, DiscoveryService::ADV
	    @return a list of advertisements, or an empty list if nothing is present in the local cache.
	    */
	void searchRemoteAdvertisements(const std::string& advType)
		throw (xplore::exception::Exception);

	//! Flushes the local cache
	/*  @param std::string advType the requested advertisement type; This parameter is technology dependent.
	    To get a Service Location Protocol advertisement use "slp", for JXTA use
	    one of DiscoveryService::PEER, DiscoveryService::GROUP, DiscoveryService::ADV
	    @return a list of advertisements, or an empty list if nothing is present in the local cache.
	    */
	void flushAdvertisements(const std::string& advType = "")
		throw (xplore::exception::Exception);

	//! Flush (remove) an advertisement
	/*! If the advertisement has been published using this discovery service
	    it will be "un"-published, i.e. revoked and will no longer be visible
	    by other (remote) discovery services.
	    If the provided advertisement has not been published using this
	    discovery service, it will only be removed from the local cache
	*/
	void
	flushAdvertisement(const std::string& id)
		throw (xplore::exception::Exception);

	//! Flush (remove) an advertisement
	/*! If the advertisement has been published using this discovery service
	    it will be "un"-published, i.e. revoked and will no longer be visible
	    by other (remote) discovery services.
	    If the provided advertisement has not been published using this
	    discovery service, it will only be removed from the local cache
	*/
	void flushAdvertisement(xplore::Advertisement::Reference adv)
		throw (xplore::exception::Exception);

	//! Publishes the given advertisement.
	void publishAdvertisement(xplore::Advertisement::Reference adv)
		throw (xplore::exception::Exception);


	//! Registers a listener to the discovery service. It is called back whenever a new peer is discovered.
	void addServiceListener(xplore::DiscoveryListener* listener)
		throw (xplore::exception::Exception);

	//! Removes a previously registered discovery listener.
	void removeServiceListener(xplore::DiscoveryListener* listener);

	//! Removes all previously registered discovery listeners.
	void removeAllServiceListeners();



	//! Extension: Perform a search to determine, which advertisement types have been published
	//
	void searchRemoteAdvertisementTypes() throw (xplore::exception::Exception);

	//! Return a vector with all cached (known) advertisement types.
	//
	std::vector<std::string>& getKnownAdvertisementTypes();



	protected:
	//! Add an advertisement type
	//
	void addAdvertisementType (const std::string& type);

	void removeAdvertisement(xplore::Advertisement::Reference adv) throw (xplore::exception::Exception);

	xplore::DiscoveryListener* listener_;
	std::map<std::string, xplore::Advertisement::Reference, std::less<std::string> > advs_;
	std::vector<std::string> advTypes_;
	BSem mutex_;

	xplore::xupnp::Notifier *notifier_;

	xplore::Advertisement::Reference getAdvertisement(CyberLink::Device *dev, std::string id);

	std::string groupName_;

	// unsafe internal function (not thread safe themselves)

	//! Internal function to add an advertisement to the cached list if it is not yet cached
	void addAdvertisement(xplore::Advertisement::Reference adv);

	//! Internal function to remove an advertisement from the cached list
	void removeAdvertisementType(const std::string& type);

	//! Internal function to remove an advertisement from the cached list
	void delAdvertisement(std::string id);

public:
	// functions called by Platform for updating the cache

	//! adds an remote advertisement to the local cache
	void addRemoteAdvertisement(xplore::Advertisement::Reference adv);
	//! removes an remote advertisement from the local cache using its id
	void delRemoteAdvertisement(std::string id);
	//! remove all advertisements of the device with the specified id and add all devices in vector
	void delRemoteAdvertisements(std::string id);
	//! remove all advertisement of PeerGroupDevice (id) and add new ones specified in advs
	void updateRemoteAdvertisements(std::vector<xplore::Advertisement::Reference> advs, std::string id);
	//! remove all PeerGroupAdvertisements with the specified groupname
	void removeLocalPeerGroupAdvertisements(std::string groupName) throw (xplore::exception::Exception);


	// remotly called functions

	//! retrieve the number of local Advertisements on the specified RootDevice
	//! throws no exception
	virtual unsigned int getAdvListSize()
	throw (xplore::exception::Exception);

	//! retrieve id of one advertisement on the specified RootDevice using an index between 0 and getAdvListSize()-1
	//! throws an exception on invalid index
	virtual std::string getAdvId(unsigned int index)
	throw (xplore::exception::Exception);

	//! retrieve the number of properties of an Advertisement on the specified RootDevice
	//! throws an exception on invalid id
	virtual unsigned int getPropertiesSize(std::string id)
	throw (xplore::exception::Exception);

	//! retrieve a property (name-value pair) of an advertisement on the specified RootDevice where the index
	//! may be between 0 and getPropertiesSize()-1
	//! throws an exception on invalid index or id
	virtual std::pair<std::string, std::string> getProperty (std::string id, unsigned int index)
	throw (xplore::exception::Exception);
};

}
}


#endif
