
/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2005, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: R.Moser                                                      *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _xplore_xupnp_Notifier_h
#define _xplore_xupnp_Notifier_h

#include "xplore/exception/Exception.h"
#include "xplore/AdvertisementImpl.h"

#include "toolbox/lang/Class.h"
#include "toolbox/task/WorkLoopFactory.h"
#include "toolbox/task/WaitingWorkLoop.h"

#include "BSem.h"

#include <string>
#include <queue>

#define HAVE_SOCKLEN_T
#include <cybergarage/upnp/CyberLink.h>
#undef HAVE_SOCKLEN_T

namespace xplore {
namespace xupnp {

class Platform;

//! This class is responsible for holding all pending notification requests and processing
//! one after the other. It decouples the ControlPoints within the netwok to avoid networkwide
//! deadlocks.  to avoid networkwide deadlocks
class Notifier:
	public toolbox::lang::Class
{
private:
	toolbox::task::ActionSignature* job;
	toolbox::task::WorkLoop* wl;
protected:
	Platform *p_;
	//! mutex to make queue thread safe
	BSem mutex_;
	//! mutex for signalling that the scanning finished if blocking was enabled
	BSem scanmutex_;

	//! queue to store events
	std::queue< std::vector<std::string> > actions_;

public:
	Notifier(Platform *p);

	//! processing events within queue one after the other (FIFO)
	bool run(toolbox::task::WorkLoop* wl);

	//! start workloop to process events in queue
	void activate();

	//! stop workloop to process events in queue
	void deactivate();

	//! add event to the notifier queue to add an advertisement with id
	//! udn is the associated RootDevice
	void addAdvertisement(std::string udn, std::string id);

	//! add event to the notifier queue to remove an advertisement with id
	//! udn is the associated RootDevice
	void remAdvertisement(std::string udn, std::string id);

	//! add event to the notifier queue to add a whole device with all its advertisements
	//! udn is the RootDevice
	void addDevice(std::string udn, std::string groupname);

	//! add event to the notifier queue to remove a whole device with all its advertisements
	//! udn is the RootDevice
	void remDevice(std::string udn, std::string groupname);

	//! scan a peergroup with groupname and wait until finished if block is set to true
	void addScan(std::string groupname, bool block=false);
};

}
}

#endif
