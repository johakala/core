
/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2005, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: R.Moser                                                      *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _xplore_xupnp_DeviceHelper_h
#define _xplore_xupnp_DeviceHelper_h

#include "xplore/exception/Exception.h"
#include "xplore/AdvertisementImpl.h"
#include "xplore/DiscoveryService.h"

#include <string>
#include <sstream>

#define HAVE_SOCKLEN_T
#include <cybergarage/upnp/CyberLink.h>
#undef HAVE_SOCKLEN_T

namespace xplore {
namespace xupnp {

//! converts one type to another - usually std::string to Integer or vice versa
template <class in_type, class out_type>
bool convert(const in_type &in, out_type &out)
{
	std::stringstream ss;
	ss << in;
	ss >> out;
	if(ss.fail() || !ss.eof())
		return false;
	return true;
}

//! convert any type into a std::string
template <class in_type>
std::string convert(const in_type &in)
{
	std::string ret;
	convert(in, ret);
	return ret;
}

//! The DeviceHelper class only wraps the marshalling and unmarshalling in function calls to
//! allow easy usage and debugging
class DeviceHelper
{
public:
	//! retrieve the number of local Advertisements on the specified RootDevice
	//! throws an exception on connection errors like Device not available anymore
	static unsigned int
	getAdvListSize(CyberLink::Device *dev) throw (xplore::exception::Exception);

	//! retrieve id of one advertisement on the specified RootDevice using an index between 0 and getAdvListSize()-1
	//! throws an exception if index is invalid
	//! throws an exception on connection errors like Device not available anymore
	static std::string
	getAdvId(CyberLink::Device *dev, unsigned int index) throw (xplore::exception::Exception);

	//! retrieve the number of properties of an Advertisement on the specified RootDevice
	//! throws an exception on connection errors like Device not available anymore
	static unsigned int
	getPropertiesSize(CyberLink::Device *dev, std::string id) throw (xplore::exception::Exception);

	//! retrieve a property (name-value pair) of an advertisement on the specified RootDevice where the index
	//! may be between 0 and getPropertiesSize()-1
	//! throws an exception if index is invalid
	//! throws an exception on connection errors like Device not available anymore
	static std::pair<std::string, std::string>
	getProperty(CyberLink::Device *dev, std::string id, unsigned int index) throw (xplore::exception::Exception);

	//! retrieve an advertisement with all its properties by its id on the specified RootDevice
	//! throws an exception on connection errors like Device not available anymore
	static xplore::Advertisement::Reference
	getAdvertisement(CyberLink::Device *dev, std::string id) throw (xplore::exception::Exception);

	//! retrieve the whole list of local advertisements with all their properties on the specified RootDevice
	//! throws an exception on connection errors like Device not available anymore
	static std::vector<xplore::Advertisement::Reference>
	getAdvertisementList(CyberLink::Device *dev, xplore::DiscoveryService *ds=NULL) throw (xplore::exception::Exception);

};

}
}

#endif
