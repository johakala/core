// $Id: Platform.h,v 1.4 2008/07/18 15:28:47 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber, G. Lo Presti and L. Orsini                      *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _xplore_xupnp_Platform_h
#define _xplore_xupnp_Platform_h

#include <string>
#include <vector>

#include "xplore/exception/Exception.h"
#include "xplore/Platform.h"
#include "xupnp/PeerGroup.h"
#include "xupnp/Notifier.h"
#include "xplore/Advertisement.h"

#define HAVE_SOCKLEN_T
#include <cybergarage/upnp/CyberLink.h>
#undef HAVE_SOCKLEN_T

namespace xplore {
namespace xupnp {

//! This class represents a discovery platform. There may be various implementations of this class.
class Platform: public xplore::Platform,
	CyberLink::DeviceChangeListener,
	CyberLink::EventListener
{
	public:

	static xplore::xupnp::Platform* getInstance();
	static void destroyInstance();

	//! Return the NetPeerGroup, i.e. the default main group to which every peer must belong. @see xplore::PeerGroup.
	xplore::PeerGroup* getNetPeerGroup();

	//! Search and join a given peer group. Throws exception if the group cannot be found on the local cache.
	//! To remotely discover a group use the Discovery Service.
	//! 'create' is used to specify if an PeerGroupAdvertisement should be created if no PeerGroupAdvertisement with 	//! groupName exists
	xplore::PeerGroup* joinPeerGroup(const std::string& groupName, bool create=false) throw (xplore::exception::Exception);

	//! Return the PeerGroup instance which represent the given peer group.
	//! Throws exception if the group cannot be found.
	xplore::PeerGroup* getPeerGroup(const std::string& groupName) throw (xplore::exception::Exception);

	//! Leave the given peer group. Cannot be used to leave the NetPeerGroup, because it's equivalent to shutdown the platform
	void leavePeerGroup(const std::string& groupName) throw (xplore::exception::Exception) ;

	//! Create a new SLP advertisement
	/*! Advertisements are created by the technology dependent platform implementations.
	    This is the implementation provided for SLP (Service Location Protocol).

	    The following general advertisement types are implemented (case sensitive specification):
	    1. Peer
	    2. PeerGroup
	    3. Endpoint

	    Other application specific advertisements may be added statically or dynamically in the future
	 */
	xplore::Advertisement::Reference createAdvertisement(const std::string& type)
		throw (xplore::exception::Exception);

	//! Create a new SLP advertisement from an SLP service string (e.g. a result from a search)
	xplore::Advertisement::Reference createAdvertisementFromSource(const std::string& source)
		throw (xplore::exception::Exception);

	protected:

	Platform();
	~Platform();

	static xplore::xupnp::Platform* instance_;
	std::map<std::string, xplore::xupnp::PeerGroup*, std::less<std::string> > groups_;
	BSem mutex_; // mutex for thread safe access to list of PeerGroup objects

	CyberLink::ControlPoint ctrl_; // Control Point which listens to events and notifications
	Notifier notifier_; // notifier queue to store all pending events and notifications

	// upnp events and notifications

	//! a new device was found
	void deviceAdded(CyberLink::Device *dev);
	//! an old device was removed or expired
	void deviceRemoved(CyberLink::Device *dev);
	//! a new Advertisement was announced or removed
	void eventNotifyReceived(const char *uuid, long seq, const char *name, const char *value);

	public:

	// functions called by notifier queue for processing the pending events/notifications

	//! retrieve information from the PeerGroupDevice specified by udn about the advertisement (id) and store
	//! it in the local cache
	//! throws an exception device, peergroup or discoveryservices is not valid anymore
	void addAdv(std::string udn, std::string id)
		throw (xplore::exception::Exception);

	//! remove Advertisement from the local cache
	//! throws an exception device, peergroup or discoveryservices is not valid anymore
	void remAdv(std::string udn, std::string id)
		throw (xplore::exception::Exception);

	//! retrieve information about all Advertisements on this PeerGroupDevice specified by udn and store them
	//! in the local cache
	//! throws an exception device, peergroup or discoveryservices is not valid anymore
	void addDev(std::string udn, std::string groupname)
		throw (xplore::exception::Exception);

	//! remove a PeerGroupDevice (udn) with all its associated advertisements
	//! throws an exception device, peergroup or discoveryservices is not valid anymore
	void remDev(std::string udn, std::string groupname)
		throw (xplore::exception::Exception);

	//! rescan the whole network for advertisements and update the local cache accordingly
	void reScan(std::string groupname)
		throw (xplore::exception::Exception);

	//! remove pointers to all devices which are not available anymore
	void cleanupDevices();

	protected:

	//! retrieve the DiscoveryService by the name of the PeerGroup
	//! throws an exception peergroup or discoveryservices doesn't exist
	DiscoveryService* getDiscoveryService(std::string name)
		throw (xplore::exception::Exception);
};

}
}

#endif
