
/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2005, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: R.Moser                                                      *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _xplore_xupnp_Device_h
#define _xplore_xupnp_Device_h

#include "xplore/exception/Exception.h"
#include "xplore/AdvertisementImpl.h"

#include "xupnp/DeviceHelper.h"

#include <string>
#include <sstream>

#define HAVE_SOCKLEN_T
#include <cybergarage/upnp/CyberLink.h>
#undef HAVE_SOCKLEN_T

namespace xplore {
namespace xupnp {

//! The Device class wraps away most of the details of the PeerGroup Device to make it easily accessable
//! in the DiscoveryService
class Device:
	public CyberLink::ActionListener
{
protected:
	CyberLink::Device dev_; // Peer Group Device

public:
	Device(std::string groupName, std::string id);
	virtual ~Device();

	//! upnp action callback listener for unmarshalling/marshalling the requests and forwarding to the functions below
	bool actionControlReceived(CyberLink::Action *action);

	// callback function which are called remotely

	//! retrieve the number of local Advertisements on the specified RootDevice
	//! throws an exception on unspecified errors
	virtual unsigned int getAdvListSize()
	throw (xplore::exception::Exception) = 0;

	//! retrieve id of one advertisement on the specified RootDevice using an index between 0 and getAdvListSize()-1
	//! throws an exception if index is invalid
	//! throws an exception on unspecified errors
	virtual std::string getAdvId(unsigned int index)
	throw (xplore::exception::Exception) = 0;

	//! retrieve the number of properties of an Advertisement on the specified RootDevice
	//! throws an exception on unspecified errors
	virtual unsigned int getPropertiesSize(std::string id)
	throw (xplore::exception::Exception) = 0;

	//! retrieve a property (name-value pair) of an advertisement on the specified RootDevice where the index
	//! may be between 0 and getPropertiesSize()-1
	//! throws an exception if index is invalid
	//! throws an exception on unspecified errors
	virtual std::pair<std::string, std::string> getProperty (std::string id, unsigned int index)
	throw (xplore::exception::Exception) = 0;

	//! announce a new Advertisement on the network
	//! throws an exception if upnp device/service was not initialized correctly
	void sendAddAdvertisement(xplore::Advertisement::Reference adv)
	throw (xplore::exception::Exception);

	//! remove an Advertisement from the network
	//! throws an exception if upnp device/service was not initialized correctly
	void sendRemoveAdvertisement(xplore::Advertisement::Reference adv)
	throw (xplore::exception::Exception);
};

}
}

#endif
