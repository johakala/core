// $Id: DeviceHelper.cc,v 1.4 2005/08/17 12:12:05 xdaq Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2005, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: R. Moser                                                     *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include "xupnp/DeviceHelper.h"

unsigned int
xplore::xupnp::DeviceHelper::getAdvListSize(CyberLink::Device *dev) throw (xplore::exception::Exception)
{
	CyberLink::Action *action=dev->getAction("getAdvListSize");
	if(action==NULL)
		XCEPT_RAISE (xplore::exception::Exception, "Action not found.");

	if(action->postControlAction()==true)
	{
		CyberLink::ArgumentList *args=action->getOutputArgumentList();
		int n=args->size();
		for(int i=0;i<n;i++)
		{
			CyberLink::Argument *arg=args->getArgument(i);
			const char *name=arg->getName();
			if(strcmp(name, "Size")==0)
				return arg->getIntegerValue();
		}

		XCEPT_RAISE (xplore::exception::Exception, "Return argument(s) not found");
	}
	else
	{
		CyberLink::UPnPStatus *err=action->getControlStatus();
		XCEPT_RAISE (xplore::exception::Exception, err->getDescription());
	}
}

std::string
xplore::xupnp::DeviceHelper::getAdvId(CyberLink::Device *dev, unsigned int index) throw (xplore::exception::Exception)
{
	CyberLink::Action *action=dev->getAction("getAdvId");
	if(action==NULL)
		XCEPT_RAISE (xplore::exception::Exception, "Action not found.");

	action->setArgumentValue("Index", convert(index).c_str());

	if(action->postControlAction()==true)
	{
		CyberLink::ArgumentList *args=action->getOutputArgumentList();
		int n=args->size();
		for(int i=0;i<n;i++)
		{
			CyberLink::Argument *arg=args->getArgument(i);
			const char *name=arg->getName();
			if(strcmp(name, "Id")==0)
				return arg->getValue();
		}

		XCEPT_RAISE (xplore::exception::Exception, "Return argument(s) not found");
	}
	else
	{
		CyberLink::UPnPStatus *err=action->getControlStatus();
		XCEPT_RAISE (xplore::exception::Exception, err->getDescription());
	}
}

unsigned int
xplore::xupnp::DeviceHelper::getPropertiesSize(CyberLink::Device *dev, std::string id) throw (xplore::exception::Exception)
{
	CyberLink::Action *action=dev->getAction("getPropertiesSize");
	if(action==NULL)
		XCEPT_RAISE (xplore::exception::Exception, "Action not found.");

	action->setArgumentValue("Id", id.c_str());
	if(action->postControlAction()==true)
	{
		CyberLink::ArgumentList *args=action->getOutputArgumentList();
		int n=args->size();
		for(int i=0;i<n;i++)
		{
			CyberLink::Argument *arg=args->getArgument(i);
			const char *name=arg->getName();
			if(strcmp(name, "Size")==0)
				return arg->getIntegerValue();
		}

		XCEPT_RAISE (xplore::exception::Exception, "Return argument(s) not found");
	}
	else
	{
		CyberLink::UPnPStatus *err=action->getControlStatus();
		XCEPT_RAISE (xplore::exception::Exception, err->getDescription());
	}
}

std::pair<std::string, std::string>
xplore::xupnp::DeviceHelper::getProperty(CyberLink::Device *dev, std::string id, unsigned int index) throw (xplore::exception::Exception)
{
	CyberLink::Action *action=dev->getAction("getProperty");
	if(action==NULL)
		XCEPT_RAISE (xplore::exception::Exception, "Action not found.");

	action->setArgumentValue("Id", id.c_str());
	action->setArgumentValue("Index", convert(index).c_str());

	if(action->postControlAction()==true)
	{
		std::pair<std::string, std::string> prop;
		CyberLink::ArgumentList *args=action->getOutputArgumentList();
		int n=args->size();
		for(int i=0;i<n;i++)
		{
			CyberLink::Argument *arg=args->getArgument(i);
			const char *name=arg->getName();
			if(strcmp(name, "Name")==0)
				prop.first=arg->getValue();
			else if(strcmp(name, "Value")==0)
				prop.second=arg->getValue();
		}

		return prop;
	}
	else
	{
		CyberLink::UPnPStatus *err=action->getControlStatus();
		XCEPT_RAISE (xplore::exception::Exception, err->getDescription());
	}
}

xplore::Advertisement::Reference
xplore::xupnp::DeviceHelper::getAdvertisement(CyberLink::Device *dev, std::string id) throw (xplore::exception::Exception)
{
//	std::cout << "getAdv(" << dev->getUDN() << ");" << std::endl;

	xplore::Advertisement *adv=new xplore::AdvertisementImpl("name", id);
	xplore::Advertisement::Reference a=xplore::Advertisement::Reference(adv);

	unsigned int size=DeviceHelper::getPropertiesSize(dev, id);
	for(unsigned int i=0;i<size;i++)
	{
		std::pair<std::string, std::string> prop=DeviceHelper::getProperty(dev, id, i);
		a->setProperty(prop.first, prop.second);
	}

	return a;
}

std::vector<xplore::Advertisement::Reference>
xplore::xupnp::DeviceHelper::getAdvertisementList(CyberLink::Device *dev, xplore::DiscoveryService *ds) throw (xplore::exception::Exception)
{
//	std::cout << "getAdvList(" << dev->getUDN() << ");" << std::endl;
	std::vector<xplore::Advertisement::Reference> list;

	unsigned int size=DeviceHelper::getAdvListSize(dev);
	for(int i=size-1;i>=0;i--)
	{
		std::string id=DeviceHelper::getAdvId(dev, i);
		std::vector<xplore::Advertisement::Reference> advs;
		if(ds!=NULL)
			advs=ds->getKnownAdvertisements("", "id", id);

		if(advs.size()!=1)
		{
			xplore::Advertisement::Reference a=getAdvertisement(dev, id);
			list.push_back(a);
		}
		else
		{
			xplore::Advertisement::Reference a=advs[0];
			list.push_back(a);
		}
	}

	return list;
}
