// $Id: Notifier.cc,v 1.3 2005/08/22 13:40:43 xdaq Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2005, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: R. Moser                                                     *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include "xupnp/Notifier.h"
#include "xupnp/Platform.h"

xplore::xupnp::Notifier::Notifier(xplore::xupnp::Platform *p): mutex_(BSem::FULL), scanmutex_(BSem::EMPTY)
{
	p_=p;
	wl = toolbox::task::getWorkLoopFactory()->getWorkLoop("xupnpNotifierWorkLoop", "waiting");
	job = toolbox::task::bind (this, &xplore::xupnp::Notifier::run, "NotifyJob");
}

void xplore::xupnp::Notifier::activate()
{
	std::cout << "activate" << std::endl;
	wl->activate();
}

void xplore::xupnp::Notifier::deactivate()
{
	std::cout << "deactivate" << std::endl;
	wl->cancel();
}

void xplore::xupnp::Notifier::addAdvertisement(std::string udn, std::string id)
{
	std::vector<std::string> args;
	args.push_back("addAdv");
	args.push_back(udn);
	args.push_back(id);

	mutex_.take();
	actions_.push(args);
	mutex_.give();
	wl->submit(job);
}

void xplore::xupnp::Notifier::remAdvertisement(std::string udn, std::string id)
{
	std::vector<std::string> args;
	args.push_back("remAdv");
	args.push_back(udn);
	args.push_back(id);

	mutex_.take();
	actions_.push(args);
	mutex_.give();
	wl->submit(job);
}

void xplore::xupnp::Notifier::addDevice(std::string udn, std::string groupname)
{
	std::vector<std::string> args;
	args.push_back("addDev");
	args.push_back(udn);
	args.push_back(groupname);

	mutex_.take();
	actions_.push(args);
	mutex_.give();
	wl->submit(job);
}

void xplore::xupnp::Notifier::remDevice(std::string udn, std::string groupname)
{
	std::vector<std::string> args;
	args.push_back("remDev");
	args.push_back(udn);
	args.push_back(groupname);

	mutex_.take();
	actions_.push(args);
	mutex_.give();
	wl->submit(job);
}

void xplore::xupnp::Notifier::addScan(std::string groupname, bool block)
{
	std::vector<std::string> args;
	args.push_back("reScan");
	args.push_back(groupname);
	if(block)
		args.push_back("blocking");
	else
		args.push_back("");

	mutex_.take();
	actions_.push(args);
	mutex_.give();

	wl->submit(job);

	if(block)
		scanmutex_.take();
}

bool xplore::xupnp::Notifier::run(toolbox::task::WorkLoop* wl)
{
	if(actions_.empty())
	{
// TODO what and how to do that?
//		p_->cleanupDevices();
		return false;
	}

	mutex_.take();
	std::vector<std::string> args=actions_.front();
	actions_.pop();
	mutex_.give();

	try
	{
		if(args[0]=="addAdv")
			p_->addAdv(args[1], args[2]);
		else if(args[0]=="remAdv")
			p_->remAdv(args[1], args[2]);
		else if(args[0]=="addDev")
			p_->addDev(args[1], args[2]);
		else if(args[0]=="remDev")
			p_->remDev(args[1], args[2]);
		else if(args[0]=="reScan")
		{
			try
			{
				p_->reScan(args[1]);
			}
			catch (xplore::exception::Exception& e)
			{
				// Possible reasons for exception: Device, DiscoveryService or Advertisement removed
				// -> Advertisement is invalid and will be ignored
//				std::cout << "xcept (" << e.module() << "," << e.line() << "): " << e.message() << std::endl;
			}
			if(args[2]=="blocking")
				scanmutex_.give();
		}
	}
	catch (xplore::exception::Exception& e)
	{
		// Possible reasons for exception: Device, DiscoveryService or Advertisement removed
		// -> Advertisement is invalid and will be ignored
//		std::cout << "xcept (" << e.module() << "," << e.line() << "): " << e.message() << std::endl;
	}

	return false;
}
