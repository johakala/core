// $Id: PeerGroup.cc,v 1.1 2005/08/03 12:31:01 xdaq Exp $
/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2005, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: R.Moser                                                      *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include "xupnp/PeerGroup.h"

#include "xplore/Id.h"

xplore::xupnp::PeerGroup::PeerGroup(const std::string& name, xplore::xupnp::Notifier *notifier)
{
	name_ = name;
	id_=xplore::generateId();
	discoveryService_ = new xplore::xupnp::DiscoveryService(name, id_, notifier);
}

xplore::xupnp::PeerGroup::~PeerGroup()
{
	delete discoveryService_;
}

//! Returns the local peer name.
std::string xplore::xupnp::PeerGroup::getPeerName()
{
	return id_;
}

//! Returns the local peer ID as string.
std::string xplore::xupnp::PeerGroup::getPeerID()
{
	return id_;
}

//! Returns the peer group name.
std::string xplore::xupnp::PeerGroup::getPeerGroupName()
{
	return name_;
}

//! Returns the peer group ID as string.
std::string xplore::xupnp::PeerGroup::getPeerGroupID()
{
	return name_;
}

//! Returns the Discovery Service associated with the PeerGroup.
/*!
	To get the DiscoveryService that is associated with the default NetPeerGroup,
	retrieve first the NetPeerGroup and get the DiscoveryService from it
*/
xplore::DiscoveryService* xplore::xupnp::PeerGroup::getDiscoveryService()
{
	return discoveryService_;
}
