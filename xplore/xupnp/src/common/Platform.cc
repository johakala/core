// $Id: Platform.cc,v 1.5 2008/07/18 15:28:48 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber, G. Lo Presti and L. Orsini                      *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include "xupnp/Platform.h"
#include "xplore/AdvertisementImpl.h"
#include "xplore/Id.h"
#include "Task.h"
#include "toolbox/string.h"

#include "xupnp/DeviceHelper.h"

#include <sstream>

xplore::xupnp::Platform* xplore::xupnp::Platform::instance_ = 0;

extern "C" xplore::Platform* xplore_xupnp_getPlatform()
{
        return xplore::xupnp::Platform::getInstance();
}

extern "C" void xplore_xupnp_destroyPlatform()
{
        xplore::xupnp::Platform::destroyInstance();
}

void xplore::xupnp::Platform::destroyInstance()
{
 	if (instance_ != 0)
        {
                delete instance_;
        }
}

xplore::xupnp::Platform* xplore::xupnp::Platform::getInstance()
{
 	if (instance_ == 0)
        {
                instance_ = new xplore::xupnp::Platform();
        }
        return instance_;
}

xplore::xupnp::Platform::Platform(): mutex_(BSem::FULL), notifier_(this)
{
	// Create a PeerGroup class for the default group.
	// this means, that an advertisement is created and published, telling
	// that this host is member of the "default" peer group
	mutex_.take();
	groups_["default"] = new xplore::xupnp::PeerGroup("default", &notifier_);
	mutex_.give();

	ctrl_.addDeviceChangeListener(this);
	ctrl_.addEventListener(this);

	// TODO make configureable?
	ctrl_.setExpiredDeviceMonitoringInterval(5);
	ctrl_.setExpiredDeviceExtraTime(0);

	ctrl_.start();
	notifier_.activate();
}

xplore::xupnp::Platform::~Platform()
{
	notifier_.deactivate();

	ctrl_.removeDeviceChangeListener(this);
	ctrl_.removeEventListener(this);

	ctrl_.stop();

	// first remove all peergroups except netpeergroup
	mutex_.take();
	std::map<std::string, xplore::xupnp::PeerGroup*, std::less<std::string> >::iterator i;
	for (i = groups_.begin(); i != groups_.end(); i++)
	{
		if((*i).first!="default")
			delete (*i).second;
	}
	// then remove netpeergroup (as the other depend on this one)
	i=groups_.find("default");
	if(i!=groups_.end())
		delete (*i).second;
	mutex_.give();
}

//! Return the NetPeerGroup, i.e. the default main group to which every peer must belong. @see xplore::PeerGroup.
xplore::PeerGroup* xplore::xupnp::Platform::getNetPeerGroup()
{
	return this->getPeerGroup("default");
}

//! Search and join a given peer group. Throws exception if the group cannot be found on the local cache.
//! To remotely discover a group use the Discovery Service.
xplore::PeerGroup* xplore::xupnp::Platform::joinPeerGroup(const std::string& groupName, bool create) throw (xplore::exception::Exception)
{
	// is there a local PeerGroupAdvertisement with this name?
	bool local=false;
	// found any PeerGroupAdvertisement with this name?
	bool found=false;

	// check if PeerGroup was announced in NetPeerGroup
	std::vector<xplore::Advertisement::Reference> a;
	xplore::DiscoveryService *ds=getNetPeerGroup()->getDiscoveryService();
	a=ds->getKnownAdvertisements("PeerGroup");
	for(unsigned int i=0;i<a.size();i++)
	{
		if(a[i]->getProperty("name")==groupName)
		{
			found=true;
			if(xplore::idIsLocal(a[i]->getID()))
				local=true;
		}
	}

	if(!found && !create)
		XCEPT_RAISE (xplore::exception::Exception, "No PeerGroup named '"+groupName+"' advertised within NetPeerGroup.");

	// create local PeerGroupAdvertisement, if
	// - forced through 'create' or
	// - no local but a remote one was found
	if((!found && create) || (found && !local))
	{
		xplore::Advertisement::Reference adv = createAdvertisement("PeerGroup");
		adv->setProperty("name", groupName);
		ds->publishAdvertisement(adv);
	}

	mutex_.take();
	// check if this xdaq process already joined the PeerGroup
	if(groups_.find(groupName)==groups_.end())
	{
		groups_[groupName] = new xplore::xupnp::PeerGroup(groupName, &notifier_);
		xplore::PeerGroup *p=groups_[groupName];
		mutex_.give();
		return p;
	}
	xplore::PeerGroup *p=groups_[groupName];
	mutex_.give();

	CyberUtil::Vector known;

	ctrl_.lock();
	// subscribe to all PeerGroup devices and synchronize
	CyberLink::DeviceList *list=ctrl_.getDeviceList();
	for(unsigned int i=0;i<list->size();i++)
	{
		CyberLink::Device *dev=list->getDevice(i);
		// ignore my own PeerGroup device
		if(xplore::idIsLocal(dev->getUDN()))
			continue;

		if(groupName==dev->getFriendlyName())
			known.add(dev);
	}
	ctrl_.unlock();

	for(unsigned int i=0;i<known.size();i++)
	{
		CyberLink::Device *dev=(CyberLink::Device*)known.at(i);
		notifier_.addDevice(dev->getUDN(), dev->getFriendlyName());
	}

	return p;
}

//! Return the PeerGroup instance which represent the given peer group.
//! Throws exception if the group cannot be found.
xplore::PeerGroup* xplore::xupnp::Platform::getPeerGroup(const std::string& groupName) throw (xplore::exception::Exception)
{
	std::map<std::string, xplore::xupnp::PeerGroup*, std::less<std::string> >::iterator i;
	mutex_.take();
	i = groups_.find(groupName);
	if (i != groups_.end())
	{
		mutex_.give();
		return (*i).second;
	}
	mutex_.give();
	std::string msg = "could not find peer group named: ";
	msg += groupName;
	XCEPT_RAISE (xplore::exception::Exception, msg);
}

//! Leave the given peer group. Cannot be used to leave the NetPeerGroup, because it's equivalent to shutdown the platform
void xplore::xupnp::Platform::leavePeerGroup(const std::string& groupName) throw (xplore::exception::Exception)
{
	if(getNetPeerGroup()->getPeerGroupName()==groupName)
		XCEPT_RAISE (xplore::exception::Exception, "Unable to leave NetPeerGroup.");

	// deactivate notifier so nobody uses an invalid pointer to a discoveryService
	notifier_.deactivate();

	try
	{
		xplore::xupnp::DiscoveryService *ds=getDiscoveryService("default");
		ds->removeLocalPeerGroupAdvertisements(groupName);
	}
	catch (xplore::exception::Exception& e)
	{
		notifier_.activate();
		throw e;
	}

	std::map<std::string, xplore::xupnp::PeerGroup*, std::less<std::string> >::iterator i;
	mutex_.take();
	i = groups_.find(groupName);
	if (i==groups_.end())
	{
		mutex_.give();
		notifier_.activate();
		XCEPT_RAISE (xplore::exception::Exception, "PeerGroup doesn't exist.");
	}

	groups_.erase(groupName);
	delete (*i).second;
	mutex_.give();

	notifier_.activate();
}


xplore::Advertisement::Reference xplore::xupnp::Platform::createAdvertisement(const std::string& type)
throw (xplore::exception::Exception)
{
	return xplore::Advertisement::Reference(new xplore::AdvertisementImpl(type, ""));
}

xplore::Advertisement::Reference xplore::xupnp::Platform::createAdvertisementFromSource(const std::string& source)
throw (xplore::exception::Exception)
{
	return xplore::Advertisement::Reference(NULL);
}


// upnp callback functions

void xplore::xupnp::Platform::deviceAdded(CyberLink::Device *dev)
{
//	std::cout << "deviceAdded " << dev->getUDN() << std::endl;

	// ignore my own PeerGroup devices
	if(xplore::idIsLocal(dev->getUDN()))
		return;

	// add to queue for subscription
	notifier_.addDevice(dev->getUDN(), dev->getFriendlyName());
}

void xplore::xupnp::Platform::deviceRemoved(CyberLink::Device *dev)
{
//	std::cout << "deviceRemoved " << dev->getUDN() << std::endl;

	// ignore my own PeerGroup devices
	if(xplore::idIsLocal(dev->getUDN()))
		return;

	// add to queue for removing all cached advertisements
	notifier_.remDevice(dev->getUDN(), dev->getFriendlyName());
}

void xplore::xupnp::Platform::eventNotifyReceived(const char *uuid, long seq, const char *name, const char *value)
{
//	std::cout << "uuid " << uuid << " " << name << " " << value << std::endl;
	// don't use uuid of service as it may be wrong ...

	if(strcmp(value,"")==0)
		return;

	toolbox::StringTokenizer strtok = toolbox::StringTokenizer(value,"|");
	// ignore invalid formatted advertisement notifications
	if(strtok.countTokens()!=2)
		return;

	std::string udn=strtok.nextToken();
	std::string id=strtok.nextToken();

	// add to queue for updating advertisement cache
	if(strcmp(name, "addAdv")==0)
		notifier_.addAdvertisement(udn.c_str(), id);
	else if(strcmp(name, "remAdv")==0)
		notifier_.remAdvertisement(udn.c_str(), id);
}

// Helper functions
xplore::xupnp::DiscoveryService* xplore::xupnp::Platform::getDiscoveryService(std::string name)
throw (xplore::exception::Exception)
{
	DiscoveryService* ds=dynamic_cast<xplore::xupnp::DiscoveryService*>(getPeerGroup(name)->getDiscoveryService());
	if(ds==NULL)
		XCEPT_RAISE (xplore::exception::Exception, "DiscoveryService not available anymore.");

	return ds;
}

// notification functions
void xplore::xupnp::Platform::addAdv(std::string udn, std::string id) throw (xplore::exception::Exception)
{
//	std::cout << "addAdv(" << udn << "," << id << ");" << std::endl;

	CyberLink::Device *dev=ctrl_.getDevice(udn.c_str());
	if(dev==NULL)
		XCEPT_RAISE (xplore::exception::Exception, "Device '"+udn+"' doesn't exist anymore.");

	xplore::xupnp::DiscoveryService* ds=getDiscoveryService(dev->getFriendlyName());
	xplore::Advertisement::Reference a=DeviceHelper::getAdvertisement(dev, id);
	ds->addRemoteAdvertisement(a);
}

void xplore::xupnp::Platform::remAdv(std::string udn, std::string id) throw (xplore::exception::Exception)
{
//	std::cout << "remAdv(" << udn << "," << id << ");" << std::endl;
	CyberLink::Device *dev=ctrl_.getDevice(udn.c_str());
	if(dev==NULL)
		XCEPT_RAISE (xplore::exception::Exception, "Device '"+udn+"' doesn't exist anymore.");

	xplore::xupnp::DiscoveryService* ds=getDiscoveryService(dev->getFriendlyName());
	ds->delRemoteAdvertisement(id);
}

void xplore::xupnp::Platform::addDev(std::string udn, std::string groupname) throw (xplore::exception::Exception)
{
//	std::cout << "addDev(" << udn << "," << groupname << ");" << std::endl;
	CyberLink::Device *dev=ctrl_.getDevice(udn.c_str());
	if(dev==NULL)
		XCEPT_RAISE (xplore::exception::Exception, "Device '"+udn+"' doesn't exist anymore.");

	// ignore PeerGroups where I have no PeerGroupDevice
	if(groups_.find(dev->getFriendlyName())==groups_.end())
		return;

	CyberLink::Service *service=dev->getService("urn:xdaq.web.cern.ch:service:advs:1");
	if(service==NULL)
		XCEPT_RAISE (xplore::exception::Exception, "No Advertisement Service available.");

	if(!ctrl_.subscribe(service))
		XCEPT_RAISE (xplore::exception::Exception, "Subscription failed.");

	std::vector<xplore::Advertisement::Reference> advs=xplore::xupnp::DeviceHelper::getAdvertisementList(dev);

	DiscoveryService *ds=getDiscoveryService(groupname);
	ds->updateRemoteAdvertisements(advs, groupname);
}

void xplore::xupnp::Platform::remDev(std::string udn, std::string groupname) throw (xplore::exception::Exception)
{
//	std::cout << "remDev(" << udn << "," << groupname << ");" << std::endl;

	DiscoveryService* ds=getDiscoveryService(groupname);
	ds->delRemoteAdvertisements(udn);
}

void xplore::xupnp::Platform::reScan(std::string groupname)
	throw (xplore::exception::Exception)
{
//	std::cout << "reScan(" << groupname << ");" << std::endl;

	CyberUtil::Vector known;

	ctrl_.lock();
	CyberLink::DeviceList *list=ctrl_.getDeviceList();
	for(unsigned int i=0;i<list->size();i++)
	{
		CyberLink::Device *dev=list->getDevice(i);
		if(dev==NULL)
			continue;
		if(xplore::idIsLocal(dev->getUDN()))
			continue;
		if(groupname==dev->getFriendlyName())
			known.add(dev);
	}
	ctrl_.unlock();

	for(unsigned int i=0;i<known.size();i++)
	{
		try
		{
			CyberLink::Device *dev=(CyberLink::Device*)known.at(i);
			DiscoveryService *ds=getDiscoveryService(groupname);
			std::vector<xplore::Advertisement::Reference> advs=DeviceHelper::getAdvertisementList(dev, ds);
			ds->updateRemoteAdvertisements(advs, dev->getUDN());
		}
		catch (xplore::exception::Exception& e)
		{
			// Possible reasons for exception: Device, DiscoveryService or Advertisement removed
			// -> Advertisement is invalid and will be ignored
//			std::cout << "xcept (" << e.module() << "," << e.line() << "): " << e.message() << std::endl;
		}
	}
}

void xplore::xupnp::Platform::cleanupDevices()
{
	ctrl_.clean();
}

