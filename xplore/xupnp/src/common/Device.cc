// $Id: Device.cc,v 1.3 2005/08/23 07:06:48 xdaq Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2005, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: R. Moser                                                     *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include "xupnp/Device.h"

const char DEVICE_DESCRIPTION[] =
"<?xml version=\"1.0\" ?>"
"<root xmlns=\"urn:schemas-upnp-org:device-1-0\">"
"	<specVersion>"
"		<major>1</major>"
"		<minor>0</minor>"
"	</specVersion>"
"	<device>"
"		<deviceType>urn:xdaq.web.cern.ch:device:peergroup:1</deviceType>"
// set 'friendlyName to name of this peer group
// set 'UDN' to xplore::generateId()
"		<manufacturer>CERN</manufacturer>"
"		<manufacturerURL>http://xdaq.web.cern.ch</manufacturerURL>"
"		<modelDescription>XDAQ Discovery Device</modelDescription>"
"		<modelName>XDAQ Peer Group Device</modelName>"
"		<modelNumber>1.0</modelNumber>"
"		<modelURL>http://xdaq.weg.cern.ch</modelURL>"
"		<serviceList>"
"			<service>"
"				<serviceType>urn:xdaq.web.cern.ch:service:advs:1</serviceType>"
"				<serviceId>urn:xdaq.web.cern.ch:serviceId:advs:1</serviceId>"
"				<SCPDURL>/service_advs.xml</SCPDURL>"
"				<controlURL>/service/advs/control</controlURL>"
"				<eventSubURL>/service/advs/eventSub</eventSubURL>"
"			</service>"
"		</serviceList>"
"	</device>"
"</root>";

const char SERVICE_DESCRIPTION[] =
"<?xml version=\"1.0\"?>"
"<scpd xmlns=\"urn:schemas-upnp-org:service-1-0\" >"
"	<specVersion>"
"		<major>1</major>"
"		<minor>0</minor>"
"	</specVersion>"
"	<actionList>"
"		<action>"
// retrieve the number of advertisement in this PeerGroup within this process
"			<name>getAdvListSize</name>"
"			<argumentList>"
"				<argument>"
"					<name>Size</name>"
"					<direction>out</direction>"
"				</argument>"
"			</argumentList>"
"		</action>"
"		<action>"
// retrieve an AdvertisementId by an index within the list - needs to be iterated in reverse (otherwise advertisements
// could be lost.
"			<name>getAdvId</name>"
"			<argumentList>"
"				<argument>"
"					<name>Index</name>"
"					<direction>in</direction>"
"				</argument>"
"				<argument>"
"					<name>Id</name>"
"					<direction>out</direction>"
"				</argument>"
"			</argumentList>"
"		</action>"
"		<action>"
// retrieve the number of properties of this advertisement
"			<name>getPropertiesSize</name>"
"			<argumentList>"
"				<argument>"
"					<name>Id</name>"
"					<direction>in</direction>"
"				</argument>"
"				<argument>"
"					<name>Size</name>"
"					<direction>out</direction>"
"				</argument>"
"			</argumentList>"
"		</action>"
// retrieve a property using an index between 0 and size-1 to retrieve the name and value of one property
"		<action>"
"			<name>getProperty</name>"
"			<argumentList>"
"				<argument>"
"					<name>Id</name>"
"					<direction>in</direction>"
"				</argument>"
"				<argument>"
"					<name>Index</name>"
"					<direction>in</direction>"
"				</argument>"
"				<argument>"
"					<name>Name</name>"
"					<direction>out</direction>"
"				</argument>"
"				<argument>"
"					<name>Value</name>"
"					<direction>out</direction>"
"				</argument>"
"			</argumentList>"
"		</action>"
"	</actionList>"
"	<serviceStateTable>"
"		<stateVariable sendEvents=\"yes\">"
// add new Advertisement with specified id
"			<name>addAdv</name>"
"			<dataType>string</dataType>"
"		</stateVariable>"
"		<stateVariable sendEvents=\"yes\">"
// remove Advertisement with specified id
"			<name>remAdv</name>"
"			<dataType>string</dataType>"
"		</stateVariable>"
"	</serviceStateTable>"
"</scpd>";

xplore::xupnp::Device::Device(std::string groupName, std::string id)
{
	/* initialize peer group device */
	dev_.loadDescription(DEVICE_DESCRIPTION);

	CyberLink::Service *service=dev_.getService("urn:xdaq.web.cern.ch:service:advs:1");
	if(service==NULL)
		XCEPT_RAISE (xplore::exception::Exception, "Loading upnp device description failed.");

	bool ret=service->loadSCPD(SERVICE_DESCRIPTION);
	if(ret==false)
		XCEPT_RAISE (xplore::exception::Exception, "Loading advertisement service description failed.");

	dev_.setUDN(id.c_str());
	dev_.setFriendlyName(groupName.c_str());
	// TODO make configureable?
	dev_.setLeaseTime(60);
}

xplore::xupnp::Device::~Device()
{
}

// upnp callback function for actions
bool xplore::xupnp::Device::actionControlReceived(CyberLink::Action *action)
{
//	std::cout << "called " << action->getName() << "(...)" << std::endl;
	try
	{
		const char *name=action->getName();
		if(strcmp(name, "getAdvListSize")==0)
		{
			std::string size=convert(getAdvListSize());
			action->setArgumentValue("Size", size.c_str());
			return true;
		}
		if(strcmp(name, "getAdvId")==0)
		{
			unsigned int index=action->getArgumentIntegerValue("Index");
			std::string id=getAdvId(index);
			action->setArgumentValue("Id", id.c_str());
			return true;
		}
		if(strcmp(name, "getPropertiesSize")==0)
		{
			std::string id=action->getArgumentValue("Id");
			std::string size=convert(getPropertiesSize(id));
			action->setArgumentValue("Size", size.c_str());
			return true;
		}
		if(strcmp(name, "getProperty")==0)
		{
			std::string id=action->getArgumentValue("Id");
			unsigned int index=action->getArgumentIntegerValue("Index");
			std::pair<std::string, std::string> prop=getProperty(id, index);
			action->setArgumentValue("Name",prop.first.c_str());
			action->setArgumentValue("Value", prop.second.c_str());
			return true;
		}

		action->setStatus(CyberLink::UPnP::INVALID_ACTION, "Invalid Function called");
	}
	catch (xplore::exception::Exception& e)
	{
		action->setStatus(CyberLink::UPnP::ACTION_FAILED, e.message().c_str());
	}
	return false;
}

void xplore::xupnp::Device::sendAddAdvertisement(xplore::Advertisement::Reference adv)
	throw (xplore::exception::Exception)
{
	CyberLink::StateVariable *stateVar=dev_.getStateVariable("addAdv");
	if(stateVar==NULL)
		XCEPT_RAISE (xplore::exception::Exception, "'addAdv' Variable not available at this PeerGroupDevice.");

	std::string udn=dev_.getUDN();
	// necessary as uuid of service may not be correct
	stateVar->setValue((udn+"|"+adv->getID()).c_str());

	// clear variable after notification
	stateVar->setValue("");
}

void xplore::xupnp::Device::sendRemoveAdvertisement(xplore::Advertisement::Reference adv)
	throw (xplore::exception::Exception)
{
	CyberLink::StateVariable *stateVar=dev_.getStateVariable("remAdv");
	if(stateVar==NULL)
		XCEPT_RAISE (xplore::exception::Exception, "'remAdv' Variable not available at this PeerGroupDevice.");

	std::string udn=dev_.getUDN();
	// necessary as uuid of service may not be correct
	stateVar->setValue((udn+"|"+adv->getID()).c_str());

	// clear variable after notification
	stateVar->setValue("");
}
