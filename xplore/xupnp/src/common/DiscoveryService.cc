// $Id: DiscoveryService.cc,v 1.5 2008/07/18 15:28:48 gutleber Exp $
/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber, G. Lo Presti and L. Orsini                      *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include "toolbox/string.h"
#include "xcept/tools.h"
#include "xplore/DiscoveryEvent.h"
#include "xplore/DiscoveryListener.h"
#include "xplore/Id.h"
#include "xupnp/DiscoveryService.h"
#include "xupnp/Platform.h"
#include "xplore/AdvertisementImpl.h"
#include "xplore/Id.h"

#include <iostream>
#include <vector>

#define __socklen_t_defined
#include <unistd.h>

xplore::xupnp::DiscoveryService::DiscoveryService (const std::string& groupName, std::string id, xplore::xupnp::Notifier *notifier)
throw (xplore::exception::Exception) :Device(groupName, id), mutex_(BSem::FULL)
{
	listener_ = 0;

	groupName_=groupName;
	notifier_=notifier;

	dev_.setActionListener(this);
	dev_.start();
}

xplore::xupnp::DiscoveryService::~DiscoveryService ()
{
	dev_.setActionListener(NULL);
	dev_.stop();
}

std::vector<xplore::Advertisement::Reference>
xplore::xupnp::DiscoveryService::getKnownAdvertisements()
throw (xplore::exception::Exception)
{
	mutex_.take();
	// Return all local and remote cached advs
	std::vector<xplore::Advertisement::Reference> result;
	std::map<std::string, xplore::Advertisement::Reference, std::less<std::string> >::iterator i;
	for (i = advs_.begin(); i != advs_.end(); i++)
	{
		result.push_back((*i).second);
	}
	mutex_.give();
	return result;
}

std::vector<xplore::Advertisement::Reference>
xplore::xupnp::DiscoveryService::getKnownAdvertisements(const std::string& advType)
throw (xplore::exception::Exception)
{
	mutex_.take();
	// Return all local and remote cached advs
	std::vector<xplore::Advertisement::Reference> result;
	std::map<std::string, xplore::Advertisement::Reference, std::less<std::string> >::iterator i;
	for (i = advs_.begin(); i != advs_.end(); i++)
	{
		xplore::Advertisement::Reference advr = (*i).second;
		if (advr->getType() == advType)
		{
			result.push_back((*i).second);
		}
	}
	mutex_.give();
	return result;
}

std::vector<xplore::Advertisement::Reference>
xplore::xupnp::DiscoveryService::getKnownAdvertisements(const std::string& advType, const std::string& property, const std::string& propertyValue)
throw (xplore::exception::Exception)
{
	mutex_.take();
	// Return all local and remote cached advs
	std::vector<xplore::Advertisement::Reference> result;
	std::map<std::string, xplore::Advertisement::Reference, std::less<std::string> >::iterator i;
	for (i = advs_.begin(); i != advs_.end(); i++)
	{
		xplore::Advertisement::Reference advr = (*i).second;
		if ((advType=="" || (advr->getType() == advType) ) && (advr->getProperty(property) == propertyValue))
		{
			result.push_back((*i).second);
		}
	}
	mutex_.give();
	return result;
}

void
xplore::xupnp::DiscoveryService::searchRemoteAdvertisements(const std::string& advType) throw (xplore::exception::Exception)
{
	// blocking scan
	notifier_->addScan(groupName_, true);

	mutex_.take();
	xplore::DiscoveryEvent* e = new xplore::DiscoveryEvent(this, listener_);

	std::map<std::string, xplore::Advertisement::Reference, std::less<std::string> >::iterator i;
	for (i = advs_.begin(); i != advs_.end(); i++)
	{
		if ( ((*i).second)->getType() == advType )
			e->addSearchResult((*i).second);
	}

	if (listener_ != 0)
		listener_->discoveryEvent(xplore::DiscoveryEvent::Reference(e));

	mutex_.give();
}

void
xplore::xupnp::DiscoveryService::flushAdvertisements(const std::string& advType)
throw (xplore::exception::Exception)
{
	mutex_.take();
	try
	{
		std::vector<xplore::Advertisement::Reference> known;

		std::map<std::string, xplore::Advertisement::Reference, std::less<std::string> >::iterator i;
		for (i = advs_.begin(); i != advs_.end(); i++)
		{
			if (( advType == "" ) || (advType == ((*i).second)->getType()))
				known.push_back((*i).second);

		}

		for (unsigned long i = 0; i < known.size(); i++)
		{
			this->removeAdvertisement(known[i]);
		}
	}
	catch (xplore::exception::Exception& e)
	{
		mutex_.give();
		throw(e);
	}

	mutex_.give();
}

void
xplore::xupnp::DiscoveryService::flushAdvertisement(const std::string& id)
throw (xplore::exception::Exception)
{
	mutex_.take();
	try
	{
		// Check if the advertisement is cached
		std::map<std::string, xplore::Advertisement::Reference, std::less<std::string> >::iterator i;
		i = advs_.find(id);
		if (i != advs_.end())
		{
			// found, remove it
			this->removeAdvertisement( (*i).second );
		} else
		{
			// not found
			std::string msg = "Cannot flush advertisement with id ";
			msg += id;
			msg += ", advertisement not found";
			mutex_.give();
			XCEPT_RAISE (xplore::exception::Exception, msg);
		}
	}
	catch (xplore::exception::Exception& e)
	{
		mutex_.give();
		throw(e);
	}
	mutex_.give();
}

void
xplore::xupnp::DiscoveryService::flushAdvertisement(xplore::Advertisement::Reference adv)
throw (xplore::exception::Exception)
{
	mutex_.take();
	try
	{
		this->removeAdvertisement(adv);
	}
	catch (xplore::exception::Exception& e)
	{
		mutex_.give();
		throw(e);
	}
	mutex_.give();
}

void
xplore::xupnp::DiscoveryService::removeAdvertisement(xplore::Advertisement::Reference adv)
throw (xplore::exception::Exception)
{
	std::string id=adv->getID();
	std::string type=adv->getType();
	if(xplore::idIsLocal(id))
	{
		delAdvertisement(id);
		std::map<std::string, xplore::Advertisement::Reference, std::less<std::string> >::iterator i;
		for (i = advs_.begin(); i != advs_.end(); i++)
		{
			xplore::Advertisement::Reference advr = (*i).second;
			if (advr->getType() == type)
				break;
		}
		if(i==advs_.end())
			removeAdvertisementType(type);

		sendRemoveAdvertisement(adv);
	}
}

void
xplore::xupnp::DiscoveryService::publishAdvertisement(xplore::Advertisement::Reference adv) throw (xplore::exception::Exception)
{
	mutex_.take();
	try
	{
		if(xplore::idIsLocal(adv->getID()))
		{
			addAdvertisement(adv);
			sendAddAdvertisement(adv);
		}
	}
	catch (xplore::exception::Exception& e)
	{
		mutex_.give();
		throw(e);
	}
	mutex_.give();
}


void
xplore::xupnp::DiscoveryService::addServiceListener(xplore::DiscoveryListener* listener) throw (xplore::exception::Exception)
{
	listener_ = listener;
}

void
xplore::xupnp::DiscoveryService::removeServiceListener(xplore::DiscoveryListener* listener)
{
	listener = 0;
}

void
xplore::xupnp::DiscoveryService::removeAllServiceListeners()
{
	listener_ = 0;
}

// unsafe function - only used internally
void xplore::xupnp::DiscoveryService::addAdvertisement(xplore::Advertisement::Reference adv)
{
//	std::cout << "Add Adv: " << adv->getID() << std::endl;
	// Check if the advertisement is already cached, if not keep it
	// This is good for local and non local advertisements (since ID is unique for process as well)
	if (advs_.find(adv->getID()) == advs_.end())
	{
//		std::cout << "Advertisement not yet cached: " << adv->getID() << std::endl;
		advs_[adv->getID()] = adv;

		addAdvertisementType(adv->getType());
	}
}

// unsafe function - only used internally
void xplore::xupnp::DiscoveryService::delAdvertisement(std::string id)
{
//	std::cout << "Remove id: " << id << std::endl;
	advs_.erase(id);
}



std::vector<std::string>& xplore::xupnp::DiscoveryService::getKnownAdvertisementTypes()
{
	return advTypes_;
}

void xplore::xupnp::DiscoveryService::searchRemoteAdvertisementTypes() throw (xplore::exception::Exception)
{
	// TODO make new AdvertisementTypeListener
	// see searchRemoteAdvertisement()
}

// unsafe functions (not mutex locked themselves)
void xplore::xupnp::DiscoveryService::addAdvertisementType(const std::string& type)
{
	for(unsigned int i=0;i<advTypes_.size();i++)
		if(advTypes_[i]==type)
			return;
	advTypes_.push_back(type);
}

void xplore::xupnp::DiscoveryService::removeAdvertisementType(const std::string& type)
{
	std::vector<std::string>::iterator i;
	for(i=advTypes_.begin();i!=advTypes_.end();i++)
		if(type==(*i))
		{
			advTypes_.erase(i);
			return;
		}
}




// update functions for remote Advertisements
void xplore::xupnp::DiscoveryService::addRemoteAdvertisement(xplore::Advertisement::Reference adv)
{
	mutex_.take();
	addAdvertisement(adv);
	mutex_.give();
}

void xplore::xupnp::DiscoveryService::delRemoteAdvertisement(std::string id)
{
	mutex_.take();
	delAdvertisement(id);
	mutex_.give();
}

void xplore::xupnp::DiscoveryService::delRemoteAdvertisements(std::string id)
{
	std::vector<xplore::Advertisement::Reference> known;

	mutex_.take();

	std::map<std::string, xplore::Advertisement::Reference, std::less<std::string> >::iterator i;
	for (i = advs_.begin(); i != advs_.end(); i++)
	{
		if(idsAreSameProcess((*i).second->getID(), id))
			known.push_back((*i).second);
	}

	for (unsigned long i = 0; i < known.size(); i++)
		this->delAdvertisement(known[i]->getID());

	mutex_.give();
}

void xplore::xupnp::DiscoveryService::updateRemoteAdvertisements(std::vector<xplore::Advertisement::Reference> advs, std::string id)
{
	mutex_.take();

	if(id!="")
	{
		// remove all advertisements of the specified root device
		std::vector<xplore::Advertisement::Reference> known;
		std::map<std::string, xplore::Advertisement::Reference, std::less<std::string> >::iterator i;
		for (i = advs_.begin(); i != advs_.end(); i++)
		{
			if(idsAreSameProcess((*i).second->getID(), id))
				known.push_back((*i).second);
		}

		for (unsigned long i = 0; i < known.size(); i++)
			this->delAdvertisement(known[i]->getID());
	}

	// add all new found devices in advs vector
	for(unsigned int i=0;i<advs.size();i++)
	{
		this->addAdvertisement(advs[i]);
	}
	mutex_.give();
}

void xplore::xupnp::DiscoveryService::removeLocalPeerGroupAdvertisements(std::string groupName) throw (xplore::exception::Exception)
{
	// flush all local PeerGroupAdvertisements with the name of the peer group to leave
	mutex_.take();

	try
	{
		std::vector<xplore::Advertisement::Reference> known;

		std::map<std::string, xplore::Advertisement::Reference, std::less<std::string> >::iterator i;
		for (i = advs_.begin(); i != advs_.end(); i++)
		{
			if ((((*i).second)->getType())=="PeerGroup" &&
			   ((*i).second)->getProperty("name")==groupName &&
			   idIsLocal(((*i).second)->getID()))
				known.push_back((*i).second);
		}

		for (unsigned long i = 0; i < known.size(); i++)
		{
			this->removeAdvertisement(known[i]);
		}
	}
	catch (xplore::exception::Exception& e)
	{
		mutex_.give();
		throw(e);
	}


	mutex_.give();
}




// callback functions for retrieving advertisments and their properties
unsigned int xplore::xupnp::DiscoveryService::getAdvListSize() throw (xplore::exception::Exception)
{
	// only count local advertisements
	mutex_.take();
	std::map<std::string, xplore::Advertisement::Reference, std::less<std::string> >::iterator i;
	unsigned int size=0;
	for(i = advs_.begin();i != advs_.end(); i++)
	{
		if(xplore::idIsLocal((*i).first))
			size++;
	}
	mutex_.give();
	return size;
}

std::string xplore::xupnp::DiscoveryService::getAdvId(unsigned int index) throw (xplore::exception::Exception)
{
	// only index local advertisements
	mutex_.take();
	std::map<std::string, xplore::Advertisement::Reference, std::less<std::string> >::iterator i;
	unsigned int j=0;
	std::string id="";
	for(i = advs_.begin();i != advs_.end(); i++)
	{
		if(!xplore::idIsLocal((*i).first))
			continue;
		if(j==index)
		{
			id=(*i).first;
			break;
		}
		j++;
	}
	mutex_.give();

	if(id=="")
		XCEPT_RAISE (xplore::exception::Exception, "Invalid Index specified.");

	return id;
}

unsigned int xplore::xupnp::DiscoveryService::getPropertiesSize(std::string id) throw (xplore::exception::Exception)
{
	mutex_.take();
	std::map<std::string, xplore::Advertisement::Reference, std::less<std::string> >::iterator i;
	i = advs_.find(id);
	if (i == advs_.end())
	{
		mutex_.give();
		XCEPT_RAISE (xplore::exception::Exception, "Advertisement not found.");
	}
	xplore::Advertisement::Reference adv=(*i).second;
	mutex_.give();
	return adv->getPropertyNames().size();
}

std::pair<std::string, std::string> xplore::xupnp::DiscoveryService::getProperty(std::string id, unsigned int index) throw (xplore::exception::Exception)
{
	mutex_.take();
	std::pair<std::string, std::string> prop;
	prop.first="";
	prop.second="";
	std::map<std::string, xplore::Advertisement::Reference, std::less<std::string> >::iterator i;
	i = advs_.find(id);
	if (i == advs_.end())
	{
		mutex_.give();
		XCEPT_RAISE (xplore::exception::Exception, "Advertisement not found.");
	}

	xplore::Advertisement::Reference adv=(*i).second;

	std::vector<std::string> props=adv->getPropertyNames();
	if(index>=props.size())
	{
		mutex_.give();
		XCEPT_RAISE (xplore::exception::Exception, "Invalid index specified.");
	}

	prop.first=props[index];
	prop.second=adv->getProperty(prop.first);
	mutex_.give();

	return prop;
}
