// $Id: PeerGroup.cc,v 1.3 2008/07/18 15:28:48 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber, G. Lo Presti and L. Orsini                      *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include <string>

#include "xupnp/Platform.h"

/*! This programs creates and searches all supported
    advertisement types.
    This program uses the xupnp classes directly.
 */

class SearchResult: public xplore::DiscoveryListener
{
	public:
	    void discoveryEvent(xplore::DiscoveryEvent::Reference e)
	    {
	    	std::vector<xplore::Advertisement::Reference>& advList = e->getResult();
		for (unsigned int i = 0; i < advList.size(); i++)
		{
			std::cout << "--- BEGIN OF ADVERTISEMENT ---" << std::endl;
			std::vector<std::string> properties = advList[i]->getPropertyNames();
			for (unsigned int j = 0; j < properties.size(); j++)
			{
				std::cout << properties[j] << " - " << advList[i]->getProperty(properties[j]);
				std::cout << std::endl;
			}
			std::cout << "--- END OF ADVERTISEMENT ---" << std::endl;
			
			
		}
	    }	
};

int main (int argc, char**argv)
{
	try
	{
		xplore::xupnp::Platform* p = xplore::xupnp::Platform::getInstance();
		xplore::PeerGroup* g = p->getNetPeerGroup();
		xplore::DiscoveryService* ds = g->getDiscoveryService();

		xplore::Advertisement::Reference adv = p->createAdvertisement("PeerGroup");
		adv->setProperty("name", "new");

		ds->publishAdvertisement(adv);

		xplore::PeerGroup* g2 = p->joinPeerGroup("new");
		xplore::DiscoveryService* ds2 = g2->getDiscoveryService();

		xplore::Advertisement::Reference adv1 = p->createAdvertisement("Peer");
		adv1->setProperty("svc", "http://testmachine.org");
		adv1->setProperty("name", "TestApplication(default)");

		xplore::Advertisement::Reference adv2 = p->createAdvertisement("Peer");
		adv2->setProperty("svc", "http://testmachine.org");
		adv2->setProperty("name", "TestApplication(new)");

		std::cout << "Press any key to publish advertisements" << std::endl;

		getchar();

		// Publish advertisement
		ds->publishAdvertisement(adv1);
		ds2->publishAdvertisement(adv2);

		std::cout << "Press any key to search for peer advertisements" << std::endl;

		getchar();

		// Look for advertisement types
		SearchResult listener;
		ds->addServiceListener(&listener);

		std::cout << "PeerGroup: default" << std::endl;
		ds->searchRemoteAdvertisements("Peer");

		std::cout << "Press any key to search for peer advertisements" << std::endl;

		getchar();

		// Look for advertisement types
		//SearchResult listener;
		ds2->addServiceListener(&listener);

		std::cout << "PeerGroup: new" << std::endl;
		ds2->searchRemoteAdvertisements("Peer");

		std::cout << "Press any key to search for peer group advertisements" << std::endl;

		getchar();

		// Look for advertisement types
		//SearchResult listener;
		ds->addServiceListener(&listener);

		std::cout << "PeerGroup: default" << std::endl;
		ds->searchRemoteAdvertisements("PeerGroup");

		std::cout << "Press any key to remove PeerGroup 'new'" << std::endl;

		getchar();

		p->leavePeerGroup("new");
		try
		{
			p->getPeerGroup("new");
			std::cout << "PeerGroup (new) was not left." << std::endl;
		}
		catch(...)
		{ }

		std::cout << "Press any key search for PeerGroupAdvertisements" << std::endl;

		getchar();

		ds->addServiceListener(&listener);

		std::cout << "PeerGroup: default" << std::endl;
		ds->searchRemoteAdvertisements("PeerGroup");

		ds->flushAdvertisements();

		xplore::xupnp::Platform::destroyInstance();
	}
	catch (xplore::exception::Exception& e)
	{
		std::cout << "Caught: " << e.message() << std::endl;
	}
	return 0;
}
