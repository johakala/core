// $Id: Publish4.cpp,v 1.3 2008/07/18 15:28:48 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber, G. Lo Presti and L. Orsini                      *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include <string>

#include "xupnp/Platform.h"

/*! This programs creates and searches all supported
    advertisement types.
    This program uses the xupnp classes directly.
 */

class SearchResult: public xplore::DiscoveryListener
{
	public:
	    void discoveryEvent(xplore::DiscoveryEvent::Reference e)
	    {
	    	std::vector<xplore::Advertisement::Reference>& advList = e->getResult();
		for (unsigned int i = 0; i < advList.size(); i++)
		{
			std::cout << "--- BEGIN OF ADVERTISEMENT ---" << std::endl;
			std::vector<std::string> properties = advList[i]->getPropertyNames();
			for (unsigned int j = 0; j < properties.size(); j++)
			{
				std::cout << properties[j] << " - " << advList[i]->getProperty(properties[j]);
				std::cout << std::endl;
			}
			std::cout << "--- END OF ADVERTISEMENT ---" << std::endl;
			
			
		}
	    }	
};

int main (int argc, char**argv)
{
	try
	{
		xplore::xupnp::Platform* p = xplore::xupnp::Platform::getInstance();
		xplore::PeerGroup* g = p->getNetPeerGroup();
		xplore::DiscoveryService* ds = g->getDiscoveryService();

		xplore::Advertisement::Reference adv1 = p->createAdvertisement("Peer");
		adv1->setProperty("svc", "http://testmachine.org");
		adv1->setProperty("name", "TestApplication1");
		
		xplore::Advertisement::Reference adv2 = p->createAdvertisement("PeerGroup");
		adv2->setProperty("name", "TestGroup");
		
		xplore::Advertisement::Reference adv3 = p->createAdvertisement("Endpoint");
		adv3->setProperty("svc", "http://testmachine.org");
		adv3->setProperty("address", "tcp://mycomputer.org:50000");

		// Publish advertisement
		ds->publishAdvertisement(adv1);
		ds->publishAdvertisement(adv2);
		ds->publishAdvertisement(adv3);
		
		// See if they have been cached properly
		std::vector<xplore::Advertisement::Reference> a1;
		a1 = ds->getKnownAdvertisements();
		for (unsigned int i = 0; i < a1.size(); i++)
		{
			std::cout << "Cached before search: " << a1[i]->toString() << std::endl;
		}
		
		std::cout << std::endl;

		SearchResult listener;
		ds->addServiceListener(&listener);
		
		// Search for all advertisement types
		ds->searchRemoteAdvertisements("Endpoint");
		ds->searchRemoteAdvertisements("Peer");
		ds->searchRemoteAdvertisements("PeerGroup");
		
		// See if they have been cached properly
		std::vector<xplore::Advertisement::Reference> a2;
		a2 = ds->getKnownAdvertisements();
		for (unsigned int i = 0; i < a2.size(); i++)
		{
			std::cout << "Cached after search: " << a2[i]->toString() << std::endl;
		}
		
		std::cout << std::endl;
		
		// Remove advertisements by type
		ds->flushAdvertisements("Peer");
		ds->flushAdvertisements("PeerGroup");
		ds->flushAdvertisements("Endpoint");
		
		// See if they have been cached properly
		std::vector<xplore::Advertisement::Reference> a3;
		a3 = ds->getKnownAdvertisements();
		for (unsigned int i = 0; i < a3.size(); i++)
		{
			std::cout << "Cached after flush: " << a3[i]->toString() << std::endl;
		}
		
		std::cout << std::endl;
		
		// Search again after removal
		ds->searchRemoteAdvertisements("Endpoint");
		ds->searchRemoteAdvertisements("Peer");
		ds->searchRemoteAdvertisements("PeerGroup");
		
		xplore::xupnp::Platform::destroyInstance();
	} 
	catch (xplore::exception::Exception& e)
	{
		std::cout << "Caught: " << e.message() << std::endl;
	}
	return 0;
}
