// $Id: Publish1.cpp,v 1.3 2008/07/18 15:28:48 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber, G. Lo Presti and L. Orsini                      *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include <stdio.h>
#include <string>

#include "xupnp/Platform.h"

/*! This is a simple test program to publish a service and to look if
    it has been published correctly.
    this program uses the xslp classes directly.
 */

class SearchResult: public xplore::DiscoveryListener
{
	public:
	    void discoveryEvent(xplore::DiscoveryEvent::Reference e)
	    {
	    	std::vector<xplore::Advertisement::Reference>& advList = e->getResult();
		for (unsigned int i = 0; i < advList.size(); i++)
		{
			std::cout << "--- BEGIN OF ADVERTISEMENT ---" << std::endl;
			std::vector<std::string> properties = advList[i]->getPropertyNames();
			for (unsigned int j = 0; j < properties.size(); j++)
			{
				std::cout << properties[j] << " - " << advList[i]->getProperty(properties[j]);
				std::cout << std::endl;
			}
			std::cout << "--- END OF ADVERTISEMENT ---" << std::endl;
		}
	    }
};

int main (int argc, char**argv)
{
	try
	{
		xplore::xupnp::Platform* p = xplore::xupnp::Platform::getInstance();
		xplore::PeerGroup* g = p->getNetPeerGroup();
		xplore::DiscoveryService* ds = g->getDiscoveryService();

		xplore::Advertisement::Reference adv = p->createAdvertisement("Peer");
		adv->setProperty("svc", "http://testmachine.org");
		adv->setProperty("name", "TestApplication");

		std::cout << "Press any key to publish the advertisement" << std::endl;

		getchar();

		// Publish advertisement
		ds->publishAdvertisement(adv);

		std::cout << "Press any key to search for advertisements" << std::endl;

		getchar();

		SearchResult listener;
		ds->addServiceListener(&listener);

		// Search for advertisement
		ds->searchRemoteAdvertisements("Peer");

		std::cout << "Press any key to flush the advertisement" << std::endl;

		getchar();

		// Remove advertisement
		ds->flushAdvertisement(adv);

		std::cout << std::endl;
		std::cout << "Flushed successfully the advertisement" << std::endl;

		// Search again after removal
		ds->searchRemoteAdvertisements("Peer");
		xplore::xupnp::Platform::destroyInstance();
	}
	catch (xplore::exception::Exception& e)
	{
		std::cout << "Caught: " << e.message() << std::endl;
	}
	return 0;
}
