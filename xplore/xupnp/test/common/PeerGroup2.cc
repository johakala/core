// $Id: PeerGroup2.cc,v 1.3 2008/07/18 15:28:48 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber, G. Lo Presti and L. Orsini                      *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include <string>

#include "xupnp/Platform.h"

/*! This programs creates and searches all supported
    advertisement types.
    This program uses the xupnp classes directly.
 */

class SearchResult: public xplore::DiscoveryListener
{
	public:
	    void discoveryEvent(xplore::DiscoveryEvent::Reference e)
	    {
	    	std::vector<xplore::Advertisement::Reference>& advList = e->getResult();
		for (unsigned int i = 0; i < advList.size(); i++)
		{
			std::cout << "--- BEGIN OF ADVERTISEMENT ---" << std::endl;
			std::vector<std::string> properties = advList[i]->getPropertyNames();
			for (unsigned int j = 0; j < properties.size(); j++)
			{
				std::cout << properties[j] << " - " << advList[i]->getProperty(properties[j]);
				std::cout << std::endl;
			}
			std::cout << "--- END OF ADVERTISEMENT ---" << std::endl;
			

		}
	    }
};

int main (int argc, char **argv)
{
	int mode=0;
	if(argc==2)
		mode=atoi(argv[1]);

	if(argc!=2 || mode<0 || mode>2 )
	{
		std::cout << "Usage: " << argv[0] << "<0|1|2>" << std::endl;
		std::cout << " 0 for joining a peergroup with implicit PeerGroupAdvertisement if necessary" << std::endl;
		std::cout << " 1 for creating PeerGroupAdvertisement manually" << std::endl;
		std::cout << " 2 for joining a peergroup without PeerGroupAdvertisement" << std::endl;
		return -1;
	}


	try
	{

		xplore::xupnp::Platform* p = xplore::xupnp::Platform::getInstance();
		xplore::PeerGroup* g = p->getNetPeerGroup();
		xplore::DiscoveryService* ds = g->getDiscoveryService();

		std::cout << "Press any key to join PeerGroup" << std::endl;

		getchar();

		if(mode==0)
		{
			p->joinPeerGroup("new", true);
		}
		else
		{
			if(mode==1)
			{
				xplore::Advertisement::Reference adv = p->createAdvertisement("PeerGroup");
				adv->setProperty("name", "new");

				ds->publishAdvertisement(adv);
			}

			p->joinPeerGroup("new", false);
		}

		std::cout << "Press any key to search for peer group advertisements" << std::endl;

		getchar();

		// Look for advertisement types
		SearchResult listener;
		ds->addServiceListener(&listener);

		std::cout << "PeerGroup: default" << std::endl;
		ds->searchRemoteAdvertisements("PeerGroup");

		std::cout << "Press any key to remove PeerGroup 'new'" << std::endl;

		getchar();

		p->leavePeerGroup("new");
		try
		{
			p->getPeerGroup("new");
			std::cout << "PeerGroup (new) was not left." << std::endl;
		}
		catch(...)
		{ }

		std::cout << "Press any key search for PeerGroupAdvertisements" << std::endl;

		getchar();

		ds->addServiceListener(&listener);

		std::cout << "PeerGroup: default" << std::endl;
		ds->searchRemoteAdvertisements("PeerGroup");

		ds->flushAdvertisements();

		xplore::xupnp::Platform::destroyInstance();
	}
	catch (xplore::exception::Exception& e)
	{
		std::cout << "Caught: " << e.message() << std::endl;
	}
	return 0;
}
