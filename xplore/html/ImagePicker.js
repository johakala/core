dataSet = new Array();
// global vars
var winImagePicker;
var docImagePicker;

// Configurable parameters
var cnTop="200";//top coordinate of calendar window.
var cnLeft="500";//left coordinate of calendar window

function newImagePicker(fieldid) {
        winImagePicker=window.open("","ImagePicker","toolbar=0,status=0,menubar=0,fullscreen=no,width=300,height=300,scrollbars=1,resizable=1,top="+cnTop+",left="+cnLeft);
        docImagePicker = winImagePicker.document;
        renderImagePicker("Pick Image", fieldid);
}

function transmitFilename(filename, fieldid) {
        var f = document.getElementById(fieldid);
        f.value = filename;
        winImagePicker.close();
}

function renderImagePicker(WindowTitle, fieldid) {
        var vCalHeader;
        var vCalData;
        var vCalTime;
        var i;
        var j;
        var SelectStr;
        var vDayCount=0;
        var vFirstDay;

        docImagePicker.open();
        docImagePicker.writeln("<html><head>");
	docImagePicker.writeln("<link rel=\"stylesheet\" type=\"text/css\" href=\"/daq/xplore/html/ImagePicker.css\">");
	docImagePicker.writeln("<title>"+WindowTitle+"</title>");
        docImagePicker.writeln("</head><body>");
	docImagePicker.writeln("<h2>Click image to select</h2>");

        for(var i=0; i<dataSet.length; i++) {
                docImagePicker.writeln( "<div class=\"clickable\">" );
		docImagePicker.writeln("<img class=\"clickable\" onclick=\"window.opener.transmitFilename('"+dataSet[i]+"','"+fieldid+"')\" src=\""+dataSet[i]+"\"/>");
		docImagePicker.writeln("<\/div>" );
        }
        
        docImagePicker.writeln("</body></html>");
        docImagePicker.close();
}
