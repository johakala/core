// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xplore/Application.h"

#include <stdarg.h>
#include <string>
#include <iostream>
#include <sstream>

#include "xgi/Utils.h"
#include "xgi/Method.h"

#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"
#include "cgicc/HTTPResponseHeader.h"

#include "xdaq/ApplicationGroup.h"
#include "xdaq/ApplicationRegistry.h"
#include "xdaq/soap/utils.h"
#include "xdaq/InstantiateApplicationEvent.h"
#include "xdaq/EndpointAvailableEvent.h"
#include "xdaq/ApplicationDescriptorImpl.h"
#include "xcept/tools.h"
#include "xplore/PublishJobEvent.h"
#include "xplore/RemoveFilterEvent.h"
// XOAP Include files
#include "xoap/MessageFactory.h"
#include "xoap/SOAPMessage.h"
#include "xoap/SOAPBody.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPHeader.h"
#include "xoap/domutils.h"
#include "xoap/DOMParser.h"

#include "toolbox/net/URL.h"
#include "toolbox/Runtime.h"
#include "toolbox/task/WorkLoopFactory.h"

#include <xercesc/dom/DOM.hpp>

#include "xplore/exception/Exception.h"
#include "xplore/DiscoveryEvent.h"

#include "xdata/String.h"

#include "toolbox/task/TimerFactory.h"
#include "toolbox/TimeInterval.h"
#include "toolbox/stl.h"

#include "xgi/framework/Method.h"
XERCES_CPP_NAMESPACE_USE

XDAQ_INSTANTIATOR_IMPL (xplore::Application)

xplore::Application::Application (xdaq::ApplicationStub * s) throw (xdaq::exception::Exception)
	: xdaq::Application(s), xgi::framework::UIManager(this), mutex_(toolbox::BSem::FULL, true), dispatcher_("urn:xdaq-workloop:xplore", "waiting", 0.8)
{

	republishInterval_ = "3600";
	this->getApplicationInfoSpace()->fireItemAvailable("republishInterval", &republishInterval_);

	// Detect when the setting of default paramaters has been perfomed
	this->getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");

	xgi::framework::deferredbind(this, this, &xplore::Application::Default, "Default");

	xgi::bind(this, &xplore::Application::display, "display");
	xgi::bind(this, &xplore::Application::doaction, "doaction");
	xgi::bind(this, &xplore::Application::search, "search");

	getApplicationContext()->addActionListener(this); // attach to instantiate events

	s->getDescriptor()->setAttribute("icon", "/xplore/images/xplore-icon.png");

	std::set < std::string > zones = getApplicationContext()->getZoneNames();
	zoneFilter_ = "(|";

	std::set<std::string>::iterator i;
	for (i = zones.begin(); i != zones.end(); ++i)
	{
		zoneFilter_ += "(zone=*";
		zoneFilter_ += (*i);
		zoneFilter_ += "*)";
	}
	zoneFilter_ += ")";

	// Create periodi timer
	toolbox::TimeVal start;
	start = toolbox::TimeVal::gettimeofday();

	toolbox::task::Timer * timer = toolbox::task::getTimerFactory()->createTimer("DiscoveryTimer");
	toolbox::TimeInterval interval(30, 0); // period of 30 secs
	timer->scheduleAtFixedRate(start, this, interval, 0, "search");

	std::stringstream baseurl;
	baseurl << this->getApplicationDescriptor()->getContextDescriptor()->getURL() << "/" << this->getApplicationDescriptor()->getURN();

	// asynchrnous advertising
	dispatcher_.addActionListener(this);

	//
	std::list<xdaq::Application*> applications = getApplicationContext()->getApplicationRegistry()->getApplications();

	try
	{
		for (std::list<xdaq::Application*>::iterator i = applications.begin(); i != applications.end(); ++i)
		{
			this->publishApplication((*i)->getApplicationDescriptor());
		}
	}
	catch (xdaq::exception::Exception& e)
	{
		getApplicationContext()->removeActionListener(this);
		XCEPT_RETHROW(xdaq::exception::Exception, "Failed to publish application", e);
	}

	try
	{
		std::vector<const xdaq::Network*> networks = this->getApplicationContext()->getNetGroup()->getNetworks();

		for (std::vector<const xdaq::Network*>::iterator i = networks.begin(); i != networks.end(); ++i)
		{
			const xdaq::Endpoint * endpoint = (*i)->getEndpoint(this->getApplicationContext()->getContextDescriptor());
			this->publishEndpoint(endpoint, (*i));
		}
	}
	catch (xdaq::exception::Exception& e)
	{
		getApplicationContext()->removeActionListener(this);
		XCEPT_RETHROW(xdaq::exception::Exception, "Failed to publish endpoints", e);
	}

	// Activates work loop for sensor asynchronous operations
	(void) toolbox::task::getWorkLoopFactory()->getWorkLoop("urn:xdaq-workloop:xplore", "waiting")->activate();
}

// process discovery
void xplore::Application::timeExpired (toolbox::task::TimerEvent& e)
{
	mutex_.take();

	// re-publishing of executive descriptor descriptors
	if (e.getTimerTask()->name == "publish")
	{
		std::list<xdaq::Application*> applications = getApplicationContext()->getApplicationRegistry()->getApplications();

		LOG4CPLUS_DEBUG(getApplicationLogger(), "Republishing services...");

		try
		{
			for (std::list<xdaq::Application*>::iterator i = applications.begin(); i != applications.end(); ++i)
			{
				this->publishApplication((*i)->getApplicationDescriptor());
			}
		}
		catch (xdaq::exception::Exception& e)
		{
			this->notifyQualified("error", e);

		}

		LOG4CPLUS_DEBUG(getApplicationLogger(), "Republishing endpoints...");

#warning "The STL might not be thread safe therefore make netowrk structure thread safe"

		std::vector<const xdaq::Network*> networks = this->getApplicationContext()->getNetGroup()->getNetworks();
		for (std::vector<const xdaq::Network*>::iterator i = networks.begin(); i != networks.end(); ++i)
		{
			const xdaq::Endpoint * endpoint = (*i)->getEndpoint(this->getApplicationContext()->getContextDescriptor());

			this->publishEndpoint(endpoint, (*i));

		}
	}

	//

	try
	{
		// search by listener and immediately dispatch
		std::multimap<toolbox::ActionListener *, std::pair<std::string, std::string> >::iterator i;
		for (i = filters_.begin(); i != filters_.end(); i++)
		{
			std::vector < xplore::Advertisement::Reference > resultSet;

			this->search((*i).second.first, (*i).second.second, resultSet);

			xplore::DiscoveryEvent e(resultSet, (*i).second.first, (*i).second.second);

			(*i).first->actionPerformed(e);
		}
	}
	catch (xdaq::exception::Exception & xe)
	{
		LOG4CPLUS_WARN(this->getApplicationLogger(), xcept::stdformat_exception_history(xe));
	}
	catch (std::exception& se)
	{
		LOG4CPLUS_WARN(this->getApplicationLogger(), se.what());
	}
	catch (...)
	{
		LOG4CPLUS_WARN(this->getApplicationLogger(), "Caught unknown exception when discovering peers");
	}

	mutex_.give();
}

void xplore::Application::search (const std::string& type, const std::string& filter, std::vector<xplore::Advertisement::Reference>& resultSet) throw (xdaq::exception::Exception)
{
	// toolbox::Runtime* r = toolbox::getRuntime();

	try
	{
		// disable signals only when shutdown listener active
		// r->disableSignals();

		// Filter example: "(zone=*),(group=*)"; 

		// "(&(|(zone=*bril*)(zone=*cdaq*))(zone=*bril*))"

		// Add zone filter so that the URLs discovered only come from the allowed zones
		std::stringstream f;
		f << "(&" << zoneFilter_ << filter << ")";

		LOG4CPLUS_DEBUG(this->getApplicationLogger(), "xplore::Application::search(): type = " << type << ", f = " << f.str());

		discoveryService_.search(type, f.str(), resultSet);

		// r->enableSignals();
	}
	catch (xslp::exception::Exception &e)
	{
		// r->enableSignals();
		XCEPT_RETHROW(xdaq::exception::Exception, "Failed to search and query a peergroup for an advertisement", e);
	}
}

void xplore::Application::retrieveProperties (const std::string& service, toolbox::Properties& properties) throw (xdaq::exception::Exception)
{
	try
	{
		discoveryService_.retrieveProperties(service, properties);
		if (properties.empty())
		{
			std::stringstream msg;
			msg << "Failed to retrieve attributes for advertisement '";
			msg << service << "'";
			XCEPT_RAISE(xdaq::exception::Exception, msg.str());
		}
	}
	catch (xslp::exception::Exception &e)
	{
		// r->enableSignals();
		std::stringstream msg;
		msg << "Failed to retrieve attributes for advertisement '";
		msg << service << "'";
		XCEPT_RETHROW(xdaq::exception::Exception, msg.str(), e);
	}
}

void xplore::Application::actionPerformed (toolbox::Event& e)
{
	try
	{
		if (e.type() == "urn:xdaq-event:InstantiateApplication")
		{
			xdaq::InstantiateApplicationEvent& ie = dynamic_cast<xdaq::InstantiateApplicationEvent&>(e);
			const xdaq::ApplicationDescriptor* descriptor = ie.getApplicationDescriptor();
			this->publishApplication(descriptor);

		}
		else if (e.type() == "xdaq::EndpointAvailableEvent")
		{
			xdaq::EndpointAvailableEvent& ie = dynamic_cast<xdaq::EndpointAvailableEvent&>(e);
			const xdaq::Endpoint* endpoint = ie.getEndpoint();
			const xdaq::Network* network = ie.getNetwork();

			this->publishEndpoint(endpoint, network);

		}
		else if (e.type() == "urn:xplore-event:PublishJobEvent") // New infospace created threfore update flashlist monitors
		{

			LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Asynchronous advertising");
			xplore::PublishJobEvent & event = dynamic_cast<xplore::PublishJobEvent&>(e);
			discoveryService_.publish(event.adv_);
		}
		else if (e.type() == "urn:xplore-event:RemoveFilterEvent")
		{
			xplore::RemoveFilterEvent & event = dynamic_cast<xplore::RemoveFilterEvent&>(e);
			toolbox::ActionListener * listener = event.listener_;

			mutex_.take();
			std::multimap<toolbox::ActionListener *, std::pair<std::string, std::string> >::iterator lower = filters_.lower_bound(listener);
			std::multimap<toolbox::ActionListener *, std::pair<std::string, std::string> >::iterator upper = filters_.upper_bound(listener);
			filters_.erase(lower, upper);
			mutex_.give();
		}
		/*
		 Shutdown listener callback
		 else if ( e.type() == "Shutdown" )
		 {
		 std::list<xdaq::Application*> applications = getApplicationContext()->getApplicationRegistry()->getApplications();

		 for (std::list<xdaq::Application*>::iterator i = applications.begin(); i != applications.end(); ++i)
		 {
		 this->revokeApplications( (*i)->getApplicationDescriptor() );
		 }
		 }
		 */
	}
	catch (xdaq::exception::Exception& xe)
	{
		LOG4CPLUS_WARN(this->getApplicationLogger(), xcept::stdformat_exception_history(xe));
	}
	catch (std::exception& se)
	{
		LOG4CPLUS_WARN(this->getApplicationLogger(), se.what());
	}

}

void xplore::Application::publishApplication (const xdaq::ApplicationDescriptor* descriptor) throw (xdaq::exception::Exception)
{
	if (descriptor->getAttribute("publish") != "true") return;

	// toolbox::Runtime* r = toolbox::getRuntime();

	// r->disableSignals();

	try
	{
		std::stringstream source;
		source << "service:peer:" << descriptor->getContextDescriptor()->getURL() << "/" << descriptor->getURN();

		xplore::Advertisement::Reference adv = discoveryService_.createAdvertisementFromSource(source.str());

		//std::string applicationURL = descriptor->getContextDescriptor()->getURL();


		const xdata::Properties & attributes = dynamic_cast<const xdata::Properties&>(*descriptor);

		for (std::map<std::string, std::string, std::less<std::string> >::const_iterator i = attributes.begin(); i != attributes.end(); ++i)
		{
			adv->setProperty((*i).first, (*i).second);
		}

		std::string csvzones = "";
		std::set < std::string > zones = getApplicationContext()->getZoneNames();
		// There is always at least one zone "default")		
		std::set<std::string>::iterator i = zones.begin();
		do
		{
			csvzones += (*i);
			++i;
			if (i != zones.end())
			{
				csvzones += ",";
			}
		} while (i != zones.end());

		adv->setProperty("zone", csvzones);

		std::string groups = descriptor->getAttribute("group");
		if (groups == "")
		{
			adv->setProperty("group", "default");
		}

		// discoveryService_.publish(adv);

		//
		toolbox::task::EventReference e(new xplore::PublishJobEvent(adv));
		try
		{
			//std::cout << "going to publish asynchronous" << std::endl; 
			dispatcher_.fireEvent(e);
		}
		catch (toolbox::task::exception::Overflow & e)
		{
			std::stringstream msg;
			msg << "Advertisement publishing lost " << e.what();
			LOG4CPLUS_WARN(this->getApplicationLogger(), msg.str());

		}
		catch (toolbox::task::exception::OverThreshold & e)
		{

		}
		catch (toolbox::task::exception::InternalError & e)
		{
			std::stringstream msg;
			msg << "Advertisement publishing lost";

			XCEPT_DECLARE_NESTED(xdaq::exception::Exception, q, msg.str(), e);
			this->notifyQualified("error", q);
		}

		//

	}
	catch (xslp::exception::Exception &e)
	{
		// r->enableSignals();
		XCEPT_RETHROW(xdaq::exception::Exception, "Failed to publish Application", e);
	}
	// r->enableSignals();
}

void xplore::Application::publishEndpoint (const xdaq::Endpoint* endpoint, const xdaq::Network* network) throw (xdaq::exception::Exception)
{
	// toolbox::Runtime* r = toolbox::getRuntime();

	// r->disableSignals();
	if (!endpoint->publish())
	{
		return;
	}
	try
	{
		pt::Address::Reference address = endpoint->getAddress();
		// std::string protocol = address->getProtocol();
		// std::string service = address->getService();	

		std::stringstream source;

		toolbox::net::URL contextURL = this->getApplicationContext()->getContextDescriptor()->getURL();

		source << "service:endpoint:" << address->getService() << "." << address->toString() << "#" << contextURL.getHost() << ":" << contextURL.getPort();

		//std::cout << "Publish endpoint: " << source.str() << std::endl;

		xplore::Advertisement::Reference adv = discoveryService_.createAdvertisementFromSource(source.str());
		adv->setProperty("network", network->getName());
		adv->setProperty("service", address->getService());
		adv->setProperty("protocol", address->getProtocol());
		adv->setProperty("address", address->toString());
		adv->setProperty("context", contextURL.toString());

		/*
		 Add attributes to advertisement

		 std::map<std::string, std::string, std::less<std::string> >& attributes =
		 dynamic_cast<xdaq::ApplicationDescriptorImpl*>(descriptor)->getProperties();
		 
		 for (std::map<std::string, std::string, std::less<std::string> >::iterator i = attributes.begin(); i != attributes.end(); ++i)
		 {
		 adv->setProperty ( (*i).first, (*i).second );
		 }
		 */

		std::string csvzones = "";
		std::set < std::string > zones = getApplicationContext()->getZoneNames();
		// There is always at least one zone "default")		
		std::set<std::string>::iterator i = zones.begin();
		do
		{
			csvzones += (*i);
			++i;
			if (i != zones.end())
			{
				csvzones += ",";
			}
		} while (i != zones.end());

		adv->setProperty("zone", csvzones);

		//
		//
		//discoveryService_.publish(adv);
		toolbox::task::EventReference e(new xplore::PublishJobEvent(adv));
		try
		{
			dispatcher_.fireEvent(e);
		}
		catch (toolbox::task::exception::Overflow & e)
		{
			std::stringstream msg;
			msg << "Advertisement publishing lost " << e.what();
			LOG4CPLUS_WARN(this->getApplicationLogger(), msg.str());

		}
		catch (toolbox::task::exception::OverThreshold & e)
		{

		}
		catch (toolbox::task::exception::InternalError & e)
		{
			std::stringstream msg;
			msg << "Advertisement publishing lost";

			XCEPT_DECLARE_NESTED(xdaq::exception::Exception, q, msg.str(), e);
			this->notifyQualified("error", q);
		}

	}
	catch (xslp::exception::Exception &e)
	{
		// r->enableSignals();
		XCEPT_RETHROW(xdaq::exception::Exception, "Failed to publish Endpoint", e);
	}
	// r->enableSignals();
}

void xplore::Application::Default (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	// Begin of tabs
	*out << "<div class=\"xdaq-tab-wrapper\">" << std::endl;

	// Tab 1
	*out << "<div class=\"xdaq-tab\" title=\"Settings\">" << std::endl;
	this->SettingsTabPage(out);
	*out << "</div>";

	// Tab 2
	*out << "<div class=\"xdaq-tab\"  title=\"Search\">" << std::endl;
	this->SearchTabPage(out);
	*out << "</div>";

	*out << "</div>"; // end of tab pane
}

void xplore::Application::doaction (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	try
	{
		cgicc::Cgicc cgi(in);

		std::string filter = "";
		if (xgi::Utils::hasFormElement(cgi, "filter"))
		{
			filter = xgi::Utils::getFormElement(cgi, "filter")->getValue();
		}
		std::string service = "";
		if (xgi::Utils::hasFormElement(cgi, "service"))
		{
			service = xgi::Utils::getFormElement(cgi, "service")->getValue();
		}
		else
		{
			XCEPT_RAISE(xgi::exception::Exception,"service not defined in form submission");
		}
		std::string zone = "";
		if (xgi::Utils::hasFormElement(cgi, "zone"))
		{
			zone = xgi::Utils::getFormElement(cgi, "zone")->getValue();
		}
		else
		{
			XCEPT_RAISE(xgi::exception::Exception,"zone not defined in form submission");
		}

		if (zone != "")
		{
			std::stringstream newFilter;
			newFilter << "(zone=" << zone << ")" << filter;
			filter = newFilter.str();
		}

		std::vector < xplore::Advertisement::Reference > resultSet;

		LOG4CPLUS_INFO(this->getApplicationLogger(), "Search : service=[" << service << "], filter=[" << filter << "]");

		*out << "{" << std::endl;
		*out << "\"messages\": [" << std::endl;

		try
		{
			this->search(service, filter, resultSet);

			for (std::vector<xplore::Advertisement::Reference>::iterator i = resultSet.begin(); i != resultSet.end(); ++i)
			{
				*out << "{" << std::endl;

				*out << "\"url\": \"" << cgicc::form_urlencode((*i)->toString()) << "\"," << std::endl;
				*out << "\"display\": \"" << (*i)->getURL() << "\"" << std::endl;

				*out << "}" << std::endl;
				std::vector<xplore::Advertisement::Reference>::iterator j = i;
				j++;
				if (j != resultSet.end()) *out << "," << std::endl;
			}
		}
		catch (xdaq::exception::Exception& e)
		{
			 XCEPT_RETHROW(xgi::exception::Exception,"failed search ", e);
		}

		*out << "]" << std::endl;
		*out << "}" << std::endl;
	}
	catch (xgi::exception::Exception& xe)
	{
		XCEPT_RETHROW(xgi::exception::Exception, "CGI processing error", xe);
	}
	catch (xcept::Exception & e)
	{
		XCEPT_RETHROW(xgi::exception::Exception, "invalid settings", e);
	}
	catch (std::exception e)
	{
		XCEPT_RAISE(xgi::exception::Exception, "Cannot interpret incoming CGI content");
	}

	out->getHTTPResponseHeader().addHeader("Content-Type", "text/json");
}

void xplore::Application::search (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	std::cout << "Calling xplore::Application::search(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception)" << std::endl;
	try
	{
		cgicc::Cgicc cgi(in);
		out->getHTTPResponseHeader().addHeader("Content-Type", "text/json");
		out->getHTTPResponseHeader().addHeader("Cache-Control", "max-age=30");
		*out << "{\"table\":{";
		// definition
		*out << "\"definition\":[";
		*out << "{\"key\":\"class\", \"type\":\"string\"},";
		*out << "{\"key\":\"className\", \"type\":\"string\"},";
		*out << "{\"key\":\"context\", \"type\":\"string\"},";
		*out << "{\"key\":\"domain\", \"type\":\"string\"},";
		*out << "{\"key\":\"group\", \"type\":\"string\"},";
		*out << "{\"key\":\"hasInstance\", \"type\":\"string\"},";
		*out << "{\"key\":\"heartbeat\", \"type\":\"string\"},";
		*out << "{\"key\":\"id\", \"type\":\"string\"},";
		*out << "{\"key\":\"instance\", \"type\":\"string\"},";
		*out << "{\"key\":\"network\", \"type\":\"string\"},";
		*out << "{\"key\":\"publish\", \"type\":\"string\"},";
		*out << "{\"key\":\"service\", \"type\":\"string\"},";
		*out << "{\"key\":\"uuid\", \"type\":\"string\"},";
		*out << "{\"key\":\"zone\", \"type\":\"string\"}";

		*out << "],";
		// rows
		*out << "\"rows\":[";

		std::string service = "";
		if (xgi::Utils::hasFormElement(cgi, "service"))
		{
			service = xgi::Utils::getFormElement(cgi, "service")->getValue();
		}

		std::string filter = "";
		if (xgi::Utils::hasFormElement(cgi, "filter"))
		{
			filter = xgi::Utils::getFormElement(cgi, "filter")->getValue();
		}

		try
		{
			std::vector < xplore::Advertisement::Reference > resultSet;
			this->search(service, filter, resultSet);

			std::vector<xplore::Advertisement::Reference>::iterator i;

			// Position absolute, but let space for the progress bar div in the top
			//
			//*out << "<div style=\"clear:both;\" id=list>" << std::endl;

			//Zones the current process is running
			std::set < std::string > localZones = getApplicationContext()->getZoneNames();
			bool first = true;
			for (i = resultSet.begin(); i != resultSet.end(); ++i)
			{
				std::string url = (*i)->toString();

				try
				{
					toolbox::Properties properties;
					this->retrieveProperties(url, properties);

					//Can be multiple zones!
					std::string zoneNames = properties.getProperty("zone");

					//Zones the remote process is running
					std::set<std::string> remoteZones;
					std::stringstream ss(zoneNames);
					std::string singleZone;
					while (std::getline(ss, singleZone, ','))
					{
						remoteZones.insert(singleZone);
					}

					//Filter out cases when SLP retrieves services with similar zone names, e.g., "daqval" and "daqvalP5dev"
					if ( toolbox::stl::intersection(localZones, remoteZones).size() > 0 )
					{
						// Display property table
						std::vector < std::string > names = properties.propertyNames();
						std::vector<std::string>::iterator ni;

						if (!first)
						{
							*out << ",";
						}
						else
						{
							first = false;
						}
						*out << "{";

						bool second = true;
						for (ni = names.begin(); ni != names.end(); ++ni)
						{
							if (!second)
							{
								*out << ",";
							}
							else
							{
								second = false;
							}
							std::string value = properties.getProperty(*ni);
							*out << "\"" << *ni << "\":\"" << properties.getProperty(*ni) << "\"";

						}
						*out << "}";
					}
					else
					{
						LOG4CPLUS_DEBUG(this->getApplicationLogger(), "xplore::Application::search(): filtering out zone " << zoneNames);
					}
				}
				catch (xdaq::exception::Exception& e)
				{
					XCEPT_RETHROW(xgi::exception::Exception, "discovery retrieve service properties error", e);
				}

			}

			*out << "]}";
			*out << "}";
		}
		catch (xdaq::exception::Exception& e)
		{
			XCEPT_RETHROW(xgi::exception::Exception, "discovery retrieve services error", e);
		}

	}
	catch (xdaq::exception::Exception& e)
	{
		XCEPT_RETHROW(xgi::exception::Exception, "discovery retrieve services error", e);
	}

}

void xplore::Application::addListener (toolbox::ActionListener * listener, const std::string & service, const std::string & filter) throw (xplore::exception::Exception)
{
	if ((service != "peer") && (service != "endpoint"))
	{
		std::stringstream msg;
		msg << "service '" << service << "' not supported";
		XCEPT_RAISE(xplore::exception::Exception, msg.str());
	}

	mutex_.take();
	std::multimap<toolbox::ActionListener *, std::pair<std::string, std::string> >::iterator lower = filters_.lower_bound(listener);
	std::multimap<toolbox::ActionListener *, std::pair<std::string, std::string> >::iterator upper = filters_.upper_bound(listener);
	for (std::multimap<toolbox::ActionListener *, std::pair<std::string, std::string> >::iterator iter = lower; iter != upper; iter++)
	{
		if (iter->second.second == filter)
		{
			mutex_.give();
			/*
			 std::stringstream msg;
			 msg << "filter '" << filter << "' already specified for listener";
			 XCEPT_RAISE (xplore::exception::Exception, msg.str());
			 */
			return;
		}
	}
	//filters_.insert(std::pair<toolbox::ActionListener *,std::string>(listener,filter));
	filters_.insert(std::pair<toolbox::ActionListener *, std::pair<std::string, std::string> >(listener, std::pair<std::string, std::string>(service, filter)));

	mutex_.give();
}

void xplore::Application::removeListener (toolbox::ActionListener * listener) throw (xplore::exception::Exception)
{
	//
	toolbox::task::EventReference e(new xplore::RemoveFilterEvent(listener));
	try
	{
		//std::cout << "going to publish asynchronous" << std::endl;
		dispatcher_.fireEvent(e);
	}
	catch (toolbox::task::exception::Overflow & e)
	{

		XCEPT_RAISE(xplore::exception::Exception, "cannot remove filter");

	}
	catch (toolbox::task::exception::OverThreshold & e)
	{
		XCEPT_RAISE(xplore::exception::Exception, "cannot remove filter");
	}
	catch (toolbox::task::exception::InternalError & e)
	{
		XCEPT_RAISE(xplore::exception::Exception, "cannot remove filter");
	}
	/*
	 mutex_.take();
	 std::multimap<toolbox::ActionListener *, std::pair<std::string,std::string> >::iterator lower = filters_.lower_bound(listener);
	 std::multimap<toolbox::ActionListener *, std::pair<std::string,std::string> >::iterator upper = filters_.upper_bound(listener);
	 filters_.erase(lower,upper);
	 mutex_.give();
	 */

}

void xplore::Application::actionPerformed (xdata::Event& e)
{
	if (e.type() == "urn:xdaq-event:setDefaultValues")
	{
		// republish interval setting
		toolbox::TimeVal start;
		start = toolbox::TimeVal::gettimeofday();
		toolbox::task::Timer * timer = toolbox::task::getTimerFactory()->getTimer("DiscoveryTimer");
		toolbox::TimeInterval republishInterval(toolbox::toLong(republishInterval_.toString()), 0); // period of 1h  default
		timer->scheduleAtFixedRate(start, this, republishInterval, 0, "publish");
	}
}

void xplore::Application::SettingsTabPage (xgi::Output * out)
{
	*out << cgicc::table().set("class", "xdaq-table") << std::endl;

	*out << cgicc::thead() << std::endl;
	*out << cgicc::tr();
	*out << cgicc::th("Setting");
	*out << cgicc::th("Value");
	*out << cgicc::tr() << std::endl;
	*out << cgicc::thead() << std::endl;

	*out << cgicc::tbody() << std::endl;

	*out << cgicc::tr();
	*out << "<td class=\"xdaq-tooltip\">" << "Republish interval" << " <span class=\"xdaq-tooltip-info\">" << "Time for refresh of slp publishing" << "</span></td>";
	*out << "<td>" << republishInterval_.toString() << "</td>";
	*out << cgicc::tr() << std::endl;

	*out << cgicc::tbody();
	*out << cgicc::table();
}

void xplore::Application::SearchTabPage (xgi::Output * out)
{
	std::stringstream baseurl;
	baseurl << this->getApplicationDescriptor()->getContextDescriptor()->getURL() << "/" << this->getApplicationDescriptor()->getURN();

	*out << cgicc::table().set("class", "xdaq-table").set("id", "xdaq-xplore").set("data-xplore-callback", baseurl.str());

	*out << cgicc::thead() << std::endl;
	*out << cgicc::tr();
	*out << cgicc::th("Zone");
	*out << cgicc::th("Peer type");
	*out << cgicc::th("Filter");
	*out << cgicc::th("");
	*out << cgicc::tr() << std::endl;
	*out << cgicc::thead() << std::endl;

	*out << cgicc::tbody() << std::endl;

	*out << cgicc::tr().set("class", "xdaq-table-nohover");

	// Zones combo box
	*out << cgicc::td();
	*out << cgicc::select().set("name", "zone").set("id", "zone") << std::endl;

	std::set < std::string > zones = getApplicationContext()->getZoneNames();
	std::set<std::string>::iterator zi;
	for (zi = zones.begin(); zi != zones.end(); ++zi)
	{
		*out << cgicc::option(*zi);
	}

	*out << cgicc::select() << std::endl;
	*out << cgicc::td() << std::endl;

	// Group combo box
	*out << cgicc::td();
	*out << cgicc::select().set("name", "service").set("id", "service") << std::endl;

	*out << cgicc::option("peer");
	*out << cgicc::option("endpoint");

	*out << cgicc::select() << std::endl;
	*out << cgicc::td() << std::endl;

	// Input field for filter expression
	*out << cgicc::td();
	*out << cgicc::input().set("type", "text").set("name", "filter").set("size", "40").set("id", "filterInput") << std::endl;
	*out << cgicc::td() << std::endl;

	// Change form target to services iframe
	//
	*out << cgicc::td();

	*out << "<input type=\"button\" class=\"grey-button\" value=\"Search\" onclick=\"xploreSearch()\">" << std::endl;

	*out << cgicc::tr() << std::endl;

	*out << cgicc::tbody();
	*out << cgicc::table();

	*out << cgicc::br() << std::endl;

	*out << "<table>" << std::endl;
	*out << "	<tbody>" << std::endl;
	*out << "		<tr>" << std::endl;
	*out << "			<td style=\"vertical-align: top; padding-right: 20px;\">" << std::endl;

	*out << cgicc::table().set("class", "xdaq-table").set("id", "xdaq-xplore-search-results");
	*out << cgicc::thead() << std::endl;
	*out << cgicc::tr();
	*out << cgicc::th("Search Results");
	*out << cgicc::th("<img src=\"/hyperdaq/images/framework/xdaq-ajax-loader.gif\" class=\"xdaq-ajax-loader\">").set("style", "min-width: 30px;");
	*out << cgicc::tr() << std::endl;
	*out << cgicc::thead() << std::endl;
	*out << cgicc::tbody() << std::endl;
	*out << cgicc::tbody() << std::endl;
	*out << cgicc::table() << std::endl;

	*out << "			</td>" << std::endl;
	*out << "			<td style=\"vertical-align: top;\">" << std::endl;

	*out << cgicc::table().set("class","xdaq-table").set("id", "xdaq-xplore-display-results") << std::endl;
	*out << cgicc::thead() << std::endl;
	*out << cgicc::tr();
	*out << cgicc::th("Name");
	*out << cgicc::th("Value");
	*out << cgicc::tr() << std::endl;
	*out << cgicc::thead() << std::endl;
	*out << cgicc::tbody() << std::endl;
	*out << cgicc::tbody() << std::endl;
	*out << cgicc::table() << std::endl;

	*out << "			</td>" << std::endl;
	*out << "		</tr>" << std::endl;
	*out << "	</tbody>" << std::endl;
	*out << "</table>" << std::endl;

	*out << "	<script type=\"text/javascript\" src=\"/xplore/html/xdaq-xplore.js\"></script>" 					<< std::endl;

}

void xplore::Application::display (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{

	try
	{
		toolbox::Properties properties;
		cgicc::Cgicc cgi(in);
		std::string url = xgi::Utils::getFormElement(cgi, "url")->getValue();
		this->retrieveProperties(url, properties);

		//std::cout << "display url = " << url << std::endl;

		// Display property table
		std::vector < std::string > names = properties.propertyNames();
		std::vector<std::string>::iterator ni;

		for (ni = names.begin(); ni != names.end(); ++ni)
		{
			*out << cgicc::tr();
			*out << cgicc::td(*ni);

			std::string value = properties.getProperty(*ni);
			std::stringstream line;
			if (value.find("http://") == 0)
			{
				line << "<a id=\"list\" href=\"" << value << "\" target=\"_blank\">" << value << "</a>";
			}
			else
			{
				line << value;
			}

			*out << cgicc::td(line.str());
			*out << cgicc::tr() << std::endl;
		}

	}
	catch (xdaq::exception::Exception& e)
	{
		XCEPT_RAISE(xgi::exception::Exception,"Error retrieving attributes");
	}

	// what is returned might cause complaints in the browser if not set to plain text (no root node, junk elements etc)
	out->getHTTPResponseHeader().addHeader("Content-Type", "text/plain");
}
