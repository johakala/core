// $Id: RemoveFilterEvent.cc,v 1.2 2008/07/18 15:28:45 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xplore/RemoveFilterEvent.h"

			
xplore::RemoveFilterEvent::RemoveFilterEvent( toolbox::ActionListener * listener  )
	  :toolbox::Event("urn:xplore-event:RemoveFilterEvent", 0), listener_(listener)
{
}

xplore::RemoveFilterEvent::~RemoveFilterEvent()
{
	//std::cout << "$$$ DTORPublishJopEvent:  $$$ " << std::endl;
}
