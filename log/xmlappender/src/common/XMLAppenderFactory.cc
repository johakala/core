// $Id: XMLAppenderFactory.cc,v 1.4 2008/07/18 15:27:08 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini                                    *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include <log4cplus/spi/factory.h>
#include <log4cplus/helpers/loglog.h>
#include <log4cplus/thread/threads.h>
#include <log4cplus/helpers/property.h>
#include <log/xmlappender/XMLAppender.h>

using namespace log4cplus;
using namespace log4cplus::helpers;
using namespace log4cplus::spi;


///////////////////////////////////////////////////////////////////////////////
// LOCAL file class definitions
///////////////////////////////////////////////////////////////////////////////

namespace log4cplus 
{
	static const std::string type = "log4cplus::XMLAppender";
	class XMLAppenderFactory : public AppenderFactory 
	{
		public:

       
		~XMLAppenderFactory(){}
 
		SharedAppenderPtr createObject(const Properties& props)
        {
            return SharedAppenderPtr(new log4cplus::XMLAppender(props));
        }

        tstring const& getTypeName() const 
	{ 
            //return LOG4CPLUS_TEXT("log4cplus::XMLAppender"); 
            return type; 
        }

 
    };
}


void log4cplus::registerXMLAppenderFactory()
{
	AppenderFactoryRegistry& reg = getAppenderFactoryRegistry();
	std::auto_ptr<AppenderFactory> ptr(new XMLAppenderFactory());
	reg.put(ptr);
}
