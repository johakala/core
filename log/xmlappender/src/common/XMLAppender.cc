// $Id: XMLAppender.cc,v 1.5 2008/07/18 15:27:08 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini                                    *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include <log/xmlappender/XMLAppender.h>
#include <log4cplus/layout.h>
#include <log4cplus/helpers/loglog.h>
#include <log4cplus/spi/loggingevent.h>
#include <log4cplus/socketappender.h>
#include <log4cplus/helpers/property.h>

#include <stdlib.h>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <string>

using namespace log4cplus;
using namespace log4cplus::helpers;

#define LOG4CPLUS_MESSAGE_VERSION 100

static void appendExactString(SocketBuffer & b,const std::string & s)
{
	 for(std::string::size_type i = 0; i < s.size(); i++) b.appendByte(s[i]);
}
//////////////////////////////////////////////////////////////////////////////
// log4cplus::UDPAppender ctors and dtor
//////////////////////////////////////////////////////////////////////////////

log4cplus::XMLAppender::XMLAppender(const log4cplus::tstring& host, 
				    int port,
                                    const log4cplus::tstring& serverName,
				    const log4cplus::tstring& session)
: host_(host),
  port_(port),
  serverName_(serverName),
  session_(session)
{
	closed = true;
	this->openConnection();
}



log4cplus::XMLAppender::XMLAppender(const Properties properties)
 : Appender(properties),
   port_(1946)
{
	host_ = properties.getProperty( LOG4CPLUS_TEXT("host") );
	if(properties.exists( LOG4CPLUS_TEXT("port") )) 
	{
		tstring tmp = properties.getProperty( LOG4CPLUS_TEXT("port") );
		port_ = atoi(LOG4CPLUS_TSTRING_TO_STRING(tmp).c_str());
	}
	
	if(properties.exists( LOG4CPLUS_TEXT("session") )) 
	{
		session_ = properties.getProperty( LOG4CPLUS_TEXT("session") );
	}
	
	serverName_ = properties.getProperty( LOG4CPLUS_TEXT("ServerName") );
	this->openConnection();
}



log4cplus::XMLAppender::~XMLAppender()
{
	// destructor Impl calls close()
	destructorImpl();
}



//////////////////////////////////////////////////////////////////////////////
// log4cplus::UDPAppender public methods
//////////////////////////////////////////////////////////////////////////////

void 
log4cplus::XMLAppender::close()
{
	closed = true;
	getLogLog().debug(LOG4CPLUS_TEXT("Entering XMLAppender::close()..."));
	socket.close(); 
}



//////////////////////////////////////////////////////////////////////////////
// log4cplus::UDPAppender protected methods
//////////////////////////////////////////////////////////////////////////////


void
log4cplus::XMLAppender::openConnection()
{
    if(!socket.isOpen()) 
    {
       	socket = Socket(host_, port_);
	closed = false;
    }			
}

void
log4cplus::XMLAppender::append(const spi::InternalLoggingEvent& event)
{
	if(!socket.isOpen()) 
	{
        	openConnection();
        	if(!socket.isOpen()) 
		{
			// still not open after 2nd try, give up
            		getLogLog().error(LOG4CPLUS_TEXT("XMLAppender::append() - Cannot connect to server"));
            		return;
        	}
    	}

	SocketBuffer buffer(LOG4CPLUS_XML_MAX_MESSAGE_SIZE);
	log4cplus::xml::convertToXMLBuffer(buffer, event, serverName_, session_);
	//SocketBuffer msgBuffer(LOG4CPLUS_XML_MAX_MESSAGE_SIZE);

	//// msgBuffer.appendSize_t(buffer.getSize());
	//msgBuffer.appendBuffer(buffer);
	socket.write(buffer);
}


/////////////////////////////////////////////////////////////////////////////
// namespace log4cplus::helpers methods
/////////////////////////////////////////////////////////////////////////////


void log4cplus::xml::convertToXMLBuffer(log4cplus::helpers::SocketBuffer &buffer, const log4cplus::spi::InternalLoggingEvent& event,
                                    const log4cplus::tstring& serverName,
				    const log4cplus::tstring& session)
{
  //log4cplus::helpers::SocketBuffer buffer(LOG4CPLUS_XML_MAX_MESSAGE_SIZE );

  std::stringstream statement;
  statement << "<log4j:event logger=\"" << event.getLoggerName() << "\" ";
  statement << "timestamp=\"";
  
  std::ostringstream number_str;

  // Time format required is milliseconds since 1/1/1970. Take seconds and append the milliseconds
  //
  number_str << event.getTimestamp().sec() << std::setfill('0') << std::setw(3) << (event.getTimestamp().usec()/1000);

  statement << number_str.str() ;

  statement << "\" level=\"";
 
  std::string logLevelStr = getLogLevelManager().toString(event.getLogLevel());
  statement << logLevelStr;

  statement << "\" thread=\"" << event.getThread() << "\">";
  statement << "<log4j:message><![CDATA[" << event.getMessage() << "]]></log4j:message>";
  statement << "<log4j:NDC><![CDATA[";

  
  if (session != "")
  {
  	//buffer.appendString("<sid>" + session + "</sid>");
        statement << "<sid>" << session << "</sid>";	
	// In case the format in the NDC has to change, uncomment this
  }
  
  statement <<  event.getNDC() << "]]></log4j:NDC>";
  statement <<  "<log4j:throwable></log4j:throwable>";
  statement <<  "<log4j:locationInfo class=\"\" method=\"\" file=\"" << event.getFile() << "\" line=\"";
  
  std::ostringstream number3_str;
  number3_str << event.getLine();

  statement << number3_str.str();
  statement << "\"/></log4j:event>";

  if ( statement.str().size() > LOG4CPLUS_XML_MAX_MESSAGE_SIZE )
  {
	std::cout << "log4cplus::xml::convertToXMLBuffer() - log statment  too large (" << statement.str().size()  << "bytes), max size accepted is " <<
        LOG4CPLUS_XML_MAX_MESSAGE_SIZE  << std::endl; 
        std::cout << "log4cplus::xml::convertToXMLBuffer() - log statement attempted was:" << statement.str() << std::endl;

  }
  else
  {
    appendExactString(buffer,statement.str());
  }
  
  //return buffer;
}
