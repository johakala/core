// $Id: UDPAppender.cc,v 1.4 2008/07/18 15:26:55 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini                                    *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include <errno.h>
#include <stdlib.h>
#include <log/udpappender/UDPAppender.h>
#include <log4cplus/layout.h>
#include <log4cplus/helpers/loglog.h>
#include <log4cplus/spi/loggingevent.h>
#include <log4cplus/socketappender.h>
#include <log4cplus/helpers/property.h>

#include <iostream>
#include <sstream>
#include <string>

using namespace log4cplus;
using namespace log4cplus::helpers;

#define LOG4CPLUS_MESSAGE_VERSION 2


//////////////////////////////////////////////////////////////////////////////
// log4cplus::UDPAppender ctors and dtor
//////////////////////////////////////////////////////////////////////////////

log4cplus::UDPAppender::UDPAppender(const log4cplus::tstring& host, 
				    int port,
                                    const log4cplus::tstring& serverName,
				    const log4cplus::tstring& session)
: host_(host),
  port_(port),
  serverName_(serverName),
  session_(session)
{
	closed = true;
	this->openConnection();
}



log4cplus::UDPAppender::UDPAppender(const Properties properties)
 : Appender(properties),
   port_(1970)
{
	host_ = properties.getProperty( LOG4CPLUS_TEXT("host") );
	if(properties.exists( LOG4CPLUS_TEXT("port") )) 
	{
		tstring tmp = properties.getProperty( LOG4CPLUS_TEXT("port") );
		port_ = atoi(LOG4CPLUS_TSTRING_TO_STRING(tmp).c_str());
	}
	if(properties.exists( LOG4CPLUS_TEXT("session") )) 
	{
		session_ = properties.getProperty( LOG4CPLUS_TEXT("session") );
	}
	serverName_ = properties.getProperty( LOG4CPLUS_TEXT("ServerName") );
	closed = true;
	openConnection();
}



log4cplus::UDPAppender::~UDPAppender()
{
	// destructor Impl calls close()
	destructorImpl();
}



//////////////////////////////////////////////////////////////////////////////
// log4cplus::UDPAppender public methods
//////////////////////////////////////////////////////////////////////////////

void 
log4cplus::UDPAppender::close()
{
	getLogLog().debug(LOG4CPLUS_TEXT("Entering UDPAppender::close()..."));
	closed = true;
	::close(udpSocket_);   
}



//////////////////////////////////////////////////////////////////////////////
// log4cplus::UDPAppender protected methods
//////////////////////////////////////////////////////////////////////////////


void
log4cplus::UDPAppender::openConnection()
{
    if(closed == true)
    {
  	struct hostent *h;

  	/* get server IP address (no check if input is IP address or DNS name */
  	h = gethostbyname(host_.c_str());
  	if(h == NULL) 
	{
		std::string msg = "UDPAppender::openConnection() - Unknown host ";
		msg += host_;
		getLogLog().error(LOG4CPLUS_TEXT(msg));
		return;
  	}

  	remoteServAddr_.sin_family = h->h_addrtype;
  	memcpy((char *) &remoteServAddr_.sin_addr.s_addr, h->h_addr_list[0], h->h_length);
  	remoteServAddr_.sin_port = htons(port_);

  	udpSocket_ = socket(AF_INET,SOCK_DGRAM,0);
  	if(udpSocket_ < 0) 
	{
    		getLogLog().error(LOG4CPLUS_TEXT("UDPAppender::openConnection() - Cannot create socket"));
    		return;
  	}
	  
  	/* bind any local port */
	struct sockaddr_in cliAddr;
  	cliAddr.sin_family = AF_INET;
  	cliAddr.sin_addr.s_addr = htonl(INADDR_ANY);
  	cliAddr.sin_port = htons(0);
  
  	int rc = bind(udpSocket_, (struct sockaddr *) &cliAddr, sizeof(cliAddr));
  	if( rc < 0) 
	{
    		getLogLog().error(LOG4CPLUS_TEXT("UDPAppender::openConnection()- Cannot bind local address"));
		return;
  	}
	
	closed = false;
    }
}


void
log4cplus::UDPAppender::append(const spi::InternalLoggingEvent& event)
{
	if (closed == true)
	{
		this->openConnection();
	}
	
	// if still not opened, return
	if (closed == true)
	{
		return;
	}

	SocketBuffer buffer(LOG4CPLUS_MAX_MESSAGE_SIZE - sizeof(unsigned int));
	convertToBuffer(buffer, event);
	SocketBuffer msgBuffer(LOG4CPLUS_UDP_MAX_MESSAGE_SIZE);

	//msgBuffer.appendSize_t(buffer.getSize());
	msgBuffer.appendInt(buffer.getSize());
	msgBuffer.appendBuffer(buffer);

	int rc = sendto(udpSocket_, 
    		msgBuffer.getBuffer(), 
		msgBuffer.getSize(), 
		0, 
		(struct sockaddr *) &remoteServAddr_, 
		sizeof(remoteServAddr_));

	if(rc < 0) 
	{
		std::string msg = "UDPAppender::append() - Cannot send: ";
		msg += strerror(errno);
	    	getLogLog().error(LOG4CPLUS_TEXT(msg));
		this->close();
	}
}


//SocketBuffer
void log4cplus::UDPAppender::convertToBuffer(log4cplus::helpers::SocketBuffer & buffer, const log4cplus::spi::InternalLoggingEvent& event)
{
    //SocketBuffer buffer(LOG4CPLUS_MAX_MESSAGE_SIZE - sizeof(unsigned int));

    buffer.appendByte(LOG4CPLUS_MESSAGE_VERSION);
#ifndef UNICODE
    buffer.appendByte(1);
#else
    buffer.appendByte(2);
#endif

	buffer.appendString(serverName_);
	buffer.appendString(event.getLoggerName());
	buffer.appendInt(event.getLogLevel());

	// session_ and NDC are put one after the other
	// so that at the receiver side they can all be read into the NDC field
	// buffer.appendString(session_);
	if (session_ != "")
	{
		buffer.appendString("<sid>" + session_ + "</sid> " + event.getNDC());
	}
	else
	{
		buffer.appendString(event.getNDC());
	}

	buffer.appendString(event.getMessage());
	buffer.appendString(event.getThread());
	buffer.appendInt( static_cast<unsigned int>(event.getTimestamp().sec()) );
	buffer.appendInt( static_cast<unsigned int>(event.getTimestamp().usec()) );
	buffer.appendString(event.getFile());
	buffer.appendInt(event.getLine());

    //return buffer;
}

log4cplus::spi::InternalLoggingEvent
log4cplus::UDPAppender::readFromBuffer(SocketBuffer& buffer)
{
    unsigned char msgVersion = buffer.readByte();
    
    if(msgVersion != LOG4CPLUS_MESSAGE_VERSION) {
        //SharedObjectPtr<LogLog> loglog = log4cplus::helpers::LogLog::getLogLog();
        log4cplus::helpers::LogLog::Ptr loglog = log4cplus::helpers::LogLog::getLogLog();
        loglog->warn(LOG4CPLUS_TEXT("helpers::readFromBuffer() received socket message with an invalid version "));
    }

    unsigned char sizeOfChar = buffer.readByte();

    tstring serverName = buffer.readString(sizeOfChar);
    tstring loggerName = buffer.readString(sizeOfChar);
    LogLevel ll = buffer.readInt();
    tstring ndc = buffer.readString(sizeOfChar);
    
    if(serverName.length() > 0) 
    {
        if(ndc.length() == 0) 
	{
            ndc = "<host>" + serverName + "</host>";
        }
        else 
	{
            ndc = "<host>" + serverName + "</host>" + ndc;
        }
    }
    
    tstring message = buffer.readString(sizeOfChar);
    tstring thread = buffer.readString(sizeOfChar);
    long sec = buffer.readInt();
    long usec = buffer.readInt();
    tstring file = buffer.readString(sizeOfChar);
    int line = buffer.readInt();

     	MappedDiagnosticContextMap mdc;

    return log4cplus::spi::InternalLoggingEvent(loggerName, 
						ll, 
						ndc,
						mdc,
						message, 
						thread, 
						Time(sec, usec), 
						file, 
						line);
}
