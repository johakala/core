// Module:  Log4CPLUS
// File:    factory.cxx
// Created: 2/2002
// Author:  Tad E. Smith
//
//
// Copyright (C) Tad E. Smith  All rights reserved.

#include <log4cplus/spi/factory.h>
#include <log4cplus/helpers/loglog.h>
#include <log4cplus/thread/threads.h>
#include <log4cplus/helpers/property.h>

#include <log/udpappender/UDPAppender.h>

using namespace log4cplus;
using namespace log4cplus::helpers;
using namespace log4cplus::spi;


///////////////////////////////////////////////////////////////////////////////
// LOCAL file class definitions
///////////////////////////////////////////////////////////////////////////////

namespace log4cplus 
{
	static const std::string type = "log4cplus::UDPAppender";

	class UDPAppenderFactory : public AppenderFactory 
	{
		public:
        
		SharedAppenderPtr createObject(const Properties& props)
        {
            return SharedAppenderPtr(new log4cplus::UDPAppender(props));
        }

        tstring const& getTypeName() const 
	{ 
            //return LOG4CPLUS_TEXT("log4cplus::UDPAppender"); 
            return type; 
        }
    };
}


void log4cplus::registerUDPAppenderFactory()
{
	AppenderFactoryRegistry& reg = getAppenderFactoryRegistry();
	std::auto_ptr<AppenderFactory> ptr(new UDPAppenderFactory());
	reg.put(ptr);
}
