
#include <log4cplus/logger.h>
#include <log4cplus/configurator.h>
#include <appender4oracle/OracleAppender.h>
#include <log4cplus/layout.h>
#include <log4cplus/ndc.h>
#include <log4cplus/helpers/loglog.h>

using namespace log4cplus;
using namespace log4cplus::helpers;


using namespace log4cplus;

int main(int argc, char**argv)
{
	if (argc < 3)
	{
		std::cout << argv[0] << " propertiesFile loopCount" << std::endl;
		return 1;
	}

	std::string properties = argv[1];
	unsigned int loopCount = atoi(argv[2]);

	try 
	{
		helpers::LogLog::getLogLog()->setInternalDebugging(true);
		log4cplus::registerOracleAppenderFactory();
        PropertyConfigurator::doConfigure(properties);
	}
    	catch(...) 
	{
        	std::cout << "Configuration error." << std::endl;
        	return 1;
    	}

    Logger root = Logger::getRoot();
    Logger test = Logger::getInstance("test");
    Logger subTest = Logger::getInstance("test.subtest");

	cout << "Start measuring " << loopCount << " log message insertions." << endl;

	time_t start = time(0);

	for (int i = 0; i < loopCount; i++)
	{
		LOG4CPLUS_INFO(subTest, "Test log message");
	}
	
	time_t end = time(0);
	
	std::cout << "Stopped measuring " << loopCount << " log message insertions." << std::endl;
	
	unsigned long duration = end-start;
	if (duration < 1)
	{
		std::cout << "Cannot calculate insertions per second. Test run too short." << std::endl;
	} 
	else
	{
		std::cout << "Test time: " << duration << " seconds." << std::endl;
		std::cout << "Insertions per second: " << (double) loopCount/ (double) duration << std::endl;
	}

    return 0;
}


