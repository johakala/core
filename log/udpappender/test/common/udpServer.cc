// $Id: udpServer.cc,v 1.3 2008/07/18 15:27:02 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini                                    *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <stdio.h>
#include <unistd.h> 
#include <string.h> 

#include <log4cplus/config.h>
#include <log4cplus/configurator.h>
#include <log4cplus/consoleappender.h>
#include <log4cplus/socketappender.h>
#include <log4cplus/helpers/loglog.h>
#include <log4cplus/helpers/socket.h>
#include <log4cplus/helpers/threads.h>
#include <log4cplus/spi/loggerimpl.h>
#include <log4cplus/spi/loggingevent.h>

#include <log4cplus/layout.h>
#include <log4cplus/helpers/loglog.h>
#include <log4cplus/spi/loggingevent.h>

#include <iostream>
#include <string>

using namespace log4cplus;
using namespace log4cplus::helpers;
using namespace log4cplus::thread;

#define LOCAL_SERVER_PORT 1970
#define MAX_MSG 8*1024

#define LOG4CPLUS_MESSAGE_VERSION 2

log4cplus::spi::InternalLoggingEvent readFromUDPBuffer(SocketBuffer& buffer)
{
    unsigned char msgVersion = buffer.readByte();
    
    if(msgVersion != LOG4CPLUS_MESSAGE_VERSION) 
    {
        SharedObjectPtr<LogLog> loglog = log4cplus::helpers::LogLog::getLogLog();
        loglog->warn(LOG4CPLUS_TEXT("helpers::readFromBuffer() received socket message with an invalid version "));
    }

    unsigned char sizeOfChar = buffer.readByte();

    tstring serverName = buffer.readString(sizeOfChar);
    tstring loggerName = buffer.readString(sizeOfChar);
    LogLevel ll = buffer.readInt();
    tstring ndc = buffer.readString(sizeOfChar);
    
    if(serverName.length() > 0) 
    {
        if(ndc.length() == 0) 
	{
            ndc = "<host>" + serverName + "</host>";
        }
        else 
	{
            ndc = "<host>" + serverName + "</host>" + ndc;
        }
    }
    
    tstring message = buffer.readString(sizeOfChar);
    tstring thread = buffer.readString(sizeOfChar);
    long sec = buffer.readInt();
    long usec = buffer.readInt();
    tstring file = buffer.readString(sizeOfChar);
    int line = buffer.readInt();

    return log4cplus::spi::InternalLoggingEvent(loggerName,
                                                ll,
                                                ndc,
                                                message,
                                                thread,
                                                Time(sec, usec),
                                                file,
                                                line);
}

int main(int argc, char *argv[]) 
{

	// Initialize logging system
	if(argc < 3) 
	{
        	cout << "Usage: port config_file" << endl;
        	return 1;
    	}

	int port = atoi(argv[1]);
    	tstring configFile = LOG4CPLUS_C_STR_TO_TSTRING(argv[2]);

    	PropertyConfigurator config(configFile);
    	config.configure();
    
  	int sd, rc, n;
	socklen_t cliLen;
  	struct sockaddr_in cliAddr, servAddr;
  	char msg[MAX_MSG];
 
  	sd=socket(AF_INET, SOCK_DGRAM, 0);
  	if(sd<0) 
	{
		std::cout << "Cannot open socket: " << strerror(errno) << std::endl;
    		return 1;
	}

  	// bind local server port 
  	servAddr.sin_family = AF_INET;
  	servAddr.sin_addr.s_addr = htonl(INADDR_ANY);
  	servAddr.sin_port = htons(port);
  	rc = bind (sd, (struct sockaddr *) &servAddr,sizeof(servAddr));
  	if(rc<0) 
	{
		std::cout << "Cannot bind port number " << port << ": " << strerror(errno);
		std::cout << std::endl;
    		return 1;
  	}

	std::cout << "Waiting for log messages on UDP port " << port << std::endl;
	std::cout << "-----------------------------------------" << std::endl;

  	while(1) 
	{
    		//init buffer 
    		memset(msg,0x0,MAX_MSG);

    		// receive message 
    		cliLen = sizeof(cliAddr);
    		n = recvfrom(sd, msg, MAX_MSG, 0, (struct sockaddr *) &cliAddr, &cliLen);

    		if(n<0) 
		{
			std::cout << "Cannot receive data: " << strerror(errno) << std::endl;
      			continue;
    		}
    
		std::cout << "From " << inet_ntoa(cliAddr.sin_addr) << ":";
		std::cout << ntohs(cliAddr.sin_port) << " >> ";

		// Convert received message into logging event
		//
		size_t msgSize = ntohl(*((size_t*) msg));
	
		// std::cout << "Received buffer size " << msgSize << std::endl;
		   
		SocketBuffer buffer(msgSize);
	
		// Currently copy over, improve in the future
		//
		memcpy (buffer.getBuffer(), msg + sizeof(size_t), msgSize );

		spi::InternalLoggingEvent event = readFromUDPBuffer(buffer);
		Logger logger = Logger::getInstance(event.getLoggerName());
		if (logger.isEnabledFor(event.getLogLevel()))
			logger.callAppenders(event);  
  	}

	return 0;
}
