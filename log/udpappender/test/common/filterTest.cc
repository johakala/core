
#include <log4cplus/logger.h>
#include <log4cplus/configurator.h>
#include <appender4oracle/OracleAppender.h>
#include <log4cplus/layout.h>
#include <log4cplus/ndc.h>
#include <log4cplus/helpers/loglog.h>

using namespace log4cplus;
using namespace log4cplus::helpers;


using namespace log4cplus;

int main(int argc, char**argv)
{
	if (argc < 3)
	{
		cout << argv[0] << " [path to filterTest.properties] loopCount" << endl;
		return 1;
	}

	std::string properties = argv[1];
	unsigned int loopCount = atoi(argv[2]);

	try 
	{
		helpers::LogLog::getLogLog()->setInternalDebugging(true);
		log4cplus::registerOracleAppenderFactory();
        PropertyConfigurator::doConfigure(properties);
	}
    catch(...) 
	{
        cout << "Configuration error." << endl;
        return 1;
    }

    Logger root = Logger::getRoot();
    Logger test = Logger::getInstance("test");
    Logger subTest = Logger::getInstance("test.subtest");

	cout << endl << "Producing info/warn/error messages. Only error level messages are stored in the database." << endl << endl;

	cout << "Start measuring " << loopCount << " log message insertions." << endl;

	time_t start = time(0);

	for (int i = 0; i < loopCount; i++)
	{
		LOG4CPLUS_INFO(subTest, "Test info log message");
		LOG4CPLUS_WARN(subTest, "Test warning log message");
		LOG4CPLUS_ERROR(subTest, "Test error log message");
	}
	
	time_t end = time(0);
	
	cout << "Stopped measuring " << loopCount << " log message insertions." << endl;
	
	unsigned long duration = end-start;
	if (duration < 1)
	{
		cout << "Cannot calculate insertions per second. Test run too short." << endl;
	} 
	else
	{
		cout << "Test time: " << duration << " seconds." << endl;
		cout << "Insertions per second: " << (double) loopCount/ (double) duration << endl;
	}

    return 0;
}


